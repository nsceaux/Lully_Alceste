\version "2.19.80"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}
%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "Alceste" }
  \markup\null
}

\actn "Prologue"
%% 0-3
\pieceTocNb "0-3" "Bruit de Guerre"
\includeScore "AACbruitGuerre"

%% 0-5
\pieceTocNb "0-5" "Rondeau pour la Gloire"
\includeScore "AAErondeau"

%% 0-13
\pieceTocNb "0-13" \markup\wordwrap {
  Air pour les divinités des fleuves et les nymphes
}
\includeScore "AAMentree"

\act "Acte Second"
%% 2-3
\scene "Scene Troisiéme" "Scene III"
\sceneDescription\markup\wordwrap-center {
  Admete et Alcide
}
\pieceTocNb "2-3" "Marche en rondeau"
\includeScore "CCAmarche"

%% 2-5
\scene "Scene Quatriême" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  Licomede, Straton, Admete, Alcide, Lychas
}
\pieceTocNb "2-5" \markup\wordwrap {
  Licomede, Admete, Alcide, chœur : \italic { Marchez, marchez, marchez }
}
\includeScore "CDAchoeur"

%% 2-6
\pieceTocNb "2-6" "Entrée"
\includeScore "CDBentree"

%% 2-7
\pieceTocNb "2_7" \markup\wordwrap {
  Chœur, Lychas, Straton : \italic { Achevons d’emporter la Place }
}
\includeScore "CDCchoeur"
