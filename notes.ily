 \notesSection "Notes"
\markuplist\with-line-width-ratio #0.7 \column-lines {
  \livretAct NOTES
  \null
  \livretParagraph {
    Cette édition de l'opéra \italic { Alceste }
    de Jean-Baptiste Lully,
    réalisée pour Les Talens Lyriques – Christophe Rousset,
    est basée sur les sources suivantes :
  }
  \null
  %% manuscrit
  \indented-lines#5 {
    \line\bold { [Manuscrit] }
    \line {
      \italic { Alceste, }
      Jean-Baptiste Lully (1632-1687).
    }
    \line { Partition manuscrite complète, ca. 1703 }
    \wordwrap-lines {
      Bibliothèque Nationale de France, RES-F-1701.
      \with-url #"http://catalogue.bnf.fr/ark:/12148/cb39759381p"
      \smaller\typewriter "http://catalogue.bnf.fr/ark:/12148/cb39759381p"
    }
  }
  \null
  %% gravure
  \indented-lines#5 {
    \line\bold { [Édition gravée] }
    \wordwrap-lines {
      \italic {
        Alceste, tragédie, mise en musique par feu Mr de Lully,
        première édition gravée par H. de Baussen,
      }
      Jean-Baptiste Lully (1632-1687).
    }
    \line { Partition imprimée réduite. Baussen, Paris, 1708 }
    \wordwrap-lines {
      Bibliothèque nationale de France, département Musique, VM2-9
      \with-url #"http://catalogue.bnf.fr/ark:/12148/cb43122859t"
      \smaller\typewriter "http://catalogue.bnf.fr/ark:/12148/cb43122859t"
    }
  }
  \null
  %% livret
  \indented-lines#5 {
    \line\bold { [Livret imprimé] }
    \wordwrap-lines {
      \italic {
        Alceste ou Le triomphe d'Alcide. Tragedie.
        Representée par l'Academie royale de musique.
      }
      Quinault, Philippe (1635-1688)
    }
    \line { Monographie imprimée. René Baudry, Paris, 1675. }
    \wordwrap-lines {
      Bibliothèque nationale de France, département Littérature et art, Rés.-Yf-1249
      \with-url #"http://catalogue.bnf.fr/ark:/12148/cb311645546"
      \smaller\typewriter "http://catalogue.bnf.fr/ark:/12148/cb311645546"
    }
  }
  \null
  \paragraph {
    Le matériel d'orchestre est constitué des parties
    séparées suivantes :
    dessus, hautes-contre, tailles, quintes, basses, basse continue,
    timbales.
  }
  \null
  \paragraph {
    Cette édition est distribuée selon les termes de la licence
    Creative Commons Attribution-ShareAlike 4.0.  Il est donc permis,
    et encouragé, de jouer cette partition, la distribuer,
    l’imprimer.
  }
}