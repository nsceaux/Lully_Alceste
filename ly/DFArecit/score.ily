\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\pageBreak
        s1*4 s2.\break s2.*3\break s2. s1 s2.\break
        s2.*3\break s2. s1*2\pageBreak
        s1 s2.*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
