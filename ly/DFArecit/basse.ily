\clef "basse" sol,2 sol4. sol8 |
sol2 fad |
sol4 sol8 fad mi2 |
r4 la8 sol fad2 |
r4 si8 la sol2~ |
sol la4 la, |
re re8 do si,2 |
do dod |
re4 sol, re,2 |
<<
  \tag #'basse {
    sol,4 r r |
    R2.*4 R1 R2.*5 R1*3 R2.*4 |
  }
  \tag #'basse-continue {
    sol,8 sol fad2 |
    sol2~ sol8 fad |
    mi4( re8) do si,4~ |
    si, do re |
    sol, sol8[ fa] mi[ re] |
    do si, la,4 si, si |
    sold la la, |
    re4. mi8 fad4 |
    sol2~ sol8 fad |
    mi4( re8.) do16 si,4~ |
    si, do re |
    sol,2 la, |
    si, mi4. re8 |
    dod2 la, |
    re4. mi8 fad4 |
    sol2 sol8 fad |
    mi4 re8. do16 si,4~ |
    si, do re |
    \custosNote sol,4
  }
>>
