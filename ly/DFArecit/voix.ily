\clef "vtaille" R1*9 |
r4 r <>^\markup\character Admete re'8. re'16 |
si4\trill si8 re' mi'8. fad'16 |
sol'4 si8. do'16 re'4 |
re'8. mi'16 do'8.\trill si16 la4\trill |
sol4 si8. si16 do'8. re'16 |
mi'4 mi'8. fad'16 red'4 si8. si16 |
mi'8. si16 do'4 do'8. si16 |
la4\trill la re'8. re'16 |
si4\trill si8 re' mi'8. fad'16 |
sol'4 si8. do'16 re'4 |
re'8. mi'16 do'8.\trill si16 la4\trill |
sol4 si8 si do'4 la8.\trill sol16 |
fad4 r8 si sold4. sold8 |
la4 mi8. fad16 sol4 sol8. la16 |
fad4\trill fad re'8. re'16 |
si4\trill si8 re' mi'8. fad'16 |
sol'4 si8. do'16 re'4 |
re'8. mi'16 do'8.\trill si16 la4\trill |
sol4
