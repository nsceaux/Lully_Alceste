\score {
  \new GrandStaff <<
    \new Staff << \global \includeNotes "dessus1" >>
    \new Staff << \global \includeNotes "dessus2" >>
  >>
  \layout {
    \context {
      \Staff
      \override MultiMeasureRest.expand-limit = 2
    }
  }
}
