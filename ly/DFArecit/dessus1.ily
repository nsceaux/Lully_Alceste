\clef "dessus" r4 si'8 do'' re''4 mi''8 re'' |
do''2 r4 re''8. re''16 |
re''2 r4 sol''8 fad'' |
mi''2 r4 la''8 sol'' |
fad''2 r4 si''8 la'' |
sol''4 sol''8 fad'' mi''4 la''8 sol'' |
fad''2 si''4. si''8 |
si''2 la''4.\trill la''8 |
la''4 si'' la''4.\trill sol''8 |
sol''4 r r |
R2.*4 R1 R2.*5 R1*3 R2.*4 |
