\key sol \major \tempo "Ritournelle"
\time 2/2 \midiTempo#160 s1*9
\digitTime\time 3/4 \midiTempo#80 s2.*5
\time 4/4 s1
\digitTime\time 3/4 s2.*5
\time 4/4 s1*3
\digitTime\time 3/4 s2.*4
\time 4/4
