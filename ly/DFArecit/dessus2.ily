\clef "dessus" r4 sol'8 la' si'4 do''8 si' |
la'2 r4 si'8 do'' |
si'2 r4 mi''8 re'' |
dod''2 r4 fad''8 mi'' |
re''2 r4 sol''8 fad'' |
mi''4 mi''8 re'' dod''4.( si'16 dod'') |
re''2 r4 sol''8. fad''16 |
mi''2. fad''8 sol'' |
fad''4 sol''8 la'' fad''4.\trill sol''8 |
sol''4 r r |
R2.*4 R1 R2.*5 R1*3 R2.*4 |
