\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s2. s1\break s2.*2 s4 \bar "" \pageBreak
        s2. s1\break s1 s2.*4\break s2.*7\break s2.*7\break
      }
    >>
  >>
  \layout { }
  \midi { }
}