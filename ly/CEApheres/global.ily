\key do \major
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 \midiTempo#160 \bar "|!:" s2.*9 \bar ":||:"
s2.*15 \alternatives s2. s2. \bar "|."
