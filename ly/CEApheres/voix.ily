\clef "vtaille" r4 r8 sol do' do'16 mi' do'8.\trill do'16 |
sol8. sol16 sol8. sol16 mi4\trill r8 sol |
do'4 do'8 do'16 re' mi'8. fa'16 |
re'4\trill r si8 r16 si si8 si16 re' |
sol4 r16 sol sol sol re8. mi16 |
fa8 fa r re'16 re' la8 la16 si |
do'8. do'16 do' do' do' si si4\trill r8 sol16 sol |
do'8. do'16 sol8 la16 sib la8\trill la r8 fa16 la |
re8\trill re r16 re re mi do2 |
sol4 sol sol |
la2 si4 |
do'2 do'4 |
r mi' re' |
do' do' si |
la2\trill la4 |
re'2 re'4 |
do' si la |
si2.\trill |
re'4 do' si |
la2\trill sol4 |
fad2 sol4 |
la2 re4 |
la2 la4 |
la2 si4 |
sol la fad |
sol2. |
si4 si do' |
re'2 re'4 |
mi'2 do'4 |
re'2 sol4 |
do'2 sol4 |
la2 si4 |
do' re' si\trill |
do'2. |
do' |
