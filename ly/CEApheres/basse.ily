\clef "basse" do1~ |
do~ |
do2. |
sol2 sol, |
sol,2. |
re |
la,8. sol,16 fad,4 sol,2 |
mi,2 fa, |
sol, do, |
do2 do4 |
fa2 fa4 |
mi2 fa4 |
do2 si,4 |
la,2 sol,4 |
re2 re4 |
si,2 si,4 |
do re re, |
sol,2. |
sol |
fad2 mi4 |
re2 sol,4 |
fad,2 sol,4 |
re, re la, |
re2 si,4 |
mi do re |
sol,2. |
sol4 sol do |
si,2 si,4 |
do2 do4 |
sol,2 sol4 |
mi2. fa2 sol4 |
la fa sol |
do2. |
do2 si,8 la, |
\once\set Staff.whichBar = "|"