Cou -- ra -- ge, cou -- rage en -- fants, je suis à vous ;
mon bras va se -- con -- der vos coups :
mais c’en est dé -- ja fait, & l’on a pris la vil -- le ;
la foi -- bles -- se de l’âge a re -- tar -- dé mes pas :
la va -- leur de -- vient i -- nu -- ti -- le
quand la for -- ce n’y ré -- pond pas.
Que la vieil -- lesse est len -- te,
les ef -- forts qu’el -- le ten -- te
sont toû -- jours im -- puis -- sans :
c’est u -- ne char -- ge bien pe -- san -- te
qu’un far -- deau de qua -- tre- vingts ans.
C’est u -- ne char -- ge bien pe -- san -- te
qu’un far -- deau de qua -- tre- vingts ans.
ans.
