\key sol \major
\time 2/2 \midiTempo#160 s1*4
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2. \midiTempo#120
\bar "|!:" s2.*4 \alternatives s2. s2. s2.*11
\time 2/2 \midiTempo#160 s1*4
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1*2 \bar "|."