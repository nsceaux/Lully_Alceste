\clef "basse" sol,1~ |
sol,2. sol4 |
re'2 si |
do' la |
mi'4 si do'8 la |
re'4 sol re2 |
sol,2. |
sol |
fad4 re2 |
mi2 do4 |
re2 re,4 |
sol,2. |
sol,2 sol4 |
la2. |
si2 si,4 |
mi4. re8 do si, |
la,4. si,8 do4 |
si,8 la, mi4 mi, |
la, la4. sol8 |
fad4 sol sol, |
re4. mi8 fad4 |
sol4. fa8 mi re |
do re do si, la,4 |
re8 sol, re,2 |
sol,1~ |
sol,2 sol |
re' si |
do' la |
mi'4 si do'8 la |
re'4 sol re2 |
sol,1 |
