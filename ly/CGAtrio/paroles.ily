\tag #'(alceste basse) {
  Cher -- chons cher -- chons Ad -- me -- te promp -- te -- ment promp -- te -- ment
  cher -- chons Ad -- me -- te promp -- te -- ment.
}
\tag #'cephise {
  Cher -- chons Ad -- me -- te promp -- te -- ment promp -- te -- ment
  cher -- chons Ad -- me -- te promp -- te -- ment.
}
\tag #'pheres {
  Cher -- chons cher -- chons Ad -- me -- te promp -- te -- ment
  cher -- chons cher -- chons Ad -- me -- te promp -- te -- ment.
}
\tag #'(alceste basse) {
  Peut- on cher -- cher ce qu’on ai -- me
  a -- vec trop d’em -- pres -- se -- ment ! - ment !
  Quand l’a -- mour est ex -- tré -- me,
  le moindre es -- loi -- gne -- ment
  est un cru -- el tour -- ment.
  Quand l’a -- mour est ex -- tré -- me,
  le moindre es -- loi -- gne -- ment
  est un cru -- el tour -- ment.
}
\tag #'(alceste basse) {
  Cher -- chons cher -- chons Ad -- me -- te promp -- te -- ment promp -- te -- ment
  cher -- chons Ad -- me -- te promp -- te -- ment.
}
\tag #'cephise {
  Cher -- chons Ad -- me -- te promp -- te -- ment promp -- te -- ment
  cher -- chons Ad -- me -- te promp -- te -- ment.
}
\tag #'pheres {
  Cher -- chons cher -- chons Ad -- me -- te promp -- te -- ment
  cher -- chons cher -- chons Ad -- me -- te promp -- te -- ment.
}
