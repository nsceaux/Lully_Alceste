\score {
  \new ChoirStaff \with { \haraKiri } <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'alceste \includeNotes "voix"
    >> \keepWithTag #'alceste \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'cephise \includeNotes "voix"
    >> \keepWithTag #'cephise \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'pheres \includeNotes "voix"
    >> \keepWithTag #'pheres \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\pageBreak
        s2. s1 s2.\break s2.*4\break s2.*5\break s2.*5\break s2.*3 s1\pageBreak
        s1*3 s2.\break
      }
      \modVersion { s1*4 s2. s1 s2.\break }
    >>
  >>
  \layout { }
  \midi { }
}