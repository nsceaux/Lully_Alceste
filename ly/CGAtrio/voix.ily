<<
  \tag #'(alceste basse) {
    \clef "vbas-dessus" <>^\markup\character Alceste
    r2 r4 sol' |
    re''4. re''8 si'4.\trill si'8 |
    la'4. la'8 si'4. si'8 |
    sol'2 do''4. re''8 |
    si'4. re''8 sol' do'' |
    la'4 si' la'4.\trill sol'8 |
    sol'2. |
    %%
    \tag #'alceste <>^\markup\character Alceste
    si'4 do'' re'' |
    la'2\trill si'8 fad' |
    sol'4 mi' do''8. si'16 |
    la'4.\trill la'8 si'8. do''16 |
    si'2.\trill |
    si'2 si'8 si' |
    do''4 la'4.\trill sol'8 |
    fad'4\trill fad'4. si'8 |
    sold'4. sold'8 la' si' |
    do''2 r8 do'' |
    re'' mi'' sold'4.\trill la'8 |
    la'2 do''8. do''16 |
    do''4 si'4. do''8 |
    la'4\trill la' r8 re'' |
    si'4.\trill si'8 do'' re'' |
    mi''2 r8 do'' |
    la' si' la'4.\trill sol'8 |
    sol'2 r4 si' |
    re''4. re''8 si'4.\trill si'8 |
    la'4 la' si'4. si'8 |
    sol'2 do''4. re''8 |
    si'4.\trill re''8 sol' do'' |
    la'4 si' la'4.\trill sol'8 |
    sol'1 |
  }
  \tag #'cephise {
    \clef "vbas-dessus" <>^\markup\character Céphise
    R1 |
    r4 re' sol'4. sol'8 |
    fad'4.\trill fad'8 sol'4. sol'8 |
    mi'2 mi'4. fad'8 |
    sol'4. si'8 mi' la' |
    fad'4 sol' fad'4.\trill sol'8 |
    sol'2. |
    R2.*17 |
    R1 |
    <>^\markup\character Céphise
    r4 re' sol'4. sol'8 |
    fad'4\trill fad' sol'4. sol'8 |
    mi'2\trill mi'4. fad'8 |
    sol'4. si'8 mi' la' |
    fad'4 sol' fad'4.\trill sol'8 |
    sol'1 |
  }
  \tag #'pheres {
    \clef "vtaille" <>^\markup\character Pheres
    R1 |
    r2 r4 sol |
    re'4. re'8 si4.\trill si8 |
    do'4. do'8 la4. la8 |
    mi'8 mi' si8.\trill si16 do'8 la |
    re'4 sol re4. sol8 |
    sol2. |
    R2.*17 |
    R1 |
    <>^\markup\character Pheres
    r2 r4 sol |
    re'4. re'8 si4.\trill si8 |
    do'4 do' la4. la8 |
    mi'8 mi' si si do' la |
    re'4 sol re4. sol8 |
    sol1 |
  }
>>