s1*2 s2 <6> s1
s4 <6>2 s1 s2.*2 <6>2 <6>4 s2 <6 5>4 <4>2 \new FiguredBass <_+>4
s2.*2 <7>2 <6+ 5>4 <_+>2\figExtOn <_+>4\figExtOff <_+>4. <4+>8 <6> <6+>
s2 <6>4 <6+> <_+>\figExtOn <_+>\figExtOff s2. <5/> s2 <6>4
s4. <4+>8 <6> <6 _-> s2. <7 _+>4 \new FiguredBass { <4> <3> } s1
s1 s2 <6> s1 s4 <6>2 <7 _+>1
