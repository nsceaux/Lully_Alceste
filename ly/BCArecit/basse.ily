\clef "basse" <<
  \tag #'basse-part { R1*88 r2 r4 }
  \tag #'basse-conducteur {
    R1*6 R2.*2 R1*3 R2.*28 R1*3 R2.*2 R1 R2.*15 R1 R2.*11
    R1*2 R2.*2 R1 R1 R2. R1*9
  }
  \tag #'basse-continue {
    la,1~ |
    la,~ |
    la, |
    mi~ |
    mi |
    dod |
    re2~ re8 do |
    si,4 la,2 |
    sol,4 sol8 mi fa4 sol8 sol, |
    do1 |
    fa2 re |
    mi fad4 |
    sold2. |
    la4. sol8 fa mi |
    re4 re,2 |
    mi, mi8 re |
    do2 re4~ |
    re mi mi, |
    la,2 la4 |
    si2. |
    do' |
    si | \allowPageTurn
    do'2 do'4 |
    si la2 |
    sol2 mi4 |
    la2. |
    si2 mi4 |
    la, si,2 |
    mi, mi8 re |
    dod4 re4. do?8 |
    si,2 do4 |
    fa, sol,2 |
    do4. do'8 si la |
    sold2. |
    la4. sol8 fa mi |
    re4 re,2 |
    mi, mi8 re |
    do2 re4~ |
    re mi mi, |
    la,1 |
    mi4. re8 do2 |
    si, sold, |
    la,4 la fad |
    sol mi re |
    do dod re mi8 mi, |
    la,2. |
    la2 la4 |
    si2 si4 |
    do'2 mi4 |
    fa2 do4 |
    sol sol,2 |
    do2. |
    do'4 dod'4. dod'8 |
    re'4 re sib |
    sol la la, |
    re8 mi fa4 re |
    la2 sol4 |
    fa2. |
    mi2 sold,4 |
    la, mi,2 |
    la, re, |
    la,2. |
    re4 mi8 do re re, |
    sol,2. |
    sol2 la4 |
    si si do' |
    re' re mi |
    fa2. |
    mi2 fad4 |
    sold2. |
    la4 fa mi |
    re la,2 |
    re1 |
    si,1 |
    do4 la,2 |
    si,4 sol,8. la,16 si,4 |
    mi, mi la fad |
    sol2 mi |
    fa8. mi16 re8 do sol sol, |
    do1~ |
    do~ |
    do |
    sol,~ |
    sol,2. fa,8 mi, |
    re,2 re~ |
    re1 | \allowPageTurn
    la2 la, |
    mi fa8 re mi mi, |
    la,2 la,4
  }
>> la,8. si,16 |
do2 re4 re8 re |
mi2 mi4 mi |
la la8 fa sol4 sol, |
do2 do'8 si la sol |
fad4. fad8 sol2 |
mi fa4. re8 |
mi4. mi8 la4 la8 sol |
fa4. mi8 re2 |
re4. re8 mi4 mi, |
<<
  \tag #'(basse-part basse-conducteur) {
    la,2 r4 |
    R2.*4 R1*2 |
  }
  \tag #'basse-continue {
    la,2.~ |
    la, |
    si, |
    mi, |
    mi4. re8 dod4 |
    re2 mi4 mi, |
    la,2~ la,8 sib, la, sol, |
    \once\set Staff.whichBar = "|"
    \custosNote fa,4
  }
>>
