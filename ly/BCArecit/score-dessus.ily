\score {
  \new GrandStaff <<
    \new Staff <<
      { \time 4/4 s1*88 s4
        s_\markup\whiteout\right-align\line\italic { Que je te donne par pitié. }
        s2 s1*9 \digitTime\time 3/4 s2.*5 \time 4/4 s1 \time 2/2 s1 \bar "|." }
      \keepWithTag #'part \includeNotes "dessus1"
      { s1*88 s1*4\break s1*5\break }
    >>
    \new Staff << \keepWithTag #'part \includeNotes "dessus2" >>
  >>
  \layout { }
}