\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:score "score-basse")
   (basse-continue
    #:music , #{ s1*6 s2.*2 s1*3 s2.*25\break #}
    #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#105 #}))
