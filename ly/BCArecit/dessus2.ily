\clef "dessus"
<<
  \tag #'conducteur {
    R1*6 R2.*2 R1*3 R2.*28 R1*3 R2.*2 R1 R2.*15 R1 R2.*11
    R1*2 R2.*2 R1 R1 R2. R1*9
  }
  \tag #'part { R1*88 }
>>
r2 r4 mi''8. re''16 |
do''2 si'4 si'8 la' |
sold'2 sold'4 si' |
do'' do''8 do'' do''4 si' |
do''2 sol'8 sol' la' si' |
do''4. re''8 si'4 si' |
r8 sol'' sol'' sol'' la''4 la''8 si'' |
sold''4. si'8 do''4 do''8 si' |
la'4. la'8 re''4 re''8 do'' |
si'4 si'8 la' sold'4. la'8 |
la'2 r4 |
R2.*4 R1*2 |
