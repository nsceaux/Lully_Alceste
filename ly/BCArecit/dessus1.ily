\clef "dessus"
<<
  \tag #'conducteur {
    R1*6 R2.*2 R1*3 R2.*28 R1*3 R2.*2 R1 R2.*15 R1 R2.*11
    R1*2 R2.*2 R1 R1 R2. R1*9
  }
  \tag #'part { R1*88 }
>>
r2 r4 <>^"Violons" do''8. re''16 |
mi''2 re''4 re''8 do'' |
si'2 si'4 mi'' |
mi'' mi''8 fa'' re''4 sol''8 fa'' |
mi''2 mi''8 mi'' fad'' sol'' |
la''4. la''8 re''4 re'' |
r8 mi'' mi'' mi'' do''4 do''8 re'' |
si'4. mi''8 mi''4. mi''8 |
la''4 la''8 sol'' fa''4 fa''8 mi'' |
re''4 re''8 do'' si'4 si'8 do'' |
la'2 r4 |
R2.*4 R1*2 |
