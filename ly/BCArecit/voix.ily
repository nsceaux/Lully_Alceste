<<
  %% Lychas
  \tag #'(lychas basse) {
    <<
      \tag #'basse { s1. \ffclef "vhaute-contre" <>^\markup\character Lychas }
      \tag #'lychas { \clef "vhaute-contre" <>^\markup\character Lychas R1 r2 }
    >> r8 mi'16 mi' la4 |
    r do' do' r8 do'16 si |
    si4\trill <<
      \tag #'basse {
        s2. s1*2 s2.*2 s4
        \ffclef "vhaute-contre"
        \once\override TextScript.outside-staff-priority = #9999
        <>^\markup\character Lychas
      }
      \tag #'lychas { r4 r2 R1*2 R2.*2 r4 }
    >> r8 <>^"très viste" sol' la'16[ sol'] fa'[ mi'] re'4\trill |
    do' <<
      \tag #'basse {
        s2. s1 s2.
        \ffclef "vhaute-contre" <>^\markup\character Lychas
      }
      \tag #'lychas { r4 r2 R1 R2. }
    >>
    r8 mi'8 mi'4 si |
    do'8[ re' do'] si( la4) |
    la'8[ sol'] fa'[ mi'] re'[ do'] |
    si[ do' si] la8( sold4) |
    r4 r8 mi' fa'[ mi'] |
    re'[ do'] do'4( si)\trill |
    la2 do'4 |
    re' mi' fa' |
    mi' fa'8[ mi'] re'[ do'] |
    sol'2 r8 re' |
    mi'4 mi' mi' |
    re' do'\trill( si8) do' |
    si2\trill sol'8. sol'16 |
    sol'4 fad'4. mi'8 |
    red'2 r8 mi' |
    mi'4( red'2) |
    mi'2 sol'8. sol'16 |
    sol'4 fa'4. fa'8 |
    fa'2 r8 mi' |
    mi'4( re'2)\trill |
    do'2. |
    r8 mi' mi'4 si |
    do'8[ re' do'] si( la4) |
    la'8[ sol'] fa'[ mi'] re'[ do'] |
    si[ do' si] la8( sold4) |
    r4 r8 mi' fa'[ mi'] |
    re'[ do'] do'4( si)\trill |
    la <<
      \tag #'basse {
        s2. s1 s8
        \ffclef "vhaute-contre" <>^\markup\character Lychas
      }
      \tag #'lychas { r4 r2 R1 r8 }
    >> si16 si si8 si16 si mi'8. mi'16 mi' si dod' re' |
    dod'8 dod' r la16 la re'8 re'16 la |
    si4 do'8 re'16 mi' fa'8. sol'16 |
    <<
      \tag #'basse {
        mi'16\trill mi' s2.. s2
        \ffclef "vhaute-contre" <>^\markup\character Lychas
      }
      \tag #'lychas { mi'4\trill mi'8 r r2 r2 }
    >> la4 |
    do'4. re'8 mi'4 |
    fa' re'\trill sol' |
    mi'\trill do' sol'8. sol'16 |
    fa'4\trill fa' mi' |
    mi'( re'4.)\trill do'8 |
    do'2. |
    sol'4 sol' mi' |
    fa'2 re'4 |
    re'2 dod'4 |
    re' re' fa' |
    mi'2\trill r8 mi' |
    fa'[ mi' re' do'] si[ la] |
    sold2 si4 |
    do'8 re' si4.\trill la8 |
    <<
      \tag #'basse {
        la8 s8 s2. s2.*3
        \ffclef "vhaute-contre" <>^\markup\character Lychas
      }
      \tag #'lychas { la4 r4 r2 R2.*3 }
    >> r4 si do' |
    re' re' mi' |
    fa' fa' mi' |
    mi'4. fa'8 re'4 |
    mi'2. |
    mi'4 mi' fa' |
    dod'4. re'8 mi'4 |
    fa' mi'2\trill |
    re'8 <<
      \tag #'basse {
        s2.. s1 s2.*2 s4
        \ffclef "vhaute-contre" <>^\markup\character Lychas
      }
      \tag #'lychas { r8 r4 r2 R1 R2.*2 r4 }
    >> mi'8 si do'8. do'16 do'8[ si16] do' |
    si2\trill do'4. sol8 |
    la8. la16 si8 do' do'[ si] |
    do'4 <<
      \tag #'basse {
        s2. s1 s4
        \ffclef "vhaute-contre" <>^\markup\character Lychas
      }
      \tag #'lychas { r4 r2 R1 r4 }
    >> r8 mi' do'\trill do' do'16 do' do' si |
    si4\trill <<
      \tag #'basse {
        s2. s1 s4.
        \ffclef "vhaute-contre" <>^\markup\character Lychas
      }
      \tag #'lychas { r4 r2 R1 r4 r8 }
    >> re'8 la4 r16 fa' fa' fa' |
    re'4.\trill re'8 la la la si |
    do'4 r8 mi' do'\trill do'16 do' la8\trill la16 si |
    sold8 sold r16 si si do' la4 la16 r la sold |
    la2 <<
      \tag #'basse {
        s2 s1*9 s2.*3 s4
        \ffclef "vhaute-contre" <>^\markup\character Lychas
      }
      \tag #'lychas { r2 R1*9 R2.*3 r4 }
    >> r8 mi'16 mi' si8 si16 si |
    sold8 sold r16 mi' mi' mi' la'8. mi'16 |
    fa'4 re'8. do'16 si4\trill si8 do' |
    la2 r |
  }
  %% Straton
  \tag #'(straton basse) {
    \clef "vbasse" <>^\markup\character Straton
    r8 la la4 r8 do16 do do8 re16 mi |
    la,4 la, <<
      \tag #'basse {
        s2 s1 s4
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r2 R1 r4 }
    >> r8 mi mi mi16 fad sold8 sold16 la |
    si4 r8 si sold sold sold8. sold16 |
    la4 r8 la mi4 mi8 fad16 sol |
    fad8\trill fad r re re16 re mi fad |
    sol4 do'8. do'16 do'8 do'16 si |
    si4\trill <<
      \tag #'basse {
        s4 s2.
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r4 r2 r4 }
    >> r8 sol do' do' do' do' |
    la4.\trill la8 re'8. do'16 si8. do'16 |
    sold2. |
    <<
      \tag #'basse {
        s2.*27 s4
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { R2.*27 r4 }
    >> do'4 do'8. do'16 mi8. fad16 |
    sol4 sol8 si mi4 mi8. fad16 |
    red8 <<
      \tag #'basse {
        s2.. s2.*2 s8
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r8 r4 r2 R2.*2 r8 }
    >> do'16 si la8 la16 sol fa8 fa16 re mi8 mi16 mi |
    la,2 <<
      \tag #'basse {
        s4 s2.*14 s8
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r4 R2.*14 r8 }
    >> mi8 mi16 mi la mi fa8 fa16 fa fa8 fa16 mi |
    mi8\trill mi r do'16 si la8 la16 sol |
    fad8. fad16 sol4 sol8 sol16 fad |
    sol2. |
    <<
      \tag #'basse {
        s2.*8 s8
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { R2.*8 r8 }
    >> fad8 fad16 fad fad la re8. re16 re re mi fad |
    sol4 r8 si16 la sol8. sol16 sol8 la16 si |
    mi4 la la16 sol fad mi |
    red8 dod16 si, mi8 mi16 la, si,4 |
    mi, <<
      \tag #'basse {
        s2. s1 s2. s4
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r4 r2 R1 R2. r4 }
    >> r8 do sol sol sol sol |
    do'4 sol8 sol mi4\trill mi8 sol |
    do4 <<
      \tag #'basse {
        s2. s4
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r4 r2 r4 }
    >> re'4 r si16 si si si |
    sol8 r re4 r8 re16 re re8. mi16 |
    fa4 fa8 <<
      \tag #'basse {
        s8 s2 s1*3 s2
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r8 r2 R1*3 r2 }
    >> r4 la,8. si,16 |
    do2 re4 re8 re |
    mi2 mi4 mi |
    la la8 fa sol4 sol, |
    do2 do'8 si la sol |
    fad4. fad8 sol4 sol |
    r8 mi mi mi fa4 fa8 re |
    mi4. mi8 la4 la8 sol |
    fa4.\trill mi8 re4 re |
    r8 re re re mi4 mi8 mi |
    la,4 r8 do'16 do' la8\trill la16 la |
    mi8 mi r do' do'16 si la sol |
    fad8. fad16 fad8 sol red8. mi16 |
    mi4
    \tag #'straton { r4 r R2. R1*2 }
  }
>>