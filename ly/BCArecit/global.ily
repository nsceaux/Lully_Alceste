\key la \minor
\time 4/4 \midiTempo#80 s1*6
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 \midiTempo#120 s2.*28
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#120 s2.*15
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*3 \midiTempo#240 s2.*8
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 4/4 s1*9
\time 2/2 \midiTempo#160 s1*10
\digitTime\time 3/4 \midiTempo#80 s2.*5
\time 4/4 s1
\time 2/2 \midiTempo#160 s1 \bar "|."
