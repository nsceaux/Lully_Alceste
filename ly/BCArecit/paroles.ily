\tag #'(straton basse) {
  Ly -- chas, j’ay deux mots à te di -- re.
}
\tag #'(lychas basse) {
  Que veux- tu ? par -- le ; je t’en -- tends.
}
\tag #'(straton basse) {
  Nous som -- mes a -- mis de tous temps ;
  Cé -- phi -- se, tu le sçais, me tient sous son em -- pi -- re.
  Tu suis par tout ses pas : qu’est- ce que tu pre -- tens ?
}
\tag #'(lychas basse) {
  Je pre -- tens ri -- re.
}
\tag #'(straton basse) {
  Pour -- quoy veux- tu trou -- bler deux cœurs qui sont con -- tents ?
}
\tag #'(lychas basse) {
  Je pre -- tens ri -- re.
  Je pre -- tens ri -- re.
  Je pre -- tens ri -- re.
  Tu peux à ton gré t’en -- fla -- mer ;
  cha -- cun a sa fa -- çon d’ai -- mer ;
  qui vou -- dra soû -- pi -- rer, soû -- pi -- re,
  qui vou -- dra soû -- pi -- rer, soû -- pi -- re,
  je pre -- tens ri -- re.
  Je pre -- tens ri -- re.
  Je pre -- tens ri -- re.
}
\tag #'(straton basse) {
  J’aime, & je suis ai -- mé : laisse en paix nos a -- mours.
}
\tag #'(lychas basse) {
  Rien ne doit t’al -- lar -- mer s’il est bien vray qu’on t’ai -- me ;
  Un ri -- val re -- but -- té donne un plai -- sir ex -- tres -- me.
}
\tag #'(straton basse) {
  Un ri -- val quel qu’il soit im -- por -- tu -- ne toû -- jours.
}
\tag #'(lychas basse) {
  Je voy ton a -- mour sans co -- le -- re,
  tu de -- vrois en u -- ser ain -- si :
  puis -- que Cé -- phi -- se t’a sçeu plai -- re,
  pour -- quoy ne veux- tu pas qu’el -- le me plaise aus -- si ?
}
\tag #'(straton basse) {
  A quoy sert- il d’ai -- mer ce qu’il faut que l’on quit -- te ?
  tu ne peux de -- meu -- rer long- temps dans cet -- te cour.
}
\tag #'(lychas basse) {
  Moins on a de mo -- mens à don -- ner à l’a -- mour,
  et plus il faut qu’on en pro -- fi -- te.
}
\tag #'(straton basse) {
  J’ai -- me de -- puis deux ans a -- vec fi -- de -- li -- té :
  je puis croi -- re, sans va -- ni -- té,
  que tu ne dois pas estre un ri -- val qui m’a -- lar -- me.
}
\tag #'(lychas basse) {
  J’ay pour moy la nou -- veau -- té,
  en a -- mour c’est un grand char -- me.
}
\tag #'(straton basse) {
  Cé -- phi -- se m’a pro -- mis un cœur tendre, & cons -- tant.
}
\tag #'(lychas basse) {
  Cé -- phi -- se m’en pro -- met au -- tant.
}
\tag #'(straton basse) {
  Ah si je le croy -- ois !… Mais tu n’es pas croy -- a -- ble.
}
\tag #'(lychas basse) {
  Croy- moy, fais ton pro -- fit d’un res -- te d’a -- mi -- tié,
  sers- toy d’un a -- vis cha -- ri -- ta -- ble
  que je te don -- ne par pi -- tié.
}
\tag #'(straton basse) {
  Le mes -- pris d’u -- ne vo -- la -- ge
  doit estre un as -- sés grand mal,
  et c’est un nou -- vel ou -- tra -- ge
  que la pi -- tié d’un Ri -- val.
  Et c’est un nou -- vel ou -- tra -- ge
  que la pi -- tié d’un Ri -- val.
  El -- le vient l’in -- fi -- del -- le,
  pour chan -- ter dans les jeux dont je prens soin i -- cy.
}
\tag #'(lychas basse) {
  Je te laisse a -- vec el -- le,
  il ne tien -- dra qu’à toy d’es -- tre mieux é -- clair -- cy.
}
