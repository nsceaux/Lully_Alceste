\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \keepWithTag #'conducteur \includeNotes "dessus1" >>
      \new Staff << \global \keepWithTag #'conducteur \includeNotes "dessus2" >>
    >>
    %% Lycas
    \new Staff \withLyrics <<
      \global \keepWithTag #'lychas \includeNotes "voix"
    >> \keepWithTag #'lychas \includeLyrics "paroles"
    %% Straton
    \new Staff \withLyrics <<
      \global \keepWithTag #'straton \includeNotes "voix"
    >> \keepWithTag #'straton \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\break s1*2\break s1 s2.\break s2. s1*2\break
        s1 s2.*4\break s2.*6\pageBreak
        s2.*6\break s2.*7\break s2.*5\break
        s1*2 s2 \bar "" \break s2 s2.*2\break s1 s2.\pageBreak
        s2.*6\break s2.*7\break s2. s1\break s2.*4\break
        s2.*6\break s2. s1\pageBreak
        s1 s2.*2\break s1*2 s2.\break s1*3\break s1*2\break
        s1*2\break s1*2 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break s2 s1*2 s2 \bar "" \break s2 s1*2 s2.\pageBreak
        s2.*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}