\key la \minor
\time 2/2 \midiTempo#120 s1*11 \alternatives s1 s1
\time 6/4 \midiTempo#180 s1.*21
\time 2/2 \midiTempo#120 \tempo "Lentement" s1*8
\alternatives { \time 6/4 s1. } { \time 2/2 s1 } \bar "|."