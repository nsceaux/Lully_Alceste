\clef "taille" mi'2 mi'4. mi'8 |
re'2. re'4 |
mi'2. mi'4 |
mi'2 re'4. mi'16 fa' |
mi'2 mi'4. fa'16 sol' |
fa'4. fa'8 re'4. re'8 |
do'8 re' mi' fa' mi' fa' sol' la' |
si'2. si'4 |
mi'2 la'~ |
la'4. la'8 sol'4. sol'8 |
do''4. do''8 si'4. si'8 |
si'1 |
si' |
R1. |
r4 r8 mi'8 re'4 do'4. si8 do'4 |
si4. do'8 re'4 dod' dod'4. si16 dod' |
re'2 re'4 la2 mi'4 |
mi' re'4. re'8 mi' fad' sold'4 la' |
si' mi'2 mi' mi'4 |
re' fa'2 sol' sol'4 |
sol'2 si'4 mi' fad' fad'8 mi' |
red'2 red'4 mi'2 mi'4 |
fad'2 fad'4 mi'2 fad'4 |
sol' la'2 si' mi'4 |
mi' re'4. re'8 re'2 mi'8 si |
do'4 mi'2 re' re'4 |
re'2 re'4 mi'4. fa'8 sol'4 |
la'4. la'8 la'4 fa'4. mi'8 fa'4 |
mi'2.~ mi' |
mi'4. mi'8 re'4 do' mi'4. fa'8 |
mi'4 re'4. re'8 re'4 do'4. do'8 |
re'4. mi'8 fa'4 mi'4. fa'8 mi'4 |
re'2 sol'4 la'2 la'4 |
re' mi'4. re'16 mi' fa'4 do' re' |
si r mi'4. mi'8 |
mi'2 re'4. re'8 |
re'2 do'4. do'8 |
do'4. do'8 si4. do'8 |
do'2 mi'4. mi'8 |
re'2 fad'4. fad'8 |
mi'2 mi'4. mi'8 |
mi'4. fa'8 mi'4. mi'8 |
mi'2.~ mi' |
mi'1 |
