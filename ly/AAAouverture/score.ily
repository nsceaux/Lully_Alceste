\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "dessus" >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff <<
      \global \includeNotes "taille"
      \keepWithTag #'taille \includeFigures "chiffres"
    >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \keepWithTag #'basse \includeNotes "basse"
      \keepWithTag #'basse \includeFigures "chiffres"
      \origLayout {
        s1*7\break s1*6 s1.*2\pageBreak
        s1.*6\break s1.*6\pageBreak
        s1.*6\break s1. s1*5\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
