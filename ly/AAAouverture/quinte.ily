\clef "quinte" la2. la4 |
la4. la8 fa4 re |
la2 mi'8. re'16 do'4 |
do'4. la8 la4 re' |
si sol do'4. re'16 mi' |
re'4. re'8 re'4 sol |
sol do' do'4. do'8 |
re'2 re'4. re'8 |
do'4 mi' fad'2~ |
fad'4 red' mi' si |
la4. la8 fad4 si |
si1 |
si |
R1.*3 |
r2 r4 r r8 do' si4 |
la4. la8 re'4 si2 la4 |
mi'2~ mi'4 mi'4. re'8 dod'4 |
re'2.~ re'2 re'4 |
do'4. do'8 re'4 mi' la2 |
si2.~ si |
si4 la2 sol4 si2 |
mi2 la4 fad si2 |
si2. mi'2 mi'4 |
mi' la4. la8 la2 la4 |
si8 do' re'2 do'4. re'8 mi'4 |
fa'4 la2 la4 si4. si8 |
si2. la2 la4 |
si2 si4 la4. do'8 si4 |
la4. sol8 la4 sol2 sol4 |
fa4 re' si do' sol4. la8 |
si2 si4 do'2 la4 |
si2 mi'4 la2 la4 |
si4 r si4. si8 |
la2 la4. la8 |
sol2 sol4. sol8 |
fa4 do' sol4. sol8 |
sol2 sol4. do'8 |
la2 si |
si si4. si8 |
la4 re' si mi' |
dod'2.~ dod' |
dod'1 |
