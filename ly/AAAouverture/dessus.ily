\clef "dessus" la''2. r16 sol'' la'' mi'' |
fa''4. fa''8 fa''4. sol''8 |
mi''2.\trill r8 mi'' |
la'4.*5/6 la'16 si' do'' re''4. re''8 |
re''4. do''16 si' do''4. do''8 |
do''4. re''8 si'4.\trill si'8 |
mi''4 do'' sol''4. sol''8 |
sold''2 sold''4. sold''8 |
la''2 fad''4. sol''8 |
red''4 si' sol''4. sol''8 |
sol''4. fad''8 fad''4. sold''16 la'' |
sold''1 |
sold'' |
r4 r8 mi'' re''4 do''4. si'8 do''4 |
si'2.\trill do''4. re''8 mi''4 |
re''4. mi''8 fa''4 mi''4. re''8 mi''4 |
la'4. sol'8 la' si' do''4. re''8 mi''4 |
fa''8 mi'' fa'' sol'' la'' si'' sold''4 mi'' la''~ |
la'' sol''4.\trill sol''8 sol''2 la''8 mi'' |
fa''2 fa''4 fa''2 sol''8 re'' |
mi''4. mi''8 fad'' sol'' la''4. si''8 la'' sol'' |
fad''2\trill fad''4 sol''4. la''8 si''4 |
la''4.\trill sol''8 fad''4 sol''4. sol''8 fad''4 |
mi''4. red''8 mi'' fad'' red''4. red''8 mi''4~ |
mi''8 fad'' fad''4.\trill mi''8 mi''2.~ |
mi''2 mi''4 la''4. sol''8 fad'' mi'' |
re''2 re''4 sol''4. fa''8 mi'' re'' |
do''4. re''8 mi''4 fa'' re''4.\trill do''8 |
si'2.\trill do''4. re''8 mi'' fa'' |
re''4.\trill do''8 si'4 do''4. re''8 mi''4 |
la'2 re''8 mi'' mi''4.\trill re''8 do''4 |
fa''4. mi''8 re''4 sol'' mi'' la''~ |
la'' sol''4.\trill sol''8 sol''4 fa''4.\trill fa''8 |
fa''4 mi''4.\trill mi''8 mi'' fa'' re''4.\trill do''16 re'' |
mi''4 r sol''4. sol''8 |
sol''4 r16 sol'' fa'' mi'' fa''4. fa''8 |
fa''4 r16 fa'' mi'' re'' mi''4. fa''16 sol'' |
fa''4.\trill mi''8 re''4.\trill do''8 |
do''2 r8 mi'' fad'' sol'' |
fad''2 r8 fad'' sold'' la'' |
sold''8. fad''16 mi''4 re''4.\trill do''16 si' |
do''4. si'8 si'4.\trill la'8 |
la'2.~ la' |
la'1 |
