\clef "haute-contre" do''2 dod''4. dod''8 |
re''4. re''8 re''2 |
re''8. re''16 do''8. si'16 do''8. si'16 la'8. sol'16 |
fa'2. r16 la' sol' fa' |
sol'4. sol'8 sol'4~ sol'16 sol' fa' mi' |
fa'4 la' sol'4. sol'8 |
sol'4 sol' do''8 re'' mi''4 |
re''2~ re''8. si'16 si'8. si'16 |
do''2. do''4 |
si'2 si'4 mi'' |
mi''2 red''4.( dod''16 red'') |
mi''1 |
mi'' |
r4 r8 do'' si'4 la'4. sold'8 la'4 |
sold'2. la'4. la'8 la'4~ |
la'4 sol'4. sol'8 sol'4. la'8 sol'4 |
fa'4. mi'8 fa'4 mi'4. fa'8 sol'4 |
la' re''4. re''8 re''4. mi''8 do''4 |
si'4 si'4. la'8 la'2. |
la'2 la'4 sol'4. la'8 si'4 |
do''4. mi''8 re''4 do''4. si'8 do''4 |
si'2 si'4 si'4. la'8 sol'4 |
fad' si'2 si' si'4 |
si' la'2 la'4. fad'8 sol' mi' |
si'2. si'4. la'8 sold'4 |
la'4 do'' do'' do''4. si'8 do''4 |
si'2 si'4 si' sib'2 |
la'4. si'8 do''4 re'' si'4. la'8 |
sold'2. la'4. si'8 do'' re'' |
si'2 si'4 mi'2 mi'4 |
fa'2 fa'4 sol'2 do''4 |
do'' si'4. si'8 si'2 la'4 |
re''4. mi''8 re''4 do''4. re''8 do''4 |
si'4. do''8 si'4 la'4. si'8 la'4 |
sold'4 r si' mi''8 re'' |
dod''4. dod''8 re''4. dod''8 |
si'4. si'8 do''4. sol'8 |
la'2 sol'4. fa'8 |
mi'2 do''~ |
do'' si'2~ |
si'4 sold' sold'4. sold'8 |
la'4. si'8 sold'4. la'8 |
la'2.~ la' |
la'1 |
