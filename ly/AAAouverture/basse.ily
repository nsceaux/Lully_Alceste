\clef "basse" la,1~ |
la, |
la2. r16 sol la mi |
fa4. fa8 fa4 r16 fa mi re |
mi4. mi8 mi4 r16 mi re do |
re4. re8 sol4 r16 sol fa sol |
do2 do4. do8 |
si,2~ si,4. si,8 |
la,2~ la,4.*5/6 la16 sol fad |
si4.*5/6 si16 la si mi4.*5/6 mi16 re mi |
la,4. la,8 si,4.*5/6 si,16 la, si, |
mi,2 mi4.*5/6 mi16 re mi |
mi,1 |
<<
  \tag #'basse { R1.*3 | r2 r4 r4 r8 }
  \tag #'basse-continue {
    \clef "haute-contre" r4 r8 do'' si'4 la'4. sold'8 la'4 |
    sold'4. \fclef "taille" mi'8 re'4 do'4. si8 do'4 |
    si4. do'8 re'4 dod' dod'4. si16 dod' |
    re'2 re'4 la4. \clef "basse"
  }
>> la8 sol4 |
fa4. mi8 fa4 mi2.~ |
mi4. mi'8 re'4 dod'4. si8 la4 |
re'4. re'8 do'4 si4. la8 sol4 |
do'4. do'8 si4 la4. sol8 fad4 |
si4. si8 la4 sol4. fad8 mi4 |
red2. mi4. mi8 re?4 |
do4. si,8 do4 si,2.~ |
si,4. si8 la4 sold4. fad8 mi4 |
la4. la8 sol4 fad4. mi8 re4 |
sol4. sol8 fa4 mi4. re8 do4 |
fa4. fa8 mi4 re4. do8 si,4 |
mi4. mi8 re4 do4. si,8 la,4 |
sold,2. la,4. la8 sol4 |
fa4. mi8 fa4 mi4. fa8 mi4 |
re4. do8 re4 do4. re8 do4 |
si,4. do8 si,4 la,4. sol,8 la,4 |
sol,4. la,8 sol,4 fa,4. mi,8 fa,4 |
mi,4 r mi4. mi8 |
la2 re4. re8 |
sol2 do4. do8 |
fa,2 sol, |
do, do |
re red |
mi1 |
la,4 re, mi,2 |
la,2.~ la, |
la,1 |
