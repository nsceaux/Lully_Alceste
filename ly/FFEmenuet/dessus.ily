\clef "dessus" sol''4. fad''8 sol''4 |
re'' do''2\trill |
sib'\trill la'4 |
sib'4 do''4.(\trill sib'16 do'') |
re''2 re''4 |
sol''4. fad''8 sol''4 |
re'' do''2\trill |
sib'\trill la'4 |
sib'8 do'' do''4.(\trill sib'16 do'') |
re''2. |
re'' |
fa''4. sol''8 fa''4 |
re'' mib''2 |
do''\trill re''4 |
sib' do''2 |
la'4.\trill sol'8 fa'4 |
fa''4. sol''8 fa''4 |
re'' mib''2 |
do''4\trill fa'' sib' |
do'' do''4.\trill sib'8 |
sib'2. |
re''4. mi''8 fa''4 |
mi'' la''2 |
fad'' sol''8\trill fad''16 sol'' |
la''2 sib''4 |
la''2\trill re''4 |
sol''4. la''8 fa''4 |
mi''4 la''2 |
fad''2 sol''4~ |
sol''8 la'' la''4.\trill sol''8 |
sol''2. |
