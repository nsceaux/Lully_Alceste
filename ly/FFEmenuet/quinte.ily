\clef "quinte" re'2 re'4 |
re' re'2 |
re' do'4 |
sib sol2 |
la2 la4 |
sib4. do'8 re'4 |
re' la2 |
sib4 re' re' |
re'4 do'2 |
la4 re'4. re'8 |
la4 re'2 |
re' do'4 |
re' sol2 |
do' sib4 |
sib mib2 |
fa do'4 |
re'2 do'4 |
re' sol2 |
la4 fa sol |
sol fa4. fa8 |
fa2 re4 |
re'2 re'4 |
re'2 sol4 |
la2 sol4 |
la2 sol4 |
la2 la4 |
sol2 re'4 |
do'4 do'2 |
do'2 sib4~ |
sib8 la la4 re'8 do' |
si2. |
