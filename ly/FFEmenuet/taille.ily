\clef "taille" sol'4 re' re' |
la' la'2 |
re'2 re'4 |
re' do'2 |
la re'4 |
re'2 re'4 |
re' re'2 |
re'4. sol'8 la'4 |
sol'4 sol'2 |
la'4. la'8 sol' fad' |
la'2. |
sib'4 fa' fa' |
fa' sol'2 |
fa' fa'4 |
mib' mib'2 |
do'4. sib8 la4 |
fa'2 fa'4 |
fa' sol'2 |
fa'4 fa' re' |
mib' do' fa' |
re'2. |
re'4 sol'2 |
sol' la'4 |
la'2 sol'4 |
re' la' sol' |
fad'2 fad'4 |
sol'2 re'4 |
mi'4 mi'2 |
re' re'4~ |
re'4 re'4. re'8 |
re'2. |
