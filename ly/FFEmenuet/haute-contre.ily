\clef "haute-contre" sib'4. do''8 sib'4 |
la' la'2 |
sol' la'4 |
sol' sol'2 |
fad'2 fad'4 |
sol'4. la'8 sib'4 |
la' la'2 |
sol' fad'4 |
sol'4 sol'2 |
fad'4. fad'8 sol' la' |
fad'2. |
fa'!4 sib' do'' |
sib' sib'2 |
la' sib'4 |
sol' sol'2 |
fa' fa'4 |
la'4. sib'8 do''4 |
sib' sib'2 |
la'4 la' sib' |
sib' la'4. sib'8 |
sib'2. |
sol'2 sol'4 |
sol'4 do''2 |
do''2 sib'4 |
la'2 re''4 |
re''2 re''4 |
re''2 re''4 |
sol'4 do''2 |
la' sib'4~ |
sib' fad'?4. sol'8 |
sol'2. |
