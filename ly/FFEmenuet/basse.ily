\clef "basse" sol4. la8 sib4 |
fad2. |
sol2 fad4 |
sol mib2 |
re2 do4 |
sib,4. la,8 sol,4 |
fad,2. |
sol,2 re4 |
sol4 mib2 |
re4 re'8 do' sib la |
re2 re'8 do' |
sib2 la4 |
sib mib2 |
fa sib,4 |
mib do2 |
fa fa4 |
re2 la4 |
sib mib2 |
fa4 re sol |
mib fa fa, |
sib,2 sib4 |
si2. |
do'4 la2 |
re' sol4 |
fad2 sol4 |
re2 re8 do |
si,2. |
do4 la,2 |
re sib,4 |
sol, re,2 |
sol,2. |
