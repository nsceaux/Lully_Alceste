\clef "basse" la,2 si,4 |
do dod2 |
re2. |
mi2 mi4 |
la,8 sol, fa,2 |
mi,4 mi2 |
la,2. |
si, |
do |
fa2 re4 |
mi2 do4 |
re2. |
mi~ |
mi8 re mi4 mi, |
la,2 mi4 |
la si do' |
sold2. |
la2 la,4 |
mi2 mi8 fad |
sold2. |
la |
si2 mi4~ |
mi si,2 |
mi2 do4 |
re2. |
mi~ |
mi8 re mi4 mi, |
la,2 la,8 si, |
dod2. |
re2 re8 mi |
fad2. |
sol2 sol8 fa |
mi2. |
do4 re mi |
fa2. |
mi8 fa sol4 sol, |
do2 do4 |
re2. |
mi2 mi4 |
la,8 sol, fa,2 |
mi,4 mi2 |
la,2 la,4 |
si,2. |
do |
fa2 re4 |
mi2 do4 |
re2. |
mi~ |
mi8 re mi4 mi, |
la,2 si,4 |
