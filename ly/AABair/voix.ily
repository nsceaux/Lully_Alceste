\clef "vbas-dessus" <>^\markup\character La Nymphe de la Seine R2. |
r4 r mi''8. mi''16 |
fa''4 re''4.\trill do''8 |
si'2\trill r8 si' |
do'' do'' do''4. re''8 |
mi''2 r8 si' |
si'4( do'') la'8. sol'16 |
sol'4 fa'( mi'8) fa' |
mi'4\trill mi' r8 do'' |
la'8.\trill la'16 la'8. si'16 si'8.\trill la'16 |
sold'4\trill sold' mi''8. mi''16 |
fa''4 re''4.\trill do''8 |
si'2\trill r8 si' |
sold' la' la'4. sold'8 |
la'2. |
r8 do'' re''4 mi'' |
re''2\trill si'4 |
do''4. si'8[ la' si'] |
si'2\trill si'4 |
r4 r8 si' mi''8. si'16 |
do''2 dod''4 |
red'' r8 si' do''[ si'] |
la'[ sol'] fad'4.\trill mi'8 |
mi'2 mi''8. mi''16 |
fa''4 re''4.\trill do''8 |
si'2\trill r8 si' |
sold'8 la' la'4. sold'8 |
la'2. |
la'4. mi'8 la'4 |
fad'2 fad'4 |
r8 re'' re''4( do''8)\trill[ si'16] do'' |
si'2\trill si'4 |
r do''4. sol'8 |
sib'4 sib'( la'8) sib'! |
la'2 la'8 si' |
do''2 do''8 si' |
do''2 mi''8. mi''16 |
fa''4 re''4.\trill do''8 |
si'2\trill r8 si' |
do'' do'' do''4. re''8 |
mi''2 r8 si' |
si'4( do'') la'8. sol'16 |
sol'4 fa'( mi'8) fa' |
mi'4\trill mi' r8 do'' |
la'8.\trill la'16 la'8. si'16 si'8.\trill la'16 |
sold'4\trill sold' mi''8. mi''16 |
fa''4 re''4.\trill do''8 |
si'2\trill r8 si' |
sold' la' la'4. sold'8 |
la'2 r4 |
