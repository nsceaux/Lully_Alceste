Le he -- ros que j’at -- tens ne re -- vien -- dra- t’il pas ?
Se -- ray- je toû -- jours lan -- guis -- san -- te
dans u -- ne si cru -- elle at -- ten -- te ?
Le he -- ros que j’at -- tens ne re -- vien -- dra- t’il pas ?
on n’en -- tend plus d’oy -- seau qui chan -- te,
on ne voit plus de fleurs qui nais -- sent sous nos pas.
Le he -- ros que j’at -- tens ne re -- vien -- dra- t’il pas ?
L’her -- be nais -- san -- te
pa -- roist __ mou -- ran -- te,
tout lan -- guit a -- vec moy dans ces lieux pleins d’ap -- pas.
Le he -- ros que j’at -- tens ne re -- vien -- dra- t’il pas ?
Se -- ray- je toû -- jours lan -- guis -- san -- te
dans u -- ne si cru -- elle at -- ten -- te ?
Le he -- ros que j’at -- tens ne re -- vien -- dra- t’il pas ?
