\score {
  \new ChoirStaff <<
    \new Staff \withRecit <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*4\break s2.*4\break s2.*4\break s2.*5\break s2.*5\break s2.*4\pageBreak
        s2.*5\break s2.*5\break s2.*5\break s2.*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}