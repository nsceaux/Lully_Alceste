\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:system-count 4)
   (basse-continue
    #:music , #{ s4 s2.*8\break s2.*8\break s2.*9\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#42 #}))
