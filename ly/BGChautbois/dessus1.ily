\clef "dessus" <>^"Hautbois" sol''4 |
la''4. sol''8 fa''4 |
mi''\trill mi'' fa'' |
sol''4. fa''8 mi''4 |
re''\trill re'' mi'' |
fa''4. sol''8 fa''4 |
mi''\trill fa'' sol'' |
la'' re''4.\trill do''8 |
do''2 mi''4 |
re''4.\trill mi''8 do''4 |
si'4\trill si' mi'' |
re''4.\trill do''8 si'4 |
do''2 la''4 |
sol''4.\trill fa''8 mi''4 |
fa'' fa'' fa'' |
fad''4. mi''8 fad''4 |
sol''2 re''4 |
re'' do''4.\trill sib'8 |
la'4 la' re'' |
do''8 sib' la'4 si'8 do'' |
si'!2 re''4 |
mi''4. re''8 do''4 |
re'' re'' sol'' |
do''4. re''8 mi''4 |
re''\trill re'' sol'' |
fa''4.\trill mi''8 re'' mi'' |
mi''2\trill sol''4 |
la''4. sol''8 fa''4 |
mi''\trill mi'' fa'' |
sol''4. fa''8 mi''4 |
re''\trill re'' mi'' |
fa''4. sol''8 fa''4 |
mi''\trill fa'' sol'' |
la'' re''4.\trill do''8 |
do''2
