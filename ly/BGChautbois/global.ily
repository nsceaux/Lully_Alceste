\key do \major \midiTempo#160
\digitTime\time 3/4 \partial 4 s4 s2.*7 s2 \bar ":|."
s4 s2.*25 s2 \bar "|."
\endMark\markup { On dit les \concat { 2 \super es } paroles }
