\clef "dessus" mi''4 |
fa''4. mi''8 re''4 |
do'' do'' re'' |
mi''4. re''8 do''4 |
si' si' do'' |
re''4. mi''8 re''4 |
sol' la' si' |
do'' si'4.\trill do''8 |
do''2 do''4 |
si'4. do''8 la'4 |
sold' sold' do'' |
si'4.\trill la'8 sold'4 |
la'2 do''4 |
dod''4. si'8 dod''4 |
re'' re'' re'' |
do''4. sib'8 la'4 |
sib'2 sib'4 |
sib' la'4.\trill sol'8 |
fad'4 fad' sib' |
la'8 sol' fad'4 sol'8 la' |
sol'2 si'4 |
do''4. si'8 la'4 |
si' si' mi'' |
la'4. si'8 do''4 |
si' si' mi'' |
re''4.\trill do''8 si'4 |
do''2 mi''4 |
fa''4. mi''8 re''4 |
do'' do'' re'' |
mi''4. re''8 do''4 |
si' si' do'' |
re''4. mi''8 re''4 |
sol' la' si' |
do'' si'4.\trill do''8 |
do''2
