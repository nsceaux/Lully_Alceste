La Lu -- miere au -- jour -- d’hui te doit es -- tre ra -- vi -- e ;
il n’est qu’un seul moy -- en de pro -- lon -- ger ton sort ;
le des -- tin me pro -- met de te rendre à la vi -- e,
si quel -- qu’au -- tre pour toy veut s’of -- frir à la mort.
Re -- con -- noist si quel -- qu’un t’ai -- me par -- fai -- te -- ment ;
sa mort au -- ra pour prix une im -- mor -- tel -- le gloi -- re :
pour en con -- ser -- ver la me -- moi -- re
les arts vont é -- le -- ver un pom -- peux mo -- nu -- ment.
