\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1 s2.*2\break
        s2.*3\break
        s2.*2\break
        s2. s1\break
        s2.*2\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
