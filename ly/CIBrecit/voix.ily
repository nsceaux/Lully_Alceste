\clef "vhaute-contre" <>^\markup\character Apollon
r8 do'16 do' sol'8 sol'16 sol' re'8\trill re'16 mi' fa'8 fa'16 sol' |
mi'8\trill mi' r do' do'16 do' re' mi' |
fa'8. fa'16 fa'8. la'16 re'8.\trill re'16 |
re'4 r8 sol16 la si8\trill si16 dod' |
re'8. re'16 re'8 re' re' dod' |
re' re' r re'16 re' re'8 mi'16 fad' |
sol'4. re'16 mi' do'8\trill si16 do' |
si4\trill r8 sol'16 sol' re'8\trill re'16 mi' |
fa'4 fa'8. fa'16 fa'8 fa'16 mi' |
mi'4\trill r8 mi' re'8. re'16 re'8 do'16[ si] |
do'4 do'8 do'16 do' do'8. si16 |
si8\trill si sol16 sol la si do'8 do'16 re' |
mi'8 mi' r sol' mi'8.\trill mi'16 fa'8 sol' |
la'4 fa'8. la'16 re'8\trill re'16 mi' |
do'2.
