\set Score.currentBarNumber = 13 \bar ""
\key do \major
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.*8
\time 4/4 s1
\digitTime\time 3/4 s2.*2
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 2/2 s2. \bar "|."
