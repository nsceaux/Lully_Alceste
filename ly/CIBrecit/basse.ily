\clef "basse" do4. do'8 do'4 si |
do'2~ do'16 sib la sol |
fa2 fad4 |
sol2. |
fa4 mi2 |
re2. |
si,2 la,4 |
sol,2 sol8 fa16 mi |
re2. |
la4. la8 la4 sold |
la4 fad2 |
sol4. fa8 mi8. re16 |
do2 do'8 sib la sol |
fa2 sol8 sol, |
do2.
