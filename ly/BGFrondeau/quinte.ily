\clef "quinte" la4 |
mi' mi'8 re' do'4 |
si2 sol4 |
sol do' do' |
la2 la4 |
la2 mi'4 |
la fa'2 |
mi' mi'4~ |
mi' mi'4. mi'8 |
mi'2 mi'4 |
do'2 do'4 |
si do' la |
sol2 sol4 |
la4. la8 do'4 |
do'4. fa8 fa4 |
sol2 la4 |
si2 mi'8 re' |
do'4. si8 la4~ |
la si2 |
si mi'4 |
mi' mi'8 re' do'4 |
si2 sol4 |
sol do' do' |
la2 la4 |
la2 mi'4 |
la fa'2 |
mi' mi'4~ |
mi' mi'4. mi'8 |
mi'2 mi'4 |
mi' mi' mi |
sol4. re8 la4 |
la4. re'8 re'4 |
mi'4. re'8 do'4 |
re'2 re'4 |
do' mi' re' |
re' do'2 |
re'4. sol8 sol4 |
la2 la4 |
si la2 |
mi si4 |
mi' mi'8 re' do'4 |
si2 sol4 |
sol do' do' |
la2 la4 |
la2 mi'4 |
la fa'2 |
mi' mi'4~ |
mi' mi'4. mi'8 |
mi'2. |
