\clef "haute-contre" la'4 |
la'2 la'4 |
la' sol'4. sol'8 |
sol'2 do''8 re'' |
mi''2 la'4 |
la'2 sold'4 |
la'2 si'4 |
sold'4. sold'8 la'4~ |
la'8 si' sold'4. la'8 |
la'2 do''4 |
do''2 do''8 re'' |
mi''4 fa''8 mi'' re'' do'' |
si'4. si'8 do''4 |
re'' mi''8 re'' do'' si' |
la'4. la'8 si'4 |
do''4. si'8 do'' re'' |
si'2 si'4 |
la'8 si' do''2 |
re''8 do'' si'4.\trill la'8 |
sold'2 la'4 |
la'2 la'4 |
la' sol'4. sol'8 |
sol'2 do''8 re'' |
mi''2 la'4 |
la'2 sold'4 |
la'2 si'4 |
sold'4. sold'8 la'4~ |
la'8 si' sold'4. la'8 |
la'2 mi'8 fad' |
sol'4. sol'8 la'4 |
si' do''8 si' la' sol' |
fad'4. fad'8 sol'4 |
la' do''2 |
si'4. do''8 re''4 |
sol'4. sol'8 la'4 |
si'4 do''4. re''8 |
si'2 si'8 dod'' |
re''4 la'8 si' do''4 |
si' do'' re'' |
mi''2 sold'4 |
la'2 la'4 |
la' sol'4. sol'8 |
sol'2 do''8 re'' |
mi''2 la'4 |
la'2 sold'4 |
la'2 si'4 |
sold'4. sold'8 la'4~ |
la'8 si' sold'4. la'8 |
la'2. |
