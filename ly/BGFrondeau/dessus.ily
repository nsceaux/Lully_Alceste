\clef "dessus" mi''4 |
do'' la' fa'' |
fa''4. mi''8 re''8. mi''16 |
mi''4\trill do'' sol'' |
sol'' la''8 sol'' fa''\trill mi'' |
fa''4 fa'' mi'' |
mi'' re''4.\trill re''8 |
re''4 do''8 si' do''4~ |
do''8 re'' si'4.\trill la'8 |
la'2 la''4 |
mi''4.\trill mi''8 fa''\trill mi''16 fa'' |
sol''4 la''8 sol'' fa'' mi'' |
re''4.\trill re''8 mi''4 |
fa'' sol''8 fa'' mi'' re'' |
do''4.\trill do''8 re''4 |
mi''4.\trill re''8 mi'' fa'' |
re''4\trill mi''8 re'' do'' si' |
do''4. re''8 mi''4 |
fa''8 mi'' re''4.\trill do''8 |
si'2\trill mi''4 |
do'' la' fa'' |
fa''4. mi''8 re''8. mi''16 |
mi''4\trill do'' sol'' |
sol'' la''8 sol'' fa'' mi'' |
fa''4 fa'' mi'' |
mi'' re''4.\trill re''8 |
re''4 do''8 si' do''4~ |
do''8 re'' si'4.\trill la'8 |
la'2 do''4 |
si'4.\trill si'8 do''\trill si'16 do'' |
re''4 mi''8 re'' do'' si' |
la'4.\trill la'8 si'\trill la'16 si' |
do''4. re''8 mi'' fa'' |
re''4.\trill mi''8 fa''4 |
mi''4.\trill mi''8 fad''4 |
sol'' fad''4.\trill( mi''16 fad'') |
sol''4 re''8 dod'' re'' mi'' |
fa''4. sol''8 mi''4\trill |
mi'' la''4. si''8 |
sold''2\trill mi''4 |
do''4 la' fa'' |
fa''4. mi''8 re''8. mi''16 |
mi''4\trill do'' sol'' |
sol'' la''8 sol'' fa'' mi'' |
fa''4 fa'' mi'' |
mi'' re''4.\trill re''8 |
re''4 do''8 si' do''4~ |
do''8 re'' si'4.\trill la'8 |
la'2. |
