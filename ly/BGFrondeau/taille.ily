\clef "taille" do'4 |
do'2 do'4 |
re'2 re'4 |
do' mi' mi' |
mi'2 mi'4 |
re'2 re'4 |
do' re'4. re'8 |
si2 la4 |
la si mi' |
mi'2 mi'4 |
mi' la'4. la'8 |
sol'4 do' re' |
re'2 la'8 sol' |
fa'4. fa'8 sol'4 |
la' si'8 la' sol' fa' |
mi' fa' sol'4 mi' |
mi'2 mi'4 |
mi' la'2 |
mi'4 fa'4. fa'8 |
mi'2 do'4 |
do'2 do'4 |
re'2 re'4 |
do' mi' mi' |
mi'2 mi'4 |
re'2 re'4 |
do' re'4. re'8 |
si2 la4 |
la si mi' |
mi'2 la'4 |
sol'8 fa' mi'4. mi'8 |
re'4. re'8 mi'4 |
re'4. re'8 mi' re' |
do'4 mi' la' |
la' sol'4. sol'8 |
sol'4 do'' la' |
sol' la'4. la'8 |
re'2 la'8 sol' |
fa' mi' re'4 mi' |
mi' mi' re' |
mi'2 mi'4 |
do'2 do'4 |
re'2 re'4 |
do' mi' mi' |
mi'2 mi'4 |
re'2 re'4 |
do' re'4. re'8 |
si2 la4 |
la si mi' |
mi'2. |
