\clef "basse" la,4 |
la,2. |
si, |
do |
dod |
re2 mi4 |
fa2 re4 |
mi2 la,4~ |
la, mi,2 |
la, la,4 |
la2 la4 |
mi4. mi8 fa4 |
sol la8 sol fa mi |
re4. re8 mi4 |
fa sol8 fa mi re |
do2 do4 |
sold,2. |
la,4. si,8 do4~ |
do re2 |
mi4 mi,2 |
la,2. |
si, |
do |
dod |
re2 mi4 |
fa2 re4 |
mi2 la,4~ |
la, mi,2 |
la,2 la,4 |
mi4. re8 do4 |
si,4. si,8 do4 |
re mi8 re do si, |
la,4 la2 |
si si4 |
do'2 do'4 |
si la2\trill |
sol4 la8 sol fa mi |
re8. mi16 fa8 re la4 |
sol4 fa2\trill |
mi4 mi,2 |
la,2. |
si, |
do |
dod |
re2 mi4 |
fa2 re4 |
mi2 la,4~ |
la, mi,2 |
la,2. |
