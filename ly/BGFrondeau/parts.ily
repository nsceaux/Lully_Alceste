\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse #:system-count 5)
   (basse-continue
    #:music , #{ s4 s2.*9\break s2.*9\break s2.*10\break s2.*10\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#48 #}))
