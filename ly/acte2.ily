\bookpart {
  %% 2-1
  \act "Acte Second"
  \sceneDescription\markup\wordwrap-center {
    Le Théatre Represente la principale Ville de l’ile de Scyros.
  }
  \scene "Scene Premiere" "Scene I"
  \sceneDescription\markup\wordwrap-center {
    Céphise, Straton
  }
  \pieceToc\markup\wordwrap {
    Céphise, Straton : \italic { Alceste ne vient point, & nous devons l’attendre }
  }
  \includeScore "CAArecit"
}
\bookpart {
  %% 2-2
  \scene "Scene Deuxiême" "Scene II"
  \sceneDescription\markup\wordwrap-center {
    Licomede, Alceste, et Straton
  }
  \pieceToc\markup\wordwrap {
    Licomede, Alceste, Straton : \italic { Allons, allons, la plainte est vaine }
  }
  \includeScore "CBArecit"
  
  %% 2-3
  \scene "Scene Troisiéme" "Scene III"
  \sceneDescription\markup\wordwrap-center {
    Admete et Alcide
  }
  \pieceToc "Marche en rondeau"
  \includeScore "CCAmarche"
  %% 2-4
  \pieceToc\markup\wordwrap {
    Admete & Alcide : \italic { Marchez, marchez, marchez }
  }
  \includeScore "CCBair"
}
\bookpart {
  \paper { min-systems-per-page = 2 }
  %% 2-5
  \scene "Scene Quatriême" "Scene IV"
  \sceneDescription\markup\wordwrap-center {
    Licomede, Straton, Admete, Alcide, Lychas
  }
  \pieceToc\markup\wordwrap {
    Licomede, Admete, Alcide, chœur : \italic { Marchez, marchez, marchez }
  }
  \includeScore "CDAchoeur"
}
\bookpart {
  %% 2-6
  \pieceToc "Entrée"
  \includeScore "CDBentree"
}
\bookpart {
  %% 2-7
  \pieceToc\markup\wordwrap {
    Chœur, Lychas, Straton : \italic { Achevons d’emporter la Place }
  }
  \includeScore "CDCchoeur"
}
\bookpart {
  \scene "Scene Cinquiême" "Scene V"
  \sceneDescription\markup\wordwrap-center { Pheres }
  %% 2-8
  \pieceToc\markup\wordwrap {
    Pheres : \italic { Courage Enfants, je suis à vous }
  }
  \includeScore "CEApheres"
}
\bookpart {
  \paper { page-count = 2 }
  \scene "Scene Sixiême" "Scene VI"
  \sceneDescription\markup\wordwrap-center {
    Alcide, Pheres, Et Alceste
  }
  %% 2-9
  \pieceToc\markup\wordwrap {
    Alcide, Pheres, Alceste :
    \italic { Rendez à vostre Fils cette aimable Princesse }
  }
  \includeScore "CFAritournelle"
  \includeScore "CFBrecit"
}
\bookpart {
  \scene "Scene Septiême" "Scene VII"
  \sceneDescription\markup\wordwrap-center {
    Alceste, Pheres, Céphise
  }
  %% 2-10
  \pieceToc\markup\wordwrap {
    Alceste, Céphise, Pheres :
    \italic { Cherchons Admete promptement }
  }
  \includeScore "CGAtrio"
  
  \scene "Scene Huitiême" "Scene VIII"
  \sceneDescription\markup\wordwrap-center {
    Admete blessé, Cleante, Alceste, Pheres, Céphise, Soldats
  }
  %% 2-11
  \pieceToc\markup\wordwrap {
    Alceste, Cleante, Admete :
    \italic { O Dieux ! quel spectacle funeste }
  }
  \includeScore "CHArecit"
  
  \scene "Scene Neuviéme" "Scene IX"
  \sceneDescription\markup\wordwrap-center {
    Apollon, les Arts, Admete, Alceste, Pheres, Céphise,
    Celante, soldats
  }
  %% 2-12
  \pieceToc\markup\wordwrap {
    Apollon : \italic { La Lumiere aujourd’hui te doit estre ravie }
  }
  \includeScore "CIAritournelle"
  \includeScore "CIBrecit"
}
\bookpart {
  %% 2-13
  \pieceToc "Entr’acte"
  \reIncludeScore "CCAmarche" "CICmarche"
  \actEnd "Fin du Second Acte"
}
