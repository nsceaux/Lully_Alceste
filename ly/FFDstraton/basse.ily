\clef "basse" sib2 mib |
sib4. sib,8 sib,4. do8 |
re4. re8 re4. mib8 |
fa2 sib, |
sib mib |
sib4. sib,8 sib,4. do8 |
re4. re8 re4. mib8 |
fa1 |
fa4. sol8 fa4. mib8 |
re4. do8 re4. sib,8 |
mib4. re8 mib4. do8 |
re2 sol, |
sol4. la8 sol4. fa8 |
mi4. re8 do4. sib,8 |
la,4. sol,8 la,4 fa, |
do2 fa, |
fa mi |
fa4. sol8 la4 fa |
sib4. lab8 sol4. fa8 |
mib4. re8 do4. sib,8 |
fa2 sib, |
