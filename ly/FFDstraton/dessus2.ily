\clef "dessus" <>^"Flûtes" re''2 mib'' |
re''4. re''8 re''4. mib''8 |
fa''4. mib''8 re''4. do''16 sib' |
la'4.(\trill sol'16 la') sib'2 |
re'' mib'' |
re''4. re''8 re''4. mib''8 |
fa''4. mib''8 re''4. do''16 sib' |
la'2.\trill la'8 la' |
la'4. la'8 sib'4. do''8 |
re''4. mib''8 re''4. do''8 |
sib'4. do''8 sib'4. do''8 |
la'2\trill sol' |
si'2. si'4 |
do''4. re''8 mi''4. mi''8 |
fa''4. mi''8 fa''4. sol''8 |
mi''2\trill fa'' |
la' sol' |
la'4. sib'8 do''4 la' |
sib'4. re''8 mib''4. re''8 |
mib''4. sib'8 la'4. sib'8 |
la'2\trill sib' |
