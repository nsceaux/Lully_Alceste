A quoy bon
tant de rai -- son
dans le bel â -- ge ?
A quoy bon
tant de rai -- son
hors de sai -- son ?
Qui craint le dan -- ger
de s’en -- ga -- ger
est sans cou -- ra -- ge :
tout rit aux a -- mants.
Les jeux char -- mants
sont leur par -- ta -- ge :
tost, tost, tost, soy -- ons con -- tents,
il vient un temps
qu’on est trop sa -- ge.
