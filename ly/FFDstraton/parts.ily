\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:system-count 4)
   (basse-continue #:system-count 4 #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#26 #}))
