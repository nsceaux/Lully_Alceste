\clef "dessus" <>^"Flûtes" fa''2 sol'' |
fa''4.\trill fa''8 fa''4. mib''8 |
re''4. mib''8 fa''4. mib''16 re'' |
do''4.\trill( sib'16 do'') re''2 |
fa'' sol'' |
fa''4.\trill fa''8 fa''4. mib''8 |
re''4. mib''8 fa''4. mib''16 re'' |
do''2.\trill fa''8 fa'' |
do''4. do''8 re''4. mib''8 |
fa''4. fa''8 sib''4. la''8 |
sol''4. fad''8 sol''4. la''8 |
fad''2 sol'' |
re''2. re''4 |
mi''4. fa''8 sol''4. sol''8 |
la''4. sib''8 la''4. sib''8 |
sol''2\trill fa'' |
do'' do'' |
do''4. do''8 fa''4. mib''8 |
re''4. fa''8 sib''4. lab''8 |
sol''4. fa''8 mib''4. re''8 |
do''2\trill sib' |
