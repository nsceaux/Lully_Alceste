\score {
  \new ChoirStaff <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*5\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
