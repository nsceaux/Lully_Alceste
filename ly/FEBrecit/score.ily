\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s1*3\break s2.*3\break s2.*5\break
        s2.*5\break s2.*5\pageBreak
        s2.*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
