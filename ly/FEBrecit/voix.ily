\clef "vhaute-contre" <>^\markup\character Apollon
r4 r8 sol' re'\trill re' re'8. mi'16 |
fa'4. fa'8 fa'8 mib' mib'8. fa'16 |
re'4\trill re' r8 sib16 do' re'8\trill re'16 mi' |
fa'4. fa'8 fa'8. mi'16 mi'8.\trill fa'16 |
fa'1 |
do'4 do'8 do' do'8. re'16 |
mib'8. fa'16 sol'4 sol' |
re'8. mib'16 fa'4 mib'8.\trill re'16 |
do'2\trill do'8 do' |
do'2 re'8 la |
sib4 sol r8 sol' |
sol'2 fa'4 |
mi'4.\trill mi'8 mi' fa' |
re'2 r8 re' |
sol'4. re'8 mib'[ re'] |
do'[ sib] la4.\trill sol8 |
sol2 re'4 |
re'( do')\trill sib8[ la] |
sib4. do'8 re' mi' |
fa'2 fa'4 |
r do'4. re'8 |
mib'2 re'4~ |
re'8 re' sol'4. la'8 |
fad'2 la4 |
sib2 sib4 |
do'4. do'8 re' mib' |
re'4\trill re' re'8. sol'16 |
mi'2 fad'4 |
sol' la'4.\trill sol'8 |
sol'2. |
