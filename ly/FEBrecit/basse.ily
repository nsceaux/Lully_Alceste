\clef "basse" sol,1 |
re2 la, |
sib, sib |
la sol |
fa1~ |
fa2 mib8. re16 |
do8. re16 mib4. fa8 |
sol4 re mib |
fa2. |
fad |
sol |
la2 re4 |
la,2. |
re |
sib,2 do4~ |
do re re, |
sol,2 sol4 |
fad2. |
sol2 sol,4 |
re2 re8 mib |
fa2 fa,4 |
do8 re mib fa sol4~ |
sol8 fa mib2 |
re4 re,2 |
sol,2 sol4 |
la2 fa4 |
sib si2 |
do'4. sib8 la4 |
sol re' re |
sol2 \custosNote sol8 \stopStaff