\key la \minor \beginMark "Ritournelle"
\digitTime\time 3/4 \midiTempo#80 s2.*27
\time 2/2 \midiTempo#160 s1*14
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*6
\digitTime\time 3/4 s2.*3
\time 4/4 s1*4
\time 2/2 \midiTempo#160 s1*3
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1*12
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*4
\digitTime\time 3/4 s2.
\time 2/2 \midiTempo#160 \grace s16 s1*13
\time 4/4 \midiTempo#80 s1*6
\time 2/2 \midiTempo#160 s1 \bar "|."
