<<
  %% Céphise
  \tag #'(cephise basse recit0 recit1 recit2) {
    <<
      \tag #'recit0 {
        \clef "vdessus" R2.*27 R1*16 R2. R1*6 R2.*3 R1*4 r4
      }
      \tag #'recit2 {
        s2.*27 s1*16 s2. s1*6 s2.*3 s1*4 s4
      }
      \tag #'(cephise basse recit1) {
        <<
          \tag #'(basse recit1) { s2.*27 s2 \ffclef "vdessus" }
          \tag #'cephise { \clef "vdessus" R2.*27 r2 }
        >> r4 <>^\markup\character Céphise
        la'4 |
        la'4. si'8 do''4. do''8 |
        do''4.( si'8) si'4. si'8 |
        si'2 la'8[ sold'] la'4 |
        sold'2 r4 sold' |
        la'4 la' fad'8[ mi'] fad'4 |
        sol'2 r4 si'8. si'16 |
        do''4 do''8 do'' do''4( si') |
        do''2 r4 sol' |
        sold' sold'8 sold' la'4 si'8 do'' |
        si'4\trill si' r sold' |
        la'2 la'4. la'8 |
        la'4.( sold'8) sold'4.\trill la'8 |
        la'2 r |
        <<
          \tag #'basse {
            s1*2 s2. s1*6 s2.*3 s1*4 s4 \ffclef "vdessus"
          }
          \tag #'cephise { R1*2 R2. R1*6 R2.*3 R1*4 r4 }
        >>
      }
    >>
    % (trio)
    <<
      \tag #'recit2 { s2. s1*2 s2. s1*12 s4 }
      \tag #'(cephise basse recit0) {
        <>^\markup\character Céphise
        do''4 do''4. do''8 |
        si'4.\trill si'8 mi''4. mi''8 |
        la'2 la'4 fa'' |
        re''8. sol''16 mi''8 fa'' re''8.\trill do''16 |
        do''2 sol'4. sol'8 |
        la'2 si'4. si'8 |
        do''4 do'' dod''4. dod''8 |
        red''2 mi''4. mi''8 |
        mi''2 re''4. re''8 |
        re''2 do''4. do''8 |
        do''4 do'' si'4. si'8 |
        si'2 do''8[ si'] do''4 |
        re''2 re''4. re''8 |
        re''2 do''8[ si'] do''4 |
        si'2 r4 sold'8*3/2 sold'8*1/2 |
        la'2 sold'4. la'8 |
        la'4
      }
    >>
    % (pheres)
    <<
      \tag #'(cephise recit0) { r4 r2 R2. R1*4 R2. r2 }
      \tag #'(recit2 basse) {
        s2. s2. s1*4 s2. s2 \ffclef "vdessus"
      }
    >>
    % (céphise)
    \tag #'(cephise recit2 basse) {
      <>^\markup\character Céphise
      r4 mi'8 mi' |
      la'4. si'8 do''4 do''8 la' |
      mi''2. si'4 |
      do''4. do''8 do''4. mi''8 |
      la'4. re''8 si'4.\trill si'8 |
      do''[ si'] do''4 do''( si') |
      do''2 r4 mi'' |
      la' la'8 la' mi'4 fad'8 sol' |
      fad'2 re''4. re''8 |
      re''4 re''8 do'' do''4( si'8) do'' |
      si'2\trill si' |
      sold'4. sold'8 la'4. si'8 |
      do''4.( re''8) si'4.\trill la'8 |
      la' \tag #'cephise { r8 r4 r2 R1*6 }
    }
  }
  %% Alceste
  \tag #'(alceste basse recit1 recit2) {
    <<
      \tag #'recit2 {
        s2.*27 s1*16 s2. s1*6 s2.*3 s1*7 s2. s1*12 s4
      }
      \tag #'(alceste basse recit1) {
        \clef "vbas-dessus" R2.*15 |
        <>^\markup\character Alceste
        r4 mi''4. sold'8 |
        la'4 la'8 la' la' sold' |
        la'4 fa''4. fa''8 |
        re''4\trill re''8. re''16 re''8[ do''] |
        do''8. do''16 do''4 si'8. si'16 |
        sold'4 sold' r8 mi'' |
        dod''2 la'8 la'16 la' |
        re''8. la'16 si'4 si'8. si'16 |
        do''8 do'' do'' mi'' la'8. sol'16 |
        fad'4.\trill fad'8 fad'8. sol'16 |
        mi'4 mi''4. sold'8 |
        la'4 la'8 la' la' sold' |
        la'2
        <<
          \tag #'(basse recit1) {
            s2 s1*13 \ffclef "vbas-dessus"
          }
          \tag #'alceste { r2 R1*13 }
        >> <>^\markup\character Alceste
        r4 r8 mi'' do''4 do''8 do''16 do'' |
        sol'8 sol'16 la' fa'8\trill fa'16 mi' mi'8\trill mi' r sol'16 sol' |
        do''8 do''16 re'' mi'' mi'' mi'' mi'' mi''8. fa''16 |
        re''8\trill re'' r si' mi''8. mi''16 mi'' re'' do''\trill si' |
        do''4 la'8. la'16 la'4 sol'8[ fad'16] sol' |
        fad'4.\trill mi'8 mi'4 r8 sold' |
        la' la'16 la' si'8 dod''16 re'' dod''4 dod'' |
        mi''8. mi''16 mi''8. fa''16 re''4. re''16 dod'' |
        re''4 r8 fa'' re''8\trill re''16 re'' la'8 si'16 do'' |
        si'4\trill si'8 r16 sol'' si'8. si'16 |
        do''4 r8 do'' do'' si' |
        do''4 r r8 mi''16 mi'' |
        si'8. si'16 sold'8 sold'16 si' mi'8 mi' do'' do''16 do'' |
        do''4 do''8 do''16 si' si'8\trill r16 sol' si'8.\trill do''16 |
        re''8. re''16 re''8. mi''16 fa''4 re''8 r16 si' |
        sold'8 sold' r la' la'4. la'16 sold'! |
        la'4
        <<
          \tag #'basse { s2. s1*2 s2. s1*12 s4 }
          \tag #'(alceste recit1) {
            <>^\markup\character Alceste
            la'4 mi'4. fad'8 |
            sol'4. sol'8 sol'4. mi'8 |
            fa'2 fa'4 la' |
            si'8. si'16 do''8 do'' si'8.\trill do''16 |
            do''2 mi'4. mi'8 |
            fad'2 sold'4. sold'8 |
            la'4 la' la'4. la'8 |
            la'2 sol'4. sol'8 |
            fad'2 fad'4. si'8 |
            sold'2 la'4. mi'8 |
            fa'4 fa' fad'4. fad'8 |
            sold'2 la'8[ sold'] la'4 |
            si'2 si'4. si'8 |
            mi'2 mi'4. la'8 |
            sold'2 r4 si'8*3/2 si'8*1/2 |
            do''4.( si'8) si'4. do''8 |
            la'4
          }
        >>
      }
    >>
    <<
      \tag #'(basse recit2) {
        s2. s2. s1*4 s2. s1*13 s8
        \ffclef "vbas-dessus"
      }
      \tag #'(alceste recit1) {
        r4 r2 R2. R1*4 R2. R1*13 r8
      }
    >>
    \tag #'(alceste basse recit2) {
      <>^\markup\character Alceste
      mi'' do''16 do'' do'' mi'' la'8 la'16 la' mi'8\trill mi'16 fad' |
      sol'8 sol' r si'16 si' mi''8 mi''16 mi'' do''8.\trill do''16 |
      la'8 la'16 fa' fa' sol' la' si' do''8 do''16 do'' re''8 mi''16 fa'' |
      mi''4\trill r8 mi'16 mi' fad'4 r8 fad'16 fad' |
      sold'4 r8 sold' la'4 re''8 re''16 si' |
      sold'8 sold' do''16 do'' re'' mi'' si'4\trill si'8. do''16 |
      la'4 r r2 |
    }
  }
  %% Pheres
  \tag #'(pheres basse recit2) {
    <<
      \tag #'basse {
        s2.*27 s1*14 s1*2 s2. s1*6 s2.*3 s1*4 s1*3 s2. s1*12 s4
        \ffclef "vtaille"
      }
      \tag #'(pheres recit2) {
        \clef "vtaille" R2.*27 |
        r2 r4 <>^\markup\character Pheres do'4 |
        do'4. re'8 mi'4. mi'8 |
        mi'4.( re'8) re'4. re'8 |
        re'2 do'8[ si] do'4 |
        si2\trill r4 si |
        do'4 do' la8[ sol] la4 |
        si2 r4 re'8. re'16 |
        mi'4 mi'8 fa' mi'4( re')\trill |
        do'2 r4 mi' |
        re'4\trill do'8 si do'4 si8 la |
        sold4 sold r si |
        do'2 do'4. re'8 |
        si2\trill si4. do'8 |
        la2 r |
        R1*2 R2. R1*6 R2.*3 R1*4 |
        r4 <>^\markup\character Pheres la la4. la8 |
        mi'4. mi'8 dod'4. dod'8 |
        re'2 re'4 re |
        sol8. mi16 la8 fa sol16[ fa] sol8 |
        do2 do'4. do'8 |
        do'2 si4. si8 |
        la4 la fad4. fad8 |
        si2 si4. si8 |
        si2 si4. si8 |
        mi'2 la4. la8 |
        re'2 re'4. re'8 |
        re'2 do'4. do'8 |
        sold4 sold sold4. sold8 |
        la2 la8[ sold] la4 |
        mi2 r4 mi'8*3/2 mi'8*1/2 |
        re'2 mi'8[ re'] mi'4 |
        la
      }
      >> <>^\markup\character Pheres
      do'8 do'16 do' la8.\trill la16 la8. mi16 |
      fa4 re'8 re'16 re' si8.\trill si16 |
      sold8 sold16 sold la8 si16 do' si8.\trill si16 mi' re' do'\trill si |
      do'4. la8 fad4 fad8 sold16 la |
      sold8 sold r si16 si do'8. do'16 dod'8 dod'16 re' |
      re'4 re' r8 fa'16 mi' re'8\trill si16 si |
      sold8. mi16 la8. la16 la8. sold16 |
      \appoggiatura sold la2
  }
>>