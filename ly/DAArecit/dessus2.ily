\clef "dessus" r4 do''8. re''16 do''4 |
si'4 si'8. do''16 si'4 |
la' la'8. si'16 la'4 |
sold' sold'2 |
r4 sold' sold' |
la' la''4. la''8 |
la''4 sol''4. sol''8 |
sol''4 fa'' sol''8 fa'' |
mi''4 mi'' fa'' |
fa'' mi''4. mi''8 |
mi''4 re''4. re''8 |
re''4 do'' sold' |
la'8. si'16 sold'4. la'8 |
la'4 r r |
R2.*13 R1*14 R1*2 R2. R1*6 R2.*3 R1*4 R1*3 R2. R1*12 R1 R2. R1*4
R2. R1*13 R1*6 R1 |
