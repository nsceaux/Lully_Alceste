\clef "basse" la,4 la4. la8 |
la4 sold4. sold8 |
la4 la,4. la,8 |
mi4 mi8. fa16 mi4 |
re re8. mi16 re4 |
do4 do4. do8 |
si,4 si,4. si,8 |
la,2. |
la4 la4. la8 |
sold4 sol8. la16 sol4 |
fad4 fa8 sol fa4 |
mi2. |
re4 mi mi, |
la,4 <<
  \tag #'basse {
    r4 r |
    R2.*13 R1*14 R1*2 R2. R1*6 R2.*3 R1*4 R1*3 R2. R1*12 R1 R2.
    R1*4 R2. R1*13 R1*6 R1 |
  }
  \tag #'basse-continue {
    la4. la8 |
    re4 re4. re8 |
    mi2 mi4 |
    fa4 re mi |
    la, re4. re8 |
    sol4 sold la |
    la, re2 |
    mi mi4 |
    la2 la4 |
    fad4 sol2 |
    la2. |
    si2 si,4 |
    mi2 mi4 |
    fa re mi |
    la,1 |
    la2. la4 |
    fa2. re4 |
    mi2 la, |
    mi, mi |
    la, re |
    sol,4 sol fa2 |
    mi4 do sol sol, |
    do2 do' |
    si la4 la, |
    mi2 re |
    do fa4 re |
    mi2 mi, |
    la,2. si,4 |
    do1 |
    do'4. si8 do'2 |
    do2. |
    sol2 sold |
    la si4 mi |
    si,2 mi,4 mi |
    do si, la,2 |
    la sib8 sol la la, |
    re2. fad,4 |
    sol,2 sol4 |
    la8 fa sol4 sol, |
    do2. | \allowPageTurn
    sold,2 la,4 la |
    fad2 sol |
    fa4. mi8 re2 |
    mi4 la, mi,2 |
    la, la |
    mi' dod' |
    re'2. re4 |
    sol8. mi16 la8 fa sol sol, |
    do2 do'4. do'8 |
    do'2 si |
    la fad |
    si mi |
    si,2. si4 |
    mi'2 la |
    re' re'4. re'8 |
    re'2 do' |
    sold2. sold4 |
    la2 la, |
    mi r4 mi'8. mi'16 |
    re'2 mi'4 mi |
    la2 dod |
    re2. |
    mi4 la, sold,2 |
    la, si, |
    mi,4 mi la sol |
    fa4. mi8 re2 |
    mi4 fa8 re mi mi, |
    la,1~ |
    la,2 la |
    sold1 |
    la2 mi |
    fa4 re sol4. mi8 |
    la4 fa sol sol, |
    do1 |
    dod |
    re2. re4 |
    mi2 fad |
    sol2. fa4 |
    mi4. re8 do4. si,8 |
    la,4 re, mi,2 |
    la,1 |
    mi |
    fa2 mi4 re |
    do2 re |
    mi fa |
    mi4 do8 si,16 la, mi4 mi, |
    la,2 la8 sol fa mi |
    \once\set Staff.whichBar = "|"
    \custosNote re4
  }
>>
