\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit0 \includeNotes "voix"
    >> \keepWithTag #'recit0 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit2 \includeNotes "voix"
    >> \keepWithTag #'recit2 \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*5\break s2.*7\break s2.*5\pageBreak
        s2.*4 s2 \bar "" \break s4 s2.*3\break
        s2.*2 s1*2\break s1*5\pageBreak
        s1*5\break s1*3 s2 \bar "" \break s2 s2. s2 \bar "" \break
        s2 s1*2\break s1*3\pageBreak
        s2.*2\break s2. s1\break s1*2\break s1*4\pageBreak
        s2. s1*3\break s1*5\break s1*4\pageBreak
        s1 s2. s4 \bar "" \break s2. s1\break s1*2\break
        s2. s1*2\break s1*5\break s1*4\pageBreak
        s1*2\break s1*2\break s1 s2 \bar "" \break
        s2 s1 s2 \bar "" \break
      }
      \modVersion {
        s2.*27\break s1*14\break
        s1*2 s2. s1*6 s2.*3 s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
