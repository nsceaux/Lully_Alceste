\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:score "score-basse")
   (basse-continue
    #:system-count 24
    #:music , #{ s2.*5\break s2.*5\break s2.*5\break #}
    #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#100 #}))
