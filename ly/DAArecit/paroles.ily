\tag #'(alceste basse recit1) {
  Ah pour -- quoy nous sé -- pa -- rez- vous ?
  Eh du moins at -- ten -- dez que la mort nous se -- pa -- re ;
  cru -- els, quel -- le pi -- tié bar -- ba -- re
  vous pres -- se d’ar -- ra -- cher Al -- ceste à son es -- poux ?
  Ah pour -- quoy nous sé -- pa -- rez- vous ?
}
\tag #'(cephise basse recit1 pheres recit2) {
  Plus vostre es -- poux mou -- rant voit d’a -- mour, & d’ap -- pas,
  et plus le jour qu’il perd luy doit fai -- re d’en -- vi -- e :
  ce sont les dou -- ceurs de la vi -- e
  qui font les hor -- reurs du tré -- pas.
}
\tag #'(alceste basse recit1) {
  Les arts n’ont point en -- core a -- che -- vé leur ou -- vra -- ge ;
  cét au -- tel doit por -- ter la glo -- ri -- euse i -- ma -- ge
  de qui si -- gna -- le -- ra sa foy
  en mou -- rant pour sau -- ver son roy.
  Le prix d’u -- ne gloire im -- mor -- tel -- le
  ne peut- il tou -- cher un grand cœur ?
  Faut- il que la mort la plus bel -- le
  ne lais -- se pas de fai -- re peur ?
  A quoy sert la foule im -- por -- tu -- ne
  dont les roys sont em -- bar -- ras -- sez ?
  Un coup fa -- tal de la for -- tu -- ne
  es -- car -- te les plus em -- pres -- sez.
}
\tag #'(alceste recit1 cephise basse recit0 pheres recit2) {
  De tant d’a -- mis qu’a -- voit Ad -- me -- te
  au -- cun ne vient le se -- cou -- rir ;
  quelque hon -- neur qu’on pro -- met -- te,
  on le lais -- se mou -- rir.
  \tag #'(alceste recit1 cephise basse recit0) {
    Quelque hon -- neur qu’on pro -- met -- te,
    on le lais -- se mou -- rir,
  }
  \tag #'(pheres recit2) {
    On le lais -- se mou -- rir,
    quelque hon -- neur qu’on pro -- met -- te,
  }
  on le lais -- se mou -- rir,
  on le lais -- se mou -- rir.
}
\tag #'(pheres recit2 basse) {
  J’ai -- me mon fils, je l’ay fait roy ;
  pour pro -- lon -- ger son sort je mour -- rois sans ef -- froy,
  si je pou -- vois of -- frir des jours di -- gnes d’en -- vi -- e ;
  je n’ay plus qu’un res -- te de vi -- e
  ce n’est rien pour Ad -- mete, & c’est beau -- coup pour moy.
}
\tag #'(céphise basse recit2) {
  Les hon -- neurs les plus é -- cla -- tans
  en vain dans le tom -- beau pro -- met -- tent de nous sui -- vre ;
  la mort est af -- freuse en tout temps :
  mais peut- on re -- non -- cer à vi -- vre
  quand on n’a ves -- cu que quinze ans ?
}
\tag #'(alceste basse recit2) {
  Cha -- cun est sa -- tis -- fait des ex -- cu -- ses qu’il don -- ne :
  ce -- pen -- dant on ne voit per -- son -- ne
  qui pour sau -- ver Ad -- mete o -- se per -- dre le jour ;
  le de -- voir, l’a -- mi -- tié, le sang, tout l’a -- ban -- don -- ne,
  il n’a plus d’es -- poir qu’en l’a -- mour.
}
