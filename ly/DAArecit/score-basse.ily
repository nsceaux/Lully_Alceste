\score {
  \new Staff \notemode {
    \key la \minor \digitTime\time 3/4
    \keepWithTag #'no-tag \includeNotes "basse"
    r4 r | R2.*86 \bar "|."
  }
  \layout { }
}
