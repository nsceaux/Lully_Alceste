\clef "dessus" <>^"Violons" r4 mi''8. fa''16 mi''4 |
re'' re''8. mi''16 re''4 |
do'' do''8. re''16 do''4 |
si'\trill si'2 |
r4 si' si' |
do''4. re''8 mi'' fa'' |
re''4 re''8. mi''16 re''4 |
dod'' re''4. re''8 |
re''4 do'' re''8 do'' |
si'4 si' do''8 si' |
la'4 la' si'8 la' |
sold'4 la' si' |
do''8. re''16 si'4.\trill la'8 |
la'4 r r |
R2.*13 R1*14 R1*2 R2. R1*6 R2.*3 R1*4 R1*3 R2. R1*12 R1 R2. R1*4
R2. R1*13 R1*6 R1 |
