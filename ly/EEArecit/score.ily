\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit2 \includeNotes "voix"
    >> \keepWithTag #'recit2 \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit1 \includeNotes "voix"
    >> \keepWithTag #'recit1 \includeLyrics "paroles"
    \new Staff \withRecit <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s1*2\break s1*2\break
        s1*3\break s1*2\break s1*2\pageBreak
        s2. s1*2\break s1*3\break
        s1*2\break s1*2\break s1*2\break s1*2 s2.\pageBreak
        s2. s1 s2.\break s2. s1 s2 \bar "" \break
        s2 s1 s2 \bar "" \break s2 s1 s2 \bar "" \break
        s2 s1*2\break s1*3\pageBreak
        s1*2\break s1 s2.\break s2. s1 s2 \bar "" \break
        s2 s1\break s1 s2.\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
