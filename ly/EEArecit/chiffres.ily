s1 s2 <\markup\triangle-down 7 6 \figure-flat >
s1 <6>2 <7>4 <6> s1 <6>2 <6 5> s1*2 <6>1 <_->2 <_+>
\new FiguredBass <6>1 s4 \new FiguredBass { <6>2. <6> } <6 5 _->4
<_+>4 <6>8 <6 5 _-> <4> \new FiguredBass <_+> s1*2 s2 <6 5/> s1 <7>2 <6+>
s2. <6>8. <6 _->16 s2 <4>4 <3> s1 <6>1 s2 <6> s <7>4 <4>8 <3>
s2. <6>8 <6> s1 s4 <7> <4>8 <3>
s2 <6>8 <6 _-> <"">4\figExtOn <"">\figExtOff <6>2 s2.
<7>4 <4>8 <3> s4 <"">8\figExtOn <"">\figExtOff <6> <6 _-> s <6> <6>4
s2. <6 4+>8. <6+>16 s2 <7 _->4 <6 5 _->
<_+>4 <6> s <_+> <_+>\figExtOn <_+>\figExtOff s <6> <6 5>2 <4>4 <3>
s1*2 s4 <6 5 _-> <4> \new FiguredBass <_+> s2 <6 5/>
s4 <6> s <4>8 \new FiguredBass <3>
s2. <_+>4 s <6> <6 5>2 s1 s2. <5>2 <6>4 s2 <6 5 _-> <_+> <6>
s1*2 <6>2 <6>4 s2. s4. <6 5>8 <4> \new FiguredBass <3>
