\clef "basse" mib1\repeatTie~ |
mib2 do |
sib,1 |
la,2 sol, |
fa,1 |
re,2 mib,4 fa, |
sib,2~ sib,8 do sib, la, |
sol,1 |
fad, |
sol, |
mi,2 fa,~ |
fa,4 re,2. |
re2 mib4 do |
re4 sib,8 do re re, |
sol,1~ |
sol, |
sol2 mi |
fa1 |
re |
do2 fa8. mib16 re8. do16 |
sib,2 do |
fa, fa |
re1 | \allowPageTurn
mib2 mi |
fa mib4 fa8 fa, |
sib,2~ sib,8 sib, la, sol, |
fa,2 fa |
sib8 la sol4 do'8 do |
fa4~ fa8 mib re do |
sib,4 la, sol, fa, |
do fa sib8. la16 |
sol4 do'8 do fa4~ |
fa8 mib re do sib, la, sol, fa, |
do2 do'4 sib8. la16 |
sol4. sol,8 do2 |
re4 fad, sol, re, |
sol, sol do' la |
sib2 do'4 do |
fa,1 |
sib, | \allowPageTurn
mib4 do re re, |
sol, sol mi fa8 fa, |
do4 la, re8 sib, do do, |
fa,4. fa8 re4 sol8 sol, |
do4 la, sib, do8 do, |
fa,1 |
fa2. |
re |
mib2 do |
re2 si, |
do1~ |
do |
la,2 mi,4 |
fa,2. |
do4 re8 sib, do do, |
fa,2. |
