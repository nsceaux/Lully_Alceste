<<
  \tag #'(recit basse) {
    \clef "vbasse" <>^\markup\character Pluton
    r4 r8 sib16 sib mib4 r8 mib16 fa |
    sol4 sol8 sib mib4 mib8 re |
    re2\trill sib,16 sib, sib, do re8\trill re16 mi |
    fa8 fa r fa16 fa sib8. sib16 mi8\trill mi16 fa |
    fa4
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    r8 la16 do' fa8 fa16 sol la8. fa16 |
    sib8 sib r re' sol16 fa mib re do8. re16 |
    sib,2 r |
    \ffclef "vbasse" <>^\markup\character Pluton
    re8 re16 re re8. re16 sol8. la16 sib sib sib sol |
    re'4 re'16 r la la fad8 fad16 fad fad8. la16 |
    re8 re r sol16 sol si,8. si,16 si,8 si,16 do |
    do4 r8 do' la8.\trill la16 la8. do'16 |
    fa8 fa16 fa sib8 re16 fa sib,4 sib,8 sib |
    sib4. re'8 sol4. la8 |
    fad8. re16 sol8. do16 re8[ do16] re |
    sol,1 |
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    re'4. re'8 sib4 sib8. re'16 |
    sol4 r8 sib sib4 sib8 sib16 la |
    la8\trill la r fa la8. la16 la8. do'16 |
    fa4. fa8 fa8. fa16 fa8. mi16 |
    mi4\trill r8 do' la16\trill la la la sib8. do'16 |
    re'4 sib16 sib sib re' sol4\trill sol8. la16 |
    fa8 fa r do' la8.\trill la16 la8. fa16 |
    sib8 sib r16 sib sib re' sib8. sib16 sib8. sib16 |
    sol4\trill r8 mib' sol8.\trill sol16 sol8. do'16 |
    la8\trill la r fa16 fa sib4. sib16 la |
    sib1 |
    \ffclef "vdessus" <>^\markup\character Proserpine
    r2 do''4 fa'' |
    re''8. do''16 sib'4 sol'8.\trill do''16 |
    la'4\trill fa'8 r16 la' sib'8 do'' |
    re''4. re''8 mi''4. fa''8 |
    mi''4\trill do''8 fa'' re''8. do''16 |
    sib'4 sol'8.\trill do''16 la'4\trill |
    fa'8 r16 la' sib'8 do'' re''8. re''16 mi''8. fa''16 |
    mi''2\trill do''8 do''16 do'' sol'8. la'16 |
    sib'4 r8 re'' mib'' mib''16 re'' do''8.\trill sib'16 |
    la'8\trill la' re''8. la'16 sib'4 la'8.\trill sol'16 |
    sol'4 r8 sol'' mi''\trill mi''16 mi'' fa''8. do''16 |
    re''8 re'' sib'8. re''16 sol'4\trill sol'8. la'16 |
    fa'4
    \ffclef "vbasse" <>^\markup\character Pluton
    la8. do'16 fa8. fa16 do8. fa16 |
    sib,4 sib, r8 sib sib8. la16 |
    sol2\trill sol4 fad |
    sol4 r8 sib sib sib16 sib la8.\trill sib16 |
    sol8\trill sol la8. do'16 fa4 fa8. mi16 |
    fa4
  }
  \tag #'(recit2 recit1) {
    \tag #'recit2 \clef "vdessus"
    \tag #'recit1 \clef "vhaute-contre"
    R1*13 R2. R1*13 R2.*2 R1 R2.*2 R1*11 r4
  }
>>
r8 <<
  \tag #'(recit2 basse) {
    \tag #'basse \ffclef "dessus" <>^\markup\character Proserpine
    la'8 re'' re''16 re'' si'8. si'16 |
    do''8 do'' do''8. do''16 sib'8.[\trill la'16] sol'8.\trill fa'16 |
    fa'4 r
  }
  \tag #'recit1 {
    <>^\markup\character Un suivant de Pluton
    do'8 fa' fa'16 fa' re'8. sol'16 |
    mi'8 mi' la'8. la'16 sol'8.[\trill fa'16] mi'8.\trill fa'16 |
    fa'4 r
  }
  \tag #'recit {
    fa8 re re16 re sol8. sol16 |
    do8 do la,8. la,16 sib,4 do16[ sib,] do8 |
    fa,4 r
  }
>>
<<
  \tag #'(recit basse) {
    \tag #'basse { \ffclef "vbasse" <>^\markup\character Pluton }
    do8 do16 do fa8. sol16 |
    la4 do'8 do'16 do' la8.\trill do'16 |
    fa8 fa r fa16 fa sib8 sib16 sib |
    sol4.\trill sol8 la la la la |
    fad4 r8 re sol8 sol16 sol re8 mi16 fa |
    mi8\trill mi r sol do' sol16 sol mi8\trill mi16 sol |
    do2 r4 r16 do do do |
    fa[\melisma sol fa sol] la[ sib la sib]\(\melismaEnd do'8.\) do'16 |
    la8\trill la r do'16 sib la8 sol16 fa |
    mi8.\trill do16 fa8 fa16 sib, do8 do16 do |
    fa,2. |
  }
  \tag #'(recit1 recit2) { r2 R2.*2 R1*4 R2.*4 }
>>
