\tag #'(recit basse) {
  In -- so -- lent jus -- qu’i -- cy bra -- ves- tu mon cour -- roux ?
  Quelle in -- juste au -- da -- ce t’en -- ga -- ge,
  a trou -- bler la paix de ces lieux ?
  
  Je suis né pour domp -- ter la ra -- ge
  des mons -- tres les plus fu -- ri -- eux.
  
  Est- ce le dieu ja -- loux qui lan -- ce le ton -- ner -- re
  qui t’o -- blige à por -- ter la guer -- re
  jus -- qu’au cen -- tre de l’u -- ni -- vers ?
  Il tient sous son pou -- voir & le ciel & la ter -- re,
  veut- il en -- cor ra -- vir l’em -- pi -- re des en -- fers ?
  
  Non, Plu -- ton, regne en paix, joü -- is de ton par -- ta -- ge ;
  je viens cher -- cher Al -- ceste en cét af -- freux sé -- jour,
  per -- mets que je la rende au jour,
  je ne veux point d’autre a -- van -- ta -- ge.
  Si c’est te faire ou -- tra -- ge
  d’en -- trer par for -- ce dans ta cour
  par -- donne à mon cou -- ra -- ge
  et fais grace à l’a -- mour.
  
  Un grand coœur peut tout quand il ai -- me,
  tout doit ce -- der à son ef -- fort.
  Un grand coœur peut tout quand il ai -- me,
  tout doit ce -- der à son ef -- fort.
  C’est un ar -- rest du Sort,
  Il faut que l’a -- mour ex -- tré -- me
  soit plus fort
  que la mort.
  Il faut que l’a -- mour ex -- tré -- me
  soit plus fort
  que la mort.
  
  Les en -- fers, Plu -- ton luy- mes -- me,
  tout doit en es -- tre d’ac -- cord ;
  il faut que l’a -- mour ex -- tré -- me
  soit plus fort
  que la mort.
}

Il faut que l’a -- mour ex -- tré -- me
soit plus fort
que la mort.

\tag #'(recit basse) {
  Que pour re -- voir le jour l’om -- bre d’Al -- ces -- te sor -- te ;
  pre -- nez pla -- ce tous deux au char dont je me sers :
  qu’au gré de vos vœux, il vous por -- te ;
  par -- tez, les che -- mins sont ou -- verts.
  Qu’u -- ne vo -- lan -- te es -- cor -- te
  vous con -- duise au tra -- vers
  des noi -- res va -- peurs des en -- fers.
}
