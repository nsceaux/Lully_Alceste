\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'vdessus \includeNotes "voix"
    >> \keepWithTag #'vdessus \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vhaute-contre \includeNotes "voix"
    >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vtaille \includeNotes "voix"
    >> \keepWithTag #'vtaille \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4. s1.*2 s2. \bar "" \break s2. s1.*2 s2. \bar "" \pageBreak
        s2. s1.*2 s2. \bar "" \break s2. s1.*3\pageBreak
        s1.*3\break s1.*3 s2. \bar "" \pageBreak
        s2. s1.*3\break
      }
      \modVersion {
        s4. s1.*3 s4.*3\break s4. s1.*4\break s1.*4\break
        s1.*4\break s1.*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
