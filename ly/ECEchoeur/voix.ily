<<
  \tag #'(vdessus basse) {
    \clef "vdessus" la'8 sib'4 |
    do''4. re''8 do''4 do''4. re''8 mi''4 |
    fa''4. fa''8 mi''4 re''4. re''8 do''4 |
    sib'4. sib'8 la'4 sol'4. la'8 sib'4 |
    la'2.~ la'4. fa''8 fa''4 |
    fa''4. fa''8 fa''4 mi''4. mi''8 re''4 |
    dod''4. dod''8 dod''4 re''4. re''8 re''4 |
    re''4. re''8 re''4 re''4. mi''8 dod''4 |
    re''2. r4 r8 re'' re''4 |
    re''4. re''8 re''4 re''4. re''8 re''4 |
    mi''4. mi''8 mi''4 mi'' re''2 |
    mi''4. mi''8 mi''4 mi''4 re''2 |
    mi''2. r4 r8 do'' do''4 |
    do''4. do''8 do''4 do''4. do''8 do''4 |
    re''4. re''8 re''4 sib'4. sib'8 sib'4 |
    do''4. do''8 do''4 la'4. la'8 la'4 |
    sib'2. r4 r8 sib' la'4 |
    sib'2 sib'4 sib'4. sib'8 la'4 |
    sib'4. sib'8 sib'4 sib'4 la'2 |
    sib'4. sib'8 sib'4 sib'4 la'2 |
    sib'2. r4 r8 sib' sib'4 |
    sib'4. sib'8 sib'4 la'4. la'8 sib'4 |
    sol'4. sol'8 sol'4 do''4. do''8 do''4 |
    la'4. la'8 re''4 sol'4. la'8 sib'4 |
    la'2.~ la'4.
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" do'8 do'4 |
    fa'4. fa'8 fa'4 fa'4. fa'8 mi'4 |
    re'4. re'8 mi'4 fa'4. fa'8 fa'4 |
    fa'4. fa'8 fa'4 fa'4. fa'8 mi'4 |
    fa'2.~ fa'4. la'8 la'4 |
    la'4. la'8 la'4 sol'4. sol'8 fa'4 |
    mi'4. mi'8 mi'4 fa'4. fa'8 fa'4 |
    fa'4. fa'8 sol'4 mi'4. fad'8 sol'4 |
    fad'2. r4 r8 fad' fad'4 |
    sol'4. sol'8 sol'4 sol'4. sol'8 sol'4 |
    sol'4. sol'8 sol'4 sol' sol'2 |
    sol'4. sol'8 sol'4 sol' sol'2 |
    sol'2. r4 r8 mi' mi'4 |
    fa'4. fa'8 fa'4 fa'4. fa'8 fa'4 |
    fa'4. fa'8 fa'4 mib'4. mib'8 mib'4 |
    mib'4. mib'8 mib'4 re'4. re'8 re'4 |
    re'2. r4 r8 sol' fad'4 |
    sol'2 sol'4 sol'4. sol'8 fad'4 |
    sol'4. sol'8 sol'4 sol'4 fad'2 |
    sol'4. sol'8 sol'4 sol'4 fad'2 |
    sol'2. r4 r8 sol' sol'4 |
    sol'4. sol'8 sol'4 fa'4. fa'8 fa'4 |
    mi'4. mi'8 mi'4 fa'4. fa'8 fa'4 |
    fa'4. fa'8 sol'4 mi'4. mi'8 mi'4 |
    fa'2.~ fa'4.
  }
  \tag #'vtaille {
    \clef "vtaille" fa8 sol4 |
    la4. sib8 la4 la4. la8 do'4 |
    la4. la8 la4 la4. sib8 do'4 |
    re'4. re'8 re'4 do'4. do'8 do'4 |
    do'2.~ do'4. do'8 do'4 |
    sib4. sib8 sib4 sib4. sib8 sib4 |
    la4. la8 la4 la4. la8 la4 |
    sib4. sib8 sib4 la4. la8 la4 |
    la2. r4 r8 la la4 |
    si4. si8 si4 si4. si8 si4 |
    do'4. do'8 do'4 do'4 si2 |
    do'4. do'8 do'4 do' si2 |
    do'2. r4 r8 sol sol4 |
    la4. la8 la4 la4. la8 la4 |
    sib4. sib8 sib4 sol4. sol8 sol4 |
    sol4. sol8 sol4 sol4. la8 fad4 |
    sol2. r4 r8 re' re'4 |
    re'2 re'4 re'4. re'8 re'4 |
    re'4. re'8 re'4 re' re'2 |
    re'4. re'8 re'4 re'4 re'2 |
    re'2. r4 r8 re' re'4 |
    do'4. do'8 do'4 do'4. do'8 do'4 |
    do'4. do'8 do'4 do'4. do'8 do'4 |
    re'4. re'8 re'4 do'4. do'8 do'4 |
    do'2.~ do'4.
  }
  \tag #'vbasse {
    \clef "vbasse" fa8 fa4 |
    fa4. fa8 fa4 fa4. fa8 do4 |
    re4. re8 re4 re4. re8 la,4 |
    sib,4. sib,8 sib,4 do4. do8 do4 |
    fa,2.~ fa,4. fa8 fa4 |
    sol4. sol8 sol4 sol4. sol8 sol4 |
    la4. la8 la4 fa4. fa8 fa4 |
    sib4. sib8 sol4 la4. la8 la4 |
    re2. r4 r8 re8 re4 |
    sol4. sol8 sol4 sol4. sol8 sol,4 |
    do4. do8 do4 do4 sol,2 |
    do4. do8 do4 do4 sol,2 |
    do2. r4 r8 do do4 |
    fa4. fa8 fa4 fa4. fa8 fa4 |
    sib,4. sib,8 sib,4 mib4. mib8 mib4 |
    do4. do8 do4 re4. re8 re4 |
    sol,2. r4 r8 sol re4 |
    sol2 sol4 sol4. sol8 re4 |
    sol4. sol8 sol4 sol re2 |
    sol4. sol8 sol4 sol4 re2 |
    sol2. r4 r8 sol sol4 |
    mi4. mi8 mi4 fa4. fa8 fa4 |
    do4. do8 do4 la,4. la,8 la,4 |
    sib,4. sib,8 sib,4 do4. do8 do4 |
    fa,2.~ fa,4.
  }
>>