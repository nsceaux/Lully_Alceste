Cha -- cun vient i -- cy bas pren -- dre pla -- ce,
sans cesse on y pas -- se,
ja -- mais on n’en sort.
C’est pour tous u -- ne loy ne -- ces -- sai -- re ;
l’ef -- fort qu’on veut fai -- re
n’est qu’un vain ef -- fort :
est- on sa -- ge
de fuïr ce pas -- sa -- ge ?
C’est un o -- ra -- ge
qui meine au port.
Cha -- cun vient i -- cy bas pren -- dre pla -- ce,
sans cesse on y pas -- se,
ja -- mais on n’en sort.
Tous les char -- mes,
plain -- tes, cris, lar -- mes,
tout est sans ar -- mes
con -- tre la mort.
Cha -- cun vient i -- cy bas pren -- dre pla -- ce,
sans cesse on y pas -- se,
ja -- mais on n’en sort.
