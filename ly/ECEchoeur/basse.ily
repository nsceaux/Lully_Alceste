\clef "basse" fa8 fa4 |
fa4. fa8 fa4 fa4. fa8 do4 |
re4. re8 re4 re4. re8 la,4 |
sib,4. sib,8 sib,4 do4. do8 do4 |
fa,2.~ fa,4. fa8 fa4 |
sol4. sol8 sol4 sol4. sol8 sol4 |
la4. la8 la4 fa4. fa8 fa4 |
sib4. sib8 sol4 la4. la8 la4 |
re2. r4 r8 re8 re4 |
sol4. sol8 sol4 sol4. sol8 sol,4 |
do4. do8 do4 do4 sol,2 |
do4. do8 do4 do4 sol,2 |
do2. r4 r8 do do4 |
fa4. fa8 fa4 fa4. fa8 fa4 |
sib,4. sib,8 sib,4 mib4. mib8 mib4 |
do4. do8 do4 re4. re8 re4 |
sol,2. r4 r8 sol re4 |
sol2 sol4 sol4. sol8 re4 |
sol4. sol8 sol4 sol re2 |
sol4. sol8 sol4 sol4 re2 |
sol2. r4 r8 sol sol4 |
mi4. mi8 mi4 fa4. fa8 fa4 |
do4. do8 do4 la,4. la,8 la,4 |
sib,4. sib,8 sib,4 do4. do8 do4 |
fa,2.~ fa,4.
