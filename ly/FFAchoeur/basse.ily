\clef "basse" sol4 |
re'2 re4 |
sol2 sol4 |
do4. re8 mi do |
fa2 fa4 |
sib,4. do8 re sib, |
fa4 fa fa8 fa |
do4 do8 re mib do |
sol4 sol8 la sib sol |
re'2 fad4 |
sol re2 |
sol,4 sol4. sol8 |
do'4. do'8 la4 |
fa do' do |
fa2 <>^\markup\whiteout "[flûtes seules]" sol4 |
la2. |
sol4 la la, |
re4 <>^"[tous]" re8 do sib, la, |
sol,4 <>^"[flûtes]" sol4. sol8 |
<>^"[tous]" do' sib la sol fad sol |
re mi fad4 sol |
do re re, |
sol, <>^"[flûtes]" sol4. sol8 |
do2 la,4 |
re2 sol,4~ |
sol, re,2 |
sol,4 <>^"[tous]" sol4. sol8 |
do'8 sib la sol fad sol |
re mi fad4 sol |
do re re, |
sol,2
