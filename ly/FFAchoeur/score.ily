\score {
  \new StaffGroupNoBar \with { \haraKiri } <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus1 \includeNotes "voix"
      >> \keepWithTag #'vdessus1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre1 \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille1 \includeNotes "voix"
      >> \keepWithTag #'vtaille1 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse1 \includeNotes "voix"
      >> \keepWithTag #'vbasse1 \includeLyrics "paroles"
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus2 \includeNotes "voix"
      >> \keepWithTag #'vdessus2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre2 \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille2 \includeNotes "voix"
      >> \keepWithTag #'vtaille2 \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse2 \includeNotes "voix"
      >> \keepWithTag #'vbasse2 \includeLyrics "paroles"
    >>
    \new StaffGroupNoBracket <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*4\pageBreak
        s2.*4\pageBreak
        s2.*5\pageBreak
        s2.*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
