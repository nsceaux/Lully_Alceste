<<
  %% Chœur 1
  \tag #'vdessus1 {
    \clef "vdessus" <>^\markup\character Chœur sib'4 |
    la'2 la'4 |
    si'4. do''8 re'' si' |
    do''4 do'' sol' |
    la'4. sib'8 do'' la' |
    re''4 re'' sib'8 sib' |
    la'4 la'8 sib' do'' re'' |
    mib''2 mib''8 mib'' |
    re''4 re''8 do'' sib' sib' |
    la'2 re''4 |
    sib' la'4. sol'8 |
    sol'2 r4 |
  }
  \tag #'vhaute-contre1 {
    \clef "vhaute-contre" sol'4 |
    fad'2 fad'4 |
    sol'4. sol'8 sol' sol' |
    mi'4 mi' mi' |
    do'4. fa'8 fa' fa' |
    fa'4 fa' fa'8 fa' |
    fa'4 fa'8 fa' fa' fa' |
    sol'2 sol'8 sol' |
    sol'4 sol'8 sol' sol' sol' |
    fad'2 la'4 |
    sol' fad'4. sol'8 |
    sol'2 r4 |
  }
  \tag #'vtaille1 {
    \clef "vtaille" re'4 |
    re'2 re'4 |
    re'4. re'8 re' re' |
    do'4 do' do' |
    do'4. do'8 do' do' |
    sib4 sib sib8 sib |
    do'4 do'8 do' do' do' |
    do'2 do'8 do' |
    sib4 sib8 do' re' re' |
    re'2 re'4 |
    re' re'4. re'8 |
    re'2 r4 |
  }
  \tag #'vbasse1 {
    \clef "vbasse" sol4 |
    re'2 re'4 |
    sol4. la8 si sol |
    do'4 do' do' |
    fa4. sol8 la fa |
    sib4 sib sib8 sib |
    fa4 fa8 sol la sib |
    do'2 do'8 do' |
    sol4 sol8 la sib sol |
    re'2 fad4 |
    sol re4. sol8 |
    sol2 r4 |
  }
  %% Chœur 2
  \tag #'vdessus2 {
    \clef "vdessus" r4 |
    r r re'' |
    si'2 re''4 |
    mi''4. fa''8 sol'' mi'' |
    fa''4 fa'' fa'' |
    fa''4. mib''8 re'' re'' |
    do''4 do'' fa''8 fa'' |
    mib''4 mib''8 fa'' sol'' sol'' |
    sol''4 re''8 do'' sib' sib' |
    la'2 re''4 |
    sib'4 la'4. sol'8 |
    sol'2 r4 |
  }
  \tag #'vhaute-contre2 {
    \clef "vhaute-contre" r4 |
    r r la' |
    sol'2 sol'4 |
    sol'4. sol'8 sol' sol' |
    fa'4 fa' fa' |
    re'4. mib'8 fa' sol' |
    la'4 la' la'8 la' |
    sol'4 sol'8 sol' sol' sol' |
    sol'4 sol'8 sol' sol'8 sol' |
    fad'2 la'4 |
    sol' fad'4. sol'8 |
    sol'2 r4 |
  }
  \tag #'vtaille2 {
    \clef "vtaille" r4 |
    r r re' |
    re'2 re'4 |
    do'4. do'8 do' do' |
    do'4 do' la |
    sib4. sib8 sib sib |
    do'4 do' do'8 do' |
    do'4 sol8 sol sol la |
    sib4 sib8 do' re' re' |
    re'2 la4 |
    sib re' la |
    sib2 r4 |
  }
  \tag #'vbasse2 {
    \clef "vbasse" r4 |
    r r re |
    sol2 sol4 |
    do4. re8 mi do |
    fa4 fa fa |
    sib,4. do8 re sib, |
    fa4 fa fa8 fa |
    do4 do8 re mib do |
    sol4 sol8 la sib sol |
    re'2 fad4 |
    sol re4. sol8 |
    sol2 r4 |
  }
>>
R2.*18 |
r4 r
