\clef "taille" re'4 |
re'2 la'4 |
sol'4. sol'8 sol' sol' |
sol'4. fa'8 mi' sol' |
fa'4 fa' fa' |
fa'4. fa'8 fa' fa' |
fa'4 fa'4. fa'8 |
mib'4 mib'8 fa' sol' sol' |
sol'4 sol'4. sol'8 |
la'2 re'4 |
re' re'4. re'8 |
re'4 sol'4. sol'8 |
sol'8 fa' sol' mi' la' sol' |
la' fa' mi'4. fa'8 |
fa'2 r4 |
R2.*2 |
r4 r8 re' mi' fad' |
sol'4 r r |
mi'8 mi' fa' sol' la' sib' |
do'' sib' la'4 sol'~ |
sol'8 la' fad'4. sol'8 |
sol'2 r4 |
R2.*3 |
r4 sol'4. sol'8 |
sol'8 mi' fa' sol' la' sib' |
do'' sib' la'4 sol'~ |
sol'8 la' fad'4. sol'8 |
sol'2
