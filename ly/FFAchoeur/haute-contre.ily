\clef "haute-contre" sib'4 |
la'2 la'4 |
si'4. do''8 re'' si' |
do''4 do'' do'' |
la'4. sib'8 do'' la' |
sib'4 sib' sib' |
la' la'4. la'8 |
sol'4 sol'4. la'8 |
sib'4 sib'4. do''8 |
la'2 la'4 |
sol' fad'4. sol'8 |
sol'4 sib'4. sib'8 |
mi'4 do''4. do''8 |
do''4 do''4.\trill sib'8 |
la'2 r4 |
R2.*2 |
r4 r8 re'' re'' re'' |
re''4 r r |
sol'8 sol' la' sib' do'' sib' |
la' sol' re''4. re''8 |
mib''4 re''4. do''8 |
si'2 r4 |
R2.*3 |
r4 re''8 do'' sib' la' |
sol' sol' la' sib' do'' sib' |
la' sol' re''4. re''8 |
mib''4 re''4. do''8 |
si'2
