\clef "dessus" sol''4 |
fad''2 fad''4 |
sol''2 re''4 |
mi''4. fa''8 sol'' mi'' |
fa''4 fa'' do'' |
re''4. mib''8 fa'' re'' |
do''4 do''8 sib' do''8. re''16 |
mib''4 mib''4. fa''8 |
re''4 sol''8 fad'' sol''8. la''16 |
fad''2 re''8. do''16 |
sib'4 la'4.\trill sol'8 |
sol'4 sol''8 la'' sol'' fa'' |
mi'' re'' mi'' do'' fa'' mi'' |
fa'' sol'' sol''4.\trill fa''8 |
fa''4. <>^"flûtes seules" <<
  { la''8 sol''8. fa''16 |
    mi''8\trill re'' mi'' fa'' sol'' mi'' |
    fa''4 mi''4.\trill re''8 |
    re''4. } \\
  { fa''8 mi''8. re''16 |
    dod''8 si' dod'' re'' mi'' dod'' |
    re''4 dod''4. re''8 |
    re''4. }
>> <>^"tous" fad''8 sol'' la'' |
sib''4. <>^"flûtes" <<
  { re''8 re''8. sol''16 | mi''4\trill } \\
  { sib'8 sib'8. sib'16 | sol'4 }
>> <>^"tous" do'''8 sib'' la'' sol'' |
fad'' sol'' la'' re'' sib''4 |
sib''8 la'' la''4.\trill sol''8 |
sol''4. <>^"flûtes" <<
  { re''8 re''8. re''16 |
    mib''4. mi''8 mi''8. mi''16 |
    fad''4. fad''8 sol''4~ |
    sol''8 la'' fad''4. sol''8 |
    sol''4 } \\
  { si'8 si'8. si'16 |
    do''4. do''8 do''8. do''16 |
    do''4. sib'16 la' sib'4~ |
    sib'8 do'' la'4. sol'8 |
    sol'4 }
>> <>^"tous" sib''8 la'' sol'' fa'' |
mi''4\trill do'''8 sib'' la'' sol'' |
fad'' sol'' la'' re'' sib''4~ |
sib''8 la'' la''4.\trill sol''8 |
sol''2
