\piecePartSpecs
#`((dessus #:music , #{ s4 s2.*6\break s2.*6\break s2.*6\break s2.*6\break #})
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue #:system-count 4)
   (silence #:on-the-fly-markup , #{ \markup\tacet#60 #}))
