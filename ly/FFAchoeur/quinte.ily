\clef "quinte" sol4 |
re'2 re'4 |
re'4. do'8 si re' |
do'4 do' do' |
do' do' do' |
sib sib sib |
do' do' do' |
do' do'4. do'8 |
re'4 re'4. re'8 |
re'2 la4 |
sib re' sol |
sol re'4. re'8 |
do'4 do'4. do'8 |
do'4 do'4. do'8 |
do'2 r4 |
R2.*2 |
r4 r8 la sib do' |
sib4 r r |
do'4. re'8 re'4~ |
re'8 re' re'4 sib |
sol re'4. re'8 |
re'2 r4 |
R2.*3 |
r4 re'4. re'8 |
do'4. re'8 re'4~ |
re'8 re' re'4 sib |
sol re'4. re'8 |
re'2
