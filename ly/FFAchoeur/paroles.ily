\tag #'(vdessus1 vhaute-contre1 vtaille1 vbasse1) {
  Chan -- tons, chan -- tons, fai -- sons en -- ten -- dre
  chan -- tons, fai -- sons en -- ten -- dre
  nos chan -- sons jus -- ques dans les cieux,
  nos chan -- sons jus -- ques dans les cieux,
  jus -- ques dans les cieux.
}
\tag #'(vdessus2 vhaute-contre2 vtaille2 vbasse2) {
  Chan -- tons, chan -- tons, fai -- sons en -- ten -- dre
  chan -- tons, fai -- sons en -- ten -- dre
  nos chan -- sons jus -- ques dans les cieux,
  jus -- ques dans les cieux,
  jus -- ques dans les cieux.
}
