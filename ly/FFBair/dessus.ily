\clef "dessus" sib'4. la'8 sib'4 do'' |
re''4. mib''8 do''2\trill |
sib'8 la' sib' do'' sib'4 la' |
sol'4. do''8 do''4.\trill sib'8 |
la'1\trill |
la'2. re''4 |
sib' sol' mib'' mib''8 re'' |
do''4\trill do'' r fa''8 fa'' |
sol'' fa'' mib'' re'' do''4.\trill sib'8 |
sib'2. sib''8 la'' |
sol''4.\trill fa''8 fa''4 sol''8 re'' |
mib''2 r8 mib'' re''\trill do'' |
re''2 r8 do'' sib'\trill la' |
sib'4. do''8 la'4.\trill sol'8 |
sol'2. sib''8 la'' |
sol''4. fa''8 fa''4 sol''8 re'' |
mib''2 r8 mib'' re'' do'' |
re''2 r8 do'' sib' la' |
sib'4. do''8 la'4. sol'8 |
sol'2. re''4 |
sol'1 |
