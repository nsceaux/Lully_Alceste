\clef "basse" sol4. fad8 sol4 la |
sib2 fad |
sol sol4 re |
mib2 do |
re4. mi8 fad4 re |
re4. mi8 fad4 re |
sol2 do4 do |
fa4 fa8 mib re2 |
mib2 fa4 fa, |
sib,2 sib |
si2. si4 |
do' do'8 sib la2 |
sib8 sib la sol fad2 |
sol4 do re re, |
sol,2 sol |
si,1 |
do4 do8 sib, la,2 |
sib,8 sib, la, sol, fad,2 |
sol,4 do, re,2 |
sol, sol4 re |
sol,1 |
