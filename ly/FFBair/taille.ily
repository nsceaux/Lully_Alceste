\clef "taille" re'4. re'8 re'4 do' |
sib re'2 re'4 |
re'8 do' re' mib' re'4 re' |
sib mib' mib'2 |
re'1 |
re'2. re'4 |
re' sib do'4. do'8 |
do'4 fa' fa'2 |
mib'4 sol' fa'4. fa'8 |
fa'2. fa'4 |
sol'2. sol'4 |
sol'2 fa'4. fa'8 |
fa'4. sol'8 la'4 re' |
re' mib' re'4. do'8 |
si2. sol'4 |
sol'2. sol'4 |
sol'2 fa'4. fa'8 |
fa'4. sol'8 la'4 re' |
re' mib' re'4. do'8 |
si2. re'4 |
si1 |
