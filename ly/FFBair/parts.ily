\piecePartSpecs
#`((dessus)
   (haute-contre #:music , #{ s1*7\break s1*7\break #})
   (taille)
   (quinte #:system-count 3)
   (basse)
   (basse-continue #:system-count 3)
   (silence #:on-the-fly-markup , #{ \markup\tacet#38 #}))
