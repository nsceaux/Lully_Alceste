\clef "quinte" re'4. do'8 sib4 fa' |
fa' sib la4. la8 |
sol4 sol8 la sib do' re'4 |
sol do' la4. la8 |
la1 |
la2. la4 |
sol4 re' do' do'8 sib |
la4. la8 sib2 |
sol4 do' do'4. re'8 |
re'2. re'4 |
re'2. fa'4 |
do'2 do'4 fa' |
fa'2 re'4 re' |
sol2 re'4. re'8 |
re'2. re'4 |
re'2. re'4 |
do'2. fa'4 |
re'2 re'4 re' |
sol2 re'4. re'8 |
re'2. la4 |
re'1 |
