\clef "haute-contre" sol'4. la'8 sol'4 fa' |
fa'4. sol'8 la'2 |
re' sol'4 fad' |
sol'4. la'8 la'4.\trill sol'8 |
fad'1 |
fad'2. fad'4 |
sol'2 sol'4. sol'8 |
la'4 la' r re'' |
sib' do''8 sib' la'4. sib'8 |
sib'2. re''4 |
re''2. re''4 |
sol'2 do''4. do''8 |
sib'4. sib'8 la'4. la'8 |
sol'4. la'8 fad'4. sol'8 |
sol'2. re''4 |
re''2. re''4 |
sol'2 do''4. do''8 |
sib'4. sib'8 la'4. la'8 sol'4. la'8 fad'4. sol'8 |
sol'2. fad'4 |
sol'1 |
