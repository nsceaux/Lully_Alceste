\clef "basse" sol,2. sol4 |
mi2. do4 |
fa fa fa fa |
re1 |
mi4 mi8 mi mi4 mi, |
la,1 |
re4 re8 re re4 re, |
sol, sol mi2 |
fa sol4 sol8 sol, |
do2~ do8 re do si, |
\once\set Staff.whichBar = "|"
\custosNote la,4
