\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'lychas \includeNotes "voix"
    >> \keepWithTag #'lychas \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'alcide \includeNotes "voix"
    >> \keepWithTag #'alcide \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'straton \includeNotes "voix"
    >> \keepWithTag #'straton \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*4\break s1*3\break }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}