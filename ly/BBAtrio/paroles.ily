\tag #'(alcide basse) {
  - co -- re !
}
\tag #'(lychas alcide) {
  L’a -- mour a bien des maux, mais le plus grand de tous
  c’est le tour -- ment d’es -- tre ja -- loux,
  d’es -- tre ja -- loux,
  c’est le tour -- ment d’es -- tre ja -- loux.
}
\tag #'straton {
  L’a -- mour a bien des maux, mais le plus grand de tous
   mais le plus grand de tous
  c’est le tour -- ment d’es -- tre ja -- loux.
}