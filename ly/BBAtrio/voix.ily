<<
  %% Lychas
  \tag #'(lychas basse) {
    <<
      \tag #'basse { s2 \ffclef "vhaute-contre" <>^\markup\character Lychas }
      \tag #'lychas { \clef "vhaute-contre" <>^\markup\character Lychas r2 }
    >> r4 <>^"viste" re' |
    mi'4. mi'8 mi'4. mi'8 |
    do'1 |
    fa'4 fa'8 mi' re'4. do'8 |
    si2 mi'4 mi'8 si |
    dod'2 dod'4 dod'8 dod' |
    re'2 re'4 re'8 la |
    si2 sol'4 sol'8 sol' |
    fa'2\trill re'4\trill mi'8 fa' |
    mi'2\trill r |
  }
  %% Alcide
  \tag #'(alcide basse) {
    <>^\markup\character Alcide
    \clef "vbasse-taille" sol4 sol
    \tag #'alcide {
      r4 <>^"viste" si |
      do'4. do'8 do'4. do'8 |
      la1 |
      re'4 re'8 do' si4. la8 |
      sold2 sold4 sold8 sold |
      la2 la4 la8 sol |
      fad2 fad4 fad8 fad |
      sol2 do'4 do'8 sol |
      la2 si4\trill si8 do' |
      do'2 r |
    }
  }
  %% Straton
  \tag #'straton {
    \clef "vbasse" <>^\markup\character Straton
    R1 |
    r2 r4 <>^"viste" do |
    fa4. fa8 fa4. fa8 |
    re1 |
    mi4 mi8 mi mi4. mi8 |
    la,1 |
    re4 re8 re re4. re8 |
    sol,4 sol mi4. mi8 |
    fa2 sol4 sol8 sol, |
    do2 r |
  }
>>
