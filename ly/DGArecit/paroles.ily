\tag #'(recit basse) {
  - vre ?
  Tu me vois ar -- res -- té sur le point de par -- tir
  par les tris -- tes cla -- meurs qu’on en -- tend re -- ten -- tir.

  Al -- ces -- te meurt pour moy par une a -- mour ex -- tres -- me,
  je ne re -- ver -- ray plus les yeux qui m’ont char -- mé :
  He -- las ! he -- las ! j’ay per -- du ce que j’ai -- me
  pour a -- voir es -- té trop ai -- mé.
  He -- las ! he -- las ! j’ay per -- du ce que j’ai -- me
  pour a -- voir es -- té trop ai -- mé.
  
  J’aime Al -- ceste, il est temps de ne m’en plus de -- fen -- dre ;
  el -- le meurt, ton a -- mour n’a plus rien à pre -- ten -- dre ;
  Ad -- me -- te, ce -- de moy la beau -- té que tu perds :
  au pa -- lais de Plu -- ton j’en -- tre -- prends de des -- cen -- dre :
  j’i -- ray jus -- qu’au fonds des en -- fers
  for -- cer la mort à me la ren -- dre.
  
  Je ver -- rois en -- cor ses beaux yeux ?
  Al -- lez, Al -- cide, al -- lez, re -- ve -- nez glo -- ri -- eux,
  ob -- te -- nez qu’Al -- ces -- te vous sui -- ve :
  le fils du plus puis -- sant des dieux
  est plus di -- gne que moy du bien dont on me pri -- ve.
  Al -- lez, al -- lez, ne tar -- dez pas,
  ar -- ra -- chez Al -- ceste au tres -- pas,
  et ra -- me -- nez au jour son om -- bre fu -- gi -- ti -- ve ;
  qu’el -- le vi -- ve pour vous a -- vec tous ses ap -- pas,
  Ad -- mete est trop heu -- reux pour -- veu qu’Al -- ces -- te vi -- ve.
}
\tag #'(cephise pheres cleante basse) {
  Al -- lez, al -- lez, ne tar -- dez pas,
  \tag #'(cephise pheres basse) { ne tar -- dez pas, }
  ar -- ra -- chez Al -- ceste au Tres -- pas.
  Al -- lez, al -- lez, ne tar -- dez pas,
  \tag #'(cephise cleante basse) { ne tar -- dez pas, }
  ar -- ra -- chez Al -- ceste au Tres -- pas.
}
