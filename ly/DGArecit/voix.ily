%% récit
<<
  \tag #'(recit basse) {
    \clef "vtaille" sol4
    \clef "vbasse-taille" <>^\markup\character Alcide
    r4 r8 si16 re' sol8 sol16 sol |
    re8 re16 re sol8 sol16 la fad4\trill r8 la16 la |
    re'8 re'16 re' re'4 r8 sol16 sol do'8 do'16 mi' |
    la4
    \ffclef "vtaille" <>^\markup\character Admete
    r8 re' si8. si16 si8 sol |
    la8. la16 la8. si16 do'8. re'16 |
    si4\trill si8. sol16 sol8 sol la si |
    do'4. mi'8 la8. sol16 sol8[ fad16] sol |
    fad4.\trill re'8 la si |
    do'4 do'8. re'16 mi'4 re'8.\trill do'16 |
    si2\trill si |
    sold8 sold sold sold la4 la8 sold |
    la8. mi'16 do'8 mi' la4 re'8. re'16 |
    re'4 dod'8. re'16 re'4 re' |
    la8. la16 la8 si sol4 sol8 fad |
    sol2
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    r8 si16 re' sol8 sol16 sol |
    re8. re16 re re re mi fad8 fad r la16 la |
    re4 r8 la16 la si8 si16 si sold8 sold16 la |
    la8 la r mi' la8. la16 mi8. fad16 |
    sol8 sol16 sol sol8 sol16 fad fad\trill r la la re'8 re'16 re' |
    fad8\trill fad16 fad fad8 sol16 la re4 re8 r16 re' |
    sol8 sol16 sol re8 re16 sol do8 r16 sol do'8. mi'16 |
    la8. la16 si8. do'16 si4( la)\trill |
    sol4
    \ffclef "vtaille" <>^\markup\character Admete
    r8 si16 si fad8. sold16 la8 la16 sold |
    sold2 r8 mi' si8. mi'16 |
    dod'8. la16 re'8 re'16 re' re'8 re'16 dod' |
    re'4 r8 la16 si do'8 do' do' do'16 si |
    si8\trill si r sol si16 si dod' re' mi'8. fad'16 |
    red'4 r8 si16 si mi'8 mi'16 si |
    do'4 r8 la fad4\trill fad8 fad16 sol |
    mi8\trill mi r si do'8. do'16 |
    la8\trill r16 re' re'8. re'16 sol4 r8 do'16 mi' |
    la8. la16 re'8 re'16 la si8. si16 si si do' re' |
    mi'8. mi'16 sold8. sold16 sold8. la16 |
    la8 la r do'16 do' do'8 do'16 si |
    si8.\trill si16 si8. la16 sol8[ fad16] sol |
    fad4\trill r8 la la8. la16 si8. do'16 |
    re'8 r16 re' mi'8. la16 si8. do'16 la4\trill |
  }
  \tag #'cephise \clef "vdessus"
  \tag #'pheres \clef "vtaille"
  \tag #'cleante \clef "vbasse"
  \tag #'(cephise pheres cleante) {
    R1*4 R2. R1*2 R2. R1*16 R2. R1*2 R2. R1 R2. R1*2 R2.*3 R1*2
  }
>>
%% trio
<<
  \tag #'recit {
    sol4 r4 r2 | R1 R2. R1*2 R2. R1
  }
  \tag #'basse {
    sol8
  }
  \tag #'(cephise basse) {
    <<
      \tag #'basse { s8 \ffclef "vdessus" }
      \tag #'cephise { r8 }
    >> <>^\markup\character Céphise
    si' si'8. do''16 re''8. re''16 re''8. mi''16 |
    fad''8. fad''16 fad''8. fad''16 sol''4 do''8. do''16 |
    do''8. re''16 si'4 la'8.\trill sol'16 |
    fad'8.\trill fad'16 fad'8. sol'16 la'8. la'16 la'8. si'16 |
    do''8. do''16 si'8. la'16 sol'4 si'8. si'16 |
    la'8.\trill sol'16 sol'4 fad'8.\trill sol'16 |
    sol'2 r |
  }
  \tag #'pheres {
    r8 <>^\markup\character Pheres
    sol sol8. la16 si8. si16 si8. dod'16 |
    re'8. do'16 do'8. do'16 si4 mi'8. mi'16 |
    re'8. re'16 re'4 re'8. dod'16 |
    re'2 r8 do' do'8. re'16 |
    mi'8. mi'16 re'8. do'16 si4\trill re'8. re'16 |
    do'8. si16 la4\trill la8. re'16 |
    si2\trill r |
  }
  \tag #'cleante {
    r2 r8 <>^\markup\character Cleante sol fad8. mi16 |
    re8. re16 re8. re16 mi4 mi8. mi16 |
    fad8. fad16 sol4 fad8. mi16 |
    re8. re16 re8. mi16 fa8. fa16 mi8. re16 |
    do8. do16 re8. re16 mi4 si,8. si,16 |
    do8. do16 re4 re8. re16 |
    sol,2 r |
  }
>>