\clef "basse" sol,1~ |
sol,2 re |
si, mi4 do |
re2 sol |
fad2 re4 |
sol2~ sol8 fa mi re |
do2 dod |
re do8 si, |
la,4. si,8 do4 re |
mi1 |
re2 do8 la, mi mi, |
la,2 fad,4 fa, |
mi,2 re,4 re8 mi |
fa4. re8 mi do re re, |
sol,1~ |
sol,2 re |
re si, |
la,1 |
mi4 dod re2~ |
re4 do si,2~ |
si,2 do |
re4 sol, re,2 |
sol,4 sol red2 |
mi1 |
la4 fad mi |
re2 la, |
mi~ mi8 re do4 |
si,2 sold,4 |
la,2 si, |
mi,4 mi la, |
re2 mi | \allowPageTurn
fad sol4~ sol16 fa mi re |
do4 si,2 |
la,4 la fad |
sol4~ sol8 fad mi4 |
re2~ re4 re8 do |
si,4 do sol, re8 re, |
sol,2 r8 sol fad8. mi16 |
re4. re8 mi4. mi8 |
fad4 sol fad8. mi16 |
re8. re16 re8. mi16 fa8. fa16 mi8. re16 |
do4 re mi si, |
do re re, |
sol,2 sol8 fa mi re |
