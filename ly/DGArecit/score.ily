\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'cephise \includeNotes "voix"
    >> \keepWithTag #'cephise \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'pheres \includeNotes "voix"
    >> \keepWithTag #'pheres \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'cleante \includeNotes "voix"
    >> \keepWithTag #'cleante \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s1*2\break s2. s1 s2 \bar "" \break s2 s2. s1\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\break s1 s2 \bar "" \break
        s2 s1\break s1 s2 \bar "" \break s2 s1\pageBreak
        s1*2\break s1*2\break s2. s1\break s1 s2.\break
        s1 s2.\break s1*2\pageBreak
        s2.*2 s4 \bar "" \break s2 s1 s2 \bar "" \break
        s2 s1 s2 \bar "" \break s2 s2. s2 \bar "" \pageBreak
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
