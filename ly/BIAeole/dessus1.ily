\clef "dessus" mi''8 re'' do'' re'' mi'' fa'' |
re''\trill do'' re'' mi'' re'' mi'' |
do'' si' la' si' do'' re'' |
mi'' fa'' mi'' re'' do'' si' |
la' sol' la' si' do'' re'' |
si' la' si' do'' re'' si' |
do'' re'' mi'' fa'' sol'' mi'' |
fa'' mi'' fa'' sol'' la'' si'' |
sold'' fad'' sold'' la'' si'' sold'' |
la'' si'' sold''4.\trill la''8 |
la''2. |
R1*4 R2.*3 R1*3 |
R2. |
<>_\markup\whiteout Violons r4 la'' la'' |
la'' sol''4. sol''8 |
sol''4. fa''8 mi''4~ |
mi'' re''4. do''8 |
si'2 dod''8 re'' |
dod''4. mi''8 fa'' sol'' |
la'' sib'' la'' sol'' fa'' mi'' |
re''4. re''8 mi'' fa'' |
sol'' fa'' mi'' re'' mi'' fa'' |
re'' do'' si' do'' re'' mi'' |
fa''4. mi''8 fa'' sol'' |
mi''4 la''4. si''8 |
sold''4 mi'' mi'' |
mi'' re''4. re''8 |
re''8 mi'' re'' do'' re'' si' |
do'' si' do'' re'' mi'' do'' |
re'' do'' re'' si' do'' re'' |
mi'' re'' do'' si' do'' re'' |
si'4 si' do'' |
la' la' si' |
sold'2 sold'4 |
r do'' do'' |
la' la'4. la'8 |
la'4. si'8 sold'4 |
la' do'' do'' |
do'' do'' re'' |
si'8 do'' si'4.\trill la'8 |
la'2. |
