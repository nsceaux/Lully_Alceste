\clef "dessus" do''8 si' la' si' do'' re'' |
si' la' si' do'' si' do'' |
la' si' do'' re'' mi'' fa'' |
sol'' la'' sol'' fa'' mi'' re'' |
do'' si' do'' re'' mi'' fa'' |
re'' do'' re'' mi'' fa'' sol'' |
mi'' re'' do'' re'' do'' si' |
la' sol' la' si' do'' re'' |
si' la' si' do'' re'' si' |
do'' re'' si'4.\trill la'8 |
la'2. |
R1*4 R2.*3 R1*3 |
R2. |
r4 do'' do'' |
re''8 do'' re'' mi'' fa'' re'' |
mi'' re'' mi'' fa'' sol'' mi'' |
fa'' mi'' fa'' sol'' la'' si'' |
sold''2 mi''8. mi''16 |
mi''4. dod''8 re'' mi'' |
fa'' sol'' fa'' mi'' re'' do'' |
si'4. si'8 do'' re'' |
mi'' re'' do'' si' do'' re'' |
si' la' sol' la' si' dod'' |
re''4 la'8 sol' la' si' |
do''4. si'8 do'' re'' |
mi''4 sold'8 la' si' sold' |
la' sold'? la' si' do'' la' |
si'2 mi''8. mi''16 |
mi''4 la''4. la''8 |
la''4 sol''4. sol''8 |
sol''8 fa'' mi'' re'' mi'' fa'' |
re''4 re'' mi'' |
do'' do'' re'' |
si'2 si'4 |
r mi'' mi'' |
do'' do'' re'' |
si'8 la' si' do'' re'' mi'' |
do''4 mi'' mi'' |
la'' la''4. la''8 |
la''4. si''8 sold''4 |
la''2. |
