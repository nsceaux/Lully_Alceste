\key la \minor
\beginMark "Ritournelle"
\digitTime\time 3/4 \midiTempo#160 s2.*11 \bar "||"
\time 4/4 \midiTempo#80 s1*4
\digitTime\time 3/4 s2.*3
\time 4/4 s1*3
\digitTime\time 3/4 \midiTempo#160 s2.*29 \bar "|."
