\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiri } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s2.*11\break s1*4 s2.*3 s1*3 s2.\break \newSpacingSection
      }
      \origLayout {
        s2.*5\break s2.*6\break
        s1 s2 \bar "" \break s2 s1 s2 \bar "" \break s2 s2.*2\pageBreak
        s2. s1\break s1*2\break s2.*6\break s2.*6\pageBreak
        s2.*5\break s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
