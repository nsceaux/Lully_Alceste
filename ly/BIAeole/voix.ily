\clef "vbasse-taille" R2.*11 |
\ffclef "vbasse-taille" <>^\markup\character Eole
r8 la mi'8. mi'16 sold8.\trill sold16 sold8. si16 |
mi8. mi'16 do'8 do' la8.\trill la16 la8. la16 |
fad8\trill fad r la re'8. la16 la la si do' |
si8\trill si r si do'16 si la sol fad8. mi16 |
red4 r8 si si8. si16 |
mi'4 r8 la16 sol fad8 fad16 si |
mi8 mi r4 r16 si si si |
sold4 r16 mi' mi' mi' dod'4 la8 si16 dod' |
re'4. re'8 si16 sol sol sol re8. sol16 |
do8 do r do' la16 la la la fa8. la16 |
mi2 mi4 |
r la la |
si2\trill si4 |
do' do' do' |
re'8[\melisma do' re' mi' fa' re'] |
mi'[ re' mi' fa' mi' mi] |
la[ sib la sol fa mi] |
re[ do re mi fa re] |
sol[ la sol fa mi re]( |
do2)\melismaEnd do4 |
r sol sol |
re re re |
la4. sol8 fa4 |
mi mi mi |
fad fad fad |
sold2 sold4 |
la la la |
si8[\melisma la si sol la si] |
do'[ si do' re' do' do] |
sol[ la sol fa sol mi] |
fa[ sol fa mi fa re]( |
mi2)\melismaEnd mi4 |
r do do |
fa fa re |
mi4. re8 mi4 |
la, la la |
fa fa re |
mi4. re8 mi4 |
la,2. |
