\piecePartSpecs
#`((dessus #:score-template "score-dessus2")
   (basse)
   (basse-continue
    #:music , #{ s2.*6\break s2.*5 s1\break s1*3\break s2.*3 s1\break
s1*2 s2.*2\break s2.*6\break s2.*7\break s2.*7\break #}
    #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#50 #}))
