\clef "basse" la,2 la4~ |
la sold2 |
la la4 |
mi2 mi4 |
fa2 do4 |
sol2 sol,4 |
do2 do4 |
re2 la,4 |
mi2. |
la,4 mi,2 |
la,2. |
<<
  \tag #'basse {
    R1*4 R2.*3 R1*3 R2. |
    r4 la la |
  }
  \tag #'basse-continue {
    la,2 si, |
    do dod |
    re fad, |
    sol, la, |
    si,2. |
    sol,4 la, si, |
    mi2. |
    mi,2 la, |
    fad, sol, |
    do fa4 re |
    mi2 mi4 |
    la,2 la4 |
  }
>>
si2 si4 |
do' do' do' |
re'8 do' re' mi' fa' re' |
mi' re' mi' fa' mi' mi |
la sib la sol fa mi |
re do re mi fa re |
sol la sol fa mi re |
do2 do4 |
r sol sol |
re re re |
la4. sol8 fa4 |
mi mi mi |
fad fad fad |
sold2 sold4 |
la la la |
si8 la si sol la si |
do' si do' re' do' do |
sol la sol fa sol mi |
fa sol fa mi fa re |
mi2 mi4 |
r do do |
fa fa re |
mi4. re8 mi4 |
la, la la |
fa fa re |
mi4. re8 mi4 |
la,2. |
