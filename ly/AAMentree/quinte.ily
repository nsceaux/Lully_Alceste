\clef "quinte" sol4 sol sol |
sol si sol |
sol sol do' |
re'2 re'4 |
sol sol sol |
sol si sol |
sol sol do' |
re'2. |
sol4 sol sol |
sol do' do' |
si sol sol |
sol sol do' |
re' re' re' |
mi' do' mi' |
re' re' re' |
do' sol sol |
sol re' sol |
sol sol sol |
sol sol8 sol sol4 |
sol2. |
do'4 do' do' |
re'2 re'4 |
do' la2 |
si si4 |
R2.*5 |
sol4 sol sol |
sol do' do' |
si sol sol |
sol sol do' |
re' re' re' |
mi' do' mi' |
re' re' re' |
do' sol sol |
sol re' sol |
sol sol sol |
sol sol8 sol sol4 |
sol2. |
sol |
