\clef "taille" mi'4 do' mi' |
re' re' re' |
do'8 re' mi' re' do'4 |
si2 si4 |
mi' do' mi' |
re' re' re' |
do'8 re' mi' re' do'4 |
si2. |
re'4 re' re' |
do'8 re' mi' re' do'4 |
re' si8 do' re'4 |
do'4 sol' sol' |
sol' sol' si |
do' sol' sol' |
sol' sol'8 fa' mi' re' |
mi' re' mi' fa' sol'4 |
sol' sol' si |
do' mi' mi' |
re' sol'8 fa' mi' re' |
mi'2. |
mi'8 re' mi' fa' sol'4 |
sol'2 sol'4 |
mi' la'2 |
re' re'4 |
R2.*5 |
re'4 re' re' |
do'8 re' mi' fa' mi'4 |
re' si8 do' re'4 |
do' sol' sol' |
sol' sol' si |
do' sol' sol' |
sol' sol'8 fa' mi' re' |
mi' re' mi' fa' sol'4 |
sol' sol' si |
do' mi' mi' |
re' sol'8 fa' mi' re' |
mi'2. |
mi' |
