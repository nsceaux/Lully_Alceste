\clef "haute-contre" do''4 sol' do'' |
si' si' si' |
do'' do'' sol' |
sol'2 sol'4 |
do'' sol' do'' |
si' si' si' |
do'' do'' sol' |
sol'2. |
si'4 si' si' |
do'' sol' sol' |
sol' sol'8 fa' mi' re' |
mi'4 mi' do'' |
si' si'8 do'' re''4 |
do'' re''8 do'' si' la' |
si'4 sol' sol' |
sol' sol' do'' |
si' si'8 do'' re''4 |
do'' re''8 do'' si' la' |
si'4 sol' sol' |
sol'2. |
sol'4 sol' sol'8 la' |
si'2 si'4 |
do''2 do''4 |
si'2 si'4 |
R2.*5 |
si'4 si' si' |
do'' sol' sol' |
sol' sol'8 fa' mi' re' |
mi'4 mi' do'' |
si' si'8 do'' re''4 |
do'' re''8 do'' si' la' |
si'4 sol' sol' |
sol' sol' do'' |
si' si'8 do'' re''4 |
do'' re''8 do'' si' la' |
si'4 sol' sol' |
sol'2. |
sol' |
