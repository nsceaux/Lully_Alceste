\clef "dessus" <>^"[avec Trompettes]" sol''4 mi'' do'' |
re'' re'' sol'' |
mi'' do''8 re'' mi''4 |
re''2\trill sol'4 |
sol''4 mi'' do'' |
re'' re'' sol'' |
mi'' do''8 re'' mi''4 |
re''2.\trill |
sol''4 sol''8 la'' sol'' fa'' |
mi'' fa'' mi'' re'' do''4 |
re'' mi''8 re'' do'' si' |
do''4 do''8 re'' mi''4 |
re''\trill re''8 mi'' fa''4 |
mi'' fa''8 mi'' re'' do'' |
re''4 mi''8 re'' do'' si' |
do'' si' do'' re'' mi''4 |
re'' re''8 mi'' fa''4 |
mi'' fa''8 mi'' re'' do'' |
re''4 mi''8 re'' do'' si' |
do''2 do''4 |
do''8 si' do'' re'' mi'' fa'' |
sol'' fa'' sol'' la'' sol''4 |
la''8 sol'' fad'' sol'' la'' fad'' |
sol''2 sol''4 |
<>^"Hautbois"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { re''4 re'' mi'' |
    re''\trill re'' mi'' |
    fa'' sol''8 fa'' mi'' re'' |
    mi''4 fa''8 mi'' re'' do'' |
    re''2\trill re''4 | }
  { si'4 si' do'' |
    si'\trill si' do'' |
    re'' mi''8 re'' do'' si' |
    do''4 re''8 do'' si' la' |
    si'2 si'4 | }
>>
<>^"[Tous]" sol''4 sol''8 la'' sol'' fa'' |
mi'' fa'' mi'' re'' do''4 |
re'' mi''8 re'' do'' si' |
do''4 do''8 re'' mi''4 |
re'' re''8 mi'' fa''4 |
mi'' fa''8 mi'' re'' do'' |
re''4 mi''8 re'' do'' si' |
do'' si' do'' re'' mi''4 |
re'' re''8 mi'' fa''4 |
mi''4 fa''8 mi'' re'' do'' |
re''4 mi''8 re'' do'' si' |
do''2 do''4 |
do''2. |
