\clef "basse" <>^"[Timbales]" \new CueVoice {
  do4 do do |
  sol, sol, sol, |
  do do do |
  sol,2 sol,4 |
  do4 do do |
  sol, sol, sol, |
  do do do |
  sol,2. |
  sol,4 sol,8 sol, sol,4 |
  do4 do8 do do4 |
  sol,4 sol,8 sol, sol,4 |
  do do8 do do4 |
  sol, sol,8 sol, sol,4 |
  do4 do8 do do4 |
  sol, sol,8 sol, sol,4 |
  do do8 do do4 |
  sol, sol,8 sol, sol,4 |
  do do8 do do4 |
  sol, sol,8 sol, sol,4 |
  do2. |
  do4 do do |
  sol,2 sol,4 |
  do2 do4 |
  sol,2. |
}
R2.*5
\new CueVoice {
  sol,4 sol,8 sol, sol,4 |
  do do8 do do4 |
  sol, sol,8 sol, sol,4 |
  do do8 do do4 |
  sol, sol,8 sol, sol,4 |
  do do8 do do4 |
  sol,4 sol,8 sol, sol,4 |
  do do8 do do4 |
  sol, sol,8 sol, sol,4 |
  do do8 do do4 |
  sol, sol,8 sol, sol,4 |
  do2 do4 |
  do2. |
}
