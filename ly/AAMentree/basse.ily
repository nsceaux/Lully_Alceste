\clef "basse" do4 do do |
sol sol sol |
do do do |
sol,2 sol,4 |
do4 do do |
sol sol sol |
do do do |
sol,2. |
sol,4 sol,8 sol, sol,4 |
do4 do8 do do4 |
sol,4 sol,8 sol, sol,4 |
do do8 do do4 |
sol sol8 sol sol4 |
do4 do8 do do4 |
sol, sol,8 sol, sol,4 |
do do8 do do4 |
sol sol8 sol sol4 |
do do8 do do4 |
sol, sol,8 sol, sol,4 |
do,2. |
do4 do do |
si,2 si,4 |
la,2 la,4 |
sol,2. |
sol2.~ |
sol~ |
sol~ |
sol~ |
sol |
sol,4 sol,8 sol, sol,4 |
do do8 do do4 |
sol, sol,8 sol, sol,4 |
do do8 do do4 |
sol sol8 sol sol4 |
do do8 do do4 |
sol,4 sol,8 sol, sol,4 |
do do8 do do4 |
sol sol8 sol sol4 |
do do8 do do4 |
sol, sol,8 sol, sol,4 |
do,2 do,4 |
do,2. |
