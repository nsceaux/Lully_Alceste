\clef "vbas-dessus" <>^\markup\character La Nymphe de la Marne
sol'4. la'8 si'4 |
do''2 la'4 |
si' do''8[ si'] la'[ sol'] |
re''2 si'4\trill |
r la' si' |
sol' mi'4. la'8 |
fad'2.\trill |
fad' |
la'4 si'4. do''8 |
si'4.\trill si'8 do''8. re''16 |
re''2\trill do''4 |
si'2\trill do''4 |
re''4. do''8 si'4 |
do''4. si'8[ la' si'] |
si'2\trill la'4 |
re''4. do''8 si'4 |
la' si'4.( do''8) |
la'2\trill sol'4 |
sol'2. |
sol' |
