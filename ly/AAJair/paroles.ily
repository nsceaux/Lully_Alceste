L’on -- de se pres -- se
d’al -- ler sans ces -- se
jus -- qu’au bout de son cours :  cours :
s’il faut qu’un cœur suive u -- ne pan -- te,
en est- il qui soit plus char -- man -- te
que le doux pen -- chant des a -- mours ?  - mours ?
