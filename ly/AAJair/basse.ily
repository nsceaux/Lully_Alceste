\clef "basse" sol4. fad8 sol4 |
do la, re |
sol fad mi |
re2 mi4 |
fad2 sol4 |
mi la la, |
re4. mi8 fad re |
re2. |
fad |
sol4 fa mi8 fa |
sol4 sol, do |
sol2 la4 |
si fad sol |
do re re, |
sol, sol re'8 do' |
si4. la8 sol4 |
fad sol do |
re re,2 |
sol,2 sol4 |
sol,2. |
