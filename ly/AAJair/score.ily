\score {
  \new ChoirStaff <<
    \new Staff \withRecit <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2.*5\break s2.*7\break }
    >>
  >>
  \layout { }
  \midi { }
}