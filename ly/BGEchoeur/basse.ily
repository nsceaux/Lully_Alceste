\clef "basse" <<
  \tag #'basse { r2 R1*3 r2 }
  \tag #'basse-continue {
    la4 sold |
    la la re4. re8 |
    mi4 mi do4. do8 |
    si,4 la, mi mi, |
    la, la,
  }
>> la4 sold |
la la, re4. re8 |
mi4 mi do4. do8 |
si,4 la, mi mi, |
la, la, <<
  \tag #'basse { r2 R1*11 r2 }
  \tag #'basse-continue {
    la4. la8 |
    mi4 mi fa2 |
    sol re4. do8 |
    si,4 do sol,2 |
    do4 do la,4. la,8 |
    re2 si, |
    mi la4 la, |
    re2 mi4 mi, |
    la,2 la,4. la,8 |
    re2 si, |
    mi la4 la, |
    re2 mi4 mi, |
    la,2
  }
>> la4. la8 |
mi4 mi fa4. fa8 |
sol2 re4. do8 |
si,4 do sol,4. sol,8 |
do4 do la,4. la,8 |
re4 re si,4. si,8 |
mi2 la4 la, |
re4. re8 mi4. mi8 |
la,2 la,4. la,8 |
re4 re si,4. si,8 |
mi2 la4 la, |
re4. re8 mi4. mi8 |
la,2
