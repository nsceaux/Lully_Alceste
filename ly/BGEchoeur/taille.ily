\clef "taille" r2 R1*3 r2
do'4 mi' |
do'2 re'4. re'8 |
si4 si do'4. do'8 |
re'4 mi' mi'4. mi'8 |
mi'4 mi'
r2 R1*11 r2
la'4. la'8 |
sol'4 sol' la'4. la'8 |
sol'2 la'4. la'8 |
sol'4 sol' sol'4. sol'8 |
sol'4 sol' la'4. mi'8 |
fa'4 fa' fad'4. fad'8 |
sold'2 la'4 mi' |
fa'4. fa'8 mi'4. mi'8 |
mi'2 la'4. mi'8 |
fa'4 fa' fad'4. fad'8 |
sold'2 la'4 mi' |
fa'4. fa'8 mi'4. mi'8 |
mi'2
