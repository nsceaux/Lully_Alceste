\clef "dessus" r2 |
R1*3 |
r2 la'4 si' |
do''4. re''8 si'4. la'8 |
sold'4 sold' mi''4. fa''8 |
re''4. do''8 si'4 do''8 re'' |
do''4 la' r2 |
R1*11 |
r2 mi''4. fa''8 |
sol''4 sol'' fa''4. mi''8 |
re''2 fa''4. fa''8 |
fa''4 mi'' re'' mi''8 fa'' |
mi''4 do'' mi''4. mi''8 |
mi''4. fa''8 re''4. re''8 |
re''2 do''4. do''8 |
do''4. si'8 si'4. la'8 |
la'2 mi''4. mi''8 |
mi''4. fa''8 re''4. re''8 |
re''2 do''4. do''8 |
do''4. si'8 si'4. la'8 |
la'2
