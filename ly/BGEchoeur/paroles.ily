\tag #'(cephise basse) {
  Jeu -- nes cœurs lais -- sez- vous pren -- dre
  le pe -- ril est grand d’at -- ten -- dre,
}
\tag #'(choeur basse) {
  Jeu -- nes cœurs lais -- sez- vous pren -- dre
  le pe -- ril est grand d’at -- ten -- dre,
}
\tag #'(cephise basse) {
  Vous per -- dez d’heu -- reux mo -- ments
  en cher -- chant à vous dé -- fen -- dre ;
  si l’a -- mour a des tour -- ments
  c’est la fau -- te des a -- mants.
  Si l’a -- mour a des tour -- ments
  c’est la fau -- te des a -- mants.
}
\tag #'(choeur basse) {
  Vous per -- dez d’heu -- reux mo -- ments
  en cher -- chant à vous dé -- fen -- dre ;
  si l’a -- mour a des tour -- ments
  c’est la fau -- te des a -- mants.
  Si l’a -- mour a des tour -- ments
  c’est la fau -- te des a -- mants.
}
