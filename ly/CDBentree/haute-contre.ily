\clef "haute-contre" sol'8 sol' sol' |
do'' do'' do'' |
mi'' mi'' do'' |
do'' sol' do'' |
si' sol'4 |
sol' sol'8 |
sol' sol'16 sol' sol'8 |
sol'4 sol'8 |
sol'4 si'16 si' |
si'8 si' si' |
do'' sol' sol' |
sol'4 sol'16 sol' |
sol'4. |
si'8 si'16 si' si'8 |
do'' sol' sol' |
sol'4 sol'8 |
la' la'16 si' do''8 |
re'' si'16 do'' re''8 |
sol' sol'16 la' si'8 |
la'4 la'8 |
si' si'16 la' si' do'' |
re''8 si' si' |
do'' do'' do'' |
si' si' si' |
do'' do'' do'' |
si' sol'4 |
si'8 si'16 si' si'8 |
\sugNotes { re'' re'' re'' } % mi'' mi'' mi'' |
do''4 si'8 |
do''16 re'' si'8.\trill do''16 |
do''4. |
