\clef "dessus" do''8 do''16 do'' do''8 |
mi'' do'' sol'' |
do''' sol'' mi'' |
do'' do''16 re'' mi'' fa'' |
sol''4 sol'8 |
si' si'16 do'' re''8 |
sol' sol''16 fa'' mi'' re'' |
do''8 re''16 mi'' fa'' mi'' |
re''8 sol' re''16 re'' |
re''8 sol''16 fa'' mi'' re'' |
do''8 re''16 mi'' fa'' mi'' |
re''8 sol' re''16 re'' |
re''4. |
sol''8 sol''16 sol'' sol''8 |
mi''\trill mi'' do'' |
re'' re'' sol' |
do'' do''16 re'' mi''8 |
re'' re''16 mi'' fa''8 |
mi''\trill mi''16 fad'' sol''8 |
la''16 sol'' fad''8.\trill( mi''32 fad'') |
sol''8 re''16 do'' re'' mi'' |
fa''8 re'' sol'' |
mi'' do'' mi'' |
re'' sol'' re'' |
mi'' do'' mi'' |
re''4 sol'8 |
sol'' sol''16 sol'' sol''8 |
sol'' sol'' sol'' |
do''' do''16 re'' mi'' re'' |
mi'' fa'' re''8.\trill do''16 |
do''4. |
