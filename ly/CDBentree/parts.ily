\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue)
   (timbales)
   (silence #:on-the-fly-markup , #{ \markup\tacet#44 #}))
