\clef "quinte" do'8 sol sol |
sol sol16 sol sol8 |
sol sol sol' |
mi' mi' do' |
re'4 re'8 |
re' re' re' |
re' re' re' |
mi' do' do' |
re'4 re'8 |
re' re' re' |
mi' do' do' |
re' re' re' |
si4. |
re'8 si re' |
do' do' do' |
sol si si |
do' do' do' |
si re' re' |
do' mi' re' |
mi' re'8. re'16 |
re'8 re' re' |
si re' re' |
do' do' do' |
re' re' re' |
do' sol sol |
sol re'4 |
re'8 re'16 re' sol8 |
sol \sug si %{do'%} sol |
sol4 sol8 |
sol sol4 |
sol4. |
