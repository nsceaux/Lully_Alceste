\clef "basse" do8 do do8 |
do4. |
do8 do do |
do' do' do' |
sol4 sol8 |
sol,8 sol,16 sol, sol,8 |
sol, sol, sol, |
do do16 do do8 |
sol4 sol8 |
sol, sol,16 sol, sol,8 |
do do16 do do8 |
sol, sol, sol, |
sol,4. |
sol,8 sol, sol, |
do4. |
si, |
la,8 la la |
si si si |
do'4 si8 |
do' re' re |
sol4. |
sol,8 sol,16 sol, sol,8 |
do mi do |
sol sol16 sol sol8 |
do do16 do do8 |
sol,4 sol,8 |
sol,8 sol,16 sol, sol,8 |
sol, sol, sol, |
do4 do8 |
do sol,4 |
do,4. |
