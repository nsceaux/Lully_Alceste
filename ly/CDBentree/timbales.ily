\clef "basse" do8 do16 do do8 |
do do do |
do do16 do do8 |
do do do |
sol,4 sol,8 |
sol, sol,16 sol, sol,8 |
sol, sol, sol, |
do do16 do do8 |
sol,4 sol,8 |
sol, sol,16 sol, sol,8 |
do do16 do do8 |
sol, sol, sol, |
sol,4. |
sol,8 sol,16 sol, sol,8 |
do4. |
R4.*5 |
sol,8 sol,16 sol, sol,8 |
sol,8 sol,16 sol, sol,8 |
do do16 do do8 |
sol, sol,16 sol, sol,8 |
do do16 do do8 |
sol,4 sol,8 |
sol, sol,16 sol, sol,8 |
sol, sol, sol, |
do do16 do do8 |
do sol,4 |
do4. |
