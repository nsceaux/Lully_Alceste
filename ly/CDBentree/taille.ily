\clef "taille" mi'8 mi' mi' |
mi' mi' mi' |
mi' sol' sol' |
sol' sol'16 fa' mi'8 |
re' si4 |
re'8 re'16 do' si8 |
si si si |
do' fa'16 mi' re' do' |
si8 si sol |
sol' sol' sol' |
mi' fa'16 mi' re' do' |
si8 si si |
si4. |
re'8 re'16 re' re'8 |
mi' do' mi' |
re'4 si8 |
mi'8 mi'16 mi' mi'8 |
sol' sol' sol' |
sol' sol' sol' |
sol'8 do'4 |
si8 si'8. si'16 |
sol'8 sol'16 sol' sol'8 |
sol'4 sol'8 |
sol' sol' sol' |
sol' sol' sol' |
sol' sol16 la si do' |
re'8 si4 |
re'8 fa' fa' |
mi' do' do' |
mi' \sug re' sol' |
mi'4. |
