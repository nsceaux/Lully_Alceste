\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:system-count 2)
   (basse-continue #:music , #{ s1*6\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#12 #}))
