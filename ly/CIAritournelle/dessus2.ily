\clef "dessus" r4 mi'' mi'' fa''8 mi'' |
re''4 re'' re''4. re''8 |
re''4 do'' do'' re''8 do'' |
si'4 si' si' do''8 si' |
la'4 la' re''4. re''8 |
re''4 mi'' mi'' mi'' |
mi'' re'' re'' re'' |
re'' do'' si'4.\trill do''8 |
do''4 mi''\doux mi'' mi'' |
mi'' re'' re'' re'' |
re'' do'' si'4.\trill do''8 |
do''1 |
