\clef "basse" do2. do'4 |
do' do' si4. si8 |
do'2 do |
sol,4 sol sol sol |
sol2 fad |
sol4 mi mi do |
fa fa fa re |
sol do sol,2 |
do mi, |
fa,2. re,4 |
sol,1 |
do, |
\once\set Staff.whichBar = "|"
