\clef "dessus" r4 sol'' sol'' la''8 sol'' |
fa''4\trill fa'' fa''4. fa''8 |
fa''4 mi'' mi'' fa''8 mi'' |
re''4\trill re'' re'' mi''8 re'' |
do''4 do'' do'' re''8 do'' |
si'4\trill sol'' sol'' sol'' |
sol'' fa'' fa'' fa'' |
fa'' mi'' re''4.\trill do''8 |
do''4 sol''\doux sol'' sol'' |
sol'' fa'' fa'' fa'' |
fa'' mi'' re''4.\trill do''8 |
do''1 |
