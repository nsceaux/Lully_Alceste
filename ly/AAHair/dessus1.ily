\clef "dessus" re''2 mib''4 |
mib''2 re''4 |
re''4. mib''8 do''4\trill |
re'' fad'' sol'' |
la'' re'' sol''~ |
sol''8 la'' fad''4.\trill sol''8 |
sol''2. |
R2.*37 |
