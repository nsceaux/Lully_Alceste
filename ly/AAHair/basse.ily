\clef "basse" sol2. |
fa |
mib |
re4 re' sol |
fad sol8 fa? mib re |
do4 re re, |
sol,2. |
<<
  \tag #'basse { R2.*37 }
  \tag #'basse-continue {
    sol2 fad4 |
    sol4 sol,4. la,8 |
    sib,4 do2 |
    re4. mi8 fad re |
    sol2 la4 |
    sib sib, do |
    re8 mib fa4 fa, |
    sib,4. la,8 sol,4 |
    sib,2 sib4 |
    mib2 fa4 |
    sol2 fa4 |
    mi2 do4 |
    fa2 mi4 |
    fa2 fa8 mib |
    re2 sol4 |
    do4. re8 mib4 |
    fa fad2 |
    sol4 fa mib |
    re2 mi4 |
    fad2. |
    \setMusic #'reprise {
      sol2 sol4 |
      la si2 |
      do'4. sib8 la sol |
      fad2 sol4 |
      re4. do8 sib,4 |
      do re re, |
    }
    \keepWithTag #'() \reprise |
    sol,2 sol8 la |
    sib2. |
    sol,2 sol4 |
    fad2. |
    \reprise |
    sol,2. |
  }
>>
