\clef "dessus" sib'2 do''4 |
la'2 sib'4 |
sol'2 la'4 |
fad' la' sib' |
do'' sib' sib'~ |
sib'8 do'' la'4.\trill sol'8 |
sol'2. |
R2.*37 |
