L’art d’ac -- cord a -- vec la na -- tu -- re
sert l’a -- mour dans ces lieux char -- mants :  - mants :
ces eaux qui font res -- ver par un si doux mur -- mu -- re,
ces ta -- pis où les fleurs for -- ment tant d’or -- ne -- ments,
ces ga -- zons, ces Lits de ver -- du -- re,
tout n’est fait que pour les a -- mants.
Ces
- mants.
Ces ga -- zons, ces lits de ver -- du -- re,
tout n’est fait que pour les a -- mants.
