\clef "vdessus" R2.*7 |
\ffclef "vdessus" <>^\markup\character La Nymphe des Thuilleries
sol'2 la'4 |
sib'2 sol'4 |
re'' do''4.\trill sib'8 |
la'2\trill la'4 |
sib'2 do''4 |
re''2 mib''4 |
fa'' do''4.\trill sib'8 |
sib'2. |
sib'2 re''4 |
mib''2 do''4 |
si'4.( la'8) si'4 |
do''4. sol'8 sol'4 |
la' sib'4. do''8 |
la'4.\trill sol'8( fa'4) |
r4 fa'' re'' |
mib''2 mib''8 re'' |
do''2\trill re''8. la'16 |
sib'4 do''8[ sib'] la'[ sol'] |
fad'2 r4 |
r r re''8. do''16 |
\setMusic #'reprise {
  si'2 sol''4 |
  fa''4.\trill mib''8 re''4 |
  mib''2 do''4 |
  la'2 sib'4 |
  fad'2 sol'4 |
  la' la'4.\trill sol'8 |
  sol'2. |
}
\keepWithTag #'() \reprise
r4 r re'' |
sol'2. |
r4 r re''8. do''16 |
\reprise
