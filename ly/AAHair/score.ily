\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiri } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s2.*7\break s2.*9\break s2.*19\break }
      \origLayout {
        s2.*7\break s2.*5\break s2.*6\pageBreak
        s2.*6\break s2.*6\break s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
