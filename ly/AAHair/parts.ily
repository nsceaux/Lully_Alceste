\piecePartSpecs
#`((dessus #:score-template "score-dessus2")
   (basse)
   (basse-continue #:score-template "score-basse-voix"
                   #:music , #{ s2.*7\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#68 #}))
