\bookpart {
  \paper { page-count = 4 }
  \act "Acte Troisiême"
  \sceneDescription\markup\wordwrap-center {
    Le Théatre Représente un Monument Élevé Par les Arts.
  }
  \scene "Scene Premiére" "Scene I"
  \sceneDescription\markup\wordwrap-center {
    Alceste, Pheres, Céphise
  }
  %% 3-1
  \pieceToc\markup\wordwrap {
    Alceste, Pheres, Céphise :
    \italic { Ah pourquoy nous séparez-vous ? }
  }
  \includeScore "DAArecit"
}
\bookpart {
  \scene "Scene Seconde" "Scene II"
  \sceneDescription\markup\wordwrap-center {
    Pheres, Cleante
  }
  %% 3-2
  \pieceToc\markup\wordwrap {
    Pheres, Cleante :
    \italic { Voyons encor mon Fils, allons, bastons nos pas }
  }
  \includeScore "DBArecit"
}
\bookpart {
  \scene "Scene III" "Scene III"
  \sceneDescription\markup\wordwrap-center {
    Le Chœur, Admete, Pheres, Cleante.
  }
  %% 3-3
  \pieceToc\markup\wordwrap {
    Chœur, Admete, Pheres, Cleante :
    \italic { O trop heureux Admete }
  }
  \includeScore "DCArecit"
}
\bookpart {
  \scene "Scene IV" "Scene IV"
  \sceneDescription\markup\wordwrap-center {
    Céphise, Admete, Pheres, Cleante, le Chœur.
  }
  %% 3-4
  \pieceToc\markup\wordwrap {
    Céphise, Admete, Chœur :
    \italic { Alceste est morte }
  }
  \includeScore "DDArecit"
}

\bookpart {
  \scene "Scene Cinquiême" "Scene V"
  \sceneDescription\markup\wordwrap-center {
    Troupe de femmes affligées, Troupe d’Hommes desolez,
    qui portent des fleurs, & tous les ornements qui ont servy
    à parer Alceste.
  }
  %% 3-5
  \pieceToc\markup Pompe funebre
  \includeScore "DEAsimphonie"
}
\bookpart {
  %% 3-6
  \pieceToc\markup\wordwrap {
    Une femme affligée, chœur : \italic { La Mort, la Mort barbare }
  }
  \includeScore "DEBair"
}
\bookpart {
  %% 3-7
  \pieceToc\markup\wordwrap {
    Chœur :
    \italic { Que nos pleurs, que nos cris renouvellent sans cesse }
  }
  \includeScore "DECchoeur"
}
\bookpart {
  \scene "Scene Sixiême" "Scene VI"
  \sceneDescription\markup\wordwrap-center {
    Admete, Pheres, Céphise, Cleante, suite.
  }
  %% 3-8
  \pieceToc\markup\wordwrap {
    Admete :
    \italic { Sans Alceste, sans ses appas }
  }
  \includeScore "DFArecit"

  \scene "Scene Septiême" "Scene VII"
  \sceneDescription\markup\wordwrap-center {
    Alcide, Admete, Pheres, Céphise, Cleante.
  }
  %% 3-9
  \pieceToc\markup\wordwrap {
    Alcide, Admete :
    \italic { Tu me vois arresté sur le point de partir }
  }
  \includeScore "DGArecit"
}
\bookpart {
  \scene "Scene Huitiême" "Scene VIII"
  \sceneDescription\markup\wordwrap-center {
    Diane, Mercure, Alcide, Admete, Pheres, Céphise, Cleante.
  }
  %% 3-10
  \pieceToc\markup\wordwrap {
    Diane : \italic { Le Dieu dont tu tiens la naissance }
  }
  \includeScore "DHArecit"
}
\bookpart {
  %% 3-11 
  \pieceToc\markup Entr’acte
  \reIncludeScore "BGAair" "DHBentracte"
  \actEnd "Fin du Troisiême Acte"
}
