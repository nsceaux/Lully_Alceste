\key la \minor \beginMark "Ritournelle"
\time 2/2 \midiTempo#120 s1*16
\time 4/4 \midiTempo#80 s1*7
\digitTime\time 3/4 s2.*2
\time 4/4 s1*8
\digitTime\time 3/4 s2.
\digitTime\time 3/4 \midiTempo#160 s2.
\bar "|!:" s2.*6 \alternatives s2. s2. s2.*17
\time 2/2 s1*16
\time 4/4 \midiTempo#80 s1*8
\digitTime\time 3/4 \midiTempo#160 s2.*22
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 \midiTempo#160 s2.*4
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.*2
\time 4/4 s1*4
\digitTime\time 3/4 \midiTempo#160 s2.*3
\time 4/4 \midiTempo#80 s1*6
\digitTime\time 3/4 \midiTempo#160 s2.*4 \bar "|."
