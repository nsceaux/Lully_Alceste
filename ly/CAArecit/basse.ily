\clef "basse" re1 |
R1*2 |
r4 la8 sol fa4. mi16 re |
sib2 r8 sol la sib |
do'2 r8 la sib do' |
re'4 sib do' do |
fa fa8 sol la4 la8 sib |
do'2. sib8 la |
sol4 sol8 la sib4 sib8 do' |
re'4 re8 mi fa4 sol |
la la8 sol fa4 mi8 re |
dod2 re4 re8 do? |
sib,1 |
la,2 sol, |
la,1 |
<<
  \tag #'basse {
    re1 |
    R1*6 R2.*2 R1*8 R2.*27 R1*24 R2. |
    r4 r
  }
  \tag #'basse-continue {
    re1~ |
    re |
    la, |
    sib, |
    si,!2 do |
    re mi |
    fa fa, |
    sib,2. |
    do4 re8 sib, do4 |
    fa, fa mi2 |
    re1 |
    mi2 mi, |
    la,1~ |
    la, |
    fad,2 sol,~ |
    sol, la,8. fa,16 sol,8 la, |
    re,4 re re'2 |
    do'4 sib fa8 sol |
    la2 la,4 |
    re2 re4 |
    mi2 mi4 |
    fa4. mi8 re do |
    si,2 la,4 |
    mi2 sold,4 |
    la, mi,2 |
    la, la,4 |
    la,4 la8 sib la sol |
    fa2 mi4 |
    re2. |
    do4 do' la |
    sib do' do |
    fa,2 fa4 |
    mi2 re4 |
    la, la sol |
    fa mi re |
    dod re do |
    sib,2. |
    la,4 la si |
    dod'2 la4 |
    re' re2 |
    la,4 la sol |
    fa mi re |
    sib la sol |
    la la,2 |
    re1~ | \allowPageTurn
    re |
    mi |
    fa4. fa8 fa4 mi |
    re2 mi4 mi, |
    la,2. la,4 |
    re2 re4 do |
    si,2. si,4 |
    do2 la, |
    sib,1 |
    do2 la, |
    sib, do4 do, |
    fa, fa, re,2 |
    sol,1 |
    la,2 fa, |
    sol, la, |
    re1~ |
    re2 do4 sib, |
    la,2 la |
    re sol4. fa8 |
    mi4 fa sol8 do sol,4 |
    do2 do' |
    sol re4 re, |
    sol,1 |
    la,2 la4 |
    re'2
  }
>> re'8 re' |
la4 la sib |
fa2 fa4 |
mi mi fa |
do2 do4 |
la,2 la,4 |
sib,2 sib,4 |
do2 do,4 |
fa,2 fa4 |
fa sol la |
sib2 la4 |
sol2 re'8 re' |
do'2 do'8 re' |
sib2 sib4 |
la2 la4 |
fa2 fa4 |
mi2 mi4 |
re2 re4 |
do2 do4 |
sib,2 sol,4 |
la,2 la,4 |
<<
  \tag #'basse { re4 r r2 | R1*2 | r4 r }
  \tag #'basse-continue {
    re1~ |
    re |
    sol2 mi4 re |
    do2
  }
>> do'4 |
la2 la4 |
sib2 sib4 |
do'2 do4 |
<<
  \tag #'basse { fa4 r r2 | R1 R2.*2 R1*4 | r4 r }
  \tag #'basse-continue {
    fa1~ |
    fa | \allowPageTurn
    sib,2.~ |
    sib,4 si,2 |
    do1~ |
    do2 sol,~ |
    sol, re |
    la, re |
    mi2
  }
>> mi4 |
fa2 re4 |
mi2 mi,4 |
<<
  \tag #'basse { la,4 r r2 | R1*5 | r4 r }
  \tag #'basse-continue {
    la,1~ |
    la,2 fad,2 |
    sol,4 sol mi8 fa sol sol, |
    do1 |
    fa1 |
    sib4. la8 sol2 |
    la2
  }
>> la4 |
sib2 sol4 |
la2 la,4 |
<<
  \tag #'basse { re2 r4 }
  \tag #'basse-continue {
    re2~ re16 do sib, la, |
    \once\set Staff.whichBar = "|"
    \custosNote sol,8
  }
>>
