\clef "dessus" r4 la''8 sol'' fa''4.\trill mi''16 re'' |
la''2 sol''4. la''16 mi'' |
fa''4. sol''16 la'' sol''4.\trill fa''8 |
mi''4.(\trill re''16 mi'') fa''2 |
r8 fa'' sol'' la'' sib''2 |
r8 sol'' la'' sib'' do'''2 |
la''4\trill sib''8 la'' sol''4\trill la''8 sib'' |
la''2.\trill la''4 |
sol'' do''8 re'' mi''4\trill mi''8 fad'' |
sol''2 re''4 re''8 mi'' |
fa''4 fa''8 sol'' la''4 sol''8 fa'' |
mi''4.\trill mi''8 fa''4. fa''8 |
sol''4 la''8 sol'' fa''4\trill fa''8 mi'' |
re''2.\trill mi''8 re'' |
dod''2 re''4. re''8 |
re''4. dod''8 dod''4.\trill re''8 |
re''1 |
R1*6 R2.*2 R1*8 R2.*27 R1*24 R2. |
r4 r <>_\markup\whiteout Violons fa''8 fa'' |
mi''4 fa'' re'' |
do''2 do''8 re'' |
mi'' fa'' sol''4 la'' |
sol''2 sol''4 |
la'' la'' sol'' |
fa'' sib'' la'' |
sol''4. la''8 sib''4 |
la''2 la'4 |
la' sib' do'' |
re'' mi'' fad'' |
sol'' re''8 mi'' fa''4 |
mi''4 mi''4. fa''8 |
re''4 re'' mi'' |
dod''2 dod''4 |
re'' la' re'' |
re'' do''4. do''8 |
do''4 sib'4. sib'8 |
sib'4 la' la''8 la'' |
la''4 sol'' sol''8 la'' |
fa''4 mi''4.\trill re''8 |
re''4 r r2 |
R1*2 R2. |
<>_\markup\whiteout Violons la''4 la'' la'' |
fa''2 fa''4 |
fa''4. sol''8 mi''4 |
fa''4 r r2 |
R1 R2.*2 R1*4 |
r4 r <>_\markup\whiteout Violons sold'' |
la''2 la''4 |
la''4. si''8 sold''4 |
la''4 r r2 |
R1*5 |
r4 r <>_\markup\whiteout Violons la''8. sol''16 |
fa''2 sib''4 |
mi''2 la''8. sol''16 |
fad''2 r4 |
