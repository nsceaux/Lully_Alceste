<<
  %% Céphise
  \tag #'(cephise basse) {
    \clef "vdessus" R1*16
    \ffclef "vdessus" <>^\markup\character Céphise
    r2 r4 r8 la' |
    re''16 re'' re'' re'' la'4 r16 la' la' la' la'8. si'16 |
    do''4 do'' <<
      \tag #'basse { s2 s1*4 s2.*2 s8
        \ffclef "vdessus" <>^\markup\character Céphise }
      \tag #'cephise { r2 R1*4 R2.*2 r8 }
    >> la'8 la'16 la' la' sib' sol'8\trill sol'16 sol' sol'8 fa'16 mi' |
    fa'8 fa' r16 re'' re'' do'' si'8.\trill si'16 si'8. la'16 |
    sold'4 r8 mi'' si'8.\trill si'16 si' si' dod'' re'' |
    <<
      \tag #'basse { dod''8 s2.. s2
        \ffclef "vdessus" <>^\markup\character Céphise }
      \tag #'cephise { dod''4 r4 r2 r }
    >> r8 la' la'16 la' la' la' |
    re''8. la'16 do'' do'' sib' la' sib'4 sib' |
    <<
      \tag #'basse { s1 s4 \ffclef "vdessus" <>^\markup\character Céphise }
      \tag #'cephise { R1 r4 }
    >> r8 re'' la' fa'16 sol' la'8\trill la'16 si' |
    do''8. la'16 re''8 re''16 mi'' fa''8 mi''16 re'' |
    dod''2 la'8. sol'16 |
    fa'2 re'4 |
    sib'2 sib'8. do''16 |
    la'4\trill la' fa''8. mi''16 |
    re''4.\trill mi''8 do''4 |
    si'2\trill mi''8. si'16 |
    do''8[ re''] si'4.\trill la'8 |
    la'2 la'8. sol'16 |
    la'2. |
    do''4. re''8 mi''4 |
    fa''2 fa''8. sol''16 |
    mi''4\trill mi'' do''8. do''16 |
    sib'8\trill la' sol'4.\trill fa'8 |
    fa'2 la'8. sib'16 |
    sol'2\trill fa'4 |
    mi'\trill mi' mi' |
    la'2.~ |
    la'~ |
    la'2 la'8. sol'16 |
    la'2. |
    r4 mi''4. mi''8 |
    fa''2 re''4 |
    dod'' la' la' |
    re''2.~ |
    re''~ |
    re''2 re''8. dod''16 |
    re''2 <<
      \tag #'basse { s2 s1*15 s8 \ffclef "vdessus" <>^\markup\character Céphise }
      \tag #'cephise { r2 R1*15 r8 }
    >> fa''8 re''16\trill re'' re'' re'' sib'8 sib'16 sib' sib'8 sib'16 la' |
    <<
      \tag #'basse { la'16\trill la' s2.. s2
        \ffclef "vdessus" <>^\markup\character Céphise }
      \tag #'cephise { la'8\trill la' r4 r2 r2}
    >> mi''4 mi''8 mi'' |
    fa''4. mi''8 re''4\trill re'' |
    r8 sol'' fa''8. mi''16 re''8.\trill do''16 si'8.\trill do''16 |
    do''2 r4 sol'8 la' |
    sib'4 sib'8. do''16 la'4\trill la'8 fa'' |
    re''4.\trill re''8 re''4. mi''8 |
    dod''2. |
    <<
      \tag #'basse { s2.*21 s4. \ffclef "vdessus" <>^\markup\character Céphise }
      \tag #'cephise { R2.*21 r4 r8 }
    >> re''8 la' la' la'16 la' la' sib' |
    fad'4 fad' r8 re''16 re'' la'8 si'16 do'' |
    si'8\trill si' r sol' do'' do''16 do'' fa''8 fa''16 sol'' |
    mi''2.\trill |
    <<
      \tag #'basse { s2.*3 s4 \ffclef "vdessus" <>^\markup\character Céphise }
      \tag #'cephise { R2.*3 r4 }
    >> r8 fa' la'16\trill la' la' la' la'8. si'16 |
    do''8 do''16 do'' fa'' fa'' fa'' fa'' do''8 do''16 do'' la'8\trill sib'16 do'' |
    re''8 re'' r fa''16 fa'' re''8\trill re''16 fa'' |
    sib'4 re''8. re''16 re''8 re''16 re'' |
    sol'4 r8 do''16 re'' mi''8.\trill fa''16 sol''8 mi''16 sol'' |
    do''8 do''16 do'' sol'\trill sol' sol' la' sib'8 sib' r sib'16 re'' |
    sib'8.\trill sib'16 sib'8 sib'16 la' la'8\trill r16 fa'' fa'' fa'' la' si'? |
    do''8 do'' r mi''16 mi'' re''8.\trill do''16 si'8\trill si'16 la' |
    sold'2 <<
      \tag #'basse { s4 s2.*2 s1*3 s8
        \ffclef "vdessus" <>^\markup\character Céphise }
      \tag #'cephise { r4 R2.*2 R1*3 r8 }
    >> sol'' mi''16\trill mi'' mi'' mi'' do''8. do''16 do'' sib' sib' la' |
    la'8\trill la' r16 do'' do'' do'' fa''8. fa''16 fa''8. fa''16 |
    re''8.\trill re''16 re'' re'' mi'' fad'' sol''8 sol''16 fa''? mi''8\trill mi''16 re'' |
    dod''2
    \tag #'cephise { r4 R2.*3 }
  }
  %% Straton
  \tag #'(straton basse) {
    <<
      \tag #'basse { s1*18 s2 }
      \tag #'straton { \clef "vbasse" R1*18 r2 }
    >> \ffclef "vbasse" <>^\markup\character Straton
    r8 la16 la la8 la16 la |
    re4 re r8 fa sib16 sib sib sib |
    sol8.\trill sol16 re re mi fa mi8\trill sol do'8. do'16 |
    do'8. do'16 do'8. si16 do'8 do' r do' |
    la8.\trill la16 la8. do'16 fa8 fa16 fa fa8 sol16 la |
    re4 r8 sib16 la sol8\trill sol16 fa |
    mi8\trill mi fa fa r fa16 mi |
    <<
      \tag #'basse { fa8 s2.. s1*2 s8
        \ffclef "vbasse" <>^\markup\character Straton }
      \tag #'straton { fa4 r4 r2 R1*2 r8 }
    >> la8 la16 la la la dod8. dod16 dod16 dod dod mi |
    la,4 la, <<
      \tag #'basse { s2 s1 \ffclef "vbasse" <>^\markup\character Straton }
      \tag #'straton { r2 R1 }
    >>
    r8 sol sol16 fa mi re dod8. la16 sol fa mi fa |
    re4 <<
      \tag #'basse { s2. s2.*27 s2
        \ffclef "vbasse" <>^\markup\character Straton }
      \tag #'straton { r4 r2 R2.*27 r2 }
    >> r4 la |
    fa re sib4. la8 |
    sol8\trill sol sol sol do'4. sib8 |
    la4\trill la r8 la si dod' |
    re'4. do'8 si4.\trill la8 |
    la2 r4 la8 sol |
    fad2 fad4. fad8 |
    sol4 sol8 sol re4 mi8 fa |
    mi4\trill mi do' do'8 do' |
    do'4 sib sib4. la8 |
    sol4\trill sol r8 do' do' do' |
    re'[ do'] sib[ la] sol4.\trill fa8 |
    fa2 la4 la8 la |
    la4 sol sol4.\trill fa8 |
    mi4\trill mi r8 la re' la |
    sib[ la] sol[ fa] mi4.\trill re8 |
    <<
      \tag #'basse { re8 s2.. s8 \ffclef "vbasse" <>^\markup\character Straton }
      \tag #'straton { re4 r r2 r8 }
    >> re8 re16 mi fa sol la8. la16 re' re' re' sol |
    la2 <<
      \tag #'basse { s2 s1*5 s2. \ffclef "vbasse" <>^\markup\character Straton }
      \tag #'straton { r2 R1*5 R2. }
    >>
    r4 r re'8 re' |
    la4 la sib |
    fa2 fa4 |
    mi mi fa |
    do2 do4 |
    la, la, la, |
    sib,2 sib,4 |
    do2 do4 |
    fa,2 fa4 |
    fa sol la |
    sib sib la |
    sol\trill sol re'8 re' |
    do'4 do'4. re'8 |
    sib4.(\trill la8) sib4 |
    la2 la4 |
    fa fa fa |
    mi\trill mi mi |
    re4 re re8 re |
    do4 do do |
    sib, sib, sol, |
    la,2 la,4 |
    <<
      \tag #'basse { re4 s2. s1*2 s2.
        \ffclef "vbasse" <>^\markup\character Straton }
      \tag #'straton { re2 r2 R1*2 R2. }
    >>
    la4 la la |
    sib2. |
    do'4 do' do |
    fa4 <<
      \tag #'basse { s2. s1 s2.*2 s1*4 s2
        \ffclef "vbasse" <>^\markup\character Straton }
      \tag #'straton { r4 r2 R1 R2.*2 R1*4 r4 r }
    >> mi4 |
    fa2 re4 |
    mi2 mi4 |
    la,8 r16 la la la la la mi8 mi16 mi dod8\trill dod16 mi |
    la,8 la, r la re' re'16 re' la8 si16 do' |
    si8\trill si r sol16 sol do'8. fa16 sol8 sol16 sol |
    do8 <<
      \tag #'basse { s2.. s1*2 s2 \ffclef "vbasse" <>^\markup\character Straton }
      \tag #'straton { r8 r4 r2 R1*2 r4 r }
    >> la4 |
    sib2 sol4 |
    la2 la,4 |
    re2 r4 |
  }
>>