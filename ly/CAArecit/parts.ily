\piecePartSpecs
#`((dessus #:score-template "score-dessus2")
   (basse)
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#134 #}))
