\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiri } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'cephise \includeNotes "voix"
    >> \keepWithTag #'cephise \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'straton \includeNotes "voix"
    >> \keepWithTag #'straton \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\break s1*5 s2 \bar "" \pageBreak
        s2 s1*5\break s1*3\break s1*2\break s1*2\break s2.*2 s2 \bar "" \pageBreak
        s2 s1 s2 \bar "" \break s2 s1\break s1*2\break s1 s2 \bar "" \break
        s2 s2.\break s2.*5\pageBreak
        s2.*6\break s2.*5\break s2.*6\break s2.*4 s1*2\break
        s1*3\break s1*4\pageBreak
        s1*4\break s1*3\break s1 s2 \bar "" \break s2 s1*2\break
        s1*3\break s1 s2.\pageBreak
        s2.*6\break s2.*6\break s2.*5\pageBreak
        s2.*4\break s1*2 s2 \bar "" \break s2 s2.*4\break s1*2\pageBreak
        s2.*2\break s1\break s1 s2 \bar "" \break s2 s1\break s2.*3\pageBreak
        s1*2\break s1*2\break s1\break s1\break
      }
    >>
  >>
  \layout { }
  \midi { }
}