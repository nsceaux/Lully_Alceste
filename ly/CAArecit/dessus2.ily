\clef "dessus" R1 |
r4 re''8 mi'' dod''4. si'16 la' |
re''4. mi''16 fa'' mi''4.\trill re''8 |
dod''4.( si'16 dod'') re''2 |
r8 re'' mi'' fa'' sol''2 |
r8 mi'' fa'' sol'' la''2 |
fa''4 sol''8 fa'' mi''4.\trill fa''8 |
fa''2. do''8 re'' |
mi''4\trill mi''8 fa'' sol''4 sol''8 la'' |
sib''2. sib''4 |
la'' la''8 sol'' fa''4 mi''8 re'' |
dod''4. dod''8 re''4. re''8 |
mi''4. mi''8 la'4 la''~ |
la'' sib''8 la'' sol''4.\trill sol''8 |
sol''4 la''8 sol'' fa''4\trill sol''8 fa'' |
mi''4. mi''8 mi''4.\trill re''8 |
re''1 |
R1*6 R2.*2 R1*8 R2.*27 R1*24 R2. |
r4 r la'8 si' |
do''4 do'' sib' |
la'2 la''4 |
sol'' sol'' fa'' |
mi''2 mi''4 |
fa'' fa'' mi'' |
re'' sol'' fa'' |
fa''4. fa''8 mi''4 |
fa''2 do''4 |
fa''2. |
fa''4 sol'' la'' |
sib''2 la''4 |
la''2. |
la''4 sol''4.(\trill fa''16 sol'') |
la''2 mi''4 |
fa''4. sol''8 la''4 |
sol''2 fa''8 mi'' |
fa''2 sol''8 re'' |
mi''4 mi'' fa'' |
re'' mi'' mi''8 fa'' |
re''4 dod''4. re''8 |
re''4 r r2 |
R1*2 R2. |
fa''4 fa'' fa'' |
re''2 sib''4 |
sol''4. la''8 sib''4 |
la''4 r r2 |
R1 R2.*2 R1*4 |
r4 r mi''8. re''16 |
do''2 fa''4 |
si'2 mi''8. re''16 |
dod''4 r r2 |
R1*5 |
r4 r dod'' |
re''2 re''4 |
re''4. mi''8 dod''4 |
re''2 r4 |
