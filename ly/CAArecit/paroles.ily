\tag #'(cephise basse) {
  Al -- ces -- te ne vient point, & nous de -- vons l’at -- ten -- dre.
}
\tag #'(straton basse) {
  Que peut- el -- le pre -- ten -- dre ?
  Pour -- quoy se tour -- men -- ter i -- cy mal à pro -- pos ?
  Ses cris ont beau se faire en -- ten -- dre,
  peut- es -- tre son es -- poux a pe -- ri dans les flots,
  et nous som -- mes en -- fin dans l’is -- le de Scy -- ros.
}
\tag #'(cephise basse) {
  Tu ne te plain -- dras point que j’en u -- se de mes -- me.
  Je t’ay don -- né peu d’em -- bar -- ras.
  Tu vois com -- me je suis tes pas.
}
\tag #'(straton basse) {
  Tu sçais dis -- si -- mu -- ler u -- ne co -- lere ex -- tres -- me.
}
\tag #'(cephise basse) {
  Et si je te di -- sois que c’est toy seul que j’ay -- me ?
}
\tag #'(straton basse) {
  Tu le di -- rois en vain je ne te croi -- rois pas.
}
\tag #'(cephise basse) {
  Croy moy : si j’ay faint de chan -- ger
  c’es -- toit pour te mieux en -- ga -- ger.
  Un ri -- val n’est pas i -- nu -- ti -- le,
  il ré -- veil -- le l’ar -- deur & les soins d’un a -- mant ;
  Un ri -   - mant ;
  u -- ne con -- ques -- te fa -- ci -- le
  don -- ne peu d’em -- pres -- se -- ment,
  et l’a -- mour tran -- qui -- le
  s’en -- dort __ ai -- sé -- ment.
  Et l’a -- mour tran -- qui -- le
  s’en -- dort __ ai -- sé -- ment.
}
\tag #'(straton basse) {
  Non, non, ne ten -- te point u -- ne se -- con -- de ru -- se,
  je voy plus clair que tu ne crois.
  On ex -- cu -- se d’a -- bord un a -- mant qu’on a -- bu -- se.
  Mais la so -- tise est sans ex -- cu -- se
  de se lais -- ser trom -- per deux fois.
  Mais la so -- tise est sans ex -- cu -- se
  de se lais -- ser trom -- per deux fois.
}
\tag #'(cephise basse) {
  N’est- il au -- cun mo -- yen d’a -- pai -- ser ta co -- le -- re ?
}
\tag #'(straton basse) {
  Con -- sens à m’es -- pou -- zer & sans re -- tar -- de -- ment.
}
\tag #'(cephise basse) {
  U -- ne si grande af -- fai -- re
  ne se fait pas si promp -- te -- ment
  un Hi -- men qu’on dif -- fe -- re
  n’en est que plus char -- mant.
}
\tag #'(straton basse) {
  Un hi -- men qui peut plai -- re
  ne cous -- te guè -- re,
  et c’est un nœud bien tost for -- mé ;
  rien n’est plus ai -- sé que de fai -- re
  un es -- poux d’un a -- mant ai -- mé.
  Rien n’est plus ai -- sé que de fai -- re
  un es -- poux d’un a -- mant d’un a -- mant ai -- mé.
}
\tag #'(cephise basse) {
  Je t’ai -- me d’une a -- mour sin -- ce -- re ;
  et s’il est ne -- ces -- sai -- re,
  je m’offre à t’en faire un ser -- ment.
}
\tag #'(straton basse) {
  A -- mu -- se -- ment, a -- mu -- se -- ment.
}
\tag #'(cephise basse) {
  L’in -- juste en -- le -- ve -- ment d’Al -- ces -- te
  at -- ti -- re dans ces lieux u -- ne guer -- re fu -- nes -- te,
  les plus bra -- ves des Grecs s’ar -- ment pour son se -- cours :
  au mi -- lieu des cris & des lar -- mes,
  l’hi -- men a peu de char -- mes ;
  at -- ten -- dons de tran -- qui -- les jours.
  Le bruit af -- freux des ar -- mes
  ef -- fa -- rou -- che bien les a -- mours.
}
\tag #'(straton basse) {
  Dis -- cours, dis -- cours, dis -- cours.
  Tu n’as qu’à m’es -- pou -- zer pour m’os -- ter tout om -- bra -- ge,
  pour -- quoy dif -- fe -- rer da -- van -- ta -- ge ?
  A quoy ser -- vent tant de fa -- çons ?
}
\tag #'(cephise basse) {
  Rends moy la li -- ber -- té pour m’es -- pou -- zer sans crain -- te ;
  un hi -- men fait a -- vec con -- trainte
  est un mau -- vais moy -- en de fi -- nir tes soup -- çons.
}
\tag #'(straton basse) {
  Chan -- sons, chan -- sons, chan -- sons.
}
