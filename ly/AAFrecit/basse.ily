\clef "basse" do2. |
sold, |
la, |
mi2 dod4 |
re2 re,4 |
la,2. |
re |
mi~ |
mi8 re mi4 mi, |
la,2. |
re2 fad,4 |
sol,2 sol8 fa |
mi2 re4 |
do2 sold,4 |
la,2. |
re |
mi2 mi4 |
la,8 sol, fa,2 |
mi,4 mi2 |
la,2 la,4 |
si,2. |
do |
fa2 re4 |
mi2 do4 |
re2. |
mi~ |
mi8 re mi4 mi, |
la,2 si,4 |
do1~ |
do |
sol,2 sol8 fa mi re |
do2 re |
re4. do8 si,2 |
do2 la, |
re mi8 do re4 |
sol,1 |
sol2 mi~ |
mi1~ |
mi |
fa2. fa8 mi |
re4. re8 sol do sol,4 |
do1 | \allowPageTurn
do4. re8 mi4 |
fa2 sol4 |
la2 fad4 |
sol2 sol8 fa |
mi2 fa8 mi |
re4 sol sol, |
do2. |
do2 do'4 |
dod'4. si8 la4 |
re'2 do'4 |
si2 do'4 |
la re'4. do'8 |
si4. la8 sol fa |
mi4 re do |
si,2 do4 |
fa, sol,2 |
do2.~ |
do1 |
fa2 mi8. re16 do8 fa, |
sol,4 sol8. fa16 mi8. re16 do4 |
si,8. la,16 sol,8. fa,16 mi,8. fa,16 sol,4 |
do2 si,4. la,8 |
sold,2. |
la,1~ |
la,4 la fad re |
sol2~ sol4 fad |
sol do re re, |
sol, sol2 |
re la8. sol16 |
fa2. |
mi4 mi'8. re'16 do'4 |
si2 la4 |
re mi mi, |
la, la8 sol fa4 |
mi fa2 |
sol4 fa mi |
re4. mi8 fa8. mi16 |
re2 do4 |
fa, sol,2 |
do do4 |
sol2 sol8 fa |
mi2 re4 |
do2. |
re4. do8 si,4 |
do re re, |
sol,2. |
sol4 fa mi |
re4. mi8 fa sol |
la4 sol fa |
mi2 mi,4 |
la,2 la,4 |
re4. do8 si,8. do16 |
la,2. |
sol,4. sol8 la si |

