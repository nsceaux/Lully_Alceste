\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withRecit <<
      \global \keepWithTag #'gloire \includeNotes "voix"
    >> \keepWithTag #'gloire \includeLyrics "paroles"
    \new Staff \with { \haraKiri } \withRecit <<
      \global \keepWithTag #'nymphe \includeNotes "voix"
    >> \keepWithTag #'nymphe \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion { s2.*28\break s1*14\break s2.*17\break }
      \origLayout {
        s2.*4\break s2.*4\break s2.*2\break s2.*3\break s2.*3\break s2.*5\pageBreak
        s2.*4\break s2.*3 s1\break s1*2\break s1*2\break s1*3\break s1*2\pageBreak
        s1*2\break s1*2 s2.*3\break s2.*7\break s2.*5\break s2.*2 s1\break s1*2\pageBreak
        s1*2\break s2. s1*2\break s1*2\break s2.*5\break s2.*6\pageBreak
        s2.*7\break s2.*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}