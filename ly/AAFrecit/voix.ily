%% La Nymphe de la Seine
<<
  \tag #'(nymphe basse) {
    \clef "vbas-dessus"  <>^\markup\character La Nymphe de la Seine
    r4 r mi'' |
    si'4.\trill si'8 mi''8. si'16 |
    do''2 la'4 |
    sold'4.\trill mi'8 la'8. mi'16 |
    fa'4 fa'8.[ mi'16] mi'8 fa' |
    mi'4\trill mi' mi''8. mi''16 |
    fa''4 re''4.\trill do''8 |
    si'2\trill r8 si' |
    sold' la' la'4. sold'8 |
    la'4 r8 do'' do''16 si' la' sol' |
    fad'4 r8 re''16 re'' la'8\trill la'16 re'' |
    si'2\trill sol'8. sol'16 |
    sold'2\trill sold'8. sold'16 |
    la'4 r8 la'16 la' si'8\trill do''16 re'' |
    do''4\trill la' mi''8. mi''16 |
    fa''4 re''4.\trill do''8 |
    si'2\trill r8 si' |
    do'' do'' do''4. re''8 |
    mi''2 r8 si' |
    si'4( do'') la'8. sol'16 |
    sol'4 fa'( mi'8) fa' |
    mi'4\trill mi' r8 do'' |
    la'8.\trill la'16 la'8. si'16 si'8.\trill la'16 |
    sold'4 sold' mi''8. mi''16 |
    fa''4 re''4.\trill do''8 |
    si'2\trill r8 si' |
    sold' la' la'4. sold'8 |
    la'2. |
    
  }
  \tag #'gloire { \clef "vdessus" R2.*28 }
>>
%% La Gloire
<<
  \tag #'(gloire basse) {
     \ffclef "vdessus" <>^\markup\character La Gloire
     r4 r8 mi'' do''8. do''16 do''8 do'' |
     sol'4 do'' do''8. re''16 mi''8. fa''16 |
     re''8\trill re'' r si' si' si' do''8. re''16 |
     mi''8 mi''16 re'' do''8 do''16 si' la'4\trill r8 la' |
     la'8. la'16 si'8 do'' re''8. sol'16 sol' fa' fa' mi' |
     mi'4\trill mi' do''8 do''16 si' la'8.\trill sol'16 |
     fad'4 la'8 si' sol'4 sol'8 fad' |
     sol'2 re''8 si'16 si' si'8 re'' |
     sol'8 sol'16 la' si'8 si'16 sol' do''4 do'' |
     mi''2 mi''8 mi'' mi'' sol'' |
     do''4 sol'8 la' sib'4 sib'8 la' |
     la'4\trill la'8 do'' do'' do'' re'' mi'' |
     fa''4. la'8 si' do'' do'' si' |
     do''1
  }
  \tag #'nymphe { R1*14 }
>>
%% La Nymphe de la Seine
<<
  \tag #'(nymphe basse) {
    \ffclef "vbas-dessus" <>^\markup\character La Nymphe de la Seine
    sol'4 sol' do'' |
    la'2\trill si'4 |
    do''2 re''4 |
    si'2\trill sol'4 |
    do''4. si'8 la' sol' |
    fa'4.\trill mi'8 fa'4 |
    mi'2.\trill |
    mi'2.\trill |
    mi'' |
    fa''2 mi''4 |
    re''\trill re'' mi'' |
    do''2\trill la'4 |
    re''2 si'4 |
    do'' re'' mi'' |
    re''2\trill do''4 |
    do''( si'4.)\trill do''8 |
    do''2. |
  }
  \tag #'gloire { R2.*17 }
>>
%% La Gloire
<<
  \tag #'(gloire basse) {
     \ffclef "vdessus" <>^\markup\character La Gloire
     r4 r8 mi'16 fa' sol'8 sol'16 sol' do''8. do''16 |
     la'8\trill la' r la'16 si' do''8. re''16 mi''8 mi''16 fa'' |
     re''2\trill do''8 do''16 re'' mi''8.\trill fa''16 |
     sol''8 sol'' r re''16 re'' mi''8.[ fa''16] re''8.\trill do''16 |
     do''4 r8 mi'' sold'8. sold'16 sold'8. la'16 |
     si'4 r8 si'16 si' sold'8 sold'16 si' |
     mi'4 mi' r mi'' |
     do'' la' re''4. re''8 |
     re''2 r4 la'8 la' |
     si'4.( do''8) la'4.\trill sol'8 |
     sol'4
  }
  \tag #'nymphe { R1*5 R2. R1*4 r4 }
>>
%% Ensemble
<<
  \tag #'(gloire basse) {
    r8 re'' mi''4 |
    fa'' fa'' mi'' |
    mi''( re''8.\trill[ do''16]) re''4 |
    mi'' mi'' r8 mi'' |
    re''2\trill do''4 |
    re'' si'2\trill |
    la'4. mi''8 fa''4 |
    sol'' fa''4.\trill mi''8 |
    re''2\trill mi''4 |
    fa'' fa'' r8 do'' |
    re''2 mi''4 |
    fa'' re''2\trill |
    do''
  }
  \tag #'nymphe {
    <>^\markup\character La Nymphe de la Seine
    r8 si' dod''4 |
    re'' re'' do'' |
    la'4.( sold'8) la'4 |
    sold' sold' r8 do'' |
    sold'2 la'4 |
    si' sold'2 |
    la'4. do''8 re''4 |
    mi'' re''4.\trill do''8 |
    si'2\trill dod''4 |
    re'' re'' r8 la' |
    si'2 do''4 |
    re'' si'2\trill |
    do''
  }
>>
%% La Nymphe de la Seine
<<
  \tag #'basse { \ffclef "vbas-dessus" <>^\markup\character La Nymphe de la Seine }
  \tag #'(nymphe basse) {
    r8 do'' |
    si'2\trill sol'4 |
    r do'' re'' |
    mi''2. |
    la'4 la' r8 re'' |
    do''8.\trill si'16 la'2\trill |
    sol'2 si'8. do''16 |
    re''4 re'' mi'' |
    fa''2 la'8 si' |
    do''4 si'4.\trill la'8 |
    sold'2 mi''8. si'16 |
    do''4 do''4. si'8 |
    la'2\trill re''8. mi''16 |
    do''2\trill si'8. do''16 |
    si'2.\trill |
  }
  \tag #'gloire { r4 R2.*14 }
>>
