\tag #'(nymphe basse) {
  He -- las ! su -- per -- be Gloire, he -- las !
  Ne dois- tu point es -- tre con -- ten -- te ?
  Le he -- ros que j’at -- tens ne re -- vien -- dra- t’il pas ?
  Il ne te suit que trop dans l’hor -- reur des com -- bas ;
  laisse en paix un mo -- ment sa va -- leur tri -- om -- phan -- te.
  Le he -- ros que j’at -- tens ne re -- vien -- dra- t’il pas ?
  Se -- ray- je toû -- jours lan -- guis -- san -- te
  dans u -- ne si cru -- elle at -- ten -- te ?
  Le he -- ros que j’at -- tens ne re -- vien -- dra- t’il pas ?
}
\tag #'(gloire basse) {
  Pour -- quoy tant mur -- mu -- rer ? Nym -- phe, ta plainte est vai -- ne,
  tu ne peux voir sans moy le he -- ros que tu sers ;
  si son é -- loi -- gne -- ment te cous -- te tant de pei -- ne,
  il re -- com -- pense as -- sés les dou -- ceurs que tu pers ;
  voy ce qu’il fait pour toy quand la Gloi -- re l’em -- mei -- ne ;
  voy com -- me sa va -- leur a soû -- mis à la Sei -- ne
  le fleu -- ve le plus fier qui soit dans l’u -- ni -- vers.
}
\tag #'(nymphe basse) {
  On ne voit plus i -- cy pa -- rais -- tre
  que des or -- ne -- ments im -- par -- faits ;  - faits ;
  ah ! ah ! rends- nous nostre au -- gus -- te mais -- tre,
  tu nous ren -- dras tous nos __ at -- traits.
}
\tag #'(gloire basse) {
  Il re -- vient, & tu dois m’en croi -- re ;
  je luy sers de guide a -- vec soin :
  puis -- que tu vois la Gloi -- re
  ton he -- ros n’est pas loin.
  Il lais -- se res -- pi -- rer tout le mon -- de qui trem -- ble ;
  soy -- ons i -- cy d’ac -- cord pour com -- bler __ ses de -- sirs.
}
\tag #'(gloire nymphe basse) {
  Qu’il est doux d’ac -- cor -- der __ en -- sem -- ble
  la Gloire & les plai -- sirs.
  Qu’il est doux d’ac -- cor -- der en -- sem -- ble
  la Gloire & les plai -- sirs.
}
\tag #'(nymphe basse) {
  Na -- ya -- des, dieux des bois, nym -- phes, que tout s’as -- sem -- ble,
  qu’on en -- ten -- de nos chants a -- pres tant de soû -- pirs.
  qu’on en -- ten -- de nos chants a -- pres tant de soû -- pirs.
}
