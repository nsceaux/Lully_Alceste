\key la \minor
%% Nymphe
\digitTime\time 3/4 \midiTempo#120 s2.*28
%% Gloire
\time 4/4 \midiTempo#80 s1*14
%% Nymphe
\digitTime\time 3/4 \midiTempo#120 \bar "|!:" s2.*6 \alternatives s2. s2. s2.*9
%% Gloire
\time 4/4 \midiTempo#80 s1*5
\digitTime\time 3/4 s2.
\time 2/2 \midiTempo#160 s1*4
%% Ensemble
\digitTime\time 3/4 s2.*27