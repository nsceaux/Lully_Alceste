\clef "basse" sib2 la |
sib sib, |
mib fa |
sol4. sol8 fa4. mib8 |
re2. mib4 |
fa4. fa8 mib4. re8 |
do2. do4 |
sol2 re4. mib8 |
fa2 sib, |
sib2. la4 |
sol2 do |
re2. do4 |
sib,4. la,8 sib,4 sol, |
re2 sol, |
sol si, |
do2. sib,4 |
la,4. sol,8 la,4 fa, |
do2 fa, |
fa mi |
fa4. sol8 la4 fa |
sib4. lab8 sol4. fa8 |
mib4. re8 do4. sib,8 |
fa4. sol8 la2 |
sib re4. mib8 |
fa2 sib, |
fa, sib, |
