\clef "dessus" fa''2 fa'' |
fa'' fa''4.(\trill mib''16 fa'') |
sol''4. sol''8 fa''4. mib''8 |
re''2\trill~ re''4. mib''8 |
fa''4. fa''8 fa''4. mib''16 re'' |
do''4. do''8 do''4. re''8 |
mib''4. mib''8 mib''4. fa''8 |
re''4.\trill mib''8 fa''4. sol''8 |
do''2\trill sib' |
fa''4. fa''8 sol''4. la''8 |
sib''4. sib''8 la''4. sol''8 |
fad''4. mi''8 re''4. re''8 |
sol''4. fad''8 sol''4. la''8 |
la''2\trill sol'' |
re''4. re''8 mi''4. fa''8 |
mi''4.\trill re''8 do''4. do''8 |
fa''4. mi''8 fa''4. sol''8 |
sol''2\trill fa'' |
do'' do'' |
do''~ do''4. fa''8 |
re''4.\trill re''8 mib''4. fa''8 |
sol''4. sol''8 la''4. sib''8 |
la''4.\trill sol''8 fa''4. mib''8 |
re''4. mib''8 fa''4. sol''8 |
do''4. do''8 re''4. mib''8 |
do''2\trill sib' |
