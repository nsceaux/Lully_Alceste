\clef "quinte" sib2 do' |
sib sib |
sib la |
sol re'4. do'8 |
sib2. sol4 |
la fa2 fa4 |
sol2 do'4. do'8 |
sib2. sib4 |
fa2 fa |
sib re'4. re'8 |
re'4 sol la2 |
la re'4. re'8 |
re'4. do'8 sib2 |
re' re' |
re'2. re'4 |
sol2 do'4. do'8 |
do'4. sib8 la4 do' |
do'2 do' |
la do' |
do'2. do'4 |
re'4. do'8 sib4. la8 |
sol4 sib mib'4. sib8 |
do'2. do'4 |
sib4. do'8 re'4. sol8 |
la4 fa fa2 |
fa fa |
