\clef "haute-contre" re''2 do'' |
re'' re''4. do''16 re'' |
mib''4. mib''8 re''4. do''8 |
sib'2. do''4 |
re''4. re''8 re''4. do''16 sib' |
la'4. la'8 la'4. sib'8 |
do''4. do''8 sol'4. la'8 |
sib'2 sib'4. sib'8 |
la'2 sib' |
re''4. re''8 do''4. sib'8 |
sib'2 do''4. sib'8 |
la'2 la'4. la'8 |
sib'4. do''8 re''4. re''8 |
re''4. do''8 sib'2 |
si'4 sol' sol'2 |
sol'2. sol'4 |
la'4. sib'8 do''4 do'' |
do''4. sib'8 la'2 |
la' sol' |
la'~ la'4. la'8 |
sib'2. sib'4 |
sib'4. sib'8 do''4. re''8 |
do''2. do''4 |
fa' sib' sib'2 |
la'4. la'8 sib'4. do''8 |
la'2 sib' |
