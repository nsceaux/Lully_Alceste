\clef "taille" re'2 fa' |
fa' fa' |
mib'4 sib8 do' re'2 |
re'4. mib'8 fa'2 |
fa'2. sol'4 |
fa'2. fa'4 |
mib'4. do'8 do'4. do'8 |
re'4 sol' fa'4. mib'8 |
mib'2 re' |
re'4. sib'8 sol'4. fad'8 |
sol'4. re'8 mib'2 |
re'4. mi'8 fad'4. fad'8 |
sol'4. la'8 sol'4 sol' |
fad'4.\trill mi'16 fad' sol'2 |
sol'2. re'4 |
do'4. re'8 mi'4 mi' |
fa'4. sol'8 fa'4 fa' |
mi'4. re'16 mi' fa'2 |
fa' sol' |
fa'2. fa'4 |
fa'4. fa'8 sol'4. re'8 |
mib'2 mib'4. fa'8 |
fa'2. fa'4 |
fa'2. fa'4 |
fa'2 fa' |
fa'4. mib'8 re'2 |
