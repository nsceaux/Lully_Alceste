\clef "quinte" do'4 si |
la2 si4. la8 |
si4 si la4. la8 |
si4 do'8 re' mi'4. mi'8 |
mi'4 do' do'4. do'8 |
do'4 do' do'4. do'8 |
re'2 re'4. re'8 |
re'4 mi'8 fa' sol'4. sol'8 |
sol'4 mi' do'4. do'8 |
la4. la8 si4. si8 |
si2 do'4. do'8 |
la4 la mi'4. mi'8 |
dod'2 do'4. do'8 |
la4. la8 si4. si8 |
si2 do'4. do'8 |
la4 la mi'4. mi'8 |
dod'2
