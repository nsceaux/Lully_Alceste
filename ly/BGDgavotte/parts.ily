\piecePartSpecs
#`((dessus #:music , #{ s2 s1*3 s2\break s2 s1*5\break #})
   (haute-contre)
   (taille)
   (quinte)
   (basse #:system-count 3)
   (basse-continue #:music , #{ s2 s1*3 s2\break s2 s1*5\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#28 #}))
