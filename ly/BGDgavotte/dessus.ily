\clef "dessus" <>^"Violons" la'4 si' |
do'' do'' si'4.\trill la'8 |
sold'4 sold' mi''4. mi''8 |
re''4.\trill do''8 si'4( do''8 re'') |
do''4 la' mi''4. fa''8 |
sol''4 sol'' fa''4.\trill mi''8 |
re''2\trill fa''4. fa''8 |
fa''4 mi'' re'' mi''8 fa'' |
mi''4\trill do'' mi''4. mi''8 |
mi''4. fa''8 re''4.\trill re''8 |
re''2 do''4.\trill do''8 |
do''4. si'8 si'4.\trill la'8 |
la'2 mi''4. mi''8 |
mi''4. fa''8 re''4.\trill re''8 |
re''2 do''4.\trill do''8 |
do''4. si'8 si'4.\trill la'8 |
la'2
