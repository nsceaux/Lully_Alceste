\clef "basse" la4 sold |
la2 re |
mi do4. do8 |
si,4 la, mi mi, |
la, la, la4. la8 |
mi4 mi fa4. do8 |
sol2 re4. do8 |
si,4 do sol sol, |
do2 la,4. la,8 |
re2 re4 si, |
mi2 la4 la, |
re2 mi4 mi, |
la,2 la,4. la,8 |
re2 re4 si, |
mi2 la4 la, |
re2 mi4 mi, |
la,2
