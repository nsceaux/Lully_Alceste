\key la \minor \beginMark "Ritournelle"
\time 2/2 \midiTempo#160 \partial 2 s2 s1*43
\time 4/4 \midiTempo#80 s1*5
\digitTime\time 3/4 s2.*2
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1*7
\digitTime\time 3/4 \midiTempo#80 s2. \bar "||"
\key re \minor \time 4/4 s1*7
