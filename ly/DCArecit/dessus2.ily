\clef "dessus" r4 fa'' |
mi''2. mi''4 |
la'2 re''4. re''8 |
re''4. dod''8 dod''4.\trill re''8 |
re''2 r |
R1*44 R2.*2 R1 R2. R1 R2. R1*7 R2. R1*7

