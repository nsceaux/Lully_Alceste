<<
  \tag #'admete {
    \clef "vtaille" r2 R1*43 r2
  }
  %% Chœur
  \tag #'(vdessus basse) {
    \clef "vdessus" r2 R1*3 |
    r2 r4 <>^\markup\character Chœur re'' |
    do''4. do''8 do''4 la' |
    re''2 re''4 re'' |
    mi''4. mi''8 fa''4. fa''8 |
    mi''2\trill mi''4 do'' |
    do'' la' sol'4.\trill do''8 |
    la'2\trill r4 do'' |
    do''4. do''8 do''4 sib'8[ la'] |
    sib'2 sib'4 si' |
    dod'' re'' dod''4. re''8 |
    <<
      \tag #'basse { re''4 s2. s1 s2. \ffclef "vdessus" }
      \tag #'vdessus { re''2 r | R1 | r2 r4 }
    >> <>^\markup\character Chœur dod''4 |
    re''4. re''8 re''4. si'8 |
    do''2 do''4 sol' |
    la'4 la' sol'4. do''8 |
    la'2\trill <<
      \tag #'basse { s2 s1*3 s2. \ffclef "vdessus" }
      \tag #'vdessus { r2 | R1*3 | r2 r4 }
    >> <>^\markup\character Chœur dod''4 |
    re''4. re''8 re''4. si'8 |
    do''2 do''4 sol' |
    la' la' sol'4. do''8 |
    la'2\trill <<
      \tag #'basse { s2 s1*5 s2. \ffclef "vdessus" }
      \tag #'vdessus { r2 | R1*5 | r2 r4 }
    >> <>^\markup\character Chœur re''4 |
    do''4. do''8 do''4 la' |
    re''2 re''4 re'' |
    mi''4. mi''8 fa''4. fa''8 |
    mi''2\trill mi''4 do'' |
    do'' la' sol'4.\trill do''8 |
    la'2.\trill do''4 |
    do''4. do''8 do''4 sib'8[ la'] |
    sib'2 sib'4 si' |
    dod''4 re'' dod''4. re''8 |
    re''2
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r2 R1*3 |
    r2 r4 fa' |
    mi'4. mi'8 mi'4 fa' |
    fa'2 fa'4 sol' |
    sol'4. sol'8 la'4. la'8 |
    sol'2 sol'4 mi' |
    fa'4 fa' mi'4. fa'8 |
    fa'2 r4 fa' |
    re'4. re'8 re'4. re'8 |
    re'2 re'4 sol' |
    sol' fa' mi'8[\trill re'] mi'4 |
    fa'2 r |
    R1 |
    r2 r4 mi' |
    fa'4. fa'8 fa'4 re' |
    mi'2 mi'4 mi' |
    fa' fa' mi'4. fa'8 |
    fa'2 r |
    R1*3 |
    r2 r4 mi' |
    fa'4. fa'8 fa'4. re'8 |
    mi'2 mi'4 mi' |
    fa'4 fa' mi'4. fa'8 |
    fa'2 r |
    R1*5 |
    r2 r4 fa' |
    mi'4. mi'8 mi'4 fa' |
    fa'2 fa'4 fa' |
    sol'4. sol'8 la'4. la'8 |
    sol'2 sol'4 mi' |
    fa'4 fa' mi'4. fa'8 |
    fa'2. fa'4 |
    re'4. re'8 re'4. re'8 |
    re'2 re'4 sol' |
    sol'4 fa' mi'8[ re'] mi'4 |
    fa'2
  }
  \tag #'(vtaille basse) {
    <<
      \tag #'basse { s2 s1*13 s4 \ffclef "vtaille" }
      \tag #'vtaille {
        \clef "vtaille" r2 R1*3 |
        r2 r4 la |
        la4. la8 la4 la |
        sib2 sib4 sib |
        do'4. do'8 do'4. do'8 |
        do'2 do'4 do' |
        do'4 do' do'4. do'8 |
        do'2 r4 la |
        la4. la8 la4. la8 |
        sol2 sol4 sol8[ la] |
        la4 la la4. la8 |
        la4
      }
    >> <>^\markup\character Pheres
    fa'4 re'4.\trill do'8 |
    si4.\trill mi'8 si4. mi'8 |
    dod'2 r4 <<
      \tag #'basse { s4 s1*3 s2 \ffclef "vtaille" }
      \tag #'vtaille {
        la4 |
        la4. la8 la4 sol |
        sol2 sol4 do' |
        do' do' do'4. do'8 |
        do'2
      }
    >> <>^\markup\character Pheres
    r4 do'4 |
    fa'8 fa' fa' mi' re'4. do'8 |
    si4\trill si r re'8. mi'16 |
    do'4.( re'8) si4.\trill la8 |
    la2 r4 <<
      \tag #'basse { s4 s1*3 s2 \ffclef "vtaille" }
      \tag #'vtaille {
        la4 |
        la4. la8 la4 sol |
        sol2 sol4 do' |
        do' do' do'4. do'8 |
        do'2
      }
    >> <>^\markup\character Pheres
    r4 la |
    la4. si8 do'4. do'8 |
    do'4.( si8) si4.\trill si8 |
    si4. do'8 re'4. re'8 |
    re'4.( dod'8) dod'4.\trill fa'8 |
    re'4. mi'8 dod'4. re'8 |
    re'2 r4
    \tag #'vtaille {
      la4 |
      la4. la8 la4 la |
      sib2 sib4 sib |
      do'4. do'8 do'4. do'8 |
      do'2 do'4 do' |
      do'4 do' do'4. do'8 |
      do'2. la4 |
      la4. la8 la4. la8 |
      sol2 sol4 sol |
      la4 la la4. la8 |
      la2
    }
  }
  \tag #'vbasse {
    \clef "vbasse" r2 R1*3 |
    r2 r4 re |
    la4. la8 la4 fa |
    sib2 sib4 sol |
    do'4. do'8 fa4. fa8 |
    do2 do4 do' |
    la4 fa do4. do8 |
    fa2 r4 fa |
    fad4. fad8 fad4. fad8 |
    sol2 sol4 mi |
    la re la,4. re8 |
    re4 <>^\markup\character Cleante re' si4.\trill la8 |
    sold4. sold8 sold4. sold8 |
    la2 r4 la |
    re4. re8 re4 sol |
    do2 do4 do' |
    la fa do4. fa8 |
    fa2 <>^\markup\character Cleante r4 la |
    re'8 re' re' do' si4. la8 |
    sold4 sold r si8*3/2 do'8*1/2 |
    la4.( si8) sold4. la8 |
    la2 r4 la |
    re4. re8 re4 sol |
    do2 do4 do' |
    la fa do4. do8 |
    fa2 r |
    R1*5 |
    r2 r4 re |
    la4. la8 la4 fa |
    sib2 sib4 sol |
    do'4. do'8 fa4. fa8 |
    do2 do4 do' |
    la fa do4. fa8 |
    fa2. fa4 |
    fad4. fad8 fad4. fad8 |
    sol2 sol4 mi |
    la re la,4. re8 |
    re2
  }
>>
%% Admete
<<
  \tag #'(admete basse) {
    \tag #'basse \ffclef "vtaille" <>^\markup\character Admete
    r8 la16 la la8 la16 la |
    re'4 re' do'8 do'16 do' do'8 sib16[ la] |
    sib4 sib re'8 re'16 re' re'8 mi' |
    fa'4 dod'8. dod'16 re'4 r8 re'16 dod' |
    re'2 r4 r8 fa' |
    re'8.\trill re'16 re'8 do' sib\trill[ la16] sib |
    la4\trill la8. la16 la8. si16 |
    do'4. do'8 dod'4. re'8 |
    mi'4 r8 mi'16 mi'16 fa'8. mi'16 |
    re'2 re'4( dod'8) re' |
    dod'4 dod' r8 la16 la |
    re'2 re'4. do'8 |
    si2\trill si4. sol'8 |
    do'4. re'8 si4.\trill do'8 |
    do'2 r4 sol8 la |
    sib2 sib4. do'8 |
    la2\trill la4 fa' |
    mi'4.\trill re'8 dod'4. re'8 |
    re'2. |
    r2 r8 sib16 re' sib8 sib16 sib |
    sol2 mib'8 mib'16 mib' mib'8. re'16 |
    re'8\trill re' r sib re'8. re'16 re'8*3/2 mib'8*1/2 |
    fa'8 fa'16 fa' sib8 sib16 re' sol8 sol r mib'16 mib' |
    mib'4 re'8. mib'16 do'4.\trill sib8 |
    sib4 r re'16 re' re' re' la8\trill sib16 do' |
    sib8\trill sol r4 r sol'8 r16 re' |
    %sib4 sib16 sib sib la
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    r2 R1*4 R2.*2 R1 R2. R1 R2. R1*7 R2. R1*7
  }
>>
