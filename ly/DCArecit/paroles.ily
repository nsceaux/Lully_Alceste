\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  O trop heu -- reux Ad -- me -- te !
  O trop heu -- reux Ad -- me -- te !
  Que vos -- tre sort est beau !
  O trop heu -- reux Ad -- me -- te !
  Que vos -- tre sort est beau !
}
\tag #'(vtaille vbasse basse) {
  Quel chan -- ge -- ment ! quel bruit nou -- veau !
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  O trop heu -- reux Ad -- me -- te !
  Que vos -- tre sort est beau !
}
\tag #'(vtaille vbasse basse) {
  L’ef -- fort d’une a -- mi -- tié par -- fai -- te
  l’a sau -- vé du Tom -- beau
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  O trop heu -- reux Ad -- me -- te !
  Que vos -- tre sort est beau !
}
\tag #'(vtaille basse) {
  O trop heu -- reux Ad -- me -- te !
  O trop heu -- reux Ad -- me -- te !
  Que vos -- tre sort est beau !
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  O trop heu -- reux Ad -- me -- te !
  O trop heu -- reux Ad -- me -- te !
  Que vos -- tre sort est beau !
  O trop heu -- reux Ad -- me -- te !
  Que vos -- tre sort est beau !
}
\tag #'(admete basse) {
  Qu’u -- ne pom -- pe fu -- ne -- bre
  rende à ja -- mais ce -- le -- bre
  le ge -- ne -- reux ef -- fort
  qui m’ar -- rache à la mort.
  Al -- ces -- te n’au -- ra plus d’al -- lar -- mes,
  je re -- ver -- ray ses yeux char -- mants
  a qui j’ay cous -- té tant de lar -- mes :
  que la vie a de char -- mes
  pour les heu -- reux a -- mants !
  Que la vie a de char -- mes
  pour les heu -- reux a -- mants !
  A -- che -- vez, dieux des arts, fai -- tes nous voir l’i -- ma -- ge
  qui doit e -- ter -- ni -- ser la gran -- deur de cou -- ra -- ge
  de qui s’est im -- mo -- lé pour moy ;
  ne dif -- fe -- rez point da -- van -- ta -- ge…
  Ciel ! ô %Ciel ! qu’est- ce que je voy !
}
