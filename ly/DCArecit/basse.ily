\clef "basse" re'2~ |
re' dod' |
re'2. re4 |
sol2 la |
re2. re4 |
la4. la8 la4 fa |
sib2 sib4 sol |
do'4. do'8 fa4. fa8 |
do2 do4 do' |
la fa do2 |
fa2. fa4 |
fad4. fad8 fad4 fad |
sol2 sol4 mi |
la4 re la,4. re8 |
<<
  \tag #'basse { re2 r | R1 | r2 r4 }
  \tag #'basse-continue {
    re2. re4 |
    mi1 |
    la,2.
  }
>> la4 |
re2 re4 sol |
do2 do4 do' |
la fa do2 |
<<
  \tag #'basse { fa2 r | R1*3 | r2 r4 }
  \tag #'basse-continue {
    fa1 |
    re2. re4 |
    mi2 sold, |
    la, mi, |
    la,2.
  }
>> la4 |
re2 re4 sol |
do2. do'4 |
la fa do2 |
<<
  \tag #'basse { fa2 r | R1*5 | r2 r4 }
  \tag #'basse-continue {
    fa2. fa4 |
    fad1 | \allowPageTurn
    sol2. sol4 |
    sold2. sold4 |
    la2. fa4 |
    sib sol la la, |
    re2.
  }
>> re4 |
la4. la8 la4 fa |
sib2. sol4 |
do'2 fa |
do do4 do' |
la fa do4. fa8 |
fa2. fa4 |
fad2. fad4 |
sol2 sol4 mi |
la re la,2 |
re <<
  \tag #'basse { r2 R1*4 R2.*2 R1 R2. R1 R2. R1*7 R2. R1*7 }
  \tag #'basse-continue {
    re2 |
    sib,2 fad, |
    sol, sol |
    re4 la, sol, la, |
    re,2 re~ |
    re mi4 |
    fa2. |
    mi2 la4. re8 |
    dod2 re8. do?16 |
    sib,1 |
    la,2 la4 |
    fad1 |
    sol2 sol4 mi |
    la fa sol sol, |
    do2 sib,4. la,8 |
    sol,2 sol |
    re fa, |
    sol, la, |
    re~ re16 do sib, la, |
    sol,1~ |
    sol,~ |
    sol,2 sol4 fa8. mib16 |
    re2 mib4 do |
    fa sib, fa,2 |
    sib,2. fad,4 |
    sol,1 |
    %sol,2
  }
>>
