\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'vdessus \includeNotes "voix"
    >> \keepWithTag #'vdessus \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vhaute-contre \includeNotes "voix"
    >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vtaille \includeNotes "voix"
    >> \keepWithTag #'vtaille \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'vbasse \includeNotes "voix"
    >> \keepWithTag #'vbasse \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'admete \includeNotes "voix"
    >> \keepWithTag #'admete \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2 s1*5\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4 s2 \bar "" \break s2 s1*4\pageBreak
        s1*3\break s1*2\break s1 s2.*2\break s1 s2. s1\pageBreak
        s2. s1*3\break s1*4\break s2. s1*2\break s1*2\break s1*2\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}