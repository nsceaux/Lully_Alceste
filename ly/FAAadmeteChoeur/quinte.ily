\clef "quinte"
\setMusic #'choeur {
  do'4 |
  re' re' sib |
  sol sol sol |
  la2 la4 |
  sol4. sol8 sol4 |
  sol4 sol4. sol8 |
  sol2 do'4 |
  do'2 do'4 |
  sib4. sib8 sib4 |
  sol fa4. fa8 |
}
sib4 |
do'2 do'4 |
sib fa4. fa8 |
fa2 sib4 |
sib2 do'4 |
fa4 sol4. sol8 |
fa2 la4 |
sib2 sib8 la |
sol4 fa4. fa8 |
fa2 r4 |
R2.*23 |
r4 r \keepWithTag #'() \choeur
fa4 r r |
R2. |
R1*2 R2. R1 |
r4 r \choeur
fa4 r r2 |
R1*4 R2. R1 |
r4 r \choeur
fa4 r r |
R1 R2.*15 |
r4 r \choeur
fa2. |
