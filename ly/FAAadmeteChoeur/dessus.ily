\clef "dessus"
\setMusic #'choeur {
  fa''4 |
  re''\trill re'' re'' |
  mib'' mib''4. mib''8 |
  do''2\trill fa''4 |
  re''4.\trill re''8 mib''4 |
  mib'' re''4.\trill do''8 |
  do''2 do''4 |
  la'2\trill do''4 |
  re''4.\trill mib''8 fa''4 |
  sol'' do''4. sib'8 |
}
sib'4 |
fa'' do'' re'' |
mib''4. re''8[ mib''8. fa''16] |
re''4\trill re'' fa'' |
sol''4. fa''8[ mib''8. fa''16] |
re''4 do''4.\trill do''8 |
do''2 fa''4 |
sib'4. do''8 re''4 |
mib'' do''4.\trill sib'8 |
sib'2 r4 |
R2.*23 |
r4 r \keepWithTag #'() \choeur
sib'4 r r |
R2. |
R1*2 R2. R1 |
r4 r \choeur
sib'4 r r2 |
R1*4 R2. R1 |
r4 r \choeur
sib'4 r4 r |
R1 R2.*15 |
r4 r \choeur
sib'2. |
