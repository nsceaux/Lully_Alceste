\setMusic #'choeur <<
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "vdessus"
    <>^\markup\character Chœur
    fa''4 |
    re''\trill re'' re'' |
    mib'' mib''4. mib''8 |
    do''2\trill fa''4 |
    re''4.\trill re''8 mib''4 |
    mib'' re''4.\trill do''8 |
    do''2 do''4 |
    la'2\trill do''4 |
    re''4.\trill mib''8 fa''4 |
    sol'' do''4.\trill sib'8 |
  }
  \tag #'vhaute-contre {
    fa'4 |
    fa' fa' fa' |
    sol' sol'4. sol'8 |
    fa'2 fa'4 |
    sol'4. sol'8 sol'4 |
    sol'4 sol'4. sol'8 |
    mi'2 mi'4 |
    fa'2 fa'4 |
    fa'4. fa'8 fa'4 |
    mib'4 mib'4. re'8 |
  }
  \tag #'vtaille {
    la4 |
    sib sib sib |
    sib sib do' |
    la2 re'4 |
    si4. si8 do'4 |
    do' si4. do'8 |
    do'2 do'4 |
    do'2 la4 |
    sib4. sib8 sib4 |
    sib la4. sib8 |
  }
  \tag #'vbasse {
    fa4 |
    sib sib sib, |
    mib mib do |
    fa2 re4 |
    sol4. sol8 mib4 |
    do sol,4. do8 |
    do2 do'4 |
    fa2 fa4 |
    sib,4. do8 re4 |
    mib fa fa, |
  }
>>
<<
  %% Admete
  \tag #'(admete basse) {
    \clef "vtaille" r4 R2.*8 |
    r4 r <>^\markup\character Admete sib |
    fa' fa' re'\trill |
    mib' mib'4.( re'16) mib' |
    re'2\trill fa'4 |
    sol'4. fa'8 mib'4\trill |
    re' do'4.\trill do'8 |
    do'2 fa'4 |
    sib4. do'8 re'4 |
    mib' do'4.\trill sib8 |
    sib2 re'8. mib'16 |
    do'2\trill sib4 |
    la2\trill sib8 do' |
    sib2\trill sol4 |
    sol' re' sol' |
    mi'2\trill do'4 |
    fa' do' fa' |
    re'\trill sib sib |
    fa' fa' re' |
    mib' mib'( re'8) mib' |
    re'2\trill fa'4 |
    sol'4. fa'8 mib'4 |
    re' do'4.\trill do'8 |
    do'2 fa'4 |
    sib4. do'8 re'4 |
    mib' <<
      \tag #'basse {
        do'4 s s2.*9 s4
        \ffclef "vtaille" <>^\markup\character Admete
      }
      \tag #'admete {
        do'4. sib8 |
        sib4 r r |
        R2.*8 |
        r4
      }
    >> re'8 re'16 re' re'8. la16 |
    sib8 sol r mib'16 mib' mib'8 mib'16 re' |
    re'4\trill re'8 re' mi' fa' fa'8. mi'16 |
    fa'2 r4 fa |
    do'8. do'16 do'8. si16 si8.\trill do'16 |
    do'4 sol' r16 fa' mib' re' do'8\trill do'16 do' |
    la4\trill la <<
      \tag #'basse {
        s4 s2.*9 s4
        \ffclef "vtaille" <>^\markup\character Admete
      }
      \tag #'admete { r4 R2.*9 r4 }
    >> fa'8. re'16 sib8. sib16 fa8 sol16 lab |
    sol4\trill sol8 r16 mib' do'8.\trill do'16 do'8. re'16 |
    si4. do'8 re'8. mib'16 re'8.\trill do'16 |
    do'4 r16 mi' mi' sol' do'8. sib16 sib8. la16 |
    la4\trill la r8 fa'16 fa' do'8\trill do'16 re' |
    mib'4. mib'16 mib' mib'8 mib'16 re' |
    re'4\trill sib16 do' re' mi' fa'4. fa'16 mi' |
    fa'4 fa' <<
      \tag #'basse {
        s4 s2.*9 s4
        \ffclef "vtaille" <>^\markup\character Admete
      }
      \tag #'admete { r4 R2.*9 r4 }
    >> fa8 fa16 fa sib do' re' mi' |
    fa'8 fa' r fa' do'16\trill do' do' re' mib'8. fa'16 |
    re'2\trill re'8. mib'16 |
    do'2\trill sib4 |
    la2\trill sib8 do' |
    sib2\trill sol4 |
    sol' re' sol' |
    mi'2\trill do'4 |
    fa'4 do' fa' |
    re'\trill sib sib |
    fa' fa' re' |
    mib' mib'( re'8) mib' |
    re'2\trill fa'4 |
    sol'4. fa'8 mib'4 |
    re' do'4.\trill do'8 |
    do'2 fa'4 |
    sib4. do'8 re'4 |
    mib' <<
      \tag #'basse { do'4 }
      \tag #'admete {
        do'4.\trill sib8 |
        sib4 r r |
        R2.*9 |
        r4
      }
    >>
  }
  %% Chœur
  \tag #'(vdessus vhaute-contre vtaille vbasse basse) {
    \tag #'vdessus \clef "vdessus"
    \tag #'vhaute-contre \clef "vhaute-contre"
    \tag #'vtaille \clef "vtaille"
    \tag #'vbasse \clef "vbasse"
    <<
      \tag #'basse {
        s4 s2.*32 s2 \ffclef "vdessus"
      }
      \tag #'(vdessus vhaute-contre vtaille vbasse) {
        r4 R2.*32 r4 r
      }
    >>
    \keepWithTag #'(vdessus vhaute-contre vtaille vbasse basse) \choeur
    <<
      \tag #'(vdessus basse) { sib'4 }
      \tag #'vhaute-contre { re'4 }
      \tag #'vtaille { sib4 }
      \tag #'vbasse { sib,4 }
    >>
    <<
      \tag #'basse {
        s2 s2. s1*2 s2. s1 s2 \ffclef "vdessus"
      }
      \tag #'(vdessus vhaute-contre vtaille vbasse) {
        r4 r | R2. | R1*2 R2. R1 | r4 r
      }
    >>
    \choeur
    <<
      \tag #'(vdessus basse) { sib'4 }
      \tag #'vhaute-contre { re'4 }
      \tag #'vtaille { sib4 }
      \tag #'vbasse { sib,4 }
    >>
    <<
      \tag #'basse {
        s2. s1*4 s2. s1 s2 \ffclef "vdessus"
      }
      \tag #'(vdessus vhaute-contre vtaille vbasse) {
        r4 r2 | R1*4 R2. R1 | r4 r
      }
    >>
    \choeur
    <<
      \tag #'(vdessus basse) { sib'4 }
      \tag #'vhaute-contre { re'4 }
      \tag #'vtaille { sib4 }
      \tag #'vbasse { sib,4 }
    >>
    <<
      \tag #'basse {
        s2 s1 s2.*15 s2 \ffclef "vdessus"
      }
      \tag #'(vdessus vhaute-contre vtaille vbasse) {
        r4 r | R1 R2.*15 | r4 r
      }
    >>
    \choeur
    <<
      \tag #'(vdessus basse) { sib'2. }
      \tag #'vhaute-contre { re' }
      \tag #'vtaille { sib }
      \tag #'vbasse { sib, }
    >>
  }
>>