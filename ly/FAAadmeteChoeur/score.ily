\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff <<
        { s4 s2.*33 \noHaraKiri }
        \global \includeNotes "dessus"
      >>
      \new Staff <<
        { s4 s2.*33 \noHaraKiri }
        \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        { s4 s2.*33 \noHaraKiri }
        \global \includeNotes "taille"
      >>
      \new Staff <<
        { s4 s2.*33 \noHaraKiri }
        \global \includeNotes "quinte"
      >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        { s4 s2.*33 \noHaraKiri }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s4 s2.*33 \noHaraKiri }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s4 s2.*33 \noHaraKiri }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        { s4 s2.*33 \noHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'admete \includeNotes "voix"
    >> \keepWithTag #'admete \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*8 s2 \bar "" \break
        s4 s2.*5\break s2.*6\break s2.*6\pageBreak
        s2.*5 s2 \bar "" \break s4 s2.*5\pageBreak
        s2.*5 s2 \bar "" \break s4 s2. s1\pageBreak
        s1 s2. s1\break s2.*6\pageBreak
        s2.*4 s1\break s1*2\pageBreak
        s1*2\break s2. s1 s2 \bar "" \break
        s4 s2.*5\pageBreak
        s2.*5 \break s1 s2 \bar "" \pageBreak
        s4 s2.*4\break s2.*5\break s2.*5\pageBreak
        s2.*5\pageBreak
      }
      \modVersion { s4 s2.*8 s2 \bar "" \break }
    >>
  >>
  \layout { }
  \midi { }
}
