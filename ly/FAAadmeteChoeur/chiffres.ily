s4 <6>2. <6 5>4 <7->\figExtOn <7->\figExtOff s2. s2 <6 _->4
s4 <6>\figExtOn <6>\figExtOff s2.*2 <6 5>2. s
<6>2. <6 5>4 <7->\figExtOn <7->\figExtOff s2 <6>4 s2 <6 _->4
s4 <6>\figExtOn <6>\figExtOff s2.*2 <6 5>2. s <5/> <4>2 \new FiguredBass <_+>4
s2. <6> s <6> s <6> s4 <7->\figExtOn <7->\figExtOff s2 <6>4
s2 <6 _->4 s4 <6> <5/> s2.*2 <6 5>2

\setMusic #'choeur {
  s4 s2. s2 <_->4 s2. <_+>2 <6>4 <_-> <_+>2
  <_+>2\figExtOn <_+>4\figExtOff s2. s2 <6>4 <6 5>
  <7->\figExtOn <7->\figExtOff
}
\keepWithTag #'() \choeur
s2 <6>4 s <6-> <6 5/> s2 <6 4+ 2>8 <6> <7> <6> s1
<6>4 <7> <6+> <_->2. <6>4 s2
\choeur
s2... <7->16 s2.. <6 5 _->8 <_+>4. <6>8 <6+> <_-> <_+>4
<_+>2. <7>4 s2. <6 4+>8 <6+> <_->2 <5/>4 s2 <6>4 <7>8 <6> s2
\choeur
s2. <6>2 <5/> s2. <6 5/> <4>2 \new FiguredBass <_+>4 s2. <6 5/> s
<6> s <6> s4 <7->2 s2 <6>4 s2 <6 _->4
s4 <6>\figExtOn <6>\figExtOff s2.*2 <6 5>2
\choeur
