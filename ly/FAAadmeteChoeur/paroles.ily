\tag #'(admete basse) {
  Al -- cide est vain -- queur du tré -- pas,
  l’en -- fer ne luy re -- sis -- te pas,
  l’en -- fer ne luy re -- sis -- te pas.
  Il ra -- meine Al -- ces -- te vi -- van -- te ;
  que cha -- cun chan -- te,
  que cha -- cun chan -- te,
  Al -- cide est vain -- queur du tré -- pas,
  l’en -- fer ne luy re -- sis -- te pas,
  l’en -- fer ne luy
  \tag #'admete { re -- sis -- te pas. }
  \tag #'basse { re -- sis - }
}
\tag #'(choeur basse) {
  Al -- cide est vain -- queur du tré -- pas,
  l’en -- fer ne luy re -- sis -- te pas.
  L’En -- fer l’en -- fer ne luy re -- sis -- te pas.
}
\tag #'(admete basse) {
  Quel -- le dou -- leur se -- cret -- te
  rend mon ame in -- qui -- et -- te,
  et trou -- ble mon a -- mour.
  Al -- ces -- te voit en -- cor le jour,
  mais c’est pour un au -- tre qu’Ad -- me -- te.
}
\tag #'(choeur basse) {
  Al -- cide est vain -- queur du tré -- pas,
  l’en -- fer ne luy re -- sis -- te pas.
  L’En -- fer l’en -- fer ne luy re -- sis -- te pas.
}
\tag #'(admete basse) {
  Ah ! du moins ca -- chons ma tris -- tes -- se ;
  Al -- ces -- te dans ces Lieux ra -- mei -- ne les plai -- sirs.
  Je dois rou -- gir de ma foi -- bles -- se,
  quel -- le honte à mon cœur de mes -- ler des sous -- pirs
  a -- vec tant de cris d’al -- le -- gres -- se.
}
\tag #'(choeur basse) {
  Al -- cide est vain -- queur du tré -- pas,
  l’en -- fer ne luy re -- sis -- te pas.
  L’En -- fer l’en -- fer ne luy re -- sis -- te pas.
}
\tag #'(admete basse) {
  Par une ar -- deur im -- pa -- ti -- en -- te
  cou -- rons, & de -- van -- çons ses pas.
  Il ra -- meine Al -- ces -- te vi -- van -- te,
  que cha -- cun chan -- te.
  Que cha -- cun chan -- te,
  Al -- cide est vain -- queur du tré -- pas,
  l’en -- fer ne luy re -- sis -- te pas,
  l’en -- fer ne luy
  \tag #'admete { re -- sis -- te pas. }
  \tag #'basse { re -- sis - }
}
\tag #'(choeur basse) {
  Al -- cide est vain -- queur du tré -- pas,
  l’en -- fer ne luy re -- sis -- te pas.
  L’En -- fer l’en -- fer ne luy re -- sis -- te pas.
}
