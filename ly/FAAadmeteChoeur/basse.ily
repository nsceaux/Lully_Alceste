\clef "basse"
\setMusic #'choeur {
  fa,4 |
  sib,2 sib,4 |
  mib mib do |
  fa2 re4 |
  sol4. sol8 mib4 |
  do sol,4. do8 |
  do2 do'4 |
  fa2 fa4 |
  sib,4. do8 re4 |
  mib fa fa, |
}
sib4 |
la2 sib4 |
mib fa fa, |
sib,2 sib,4 |
mib4. re8 do4 |
sib,4 mib mi |
fa2 re4 |
sol4. la8 sib4 |
mib fa fa, |
sib,2 <<
  \tag #'basse { r4 | R2.*23 | r4 r }
  \tag #'basse-continue {
    sib4 |
    la2 sib4 |
    mib fa fa, |
    sib,4. do8 re4 |
    mib4. re8 do4 |
    sib, mib mi |
    fa2 re4 |
    sol4. la8 sib4 |
    mib fa fa, |
    sib,2 sib4 |
    fad2 sol4 |
    re2 re,4 |
    sol,2 sol8 la |
    si2. |
    do'2 do'8 sib |
    la4. sol8 fa4 |
    sib2 sib4 |
    la2 sib4 |
    mib fa fa, |
    sib,4. do8 re4 |
    mib4. re8 do4 |
    sib, mib mi |
    fa2 re4 |
    sol4. la8 sib4 |
    mib fa
  }
>>
\keepWithTag #'() \choeur
sib,4 <<
  \tag #'basse { r4 r | R2. | R1*2 R2. R1 | r4 r }
  \tag #'basse-continue {
    sib4 fad |
    sol2 la4 |
    sib2 sib8 la sol4 |
    fa1 | \allowPageTurn
    mib4 re2 |
    do2. mi,4 |
    fa,2
  }
>>
\choeur
<<
  \tag #'basse { sib,4 r r2 | R1*4 R2. R1 | r4 r }
  \tag #'basse-continue {
    sib,1 |
    mib2 lab4. fa8 |
    sol4. mib8 re do sol,4 |
    do1 | \allowPageTurn
    fa2. mib8 re |
    do2 la,4 |
    sib,2 la,4 sol, |
    fa,2
  }
>>
\choeur
<<
  \tag #'basse { sib,4 r4 r | R1 R2.*15 | r4 r }
  \tag #'basse-continue {
    sib,2. |
    la,1 |
    sib,2 sib4 |
    fad2 sol4 |
    re2 re,4 |
    sol,2 sol8 la |
    si2. |
    do'2 do'8 sib |
    la4. sol8 fa4 |
    sib2 sib4 |
    la2 sib4 |
    mib fa fa, |
    sib,4. do8 re4 |
    mib4. re8 do4 |
    sib, mib mi |
    fa2 re4 |
    sol4. la8 sib4 |
    mib fa
  }
>>
\choeur
<<
  \tag #'basse { sib,2. | }
  \tag #'basse-continue {
    sib,4. do8 sib,\trill la, |
    \once\set Staff.whichBar = "|"
    \custosNote sol,8
  }
>>

