\clef "haute-contre"
\setMusic #'choeur {
  la'4 |
  sib' sib' sib' |
  sib' sib' do'' |
  la'2 re''4 |
  si'4. si'8 do''4 |
  do'' si'4. do''8 |
  do''2 sol'4 |
  fa'2 la'4 |
  sib'4. sib'8 sib'4 |
  sib' la'4. sib'8 |
}
fa'4 |
fa'2 fa'4 |
sol'8 la' la'4. sib'8 |
sib'2 re''4 |
sol'4. sol'8 la'4 |
sib' sol' do'' |
la'2 la'4 |
sol' sib'2 |
sib'4 la'4. sib'8 |
sib'2 r4 |
R2.*23 |
r4 r \keepWithTag #'() \choeur
sib'4 r r |
R2. |
R1*2 R2. R1 |
r4 r \choeur
sib'4 r r2 |
R1*4 R2. R1 |
r4 r \choeur
sib'4 r r |
R1 R2.*15 |
r4 r \choeur
sib'2. |
