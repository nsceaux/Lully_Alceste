\clef "taille"
\setMusic #'choeur {
  fa'4 |
  fa' fa' fa' |
  sol' sol'4. sol'8 |
  fa'2 fa'4 |
  sol'4. sol'8 sol'4 |
  sol'4 sol'4. sol'8 |
  mi'2 mi'4 |
  fa'2 fa'4 |
  fa'4. fa'8 fa'4 |
  mib'! mib'4. re'8 |
}
re'4 |
do' fa'2 |
mib'8 re' do'4. sib8 |
sib4 fa'2 |
mib' mib'4 |
fa' mib' sol' |
do'2 re'4 |
re' sol' fa' |
mib' mib'4. re'8 |
re'2 r4 |
R2.*23 |
r4 r \keepWithTag #'() \choeur
re'4 r r |
R2. |
R1*2 R2. R1 |
r4 r \choeur
re'4 r r2 |
R1*4 R2. R1 |
r4 r \choeur
re'4 r r |
R1 R2.*15 |
r4 r \choeur
re'2. |
