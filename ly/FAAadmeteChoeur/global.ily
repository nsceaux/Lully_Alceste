\key fa \major \midiTempo#144 \tempo "Simphonie"
\digitTime\time 3/4 \partial 4
s4 s2.*42 \midiTempo#72 s2.*2
\time 4/4 s1
\time 2/2 \midiTempo#144 s1
\digitTime\time 3/4 \midiTempo#72 s2.
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#144 s2.*10
\time 4/4 \midiTempo#72 s1*5
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#144 s2.*10 \midiTempo#72 s2.
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#144 s2.*26 \bar "|."
