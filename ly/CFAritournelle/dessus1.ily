\clef "dessus" r4 re'' mi'' fa''8 mi'' |
re''4.\trill re''8 re''4 mi'' |
fad''4. fad''8 fad''4 sol'' |
la''2. la''4 |
re''4. re''8 sol''4. sol''8 |
sol''4. fa''8 fa''4. sol''8 |
mi''2 r8 mi'' la'' sol'' |
fad''4. re''8 sol''4. sol''8 |
sol''4. fad''8 fad''4.\trill mi''16 fad'' |
sol''1 |
