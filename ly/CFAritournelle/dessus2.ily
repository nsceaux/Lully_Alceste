\clef "dessus" r4 si' do'' re''8 do'' |
si'4. si'8 si'4 dod'' |
re''2 re''4 re'' |
re''4. la'8 do''4. do''8 |
do''4 si' si' do'' |
re''2 re''4. re''8 |
sol'4 r16 sol' la' si' do''4. do''8 |
do''4 re''8 do'' si'4 do''8 si' |
la'4. la'8 la'4.\trill sol'8 |
sol'1 |
