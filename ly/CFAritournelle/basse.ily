\clef "basse" sol,1~ |
sol,4. sol8 fad4 mi |
re4. re8 re4 mi |
fad4. fad8 fad4. fad8 |
sol4. sol8 sol4. la8 |
si4. si,8 si,4. si,8 |
do4. do8 la,2 |
re mi4 do |
re2 re, |
sol,1 |
