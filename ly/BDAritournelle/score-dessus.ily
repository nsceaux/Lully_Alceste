\score {
  \new GrandStaff <<
    \new Staff <<
      \global \includeNotes "dessus1"
      { s1*5\break }
    >>
    \new Staff << \keepWithTag #'() \global \includeNotes "dessus2" >>
  >>
  \layout { }
}