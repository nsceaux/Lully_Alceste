\score {
  \new StaffGroup <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \keepWithTag #'() \global \includeNotes "dessus2" >>
    >>
    \new Staff <<
      \keepWithTag #'() \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*4\break }
    >>
  >>
  \layout { }
  \midi { }
}