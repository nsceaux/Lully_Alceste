\clef "basse" fa,1~ |
fa,4. fa8 re2 |
do1 |
do'4. do'8 sib4. la8 |
sol4. la8 sib4. sol8 |
re'4. re'8 do'4. sib8 |
la4. la8 sib4. sol8 |
do'4. fa8 do2 |
fa,1 |
