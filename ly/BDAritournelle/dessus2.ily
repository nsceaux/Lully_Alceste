\clef "dessus" r4 la' sib'2 |
la'4. fa'8 la'4. si'8 |
do''4. si'8 do''4. re''8 |
mi''4. re''8 mi''4. fad''8 |
sol''4. sol''8 re''4. mi''8 |
fa''4. mi''8 fa''4. sol''8 |
la''4. la''8 re''4. sol''8 |
mi''4. fa''8 mi''4.\trill fa''8 |
fa''1 |
