\clef "dessus" r4 do'' re''4. mi''8 |
fa''4. fa''8 fa''4. sol''8 |
mi''4.\trill re''8 mi''4. fad''8 |
sol''4. fad''8 sol''4. la''8 |
sib''2. sib''4 |
la''4.\trill sol''8 la''4. sib''8 |
do'''4. do'''8 fa''4. sib''8 |
sol''4. la''8 sol''4.\trill fa''8 |
fa''1 |
