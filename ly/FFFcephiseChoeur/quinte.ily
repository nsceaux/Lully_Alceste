\clef "quinte" R2.*55 |
r4 r la8 la |
sol2 sib8 sib |
do'2 fa'8 fa' |
re'2 sib4 |
fa2 fa4 |
fa2 r4 |
R2.*4 |
r4 r re'8 re' |
sib2 re'8 re' |
do'2 fa'8 fa' |
re'2 sib4~ |
sib fa4. fa8 |
fa2 r4 |
R2.*3 |
r4 r re'8 re' |
re'2 la'8 la' |
la'2 re'8 re' |
re'2 mi'4 |
dod'2 r4 |
r r re'8 re' |
re'2 r4 |
r r do'8 do' |
do'2 r4 |
r r re'8 re' |
do'2 la8 la |
sib2 sol4 |
la la r |
R2.*3 |
r4 r re'8 re' |
re'2 r4 |
r r do'8 do' |
do'2 r4 |
r r sib8 sib |
do'2 do'8 do' |
do'2 la8 la |
sib2 sol4 |
sol2. |
la2 r4 |
R2.*3 |
r4 r re'8 re' |
do'2 do'8 do' |
sib2 re'8 re' |
sol'2 sol'4 |
re' re' re' |
si2 sol4 |
re'2 re'4 |
sib2 la4 |
sol la4. la8 |
sib4 re'4. re'8 |
re'2 re'4 |
sol2 sol4 |
re'2 re'4~ |
re'8 re' re'4 la |
sib4 r r |
R2.*3 |
r4 r re'8 re' |
do'2 do'8 do' |
sib2 re'8 re' |
sol'2 sol'4 |
re'4 re' re' |
si2 sol4 |
re'2 re'4 |
sib2 la4 |
sol sol4. la8 |
sib2 r4 |
R2.*35 |
r4 r re'8 re' |
do'2 do'8 do' |
sib2 re'8 re' |
sol'2 sol'4 |
re' re' re' |
si2 sol4 |
re'2 re'4 |
sib2 la4 |
sol sol4. la8 |
sib2
