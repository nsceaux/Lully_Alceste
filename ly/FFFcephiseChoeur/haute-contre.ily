\clef "haute-contre" R2.*55 |
r4 r fad'8 fad' |
sol'2 sol'8 sol' |
fa'2 fa'8 fa' |
fa'2 sol'4 |
fa'2. |
re'2 r4 |
R2.*4 |
r4 r fad'8 fad' |
sol'2 sib'8 sib' |
la'2 do''8 do'' |
sib'2 sib'4 |
sib'4( la'2) |
sib'2 r4 |
R2.*3 |
r4 r sib'8 sib' |
la'2 la'8 re'' |
dod''2 la'8 la' |
sib'2 sib'4 |
la'2 r4 |
r r re''8 re'' |
si'2 r4 |
r r do''8 do'' |
la'2 r4 |
r r sib'8 sib' |
do''2 do''8 re'' |
sib'4. la'8 sib'4 |
la' la' r |
R2.*3 |
r4 r la'8 re'' |
si'2 r4 |
r r do''8 do'' |
la'2 r4 |
r r sib'8 sib' |
la'2 la'8 si' |
do''2 do''8 do'' |
sib'2 re''4~ |
re'' do''2\trill |
re''2 r4 |
R2.*3 |
r4 r sib'8 sib' |
do''2 do''8 do'' |
re''2 re''8 re'' |
do''2 mib''4 |
re'' re'' re'' |
re''2 do''4 |
do''2 la'4 |
sib'2 r8 do'' |
sib'4( la'4.)\trill sol'8 |
sol'4 re''8 do'' re'' sib' |
la'4. la'8 re''4 |
re''4 do''4. do''8 |
do''4 la' sib'~ |
sib'8 do'' re''4. re''8 |
re''4 r r |
R2.*3 |
r4 r sib'8 sib' |
do''2 do''8 do'' |
re''2 re''8 re'' |
do''2 mib''4 |
re''4 re'' re'' |
re''2 do''4 |
do''2 la'4 |
sib'2 r8 do'' |
sib'4( la'4.\trill) sol'8 |
sol'2 r4 |
R2.*35 |
r4 r sib'8 sib' |
do''2 do''8 do'' |
re''2 re''8 re'' |
do''2 mib''4 |
re'' re'' re'' |
re''2 do''4 |
do''2 la'4 |
sib'2 r8 do'' |
sib'4( la'4.)\trill sol'8 |
sol'2
