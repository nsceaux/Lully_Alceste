\clef "basse"
<<
  \tag #'basse {
    R2.*55 |
    r4 r re8 re |
    sol2
  }
  \tag #'basse-continue {
    sol4. la8 sib4 |
    do'2. |
    re'2 sol4 |
    re2 mi4 |
    fad2 sol4 |
    sol,4. la,8 sib,4 |
    do2. |
    re2 sib,4 |
    sol, re,2 |
    sol,2. |
    sol2 re8 mib |
    fa4 sib,2 |
    mib do4 |
    fa2 sib,4 |
    fa,2 fa4 |
    sib,4. do8 re4 |
    mib do2 |
    fa sib,4~ |
    sib, fa,2 |
    sib,4. do8 re mib |
    fa2 re4 |
    mib do2 |
    re mi4 |
    fad2 re4 |
    sol sol, re |
    sol4. la8 sib4 |
    do'2. |
    re'2 sol4 |
    re2 mi4 |
    fad2 sol4 |
    sol,4. la,8 sib,4 |
    do2. |
    re2 sib,4 |
    sol, re,2 |
    sol, sol4 |
    fad2. |
    sol2 fa4 |
    mib si,2 |
    do do4 |
    fa4. sol8 la4 |
    sib4. la8 sol4 |
    fa do' do |
    fa4. sol8 fa mi |
    re4 sol2 |
    la sol4 |
    fa sib sol |
    la la, re |
    sol4. la8 sib4 |
    do'2. |
    re'2 sol4 |
    re2 mi4 |
    fad2 sol4 |
    sol,4. la,8 sib,4 |
    do2. | \allowPageTurn
    re2 sib,4 |
    sol,4 re,2 |
    sol,
  }
>> sol8 sol |
la2 la8 la |
sib2 mib4 |
fa fa,2 |
sib,2 <<
  \tag #'basse { r4 R2.*4 r4 r }
  \tag #'basse-continue {
    sib,4 |
    fa mib re |
    do2 do4 |
    sol2 fa4 |
    mib2. |
    re2
  }
>> re8 re |
sol2 sol8 sol |
la2 la8 la |
sib2 mib4 |
fa2 fa,4 |
sib,2 <<
  \tag #'basse { r4 R2.*3 r4 r }
  \tag #'basse-continue {
    sib,4 |
    do2 la,4 |
    re2. |
    sol,4 re,2 |
    sol,2
  }
>> sol4 |
re2 re4 |
la2 fa4 |
sib2 sol4 |
la2 <<
  \tag #'basse { r4 | r4 r }
  \tag #'basse-continue { la,4 | re2 }
>> re4 |
sol2 <<
  \tag #'basse { r4 | r4 r }
  \tag #'basse-continue { sol,4 | do2 }
>> do8 do |
fa2 <<
  \tag #'basse { r4 | r4 r }
  \tag #'basse-continue { fa,4 | sib,2 }
>> sib8 sib |
la2 la8 sib |
sol2 do'4 |
fa fa <<
  \tag #'basse { r4 | R2.*3 | r4 r }
  \tag #'basse-continue {
    fa, |
    sol,2. |
    la,2 re4~ |
    re la,2 |
    re,2
  }
>> re8 re |
sol2 <<
  \tag #'basse { r4 | r4 r }
  \tag #'basse-continue { sol,4 | do2 }
>> do4 |
fa2 <<
  \tag #'basse { r4 | r4 r }
  \tag #'basse-continue { fa,4 | sib,2 }
>> sib,8 sib, |
fa2 fa8 fa |
do2 do8 do |
sol2 fa4 |
mib2. |
re2 <<
  \tag #'basse { r4 | R2.*3 | r4 r }
  \tag #'basse-continue {
    re4 |
    re2 sol,4 |
    fad,2. |
    sol,4 re,2 |
    sol,
  }
>> sol4 |
la2 la4 |
sib2 sib8 sib |
do'2 do'4 |
re' re' re |
mi2 mi4 |
fad2 fad4 |
sol2 do4 |
re2 re,4 |
sol,4 sol2 |
fad4 fa2 |
mi4 mib2 |
re sib,4 |
sol, re,2 |
sol,4 <<
  \tag #'basse { r4 r4 | R2.*3 | r4 r }
  \tag #'basse-continue {
    sol2 |
    re8 mib fa4 mib8 re |
    do2. |
    do4 re re, |
    sol,2
  }
>> sol8 sol |
la2 la8 la |
sib2 sib8 sib |
do'2 do'4 |
re'4 re' re |
mi2 mi4 |
fad2 fad4 |
sol2 do4 |
re2 re,4 |
sol,2 <<
  \tag #'basse { r4 R2.*35 r4 r }
  \tag #'basse-continue {
    sol4 |
    re2 sol4 |
    do2 do4 |
    fa2 fa4 |
    sib2 sol4 |
    la2 la4 |
    re2 re4 |
    sol2 do4 |
    sol,2. |
    do2 do4 |
    fa2 sib,4 |
    fa2 fa4 |
    sib,2 sib4 |
    la2. |
    sol2 sol4 |
    re2 sol4 |
    do2 do4 |
    sol8 fa mib2 |
    re2 re4 |
    sol2 sol4 |
    la2 la4 |
    sib2 sib8 sib |
    la4 sol4. fa8 |
    fa2 fa4 |
    sol2 sol4 |
    la2 la4 |
    sol la la, |
    re,2 re4 |
    sol2 sol4 |
    do2 do4 |
    fa fa4. fa8 |
    sib,2 sib,8 sib, |
    mib4 mib do |
    re2 re8 mi |
    fad2 fad4 |
    sol re re, |
    sol,2
  }
>> sol8 sol |
la2 la8 la |
sib2 sib8 sib |
do'2 do'4 |
re' re' re |
mi2 mi4 |
fad2 fad4 |
sol2 do4 |
re2 re,4 |
sol,2
