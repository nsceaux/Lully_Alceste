\tag #'(cephise basse) {
  C’est la sai -- son d’ai -- mer
  quand on sçait plai -- re,
  c’est la sai -- son d’ai -- mer
  quand on sçait char -- mer.
  Les plus beaux de nos jours ne du -- rent gué -- re,
  Le sort de la beau -- té nous doit al -- lar -- mer,
  nos champs n’ont point de fleur plus pas -- sa -- ge -- re ;
  c’est la sai -- son d’ai -- mer
  quand on sçait plai -- re,
  c’est la sai -- son d’ai -- mer
  quand on sçait char -- mer.
  Un peu d’a -- mour est ne -- ces -- sai -- re,
  il n’est ja -- mais trop tost de s’en -- fla -- mer ;
  nous don -- ne- t’on un cœur pour n’en rien fai -- re ?
  C’est la sai -- son d’ai -- mer
  quand on sçait plai -- re,
  c’est la sai -- son d’ai -- mer
  quand on sçait \tag #'cephise { char -- mer. }
}

\tag #'(vdessus basse vhaute-contre vtaille vbasse) {
  Tri -- om -- phez, tri -- om -- phez, ge -- ne -- reux Al -- ci -- de,
  \tag #'(vdessus basse) {
    ai -- mez, ai -- mez en paix heu -- reux es -- poux.
  }
  Tri -- om -- phez, tri -- om -- phez, ge -- ne -- reux Al -- ci -- de,
  \tag #'(vdessus basse) {
    ai -- mez en paix heu -- reux es -- poux.
  }
  Tri -- om -- phez, tri -- om -- phez, ge -- ne -- reux Al -- ci -- de,
  \tag #'(vdessus basse) { ai -- mez, }
  tri -- om -- phez,
  \tag #'(vdessus basse) { ai -- mez, }
  tri -- om -- phez,
  \tag #'(vdessus basse) { ai -- mez, }
  tri -- om -- phez, ge -- ne -- reux Al -- ci -- de,
  \tag #'(vbasse basse) {
    ai -- mez en paix heu -- reux es -- poux.
  }
  Tri -- om -- phez,
  \tag #'(vdessus basse) { ai -- mez, }
  tri -- om -- phez,
  \tag #'(vdessus basse) { ai -- mez, }
  tri -- om -- phez, tri -- om -- phez, ge -- ne -- reux Al -- ci -- de,
  \tag #'(vdessus basse) {
    ai -- mez en paix heu -- reux es -- poux.
  }
  Tri -- om -- phez, tri -- om -- phez, ge -- ne -- reux Al -- ci -- de,
  ai -- mez, ai -- mez en paix heu -- reux es -- poux.
  
  Tri -- om -- phez, tri -- om -- phez, ge -- ne -- reux Al -- ci -- de,
  ai -- mez, ai -- mez en paix heu -- reux es -- poux.
}
\tag #'(vdessus vtaille basse) {
  Que tou -- jours la gloi -- re vous gui -- de.
}
\tag #'(vhaute-contre vbasse basse) {
  Que sans ces -- se l’a -- mour vous gui -- de.
}
\tag #'(vdessus vtaille basse) {
  Que tou -- jours la gloi -- re vous gui -- de.
}
\tag #'(vhaute-contre vbasse basse) {
  Que sans ces -- se l’a -- mour vous gui -- de.
}
\tag #'(vdessus vtaille basse) {
  Joü -- is -- sez à ja -- mais des hon -- neurs les plus doux,
  des hon -- neurs les plus doux.
}
\tag #'(vhaute-contre vbasse basse) {
  Joü -- is -- sez joü -- is -- sez à ja -- mais des plai -- sirs les plus doux.
}
\tag #'(vdessus vtaille basse) {
  Joü -- is -- sez à ja -- mais des hon -- neurs les plus doux.
  Joü -- is -- sez à ja -- mais des hon -- neurs les plus doux.
  Joü -- is -- sez à ja -- mais des hon -- neurs les plus doux.
}
\tag #'(vhaute-contre vbasse basse) {
  Joü -- is -- sez à ja -- mais des plai -- sirs les plus doux.
  Joü -- is -- sez à ja -- mais des plai -- sirs des plai -- sirs les plus doux.
}
\tag #'(vdessus basse vhaute-contre vtaille vbasse) {
  Tri -- om -- phez, tri -- om -- phez, ge -- ne -- reux Al -- ci -- de,
  ai -- mez, ai -- mez en paix heu -- reux es -- poux.
}
