\clef "dessus" R2.*55 |
r4 r la'8 la' |
sib'2 sib'8 sib' |
do''2 do''8 do'' |
re''2 mib''4 |
re''( do''2)\trill |
sib'2 r4 |
R2.*4 |
r4 r la'8 la' |
sib'2 sib'8 sib' |
do''2 do''8 do'' |
re''2 mib''4 |
re''( do''2)\trill |
sib'2 r4 |
R2.*3 |
r4 r re''8 mi'' |
fa''2 re''8 re'' |
mi''2 la''8 la'' |
re''2 sol''4 |
mi''2 r4 |
r r fa''8 fa'' |
re''2 r4 |
r r mib''8 mib'' |
do''2 r4 |
r r re''8 mi'' |
fa''2 fa''8 fa'' |
fa''2 mi''4 |
fa'' fa'' r |
R2.*3 |
r4 r la''8 la'' |
sol''2 r4 |
r r mib''8 mib'' |
do''2 r4 |
r r re''8 re'' |
do''2 do''8 re'' |
mib''2 mib''8 mib'' |
re''2 re''4 |
sol''4. fad''8 sol'' la'' |
fad''2 r4 |
R2.*3 |
r4 r sol''8 sol'' |
sol''2 fa''8 fa'' |
fa''2 sib''8 sib'' |
sol''2 do'''4 |
la'' la'' fad'' |
sol''2 sol''4 |
la''2 la''4 |
re''2 r8 sol'' |
sol''4( fad''4.) sol''8 |
sol''4 sib''8 la'' sib'' sol'' |
la''4 re''8 mi'' fa'' re'' |
sol''4 sol''8 fad'' sol'' la'' |
fad'' mi'' fad'' re'' sol''4~ |
sol''8 la'' la''4. sol''8 |
sol''4 \twoVoices #'(dessus1 dessus2 dessus) <<
  { re''8 do'' re'' mib'' |
    fa''4 do''8 sib' do'' re'' |
    mib'' fa'' sol'' fa'' mib'' re'' |
    do''8 sib' la'4. sol'8 |
    sol'2 }
  { sib'8 la' sib' do'' |
    re''4 la'8 sol' la' si' |
    do'' re'' mib'' re'' do'' sib' |
    la' sol' fad'4. sol'8 |
    sol'2 }
>> sol''8 sol'' |
sol''2 fa''8 fa'' |
fa''2 sib''8 sib'' |
sol''2 do'''4 |
la''4 la'' fad'' |
sol''2 sol''4 |
la''2 la''4 |
re''2 r8 sol'' |
sol''4( fad''4.) sol''8 |
sol''2 r4 |
R2.*35 |
r4 r sol''8 sol'' |
sol''2 fa''8 fa'' |
fa''2 sib''8 sib'' |
sol''2 do'''4 |
la'' la'' fad'' |
sol''2 sol''4 |
la''2 la''4 |
re''2 r8 sol'' |
sol''4( fad''4.) sol''8 |
sol''2
