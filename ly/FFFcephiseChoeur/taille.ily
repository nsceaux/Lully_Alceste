\clef "taille" R2.*55 |
r4 r re'8 re' |
re'2 re'8 re' |
do'2 do'8 do' |
sib2 sib4 |
sib( la2)\trill |
sib2 r4 |
R2.*4 |
r4 r re'8 re' |
re'2 sol'8 sol' |
fa'2 fa'8 fa' |
fa'2 sol'4 |
fa' fa'4. mib'8 |
re'2 r4 |
R2.*3 |
r4 r sol'8 sol' |
fa'2 fa'8 fa' |
mi'2 fa'8 fa' |
fa'2 mi'4 |
mi'2 r4 |
r r la'8 la' |
sol'2 r4 |
r r sol'8 sol' |
fa'2 r4 |
r r fa'8 sol' |
la'2 fa'8 fa' |
sol'2 sol'4 |
do' do' r4 |
R2.*3 |
r4 r fad'8 fad' |
sol'2 r4 |
r r mib'8 mib' |
fa'2 r4 |
r r fa'8 fa' |
fa'2 fa'8 fa' |
sol'2 sol'8 sol' |
sol'2 la'4 |
sib' do''2 |
la' r4 |
R2.*3 |
r4 r sol'8 sol' |
mi'2 fa'8 fa' |
fa'2 fa'8 fa' |
mib'2 sol'4 |
fad'4 fad' la' |
sol'2 do''4 |
la'2 la'4 |
sol'2 mib'4 |
re' re'4. re'8 |
re'4 sol'4. sol'8 |
re'8 mi' fa' sol' la' fa' |
sol'2 sol'4 |
la'8 sol' la' fad' sib'4~ |
sib'8 la'16 sol' fad'4. sol'8 |
sol'4 r r |
R2.*3 |
r4 r sol'8 sol' |
mi'2 fa'8 fa' |
fa'2 fa'8 fa' |
mib'2 sol'4 |
fad'4 fad' la' |
sol'2 do''4 |
la'2 la'4 |
sol'2 mib'4 |
re' re'4. re'8 |
re'2 r4 |
R2.*35 |
r4 r sol'8 sol' |
mi'2 fa'8 fa' |
fa'2 fa'8 fa' |
mib'2 sol'4 |
fad'4 fad' la' |
sol'2 sol'4 |
la'2 la'4 |
sol'2 mib'4 |
re' re'4. re'8 |
re'2
