\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff << \global \keepWithTag #'dessus \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
      \new Staff << \global \keepWithTag #'basse \includeNotes "basse" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'cephise \includeNotes "voix"
    >> \keepWithTag #'cephise \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*6\break s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*7\break s2.*7\break
        s2.*6\break s2.*6\break s2.*2\pageBreak
        s2.*5\pageBreak
        s2.*6 s2 \bar "" \pageBreak
        s4 s2.*6\pageBreak
        s2.*5\pageBreak
        s2.*5 s2 \bar "" \pageBreak
        s4 s2.*5\pageBreak
        s2.*5 s2 \bar "" \pageBreak
        s4 s2.*5\pageBreak
        s2.*6\pageBreak
        s2.*6\pageBreak
        s2.*8\pageBreak
        s2.*5\pageBreak
        s2.*6\pageBreak
        s2.*5\break s2.*5\pageBreak
        s2.*5\break s2.*5\pageBreak
        s2.*5\break s2.*4\pageBreak
        s2.*6 s2 \bar "" \pageBreak
        s4 s2.*4\pageBreak
      }
        \modVersion {
          s2.*55%\pageBreak
          s2.*111 s2 \break
        }
    >>
  >>
  \layout { }
  \midi { }
}
