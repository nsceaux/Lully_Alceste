<<
  \tag #'(cephise basse) {
    \clef "vdessus" <>^\markup\character Céphise
    sib'4. do''8 re''4 |
    mib''4 do''2\trill |
    la'2 sib'4 |
    fad'2 sol'16[ fad'8 sol'16] |
    la'2 re'4 |
    sib'4. do''8 re''4 |
    mib'' do''2\trill |
    la'\trill sib'4~ |
    sib'8 do'' la'4.\trill sol'8 |
    sol'2. |
    re''4. mib''8 fa''4 |
    do'' re''2 |
    sib' do''4 |
    la'2\trill sib'16[ la'8 sib'16] |
    do''2 do''4 |
    re''4. mib''8 fa''4 |
    sib' mib''2 |
    do''\trill re''4~ |
    re''8 mib'' do''4.\trill sib'8 |
    sib'2. |
    la'4.\trill la'8 sib'4 |
    sol' la'2 |
    fad'2 sol'4 |
    la'2 sib'16[ la'8 sib'16] |
    sib'2\trill la'4 |
    sib'4. do''8 re''4 |
    mib'' do''2\trill |
    la' sib'4 |
    fad'2 sol'16[ fad'8 sol'16] |
    la'2 re'4 |
    sib'4. do''8 re''4 |
    mib'' do''2\trill |
    la' sib'4~ |
    sib'8 do'' la'4.\trill sol'8 |
    sol'2. |
    re''4. mib''8 do''4 |
    si'2 si'4 |
    do'' re''2\trill |
    mib'' mib''4 |
    do'' do'' fa'' |
    re''4.\trill do''8 sib'4 |
    la' sol'4.\trill do''8 |
    la'2.\trill |
    fa''4 mi''4.\trill re''8 |
    dod''4. re''8 mi''4 |
    la' re''4. mi''8 |
    mi''2\trill re''4 |
    sib'4. do''8 re''4 |
    mib''4 do''2\trill |
    la'\trill sib'4 |
    fad'2 sol'16[ fad'8 sol'16] |
    la'2 re'4 |
    sib'4. do''8 re''4 |
    mib'' do''2\trill |
    la'\trill sib'4~ |
    <<
      \tag #'basse { sib'8 do'' la'4 }
      \tag #'cephise {
        sib'8 do'' la'4.\trill sol'8 |
        sol'2. |
      }
    >>
  }
  \tag #'vdessus {
    \clef "vdessus" R2.*55 | r4 r
  }
  \tag #'vhaute-contre { \clef "vhaute-contre" R2.*55 | r4 r }
  \tag #'vtaille { \clef "vtaille" R2.*55 | r4 r }
  \tag #'vbasse { \clef "vbasse" R2.*55 | r4 r }
>>
%% Chœur
<<
  \tag #'cephise R2.*29
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "vdessus"
    <>^\markup\character Chœur
    la'8 la' |
    sib'2 sib'8 sib' |
    do''2 do''8 do'' |
    re''2 mib''4 |
    re''( do''2)\trill |
    sib'2 <<
      \new Voice \with { autoBeaming = ##f } {
        \voiceOne re''4 |
        do''2 re''4 |
        mib''2 mib''4 |
        re''2 re''4 |
        re''4( \once\override Script.avoid-slur = #'inside do''8.[\trill sib'16]) do''4 |
        re''2
      }
      { \voiceTwo sib'4 |
        la'2 si'4 |
        do''2 sib'8[ la'] |
        sib'2 la'4 |
        sol'4.( fad'8) sol'4 |
        fad'2 \oneVoice }
    >> la'8 la' |
    sib'2 sib'8 sib' |
    do''2 do''8 do'' |
    re''2 mib''4 |
    re''( do''2)\trill |
    sib'2 <<
      \new Voice \with { autoBeaming = ##f } {
        \voiceOne re''4 |
        mib''2 do''4 |
        do''2. |
        s8 sib' la'4. sol'8 |
        sol'2
      }
      { \voiceTwo sib'4 |
        sol'2 la'4 |
        fad'2. |
        \oneVoice r8 \voiceTwo sol' fad'4. sol'8 |
        sol'2 \oneVoice }
    >> sib'8 sib' |
    la'2 re''8 re'' |
    dod''2 re''8 re'' |
    re''2 mi''4 |
    dod''4 dod'' <<
      \new Voice \with { autoBeaming = ##f } { \voiceOne mi''4 | fa''2 }
      { \voiceTwo dod''4 | re''2 \oneVoice }
    >> fa''8 fa'' |
    re''2 <<
      \new Voice \with { autoBeaming = ##f } { \voiceOne re''4 | mib''2 }
      { \voiceTwo si'4 | do''2 \oneVoice }
    >> mib''8 mib'' |
    do''2 <<
      \new Voice \with { autoBeaming = ##f } { \voiceOne do''4 | re''2 }
      { \voiceTwo la'4 | sib'2 \oneVoice }
    >> re''8 mi'' |
    fa''2 fa''8 fa'' |
    fa''2 mi''4 |
    fa'' fa''
  }
  \tag #'vhaute-contre {
    fad'8 fad' |
    sol'2 sol'8 sol' |
    fa'2 fa'8 fa' |
    fa'2 sol'4 |
    fa'2. |
    re'2 r4 |
    R2.*4 |
    r4 r fad'8 fad' |
    sol'2 sol'8 sol' |
    fa'2 fa'8 fa' |
    fa'2 sol'4 |
    fa'2. |
    re'2 r4 |
    R2.*3 |
    r4 r sol'8 sol' |
    fa'2 fa'8 fa' |
    mi'2 fa'8 fa' |
    fa'2 sol'4 |
    mi'4 mi' r |
    r r la'8 la' |
    sol'2 r4 |
    r r sol'8 sol' |
    fa'2 r4 |
    r r fa'8 sol' |
    la'2 fa'8 fa' |
    sol'2 sol'4 |
    do' do'
  }
  \tag #'vtaille {
    re'8 re' |
    re'2 re'8 re' |
    do'2 do'8 do' |
    sib2 sib4 |
    sib( la2)\trill |
    sib2 r4 |
    R2.*4 |
    r4 r re'8 re' |
    re'2 re'8 re' |
    do'2 do'8 do' |
    sib2 sib4 |
    sib( la2) |
    sib2 r4 |
    R2.*3 |
    r4 r re'8 re' |
    re'2 la8 la |
    la2 la8 la |
    sib2 sib4 |
    la4 la r |
    r r re'8 re' |
    si2 r4 |
    r r do'8 do' |
    la2 r4 |
    r r sib8 sib |
    do'2 do'8 do' |
    sib2 sib4 |
    la la
  }
  \tag #'vbasse {
    re8 re |
    sol2 sol8 sol |
    la2 la8 la |
    sib2 mib4 |
    fa2. |
    sib,2 r4 |
    R2.*4 |
    r4 r re8 re |
    sol2 sol8 sol |
    la2 la8 la |
    sib2 mib4 |
    fa2. |
    sib,2 r4 |
    R2.*3 |
    r4 r sol8 sol |
    re2 re8 re |
    la2 fa8 fa |
    sib2 sol4 |
    la4 la r |
    r r re8 re |
    sol2 r4 |
    r r do8 do |
    fa2 r4 |
    r r sib8 sib |
    la2 la8 sib |
    sol2 do'4 |
    fa fa
  }
>>
%% Alcide
<<
  \tag #'(vdessus vhaute-contre vtaille) { r4 R2.*3 r4 r }
  \tag #'(vbasse basse) {
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    la4 |
    sib2 sol4 |
    sol2 r8 fa |
    fa4( mi4.\trill) re8 |
    re2
  }
>>
%% Chœur
<<
  \tag #'(vdessus basse) {
    \tag #'basse { \ffclef "vdessus" <>^\markup\character Chœur }
    la'8 re'' |
    si'2 <<
      \new Voice \with { autoBeaming = ##f } { \voiceOne re''4 | mib''2 }
      { \voiceTwo si'4 | do''2 \oneVoice }
    >> mib''8 mib'' |
    do''2 <<
      \new Voice \with { autoBeaming = ##f } { \voiceOne do''4 | re''2 }
      { \voiceTwo la'4 | sib'2 \oneVoice }
    >> re''8 re'' |
    do''2 do''8 re'' |
    mib''2 mib''8 mib'' |
    re''2 re''4 |
    re''( do''2)\trill |
    re''2 <<
      \new Voice \with { autoBeaming = ##f } {
        \voiceOne re''4 |
        la'2 sib'4 |
        do''2. |
        s8 sib' la'4. sol'8 |
        sol'2
      }
      { \voiceTwo la'4 |
        fad'2 sol'4 |
        la'2. |
        \oneVoice r8 \voiceTwo sol' fad'4. sol'8 |
        sol'2 \oneVoice }
    >> sib'8 sib' |
    do''2 do''8 do'' |
    re''2 re''8 re'' |
    do''2 do''4 |
    la' la' re'' |
    re''2 do''4 |
    do''2 la'4 |
    sib'2 r8 do'' |
    sib'4( la'4.)\trill sol'8 |
    sol'2 r4 |
  }
  \tag #'vhaute-contre {
    fad'8 fad' |
    sol'2 r4 |
    r r sol'8 sol' |
    fa'2 r4 |
    r r fa'8 fa' |
    fa'2 fa'8 fa' |
    sol'2 sol'8 sol' |
    sol'2 re'4 |
    sol'2. |
    fad'2 r4 |
    R2.*3 |
    r4 r sol'8 sol' |
    sol'2 fa'8 fa' |
    fa'2 sib'8 sib' |
    sol'2 sol'4 |
    fad' fad' fad' |
    sol'2 sol'4 |
    la'2 la'4 |
    sol'2 r8 sol' |
    sol'4( fad'4.) sol'8 |
    sol'2 r4 |
  }
  \tag #'vtaille {
    re'8 re' |
    re'2 r4 |
    r r do'8 do' |
    la2 r4 |
    r r sib8 sib |
    la2 la8 sib |
    do'2 do'8 do' |
    sib2 la4 |
    sol( do'2) |
    la2 r4 |
    R2.*3 |
    r4 r re'8 re' |
    do'2 do'8 do' |
    sib2 re'8 re' |
    mib'2 mib'4 |
    re' re' la |
    sol2 sol4 |
    re'2 re'4 |
    re'2 r8 mib' |
    re'2 la4 |
    sib2 r4 |
  }
  \tag #'vbasse {
    \ffclef "vbasse" re8 re |
    sol2 r4 |
    r r do8 do |
    fa2 r4 |
    r r sib,8 sib, |
    fa2 fa8 fa |
    do2 do8 do |
    sol2 fa4 |
    mib2. |
    re2 r4 |
    R2.*3 |
    r4 r sol8 sol |
    la2 la8 la |
    sib2 sib8 sib |
    do'2 do'4 |
    re' re' re |
    mi2 mi4 |
    fad2 fad4 |
    sol2 r8 do |
    re4.( do8) re4 |
    sol,2 r4 |
  }
>>
R2.*8 |
%% Chœur
<<
  \tag #'(vdessus basse) {
    r4 r sib'8 sib' |
    do''2 do''8 do'' |
    re''2 re''8 re'' |
    do''2 do''4 |
    la'4 la' re'' |
    re''2 do''4 |
    do''2 r8 la' |
    sib'2 r8 do'' |
    sib'4( la'4.)\trill sol'8 |
    sol'2
  }
  \tag #'vhaute-contre {
    r4 r sol'8 sol' |
    sol'2 fa'8 fa' |
    fa'2 sib'8 sib' |
    sol'2 sol'4 |
    fad'4 fad' fad' |
    sol'2 sol'4 |
    la'2 r8 la' |
    sol'2 r8 sol' |
    sol'4( fad'4.) sol'8 |
    sol'2
  }
  \tag #'vtaille {
    r4 r re'8 re' |
    do'2 do'8 do' |
    sib2 re'8 re' |
    mib'2 mib'4 |
    re'4 re' la |
    sol2 sol4 |
    re'2 re'4 |
    re'2 r8 mib' |
    do'2 la4 |
    sib2
  }
  \tag #'vbasse {
    r4 r sol8 sol |
    la2 la8 la |
    sib2 sib8 sib |
    do'2 do'4 |
    re'4 re' re |
    mi2 mi4 |
    fad2 fad4 |
    sol2 do4 |
    re4.( do8) re4 |
    sol,2
  }
>>
%% Alceste & Admete
<<
  \tag #'(vdessus basse) {
    \ffclef "vbas-dessus" <>^\markup\character Alceste sib'8 do''
    la'2 si'4 |
    do''2 do''8 sib' |
    la'4 la'
  }
  \tag #'vtaille {
    \ffclef "vtaille" <>^\markup\character Admete sol'8 la' |
    fa'2 re'4 |
    mib'2 mib'8 re' |
    do'4 do'
  }
  \tag #'(vhaute-contre vbasse) { r4 R2.*2 r4 r }
>>
%% Lycas & Alcide
<<
  \tag #'(vhaute-contre basse) {
    \ffclef "vhaute-contre" <>^\markup\character Lychas fa'8 fa' |
    re'4. re'8 mi'4 |
    dod'4.( si8) dod'4 |
    re' re'
  }
  \tag #'vbasse {
    \ffclef "vbasse-taille" <>^\markup\character Alcide fa8 fa |
    sib4. sib8 sol4 |
    la2 la4 |
    re re
  }
  \tag #'(vdessus vtaille) { r4 R2.*2 r4 r }
>>
%% Alceste & Admete
<<
  \tag #'(vdessus basse) {
    \tag #'basse { \ffclef "vbas-dessus" <>^\markup\character Alceste }
    la'8. re''16 |
    si'2 do''4 |
    do''4. re''8 si'4 |
    do'' do''
  }
  \tag #'vtaille {
    fad'8. fad'16 |
    sol'2 mib'4 |
    re'4. mib'8 fa'4 |
    mib' do'
  }
  \tag #'(vhaute-contre vbasse) { r4 R2.*2 r4 r }
>>
%% Lycas & Alcide
<<
  \tag #'(vhaute-contre basse) {
    \tag #'basse { \ffclef "vhaute-contre" <>^\markup\character Lychas }
    mib'8 mib' |
    mib'4. fa'8 re'4 |
    do'4.( sib8) do'4 |
    re' re'
  }
  \tag #'vbasse {
    do'8 sib |
    la4. la8 sib4 |
    fa2 fa4 |
    sib,4 sib,
  }
  \tag #'(vdessus vtaille) { r4 R2.*2 r4 r }
>>
%% Alceste & Admete
<<
  \tag #'(vdessus basse) {
    \tag #'basse { \ffclef "vbas-dessus" <>^\markup\character Alceste }
    re''8. re''16 |
    do''2 do''8[ sib'16] la' |
    sib'2 sib'8 do'' |
    la'4 la' si' |
    do''2 sol'8. la'16 |
    sib'4 la'4. sol'8 |
    fad'2
  }
  \tag #'vtaille {
    fa'8. fa'16 |
    fad'4 fad'4. fad'8 |
    sol'2 re'8. mi'16 |
    fa'4 fa' re' |
    mib'2 mib'8. fa'16 |
    re'4. mib'8 do'4 |
    re'2
  }
  \tag #'(vhaute-contre vbasse) { r4 R2.*5 r4 r }
>>
%% Lycas & Alcide
<<
  \tag #'(vhaute-contre basse) {
    \tag #'basse { \ffclef "vhaute-contre" <>^\markup\character Lychas }
    la8. la16 |
    sib2 sib8. sib16 |
    do'4 do'4. do'8 |
    re'2 re'8 mi' |
    fa'4 sib( la8) sib |
    la2
  }
  \tag #'vbasse {
    re8. re16 |
    sol2 sol8. sol16 |
    la4 la4. la8 |
    sib2 sib8 sib |
    la4 sol4. fa8 |
    fa2
  }
  \tag #'(vdessus vtaille) { r4 R2.*4 r4 r }
>>
%% Alceste & Admete
<<
  \tag #'(vdessus basse) {
    \tag #'basse { \ffclef "vbas-dessus" <>^\markup\character Alceste }
    la'8 la' |
    sib'4 si'4. si'8 |
    dod''2 dod''8 dod'' |
    re''4 dod''4. re''8 |
    re''2
  }
  \tag #'vtaille {
    fa'8 fa' |
    fa'4 mi'4. mi'8 |
    mi'2 mi'8 mi' |
    fa'8.[ sol'16] mi'4. re'8 |
    re'2
  }
  \tag #'(vhaute-contre vbasse) { r4 R2.*3 r4 r }
>>
%% Tous
<<
  \tag #'(vdessus basse) {
    la'8 la' |
    sib'4 si'4. si'8 |
    do''2 sol'8 sol' |
    la'4 la'4. la'8 |
    sib'2 sib'8 sib' |
    sol'4 sol' la' |
    fad'2 r4 |
    r re''4. la'8 |
    sib'8[ do''] la'4. sol'8 |
    sol'2
  }
  \tag #'vhaute-contre {
    fad'8 fad' |
    sol'4 sol'4. sol'8 |
    mi'2 mi'8 mi' |
    fa'4 fa'4. fa'8 |
    fa'2 fa'8 fa' |
    mib'4 mib' mib' |
    re'2 fad'8 sol' |
    la'4 la'4. la'8 |
    sol'[ la'] fad'4. sol'8 |
    sol'2
  }
  \tag #'vtaille {
    re'8 re' |
    re'4 re'4. re'8 |
    do'2 do'8 do' |
    do'4 do'4. do'8 |
    re'2 re'8 do' |
    sib4 sib do' |
    la2 r4 |
    r re'4. re'8 |
    re'4 re' la |
    sib2
  }
  \tag #'vbasse {
    re8 re |
    sol4 sol4. sol8 |
    do2 do8 do |
    fa4 fa4. fa8 |
    sib,2 sib,8 sib, |
    mib4 mib do |
    re2 re8 mi |
    fad4 fad4. fad8 |
    sol4 re4. re8 |
    sol,2
  }
>>
%% Chœur
<<
  \tag #'(vdessus basse) {
    \ffclef "vdessus" <>^\markup\character Chœur sib'8 sib' |
    do''2 do''8 do'' |
    re''2 re''8 re'' |
    do''2\trill do''4 |
    la'\trill la' re'' |
    re''2 do''4 |
    do''2 la'4 |
    sib'2 r8 do'' |
    sib'4( la'4.\trill) sol'8 |
    sol'2
  }
  \tag #'vhaute-contre {
    \ffclef "vhaute-contre" sol'8 sol' |
    sol'2 fa'8 fa' |
    fa'2 sib'8 sib' |
    sol'2 sol'4 |
    fad' fad' fad' |
    sol'2 sol'4 |
    la'2 la'4 |
    sol'2 r8 sol' |
    sol'4( fad'4.) sol'8 |
    sol'2
  }
  \tag #'vtaille {
    \ffclef "vtaille" re'8 re' |
    do'2 do'8 do' |
    sib2 re'8 re' |
    mib'2 mib'4 |
    re' re' la |
    sol2 sol4 |
    re'2 re'4 |
    re'2 r8 mib' |
    re'2 la4 |
    sib2
  }
  \tag #'vbasse {
    \ffclef "vbasse" sol8 sol |
    la2 la8 la |
    sib2 sib8 sib |
    do'2 do'4 |
    re' re' re |
    mi2 mi4 |
    fad2 fad4 |
    sol2 r8 do |
    re4.( do8) re4 |
    sol,2
  }
>>
