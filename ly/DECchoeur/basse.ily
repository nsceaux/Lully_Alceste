\clef "basse" do1. |
si,2 sib,1 |
lab,1. |
sol, |
do |
si,2 sib,1 |
lab,1. |
sol,1 sol2 |
do'1. |
si2 sib1 |
lab1. |
sol1 sol,2 |
do1. |
sib, |
lab, |
sol,1 sol,2 |
do1. |
fa |
sib |
mib1 mib2 |
lab1 fa2 |
do'1 do'4 do' |
sib2 sib do' |
lab1. |
sol1 sol2 |
mib1 mib2 |
re1 re2 |
do do2. do4 |
re2 re mib |
re1. |
sol,2 sol2. sol4 |
do2 do2. do4 |
fa2 fa2. fa4 |
sib,2 sib2. sib4 |
mib1 mib2 |
fa sol1 |
lab fa2 |
sol2 sol,1 |
do do2 |
fa, sol,1 |
lab, fa,2 |
sol,1. |
\tag #'basse-continue \allowPageTurn
do1 do2 |
sib, sib1 |
lab1 lab4 lab |
sol2 sol lab |
fa1. |
mib2. mib4 mib2 |
re do2. do4 |
sib,2 sib,2. sib,4 |
do1 reb2 |
do1. |
fa,2 fa1 |
sol fa2 |
mib1 re2 |
do1 do2 |
fa sol1 |
lab1 fa2 |
sol sol,1 |
do1 do'2 |
sol1 sol2 |
lab1 fa2 |
sib sib sol |
do' do' lab |
sib sib,1 |
mib2 mib2. mib4 |
mi2 mi2. mi4 |
fa2 fa2. fa4 |
fad2 fad2. fad4 |
sol2 sol2. sol4 |
lab2 lab fa |
sol( sol,1) |
do1. |
