\piecePartSpecs
#`((dessus #:score "score-dessus")
   (haute-contre)
   (taille)
   (quinte)
   (basse #:system-count 7)
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#73 #}))
