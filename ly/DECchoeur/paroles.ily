Que nos pleurs, que nos cris
que nos pleurs, que nos cris re -- nou -- vel -- lent sans ces -- se
al -- lons por -- ter par tout la dou -- leur qui nous pres -- se.

Que nos pleurs, que nos cris re -- nou -- vel -- lent sans ces -- se
al -- lons por -- ter par tout la dou -- leur qui nous pres -- se.
\tag #'(vdessus basse) {
  Al -- lons por -- ter par tout la dou -- leur
  la dou -- leur qui nous pres -- se.
}
Al -- lons por -- ter par tout la dou -- leur qui nous pres -- se.
Que nos pleurs, que nos cris re -- nou -- vel -- lent sans ces -- se.
