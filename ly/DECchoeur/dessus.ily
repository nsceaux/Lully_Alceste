\clef "dessus"
<<
  \tag #'violons {
    r2 do''2. do''4 |
    re''2 sol'2. sol'4 |
    sol'2 fa'2.\trill mib'8 fa' |
    sol'2 sol''2. re''4 |
    mib''2 mib'' do'' |
    re'' re''2. mib''4 |
    do''2\trill do''2. re''4 |
    si'1 sol''4 sol'' |
    sol''1 lab''2 |
    lab'' sol''2. sol''4 |
    sol''2 fa''2.\trill fa''4 |
    fa''2 mib''4. fa''8 re''2 |
    mib'' sol'2. la'4 |
    sib'2 sol'2.\trill sol'4 |
    do''1 do''4 re'' |
    mib''2 si'2. si'4 |
    do''2 mi''2. mi''4 |
    fa''1 r2 |
    r fa''2. fa''4 |
    sol''2 r r |
    R1.*10 |
  }
  \tag #'(flute1 flute2) { R1.*30 }
>>
<<
  \tag #'flute1 {
    r2 <>^"Fluttes" re''2. re''4 |
    mib''1 r2 |
    r fa''2. do''4 |
    re''1 r2 |
    r mib'' mib'' |
    mib'' reb''2. reb''4 |
    do''1 do''2 |
    do''2 si'2.\trill la'8 si' |
    do''2 r r |
    R1.*4 |
  }
  \tag #'flute2 {
    r2 si'2. si'4 |
    do''1 r2 |
    r la'!2. la'4 |
    sib'1 r2 |
    r sol'' sol'' |
    lab'' sib''1 |
    mib'' mib''2 |
    re''2 re''2.\trill do''4 |
    do''2 r r |
    R1.*4 |
  }
  \tag #'violons {
    R1. |
    r2 <>^\markup\whiteout Violons do''2. sol'4 |
    lab'2 r r |
    r sib'2. fa'4 |
    sol'1 r2 |
    R1.*3 |
    r2 mib'' mib'' |
    mib'' reb''2. mib''4 |
    do''1 do''2 |
    do'' si'2. la'8 si' |
    do''2 r r |
  }
>>
R1.*16 |
r2 r <<
  \tag #'(flute1 violons) {
    mib''2 |
    re''1 mib''2 |
    do''1 fa''2 |
    re''2\trill re'' re'' |
    mib'' mib'' mib'' |
    mib''( re''1)\trill |
    mib''1 r2 |
    r2 do''2. do''4 |
    la'1 r2 |
    r2 re''2. re''4 |
    si'2 sol''2. sol''4 |
    mib''2 mib'' fa''2 |
    mib''2( re''2.)\trill do''4 |
    do''1. |
  }
  \tag #'flute2 {
    do''2 |
    sib'1 sib'2 |
    lab'1 do''2 |
    sib'2 sib' re'' |
    do'' do'' do'' |
    sib'1( lab'2) |
    sol'1 r2 |
    r sol'2. sol'4 |
    fa'1 r2 |
    r2 la'2. la'4 |
    sol'2 si'2. si'4 |
    do''2 do'' do'' |
    do''( si'1) |
    do''1. |
  }
>>
