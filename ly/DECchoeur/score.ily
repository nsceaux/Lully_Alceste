\score {
  \new StaffGroupNoBar <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        \global \keepWithTag #'flute1 \includeNotes "dessus"
      >>
      \new Staff <<
        \global \keepWithTag #'flute2 \includeNotes "dessus"
      >>
    >>
    \new StaffGroupNoBracket <<
      \new Staff <<
        \global \keepWithTag #'violons \includeNotes "dessus"
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1.*7\break s1.*7\pageBreak
        s1.*6\pageBreak
        s1.*6\break s1.*4\pageBreak
        s1.*7\pageBreak
        s1.*7 s1 \bar "" \pageBreak
        s2 s1.*5\pageBreak
        s1.*6\pageBreak
        s1.*6\pageBreak
        s1.*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}