\clef "quinte" r2 do'2. do'4 |
sol2 sol1 |
lab4 sib do'2. do'4 |
re'2 re'2. do'4 |
do'2 mib' fa' |
re'1 sib2 |
do'1 do'2 |
re'2 re'2. re'4 |
do'2 mib' fa' |
re'1 sol'2 |
do'1 fa'2 |
re' sol2. sol4 |
sol1 sol2 |
sol1 sol2 |
lab4 sib do'2 fa' |
mib' re'2. re'4 |
do'2 do'2. do'4 |
do'1 r2 |
r sib2. sib4 |
sib2 r r |
R1.*11 | \allowPageTurn
r2 do'2. do'4 |
do'2 r2 r |
r sib2. sib4 |
sib1 r2 |
R1.*3 |
r2 do' do' |
do' sib sib |
do' do'1 |
sol1. |
sol2 r r |
R1.*16 |
r2 r do'2 |
re'1 sib2 |
do'1 do'2 |
re' sib sib |
do' mib' mib' |
sib1 sib2 |
sib1 r2 |
r sol2. sol4 |
la1 r2 |
r2 la2. re'4 |
re'2 sol2. sol4 |
do'2 lab lab |
sol1 sol2 |
sol1. |
