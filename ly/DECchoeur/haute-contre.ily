\clef "haute-contre" r2 mib'2. mib'4 |
re'2 re'2. re'4 |
do'2 do' fa' |
re'1 sol'2 |
sol' lab'2. lab'4 |
lab'2 sol'2. sol'4 |
sol'2 fa'2. mib'8 fa' |
sol'1 si'4 si' |
do''1 do''2 |
re'' re''2. mib''4 |
do''2 do''2. re''4 |
si'2 do''4. re''8 si'2 |
do'' sol'4 fa' mib'2 |
re'1 sol'2 |
sol'2 fa'2. fa'4 |
sol'2 sol'2. sol'4 |
sol'2 do''2. do''4 |
do''1 r2 |
r re''2. re''4 |
mib''2 r r |
R1.*11 | \allowPageTurn
r2 sol'2. sol'4 |
fa'2 r2 r |
r fa'2. fa'4 |
mib'1 r2 |
R1.*3 |
r2 sol' sol' |
lab' sib'1 |
lab' lab'2 |
sol'1. |
sol'2 r r |
R1.*16 |
r2 r do'' |
sib'1 sib'2 |
lab'1 do''2 |
sib'2 sib' re'' |
do'' do'' do'' |
sib'1( lab'2) |
sol'1 r2 |
r sol'2. sol'4 |
fa'1 r2 |
r la'2. la'4 |
sol'2 si'2. si'4 |
do''2 do''2 do'' |
do'' si'2. do''4 |
do''1. |
