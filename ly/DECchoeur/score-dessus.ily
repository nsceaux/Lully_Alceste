\score {
  \new StaffGroup <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff <<
        \global \keepWithTag #'flute1 \includeNotes "dessus"
      >>
      \new Staff <<
        \global \keepWithTag #'flute2 \includeNotes "dessus"
      >>
    >>
    \new Staff <<
      \global \keepWithTag #'violons \includeNotes "dessus"
      { s1.*30\break }
    >>
  >>
  \layout { }
}
