\clef "taille" r2 sol'2. sol'4 |
sol'1 sib2 |
do'2 do'2. do'4 |
do'2 si2. si4 |
do'2 do' fa' |
fa' sol'2. sol'4 |
lab'1 fa'4. fa'8 |
re'1 re'4 re' |
mib'2 sol' fa' |
fa' sol'2. sol'4 |
lab'1 fa'4. fa'8 |
sol'1 sol'4 fa' |
mib' re' do'2. do'4 |
re'2 re'2. re'4 |
do'1 do'2 |
do' re'2. re'4 |
mi'2 sol'2. sol'4 |
la'1 r2 |
r sib'2. sib'4 |
do''2 r r |
R1.*11 | \allowPageTurn
r2 mi'2. mi'4 |
fa'2 r2 r |
r re'2. re'4 |
mib'1 r2 |
R1.*3 |
r2 sol' sol' |
fa'1 fa'2 |
mib'1 mib'2 |
re'1. |
mib'2 r r |
R1.*16 |
r2 r sol' |
sol'1 sol'2 |
mib'1 lab'2 |
fa' fa' sol' |
sol' sol' lab' |
sol'( fa'1) |
mib'1 r2 |
r do'2. do'4 |
do'1 r2 |
r2 re'2. re'4 |
re'2 re'2. re'4 |
do'2 do' do' |
do' re'2. re'4 |
mib'1. |

