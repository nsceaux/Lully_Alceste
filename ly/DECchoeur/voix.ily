<<
  \tag #'(vdessus basse) {
    \clef "vdessus" R1.*15 |
    r2 <>^\markup\character Chœur si'2. si'4 |
    do''1 r2 |
    r do''2. do''4 |
    re''1 r2 |
    r mib''2. mib''4 |
    do''2\trill do'' fa'' |
    mib''1 fa''4 mib'' |
    re''2\trill re'' mib'' |
    do''1. |
    si'1 si'2 |
    do''1 do''2 |
    re''1 re''2 |
    mib''2 do''2. do''4 |
    do''2 do'' sib' |
    sib'( la'1)\trill |
    sol'2 r r |
    R1.*11 |
    r2 mib'' fa'' |
    re'' re'' mib'' |
    do''1 do''4 do'' |
    sib'2 sib' do'' |
    lab'1. |
    sol'2. sol'4 la'2 |
    sib'2 sib' la' |
    sib'2 sib'2. sib'4 |
    sib'2 sib' lab' |
    lab'( sol'1)\trill |
    fa'1 la'2 |
    si'1 si'2 |
    do''1 re''2 |
    mib''2 mib''2. mib''4 |
    mib''2 reb''2. reb''4 |
    do''1 do''4 do'' |
    do''1( si'2) |
    do''1 mib''2 |
    re''1 mib''2 |
    do''1 fa''2 |
    re''2\trill re'' re'' |
    mib'' mib'' mib'' |
    mib''( re''1)\trill |
    mib''2 sol'2. sol'4 |
    sol'1 r2 |
    r la'2. la'4 |
    la'1 r2 |
    r si'2. si'4 |
    do''2 do'' do'' |
    do''2( si'1) |
    \appoggiatura si'16 do''1. |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1.*15 |
    r2 re'2. re'4 |
    do'1 r2 |
    r fa'2. fa'4 |
    fa'1 r2 |
    r sol'2. sol'4 |
    mib'2 mib' lab' |
    sol'1 lab'4 lab' |
    lab'2 sol'2. sol'4 |
    sol'1( fa'2) |
    sol'1 re'2 |
    mib'!1 mib'2 |
    fa'1 fa'2 |
    sol'2 mi'2. mi'4 |
    fad'2 fad' sol' |
    sol'2( fad'1) |
    sol'2 r r |
    R1.*11 |
    r2 sol' lab' |
    lab' sol' sol' |
    sol'1 fa'4 fa' |
    fa'2 mib' mib' |
    mib'( re'1)\trill |
    mib'2. sib4 do'2 |
    re' mib'2. fa'4 |
    re'2 re'2. re'4 |
    mi'2 mi' fa' |
    fa'( mi'1)\trill |
    fa'1 r2 |
    R1.*6 |
    r2 r sol'2 |
    sol'1 sol'2 |
    mib'1 lab'2 |
    fa'2 fa' sol' |
    sol' sol' lab' |
    sol'( fa'1)\trill |
    mib'2 mib'2. mib'4 |
    do'1 r2 |
    r fa'2. fa'4 |
    re'1 r2 |
    r sol'2. sol'4 |
    mib'2 mib' fa' |
    mib'( re'1)\trill |
    do'1. |
  }
  \tag #'vtaille {
    \clef "vtaille" R1.*15 |
    r2 sol2. sol4 |
    sol1 r2 |
    r la2. la4 |
    sib1 r2 |
    r sib2. sib4 |
    lab2 lab do' |
    do'1 do'4 do' |
    re'2 re' do' |
    do'1( fa'2) |
    re'1 re'2 |
    sol1 do'2 |
    do'1 si2 |
    do' sol2. do'4 |
    fad2 la sol |
    re'( do'1) |
    si2 r r |
    R1.*11 |
    r2 do' do' |
    do' sib sib |
    mib'1 lab4 lab |
    sib2 sib lab |
    lab1. |
    sib2. mib'4 do'2 |
    sib do'2. do'4 |
    fa2 fa2. sib4 |
    sol2 sol fa |
    do'1( sib2) |
    la1 r2 |
    R1.*6 |
    r2 r do'2 |
    sib1 sib2 |
    lab1 do'2 |
    sib2 sib re' |
    do' do' do' |
    sib1( lab2) |
    sol2 sib2. sib4 |
    sol1 r2 |
    r do'2. do'4 |
    do'1 r2 |
    r re'2. re'4 |
    do'2 lab lab |
    sol1. |
    sol |
  }
  \tag #'vbasse {
    \clef "vbasse" R1.*15 |
    r2 sol2. sol4 |
    mi1 r2 |
    r fa2. fa4 |
    sib1 r2 |
    r mib2. mib4 |
    lab2 lab fa |
    do'1 do'4 do' |
    sib2 sib do' |
    lab1. |
    sol1 sol2 |
    mib1 mib2 |
    re1 re2 |
    do do2. do4 |
    re2 re mib |
    re1. |
    sol,2 r r |
    R1.*11 |
    r2 do do |
    sib, sib sib |
    lab1 lab4 lab |
    sol2 sol lab |
    fa1. |
    mib2. mib4 mib2 |
    re do2. do4 |
    sib,2 sib,2. sib,4 |
    do2 do reb |
    do1. |
    fa,1 r2 |
    R1.*6 |
    r2 r do' |
    sol1 sol2 |
    lab1 fa2 |
    sib2 sib sol |
    do' do' lab |
    sib( sib,1) |
    mib2 mib2. mib4 |
    mi1 r2 |
    r fa2. fa4 |
    fad1 r2 |
    r sol2. sol4 |
    lab2 lab fa |
    sol( sol,1) |
    do1. |
  }
>>