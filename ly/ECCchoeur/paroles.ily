Tout mor -- tel doit i -- cy pa -- rois -- tre,
on ne peut nais -- tre
que pour mou -- rir :
de cent maux le tres -- pas dé -- li -- vre ;
qui cherche à vi -- vre
cherche à souf -- frir.
ve -- nez tous sur nos som -- bres bords.
Le re -- pos qu’on de -- si -- re
ne tient son Em -- pi -- re
que dans le se -- jour des morts.
Le re -  morts.
