<<
  \tag #'(vdessus basse) {
    \clef "vdessus" do''8 sib' |
    la'4 la'8 la' la'4 sol' |
    la' la'8 sib' do''4 do'' |
    la' la'8 la' sib'4 sol' |
    la'2 r4 do''8 do'' |
    re''4 re''8 re'' re''4 do'' |
    sib' re''8 re'' re''4 do'' |
    do'' do''8 do'' do''4 si' |
    do''2 r4 mi''8 re'' |
    mi''4 mi''8 mi'' mi''4 fa'' |
    mi''2 r4 do''8 do'' |
    do''4 do''8 re'' sib'4 sib'8 do'' |
    la'4 la'8 sib' sol'4 sol'8 la' |
    fa'4 sib'8 sib' sol'4 do'' |
    la'2. do''8 do'' |
    la'2.
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" fa'8 fa' |
    fa'4 fa'8 fa' fa'4 mi' |
    fa' fa'8 fa' fa'4 mi' |
    re' re'8 re' re'4 do' |
    do'2 r4 fa'8 fa' |
    fa'4 fa'8 fa' fa'4 fa' |
    re' fa'8 fa' fa'4 fa' |
    fa' fa'8 fa' fa'4 re' |
    mi'2 r4 sol'8 sol' |
    sol'4 sol'8 sol' sol'4 sol' |
    sol'2 r4 mi'8 mi' |
    fa'4 fa'8 fa' mib'4 mib'8 mib' |
    re'4 fa'8 fa' mi'4 mi'8 fa' |
    re'4 fa'8 fa' fa'4 mi' |
    fa'2. mi'8 mi' |
    fa'2.
  }
  \tag #'vtaille {
    \clef "vtaille" la8 sib |
    do'4 do'8 do' do'4 do' |
    do' do'8 sib la4 sol |
    fa fa8 fa fa4 mi |
    fa2 r4 la8 la |
    sib4 sib8 sib sib4 la |
    sib sib8 sib sib4 sib |
    la la8 la la4 sol |
    sol2 r4 do'8 si |
    do'4 do'8 do' do'4 si |
    do'2 r4 sol8 sol |
    la4 la8 sib sol4 sol8 la |
    fa4 re'8 re' do'4 do'8 do' |
    sib4 re'8 re' do'4 do' |
    la2. sol8 sol |
    la2.
  }
  \tag #'vbasse {
    \clef "vbasse" fa8 fa |
    fa4 fa8 fa fa4 do |
    fa fa8 fa fa4 do |
    re re8 re sib,4 do |
    fa,2 r4 fa8 fa |
    sib4 sib8 sib sib4 fa |
    sib4 sib8 sib sib4 sib, |
    fa4 fa8 fa re4 sol |
    do2 r4 do8 sol, |
    do4 do8 do do4 sol, |
    do2 r4 do8 do |
    fa4 fa8 re mib4 mib8 do |
    re4 re8 sib, do4 do8 la, |
    sib,4 sib,8 sol, do4 do |
    fa,2. do8 do |
    fa,2.
  }
>>
