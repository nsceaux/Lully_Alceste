\key fa \major \midiTempo#120
\time 4/4 \partial 4 s4 s1*10
\bar "|!:" \segnoMark s1*3
\alternatives { s1 \segnoMarkEnd } s2. \bar "|."
