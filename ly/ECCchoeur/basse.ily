\clef "basse" fa8 fa |
fa4 fa8 fa fa4 do |
fa fa8 fa fa4 do |
re re8 re sib,4 do |
fa,2 r4 fa8 fa |
sib4 sib8 sib sib4 fa |
sib4 sib8 sib sib4 sib, |
fa4 fa8 fa re4 sol |
do2 r4 do8 sol, |
do4 do8 do do4 sol, |
do2 r4 do8 do |
fa4 fa8 re mib4 mib8 do |
re4 re8 sib, do4 do8 la, |
sib,4 sib,8 sol, do4 do |
fa,2. do8 do |
fa,2.
