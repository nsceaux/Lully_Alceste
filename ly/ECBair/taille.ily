\clef "taille" fa'8. fa'16 |
fa'4 fa'8 sol' sol'4 fa'8 mi' |
fa'2 fa'4. fa'8 |
mi'8 do'16 re' mi'8 fa'16 mi' re'4. re'8 |
do'4 mi'2 mi'4 |
re' sol' la'4. la'8 |
re'4 si r8 si16 do' re'8 si |
mi'4. re'16 do' si8 mi' re'8. do'16 |
do'2. mi'8. mi'16 |
re'4 re'8 re' re'4 re' |
re' re'8 re' re'4 re' |
re'4 sib2 re'8. re'16 |
do'8 mi' fa'4 fa'8 sib' sol'8. fa'16 |
fa'4 do'2 do'4 |
\setMusic #'reprise {
do'8 sol' sol'4 fa'4. fa'8 |
mi'4 sol'2 fa'8. fa'16 |
fa'8. re'16 re'8. re'16 mi'8 fa' mi'8. fa'16 |
fa'2. fa'4 |
fa' re' do'8. do'16 do'8. sib16 |
}
\keepWithTag #'() \reprise
la2. do'4 |
\reprise
la2.
