\clef "haute-contre" la'8. la'16 |
sib'4 sib'8. sib'16 sol'4 la'8 sib' |
la'4 la' r8 la'16 sib' do''8. do''16 |
do''4. do''8 re''4. re''8 |
sol'4 sol'2 sol'8. la'16 |
sib'4. sib'8 do''4. re''8 |
si'4 sol'2 sol'4 |
sol' sol' sol' sol'8.\trill fa'16 |
mi'2. mi'8. fad'16 |
sol'4 sol'8 sol' sol'4 fad' |
sol'16 fad' sol' la' sol'8 sol' sol'4 fad' |
sol'2. sib'8. sib'16 |
do''4. do''8 fa'8. fa'16 mi'8. fa'16 |
fa'2. fa'4 |
\setMusic #'reprise {
  sol' do'' do'' si' |
  do''2. do''8. do''16 |
  sib'4 sib'8. sib'16 do''4 do''8.\trill sib'16 |
  la'8 la'16 sib' do''8 do'' do''4. do''8 |
  fa'8 sib'16 la' sol' la' sol' fa' mi'8 fa' mi'8. fa'16 |
}
\keepWithTag #'() \reprise
fa'2. fa'4 |
\reprise
fa'2.
