\clef "quinte" do'8. do'16 |
sib4 re'8. sib16 do'4. do'8 |
do'2. fa'8. fa'16 |
sol'2. sol'8. sol'16 |
sol'2. do'4 |
re'4. re'8 do'4. do'8 |
\footnoteHere #'(0 . 0) \markup\wordwrap {
  Manuscrit : \raise#0.7 \score {
    { \tinyQuote \key fa \major \clef "alto"
      do'4 do' sol8. sol16 sol4 | }
    \layout { \quoteLayout }
  }
}
\sug { re'4 re' } sol8. sol16 sol4 |
do'4 do' sol8. sol16 sol4 |
sol2. do'8. do'16 |
sib4 sol8 sol sol4 la |
sol4 sol8 sol sol4 la |
sol4 re'2 sol8. sol16 |
sol4 fa fa do'8. do'16 |
do'2. do'4 |
\setMusic #'reprise {
do'4. do'8 re'4. re'8 |
sol2 do'4. do'8 |
re'4 re' do'8 do' do'4 |
do'2 do'4. do'8 |
sib8 fa sol4 sol8 fa do'8. do'16 |
}
\keepWithTag #'() \reprise
do'2. do'4 |
\reprise
do'2.
