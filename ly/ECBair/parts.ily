\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue #:system-count 4)
   (silence #:on-the-fly-markup , #{ \markup\tacet#33 #}))
