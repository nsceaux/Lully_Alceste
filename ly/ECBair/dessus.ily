\clef "dessus" do''8. fa''16 |
re''4\trill re''8. sol''16 mi''8\trill mi'' fa'' sol'' |
la''4 fa'' r8 fa''16 sol'' la''8 sib''16 la'' |
sol''8\trill mi''16 fa'' sol''8 la''16 sol'' fa''8 mi''16 fa'' re''8 mi''16 fa'' |
mi''4\trill do''2 mi''8. fad''16 |
sol''4 sol''8. sol''16 sol''4 fad''8.( mi''32 fad'') |
sol''2 r8 sol'16 la' si'8 sol' |
do''8 do''16 re'' mi''8 fa''16 mi'' re''8.\trill do''16 si'8. do''16 |
do''2. sol'8. la'16 sib'4 sib'8 sib' sib'4 la'\trill |
sib'16 la' sib' do'' sib'8 sib' sib'4 la'\trill |
sib'4 sol'2 sol''8. re''16 |
mi''8. do''16 fa''8. do''16 re''8. re''16 do''8.\trill sib'16 |
la'2 r8 fa'16 sol' la'8 fa' |
\setMusic #'reprise {
  do''8 do''16 re'' mi''8 do'' fa'' re''16 mi'' fa''8 re'' |
  sol''4 mi''2\trill la''8. la''16 |
  re''4 sol''8. sol''16 sol''8. la''16 sol''8.\trill fa''16 |
  fa''8 do''16 re'' mib''8 mib'' mib'' re''16 mib'' do''8 re''16 mib'' |
  re''8 re''16 do'' sib' do'' sib' la' sol'8 la' sol'8.\trill fa'16 |
}
\keepWithTag #'() \reprise
fa'2 r8 fa'16 sol' la'8 fa' |
\reprise
fa'2.
