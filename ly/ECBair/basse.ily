\clef "basse" fa8. fa16 |
sib,4 sib,8. sol,16 do8. sib,16 la,8. sol,16 |
fa,2 fa4. fa8 |
do'4 do'8 do' do'4 si |
do'2 do4. do8 |
sib,4. sib,8 la,4. la,8 |
sol,2 sol |
mi4 do sol8. do16 sol,4 |
do2. do8. do16 |
sol4 sol8 sol sol4 re |
sol4 sol8 sol sol4 re |
sol,2 sol4. sol8 |
do4 la, sib, do |
fa,2 fa4. fa8 |
\setMusic #'reprise {
mi4. mi8 re4. re8 |
do2 do'4 la |
sib sol do'8 fa do4 |
fa4. fa8 la,2 |
sib, do8 fa, do,4 |
}
\keepWithTag #'() \reprise
fa,2 fa4. fa8 |
\reprise
fa,2.
