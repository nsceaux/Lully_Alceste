\clef "quinte" la2. la4 |
sol2. sol4 |
la2 sib |
do' do'4. do'8 |
sib2 sol4. sol8 |
sol1 |
la2. do'4 |
sib2. re'4 |
do'2. do'4 |
la4 re' re' sib |
sol do' do'4. do'8 |
re'2 do'4. do'8 |
do'1 |
