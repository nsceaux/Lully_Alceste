\clef "haute-contre" fa'2. fa'4 |
sol'2 sol'4. do''8 |
do''2 sib' |
sib'4. do''8 la'4. la'8 |
sib'2 re''4. re''8 |
do''1 |
do''2. la'4 |
sib'2. sib'4 |
sol'4. sol'8 do''2 |
r4 la' re''4. re''8 |
do''2. do''4 |
sib'4. sib'8 sib'4. la'8 |
la'1 |
