\clef "dessus" fa''4. fa'8 la'4. fa'8 |
do''2. do''8 do'' |
fa''4. fa''8 fa''4. sol''8 |
mi''4.*5/6 mi''16 fa'' sol'' la''4. la''8 |
re''2 sol''4. sol''8 |
sol''1 |
fa''4. do''8 mib''4. fa''8 |
re''4. re''8 sol''2~ |
sol''4 mi'' la''2~ |
la''4 fa'' sib''2~ |
sib''4 do''' la''2~ |
la''4.*5/6 sib''16 la'' sol'' sol''4.\trill fa''8 |
fa''1 |
