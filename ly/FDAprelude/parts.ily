\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille #:system-count 2)
   (quinte #:system-count 2)
   (basse #:system-count 2)
   (basse-continue #:system-count 2)
   (silence #:on-the-fly-markup , #{ \markup\tacet#13 #}))
