\clef "taille" do'2. do'4 |
do'2. do'4 |
re'2. re'4 |
sol'2 fa'4. fa'8 |
fa'2. fa'4 |
mi'1 |
fa'2. fa'4 |
fa'4. fa'8 re'4. re'8 |
mi'2. mi'4 |
fa'2. fa'4 |
sol'2 fa'4. fa'8 |
fa'4.*5/6 sol'16 fa' mi' mi'4. fa'8 |
fa'1 |
