\clef "basse" fa,2 fa |
mi mib |
re1 |
do2 fa,4. fa,8 |
sib,2 si, |
do4. re8 do4. sib,8 |
la,2. fa,4 |
sib,2. sol,4 |
do2. la,4 |
re2. re4 |
mi2 fa4 fa, |
sib,2 do4 do, |
fa,1 |
