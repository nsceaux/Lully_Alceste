\bookpart {
  \paper { min-systems-per-page = 2 }
  \act "Acte Cinquiême"
  \sceneDescription\markup\wordwrap-center {
    Le Théatre Représente un Arc de Triomphe.
  }
  \scene "Scene Premiére" "Scene I"
  \sceneDescription\markup\wordwrap-center {
    Admete
  }
  %% 5-1
  \pieceToc\markup\wordwrap {
    Admete, chœur : \italic { Alcide est vainqueur du Trépas}
  }
  \includeScore "FAAadmeteChoeur"
}
\bookpart {
  \scene "Scene II" "Scene II"
  \sceneDescription\markup\wordwrap-center {
    Lychas, Straton.
  }
  %% 5-2
  \pieceToc\markup\wordwrap {
    Straton, Lychas : \italic { Ne m'osteras-tu point la chaine qui m'accable }
  }
  \includeScore "FBAstratonLychas"

  \scene "Scene Troisiême" "Scene III"
  \sceneDescription\markup\wordwrap-center {
    Céphise, Lychas, Straton.
  }
  %% 5-3
  \pieceToc\markup\wordwrap {
    Lychas, Straton, Céphise : \italic { Voy, Céphise, voy qui de nous }
  }
  \includeScore "FCAcephiseLychasStraton"
}
\bookpart {
  \scene "Scene Quatriême" "Scene IV"
  \sceneDescription\markup\wordwrap-center {
    Alcide, Admete, Alceste.
  }
  %% 5-4
  \pieceToc\markup\wordwrap {
    Alcide, Alceste, Admete : \italic { Pour une si belle victoire }
  }
  \includeScore "FDAprelude"
  \includeScore "FDBrecit"
}
\bookpart {
  \scene "Scene Cinquiême" "Scene V"
  \sceneDescription\markup\wordwrap-center {
    Apollon, les Muses.
  }
  %% 5-5
  \pieceToc "Prélude"
  \includeScore "FEAprelude"
  %% 5-6
  \pieceToc\markup\wordwrap {
    Apollon : \italic { Les Muses & les Jeux s’empressent de descendre }
  }
  \includeScore "FEBrecit"
}
\bookpart {
  \scene "Sixiême et derniére Scene" "Scene VI"
  %% 5-7
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Chantons, chantons, faisons entendre }
  }
  \includeScore "FFAchoeur"
}
\bookpart {
  %% 5-8
  \pieceToc\markup Premier Air
  \includeScore "FFBair"
}
\bookpart {
  %% 5-9
  \pieceToc\markup { \concat { 2 \super e } Air – Les Pastres }
  \includeScore "FFCair"
}
\bookpart {
  %% 5-10
  \pieceToc\markup\wordwrap {
    Straton : \italic { A quoy bon Tant de raison }
  }
  \includeScore "FFDstraton"
}
\bookpart {
  %% 5-11
  \pieceToc\markup { \concat { 3 \super e } Air – Menuet }
  \includeScore "FFEmenuet"
}
\bookpart {
  %% 5-12
  \pieceToc\markup\wordwrap {
    Céphise, chœur : \italic { C'est la saison d'aimer }
  }
  \includeScore "FFFcephiseChoeur"
  \actEnd Fin du Cinquiême et dernier Acte
}
