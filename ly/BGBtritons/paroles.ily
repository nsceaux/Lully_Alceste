\tag #'(couplet1 basse) {
  Mal -- gré tant d’o -- ra -- ges,
  et tant de nau -- fra -- ges,
  cha -- cun à son tour
  s’em -- barque a -- vec l’a -- mour.
  Par tout où l’on mei -- ne
  les cœurs a -- mou -- reux,
  on voit la mer plei -- ne
  d’es -- cueils dan -- ge -- reux,
  mais sans quel -- que pei -- ne
  on n’est ja -- mais heu -- reux :
  une a -- me cons -- tan -- te
  a -- pres la tour -- men -- te
  es -- pere un beau jour.
  Mal -- gré tant d’o -- ra -- ges,
  et tant de nau -- fra -- ges,
  cha -- cun à son tour
  s’em -- barque a -- vec l’a -- mour.
}
\tag #'couplet2 {
  Un cœur qui dif -- fe -- re
  d’en -- trer en af -- fai -- re
  s’ex -- pose à man -- quer
  le temps de s’em -- bar -- quer.
  Une a -- me com -- mu -- ne
  s’es -- ton -- ne d’a -- bord,
  le soin l’im -- por -- tu -- ne,
  le cal -- me l’en -- dort,
  mais quel -- le for -- tu -- ne
  fait- on sans quelque ef -- fort ?
  Est- il un com -- mer -- ce
  ex -- empt de tra -- ver -- se ?
  Cha -- cun doit ris -- quer.
  Un cœur qui dif -- fe -- re
  d’en -- trer en af -- fai -- re
  s’ex -- pose à man -- quer
  le temps de s’em -- bar -- quer.
}
