\score {
  \new ChoirStaff <<
    \new Staff \withLyricsB <<
      \global \keepWithTag #'triton1 \includeNotes "voix"
    >> \keepWithTag #'couplet1 \includeLyrics "paroles"
    \keepWithTag #'couplet2 \includeLyrics "paroles"
    \new Staff \withLyricsB <<
      \global \keepWithTag #'triton2 \includeNotes "voix"
    >> \keepWithTag #'couplet1 \includeLyrics "paroles"
    \keepWithTag #'couplet2 \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s4 s2.*5\break s2.*6\pageBreak
        s2.*6\break s2.*5\break s2.*6\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
