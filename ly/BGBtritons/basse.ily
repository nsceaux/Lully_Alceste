\clef "basse" do4 |
fa re2 |
la4 sol fa |
mi do2 |
sol do4 |
si,2. |
do8 si, la,4 sol, |
fa, sol,2 |
do2 do'4 |
sold2 la4 |
mi mi8 re do4 |
re si, mi |
la, la8 sol fa4 |
mi2 la4 |
re re8 do sib,4 |
la,2 re4 |
sol, sol4. fa8 |
mib4. re8 do4 |
re re8 do sib,4 |
do re re, |
sol,2 sol4 |
do2. |
sol,4 sol mi |
fa2 do4 |
sol4. fa8 mi4 |
fa re sol |
do2 do4 |
fa re2 |
la4 sol fa |
mi do2 |
sol do4 |
si,2. |
do8 si, la,4 sol, |
fa, sol,2 |
do,2
