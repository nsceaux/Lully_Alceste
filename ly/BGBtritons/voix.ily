<<
  \tag #'(triton1 basse) {
    \clef "vhaute-contre"
    sol'4 |
    la'4. sol'8 fa'4 |
    mi'\trill mi' fa' |
    sol'4. fa'8 mi'4 |
    re'\trill re' mi' |
    fa'4. sol'8 fa'4 |
    mi'\trill fa' fa' |
    mi' re'4.\trill do'8 |
    do'2 mi'4 |
    re'4.\trill mi'8 do'4 |
    si4\trill si mi' |
    re'4.\trill do'8 si4 |
    do'2 la'4 |
    sol'4.\trill fa'8 mi'4 |
    fa' fa' fa' |
    fad'4. mi'8 fad'4 |
    sol'2 re'4 |
    re' do'4.\trill sib8 |
    la4 la re' |
    do'8 sib la4( si8) do' |
    si!2 re'4 |
    mi'4. re'8 do'4 |
    re' re' sol' |
    do'4. re'8 mi'4 |
    re'\trill re' sol' |
    fa'4.\trill mi'8 re'8.[ mi'16] |
    mi'2\trill sol'4 |
    la'4. sol'8 fa'4 |
    mi'\trill mi' fa' |
    sol'4. fa'8 mi'4 |
    re'\trill re' mi' |
    fa'4. sol'8 fa'4 |
    mi'\trill fa' fa' |
    mi' re'4.\trill do'8 |
    do'2
  }
  \tag #'triton2 {
    \clef "vhaute-contre"
    mi'4 |
    fa'4. mi'8 re'4 |
    do' do' re' |
    mi'4. re'8 do'4 |
    si si do' |
    re'4. mi'8 re'4 |
    sol la si |
    do' si4.\trill do'8 |
    do'2 do'4 |
    si4. do'8 la4 |
    sold sold do' |
    si4.\trill la8 sold4 |
    la2 do'4 |
    dod'4. si8 dod'4 |
    re' re' re' |
    do'4. sib8 la4 |
    sib2 sib4 |
    sib la4.\trill sol8 |
    fad4 fad sib |
    la8 sol fad4. sol8 |
    sol2 si4 |
    do'4. si8 la4 |
    si si mi' |
    la4. si8 do'4 |
    si si mi' |
    re'4.\trill do'8 si4 |
    do'2 mi'4 |
    fa'4. mi'8 re'4 |
    do' do' re' |
    mi'4. re'8 do'4 |
    si si do' |
    re'4. mi'8 re'4 |
    sol la si |
    do' si4.\trill do'8 |
    do'2
  }
>>