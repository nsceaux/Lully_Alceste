\clef "haute-contre" sol'4 |
sol' sol'8 sol' mi'4 mi' |
mi' sol' do'' do''8 re'' |
mi''4 mi'' mi''4. re''16 do'' |
si'4 sol' sol' si' |
do'' do'' do''4. si'16 la' |
si'4. si'8 do''4. si'16 la' |
si'4 si' si'4. si'8 |
\sug do''4 \sug do'' si'4. si'8 |
do''4. re''16 do'' si'4. do''16 re'' |
do''4 sol'8 sol' sol'4 sol' |
sol'4. sol'8 sol'4 sol' |
sol'2 sol'4 sol' |
mi'4 mi'8 mi' mi'4 mi' |
fa'4 fa' fa' fa'8 mi' |
re'4 sol' sol'4. sol'8 |
mi'4. fa'16 sol' la'4. sol'16 la' |
fa'4. sol'16 la' sib'4. la'16 sib' |
sol'4 sol' do''4. sib'16 do'' |
la'4. la'8 fad'4. fad'8 |
sol'2 sol'4. sol'8 |
sol'4. la'8 fad'4. mi'16 fad' |
sol'2 sol'4 sol' |
sol' sol'8 sol' mi'4 mi' |
mi' sol' do'' do''8 re'' |
mi''4 mi'' mi''4. re''16 do'' |
si'4 sol' sol' si' |
do'' do'' do''4. si'16 la' |
si'4. si'8 do''4. si'16 la' |
si'4. si'8 si'4. si'8 |
\sug do''4 \sug do'' si'4. si'8 |
do''4. re''16 do'' si'4. do''16 re'' |
do''4 sol'8 sol' sol'4 sol' |
sol'4. sol'8 sol'4 sol' |
sol'2 sol'4 do''8 do'' |
la'4. la'8 si'4 si'8 la' |
sold'2 sold'4. sold'8 |
la'2~ la'4 dod''8 dod'' |
dod''2 dod''4.\trill si'16 dod'' |
re''4 la' la'2 |
la' la'4. la'8 |
sol'2 sol'4 sol' |
sol' sol'8 sol' mi'4 mi' |
mi' sol' do'' do''8 re'' |
mi''4 mi'' mi''4. re''16 do'' |
si'4 sol' sol' si' |
do'' do'' do''4. si'16 la' |
si'4. si'8 do''4. si'16 la' |
si'4 si' si'4. si'8 |
\sug do''4 \sug do'' si'4. si'8 |
do''4. re''16 do'' si'4. do''16 re'' |
do''4 sol'8 sol' sol'4 sol' |
sol'2 sol' |
