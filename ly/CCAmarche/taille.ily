\clef "taille" mi'4 |
mi' mi'8 mi' do'4 do' |
do' mi'8 re' do' re' mi'4 |
mi' do' sol' sol' |
sol'2 sol'4 sol' |
sol' sol'8 sol' sol'4. sol'8 |
sol'4 sol'8 sol' sol'4. sol'8 |
sol'4 sol'8 sol' sol'4. sol'8 |
mi'4 sol' sol'4. sol'8 |
sol'4. re'16 do' si4. do'16 re' |
do'4. do'8 do'4. sol'8 |
mi'2 mi'4 mi' |
mi'2 mi'4 mi' |
do'4 do'8 do' do'4 do' |
do' la la re' |
re'4. re'8 mi'4. mi'8 |
mi'4 do' do'4. do'8 |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 mi'4 la' |
la'2 la'4. la'8 |
sol'4 re' mi'4. mi'8 |
re'4. re'8 re'4. re'8 |
re'2 re'4 mi' |
mi' mi'8 mi' do'4 do' |
do' mi'8 re' do' mi' mi'4 |
mi' do' sol' sol' |
sol'2 sol'4 sol' |
sol' sol'8 sol' sol'4. sol'8 |
sol'4 sol'8 sol' sol'4 sol' |
sol' re' re'4. re'8 |
mi'4 sol' sol'4. sol'8 |
sol'4. re'16 do' si4. do'16 re' |
do'4. do'8 do'4. sol'8 |
mi'2 mi'4 mi' |
mi'4. fa'8 sol'4 sol' |
fa'2 fa'4. fa'8 |
mi'4 mi'8 mi' mi'4 mi' |
mi'4. mi'8 dod'4 la |
mi'4. mi'8 la'4. la'8 |
la'4 fad' fad'4. fad'8 |
fad'2 fad'4.\trill mi'16 fad' |
sol'4 re' re' mi' |
mi' mi'8 mi' do'4 do' |
do' mi'8 re' do' re' mi'4 |
mi' do' sol' sol' |
sol'2 sol'4 sol' |
sol' sol'8 sol' sol'4. sol'8 |
sol'4 sol'8 sol' sol'4. sol'8 |
sol'4 sol'8 sol' sol'4. sol'8 |
mi'4 sol' sol'4. sol'8 |
sol'4. re'16 do' si4. do'16 re' |
do'4. do'8 do'4. sol'8 |
mi'2 mi' |
