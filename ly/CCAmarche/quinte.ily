\clef "quinte" do'4 |
do' sol sol sol |
sol sol8 sol sol4 sol |
sol do' do' do' |
re'2 re'4 re' |
do' mi' do'8 re' mi'4 |
re'4. re'8 do'4. re'16 mi' |
re'4 si8 re' re'4 si |
sol4 do' re'4. re'8 |
mi'4 sol' sol' sol |
sol sol8 sol sol4 sol |
sol2 do'4 do' |
sol2 do'4 do' |
sol4 sol8 sol sol4 sol |
la do' re' si |
si4. si8 si4. si8 |
do'4 la la4. la8 |
sib?2 sib4. sib8 |
do'4. do'8 do'4. do'8 |
re'2 re'4 la |
si8 do' re'4 do'4. do'8 |
do'4 la la4. la8 |
sol4. la8 si4 do' |
do' sol sol sol |
sol sol8 sol sol4 sol |
sol do' do' do' |
re' si si8 do' re'4 |
do' mi' do'4. re'16 mi' |
re'4. re'8 do'4. re'16 mi' |
re'4 si8 do' re'4 sol |
sol do' re'4. re'8 |
mi'4 sol' sol' sol |
sol sol8 sol sol4 sol |
sol2 do'4 do' |
do'2 do'4. do'8 |
do'2 si4. si8 |
si4. si16 si si4 si |
la2~ la4 la |
la mi' mi' mi' |
re'2 re'4 re' |
la' fad' re'4. re'8 |
re'4. do'8 si4 do' |
do' sol sol sol |
sol sol8 sol sol4 sol |
sol do' do' do' |
re'2 re'4 re' |
do' mi' do'8 re' mi'4 |
re'4. re'8 do'4. re'16 mi' |
re'4 si8 re' re'4 si |
sol4 do' re'4. re'8 |
mi'4 sol' sol' sol |
sol sol8 sol sol4 sol |
sol2 do' |
