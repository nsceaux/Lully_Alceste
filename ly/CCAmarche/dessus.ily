\clef "dessus" <>^"Trompettes et violons" sol''4 |
do'' do''8 do'' do''4 sol' |
do'' do''8 re'' mi''4 do'' |
sol'' sol'' sol''4. fa''16 mi'' |
re''2\trill sol'4 re'' |
mi'' mi'' mi''4. re''16 do'' |
re''4. sol''16 fa'' mi''4.\trill re''16 do'' |
re''4 sol'' re''4. mi''16 fa'' |
mi''4. fa''16 mi'' re''4. mi''16 fa'' |
mi''4. fa''16 mi'' re''4. mi''16 fa'' |
mi''4. re''16 do'' re''4. do''16 si' |
do''2 do''4 sol'' |
do''2 do''4 <>^"Violons" sol' |
do'' sol' do'' do''8 si' |
la'4 la' re'' re''8 do'' |
si'4\trill si' mi''4. re''16 mi'' |
do''4. re''16 mi'' fa''4. mi''16 fa'' |
re''4.\trill mi''16 fa'' sol''4. fa''16 sol'' |
mi''4\trill mi'' la''4. sol''16 la'' |
fad''4. mi''16 fad'' re''4. re''8 |
sol''4. fa''8 mi'' re'' do'' si' |
la'4 re'' la' si'8 do'' |
si'4.\trill la'8 sol'4 <>^"Trompettes et violons" sol'' | \noBreak
do'' do''8 do'' do''4 sol' |
do'' do''8 re'' mi''4 do'' |
sol'' sol'' sol''4. fa''16 mi'' |
re''2\trill sol'4 re'' |
mi'' mi'' mi''4.\trill re''16 do'' |
re''4. sol''16 fa'' mi''4.\trill re''16 do'' |
re''4 sol'' re''4. mi''16 fa'' |
mi''4. fa''16 mi'' re''4. mi''16 fa'' |
mi''4. fa''16 mi'' re''4. mi''16 fa'' |
mi''4. re''16 do'' re''4. do''16 si' |
do''2 do''4 <>^"Violons" sol' |
do'' do''8 re'' mi'' fa'' sol'' mi'' |
fa''4 fa''8 mi'' re''4 re''8 do'' |
si'4\trill mi'' si' mi'' |
dod''4.\trill re''16 mi'' la'4 la''8 la'' |
la''4. sol''16 fad'' mi''8 fad'' sol'' la'' |
fad''4. sol''16 la'' re''4 re''8 re'' |
re''4. do''16 si' la'8 si' do'' re'' |
si'4.\trill do''16 re'' sol'4 <>^"Trompettes et violons" sol'' | \noBreak
do'' do''8 do'' do''4 sol' |
do'' do''8 re'' mi''4 do'' |
sol'' sol'' sol''4. fa''16 mi'' |
re''2\trill sol'4 re'' |
mi'' mi'' mi''4. re''16 do'' |
re''4. sol''16 fa'' mi''4.\trill re''16 do'' |
re''4 sol'' re''4. mi''16 fa'' |
mi''4. fa''16 mi'' re''4. mi''16 fa'' |
mi''4. fa''16 mi'' re''4. mi''16 fa'' |
mi''4. re''16 do'' re''4. do''16 si' |
do''2 do'' |
