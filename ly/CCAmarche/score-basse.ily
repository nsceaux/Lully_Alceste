\score {
  \new StaffGroup <<
    \new Staff << \global \includeNotes "timbales" >>
    \new Staff << \global \includeNotes "basse" >>
  >>
  \layout { }
}
