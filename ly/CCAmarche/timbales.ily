\clef "basse" do4 |
do4. do16 do do4 do |
do4. do16 do do4 do |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,4. sol,16 sol, do4. do16 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do sol,4. sol,16 sol, |
do4. do16 do sol,4. sol,16 sol, |
do4. do16 do sol,4. sol,16 sol, |
do2 do4 do |
do2 do4 r |
R1*9 |
r2 r4 do |
do4. do16 do do4 do |
do4. do16 do do4 do |
do4 do do do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,4. sol,16 sol, do4. do16 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do sol,4. sol,16 sol, |
do4. do16 do sol,4. sol,16 sol, |
do4. do16 do sol,4. sol,16 sol, |
do2 r |
R1*7 |
r2 r4 do |
do4. do16 do do4 do |
do4. do16 do do4 do |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,4. sol,16 sol, do4. do16 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do sol,4. sol,16 sol, |
do4. do16 do sol,4. sol,16 sol, |
do4. do16 do sol,4. sol,16 sol, |
do2 do |
