\clef "basse" do4 |
do2. do4 |
do4. do16 do do4 do |
do do8 re mi4 do |
sol2 sol4 sol |
do4. do16 do do4 do |
sol, sol,8 sol, do4 do8 do |
sol,2 sol4 sol8 sol |
do4 do8 do sol,4 sol,8 sol, |
do4 do8 do sol,4 sol,8 sol, |
do4 do8 do sol,4 sol,8 sol, |
do,2 do,4 do |
do,2 do,4 do |
do do do do |
fa fa8 mi re2 |
sol4 sol8 fa mi2 |
la4. la16 sol fa2 |
sib4. sib16 la sol2 |
do'4. do'16 sib la4. la8 |
re'2 re4. re16 do |
si,2 do |
re4. re8 re,4 re, |
sol,2 sol,4 do |
do2. do4 |
do4. do16 do do4 do |
do4 do8 re mi4 do |
sol2 sol4 sol |
do4. do16 do do4 do |
sol, sol,8 sol, do4 do8 do |
sol,2 sol4 sol8 sol |
do4 do8 do sol,4 sol,8 sol, |
do4 do8 do sol,4 sol,8 sol, |
do4 do8 do sol,4 sol,8 sol, |
do,2 do,4 do4 |
do2 do4. do8 |
re2 re4. re8 |
mi4 mi8 mi mi4 mi, |
la, la la la |
la la8 la la4 la, |
re re' re' re' |
re' re'8 re' re'4 re |
sol2 sol4 do |
do2. do4 |
do4. do16 do do4 do |
do do8 re mi4 do |
sol2 sol4 sol |
do4. do16 do do4 do |
sol, sol,8 sol, do4 do8 do |
sol,2 sol4 sol8 sol |
do4 do8 do sol,4 sol,8 sol, |
do4 do8 do sol,4 sol,8 sol, |
do4 do8 do sol,4 sol,8 sol, |
do,2 do, |
