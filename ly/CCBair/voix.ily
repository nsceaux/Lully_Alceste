<<
  %% Admete
  \tag #'(admete basse) {
    \clef "vtaille" <>^\markup\character Admete
    r4 r8 sol' do'4. fa'8 |
    re'4.\trill sol'8 mi'4\trill r8 sol'16 fa' |
    mi'8.\trill re'16 do'8 do'16 si la4.\trill re'8 |
    sol4. do'8 la4. re'8 |
    si2\trill re'8. mi'16 |
    fa'4 fa'8 fa' fa'8. sol'16 |
    mi'4\trill mi'8 mi' mi'8. fa'16 |
    sol'4 sol'8. sol'16 do'8 do'16 do' fa'8 fa'16 mi' |
    re'2\trill r4 sol' |
    do'2. fa'4 |
    re'2.\trill sol'4 |
    mi'2.\trill
  }
  \tag #'alcide {
    \clef "vbasse-taille" <>^\markup\character Alcide
    r4 r8 sol la4. la8 |
    si4. si8 do'4 r8 mi'16 re' |
    do'8. si16 la8 la16 sol fad4. re8 |
    mi4. mi8 fad4. fad8 |
    sol2 si8. do'16 |
    re'4 re'8 re' la8. si16 |
    do'4 do'8 do' do'8. re'16 |
    mi'4 mi'8. mi'16 la8 la16 la re'8 re'16 do' |
    si2 r4 sol |
    la2. la4 |
    si2. si4 |
    do'2.
  }
>>
