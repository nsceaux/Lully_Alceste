\clef "basse" sol,4. sol8 la4. la8 |
si4. si8 do'2 |
do re4. re8 |
mi4. mi8 fad4. fad8 |
sol2 sol4 |
re2 re4 |
la2 sol8 fa |
mi4. mi8 fa4 re |
sol2. sol4 |
la2. la4 |
si2. si4 |
do'2.
