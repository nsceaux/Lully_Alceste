\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \keepWithTag #'admete \includeNotes "voix"
    >> \keepWithTag #'admete \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'alcide \includeNotes "voix"
    >> \keepWithTag #'alcide \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\break s1 s2.*3\break
      }
    >>
  >>
  \layout { }
  \midi { }
}