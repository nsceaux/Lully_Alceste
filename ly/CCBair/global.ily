\key do \major \midiTempo#120
\time 4/4 s1*4
\digitTime\time 3/4 s2.*3
\time 4/4 s1
\time 2/2 s1*3 s2. \bar "|."
\endMark "On reprend la marche"
