\score {
  \new ChoirStaff <<
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\pageBreak
        s1*5\break s1*5 s2 \bar "" \break s2 s2. s2 \bar "" \pageBreak
        s4 s2. s1*2\break s1*4\break s1*3\pageBreak
        s1*2\break s1*4\break s1*4 s2 \bar "" \break s2 s1*2\pageBreak
        s1*3\break s1*6\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}