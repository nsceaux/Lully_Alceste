\piecePartSpecs
#`((basse-continue
    #:music , #{ s1*2 s1*10 s1 s2.*3 s1 s1 s1 s1*5 s1*5 s1*5 s1*6 s1*3\break #}
    #:score-template "score-basse-voix")
  (basse #:score-template "score-basse-voix")
  (silence #:on-the-fly-markup , #{ \markup\tacet#50 #}))
