\clef "basse"
<<
  \tag #'basse { R1*3 r2 r4 }
  \tag #'basse-continue {
    sol,2 re4. re8 |
    do4 sib,8 do re4 re, |
    sol,2 mib |
    re2.
  }
>> re4 |
sol2. sol4 |
fa2. fa4 |
mib2. mib4 |
re1~ |
re2 re4 do |
sib,2 sib, |
do re |
<<
  \tag #'basse { sol,2. r4 | R1 R2.*3 R1*5 \allowPageTurn r2 }
  \tag #'basse-continue {
    sol,1~ |
    sol,2 re4. re8 |
    mib2 mi4 |
    fa re mib |
    re2 fad,4 |
    sol,2 do |
    re1 | \allowPageTurn
    do4 sib,8 do re4 re, |
    sol,2 mib |
    re re4 do4 |
    sib,2
  }
>> sib,2 |
do re |
<<
  \tag #'basse { sol,2. r4 | R1*7 \allowPageTurn | r2 }
  \tag #'basse-continue {
    sol,1 |
    sol,2 sol4. fa8 |
    mib2 mi |
    fa4 mib re2 |
    do sib, |
    la,4 sol, re re, |
    sol,2 mib |
    re2. do4 |
    sib,2
  }
>> sib,2 |
do re |
<<
  \tag #'basse { sol,2. r4 | R1*7 | r2 r4 }
  \tag #'basse-continue {
    sol,1 |
    sol2 la |
    sib4 re mib mi |
    fa re sib2 |
    la re |
    la,8 sol, la,4 re2 |
    do4 sib,8 do re4 re, |
    sol,2 mib |
    re2.
  }
>> re4 |
sol2. sol4 |
fa2. fa4 |
mib2. mib4 |
re1~ |
re2 re4 do |
sib,2 sib, |
do re |
sol, sol, |
