\tag #'(recit basse) {
  Ciel ! qu’est- ce que je voy !

  Al -- ceste est mor -- te.
  
  Al -- ceste est mor -- te !
}
\tag #'(vtaille vbasse basse) { Al -- ceste }
\tag #'(vtaille vbasse) { est mor -- te. }
\tag #'(vdessus basse) {
  Al -- ceste est mor -- te.
  Al -- ceste est mor -- te.
  Al -- ceste est mor -- te.
}
\tag #'(vhaute-contre) { Al -- ceste }
\tag #'(vhaute-contre vtaille vbasse) {
  Al -- ceste est mor -- te.
  Al -- ceste est mor -- te.
}
\tag #'(recit basse) {
  Al -- ceste a sa -- tis -- fait les Par -- ques en cour -- roux ;
  vos -- tre tom -- beau s’ou -- vroit, elle y des -- cend pour vous,
  el -- le- mesme a vou -- lu vous en fer -- mer la por -- te ;
  Al -- ceste est mor -- te.

  Al -- ceste est mor -- te !
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Al -- ceste est mor -- te.
}
\tag #'(recit basse) {
  J’ay cou -- ru, mais trop tard pour ar -- res -- ter ses coups :
  ja -- mais en fa -- veur d’un es -- poux
  on ne ver -- ra d’ar -- deur si fi -- delle & si for -- te ;
  Al -- ceste est mor -- te.
  
  Al -- ceste est mor -- te !
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Al -- ceste est mor -- te.
}
\tag #'(recit basse) {
  Su -- jets, a -- mis, pa -- rents, vous a -- ban -- don -- noient tous ;
  sur les droits les plus forts, sur les nœuds les plus doux,
  l’a -- mour, le tendre a -- mour l’em -- por -- te :
  Al -- ceste est mor -- te.
  
  Al -- ceste est mor -- te !
}
\tag #'(vtaille vbasse basse) { Al -- ceste }
\tag #'(vtaille vbasse) { est mor -- te. }
\tag #'(vdessus basse) {
  Al -- ceste est mor -- te.
  Al -- ceste est mor -- te.
  Al -- ceste est mor -- te.
}
\tag #'(vhaute-contre) { Al -- ceste }
\tag #'(vhaute-contre vtaille vbasse) {
  Al -- ceste est mor -- te.
  Al -- ceste est mor -- te.
}
