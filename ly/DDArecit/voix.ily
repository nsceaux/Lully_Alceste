<<
  %% Chœur
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s1*4 s2. \ffclef "vdessus" }
      \tag #'vdessus {
        \clef "vdessus"
        << R1*4 { s1*3 s2. s4^\markup\character Tous } >>
        r4 r2
      }
    >> sol'4 |
    re''2. re''4 |
    re''2 do''4. do''8 |
    do''2 sib' |
    la'2 la'4 r |
    r2 sib' |
    la' la' |
    sib' sib'4 <<
      \tag #'basse {
        s4 s1 s2.*3 s1*5 \ffclef "vdessus" <>^\markup\character Chœur
      }
      \tag #'vdessus { r4 R1 R2.*3 R1*5 }
    >>
    r2 sib' |
    la' la' |
    sib'2 sib'4 <<
      \tag #'basse {
        s4 s1*7 \ffclef "vdessus" <>^\markup\character Chœur
      }
      \tag #'vdessus { r4 R1*7 }
    >>
    r2 sib' |
    la' la' |
    sib'2 sib'4 <<
      \tag #'basse { s4 s1*8 s2. \ffclef "vdessus" }
      \tag #'vdessus { r4 R1*8 r2 r4 }
    >> sol' |
    re''2. re''4 |
    re''2 do''4. do''8 |
    do''2 sib' |
    la'2 la'4 r |
    r2 sib' |
    la' la' |
    sib' sib'4 r |
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" R1*5 |
    r2 r4 re' |
    sol'2. sol'4 |
    fad'2 sol' |
    fad' fad'4 r |
    r2 re' |
    mib' re' |
    re' re'4 r4 |
    R1 R2.*3 R1*5 |
    r2 re' |
    mib' re' |
    re' re'4 r |
    R1*7 |
    r2 re' |
    mib' re' |
    re' re'4 r |
    R1*9 |
    r2 r4 re' |
    sol'2. sol'4 fad'2 sol' |
    fad'2 fad'4 r |
    r2 re' |
    mib' re' |
    re' re'4 r |
  }
  \tag #'(vtaille basse) {
    <<
      \tag #'basse {
        s1*3 s2. \ffclef "vtaille" <>^\markup\character Chœur
      }
      \tag #'vtaille { \clef "vtaille" R1*3 r2 r4 }
    >> la4 |
    sib2. <<
      \tag #'basse {
        s4 s1*8 s2.*3 s1*25 s2.
        \ffclef "vtaille" <>^\markup\character Chœur
      }
      \tag #'vtaille {
        sib4 |
        la2 la4 r |
        r2 r4 sol |
        re'2. re'4 |
        re'2 re'4 r |
        r2 sol |
        sol fad |
        sol sol4 r |
        R1 R2.*3 R1*5 |
        r2 sol |
        sol fad |
        sol sol4 r |
        R1*7 |
        r2 sol |
        sol fad |
        sol sol4 r |
        R1*7 |
        r2 r4
      }
    >> la4 |
    sib2.
    \tag #'vtaille {
      sib4 |
      la2. la4 |
      r2 r4 sol |
      re'2. re'4 |
      re'2 re'4 r |
      r2 sol |
      sol fad |
      sol2 sol4 r |
    }
  }
  \tag #'vbasse {
    \clef "vbasse" R1*3 |
    r2 r4 re |
    sol2. sol4 |
    fa2 fa4 fa |
    mib2. mib4 |
    re1 |
    re2 r |
    r sib, |
    do re |
    sol, sol,4 r |
    R1 R2.*3 R1*5 |
    r2 sib, |
    do re |
    sol, sol,4 r |
    R1*7 |
    r2 sib, |
    do re |
    sol, sol,4 r |
    R1*7 |
    r2 r4 re |
    sol2. sol4 |
    fa2 fa4 fa |
    mib2. mib4 |
    re1 |
    re2 r |
    r sib, |
    do re |
    sol, sol,4 r |
  }
  %% Récit
  \tag #'(recit basse) {
    \clef "vtaille" <>^\markup\character Admete
    sib4\trill sib16 sib sib do' la4\trill
    \ffclef "vdessus" <>^\markup\character Céphise
    r8 re'' |
    fad'4 sol' sol'( fad') |
    sol'4
    \ffclef "vtaille" <>^\markup\character Admete
    r8 re' sol'4. do'8 |
    re'2 re'4 <<
      \tag #'basse { s4 s1*7 s2. }
      \tag #'recit { r4 | R1*7 | r2 r4 }
    >>
    \ffclef "vdessus" <>^\markup\character Céphise
    r8 sol'' |
    re''\trill re'' re'' mib'' fa''8. fa''16 sib' sib' sib' re'' |
    sol'4 mib''8 mib''16 sol'' do''8. do''16 |
    la'4\trill sib'8 sib'16 re'' sol'8[ fad'16] sol' |
    fad'4 r8 la'16 sib' do''8 do''16 sib'32[ la'] |
    sib'4 r8 sib'8 do''8. re''16 do''8.\trill sib'16 |
    la'4\trill la'4 r re'' |
    fad' sol' sol'( fad') |
    sol'4
    \ffclef "vtaille" <>^\markup\character Admete
    r8 re' sol'4. do'8 |
    re'2 re'4 r |
    <<
      \tag #'basse { s1*2 s2. }
      \tag #'recit { R1*2 r2 r4 }
    >>
    \ffclef "vdessus" <>^\markup\character Céphise
    sol''8. re''16 |
    mib''4 r8 mib''16 sol'' si'8. sol'16 sol' sol' la' si' |
    do''4. do''8 sol' sol'16 la' sib'8 sib'16 la' |
    la'8. fa'16 fa' fa' sol' la' sib'4 r8 sib'16 sib' |
    mib''8.[ la'16] la'8. sib'16 sib'4 sib'8 re'' |
    fad'4 sol' sol'( fad') |
    sol'4
    \ffclef "vtaille" <>^\markup\character Admete
    re' sol'4. do'8 |
    re'2 re'4 r |
    <<
      \tag #'basse { s1*2 s2. }
      \tag #'recit { R1*2 r2 r4 }
    >>
    \ffclef "vdessus" <>^\markup\character Céphise
    r8 re'' |
    sib'4 r8 mib'' do''4\trill r8 fa'' |
    re''8.\trill re''16 re'' re'' mib'' fa'' sib'8 mib''16 sol'' do''8 do''16 do'' |
    la'4\trill r8 fa''16 fa'' re''4 re''8[ dod''16] re'' |
    dod''4 r8 mi'' fa''4 r8 fa'' |
    dod''8 re'' re'' dod'' re''4 re''8 re'' |
    fad'4 sol' sol'( fad') |
    sol'4
    \ffclef "vtaille" <>^\markup\character Admete
    r8 re' sol'4. do'8 |
    re'2 re'4
    \tag #'recit { r4 R1*8 }
  }
>>