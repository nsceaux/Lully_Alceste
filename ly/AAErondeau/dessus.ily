\clef "dessus"
sol''4. la''8 sol''4 fa'' |
mi''2 re''\trill |
do''8 si' do'' re'' mi''4 fa''8 mi'' |
re''4 do''8\trill si' do''4. re''8 |
si'4.\trill la'8 sol'4 sol' |
sol''4. la''8 sol''4 fa'' |
mi''2 re''\trill |
do''8 si' do'' re'' mi''4 fa''8 mi'' |
re''4 mi''8 fa'' re''4.\trill do''8 |
do''1 |
do''2. mi''8 fa'' |
sol''4 re'' re'' mi''8 re'' |
do''4\trill si'8 do'' la'4 si'8\trill do'' |
re''2 r8 si' do'' re'' |
mi''4. fa''8 mi'' re'' do'' si' |
la'4 si'8 do'' la'4.\trill sol'8 |
sol'1 |
sol''4. la''8 sol''4 fa'' |
mi''2\trill re''\trill |
do''8 si' do'' re'' mi''4 fa''8 mi'' |
re''4 do''8 si' do''4. re''8 |
si'4.\trill la'8 sol'4 sol' |
sol''4. la''8 sol''4 fa'' |
mi''2\trill re''\trill |
do''8 si' do'' re'' mi''4 fa''8 mi'' |
re''4 mi''8 fa'' re''4.\trill do''8 |
do''2 r8 mi'' fad'' sol'' |
fad''2 r8 fad'' sold'' la'' |
sold''4 la'' la''4.\trill sold''8 |
la''2 r8 sol'' la'' mi'' |
fa''4. fa''8 fa''4. sol''8 |
mi''2.\trill r16 fa'' mi'' fa'' |
sol''4 la''8 sol'' fa''4.\trill mi''8 |
re''1 |
sol''4. la''8 sol''4 fa'' |
mi''2\trill re''\trill |
do''8 si' do'' re'' mi''4 fa''8 mi'' |
re''4 do''8 si' do''4. re''8 |
si'4.\trill la'8 sol'4 sol' |
sol''4. la''8 sol''4 fa'' |
mi''2\trill re''\trill |
do''8 si' do'' re'' mi''4 fa''8 mi'' |
re''4 mi''8 fa'' re''4.\trill do''8 |
do''1 |
