\clef "basse" do4. do16 do do4 sol, |
do2 sol, |
do4. do16 do do4 do |
sol,2 do4 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 sol, |
do2 sol, |
do4. do16 do do4 do |
sol, do sol,4. do8 |
do1 |
do |
R1*5 |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 sol, |
do2 sol, |
do4. do16 do do4 do |
sol,2 do4 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 sol, |
do2 sol, |
do4. do16 do do4 do |
sol, do sol,4. do8 |
do1 |
R1*6 |
sol,2 sol,4 sol, |
do4. do16 do do4 sol, |
do2 sol, |
do4. do16 do do4 do |
sol,2 do4 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 sol, |
do2 sol, |
do4. do16 do do4 do |
sol, do sol,4. do8 |
do1 |
