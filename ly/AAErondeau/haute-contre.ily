\clef "haute-contre"
do''4. do''8 do''4 si' |
do''2 si'4. la'16 si' |
do''2 do''4 re''8 do'' |
si' la' sol'4 sol'4. sol'8 |
sol'2. sol'4 |
sol'2 do''4 si' |
do''2 si'\trill |
do'' do''4 re''8 do'' |
si'4 do''8 re'' si'4. do''8 |
do''1 |
do''2. sol'8 la' |
si'2 sol'4. sol'8 |
sol'2 fad'4.(\trill mi'16 fad') |
sol'2 r8 sol' la' si' |
do''4 sol' la' la'8 sol' fad'4 sol'8 la' fad'4. sol'8 |
sol'1
do''4. do''8 do''4 si' |
do''2 si'4. la'16 si' |
do''2 do''4 re''8 do'' |
si' la' sol'4 sol'4. sol'8 |
sol'2. sol'4 |
sol'2 do''4 si'\trill |
do''2 si'4.\trill la'16 si' |
do''2 do''4 re''8 do'' |
si'4 do''8 re'' si'4. do''8 |
do''2 do''4. do''8 |
la'2 re''4. do''8 |
si'4 do'' si' mi''8 re'' |
dod''2 dod''4. dod''8 |
re''2 re''4. re''8 |
sol'2 do'' |
do''4 do''8 si' la'4 re''8 do'' |
si'1 |
do''4. do''8 do''4 si' |
do''2 si'4. la'16 si' |
do''2 do''4 re''8 do'' |
si' la' sol'4 sol'4. sol'8 |
sol'2. sol'4 |
sol'2 do''4 si'\trill |
do''2 si'4.\trill la'16 si' |
do''2 do''4 re''8 do'' |
si'4 do''8 re'' si'4. do''8 |
do''1 |
