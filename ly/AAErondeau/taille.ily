\clef "taille" mi'4. fa'8 mi'4 re' |
do'2 re'4. do'16 re' |
mi'8 re' mi' fa' sol'2 |
sol'4 sol'8 fa' mi'4. fa'8 |
re'4. do'8 si4 si |
mi'4. fa'8 mi'4 re' |
do'2 re'4. do'16 re' |
mi'8 re' mi' fa' sol'2 |
sol'4 sol' sol'4. fa'8 |
mi'1 |
mi'2. mi'4 |
re'2 re'4 sol' |
la'2 la'4. la'8 |
re'2 sol'4. sol'8 |
sol'8 fa' mi' re' do'4 mi' |
re' re' re'4. do'8 |
si4. do'8 re'4 si |
mi'4. fa'8 mi'4 re' |
do'2 re'4.(\trill do'16 re') |
mi'8 re' mi' fa' sol'2 |
sol'4 sol'8 fa' mi'4. fa'8 |
re'4. do'8 si4 si |
mi'4. fa'8 mi'4 re' |
do'2 re'4. do'16 re' |
mi'8 re' mi' fa' sol'2 |
sol'4 sol' sol'4. fa'8 |
mi'2. mi'4 |
re'2 re'4
\footnoteHere #'(0 . 0) \markup {
  Manuscrit : \raise#0.5 \score {
    \new Staff \with { \tinyQuote } {
      \clef "taille" \partial 4 sol'4 |
      la'2 la'4. la'8 |
      re'2 sol'4. sol'8 |
      sol'8 fa' mi' re' do'4 mi' |
      re' re' re'4.\trill do'8 |
      si4. do'8 re'4 si |
    }
    \layout { \quoteLayout }
  }
}
\sugNotes {
  la'4 |
  si'2 si'4. si'8 |
  mi'2 la'4. la'8 |
  la'8 sol' fa' mi' re'4 mi' |
  do' do' do'4.\trill si8 |
  do'4. re'8 mi'4 do' |
}
re'1 |
mi'4. fa'8 mi'4 re' |
do'2 re'4.(\trill do'16 re') |
mi'8 re' mi' fa' sol'2 |
sol'4 sol'8 fa' mi'4. fa'8 |
re'4. do'8 si4 si |
mi'4. fa'8 mi'4 re' |
do'2 re'4. do'16 re' |
mi'8 re' mi' fa' sol'2 |
sol'4 sol' sol'4. fa'8 |
mi'1 |
