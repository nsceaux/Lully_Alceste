\clef "quinte" do'4. do'8 sol4 sol |
sol2 sol4 sol |
sol2 do'4. do'8 |
re'4 re' do' sol |
sol2 re'4 re' |
do'4. do'8 sol4 sol |
sol2 sol4 sol |
sol2 do'4. do'8 |
re'4 do' re' sol |
sol1 |
sol2 do'4. do'8 |
re'4. do'8 si4. si8 |
do'4. do'8 do'2 |
si4. do'8 si la sol4 |
sol do' la la |
la sol re'4. re'8 |
re'1 |
do'4. do'8 sol4 sol |
sol2 sol4 sol |
sol2 do'4. do'8 |
re'4 re' do' sol |
sol2 re'4 re' |
do'4. do'8 sol4 sol |
sol2 sol4 sol |
sol2 do'4. do'8 |
re'4 do' re' sol |
sol2 sol4 la |
la re' re'4. re'8 |
re'4 do'8 re' mi'4. mi'8 |
mi'2 la4. la8 |
la4. re'8 re'2 |
do'2. do'4 |
sol2 la4. la8 |
sol1 |
do'4. do'8 sol4 sol |
sol2 sol4 sol |
sol2 do'4. do'8 |
re'4 re' do' sol |
sol2 re'4 re' |
do'4. do'8 sol4 sol |
sol2 sol4 sol |
sol2 do'4. do'8 |
re'4 do' re' sol |
sol1 |
