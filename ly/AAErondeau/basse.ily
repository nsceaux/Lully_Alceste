\clef "basse"
do4. do8 do4 sol, |
do2 sol4. fa8 |
mi fa mi re do2 |
sol do |
sol, sol,4 sol4 |
do4. do8 do4 sol, |
do2 sol4. fa8 |
mi fa mi re do2 |
sol4 do sol,2 |
do1 |
do2 do' |
si1 |
la |
sol4. la8 sol fa mi re |
do2. la,4 |
re sol, re,2 |
sol,4. la,8 si,4 sol, |
do4. do8 do4 sol, |
do2 sol4. fa8 |
mi fa mi re do2 |
sol do |
sol, sol,4 sol4 |
do4. do8 do4 sol, |
do2 sol4. fa8 |
mi fa mi re do2 |
sol4 do sol,2 |
do,2 do |
re1 |
mi4 la, mi,2 |
la, la |
re si, |
do r8 si, do re |
mi2 fa4 re |
sol4. la8 sol fa mi re |
do4. do8 do4 sol, |
do2 sol4. fa8 |
mi fa mi re do2 |
sol do |
sol, sol,4 sol |
do4. do8 do4 sol, |
do2 sol4. fa8 |
mi fa mi re do2 |
sol4 do sol,2 |
do1 |
