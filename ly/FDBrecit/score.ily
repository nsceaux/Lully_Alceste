\score {
  \new ChoirStaff <<
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit0 \includeNotes "voix"
    >> \keepWithTag #'recit0 \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3 s2 \bar "" \break s2 s1*3\pageBreak
        s1*3 s2 \bar "" \break s2 s1*3\break s1*3\break
        s1 s2. \bar "" \break s4 s1 s2 \bar "" \break s2 s1 \pageBreak
        s1*2\break s1*2 s2 \bar "" \break s2 s2. s2 \bar "" \break
        s2 s1*2\break s1*2 s2 \bar "" \pageBreak
        s2 s2.*4\break s2.*6\break s2.*2 s1*2\break s1*2\pageBreak
        s1*3\break s1*3\break s1*2\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2\break s1*3\break s1*3\break s1*2\pageBreak
        s1*2\break s2. s1\break s1*2\break
        s1*2 s2 \bar "" \break s2 s2.*3\pageBreak
        s2.*5\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
