<<
  \tag #'(recit basse) {
    \clef "vbasse-taille" <>^\markup\character Alcide
    r2 r4 r8 do' |
    do'4 sib8\trill la sib4 sol8. do'16 |
    la4\trill fa r8 fa fa fa |
    fa4 mi re4.\trill do8 |
    do2 r4 r8 do' |
    do2 mi4\trill mi8 fa |
    sol4 sol8 la sib4 sib8 do' |
    re'4 re' fad fad8 fad |
    sol4. sol8 la4 sib |
    la4.\trill sol8 sol2 |
    re4 re8 mi fa4 fa8 sol |
    la4\trill la8 sib do'4 do' |
    sol4 sol8 la sib4. sib8 |
    sib4 la sol4.\trill fa8 |
    fa2 mi4 mi8 fa |
    fa1 |
    r2 r8 re re16 mi fa re |
    la8 la16 la re'8 re'16 mi' dod'8 dod' r la |
    re'8. re'16 re'16[ do'8\trill] la16 sib8 sib16 la sol8\trill la16 sib |
    la4\trill
    \ffclef "vbas-dessus" <>^\markup\character Alceste
    r8 fa'' re''8 re''16 do'' sib'8.\trill la'16 |
    sib'4 sol' sol'16 sol' sol' la' fa'8\trill[ mi'16] fa' |
    mi'4\trill
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    r16 la la sol fa8. fa16 fa8. mi16 |
    re8 re sib8. re'16 sol8 sol16 sol sol8. sol16 |
    mi4\trill
    \ffclef "vbas-dessus" <>^\markup\character Alceste
    r8 do''16 do'' do''4. sib'8 |
    la'4\trill la'8. sib'16 sol'8\trill sol' r sib'16 sib' |
    sib'4 la' sol'\trill r8 la'16 sib' |
    la'4\trill
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    r8 do' la8.\trill la16 re'8. la16 |
    sib8. la16 sol8. fa16 mi8. re16 |
    dod4 dod8
    \ffclef "vtaille" <>^\markup\character Admete
    mi' fa' fa' fa' fa' |
    re'4\trill re'8 re' re'4 do'8[ si16] do' |
    si4\trill r8 sol do'8 do' r do'16 do' |
    la4\trill r8 do'16 do' fa'4 fa'8[ mi'16] fa' |
    mi'4\trill r8 do'16 do' sol4 sol8 la |
    sib4 sib8 sib16 do' la8. la16 la8[ sol] |
    la4
  }
  \tag #'recit0 {
    \clef "vbas-dessus" R1*27 R2. R1*6 r4
  }
>>
<<
  \tag #'(recit0 basse) {
    \tag #'basse \ffclef "vbas-dessus"
    <>^\markup\character Alceste
    dod''2 |
    r4 re''2 |
    r4 si'8. si'16 si'8[ la'16] si' |
    do''2 do''8. do''16 |
    do''4 si'4.\trill la'8 |
    la'4( sold'2) |
    la'4 dod''2 |
    r4 re''2 |
    r4 sib'8. la'16 sol'8.\trill fa'16 |
    mi'2\trill mi'8 fa' |
    sol'4 sol' fa' |
    fa'( mi'2)\trill |
    re'4
  }
  \tag #'recit {
    mi'2 |
    r4 fa'2 |
    r4 re'8. re'16 re'8[ do'16] re' |
    mi'2 mi'8. mi'16 |
    mi'4 re'4.\trill do'8 |
    do'4( si2)\trill |
    la4 mi'2 |
    r4 fa'2 |
    r4 mi'8. fa'16 mi'8. re'16 |
    dod'2 dod'8 re' |
    mi'4 mi' re' |
    re'( dod'2)\trill |
    re'4
  }
>>
<<
  \tag #'(recit basse) {
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    r4 re'8 re'16 re' fad8. fad16 |
    sol4. sol8 la8. la16 si8. do'16 |
    si4 r8 sol16 sol do'8 do'16 do' sol8. do'16 |
    la4\trill la8
  }
  \tag #'recit0 { r4 r2 R1*2 r4 r8 }
>>
<<
  \tag #'(recit0 basse) {
    \tag #'basse \ffclef "vbas-dessus"
    <>^\markup\character Alceste
    la'16 la' sol'8\trill sol'16 sol' sol'8[ fa'16] mi' |
    fa'2 sib'8 sib'16 sib' sib'8. sib'16 |
    sib'8. sib'16 la'4. la'8 la'8 sol' |
    la'4 la' r r8 sib' |
    sol'4 sol' r r8 do'' |
    re'' re'' r sib' sib' la' sol'8.\trill fa'16 |
    fa'4 r8 la' sib' sib' r si' |
    dod'' dod'' r re'' mi'' fa'' mi''8.\trill re''16 |
    re''4 r8 re'' la'8. la'16 la'8[ sol'16] la' |
    sib'4 sib'8. do''16 la'4 la'8. si'?16 |
    do''4 do''8. re''16 si'8\trill si' r si' |
    do'' do'' re''[ do''16] re'' sol'4 do''8. do''16 |
    la'4 la'8. si'16 do''4 do''8. si'16 |
    do''4 do''8 do'' si'8.\trill si'16 si'8 dod'' |
    re''4 si'8. si'16 si'4 la'8[ sold'16] la' |
    sold'2 sold' |
    r4 do''8 do''16 do'' sold'8 la' la' sold' |
    la'4 r r r8 sib' |
    sol'4 sol' r r8 do'' |
    re'' re'' r sib' sib' la' sol'8.\trill fa'16 |
    fa'4 r8 la' sib' sib' r si' |
    dod'' dod'' r re'' mi'' fa'' mi''8.\trill re''16 |
    re''4
  }
  \tag #'recit {
    \ffclef "vtaille" <>^\markup\character Admete
    do'16 do' dod'8 dod'16 dod' dod'8[ si16] dod' |
    re'2 re'8 re'16 re' re'8[ mi'16] fa' |
    mi'8. mi'16 fa'4. re'8 re' mi' |
    dod'4 dod'8 dod' re'4 re' |
    r4 r8 mi' fa'4 fa' |
    r4 r8 re' mi' fa' mi'8.\trill fa'16 |
    fa'4 r8 fa' re' re' r sol' |
    mi'\trill mi' r fa' sol' la' dod'8. re'16 |
    re'4 r r r8 re' |
    re'8. re'16 re'8. mi'16 fa'4 fa'8. sol'16 |
    mi'4 mi'8. fa'16 re'8\trill re' r sol' |
    sol' fa' fa'[ mi'16] fa' mi'4\trill mi'8. mi'16 |
    do'4 do'8. re'16 mi'4 fa'8[ mi'16] fa' |
    mi'4 mi'8 mi' re'8.\trill re'16 re'8 mi' |
    fa'4 re'8. re'16 re'4 do'8[ si16] do' |
    si2\trill si |
    r4 mi'8 mi'16 mi' re'8 do' si8.\trill la16 |
    la4 r8 dod' re'4 re' |
    r r8 mi' fa'4 fa' |
    r4 r8 re' mi' fa' mi'8.\trill fa'16 |
    fa'4 r8 fa' re' re' r sol' |
    mi' mi' r fa' sol' la' dod'8. re'16 |
    re'4
  }
>>
<<
  \tag #'(recit basse) {
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    r8 la fa16 re' re' re' la8. si16 |
    do'8 do' do' mi' la4 la8. si16 |
    sold4 mi'8. mi'16 si8\trill dod'16 re' |
    dod'4 r8 la16 la mi4 r8 mi16 mi |
    dod4 r8 la la8. sol16 sol8. fad16 |
    fad4 fad la16 la la si do'8 do'16 re' |
    si8\trill si si16 si si dod' re'4. re'16 dod' |
    re'4
  }
  \tag #'recit0 { r4 r2 R1 R2. R1*4 r4 }
>>
<<
  \tag #'(recit0 basse) {
    \tag #'basse \ffclef "vbas-dessus"
    <>^\markup\character Alceste
    la'4 sib'8. sib'16 sib'8. sib'16 |
    sol'4\trill sol' do''8 do''16 re'' sib'8[ la'16] sib' |
    la'2\trill la'8 la' |
    sol'2 la'8 mi' |
    fa'2 r8 re'' |
    do''[ si' la' si' do'' re'']( |
    mi''4.) mi''8 re''8. do''16 |
    do''4( si'2) |
    do''2 sol'8 la' |
    sib'2 sib'8[ la'16] sib' |
    la'2\trill r8 re'' |
    dod''[ si' dod'' re'' dod'' re'']( |
    mi''4.) mi''8 re''[ dod''] |
    re''4 re''( dod'') |
    re''2. |
  }
  \tag #'recit {
    \ffclef "vtaille" <>^\markup\character Admete
    fa'4 re'8. re'16 re'8. sol'16 |
    mi'4\trill mi' fa'8 fa'16 fa' fa'8 mi' |
    fa'2 do'8 do' |
    dod'2 dod'8 dod' |
    re'2 r8 fa' |
    mi'8[ re' do' re' mi' fa']( |
    sol'4.) sol'8 fa'8. mi'16 |
    mi'4( re'2)\trill |
    do'2 mi'8 fad' |
    sol'4 re'4. mi'8 |
    fa'2 r8 fa' |
    mi'\trill[ re' mi' fa' mi' fa']( |
    sol'4.) sol'8 fa'[ mi'] |
    fa'4 fa'( mi')\trill |
    re'2. |
  }
>>
