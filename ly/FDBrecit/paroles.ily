\tag #'(recit basse) {
  Pour u -- ne si bel -- le vic -- toi -- re
  peut- on a -- voir trop en -- tre -- pris ?
  Pour  - pris ?
  Ah qu’il est doux de cou -- rir à la gloi -- re
  lors que l’a -- mour en doit don -- ner le prix !
  Ah qu’il est doux de cou -- rir à la gloi -- re
  lors que l’a -- mour en doit don -- ner le prix !
  Ah qu’il est  prix !
  Vous dé -- tour -- nez vos yeux ! je vous trouve in -- sen -- si -- ble ?
  Ad -- mete a seul i -- cy vos re -- gards les plus doux ?

  Je fais ce qui m’est pos -- si -- ble
  Pour ne re -- gar -- der que vous.
  
  Vous de -- vez sui -- vre mon en -- vi -- e,
  C’est pour moy qu’on vous rend le jour.
  
  Je n’ay pû re -- pren -- dre la vi -- e
  Sans re -- prendre aus -- si mon a -- mour.
  
  Ad -- mete en ma fa -- veur vous a ce -- dé luy- mes -- me.
  
  Al -- ci -- de pou -- voit seul vous os -- ter au tré -- pas.
  Al -- ces -- te, vous vi -- vez, je re -- voy vos ap -- pas,
  ay- je pû trop pay -- er cet -- te dou -- ceur ex -- tré -- me.
}
\tag #'(recit0 recit basse) {
  Ah ! Ah ! que ne fait- on pas
  pour sau -- ver ce qu’on ai -- me !
  Ah ! Ah ! que ne fait- on pas
  pour sau -- ver ce qu’on ai -- me !
}
\tag #'(recit basse) {
  Vous soû -- pi -- rez tous deux au gré de vos de -- sirs ;
  est-ce ain -- si qu’on me tient pa -- ro -- le ?
}
\tag #'(recit0 recit basse) {
  Par -- don -- nez aux der -- niers soû -- pirs
  d’un mal- heu -- reux a -- mour qu’il faut qu’on vous im -- mo -- le.
  \tag #'recit { Al -- ces -- te, Al -- ces -- te, }
  \tag #'(recit0 basse) { Ad -- me -- te, Ad -- me -- te, }
  il ne faut plus nous voir.
  \tag #'recit { Al -- ces -- te, Al -- ces -- te, }
  \tag #'(recit0 basse) { Ad -- me -- te, Ad -- me -- te, }
  il ne faut plus nous voir.
  \tag #'(recit0 basse) {
    D’un au -- tre que de vous mon des -- tin mon des -- tin
  }
  \tag #'recit { D’un au -- tre que de moy vô -- tre sort }
  doit dé -- pen -- dre,
  \tag #'(recit0 basse) {
    d’un au -- tre que de vous mon des -- tin mon des -- tin
  }
  \tag #'recit {
    d’un au -- tre que de moy vô -- tre sort vô -- tre sort
  }
  doit dé -- pen -- dre,
  il faut dans les grands cœurs que l’a -- mour le plus ten -- dre
  soit la vic -- ti -- me du de -- voir.
  \tag #'recit { Al -- ces -- te, Al -- ces -- te, }
  \tag #'(recit0 basse) { Ad -- me -- te, Ad -- me -- te, }
  il ne faut plus nous voir.
  \tag #'recit { Al -- ces -- te, Al -- ces -- te, }
  \tag #'(recit0 basse) { Ad -- me -- te, Ad -- me -- te, }
  il ne faut plus nous voir.
}
\tag #'(recit basse) {
  Non, non, vous ne de -- vez pas croi -- re
  Qu’un vain -- queur des ti -- rans soit ti -- ran à son tour :
  Sur l’en -- fer, sur la mort, j’em -- por -- te la vic -- toi -- re ;
  il ne man -- que plus à ma gloi -- re
  que de tri -- om -- pher de l’a -- mour.
}
\tag #'(recit0 recit basse) {
  Ah quel -- le gloire ex -- tres -- me !
  Quel he -- ro -- ïque ef -- fort !
  Le vain -- queur de la mort
  tri -- om -- phe de luy- mes -- me.
  Le vain -- queur de la mort
  tri -- om -- phe de luy- mes -- me.
}
