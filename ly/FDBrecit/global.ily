\set Score.currentBarNumber = 14 \bar ""
\key fa \major
\time 4/4 \midiTempo#120 s1 \bar "|!:" s1*3 \alternatives s1 s1
\bar "|!:" s1*8 \alternatives s1 s1
\key la \minor
\time 4/4 \midiTempo#80 \tempo "Majeur" s1*11
\digitTime\time 3/4 s2.
\time 4/4 s1*6
\digitTime\time 3/4 s2.*12
\time 4/4 s1*18
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*8
\digitTime\time 3/4 s2.
\time 4/4 s1*6
\digitTime\time 3/4 \midiTempo#160 s2.*13 \bar "|."
