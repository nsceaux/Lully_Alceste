\clef "basse" fa,2 fa |
fa fa4 mi |
fa4. mi8 re4. do8 |
si,4 do sol,2 |
do,4 do8. re16 mi4 do |
do,2 do |
sib,4. la,8 sol,4 sol,8 la, |
sib,4 sib,8 do re4. do8 |
sib,2 fad,4 sol, |
re,2 sol,4 sol,8 la, |
sib,4 sib,8 do re4 re8 mi |
fa4 fa,8 sol, la,4 la,8 sib, |
do4 sib,8 la, sol, la, sol, fa, |
mi,4 fa, do,2 |
fa,8 sol, la, sib, do2 |
fa,~ fa,8 sol, fa, mi, |
re,2 re |
do4 sib, la,2 |
fad,2 sol,4 do8 do, |
fa,4 fa fad2 |
sol la4 re |
la,2 re4 la, |
sib,2 si, |
do4 la, re sol, |
re,2 sol,4 sol8. fa16 |
mi4 fa do do, |
fa,2 fad, |
sol,2. |
la,2 re |
sib fad |
sol mi |
fa re |
do sib,4. la,8 |
sol,2 re8 do sib,4 |
la,2 la4 |
re2 re4 |
sol2 sol,4 |
do4. si,8 la,4 |
re2. |
mi4 mi,2 |
la,2. |
re |
sol, |
la,4 la4. re8 |
dod2 re4 |
la,2. | \allowPageTurn
re,2 re |
sib,2 fad, |
sol, mi, |
fa,4 fa mi2 |
re2 sol, |
do4 fa, sib,2 |
la,4. la8 sib2 |
do' la |
sib8. la16 sol4 do'8 fa do4 |
fa8. mi16 re4 sol mi |
la fa mi8 re la,4 |
re2 fad, |
sol,4 sol re2 |
la4 la8 fa sol2 |
la4 si do' do |
fa2 mi4 re8. sol16 |
do2 sol8. sol16 fa8 mi |
re2 mi4 la, |
mi,2 mi4 re |
do2 si,8 la, mi mi, |
la,4. la8 sib2 |
do' la |
sib8. la16 sol4 do'8 fa do4 |
fa8. mi16 re4 sol mi |
la fa mi8 re la la, |
re1 |
la,2 fa, |
mi,2. |
la,1~ |
la,1 |
re2 fad, |
sol,4 sol fa8 sol la la, |
re4 re' sol2 |
do'4. sib8 la sib sol4 |
fa2 fa8 fa |
mi2 mi4 |
re4. mi8 fa re |
la2 sol8 fa |
mi2 fa4 |
sol sol,2 |
do4 do' sib8 la |
sol2 sol4 |
re'2 re4 |
la2. |
dod2 re4 |
sol, la,2 |
re,4 re8 do sib, la, |
\once\set Staff.whichBar = "|"
sol,2. |

