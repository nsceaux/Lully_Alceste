Sor -- tez, Om -- bres, fai -- tes moy pla -- ce,
vous pas -- se -- rez une au -- tre fois.


Ah ! ma bar -- que ne peut souf -- frir un si grand poids !

Al -- lons, il faut que l’on me pas -- se.

Re -- ti -- re- toy d’i -- cy, mor -- tel, qui que tu sois,
les en -- fers ir -- ri -- tez pu -- ni -- ront ton au -- da -- ce.

Pas -- se- moy, sans tant de fa -- çons.

L’eau nous ga -- gne, ma bar -- que cré -- ve.

Al -- lons, ra -- me, dé -- pes -- che, a -- ché -- ve.

Nous en -- fon -- çons

Pas -- sons, pas -- sons.

Nous en -- fon -- çons, nous en -- fon -- çons

Pas -- sons, pas -- sons.
