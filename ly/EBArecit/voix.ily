\clef "vbasse-taille" <>^\markup\character Alcide
r4 r8 do'16 do' fa8 fa do do16 fa |
re8\trill re r16 sib sib sib sol4\trill r16 sol sol sol |
do4
\ffclef "vbasse" <>^\markup\character Charon
do'4 r8 sol |
mi\trill mi16 mi mi8. sol16 do8. do16 re8. mi16 |
fa4
\ffclef "vbasse-taille" <>^\markup\character Alcide
r8 do' re'8. re'16 sib sib sib re' |
sol4 sol8
\ffclef "vbasse" <>^\markup\character Charon
do8 do16 re mi fad |
sol8. sol16 la4 la8 si16 do' |
si8\trill sol16 sol do'8 do'16 sol la8 fa16 la re8 re16 sol |
do8 do
\ffclef "vbasse-taille" <>^\markup\character Alcide
r8 sol16 sol la8. la16 fad8\trill fad16 la |
re4
\ffclef "vbasse" <>^\markup\character Charon
r8 sol16 sol do8 do r16 do' do' do' |
fa4 fa
\ffclef "vbasse-taille" <>^\markup\character Alcide
r8 la sib4 |
sol8 sol r sol la la r la |
fad4\trill fad
\ffclef "vbasse" <>^\markup\character Charon
r8 re re re |
sol,
\ffclef "vbasse-taille" <>^\markup\character Alcide
sol re sol mi\trill
\ffclef "vbasse" <>^\markup\character Charon
do do do |
fa, fa fa fa sib,
\ffclef "vbasse-taille" <>^\markup\character Alcide
sib sol8. do'16 |
la4\trill r r2 |
