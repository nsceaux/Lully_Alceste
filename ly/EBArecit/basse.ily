\clef "basse" fa,1 |
sib,2 si, |
do2. |
do2. sib,4 |
la,2 sib, |
do2. |
sib,4 la,2 |
sol,4 mi, fa, sol, |
do2 la, |
sol, do |
fa re |
mib do |
re re, |
sol, do |
fa,4 fa sib, do |
fa,1 |
