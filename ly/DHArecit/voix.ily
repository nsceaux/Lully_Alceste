\clef "vdessus" R1*11 |
r4 r8 <>^\markup\character Diane mi'' do''8 do''16 do'' sol'8 sol'16 do'' |
la'8\trill la' r do'' fa''8. fa''16 fa''8. fa''16 |
re''4\trill la'8. la'16 la'8 si'16 do'' |
si'8\trill si' r re''16 mi'' do''8\trill do''16 si' la'8.\trill sol'16 |
sol'4 r16 si' si' re'' sol'8. sol'16 la'8. si'16 |
do''8 do'' r mi''16 re'' do''8 do''16 mi'' |
la'8 la' r fa''16 mi'' re''8\trill re''16 do'' |
si'8\trill la'16 sol' do''4. do''16 si' |
do''2. |

