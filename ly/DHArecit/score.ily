\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*3\break s1*2\break s2. s1\break s1 s2.\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
