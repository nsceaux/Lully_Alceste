\clef "dessus" r2 r4 do''8 re'' |
mi''4 mi''8 fa'' sol''4 sol'8 la' |
si'4 si'8 dod'' re''4 la'8 si' |
do''4. do''8 do''4 re'' |
mi'' mi''8 re'' dod''4. si'16 dod'' |
re''8 fad'' sol'' la'' sib''4. sib''8 |
sib''4 la''8 sol'' fad''4.( mi''16 fad'') |
sol''4 re''8 mi'' fa''4 sol''8 fa'' |
mi''4.\trill fa''16 sol'' fa''4.\trill mi''8 |
re''4. re''8 re''4.(\trill do''16 re'') |
mi''4 fa''8 sol'' re''4.\trill do''8 |
do''4 r r2 |
R1 R2. R1*2 R2.*4 |
