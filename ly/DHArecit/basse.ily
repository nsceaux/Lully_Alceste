\clef "basse" do2 do'~ |
do'4 do8 re mi4 mi8 fa |
sol4. sol8 re4. re8 |
la4 sol fa2 |
mi4. mi8 la4 la, |
re2 sol, |
do re |
sol, sol |
do fa, |
sol,8 sol re mi fa4 sol8 fa |
mi4 re8 do sol4 sol, |
<<
  \tag #'basse {
    do4 r r2 |
    R1 R2. R1*2 R2.*4 |
  }
  \tag #'basse-continue {
    do1 |
    fa |
    fad2. |
    sol4 si, do re8 re, |
    sol,2 sol4 fa |
    mi2. |
    fa |
    sol4 mi8 fa sol sol, |
    do2. |
  }
>>
