\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:system-count 3)
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#20 #}))
