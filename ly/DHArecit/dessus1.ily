\clef "dessus" r4 do''8 re'' mi''4 mi''8 fa'' |
sol''4 sol''8 fa'' mi'' sol'' fa'' mi'' |
re''4\trill re''8 mi'' fa''4 sol''8 fa'' |
mi''4. mi''8 la''4. si''8 |
sold''4.\trill fad''16 sold'' la''4 la''8 la'' |
fad''8 re'' mi'' fad'' sol''4. re''8 |
mib'' re'' do'' sib' la'4 re''8 do'' |
si'4\trill si'8 do'' re''4 mi''8 re'' |
do''4. re''16 mi'' re''4.\trill do''8 |
si'4. si'8 si'4.\trill( la'16 si') |
do''4 re''8 mi'' si'4.\trill do''8 |
do''4 r r2 |
R1 R2. R1*2 R2.*4 |
