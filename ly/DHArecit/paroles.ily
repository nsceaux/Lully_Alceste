Le Dieu dont tu tiens la nais -- san -- ce
o -- bli -- ge tous les dieux d’es -- tre d’in -- tel -- li -- gen -- ce
en fa -- veur d’un des -- sein si beau ;
je viens t’of -- frir mon as -- sis -- tan -- ce ;
et Mer -- cu -- re s’a -- van -- ce
pour t’ou -- vrir aux en -- fers un pas -- sa -- ge nou -- veau.
