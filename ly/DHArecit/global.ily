\key do \major
\time 2/2 \midiTempo#160 s1*11
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*2
\digitTime\time 3/4 s2.*4 \bar "|."
