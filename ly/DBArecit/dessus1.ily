\clef "dessus" r2 r4 la'' |
sol''2. sol''4 |
sol''2 fa''4. fa''8 |
fa''4. mi''8 mi''4.\trill re''8 |
re''2 r |
R1*3 R1*4 R1 R1*4 R2. R1*2 R1 R2. R1*3 R1*3 R1*2 R1*3 r2
