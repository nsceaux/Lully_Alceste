s1 <6- 4 2>2 <5/> <9 4> <8 3> <7 _-> <_+> s1 s2 <6- 4> s <6 5>
<_+> \new FiguredBass <6 4>4.. <5 _+>16 <_+>2\figExtOn <_+>\figExtOff
s2. <6 5 _->4 <_+>2.\figExtOn <_+>4\figExtOff s1 s2... <6 4+>16
s1 s2. <6 5>4 <_+>2.\figExtOn <_+>4\figExtOff <_+>2 <_+> <6>2.
<_->1 s s2 <6>4 <5/> s4 <6->2 <7->4 <6> <7> <6> <7 _+> <6 4> <5 _+>2
s2 <7>4 <6> <_+>2. <_+>4 <6>2.\figExtOn <6>4\figExtOff <5/>1
s2 <7>4 <6> s2 <7>4 <6> <_+>2.\figExtOn <_+>4\figExtOff
s2. <6 5 _->4 <_+>2.\figExtOn <_+>4\figExtOff