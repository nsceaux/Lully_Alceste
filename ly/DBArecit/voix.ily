<<
  %% Pheres
  \tag #'(recit basse) {
    \clef "vtaille" R1*4 |
    r4 <>^\markup\character Pheres r8 la fa8.\trill fa16 fa8. fa16 |
    re4 r8 re' sib4 sib8 sib16 la |
    la4\trill r8 re' si4\trill si8 si16 si |
    sold4 r8 mi16 mi la4. la16 sold! |
    la4 la
  }
  %% Choeur
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R1*8 r2 }
>>
<<
  %% Pheres
  \tag #'recit { r2 R1*2 r2 }
  %% Choeur
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "vdessus" <>^\markup\character Chœur
    r4 dod'' | re''2 r4 mi'' | dod''2 r4 dod'' | re''2
  }
  \tag #'vhaute-contre {
    r4 la' | fa'2 r4 sol' | mi'2 r4 mi' | fa'2
  }
  \tag #'vtaille {
    r4 mi' | re'2 r4 sib | la2 r4 la | la2
  }
  \tag #'vbasse {
    r4 la | sib2 r4 sol | la2 r4 la | re2
  }
>>
<<
  %% Pheres
  \tag #'(recit basse) {
    \tag #'basse \ffclef "vtaille" <>^\markup\character Pheres
    r4 fa' | re'\trill r8 re'16 re' la4 la8. si16 | do'4 do'
  }
  %% Choeur
  \tag #'(vdessus vhaute-contre vtaille vbasse) { r2 R1 r2 }
>>
<<
  %% Pheres
  \tag #'recit { r2 R1*2 r2 }
  %% Choeur
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "vdessus"
    r4 do'' | do''2 r4 re'' | si'2 r4 mi'' | dod''2
  }
  \tag #'vhaute-contre {
    r4 mi' | fa'2 r4 fa' | mi'2 r4 mi' | mi'2
  }
  \tag #'vtaille {
    r4 do' | la2 r4 si | sold2 r4 sold | la2
  }
  \tag #'vbasse {
    r4 la | fa2 r4 re | mi2 r4 mi | la,2
  }
>>
<<
  \tag #'(recit basse) {
    %% Pheres
    \tag #'basse \ffclef "vtaille" <>^\markup\character Pheres
    r4 la8 la |
    re'8. re'16 la8\trill la r re' |
    sol4 sol
    %% Cleante
    \ffclef "vbasse" <>^\markup\character Cleante
    r4 sib |
    sol2\trill r4 do' |
    la\trill r8 la16 la sol8. sol16 sol8 fa16\trill mi |
    fa8 fa r sib sib8. sib16 |
    sib8. la16 la8. la16 la[ sol] sol8 r8 sol16 sol |
    sol4 r8 fa8 mi4\trill mi8. fa16 |
    re4 r8 la re'4. sol8 |
    la2
  }
  %% Choeur
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    r2 R2. R1*3 R2. R1*3 r2
  }
>>
<<
  %% Pheres
  \tag #'recit { r2 R1*2 r4 }
  %% Choeur
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "vdessus"
    r4 dod'' | re''2 r4 re'' | mi''2 r4 mi'' | la'
  }
  \tag #'vhaute-contre {
    r4 mi' | fa'2 r4 fa' | sol'2 r4 mi' | fa'
  }
  \tag #'vtaille {
    r4 la | la2 r4 la | la2 r4 la | la
  }
  \tag #'vbasse {
    r4 la | fa2 r4 fa | dod2 r4 dod | re
  }
>>
<<
  %% Pheres
  \tag #'(recit basse) {
    \ffclef "vtaille" <>^\markup\character Pheres
    la8. sib16 sol8\trill sol16 sol do'8. do'16 |
    la8\trill la r8 la16 la re'8 re'16 re' re'8. mi'16 |
    dod'2
  }
  %% Choeur
  \tag #'(vdessus vhaute-contre vtaille vbasse) { r4 r2 R1 r2 }
>>
<<
  %% Pheres
  \tag #'recit { r2 R1*2 r2 }
  %% Choeur
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "vdessus"
    r4 dod'' | re''2 r4 mi'' | dod''2 r4 dod'' | re''2
  }
  \tag #'vhaute-contre {
    r4 mi' | fa'2 r4 sol' | mi'2 r4 mi' | fa'2
  }
  \tag #'vtaille {
    r4 mi' | re'2 r4 sib | la2 r4 la | la2
  }
  \tag #'vbasse {
    r4 la | sib2 r4 sol | la2 r4 la | re2
  }
>>
