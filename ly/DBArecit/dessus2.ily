\clef "dessus" r2 r4 fa'' |
mi''2. mi''4 |
la'2 re''4. re''8 |
re''4. dod''8 dod''4. re''8 |
re''2 r |
R1*3 R1*4 R1 R1*4 R2. R1*2 R1 R2. R1*3 R1*3 R1*2 R1*3 r2
