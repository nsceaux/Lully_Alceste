\clef "basse" re2 re' |
re' dod' |
re'2. re4 |
sol2 la |
<<
  \tag #'basse {
    re2 r | R1*3 | r2 r4
  }
  \tag #'basse-continue {
    re1~ |
    re~ |
    re |
    mi2 mi, |
    la,2.
  }
>> la4 |
sib2 r4 sol |
la2 r4 la |
<<
  \tag #'basse { re2 r R1 r2 r4 }
  \tag #'basse-continue {
    re1~ |
    re |
    la,2.
  }
>> la4 |
fa2 r4 re |
mi2 r4 mi |
<<
  \tag #'basse {
    la,2 r | R2. R1*3 R2. R1*3 r2 r4
  }
  \tag #'basse-continue {
    la,1 |
    fad,2. |
    sol,1 |
    do |
    fa2 dod |
    re2. |
    do2 sib, |
    la,1 |
    re4. do8 sib,2 |
    la,2.
  }
>> la4 |
fa2. fa4 |
dod2. dod4 |
<<
  \tag #'basse {
    re4 r r2 | R1 r2 r4
  }
  \tag #'basse-continue {
    re2 mi |
    fa sib |
    la2.
  }
>> la4 |
sib2. sol4 |
la2. la4 |
re2

