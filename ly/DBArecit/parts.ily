\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse)
   (basse-continue #:system-count 7
                   #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#34 #}))
