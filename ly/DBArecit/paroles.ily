\tag #'(recit basse) {
  Voy -- ons en -- cor mon fils, al -- lons, has -- tons nos pas ;
  ses yeux vont se cou -- vrir d’é -- ter -- nel -- les te -- ne -- bres.
}
\tag #'(choeur basse) {
  He -- las ! he -- las ! he -- las !
}
\tag #'(recit basse) {
  Quels cris ! quel -- les plain -- tes fu -- ne -- bres !
}
\tag #'(choeur basse) {
  He -- las ! he -- las ! he -- las !
}
\tag #'(recit basse) {
  Où vas- tu ? Cle -- an -- te, de -- meu -- re.
  
  He -- las ! he -- las !
  Le roy touche à sa der -- nière heu -- re,
  il s’af -- foi -- blit, il faut qu’il meu -- re,
  et je viens pleu -- rer son tres -- pas.
  He -- las ! he -- las !
}
\tag #'(choeur basse) {
  He -- las ! he -- las ! he -- las !
}
\tag #'(recit basse) {
  On le plaint, tout le mon -- de pleu -- re,
  mais nos pleurs ne le sau -- vent pas.
  He -- las ! he -- las !
}
\tag #'(choeur basse) {
  He -- las ! he -- las ! he -- las !
}
