\clef "basse"
<<
  \tag #'basse-continue {
    fa,1 |
    fa4. fa8 mi4 fa |
    do2. la,4 |
    sib,2 do |
    fa,1 |
    sib,2. si,4 |
    do4. re8 mi do |
    fa2. |
    mi |
    re |
    do |
    la, |
    sib,4 sol, do |
    fa,2. |
  }
  \tag #'basse { R1*6 R2.*8 }
>>
fa4 fa fa |
mi2 fa4 |
re sol2 |
do do'8 sib |
la4 la8 sol fa4 |
sib4 sib8 do' re'4 |
sol2 sol4 |
do'8 re' do' sib la sol |
fa4 do2 |
fa,2. |
<<
  \tag #'basse { R1 R2.*7 r4 }
  \tag #'basse-continue {
    fa,2 fa4 re8. do16 |
    sib,4. do8 re sib, |
    fa2. |
    mi |
    re |
    do |
    la, |
    sib,4. sol,8 do4 |
    fa,4
  }
>> fa4 fa16 fa fa fa fa8. fa16 |
sib4 sib <<
  \tag #'basse { r2 | R1*3 r2 r4 }
  \tag #'basse-continue {
    sib,2~ |
    sib, mib |
    lab2. lab4 |
    fa2 sol4. la8 |
    si2 do'4
  }
>> do'8 do' |
la4 la8 fa do4 do8 do |
<<
  \tag #'basse { fa,2 r R1 }
  \tag #'basse-continue {
    fa,2~ fa,8 sol, fa, mi, |
    re,1 |
  }
>>
