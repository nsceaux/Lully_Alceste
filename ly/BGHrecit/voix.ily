%% Licomede et Straton
<<
  %% Licomede
  \tag #'(voix3 basse) {
    \clef "vbasse" R1*4
    <>^\markup\character Licomede
    r8 fa fa sol la\trill la la sib16 do' |
    re'4 r8 sib sib16 sib sib re' sol8.\trill sol16 |
    sol2. |
    r4 la la |
    sol2\trill sol4 |
    fa fa sol |
    mi2\trill mi4 |
    r fa do |
    re4. sol8 mi4\trill |
    fa2. |
    R2.*10
  }
  % Straton
  \tag #'voix4 {
    \clef "vbasse" R1*6 |
    R2. |
    <>^\markup\character Straton
    r4 fa fa |
    mi2\trill mi4 |
    re re re |
    do2 do4 |
    r la, la, |
    sib,4. sol,8 do4 |
    fa,2. |
    R2.*10
  }
  \tag #'(voix1 voix2) {
    R1*6 R2.*18
  }
>>
%% Admete et Alcide
<<
  %% Admete
  \tag #'(voix3 basse) {
    \ffclef "vtaille" <>^\markup\character Admete
    fa'4 r8 do' la8.\trill la16 la8 sib16 do' |
    re'2. |
  }
  %% Alcide
  \tag #'voix4 {
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    la4 r8 la fa8. fa16 re8 re16 do |
    sib,2. |
  }
  \tag #'(voix1 voix2) { R1 R2. }
>>
%% Licomede et Straton
<<
  %% Licomede
  \tag #'(voix3 basse) {
    \ffclef "vbasse" <>^\markup\character Licomede
    r4 la la |
    sol2\trill sol4 |
    fa fa sol |
    mi2\trill mi4 |
    r fa do |
    re4. sol8 mi4\trill |
    fa4
  }
  % Straton
  \tag #'voix4 {
    \ffclef "vbasse" <>^\markup\character Straton
    r4 fa fa |
    mi2\trill mi4 |
    re re re |
    do2 do4 |
    r la, la, |
    sib,4. sol,8 do4 |
    fa,4
  }
  \tag #'(voix1 voix2) {
    R2.*6 r4
  }
>>
%% Chœur
<<
  \tag #'(voix1 basse) {
    \ffclef "vdessus" <>^\markup\character Chœur
    do''4 do''16 do'' do'' do'' do''8. fa''16 |
    re''4\trill re''
  }
  \tag #'voix2 {
    \ffclef "vhaute-contre"
    fa'4 fa'16 fa' fa' fa' fa'8. fa'16 |
    fa'4 fa'
  }
  \tag #'voix3 {
    \ffclef "vtaille"
    la4 la16 la la la la8. la16 |
    sib4 sib
  }
  \tag #'voix4 {
    \ffclef "vbasse"
    fa4 fa16 fa fa fa fa8. fa16 |
    sib4 sib
  }
>>
<<
  %% Céphise
  \tag #'(voix1 basse) {
    \ffclef "vbas-dessus" <>^\markup\character Céphise
    r8 fa''16 fa'' re''8\trill re''16 re'' |
    sib'4
    \tag #'voix1 { r4 r2 R1*2 r2 r4 }
  }
  %% Alceste
  \tag #'voix2 {
    \ffclef "vbas-dessus" <>^\markup\character Alceste
    r8 fa'16 fa' sib'8 sib'16 sib' |
    re''4
    \tag #'voix2 { r4 r2 R1*2 r2 r4 }
  }
  %% Admete
  \tag #'(voix3 basse) {
    <<
      \tag #'basse { s2 s2.. \ffclef "vtaille" <>^\markup\character Admete }
      \tag #'voix3 { \ffclef "vtaille" <>^\markup\character Admete r2 r r4 r8 }
    >> mib'8 |
    do'4\trill do' r mib' |
    fa'8 fa' fa' mib' re'4\trill sol'8 sol' |
    re'4 re'8 sol' mi'4\trill
  }
  %% Alcide
  \tag #'(voix4 basse) {
    <<
      \tag #'basse { s2 s4 \ffclef "vbasse-taille" <>^\markup\character Alcide }
      \tag #'voix4 { \ffclef "vbasse-taille" <>^\markup\character Alcide r2 r4 }
    >> r8 re' sol sol
    \tag #'voix4 {
      r4 |
      r2 r4 do' |
      re'8 re' re' do' si4 sol8 la |
      si4 si8 si do'4
    }
  }
>>
%% Chœur
<<
  \tag #'(voix1 basse) {
    \ffclef "vdessus" <>^\markup\character Chœur
    do''8 do'' |
    fa''4 fa''8 fa'' fa''4 mi''8.\trill fa''16 |
    fa''2 r |
    R1 |
  }
  \tag #'voix2 {
    \ffclef "vhaute-contre"
    sol'8 sol' |
    la'4 la'8 la' sol'4 sol'8 la' |
    fa'2 r |
    R1 |
  }
  \tag #'voix3 {
    \ffclef "vtaille"
    do'8 do' |
    do'4 do'8 do' do'4 do'8 do' |
    la2 r |
    R1 |
  }
  \tag #'voix4 {
    \ffclef "vbasse"
    do'8 do' |
    la4 la8 fa do4 do8 do |
    fa,2 r |
    R1 |
  }
>>