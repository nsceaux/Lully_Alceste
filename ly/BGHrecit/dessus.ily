\clef "dessus" R1*6 R2.*8
fa'8 sol' la' sib' do'' re'' |
do''2 do''4 |
re''8 do'' re'' mi'' fa'' sol'' |
mi''4.\trill re''8 do''4 |
fa'' fa''8 sol'' la''4 |
re''2 re''4 |
sib''8 la'' sol'' la'' sol'' fa'' |
mi''4.\trill mi''8 fa''4~ |
fa''8 sol'' sol''4.\trill fa''8 |
fa''2. |
R1 R2.*7 R1*9 |
