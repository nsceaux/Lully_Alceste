\piecePartSpecs
#`((dessus #:music , #{ s1*6 s2.*8 s2.*3\break s2.*5\break #})
   (haute-contre)
   (taille #:system-count 3)
   (quinte)
   (basse #:system-count 3)
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#41 #}))
