\clef "haute-contre" R1*6 R2.*8
do'4 fa'8 sol' la' sib' |
sol'4. fa'16 sol' la'4 |
la' si'2 |
do'' do''4 |
do'' do''8 sib' la'4 |
sib'2 sib'8 do'' |
re'' do'' sib' do'' sib' la' |
sol'4. sol'8 la'4~ |
la'8 sib' do''4. sib'8 |
la'2. |
R1 R2.*7 R1*9 |
