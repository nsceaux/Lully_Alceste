\clef "taille" R1*6 R2.*8
la8 sib do'4 do' |
do' sol' fa' |
fa'8 mi' re' sol' sol'4 |
sol'4. fa'8 mi'4 |
la'8 sol' fa'4 fa' |
fa'2 fa'4 |
sol'2 sol'4 |
sol'4. sol'8 fa'4 |
fa' mi'4. re'16 mi' |
fa'2. |
R1 R2.*7 R1*9 |
