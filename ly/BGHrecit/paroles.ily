\tag #'(voix3 basse) {
  On vous a -- pres -- te
  dans mon vais -- seau
  un di -- ver -- tis -- se -- ment nou -- veau.
}
\tag #'(voix3 voix4 basse) {
  Ve -- nez voir ce que nos -- tre fes -- te
  doit a -- voir de plus beau.
}
\tag #'(voix3 voix4 basse) {
  Dieux ! le pont s’a -- bis -- me dans l’eau.
}
\tag #'(voix3 voix4 basse) {
  Ve -- nez voir ce que nos -- tre fes -- te
  doit a -- voir de plus beau.
}
\tag #'(voix1 voix2 voix3 voix4 basse) {
  Ah quel -- le tra -- hi -- son fu -- nes -- te.
}
\tag #'(voix1 voix2 basse) {
  Au se -- cours, au se -- cours.
}
\tag #'(voix4 basse) {
  Per -- fi -- de…
}
\tag #'(voix3 basse) {
  Al -- ces -- te…
}
\tag #'(voix3 voix4 basse) {
  Lais -- sons les vains dis -- cours.
  Au se -- cours, au se -- cours.
}
\tag #'(voix1 voix2 voix3 voix4 basse) {
  Au se -- cours, au se -- cours, au se -- cours.
}
