\clef "quinte" R1*6 R2.*8
fa'4 fa' fa' |
sol' do'2 |
re'4 re'4. re'8 |
do'4 sol'2 |
fa'4 do' do' |
re'2 re'4 |
re'2 re'4 |
do'2 do'4 |
r8 do' do'2 |
do'2. |
R1 R2.*7 R1*9 |
