\key fa \major
\beginMark "Prélude"
\time 2/2 \midiTempo#160 s1*4
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*8
\beginMark "Prélude 2 fois" \bar "|!:" s2.*10 \bar ":|."
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo#160 s2.*7
\time 4/4 \midiTempo#80 s1*3
\time 2/2 \midiTempo#160 s1*6 \bar "|."
