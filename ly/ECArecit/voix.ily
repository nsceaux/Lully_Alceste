<<
  \tag #'(vdessus basse) {
    <<
      \tag #'basse {
        s8 s1*18 s4 \ffclef "vdessus"
      }
      \tag #'vdessus { \clef "vdessus" r8 R1*18 r4 }
    >>
    r8 <>^\markup\character Chœur do'' la' la' la' la' |
    re''4 re''8 re'' sib'4 sib'8 sib' |
    sol'4 sol' do''8 sib'16 la' sol'8.\trill fa'16 |
    fa'4
    r8 <>^\markup\character Proserpine fa'' do''8. do''16 do''8. re''16 |
    sib'4\trill r8 sib'16 sib' sib'4 r8 sib'16 la' |
    la'8\trill la' r re''16 re'' si'8. do''16 do''8 do''16 si' |
    do''4
    r8 <>^\markup\character Chœur do'' la' la' la' la' |
    re''4 re''8 re'' sib'4 sib'8 sib' |
    sol'4 sol' do''8 sib'16 la' sol'8.\trill fa'16 |
    fa'4
    <>^\markup\character Proserpine
    la'8*3/2 la'8*1/2 si'4. do''8 |
    do''4 do''8. si'16 do''4 do'' |
    r4 do''8 do'' la'4\trill la'8 do'' |
    fa'4 fa''8 fa'' re''4\trill re''8 sib' |
    la'2\trill re''8 re''16 re'' re''8 re'' |
    si'4 sol''8 sol'' mi''4\trill mi''8 do'' |
    fa''2 re''8 re''16 do'' sib'8.\trill la'16 |
    sol'2\trill la'8 la'16 sib' sol'8.\trill fa'16 |
    fa'4
    <>^\markup\character Chœur
    la'8 la' si'4 r8 do'' |
    do''4 do''8 si' do''4 do'' |
    r do''8 do'' la'4\trill la'8 do'' |
    fa'4 fa''8 fa'' re''4 re''8 sib' |
    la'2\trill re''8 re''16 re'' re''8 re'' |
    si'4 sol''8 sol'' mi''4\trill mi''8 do'' |
    fa''2 re''8 re''16 do'' sib'8.\trill la'16 |
    sol'2\trill la'8 la'16 sib' sol'8.\trill fa'16 |
    fa'2.
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r8 R1*18 |
    r4 r8 mi' fa' fa' fa' fa' |
    re'4 re'8 re' sol'4 sol'8 sol' |
    mi'4 mi' sol'8 sol'16 fa' mi'8. fa'16 |
    fa'4 r r2 |
    R1*2 |
    r4 r8 mi' fa' fa' fa' fa' |
    re'4 re'8 re' sol'4 sol'8 sol' |
    mi'4 mi' sol'8 sol'16 fa' mi'8. fa'16 |
    fa'4 r r2 |
    R1*7 |
    r4 do'8 do' re'4 r8 mib'? |
    fa'4 fa'8. fa'16 mi'4 mi' |
    r mi'8 mi' fa'4 do'8 do' |
    re'4 re'8 do' re'4 fa'8 fa' |
    fa'2 fa'8 fa'16 fa' fa'8 fa' |
    re'4 re'8 re' sol'4 sol'8 mi' |
    la'2 fa'8 fa'16 fa' sol'8. fa'16 |
    mi'2 fa'8 fa'16 fa' mi'8. fa'16 |
    fa'2.
  }
  \tag #'vtaille {
    \clef "vtaille" r8 R1*18 |
    r4 r8 do' do' do' do' do' |
    sib4 sib8 sib re'4 re'8 re' |
    do'4 do' do'8 do'16 do' do'8. do'16 |
    la4 r r2 |
    R1*2 |
    r4 r8 do' do' do' do' do' |
    sib4 sib8 sib re'4 re'8 re' |
    do'4 do' do'8 do'16 do' do'8. do'16 |
    la4 r r2 |
    R1*7 |
    r4 la8 la sol4 r8 do' |
    re'4 re'8. re'16 sol4 sol |
    r2 r4 la8 la |
    sib4 sib8 la sib2 |
    do'8 do'16 do' la8 la la2 |
    sol8 sol16 la si8 si do'4 do'8 do' |
    do'4 do'8. do'16 sib4 re'8 re' |
    do'2 do'8 do'16 do' do'8. do'16 |
    la2.
  }
  \tag #'(vbasse basse) {
    \clef "vbasse" r8 R1*11 |
    <>^\markup\character Pluton
    r4 r8 fa la8. la16 la8. sib16 |
    do'4. sol8 sol8. la16 sib8. do'16 |
    la4\trill la8 fa fa8. fa16 sol8. la16 |
    sib4 sib8. re'16 sol4 sol8. sol16 |
    sol4 r8 do sol sol sol la |
    sib4 sib8 sib sib4 sib8. la16 |
    la4\trill la fa8 fa16 mi re8.\trill do16 |
    do4
    \tag #'vbasse {
      r8 do' fa fa fa fa |
      sib4 sib8 sib sol4 sol8 sol |
      do'4 do' mi8 mi16 fa do8. do16 |
      fa,4 r r2 |
      R1*2 |
      r4 r8 do' fa fa fa fa |
      sib4 sib8 sib sol4 sol8 sol |
      do'4 do' mi8 mi16 fa do8. do16 |
      fa,4
      <>^\markup\character Pluton
      fa8*3/2 fa8*1/2 fa4. mib8 |
      re4 re8. re16 do4 do |
      r2 r4 fa8 fa |
      re4 re8 fa sib,2 |
      fa8 fa16 fa fa8 fa re2 |
      sol8 sol16 sol sol8 sol do4 do'8 do' |
      la4 la8 fa sib4 sib8 sol |
      do'2 fa8 fa16 fa do8. do16 |
      fa,4
      fa8 fa fa4 r8 mib |
      re4 re8. re16 do4 do |
      r2 r4 fa8 fa |
      re4 re8 fa sib,2 |
      fa8 fa16 fa fa8 fa re2 |
      sol8 sol16 sol sol8 sol do4 do'8 do' |
      la4 la8 fa sib4 sib8 sol |
      do'2 fa8 fa16 fa do8. do16 |
      fa,2.
    }
  }
>>
