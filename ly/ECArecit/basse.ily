\clef "basse" fa,8 |
fa,2 fa |
mi fa4. fa8 |
sib4 r16 sol la sib do'4 r16 do' sib la |
sib4 r16 sib la sol la4 r16 la sol fa |
sol4 do sol,2 |
do la, |
sol,4 sol re4. re8 |
la4 la, re sol, |
do re8 re, sol,4 sol |
do' la sib sol |
do' fa do2 |
fa, <<
  \tag #'basse { r2 | R1*6 | r4 }
  \tag #'basse-continue {
    fa2 |
    mi1 |
    fa2~ fa8 mib re do |
    sib,2 si, |
    do~ do4 sib,8 la, |
    sol,1 |
    re2 re,8 mi,16 fa, sol,4 |
    do
  }
>> r8 do' fa fa fa fa |
sib4 sib8 sib sol4 sol8 sol |
do'4 do' mi8 mi16 fa do8. do16 |
<<
  \tag #'basse { fa,4 r r2 | R1*2 | r4 r8 }
  \tag #'basse-continue {
    fa,2 fa4. fa8 |
    fa2 mi |
    fa4. fad8 sol8 do sol,4 |
    do4.
  }
>> do'8 fa fa fa fa |
sib4 sib8 sib sol4 sol8 sol |
do'4 do' mi8 mi16 fa do8. do16 |
fa,4 <<
  \tag #'basse { r4 r2 | R1*7 | r4 }
  \tag #'basse-continue {
    fa8. fa16 fa4. mib8 |
    re2 do |
    do'2 fa4 fa8 fa |
    re4 re8 fa sib,2 |
    fa8 fa16 fa fa8 fa re2 |
    sol8 sol16 sol sol8 sol do4 do'8 do' |
    la4 la8 fa sib4 sib8 sol |
    do'2 fa8 fa16 fa do8. do16 |
    fa,4
  }
>> fa8. fa16 fa4 r8 mib |
re4 re8. re16 do4 do |
do'2 fa4 fa8 fa |
re4 re8 fa sib,2 |
fa8 fa16 fa fa8 fa re2 |
sol8 sol16 sol sol8 sol do4 do'8 do' |
la4 la8 fa sib4 sib8 sol |
do'2 fa8 fa16 fa do8. do16 |
fa,2.
