\tag #'(vbasse basse) {
  Re -- çoy le jus -- te prix de ton a -- mour fi -- del -- le ;
  que ton des -- tin nou -- veau soit heu -- reux à ja -- mais :
  com -- men -- ce de goû -- ter la dou -- ceur e -- ter -- nel -- le
  d’u -- ne pro -- fon -- de paix.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Com -- men -- ce de goû -- ter la dou -- ceur e -- ter -- nel -- le
  d’u -- ne pro -- fon -- de paix.
}
\tag #'(vdessus basse) {
  L’es -- pou -- ze de Plu -- ton te re -- tient au -- prés d’el -- le :
  tous tes vœux se -- ront sa -- tis -- faits.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Com -- men -- ce de goû -- ter la dou -- ceur e -- ter -- nel -- le
  d’u -- ne pro -- fon -- de paix.
}
\tag #'(vdessus vbasse basse) {
  En fa -- veur d’une om -- bre si bel -- le,
  \tag #'(vdessus basse) { que l’en -- fer fas -- se voir }
  que l’en -- fer fas -- se voir tout ce qu’il a d’at -- traits,
  \tag #'vbasse { tout ce qu’il a d’at -- traits, }
  que l’en -- fer fas -- se voir
  \tag #'vbasse { fas -- se voir }
  \tag #'(vdessus basse) { tout ce qu’il a d’at -- traits, }
  tout ce qu’il a d’at -- traits.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  En fa -- veur d’une om -- bre si bel -- le,
  \tag #'(vdessus vhaute-contre basse) {
    que l’en -- fer fas -- se voir
  }
  que l’en -- fer fas -- se voir tout ce qu’il a d’at -- traits,
  \tag #'(vtaille vbasse) { tout ce qu’il a d’at -- traits, }
  que l’en -- fer fas -- se voir
  \tag #'(vtaille vbasse) { fas -- se voir }
  \tag #'(vdessus vhaute-contre basse) {
    tout ce qu’il a d’at -- traits,
  }
  tout ce qu’il a d’at -- traits.
}
