\clef "quinte" fa'8 |
fa'2 fa'4 do'8. do'16 |
do'2 do'4. do'8 |
sib4. sol'8 sol'4 fa' |
fa' sib do' la |
sol sol sol sol8. sol16 |
sol4. do'8 do'4. do'8 |
re'4 si8. si16 si4. si8 |
la4. la8 la4 sol8 re' |
do'4 la8 do' re'4 re'8. re'16 |
do'4. fa'8 fa' re' re'8. re'16 |
do'4. do'8 do'4 do'8. do'16 |
do'2 r |
R1*31 |
r2 r4

