\clef "dessus" fa''8 |
fa''4. do''8 do''16 re'' do'' sib' la' sol' la' fa' |
do''4 r16 sol' la' sib' la'8.\trill do''16 fa''8. do''16 |
re'' fa'' sol'' la'' sib''4 r16 sib'' la'' sol'' la''4 |
r16 re'' mi'' fa'' sol''4 r16 do'' re'' mi'' fa''8. fa''16 |
fa''8. sol''16 mi''8.\trill fa''16 re''8. re''16 sol''8. sol''16 |
sol''4 r16*3/2 la''32 sol'' fa'' mi'' re'' do''8. do''16 do''8. re''16 |
si'4 r16 sol'32 la' si' do'' re'' mi'' fa''8. fa''16 fa''8.\trill mi''16 |
mi''4 r16 mi'' la'' sol'' fad''8.*5/6 mi''32 fad'' re'' sib''8. sib''16 |
sib''8. la''16 la''8.\trill sol''16 sol''4~ sol''16 fa'' sol'' re'' |
mi''8[ r16*1/2 re''32 mi'' do''] fa''8[ r16*1/2 mi''32 fa'' do''] re''8. fa''16 sol''32 la'' sib'' la'' sol'' fa'' mi'' re'' |
mi''8.*5/6\trill re''32 mi'' do'' fa''8.*5/6 mi''32 fa'' sol'' sol''4.\trill fa''8 |
fa''2 r |
R1*31 |
r2 r4

