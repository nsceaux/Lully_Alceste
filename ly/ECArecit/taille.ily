\clef "taille" do'8 |
do'2 do'4 do'8. re'16 |
mi'8. fa'16 sol'4 do'8. fa'16 fa'8. fa'16 |
fa'4. sol'8 mi'4 fa' |
re'4. sol'8 do'4 do'8. fa'16 |
re'4 do' re'8. re'16 re'8. do'16 |
do'4 sol'8. sol'16 la'4. la'8 |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 fad'4 sol'8. sol'16 |
sol'8. la'16 fad'8 mi'16 fad' sol'4 sol'8 sol' |
sol'4 sol'8. sol'16 fa'8. re'16 sol'8. sol'16 |
sol'4 fa'8. fa'16 fa'4 mi'8. fa'16 |
fa'2 r |
R1*31 |
r2 r4

