\clef "haute-contre" la'8 |
la'2 fa'4. fa'8 |
sol'2 fa'8. la'16 la'8. la'16 |
sib'4 re''8. do''16 do''4. do''8 |
re''4. re''8 do''4. do''8 |
si'4 do'' do''4 si'8 la'16 si' |
do''4 sol' sol' fad'8 mi'16 fad' |
sol'4 sol' la'8. la'16 la'8. si'16 |
do''4. do''8 do''4 sib'8. re''16 |
mib''4 re''8. do''16 si'4 si'8. si'16 |
do''4 do''8. do''16 sib'!4. sib'8 |
sib'4 la'8. sib'16 do''8. do''16 do''8. sib'16 |
la'2 r |
R1*31 |
r2 r4

