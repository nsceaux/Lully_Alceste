\clef "basse" r8 r do |
do8. do32 do do8 do8 do do |
do8 do16 do do8 do do do |
sol, sol,16 sol, sol,8 sol, sol, sol, |
sol, sol,16 sol, sol,8 sol, sol, sol, |
do do16 do do8 do sol,8. do16 |
do4. |
