\clef "haute-contre" r8 r8 sol' |
sol'4. do''8 do'' do''16 re'' |
mi''8 mi'' mi'' do'' do'' do'' |
si' sol' re' sol' sol' sol' |
sol'4. si'8 si' si' |
do''16 si' do'' re'' mi'' re'' do'' re'' si'8. do''16 |
do''4. |
