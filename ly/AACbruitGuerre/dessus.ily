\clef "dessus" r8 r do'' |
do'' sol' do''16 re'' mi''8 do'' mi''16 fa'' |
sol''8 do''' sol'' mi'' do'' sol''16 sol'' |
sol''4. ~ sol''8 sol'16 la' si' do'' |
re''8 si' sol' re'' sol'' re'' |
mi''16 re'' mi'' fa'' sol'' fa'' mi'' fa'' re''8.\trill do''16 |
do''4. |
