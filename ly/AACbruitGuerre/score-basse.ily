\score {
  \new GrandStaff <<
    \new Staff << \global \includeNotes "basse" >>
    \new Staff << \global \includeNotes "basse-continue" >>
  >>
  \layout { system-count = 2 }
}
