\piecePartSpecs
#`((dessus #:system-count 2)
   (haute-contre #:system-count 2)
   (taille #:system-count 2)
   (quinte #:system-count 2)
   (basse #:notes "basse-continue" #:system-count 2)
   (basse-continue #:notes "basse-continue" #:system-count 2)
   (timbales #:notes "basse" #:system-count 2)
   (silence #:on-the-fly-markup , #{ \markup\tacet#12 #}))
