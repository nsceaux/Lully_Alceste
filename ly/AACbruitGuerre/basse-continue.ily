\clef "basse"
%% do2. | do2 do4 | do2 do4 | sol,2 sol,4 | sol,2 sol4 | mi do sol, | do2. |
do4 do8 do4. |
do4 do8 do4. |
do4 do8 sol,4. |
sol,4 sol,8 sol,4. |
sol,4 sol8 |
mi4 do8~ do sol,4 |
do4. |

