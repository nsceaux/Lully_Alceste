\clef "haute-contre" r4 r do''8. do''16 |
do''4 do''8. do''16 do''8. do''16 |
do''4 do'' do''8. do''16 |
si'4 si'8. si'16 si'8 la' |
sol'4 si'8 |
do'' do'' do'' |
la' la' si' |
sold'4 sold' r |
R2.*2 |
R4. |
dod''8 dod'' dod'' |
re''4 re'' r |
R2.*3 R1 R2.*3 |
do''4 do''8 do'' do''4 |
si' si' si'8 si' |
do''4 do''8 do'' do''4 |
si'2 do''4~ |
do''4 si'4.\trill do''8 |
do''2. |
