\tag #'(vdessus vhaute-contre vtaille vbasse) {
  A -- che -- vons d’em -- por -- ter la pla -- ce ;
  L’en -- ne -- my com -- mence à pli -- er.
  Main bas -- se, main bas -- se, main bas -- se.

  Quar -- tier, quar -- tier, quar -- tier.

  La ville est pri -- se.

  Quar -- tier, quar -- tier, quar -- tier.
}
\tag #'(vhaute-contre basse) {
  Il faut ren -- dre Cé -- phi -- se.
}
\tag #'(vbasse basse) {
  Je suis ton pri -- son -- nier,
  quar -- tier, quar -- tier, quar -- tier.
}
