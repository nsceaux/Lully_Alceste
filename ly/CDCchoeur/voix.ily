%% Chœur
<<
  \tag #'vdessus {
    \clef "vdessus" <>^\markup\character Chœur
    r4 r mi''8. mi''16 |
    mi''4 mi''8 mi'' fa''8. fa''16 |
    mi''4 mi'' mi''8. mi''16 |
    re''4 re''8 re'' re'' re'' |
    re''4 re''8 |
    mi'' mi'' mi'' |
    do'' do'' re'' |
    si'4\trill si' do'' |
    do'' r si' |
    si' r mi'' |
    dod''4. |
    dod''8 dod'' dod'' |
    re''4 re'' si' |
    si' r la' |
    la' r re'' |
    si'
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r4 r sol'8. sol'16 |
    sol'4 sol'8 sol' la'8. la'16 |
    sol'4 sol' sol'8 sol' |
    sol'4 sol'8 sol' sol' fad' |
    sol'4 sol'8 |
    sol' sol' sol' |
    fa' fa' fa' |
    mi'4 mi' mi' |
    fa' r fa' |
    mi' r mi' |
    mi'4. |
    mi'8 mi' mi' |
    fad'4 fad' re' |
    mi' r mi' |
    re' r re' |
    re'4
  }
  \tag #'vtaille {
    \clef "vtaille" r4 r do'8. do'16 |
    do'4 do'8. do'16 do'8. do'16 |
    do'4 do' do'8 do' |
    si4 si8 si si la |
    si4 si8 |
    do' do' do' |
    la la si |
    sold4 sold la |
    la r la |
    la r sold |
    la4. |
    la8 la la |
    la4 la sol |
    sol r sol |
    sol r fad |
    sol4
  }
  \tag #'vbasse {
    \clef "vbasse" r4 r do'8. do'16 |
    do'4 do'8. do'16 fa8. fa16 |
    do4 do do8 do |
    sol4 sol8 sol sol re |
    sol4 sol8 |
    do' do' do |
    fa fa re |
    mi4 mi do |
    re r re |
    mi r mi |
    la,4. |
    la8 la la |
    re4 re si, |
    do r do |
    re r re |
    sol,
  }
  \tag #'basse { R2.*4 R4.*3 R2.*3 R4.*2 R2.*3 r4 }
>>
%% Lychas, Straton
<<
  \tag #'(vdessus vtaille) { r4 r R1 R2.*9 }
  \tag #'(vhaute-contre basse) {
    \ffclef "vhaute-contre" <>^\markup\character Lychas
    r8 sol'16 sol' re'8 mi'16 fa' |
    mi'4\trill
    <<
      \tag #'basse { mi'8 }
      \tag #'vhaute-contre { mi'4 r2 | R2.*9 }
    >>
  }
  \tag #'(vbasse basse) {
    <<
      \tag #'basse { s2 s4. \ffclef "vbasse" }
      \tag #'vbasse { r4 r | r4 r8 }
    >> <>^\markup\character Straton
    do'8 sol4 sol8 la16 sib |
    la4\trill r fa |
    fa r mi |
    re r sol |
    mi\trill r r |
    R2.*5 |
  }
>>
