\clef "taille" r4 r sol'8. sol'16 |
sol'4 sol'8. sol'16 la'8. la'16 |
sol'4 sol' sol'8. sol'16 |
sol'4 re'8. re'16 re'8 re' |
re'4 sol'8 |
sol' sol' sol' |
fa' fa' fa' |
mi'4 mi' r |
R2.*2 R4. |
la'8 la' la' |
la'4 la' r |
R2.*3 R1 R2.*3 |
mi'8 fa' sol' fa' mi' fa' |
re'4. sol'8 sol' sol' |
sol' fa' mi' fa' sol'4 |
sol'2 sol'4~ |
sol' sol'4. sol'8 |
sol'2. |
