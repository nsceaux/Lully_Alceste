\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse #:system-count 3)
   (basse-continue #:score-template "score-basse-voix")
   (timbales)
   (silence #:on-the-fly-markup , #{ \markup\tacet#26 #}))
