\key do \major \midiTempo#132
\digitTime\time 3/4 s2.*4
\time 3/8 s4.*3
\digitTime\time 3/4 s2.*3
\time 3/8 s4.*2
\digitTime\time 3/4 s2.*4
\time 4/4 s1
\digitTime\time 3/4 s2.*9 \bar "|."