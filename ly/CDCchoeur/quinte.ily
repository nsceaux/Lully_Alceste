\clef "quinte" r4 r do'8. do'16 |
do'4 do'8. do'16 do'8. do'16 |
do'4 do' sol8. sol16 |
sol4 sol8. sol16 sol8 la |
si4 re'8 |
do' do' do' |
do' do' do' |
si4 si r |
R2.*2 R4. |
mi'8 mi' mi' |
re'4 re' r |
R2.*3 R1 R2.*3 |
sol4 do'8 do' do'4 |
re' re'8 re' re'4 |
do' do'8 do' do'4 |
re'2 mi'4~ |
mi' re'4. do'8 |
do'2. |
