\clef "basse"
<<
  \tag #'basse r2
  \tag #'basse-continue do2
>> do'8 do' |
do'4 do'8 do' fa8. fa16 |
do4 do do8. do16 |
sol4 sol8 sol sol8 re |
sol4 sol8 |
do' do' do |
fa fa re |
mi4 mi do4 |
re r re |
mi r mi |
la,4. |
la8 la la |
re4 re si,4 |
do r do |
re r re |
<<
  \tag #'basse { sol,4 r r R1 R2.*3 }
  \tag #'basse-continue {
    sol,2. |
    do1 |
    fa,2. |
    sol,~ |
    sol, |
  }
>>
do4 do8 do do4 |
sol, sol,8 sol, sol,4 |
do do8 do do4 |
sol,2 do4~ |
do sol,2 |
do,2. |
