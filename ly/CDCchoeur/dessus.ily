\clef "dessus" r4 r mi''8. mi''16 |
mi''4 mi''8. mi''16 fa''8. fa''16 |
mi''4 mi'' mi''8. mi''16 |
re''4 re''8. sol''16 sol''8 fad'' |
sol''4 re''8 |
mi'' mi'' mi'' |
do'' do'' re'' |
si'4 si' r |
R2.*2 |
R4. |
la''8 la'' mi'' |
fad''4 fad'' r |
R2.*3 R1 R2.*3 |
do''8 re'' mi'' fa'' sol'' la'' |
sol''4 re'' sol''8 fa'' |
mi''4 do''8 re'' mi'' fa'' |
sol''2 sol''4~ |
sol'' sol''4.\trill fa''8 |
mi''2. |
