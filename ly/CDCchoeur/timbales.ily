\clef "basse" R2.*4 R4.*3 R2.*3 R4.*2 R2.*4 R1 R2.*3
do4. do16 do do4 |
sol,4. sol,16 sol, sol,4 |
do4. do16 do do4 |
sol,2 do4~ |
do sol,4. do8 |
do2. |
