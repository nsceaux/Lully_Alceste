\score {
  \new GrandStaff <<
    \new Staff <<
      \global \keepWithTag #'dessus1 \includeNotes "dessus"
      { s1*32 s2. s1*6 s2.*4 s1 s2. s1*35 s1*8 s1 s1 s2. s1*3 s1*7% \bar "||"
        s1\break s1*18 s1*31 }
    >>
    \new Staff \with { \haraKiriFirst } <<
      \global \keepWithTag #'dessus2 \includeNotes "dessus"
    >>
  >>
  \layout { }
}