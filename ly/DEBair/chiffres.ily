s1 s2 <6- 4> <7> <6> <7> <6> <6- 4> <6 5/> <_+>\figExtOn <_+>\figExtOff
s <6 5> <_+>1\figExtOn <_+>\figExtOff
s4 <6 5> <4> \new FiguredBass <_+> <_+>1 <_+>2 <6 4+> <6>1 <_-> s
s2 <6 4+> <6>1 <6 5>2 <4>4 <3> s1 <7>2 <6> <_+>1 <6 4+>
<6>4 <6 5 _-> <4> <_+> s1 s2. <_+>4 <_->1 <7 _+>
<7 _->2 <_+>4\figExtOn <_+>\figExtOff
s1*4 s2. s1*6 s2.*4 s1 s2.
s1*35
s2 <_+> <6 +4 2> <6> s s8 \new FiguredBass { <6 5 _-> <4> <_+> }
s2 <_+> s1 <_+>2\figExtOn <_+>\figExtOff s <6 5 _->
<4>4. <_+>8 s2 s1*2 <6->4 <6 5> <4>8 \new FiguredBass <3> s2 <_+>
<6 4+ 2>2 <6> s s8 \new FiguredBass { <6 5 _-> <4> <_+> } s1
<6>4 <_->2. s2.. <6 5>8 <4>4 <3>2. s2 <6> <_+> <6>4 <6 5 _-> <7 _+>1

s2 <6> <6>4\figExtOn <6>\figExtOff s2 s1*2
<6>4\figExtOn <6>\figExtOff <6>2 <4>2\figExtOn <4>4\figExtOff <3>
s2 <6> s2 <6 5 _->4\figExtOn <6>\figExtOff <_+>2 <6> s <6>
s1*2 <5/>4\figExtOn <5/>\figExtOff s2 <4> <_+> s <7>4 <6>
<5/>1 s <6>2 <6 5/> <4>2\figExtOn <4>4\figExtOff <3>
s2 <6> s <6> s <6 5 _->4\figExtOn <6>\figExtOff <_+>2 <6>
s <6> <7 _+>4 <3> <_+>2 s2 <6>
s2 <6> s <6> s <6 5 _->4\figExtOn <6>\figExtOff <_+>2 <6>
s <6> <7 _+>4 <3> <_+>2 s1
<6> s s2 <6> <7>4 <6>2\figExtOn <6>4\figExtOff
<5/>4 <3>2\figExtOn <3>4\figExtOff <7>2 <6> s1*2 <6>4 <6 5>2.
s1*2 s2 <6> <7>4 <6>2\figExtOn <6>4\figExtOff
<5/>4 <3>2\figExtOn <3>4\figExtOff <7>2 <6> s1*2 <6>4 <6 5>2.

