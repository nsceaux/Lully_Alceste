\clef "taille"
R1*32 R2. R1*6 R2.*4 R1 R2. R1*35 R1*8 R1 R1 R2. R1*3 R1 R1*6
R1*19
r4 mi' re' sol' |
sol' sol' fa' fa' |
fa' fa' mi' mi' |
mi' dod' re' re' |
re' re' mi' mi' |
re' re' re'4. la8 |
si1 |
r4 mi' re' sol' |
sol' sol' fa' fa' |
fa' fa' mi' mi' |
mi' dod' re' re' |
re' re' mi'4. mi'8 |
re'4 re' re'4. la8 |
si1 |
mi'4 mi' mi' mi' |
fa' fa'8 sol' la' sol' fa' mi' |
re'4 re' re' sol' |
la'2. la'4 |
sol'4 sol' sol' sol' |
fa'2 fa'8 sol' fa' mi' |
re' do' si do' re'4 sol' |
fa'8 sol' la'4 la' la' |
sol' fa' fa'4.\trill mi'8 |
mi'4 mi' mi' mi' |
fa' fa'8 sol' la' sol' fa' mi' |
re'4 re' re' sol' |
la'2. la'4 |
sol'4 sol' sol' sol' |
fa'2 fa'8 sol' fa' mi' |
re' do' si do' re'4 sol' |
fa'8 sol' la'4 la' la' |
sol'4 fa' fa'4.\trill mi'8 |
mi'2. r4 |
sol'4 fa' fa'4.\trill mi'8 |
mi'2.
