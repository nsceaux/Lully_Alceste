\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new GrandStaff <<
        \new Staff <<
          \global \keepWithTag #'dessus1 \includeNotes "dessus"
        >>
        \new Staff <<
          \global \keepWithTag #'dessus2 \includeNotes "dessus"
        >>
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'femme \includeNotes "voix"
    >> \keepWithTag #'femme \includeLyrics "paroles"
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*5\pageBreak
        s1*4 s2 \bar "" \break s2 s1*3 s2 \bar "" \break
        s2 s1*3\break s1*4\break s1*5\pageBreak
        s1*6\break s2. s1*2\break s1*4 s2.\pageBreak
        s2.*3 s1\break s2. s1*3\pageBreak
        s1*5\break s1*5\pageBreak
        s1*5\break s1*4\pageBreak
        s1*5\break s1*7\break s1*3\pageBreak
        s1*3\break s1*3\pageBreak
        s1*2 s2.\break s1*3\break s1*5\break s1 s2 \bar "" \pageBreak
        s2 s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*5\break s1*7\pageBreak
        s1*7\break s1*8\break s1*8\pageBreak
        s1*4\break s1*4\pageBreak
        s1*4\break s1*4\pageBreak
        s1*5\break s1*7\pageBreak
        s1*7\break s1*8\break
      }
      \modVersion {
        s1*32 s2. s1*6 s2.*4 s1 s2. s1*27%\break
      }
    >>
  >>
  \layout { }
  \midi { }
}