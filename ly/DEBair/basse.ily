\clef "basse"
<<
  \tag #'basse { R1*24 }
  \tag #'basse-continue {
    do1 |
    do |
    si,2 sib, |
    lab,1 |
    sol,2 fad, |
    sol, sol |
    do1 |
    re~ |
    re |
    mib4 do re re, |
    sol,1 |
    sol2 fa |
    mi1 |
    fa2 fa, |
    sib,1 |
    sib2 lab |
    sol1 |
    lab2 sib4 sib, |
    mib1 |
    fa2 fad |
    sol1 |
    fa |
    mib4 fa sol sol, |
    do1 | \allowPageTurn
  }
>>
do'2 sol4 do' |
fa2 sib4 lab |
sol2 do |
fa sol4 sol, |
do2 <<
  \tag #'basse { r2 R1*3 R2. R1*4 r2 r4 }
  \tag #'basse-continue {
    do'2~ |
    do' sib |
    lab2. sol8 fa |
    mi1 |
    fa4 re8 mib do4 |
    sib,2 sib4 lab |
    sol fa mib re |
    do re mib2 |
    fa1 |
    sol2.
  }
>> sol4 |
do' do' do'4. do'8 |
lab4 lab8 lab mi8. mi16 |
fa4 fa8 fa fa8. fa16 |
do4 do8 do do8. do16 |
sol,4 sol, r8 sol |
do'4 do' lab fa |
sib8 sib sol mib sib sib, |
<<
  \tag #'basse { mib2 r2 | \allowPageTurn R1*5 r2 r4 }
  \tag #'basse-continue {
    mib1~ |
    mib2 mi |
    fa1~ |
    fa2 fad |
    sol1 |
    fa2 mib4. fa8 |
    sol2.
  }
>> sol4 |
do' do' do4. do8 |
fa2 r4 fa |
fa1 |
sol2 r4 sol |
mib do fa fa, |
sib,1 |
do |
re2 r4 re |
sol do re4. re8 |
sol,2. sol4 |
sol2. sol4 |
sol fa mib re |
do2. do4 |
fa mib re do |
si,2 sol,4 sol |
do' sib lab sol |
fa2 r4 fa |
sol2 r4 do |
sol,2 r4 sol, |
do1~ |
do2. do4 |
re2 do |
re2. re4 |
sol fa mib re |
do sib, lab, sol, |
fa,2 fa4. fa8 |
fa2 mib4 do |
sol2 sol, |
do <<
  \tag #'basse { r2 R1*2 r2 }
  \tag #'basse-continue {
    sol2 |
    fa mib |
    do lab8 fa sol sol, |
    do2
  }
>> do8 do do do |
fa4 fa re4. re8 |
sol4 sol sol8 sol sol sol |
lab4 lab fa8 fa fa fa |
sol4 sol8 sol <<
  \tag #'basse { do4 r | R1*2\allowPageTurn R2. R1*3 | r2 r4 }
  \tag #'basse-continue {
    do2~ | \allowPageTurn
    do1 |
    fa2 sib,4 sib |
    sol lab sib8 sib, |
    mib4 do sol2 |
    fa mib |
    do2 lab8 fa sol sol, |
    do2.
  }
>> do'4 |
lab fa sib8 sib sib sib |
sol4 sol do'8. do'16 do'8 lab |
sib4 sib, mib4. mib8 |
lab8. lab16 lab8. lab16 fa4 fa |
sol8. sol16 sol8. sol16 mib4 fa |
sol do sol,2 |
do4 do' si sol |
mi mi la la |
fa fa re4. re8 |
sol2 sol |
mi4 mi si,4. do8 |
sol,2 sol,4 sol, |
do do' la fa |
sib sib sol mi |
la la fad re |
sol sol mi do |
fa re sol mi |
la2 la |
fad4 fad sol4. do8 |
re2 re4 re, |
sol, sol la2 |
si do'4 do |
sol2 sol |
mi4 mi si,4. do8 |
sol,2 sol,4 sol, |
do do'4 si sol |
do' do' la fa |
sib sib sol mi |
la la fad re |
sol sol mi do |
re sol, re,2 |
sol,4 sol8 la si4 sol |
do' do' si sol |
do' do' la fa |
sib sib sol mi |
la la fad re |
sol sol mi do |
re sol, re,2 |
sol,4 sol sol sol |
mi mi do do |
fa fa re re |
sol sol8 fa mi re do si, |
la,4 la8 sol fa mi re do |
si,4 do8 si, la, sol, fa, mi, |
fa,1 |
sol,4 sol sol sol |
re re la la |
mi fa sol sol, |
do do do do |
fa fa re re |
sol sol8 fa mi re do si, |
la,4 la8 sol fa mi re do |
si,4 do8 si, la, sol, fa, mi, |
fa,1 |
sol,4 sol sol sol |
re re la la |
mi fa sol sol, |
do4 do' si sol |
mi4 fa sol sol, |
do2.
