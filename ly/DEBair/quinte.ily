\clef "quinte"
R1*32 R2. R1*6 R2.*4 R1 R2. R1*35 R1*8 R1 R1 R2. R1*3 R1 R1*6
R1*19
r4 do' re' re' |
do' do' do' do' |
re' sib sib sib |
la la la la |
si sol sol do' |
la sol re'4. re'8 |
re'1 |
r4 do' re'4. re'8 |
do'4 do' do' do' |
re' sib sib sib |
la la si si |
si sol sol do' |
la sol re'4. re'8 |
re'1 |
do'4 do' do' do' |
do' la re' re' |
re'2. re'4 |
do'8 si la4 la re' |
re' mi' do' do' |
do'2 re'4. re'8 |
re'2 sol4. sol8 |
la4 re' mi' mi' |
mi'8 re' do'4 sol4. sol8 |
sol4 do' do' do' |
do' la re' re' |
re'2. re'4 |
do'8 si la4 la re' |
re' mi' do' do' |
do'2 re'4. re'8 |
re'2 sol4. sol8 |
la4 re' mi' mi' |
mi'8 re' do'4 sol4. sol8 |
sol2. r4 |
mi'8 re' do'4 sol4. sol8 |
sol2.
