  %% Une femme affligée
  <<
  \tag #'(femme basse) {
    \clef "vbas-dessus" <>^\markup\character Une femme affligée
    r2 r4 mib'' |
    do''4. do''8 lab'4. lab'8 |
    lab'4.( sol'8) sol'4. sol'8 |
    do''2 do''4. re''8 |
    mib''2 re''4. do''8 |
    si'2 r4 re'' |
    mib'' do'' la'4. la'8 |
    fad'2 fad'4 re'' |
    la'2 r4 la'8 sib' |
    sol'2 r4 sol'8 fad' |
    sol'1 |
    si'4 si'8 si' si'[ la'] si'4 |
    do''2 r4 do''8 sol' |
    lab'4. lab'8 lab'4 lab'8 sol' |
    fa'2\trill fa' |
    re''4 re''8 re'' re''[ do''] re''4 |
    mib''2. sib'8. do''16 |
    lab'4. sol'8 fa'4\trill sol'8 lab' |
    sol'2\trill sol'4 mib'' |
    mib''4. re''8 re''4. re''8 |
    si'2 si'4 sol' |
    re''2 re''4 mib'' |
    do''2 r4 do''8 si' |
    do''2 r |
    R1*4 |
    r4 do'' do''4. do''8 |
    re''2 re''4 re''8 mib'' |
    do''2 do'' |
    r4 sol'8 la' sib'4 sib'8 do'' |
    la'8. fa'16 sib'8 sib' sib' la' |
    sib'4 sib' r sib' |
    si' si' do'' re'' |
    mib''2. sol'4 |
    lab' sol' fa'4. mib'8 |
    re'2
  }
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    R1*32 R2. R1*4 r2
  }
>>
%% Chœur
<<
  \tag #'femme { r2 R1 R2.*4 \clef "treble" R1 R2. r2 }
  \tag #'(vdessus basse) {
    r4 <>^\markup\character Chœur re''4 |
    mib'' mib'' mib''4. mib''8 |
    do''4 do''8 do'' do'' do'' |
    lab'4 lab'8 lab' lab'8. lab'16 |
    sol'4 sol'8 sol' do''8. do''16 |
    si'4 si' r8 re'' |
    mib''4 mib'' mib'' fa'' |
    re''8 re'' mib'' mib'' re''8. mib''16 |
    mib''2
  }
  \tag #'vhaute-contre {
    r4 sol'4 |
    sol' sol' sol'4. sol'8 |
    lab'4 lab'8 lab' sol' sol' |
    fa'4 fa'8 fa' fa'8. fa'16 |
    mib'4 mib'8 mib' mib'8. mib'16 |
    re'4 re' r8 sol' |
    sol'4 sol' lab' lab' |
    fa'8 fa' sol' sol' fa'8. sol'16 |
    sol'2
  }
  \tag #'vtaille {
    r4 si4 |
    do' do' mib'4. mib'8 |
    mib'4 mib'8 mib' do' do' |
    do'4 do'8 do' do'8. do'16 |
    do'4 do'8 do'8 sol8. sol16 |
    sol4 sol r8 si |
    do'4 do' do' do' |
    sib8 sib sib sib sib8. sib16 |
    sib2
  }
  \tag #'vbasse {
    r4 sol4 |
    do' do' do'4. do'8 |
    lab4 lab8 lab mi8. mi16 |
    fa4 fa8 fa fa8. fa16 |
    do4 do8 do do8. do16 |
    sol,4 sol, r8 sol |
    do'4 do' lab fa |
    sib8 sib sol mib sib sib, |
    mib2
  }
>>
%% Une femme affligée
<<
  \tag #'(femme basse) {
    r4 mib'' |
    mib'' re'' do'' sib' |
    la'2. fa''4 |
    fa'' mib'' re'' do'' |
    si'2 r4 si' |
    si'2 r4 do'' |
    do''2.
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { r2 R1*5 r2 r4 }
>>
%% Chœur
<<
  \tag #'femme {
    si'4 |
    do''2 r |
    R1*27
  }
  \tag #'(vdessus basse) {
    re''4 |
    mib'' mib'' mi''4. mi''8 |
    fa''2 <<
      \tag #'basse { s2 s1 s2 \ffclef "vdessus" }
      \tag #'vdessus { r2 R1 r2 }
    >> r4 re'' |
    mib'' mib'' do''4. do''8 |
    re''2 <<
      \tag #'basse { s2 s1 s2 \ffclef "vdessus" }
      \tag #'vdessus { r2 R1 r2 }
    >> r4 la' |
    sib'4. do''8 la'4. re''8 |
    si'2 r4 si' |
    si' si' do''( si'8) do'' |
    si'4 si' do'' re'' |
    mib'' re'' do'' sib' |
    la' la' si' do'' |
    re''2 r4 re'' |
    mib'' mib'' mi''4. mi''8 |
    fa''4 mib'' re'' do'' |
    si'2 r4 do'' |
    do''2( si'4.)\trill do''8 |
    do''2 r |
    R1*8 |
  }
  \tag #'(vhaute-contre basse) {
    <<
      \tag #'basse { s4 s1 s2 \ffclef "vhaute-contre" }
      \tag #'vhaute-contre {
        sol'4 |
        sol' sol' sol'4. sol'8 |
        lab'2
      }
    >> r4 lab' |
    lab' sol' fa' mib' |
    re'2 <<
      \tag #'basse { s2 s1 s2 \ffclef "vhaute-contre" }
      \tag #'vhaute-contre {
        r4 sol' |
        sol' sol' fa'4. fa'8 |
        fa'2
      }
    >> r4 fa' |
    mib' re' do' sib |
    la2
    \tag #'vhaute-contre {
      r4 fad' |
      sol'4. sol'8 fad'4. sol'8 |
      sol'2 r4 sol' |
      sol' fa' mib'!( re'8) mib' |
      re'4 re' mib' fa' |
      sol' fa' mi'4. mi'8 |
      fa'4 do' re' mib' |
      fa' fa' sol'4. sol'8 |
      sol'2 r4 sol' |
      lab' sol' fa' fa' |
      fa'2 r4 mib' |
      mib'2( re'4.)\trill do'8 |
      do'2 r |
      R1*8 |
    }
  }
  \tag #'vtaille {
    re'4 |
    do' do' do'4. do'8 |
    do'2 r4 fa' |
    fa' mib' re' do' |
    si2 r4 si |
    do' do' la4. la8 |
    sib2 r4 re' |
    do' sib la sol |
    fad2 r4 re' |
    re'4. re'8 re'4. re'8 |
    re'2 r4 re' |
    re' re' sol4. sol8 |
    sol4 re' do' si |
    do'2 r4 do' |
    do' do' fa' mib' |
    re' do' si si |
    do' do' do' do' |
    do' sol lab lab |
    sol2 r4 sol |
    sol2. sol4 |
    sol2 r |
    R1*8 |
  }
  \tag #'vbasse {
    sol4 |
    do' do' do4. do8 |
    fa2 r2 |
    R1 |
    r2 r4 sol |
    mib do fa fa, |
    sib,2 r |
    R1 |
    r2 r4 re |
    sol do re4. re8 |
    sol,2 r4 sol |
    sol2. sol4 |
    sol fa mib re |
    do2. do4 |
    fa mib re do |
    si,2 r4 sol |
    do' sib lab sol |
    fa2 r4 fa |
    sol2 r4 do |
    sol,2 r4 sol, |
    do2 r |
    R1*8 |
  }
>>
%% Une femme affligée
<<
  \tag #'(femme basse) {
    <>^\markup\character Une femme affligée
    r4 r8 mib'' si'4 si' |
    si'8. si'16 si'8. re''16 sol'4 sol' |
    mib''8 mib'' mib''8. mib''16 do''4. do''16 si' |
    do''2
  }
  \tag #'vhaute-contre {
    R1*2 |
    r2 r4 r8 sol' |
    mib'4 do'
  }
  \tag #'(vdessus vtaille vbasse) { R1*3 r2 }
>>
%% Chœur
<<
  \tag #'femme { r2 R1*4 }
  \tag #'(vdessus basse) {
    mi''8 mi'' mi'' mi'' |
    fa''4 fa'' <<
      \tag #'basse { s2 s2 \ffclef "vdessus" }
      \tag #'vdessus { r2 r2 }
    >> si'8 si' si' si' |
    do''4 do'' do''8 do'' do'' do'' |
    do''4 do''8 si' do''4 r |
  }
  \tag #'(vhaute-contre basse) {
    <<
      \tag #'basse { s2 s2 \ffclef "vhaute-contre" }
      \tag #'vhaute-contre {
        sol'8 sol' sol' sol' |
        lab'4 lab'
      }
    >> fa'8. fa'16 re'8. re'16 |
    si4 si
    \tag #'vhaute-contre {
      sol'8 sol' sol' sol' |
      mib'4 mib' fa'8 fa' fa' fa' |
      re'4 re'8 re' mib'4 r |
    }
  }
  \tag #'vtaille {
    do'8 do' do' do' |
    do'4 do' r2 |
    r re'8 re' re' re' |
    do'4 do' lab8 lab lab lab |
    sol4 sol8 sol sol4 r |
  }
  \tag #'vbasse {
    do8 do do do |
    fa4 fa r2 |
    r sol8 sol sol sol |
    lab4 lab fa8 fa fa fa |
    sol4 sol8 sol do4 r |
  }
>>
%% Une femme affligée
<<
  \tag #'(femme basse) {
    r2 sol'4 sol'8 do'' |
    la'4\trill do''8 do''16 fa'' re''4\trill r8 re''16 re'' |
    mib''8. sib'16 do''8 lab'16 sol' fa'4\trill |
    mib'4 r8 mib'' si'4 si' |
    si'8. si'16 si'8. re''16 sol'4 sol' |
    mib''8. mib''16 mib''8. mib''16 do''4. do''16 si'16 |
    do''2 r |
  }
  \tag #'(vdessus vhaute-contre vtaille vbasse) { R1*2 R2. R1*4 }
>>
%% Simphonie
R1*5
%% Chœur
<<
  \tag #'femme { R1*21 }
  \tag #'(vdessus basse) {
    r2 r4 <>^\markup\character Chœur sol'' |
    mi'' mi'' re'' re'' |
    mi'' mi'' do'' do'' |
    do'' do'' re''4. re''8 |
    si'2 si' |
    mi''4 mi'' re''4. do''8 |
    do''2 do''4 si' |
    do'' mi'' fa'' fa'' |
    re'' re'' mi'' mi'' |
    dod'' dod'' re'' re'' |
    si' si' do'' do'' |
    la' re'' si' mi'' |
    do''2 do'' |
    do''4 do'' si'4. do''8 |
    la'2\trill la'4. re''8 |
    si'4\trill sol'' sol'' fa'' |
    fa'' fa'' mi''4. fa''8 |
    re''2\trill re'' |
    mi''4 mi'' re''4. do''8 |
    do''2 do''4 si' |
    do''2 r |
  }
  \tag #'vhaute-contre {
    R1*2 |
    r4 sol' mi' mi' |
    fa' fa' fa'4. fa'8 |
    re'2 re' |
    do'4 sol' sol'4. sol'8 |
    sol'2 sol'4 sol' |
    mi' sol' la' la' |
    fa' fa' sol' sol' |
    mi' mi' re' re' |
    re' re' mi' mi' |
    do' fa' re' sol' |
    mi'2 mi' |
    re'4 re' re'4. mi'8 |
    re'2 re'4. re'8 |
    re'4 sol' mi' la' |
    sol' sol' sol'4. sol'8 |
    sol'2 sol' |
    sol'4 sol' sol'4. sol'8 |
    sol'2 sol'4 sol' |
    mi'2 r |
  }
  \tag #'vtaille {
    R1 |
    r4 do' si si |
    do' do' do' do' |
    la la la4. la8 |
    sol2 sol |
    do'4 do' re' mi' |
    re'2 re'4 mi' |
    do' do' do' do' |
    sib sib sib sib |
    la la la la |
    sol sol sol sol |
    la la si si |
    la2 la |
    la4 la sol4. sol8 |
    sol2 fad4. sol8 |
    sol4 si do' do' |
    re' re' do'4. re'8 |
    si2 si |
    do'4 do' re'4. mi'8 |
    re'2 re'4 mi' |
    do'2 r |
  }
  \tag #'vbasse {
    R1 |
    r2 r4 sol |
    mi mi la la |
    fa fa re4. re8 |
    sol2 sol |
    mi4 mi si,4. do8 |
    sol,2 sol,4 sol, |
    do do' la fa |
    sib sib sol mi |
    la la fad re |
    sol sol mi do |
    fa re sol mi |
    la2 la |
    fad4 fad sol4. do8 |
    re2 re4. re8 |
    sol,4 sol la la |
    si si do'4. do8 |
    sol2 sol |
    mi4 mi si,4. do8 |
    sol,2 sol,4 sol, |
    do2 r |
  }
>>
R1*30
<<
  \tag #'(vdessus basse) {
    r2 r4 <>^\markup\character Chœur sol'' |
    mi'' mi'' re'' re'' |
    R1 r2 r4
  }
  \tag #'vhaute-contre {
    \noHaraKiri
    R1*2 |
    R1 r2 r4
  }
  \tag #'vtaille {
    \noHaraKiri
    R1 |
    r4 do' si si |
    R1 r2 r4
  }
  \tag #'vbasse {
    \noHaraKiri
    R1 |
    r2 r4 sol |
    R1 r2 r4
  }
>>