\piecePartSpecs
#`((dessus #:score "score-dessus")
   (haute-contre)
   (taille #:system-count 8)
   (quinte #:system-count 8)
   (basse #:music , #{
s1*24 \grace s8_\markup\italic\right-align\smaller\right-column {
  La mort barbare détruit aujourd’huy mille appas
}
#})
   (basse-continue #:system-count 28 #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#204 #}))
