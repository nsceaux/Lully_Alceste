\key sol \minor
\time 2/2 \midiTempo#160 s1*32
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1*6
\digitTime\time 3/4 \midiTempo#80 s2.*4
\time 2/2 \midiTempo#160 s1
\digitTime\time 3/4 \midiTempo#80 s2.
\time 2/2 \midiTempo#160 s1*35
\time 4/4 \midiTempo#80 s1*8
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 2/2 \midiTempo#160
\tempo "Ritournelle" s1*7 \bar "||"
\key do \major \time 2/2 \midiTempo#160 \tempo "Viste" s1
\segnoMark \bar "|!:" s1*18
\tempo "Simphonie" s1*31
\alternatives { s1*2 } { s1 s2. \bar "|." }
