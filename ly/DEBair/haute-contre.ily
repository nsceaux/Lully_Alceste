\clef "haute-contre"
R1*32 R2. R1*6 R2.*4 R1 R2. R1*35 R1*8 R1 R1 R2. R1*3 R1 R1*6
R1*19
r4 sol'4 sol'8 la' si'4 |
do'' sol' la' la'8 sol' |
fa'4 sib' sib'8 la' sol' fa' |
mi'4 mi' fad'4. fad'8 |
sol'4 sol' sol'4. sol'8 |
fad'4 sol' fad'4. sol'8 |
sol'1 |
r4 sol' sol'8 la' si'4 |
do'' sol' la' la'8 sol' |
fa'4 sib' sib'8 la' sol' fa' |
mi'4 mi' fad'4. fad'8 |
sol'4 sol' sol'4. sol'8 |
fad'4 sol' fad'4. sol'8 |
sol'1 |
sol'4 sol' sol' sol' |
la' la' la' la' |
sol' si' si' si' |
do'' do'' re'' re'' |
re'' do'' do'' do'' |
la'2 la'4 la' |
sol'2 sol'8 la' si' dod'' |
re''4 la'8 si' do''4 do'' |
do'' do'' si'4. do''8 |
do''4 sol' sol' sol' |
la' la' la' la' |
sol' si' si' si' |
do'' do'' re'' re'' |
re'' do'' do'' do'' |
la'2 la'4 la' |
sol'2 sol'8 la' si' dod'' |
re''4 la'8 si' do''4 do'' |
do''4 do'' si'4. do''8 |
do''2. r4
do''4 do'' si'4. do''8 |
do''2.
