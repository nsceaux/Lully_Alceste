\clef "dessus" R1*23 |
<<
  \tag #'dessus1 {
    <>^"Fluttes" mib''4. re''8 mib''4 fa'' |
    sol'' lab'' sib'' sol'' |
    lab'' sol'' fa''4. fa''8 |
    fa''2 mib''4. mib''8 |
    mib''4. re''8 re''4.\trill do''8 |
    do''2
  }
  \tag #'dessus2 {
    do''4. si'8 do''4 re'' |
    mib''2 re''4 mi'' |
    fa'' mib'' re'' do'' |
    si'2 do''4. do''8 |
    do''4. re''8 si'4. do''8 |
    do''2
  }
>> r2 |
R1*3 R2. R1*4 |
r2 r4 <<
  \tag #'dessus1 {
    <>^"Fluttes" re''4 |
    mib'' mib'' mib'' mib'' |
    do'' do''8 do'' do'' do'' |
    do''4 do''8 do'' do''8. do''16 |
    mib''4 mib''8 mib'' mib''8. mib''16 |
    re''4 re'' r8 re'' |
    mib''4 mib'' mib'' lab'' |
    fa''8[ fa''] sol''[ sol''] fa''[ sib''] |
    sol''2
  }
  \tag #'dessus2 {
    si'4 |
    do'' do'' sol'4. sol'8 |
    lab'4 lab'8 lab' sol' sol' |
    lab'4 lab'8 lab' lab'8. lab'16 |
    sol'4 sol'8 sol' do''8. do''16 |
    si'4 si' r8 si' |
    do''4 do'' do'' fa'' |
    re''8 re'' mib'' mib'' re''8. mib''16 |
    mib''2
  }
>> r2 |
R1*5 |
r2 r4 <<
  \tag #'dessus1 {
    re''4 |
    mib'' mib'' mi''4. mi''8 |
    fa''2
  }
  \tag #'dessus2 {
    si'4 |
    do'' do'' do''4. sol'8 |
    lab'2
  }
>> r |
R1 |
r2 r4 <<
  \tag #'dessus1 {
    re''4 |
    mib'' mib'' do''4. do''8 |
    re''2
  }
  \tag #'dessus2 {
    si'4 |
    do'' do'' la'4. la'8 |
    sib'2
  }
>> r |
R1 |
r2 r4 <<
  \tag #'dessus1 {
    fad''4 |
    sol''4. sol''8 fad''4. sol''8 |
    sol''2 r4 re'' |
    re'' re'' mib'' re''8 mib'' |
    re''4 re'' mib'' fa'' |
    sol'' fa'' mi''4. mi''8 |
    fa''4 do'' re'' mib'' |
    fa'' fa'' sol''4. sol''8 |
    sol''2. sol''4 |
    lab'' sol'' fa''4. fa''8 |
    re''2 r4 do'' |
    sol''2. sol''4 |
    sol'' fa'' mib'' re'' |
    do'' re'' do'' sib' |
    la'2 sib'4. do''8 |
    la'4. la'8 re''4 do'' |
    si' si' do'' re'' |
    mib'' mi'' fa'' sol'' |
    lab'' sol'' fa'' mib'' |
    re''2 mib''4. fa''8 |
    re''4. re''8 re''4. do''8 |
    do''4
  }
  \tag #'dessus2 {
    la'4 |
    sib'4. do''8 la'4. re''8 |
    si'2 r4 si' |
    si' si' do'' si'8 do'' |
    si'4 si' do'' re'' |
    mib'' re'' do'' sib' |
    la' la' si' do'' |
    re''2 r4 re'' |
    mib''4 mib'' mi''4. mi''8 |
    fa''4 mib'' re'' do'' |
    si'2 r4 mib'' |
    re''2 sol''4 fa'' |
    mib'' re'' do'' sib' |
    la' sib' la' sol' |
    fad'2 sol'4. sol'8 |
    sol'4. la'8 fad'2 |
    sol'4 sol' la' si' |
    do'' do'' re'' mi'' |
    fa'' mib'' re'' do'' |
    si'2 do''4. do''8 |
    do''4. re''8 si'4. do''8 |
    do''4
  }
>> r4 r2 |
R1*2 |
r2 <<
  \tag #'dessus1 {
    sol''8 sol'' sol'' sol'' |
    fa''4 fa'' r2 |
    r re''8 re'' sol'' sol'' |
    mib''4 mib'' lab''8 lab'' fa'' fa'' |
    re''4 re''8 sol'' mib''4 r |
  }
  \tag #'dessus2 {
    mi''8 mi'' mi'' mi'' |
    fa''4 fa'' r2 |
    r si'8 si' si' si' |
    do''4 do'' do''8 do'' do'' do'' |
    do''4 do''8 si' do''4 r |
  }
>>
R1*2 \allowPageTurn R2. R1*3 |
r4 r8 <>-\tag #'dessus1 _\markup\whiteout { Flûtes et Violons }
sol'' mib''4 do'' |
<<
  \tag #'dessus1 {
    lab''8 lab'' lab'' lab'' fa''4 fa'' |
    sib''8. sib''16 sib''8 sib'' sol''4. lab''16 sol'' |
    fa''8. fa''16 fa''8. fa''16 sol''8. sol''16 sol''8 sol'' |
    mib''4 mib'' fa''8. fa''16 fa''8. fa''16 |
    re''4 re'' sol''8. sol''16 fa''8. fa''16 |
    fa''4 mib''8. re''32 mib'' re''4.\trill do''8 |
    do''4
  }
  \tag #'dessus2 {
    fa''8 fa'' fa'' fa'' re''4 re'' |
    sol''8. sol''16 sol''8 sol'' mib''4 mib''8. mib''16 |
    mib''4 re''8.\trill do''32 re'' mib''8. mib''16 mib''8. mib''16 |
    do''4 do'' re''8. re''16 re''8. re''16 |
    si'4 si' mib''8. mib''16 re''8. do''16 |
    si'4 do''8. do''16 do''4 si'8. do''16 |
    do''4
  }
>> r4 r2 |
R1*17 |
<<
  \tag #'dessus2 { R1*30 }
  \tag #'dessus1 {
    r2 r4 sol'' |
    mi'' do'' sol'' fa''8 sol'' |
    mi'' do'' re'' mi'' fa''4 fa''8 mi'' |
    re''4 re'' mi'' mi''8 re'' |
    dod''4 la' re'' re''8 re'' |
    re''4 si' do'' do''8 do'' |
    do''4 si'8 do'' la'4 si'8 do'' |
    si' do'' si' la' sol'4 sol'' |
    mi'' do'' sol'' fa''8 sol'' |
    mi'' do'' re'' mi'' fa''4 fa''8 mi'' |
    re''4 re'' mi'' mi''8 re'' |
    dod''4 la' re'' re''8 re'' |
    re''4 si' do'' do''8 do'' |
    do''4 si'8 do'' la'4 si'8 do'' |
    si' do'' si' la' sol' la' si' sol' |
    do''4 sol''8 fa'' mi'' re'' do'' si' |
    la'4 la''8 sol'' fa'' mi'' re'' do'' |
    si'4 sol'' sol'' sol'' |
    sol'' fa'' fa'' fa'' |
    fa'' mi'' mi'' mi'' |
    mi'' fa''8 mi'' re'' mi'' re'' do'' |
    si' la' sol' la' si' do'' re'' mi'' |
    fa'' mi'' fa'' sol'' mi'' re'' mi'' fa'' |
    sol''4 la'' re''4.\trill do''8 |
    do''4 sol''8 fa'' mi'' re'' do'' si' |
    la'4 la''8 sol'' fa'' mi'' re'' do'' |
    si'4 sol'' sol'' sol'' |
    sol'' fa'' fa'' fa'' |
    fa'' mi'' mi'' mi'' |
    mi'' fa''8 mi'' re'' mi'' re'' do'' |
    si' la' sol' la' si' do'' re'' mi'' |
    fa'' mi'' fa'' sol'' mi'' re'' mi'' fa'' |
    sol''4 la'' re''4.\trill do''8 |
    do''2. r4 |
    sol''4 la'' re''4.\trill do''8 |
    do''2.
  }
>>
