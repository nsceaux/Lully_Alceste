\tag #'(femme basse) {
  La mort, la mort bar -- ba -- re,
  dé -- truit au -- jour -- d’huy mille ap -- pas.
  La mort, la mort bar -- ba -- re,
  dé -- truit au -- jour -- d’huy mille ap -- pas.
  Quel -- le vic -- time, he -- las !
  fut ja -- mais si belle, & si ra -- re ?
  Quel -- le vic -- time, he -- las !
  fut ja -- mais si belle, & si ra -- re ?
  La mort, la mort bar -- ba -- re
  dé -- truit au -- jour -- d’huy mille ap -- pas.
}
%Al -- ces -- te si jeune, & si bel -- le,
%Court se pre -- ci -- pi -- ter dans la Nuit e -- ter -- nel -- le,
%Pour sau -- ver ce qu’elle aime elle a per -- du le jour.

%O trop par -- fait Mo -- del -- le
%D’une Es -- pou -- se fi -- del -- le !
%O trop par -- fait Mo -- del -- le
%D’un ve -- ri -- table A -- mour !
\tag #'(femme basse) {
  Que nos -- tre zé -- le se par -- ta -- ge ;
  Que les uns par leurs chants ce -- le -- brent son cou -- ra -- ge,
  Que d’au -- tres par leurs cris dé -- plo -- rent ses mal- heurs.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Ren -- dons ren -- dons hom -- ma -- ge
  a son i -- ma -- ge
  a son i -- ma -- ge
  a son i -- ma -- ge ;
  jet -- tons jet -- tons des fleurs,
  jet -- tons jet -- tons des fleurs.
}
\tag #'(femme basse) {
  Ver -- sons ver -- sons des pleurs.
  Ver -- sons ver -- sons des pleurs.
  Ver -- sons ver -- sons \tag #'femme { des pleurs. }
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Jet -- tons jet -- tons des fleurs,
  \tag #'(vhaute-contre vtaille basse) {
    ver -- sons ver -- sons des pleurs.
  }
  Jet -- tons jet -- tons des fleurs.
  \tag #'(vhaute-contre vtaille basse) {
    Ver -- sons ver -- sons des pleurs.
  }
  Jet -- tons jet -- tons des fleurs,
  \tag #'(vdessus basse) {
    ver -- sons ver -- sons des pleurs,
    jet -- tons des fleurs,
    ver -- sons des pleurs,
    jet -- tons des fleurs,
    jet -- tons jet -- tons des fleurs,
    ver -- sons des pleurs,
    ver -- sons des pleurs.
  }
  \tag #'vhaute-contre {
    ver -- sons ver -- sons des pleurs,
    jet -- tons des fleurs,
    ver -- sons des pleurs,
    jet -- tons des fleurs,
    jet -- tons des fleurs,
    ver -- sons ver -- sons des pleurs,
    ver -- sons des pleurs.
  }
  \tag #'vtaille {
    ver -- sons ver -- sons des pleurs,
    jet -- tons des fleurs,
    ver -- sons ver -- sons des pleurs,
    jet -- tons des fleurs,
    ver -- sons des pleurs,
    jet -- tons des fleurs,
    ver -- sons des pleurs.
  }
  \tag #'vbasse {
    ver -- sons ver -- sons ver -- sons des pleurs,
    ver -- sons ver -- sons des pleurs,
    jet -- tons jet -- tons des fleurs,
    ver -- sons ver -- sons des pleurs.
  }
}
\tag #'(femme basse) {
  Al -- ces -- te, la char -- mante Al -- ces -- te,
  la fi -- delle Al -- ces -- te n’est plus.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  \tag #'vhaute-contre { Al -- ces -- te, }
  la char -- mante Al -- ces -- te,
  \tag #'(vhaute-contre basse) {
    la fi -- delle Al -- ces -- te
  }
  la char -- mante Al -- ces -- te,
  la fi -- delle Al -- ces -- te n’est plus.
}
\tag #'(femme basse) {
  Tant de beau -- tez, tant de ver -- tus,
  me -- ri -- toient un sort moins fu -- nes -- te.
  Al -- ces -- te, la char -- mante Al -- ces -- te,
  la fi -- delle Al -- ces -- te n’est plus.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Rom -- pons, bri -- sons
  \tag #'(vdessus basse) {
    bri -- sons rom -- pons rom -- pons
  }
  \tag #'vtaille {
    rom -- pons bri -- sons
  }
  \tag #'vbasse {
    bri -- sons
  }
  le tris -- te res -- te
  de ces or -- ne -- mens su -- per -- flus.
  Rom -- pons bri -- sons bri -- sons rom -- pons
  rom -- pons bri -- sons bri -- sons rom -- pons le tris -- te res -- te
  de ces or -- ne -- mens su -- per -- flus.
  Rom -- pons, bri -- sons le tris -- te res -- te
  de ces or -- ne -- mens su -- per -- flus.
}
\tag #'(vdessus basse) { Rom -- pons, bri -- sons bri - }
\tag #'vtaille { Rom -- pons, bri - }
\tag #'vbasse Rom -
