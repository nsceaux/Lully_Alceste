\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte #:system-count 6)
   (basse #:system-count 5)
   (basse-continue #:system-count 5)
   (silence #:on-the-fly-markup , #{ \markup\tacet#36 #}))
