\clef "dessus" fa'8 sol'4 |
la'4.\trill sol'8 fa'4 do''4. re''8 mi''4 |
fa''2 fa''4 fa''4. mi''8 re''4 |
sol''2 sol''4 fa''4.\trill sol''8 re''4 |
mib''2 mib''4 mib''4. fa''8 sol''4 |
\footnoteHere #'(0 . 0) \markup\wordwrap {
  Édition Baussen :
  \raise#0.7 \score {
    { \tinyQuote \clef "french" \time 6/4 \key fa \major
      do''4. sib'8 do''4 re''4. do''8 re''4 | }
    \layout { \quoteLayout }
  }
}
do''4. si'8 do''4 re''4. mib''8 do''4 |
si'4. la'8 sol'4 sol''2 sol''4 |
sol''4. fa''8 mib''4 re''4. mi''8 fa''4 |
mi''4.\trill re''8 do''4 do''4. sol'8 la'4 |
sib'4. do''8 sib'4 sib'4. la'8 sol'4 |
la'2 la'4 la'4. fa''8 mi''4 |
re''4. dod''8 re''4 mi''4.
\footnoteHere #'(0 . 0) \markup\wordwrap {
  Édition Baussen :
  \raise#0.7 \score {
    { \tinyQuote \clef "french" \time 6/4 \key fa \major
      re''4. dod''8 re''4 mi''4. re''8 mi''4 | }
    \layout { \quoteLayout }
  }
}
fa''8 re''4 |
dod''4. si'8 la'4 la''2 la''4 |
la''4. sol''8 fa''4 mi''4. fad''8 sol''4 |
fad''4. mi''8 re''4 re''4. mi''8 fad''4 |
sol''2 sol''4 sol''4. fa''8 sol''4 |
mi''4.\trill re''8 do''4 do''4. re''8 mi''4 |
fa''2 fa''4 fa''4. sol''8 la''4 |
re''4. mi''8 fa''4 sol''4. la''8 sol''4 |
sol'' mi'' la'' fa'' sol'' mi'' |
fa''2 fa''4 fa''4. la'8 sib'4 |
do''2 do''4 do''4. sol'8 la'4 |
sib'2 sib'4 sib'4. fa'8 sol'4 |
la'4. sib'8 la'4 sol'4. fa'8 mi'4 |
fa'2 fa'4 fa'4. la'8 sib'4 |
fa'2 fa'4 fa'4.
