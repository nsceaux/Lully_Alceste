\clef "haute-contre" fa'8 do'4 |
fa'2 fa'4 la'2 la'4 |
la'4. sol'8 fa'4 re''2 re''4 |
re''2. re''2 re''4 |
do''2 sol'4 sol'2 sol'4 |
sol'2. fa'4. mib'8 fa'4 |
sol'2 sol'4 sol'2 do''4 |
la'2 la'4 re'' sol'2 |
sol'4. fa'8 mi'4 mi'4. mi'8 fad'4 |
sol'2 re'4 re'4. re'8 mi'4 |
fa'2 fa'4 fa'4. la'8 sol'4 |
fa'2 fa'4 fa'4. la'8 la'4 |
la'2 la'4 la' re''2 |
sib' sib'4 la'2 la'4 |
la'4. sol'8 fad'4 la'2 re''4 |
re''2 re''4 re''4. do''8 si'4 |
do''2 do''4 sol' sol'4. sol'8 |
la'4. sib'8 do''4 do''4. sib'8 la'4 |
sib'2 re''4 re''2 re''4 |
sol'4 sol' do'' la' sib' sol' |
la'2 la'4 fa'4. do'8 re'4 |
mib'2 mib'4 mib'4. sib8 do'4 |
re'2 re'4 re'2 do'4 |
do'2 do'4 do'2 do'4 |
do'2 do'4 fa'4. do'8 re'4 |
do'2 do'4 fa'4.
