\clef "basse" fa8 mi4 |
fa2 fa4 fa4. fa8 mi4 |
re2 re4 re2 re4 |
si,2.~ si,2 si,4 |
do2 do4 do2 sib,4 |
lab,2 lab,4 lab,2 lab,4 |
sol,2 sol4 mib2. |
fa sol2 sol,4 |
do2 do4 do4. do8 do4 |
sol,2 sol,4 sol,2 sol,4 |
re2 re4 re4. re8 do4 |
sib,2 sib,4 sib,2 sib,4 |
la,2 la4 fa2. |
sol la2 la,4 |
re2 re4 re4. re'8 do'4 |
si2 si4 si4. la8 sol4 |
do'2 do'4 do'4. do'8 sib4 |
la2 la4 la4. sol8 fa4 |
sib2 sib4 si2 si4 |
do'2 la4 re' sib do' |
fa2 fa4 fa4. fa8 fa4 |
mib2 mib4 mib2 mib4 |
re2 re4 re4. re8 mi4 |
fa2 sib,4 do2 do4 |
fa,2 fa,4 fa,4. fa8 fa4 |
fa,2 fa,4 fa,4.
