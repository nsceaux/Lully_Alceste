\clef "quinte" la8 sol4 |
fa4. sol8 la4 la4. la8 sol4 |
fa4. sol8 la4 la2 la4 |
sol2 sol4 sol2 re'4 |
sol2 do'4 do'2 sol4 |
lab2 lab4 lab2 fa'4 |
re'2 re'4 do'2 do'4 |
do'2 do'4 sol4. la8 si4 |
do'2 sol4 sol4. do'8 do'4 |
sib4. la8 sol4 sol2 sol4 |
fa2 fa4 la2 la4 |
sib2 sib4 sib2 sol'4 |
mi'2 mi'4 re'2 re'4 |
re'2 re'4 la4. re'8 dod'4 |
re'2 re'4 re'2 re'4 |
re'2.~ re'2 re'4 |
do'2 do'4 do'2. |
do' do'2 do'4 |
re'4. do'8 sib4 re'2 re'4 |
do'2 do'4 re' re' do' |
do'2 do'4 do'4. do'8 sib4 |
sol2 sol4 do'4. do'8 do'4 |
sib2 sib4 sib2 sol4 |
fa4. sol8 fa4 mi4. fa8 sol4 |
fa2 fa4 fa4. do'8 sib4 |
fa2 fa4 fa4.
