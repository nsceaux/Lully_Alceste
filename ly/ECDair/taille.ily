\clef "taille" do'8 do'4 |
do'4. sib8 la4 la4. dod'8 dod'4 |
re' la'2 la'4. sol'8 fa'4 |
sol'2 re'4 sol'2 sol'4 |
sol'2 sol'4 mib'4. mib'8 re'4 |
mib'2. re'2 re'4 |
re'4. do'8 si4 do'4. re'8 mib'4 |
mib'4. re'8 do'4 si4. do'8 re'4 |
do'2 do'4 do'4. do'8 do'4 |
re'2 re'4 re'4. do'8 sib4 |
la2 fa'4 re'4. re'8 mi'4 |
fa'2 fa'4 mi'2 mi'4 |
mi'4. re'8 dod'4 re'4. mi'8 fa'4 |
fa'4. mi'8 re'4 dod'4. re'8 mi'4 |
re'2 la'4 fad'4. sol'8 la'4 |
sol'2 re'4 sol'2 sol'4 |
sol'4. fa'8 mi'4 mi'4. fa'8 sol'4 |
fa'2 fa'4 fa'2 fa'4 |
fa'2 fa'4 re'2 sol'4 |
mi'4 mi' fa' fa' re' sol' |
fa'2 do'4 la4. fa'8 fa'4 |
do'2 sol'4 sol'2 do'4 |
fa'2 fa'4 sib2 sib4 |
la fa'2 do' do'4 |
la2 la4 la4. fa'8 fa'4 |
la2 la4 la4.
