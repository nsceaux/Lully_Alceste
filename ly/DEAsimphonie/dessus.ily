\clef "dessus" r2 <>^"Violons" do''4. do''8 |
do''4. sib'8 sib'4. sib'8 |
sib'4. do''16 sol' lab'4. sib'8 |
sol'4. sol'8 sol'4. la'8 |
sib'2 si'4. si'8 |
do''2. re''4 |
mib''2 si'4. do''8 |
re''4. la'8 sib'4. do''8 |
la'4. la'8 re''4. do''8 |
si'4 sol' mib''4. mib''8 |
mib''4. fa''8 re''4.\trill re''8 |
sol''2 fa''4 sol''8 re'' |
mib''4 do'' lab''4. lab''8 |
lab''2 sol''4. sol''8 |
sol''2 fa''4. la'8 |
si'2 do''4. do''8 |
do''4. re''8 si'4. do''8 |
do''1 |
