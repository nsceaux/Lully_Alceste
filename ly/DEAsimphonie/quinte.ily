\clef "quinte" r2 do'4. do'8 |
re'2 sol |
do' fa |
sol do'4 do' |
sib2 sol4. sol8 |
do'2 do' |
do' re'4. re'8 |
re'2 re'4 do' |
do'4. sib8 la4.\trill sol8 |
sol4 re' do'4. do'8 |
do'2 re'4. re'8 |
re'4 sol'2 sol'4 |
do' mib' fa' do' |
sib2 sib4. sib8 |
do'2 fa'4. fa'8 |
fa'8 mib' re'4 do'2 |
do' sol4. sol8 |
sol1 |
