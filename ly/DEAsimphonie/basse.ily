\clef "basse" do2. do4 |
sol2 sol4. fa8 |
mi2 fa4 fa, |
do2. do4 |
sol,2 sol4. sol8 |
lab1 |
sol2. sol4 |
fad2 sol4 do |
re2. re4 |
sol2. sol4 |
la2 sib4. sib8 |
si1 |
do'2 fa |
sib mib |
lab2. fa4 |
sol2 do |
fa, sol, |
<<
  \tag #'basse do1
  \tag #'basse-continue { \custosNote do4 \stopStaff }
>>
