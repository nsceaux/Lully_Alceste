\clef "haute-contre" r2 sol'4. sol'8 |
sol'2 sol'4. sol'8 |
sol'2 fa'4. fa'8 |
fa'2 mib'4. fa'8 |
re'4 sol' sol'4. sol'8 |
sol'2 fa'4.\trill mib'16 fa' |
sol'2 sol'4. do''8 |
la'4 la' sol'4. sol'8 |
sol'2 fad'4. mi'16 fad' |
sol'2 do''4. do''8 |
do''2 sib'4. re''8 |
re''2. re''4 |
do''2 do''4. do''8 |
re''2 mib''4. re''8 |
do''4 mib' fa'4. mib'8 |
re'4 sol' sol'4. sol'8 |
lab'2 sol'4. fa'8 |
mi'1 |
