\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille #:system-count 3)
   (quinte #:system-count 3)
   (basse)
   (basse-continue)
   (silence #:on-the-fly-markup , #{ \markup\tacet#18 #}))
