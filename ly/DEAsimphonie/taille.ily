\clef "taille" r2 mib'4. mib'8 |
re'2 re'4. do'8 |
do'2 do'4. do'8 |
do'2. do'4 |
re'2 re'4. re'8 |
do'2 fa' |
mib' re' |
re' re'4 mib' |
re'2. re'4 |
re' si do' sol' |
fa'2 fa'4. fa'8 |
fa'2 re'4. sol'8 |
sol'4 sol' fa'4. fa'8 |
fa'2 sol'4. fa'8 |
mib'4 do' re'4. re'8 |
re'2 mib'4. mib'8 |
mib'4. re'8 re'4. do'8 |
do'1 |
