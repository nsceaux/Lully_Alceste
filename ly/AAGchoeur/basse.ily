\clef "basse" do'4. do'8 do'4 |
si si si |
do'2 do4 |
sol4 sol4. sol8 |
re8 do re mi fa sol |
la2 fa4 |
sol la2 |
re2 re4 |
sol4. sol8 fa4 |
mi4. re8 do4 |
fa2 re4 |
sol2 mi4 |
la2 fa4 |
re sol sol, |
do2. |
si, |
la, |
sol, |
do |
si, |
la, |
fa,4 sol,2 |
do2 do4 |
fa4 fa fa |
re2 re4 |
mi mi4. mi8 |
la2 do4 |
re mi2 |
la,2 la,4 |
re2 do4 |
si,4. la,8 sol,4 |
do2 la,4 |
re re2 |
sol si,4 |
do re2 |
sol,2.~ |
sol,~ |
sol,~ |
sol,~ |
sol,4. sol8 sol4 |
do' do' do |
fa2 re4 |
sol4 sol4. sol8 |
do8 si, do re mi fa |
sol fa sol la si sol |
do'2 do'4 |
fa do2 |
sol2 mi4 |
la2 fa4 |
re sol2 |
do do'4 |
fa sol2 |
do2 sib,8 la, | \once\set Staff.whichBar = "|"
sol,2. |
