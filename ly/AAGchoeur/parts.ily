\piecePartSpecs
#`((dessus #:music , #{ s2.*22 s4. s^"[Tous]" s2.*16 s4. s^"[Tous]" #})
   (haute-contre #:music , #{ s2.*8\break s2.*8\break s2.*7\break s2.*9\break s2.*11\break #})
   (taille)
   (quinte)
   (basse)
   (basse-continue #:music , #{ s2.*9\break s2.*10\break s2.*9\break s2.*9\break s2.*8\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#54 #}))
