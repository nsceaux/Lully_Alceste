\clef "taille" r4 r8 mi' mi'4 |
re' re' sol' |
mi'2 sol'4 |
sol' sol'4. sol'8 |
la'4 la'4. la'8 |
la'2 la'4 |
sib' la'2 |
fad'4. fad'8 fad'4 |
sol'4. re'8 re'4 |
mi'4. fa'8 sol'4 |
la'2 la'4 |
sol' sol' sol' |
mi'2 fa'4 |
fa' re'4. do'16 re' |
mi'8 fa' sol' fa' mi'4 |
re' sol'2 |
sol'4 fad'2 |
sol' sol'4 |
mi'8 fa' sol' fa' mi'4 |
re' sol'2 |
mi' la'4 |
la' sol'4. fa'8 |
mi'4. sol'8 sol'4 |
fa'4 fa' fa' |
fa'2 fa'4 |
mi' mi'4. mi'8 |
mi'2 mi'4 |
fa' mi'2 |
mi'4. mi'8 mi'4 |
re'4. la'8 la'4 |
sol'4. la'8 re'4 |
mi'2 mi'4 |
re'4 re'4. re'8 |
re'2 re'4 |
mi' re'2 |
re'2 r4 |
R2.*3 |
r4 r8 sol' sol'4 |
sol' sol' sol' |
la'2 la'4 |
sol'4 sol'4. sol'8 |
sol'2 sol'4 |
sol' sol'2 |
sol' sol'4 |
la' sol'2 |
sol' sol'4 |
mi'2 fa'4 |
fa' re'2 |
mi' sol'4 |
la' sol'2 |
mi'2. |
