Qu’il est doux d’ac -- cor -- der en -- sem -- ble
\tag #'(vdessus vhaute-contre vtaille) { la Gloi -- re la Gloire }
\tag #'vbasse { la Gloire __ }
& les plai -- sirs.
\tag #'(vdessus vhaute-contre vtaille) { Qu’il est doux }
Qu’il est doux d’ac -- cor -- der en -- sem -- ble
la Gloire & les plai -- sirs.
Qu’il est doux d’ac -- cor -- der en -- sem -- ble
la Gloire & les plai -- sirs.
\tag #'(vdessus vhaute-contre vtaille) { Qu’il est doux }
Qu’il est doux d’ac -- cor -- der en -- sem -- ble
la Gloire & les plai -- sirs.
Qu’il est doux d’ac -- cor -- der en -- sem -- ble
\tag #'(vdessus vhaute-contre) {
  la Gloire & les plai -- sirs
}
\tag #'vtaille {
  la Gloi -- re la Gloire __
}
\tag #'vbasse {
  la Gloire __
}
& les plai -- sirs
la Gloire & les plai -- sirs
& les plai -- sirs.
