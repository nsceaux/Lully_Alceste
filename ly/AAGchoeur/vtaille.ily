\clef "vtaille" r4 r8 mi' mi'4 |
re' re' re' |
do'2 do'4 |
si si4. dod'8 |
re'4 re'4. re'8 |
dod'2 re'4 |
re' dod'2 |
re'4. re'8 re'4 |
re'4. sol8 sol4 |
sol4. sol8 do'4 |
do'2 re'4 |
si4 si si |
la2 la4 |
la sol2 |
sol r4 |
R2.*7 |
r4 r8 do' do'4 |
la la la |
si2 si4 |
sold4 sold4. sold8 |
la2 la4 |
la sold2 |
la4. la8 la4 |
la4. re'8 re'4 |
re'4. do'8 si4 |
do'2 mi'4 |
re' re'4. re'8 |
re'2 re'4 |
mi' re'2 |
si2 r4 |
R2.*3 |
r4 r8 si si4 |
do' do' do' |
do'2 re'4 |
si4 si4. si8 |
do'4 do'4. do'8 |
si[\melisma la si do' re' si]( |
mi'2)\melismaEnd do'4 |
do' do'2 |
si si4 |
la2 la4 |
la sol2 |
sol do'4 |
do' si2 |
do'2. |
