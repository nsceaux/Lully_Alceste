\clef "vdessus" r4 r8 do'' do''4 |
re'' re'' re'' |
mi''2 mi''4 |
re''\trill re''4. mi''8 |
fa''4 fa''4. fa''8 |
mi''2 fa''4 |
mi'' mi''2\trill |
re''4. re''8 do''4 |
si'4.\trill si'8 si'4 |
do''4. re''8 mi''4 |
fa''2 fa''4 |
re''\trill re'' mi'' |
do''2 do''4 |
re'' si'2\trill |
do'' r4 |
R2.*7 |
r4 r8 mi'' mi''4 |
do''4 do'' do'' |
re''2 re''4 |
si'\trill si'4. si'8 |
do''2 do''4 |
si' si'2\trill |
la'4. dod''8 dod''4 |
re''4. la'8 la'4 |
si'4.\trill do''8 re''4 |
mi''2 do''4 |
la'4\trill la'4. re''8 |
si'2\trill si'4 |
la' la'2\trill |
sol'2 r4 |
R2.*3 |
r4 r8 re'' re''4 |
mi'' mi'' mi'' |
fa''2 fa''4 |
re''4\trill re''4. re''8 |
mi''2 mi''4 |
re'' re''2\trill |
do'' mi''4 |
fa'' mi''2\trill |
re'' mi''4 |
do''2 do''4 |
re'' si'2\trill |
do''2 mi''4 |
fa'' re''2\trill |
do''2. |
