\clef "haute-contre" r4 r8 sol' sol'4 |
sol' sol' re'' |
do''2 do''4 |
si' si'4. dod''8 |
re''4 re''4. re''8 |
dod''2 re''4 |
re'' dod''2 |
re''4. la'8 la'4 |
sol'4. sol'8 sol'4 |
sol'4. sol'8 do''4 |
do''2 re''4 |
si' si' si' |
la'2 la'4 |
sol' sol'2 |
sol'4 sol'4. la'8 |
si' la' si' do'' si' sol' |
la' sol' la' si' do'' la' |
si' la' si' do'' si'4 |
do'' sol'4. la'8 |
si' la' si' do'' si' sol' |
la' sol' la' si' do'' la' |
re'' do'' si'4.\trill do''8 |
do''4. do''8 do''4 |
la'4 la' la' |
si'2 si'4 |
sold'4 sold'4. sold'8 |
la'2 la'4 |
la' sold'2 |
la'4. dod''8 dod''4 |
re''4. re''8 re''4 |
re''4. do''8 si'4 |
do''2 mi'4 |
fad'4 fad'4. fad'8 |
sol'2 sol'4 |
sol' fad'2 |
sol'2 r4 |
R2.*3 |
r4 r8 si' si'4 |
do'' do'' do'' |
do''2 re''4 |
si'4 si'4. si'8 |
do''2 do''4 |
si' si'2 |
do'' do''4 |
do'' do''2 |
si' si'4 |
la'2 la'4 |
la' sol'2 |
sol' do''4 |
do'' si'2\trill |
do''2. |
