\clef "quinte" r4 r8 do' do'4 |
sol' re' re' |
do'2 do'4 |
re' sol'4. sol'8 |
fa'4 re'4. re'8 |
mi'4 mi' re' |
mi' mi' la |
la4. re'8 re'4 |
re' re' re' |
do'4. si8 do'4 |
la2 la4 |
si sol sol |
la2 la4 |
re' re'2 |
do' do'4 |
sol'2 si4 |
do'2 do'4 |
do' sol4. sol8 |
sol4 do' do' |
sol'2 si4 |
do'2 do'4 |
do' sol4. sol8 |
sol4. do'8 do'4 |
do'4 do' do' |
si2 si4 |
si si4. si8 |
la2 mi'4 |
re' si2 |
dod'4. la8 la4 |
la4. la8 la4 |
si4. do'8 re'4 |
do'2 do'4 |
re' la4. la8 |
sol2 re'4 |
do' la2 |
si2 r4 |
R2.*3 |
r4 r8 re' re'4 |
do' do' do' |
la2 re'4 |
re'4 re'4. re'8 |
do'2 do'4 |
re' re'2\trill |
mi' do'4 |
do' do'2 |
re' sol4 |
la2 la4 |
re' re'2 |
do' do'4 |
do' sol2 |
sol2. |
