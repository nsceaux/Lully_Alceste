\clef "vbasse" r4 r8 do' do'4 |
si si si |
do'2 do4 |
sol4 sol4. sol8 |
re8[\melisma do re mi fa sol] |
la2\melismaEnd fa4 |
sol la2 |
re r4 |
r4 r8 sol fa4 |
mi4. re8 do4 |
fa2 re4 |
sol sol mi |
la2 fa4 |
re sol2 |
do2 r4 |
R2.*7 |
r4 r8 do do4 |
fa fa fa |
re2 re4 |
mi mi4. mi8 |
la2 do4 |
re mi2 |
la, r4 |
r4 r8 re do4 |
si,4. la,8 sol,4 |
do2 la,4 |
re re4. re8 |
sol2 si,4 |
do re2 |
sol,2 r4 |
R2.*3 |
r4 r8 sol sol4 |
do' do' do |
fa2 re4 |
sol4 sol4. sol8 |
do8[\melisma si, do re mi fa] |
sol[ fa sol la si sol]( |
do'2)\melismaEnd do'4 |
fa do2 |
sol mi4 |
la2 fa4 |
re sol2 |
do do'4 |
fa sol2 |
do2. |
