\clef "taille" la'8 la'4 |
la'4. |
la'4 la'16 la' |
sol' la' sib' la' sol' fa' |
sol'8. la'16 sib'8 |
sol'4 sol'8 |
sol'4 sol'8 |
do'8. do'16 fa' mi' |
fa'8 fa'16 sol' la'8 |
la'16 sol' la' sib' la' sol' |
fa' mib' re' mib' re' do' |
sib8 sol' sol' |
sol'8. sol'16 fa'8 |
fa' do'4 |
do'4. |
mi'16 fa' mi' re' mi' fad' |
sol'4 sol'8 |
fa'16 sol' la' sol' fa' sol' |
mi'8 la'4 |
la'16 sol' la' sib' la' sol' |
fa' mi' fa' sol' fa' sol' |
mi'4 mi'8 mi'4 fa'16 sol' |
la'4 la'8 |
fa'16 mi' fa' sol' fa' sol' |
mi'8~ mi'8. mi'16 |
fad'?8 re'4 |
re' sol'16 la' |
la'8 la'8. sol'16 |
fad'4. |
