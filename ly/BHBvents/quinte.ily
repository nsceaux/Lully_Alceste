\clef "quinte" re'8 mi'4 |
re'4. re'8 re'16 mi' re' do' |
re'4 re'16 re' |
re'4 re'8 |
do'4 do'16 re' |
mi' fa' mi' re' do' sib |
do'4 do'8 |
do'16 sib la sol la sib |
do'4 do'8 |
sib16 do' re'8 re' |
re'4 re'8 |
do'4 do'8 |
do'8. do'16 do'8 |
do'16 sib la sib do' re' |
do'8 do'4 |
re' re'8 |
re'8. mi'16 fa'8 |
sol' sol'8. sol'16 |
fa'4 fa'8 |
fa'4 mi'8 |
mi'4 la8 |
la4 la8 |
la la4 |
la8 re' re' |
mi' la4 |
la la8 |
sol4 re'8 |
mi' la8. la16 |
la4. |
