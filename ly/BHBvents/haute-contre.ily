\clef "haute-contre" re''8 la'4 |
re'16 mi' fa' sol' la' sol' |
fad'4. |
sol'8. fad'16 sol' la' |
sib' do'' sib' la' sol' fa' |
sol' fa' mi' fa' sol' fa' |
mi'8 mi'16 fa' sol'8 |
fa'16 sol' la' sol' la' sib' |
la' sol' la' sib' do'' sib' |
la'8 la'8. la'16 |
sib'8. fa'16 sol' la' |
sib' do'' re'' do'' sib' la' |
sol' fa' sol'8 la' |
la' sol'8. do''16 |
la'4. |
sol'4 do''8 |
sib'16 la' sol'8 mib'' |
re'' la' re'' |
mi''4 mi''8 |
re'' re'' do'' |
sib'4 sib'8 |
la'16 sol' la' si' dod'' re'' |
mi'' re'' mi'' fa'' re'' dod'' |
re'' mi'' dod''8. si'32 dod'' |
re''8 la' la' |
la' la'16 sib' la' sol' |
fad'8. fad'16 sol' la' |
sib' do'' re''8. re''16 |
dod'' re'' dod''8. re''16 |
re''4. |
