\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue #:music , #{ s4.*7\break s4.*7\break s4.*9\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#30 #}))
