\clef "basse" re8 dod4 |
re4. |
re'16 mib' re' do' sib la |
sib do' re' do' sib la |
sol4 sol8 |
do'4. |
do'16 re' do' sib la sol |
la sib do' sib la sol |
fa4 fa16 sol |
la8 fa fa |
sib16 la sib do' sib la |
sol4 sol8 |
do'16 re' do' sib la sol |
la fa do' sib do' do |
fa mi fa sol la sib |
do'4. |
sol16 fa sol la sib do' |
re'4 re'8~ |
re' dod'4 |
re' la8 |
sib4 sol8 |
la4 la8 |
dod4 re8~ |
re la,4 |
re, re8 |
la16 sib la sol fa mi |
re mib re do sib, la, |
sol, la, sib, la, sol, fa, |
mi, re, la,4 |
re,4. |
