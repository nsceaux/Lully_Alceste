\clef "dessus" la''16 sib'' la'' sol'' fa'' mi'' |
fa'' sol'' la'' sol'' fa'' mi'' |
re''4.~ |
re'' |
sol''16 la'' sol'' fa'' mi'' re'' |
mi'' fa'' sol'' fa'' mi'' re'' |
do''8 do''8. do''16 |
fa''8. mi''16 fa'' sol'' |
la'' sib'' do''' sib'' la'' sol'' |
fa'' mi'' fa'' sol'' fa'' mi'' |
re''4. |
sol''16 la'' sib'' la'' sol'' fa'' |
mi'' re'' mi'' do'' fa'' mi'' |
fa'' sol'' mi'' fa'' sol'' mi'' |
fa''4. |
mi''16 re'' mi'' fa'' sol'' la'' |
sib''4. |
la''16 sol'' fa'' sol'' la'' sib'' |
sol'' fa'' mi'' fa'' sol'' mi'' |
fa'' mi'' fa'' sol'' fa'' mi'' |
re'' dod'' re'' mi'' re'' mi'' |
dod'' si' dod'' re'' mi'' fa'' |
sol'' fa'' sol'' la'' fa'' mi'' |
fa'' sol'' mi'' fa'' mi'' fa'' |
re'' dod'' re'' mi'' re'' mi'' |
dod''8~ dod''8. dod''16 |
re''8. re''16 mi'' fad'' |
sol''8. la''16 sib'' la'' |
sol'' fa'' mi''8.\trill re''16 |
re''4. |
