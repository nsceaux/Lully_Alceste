<<
  %% Céphise
  \tag #'(cephise basse) {
    \clef "vdessus" <>^\markup\character Céphise la'4 si' |
    do''4. re''8 si'4. la'8 |
    sold'4 sold' mi''4. mi''8 |
    re''4 do'' si'( do''8) re'' |
    do''4 la' <<
      \tag #'basse { s2 s1*3 s2 }
      \tag #'cephise { r2 | R1*3 | r2 }
    >> <>^\markup\character Céphise mi''4. fa''8 |
    sol''4 sol'' fa''4. mi''8 |
    re''2 fa''4. fa''8 |
    fa''4 mi'' re''( mi''8) fa'' |
    mi''4 do'' mi''4. mi''8 |
    mi''4. fa''8 re''4. re''8 |
    re''2 do''4.\trill do''8 do''4. si'8 si'4. la'8 |
    la'2 mi''4. mi''8 |
    mi''4. fa''8 re''4. re''8 |
    re''2 do''4.\trill do''8 |
    do''4. si'8 si'4.\trill la'8 |
    la'2
    \tag #'cephise { r2 | R1*11 | r2 }
  }
  %% Nymphe
  \tag #'nymphe {
    \clef "vbas-dessus"
    \ffclef "vbas-dessus" <>^\markup\character Une nymphe
    mi''4 mi'' |
    mi'' mi'' re''4. do''8 |
    si'4 si' do''4. do''8 |
    sold'4 la' la'4. sold'8 |
    la'4 la' r2 |
    R1*3 |
    r2 <>^\markup\character Une nymphe do''4. re''8 |
    mi''4 mi'' la'( si'8) do'' |
    si'2 re''4. re''8 |
    re''4 do'' do'' si' |
    do'' do'' do''4. do''8 |
    do''4. re''8 si'4. si'8 |
    si'2 la'4. la'8 |
    la'4. si'8 sold'4. la'8 |
    la'2 do''4. do''8 |
    do''4. re''8 si'4. si'8 |
    si'2 la'4. la'8 |
    la'4. si'8 sold'4. la'8 |
    la'2 r |
    R1*11 |
    r2
  }
  %% Dessus
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s2 s1*3 s2 \ffclef "vdessus" }
      \tag #'vdessus { \clef "vdessus" r2 R1*3 r2 }
    >> <>^\markup\character Chœur la'4 si' |
    do''4. re''8 si'4. la'8 |
    sold'4 sold' mi''4. mi''8 |
    re''4 do'' si'( do''8) re'' |
    do''4 la' <<
      \tag #'basse { s2 s1*11 s2 \ffclef "vdessus" }
      \tag #'vdessus { r2 R1*11 r2 }
    >> <>^\markup\character Chœur mi''4. fa''8 |
    sol''4 sol'' fa''4. mi''8 |
    re''2 fa''4. fa''8 |
    fa''4 mi'' re''( mi''8) fa'' |
    mi''4 do'' mi''4. mi''8 |
    mi''4. fa''8 re''4. re''8 |
    re''2 do''4. do''8 |
    do''4. si'8 si'4.\trill la'8 |
    la'2 mi''4. mi''8 |
    mi''4. fa''8 re''4. re''8 |
    re''2 do''4. do''8 |
    do''4. si'8 si'4.\trill la'8 |
    la'2
  }
  %% Haute-contre
  \tag #'vhaute-contre {
    \clef "vhaute-contre"
    r2 R1*3 r2
    mi'4 mi' |
    mi'4. fa'8 fa'4. fa'8 |
    mi'4 mi' la4. la8 |
    si4 do'8[ re'] mi'4. mi'8 |
    mi'4 do'
    r2 R1*11 r2
    la'4. la'8 |
    sol'4 sol' la'4. la'8 |
    sol'2 la'4. la'8 |
    sol'4 sol' sol'4. sol'8 |
    sol'4 mi' la'4. mi'8 |
    fa'4. fa'8 fad'4. fad'8 |
    sold'2 la'4. mi'8 |
    fa'4. fa'8 mi'4. re'8 |
    dod'2 la'4. mi'8 |
    fa'4. fa'8 fad'4. fad'8 |
    sold'2 la'4. mi'8 |
    fa'4. fa'8 mi'4. re'8 |
    dod'2
  }
  %% Taille
  \tag #'vtaille {
    \clef "vtaille"
    r2 R1*3 r2
    do'4 si |
    la la re'4. do'8 |
    si4 si do'4. do'8 |
    sold4 la la sold |
    la la
    r2 R1*11 r2
    do'4. re'8 |
    mi'4 mi' do'4. do'8 |
    si2 re'4. re'8 |
    re'4 do' do' si |
    do' sol do'4. do'8 |
    do'4. re'8 si4. si8 |
    si2 la4. la8 |
    la4. si8 sold4. la8 |
    la2 do'4. do'8 |
    do'4. re'8 si4. si8 |
    si2 la4. la8 |
    la4. si8 sold4. la8 |
    la2
  }
  %% Basse
  \tag #'vbasse {
    \clef "vbasse"
    r2 R1*3 r2
    la4 sold |
    la la re4. re8 |
    mi4 mi do4. do8 |
    si,4 la, mi4. mi8 |
    la,4 la,
    r2 R1*11 r2
    la4. la8 |
    mi4 mi fa4. fa8 |
    sol2 re4. do8 |
    si,4 do sol,4. sol,8 |
    do4. do8 la,4. la,8 |
    re4 re si,4. si,8 |
    mi2 la4 la, |
    re4 re mi4. mi8 |
    la,2 la,4. la,8 |
    re4 re si,4. si,8 |
    mi2 la4 la, |
    re4 re mi4. mi8 |
    la,2
  }
>>