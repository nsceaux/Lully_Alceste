\score {
  \new StaffGroupNoBar <<
    %% Violons
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff <<
        { s2 s1*3 s2 \ffclef "dessus" }
        \global \includeNotes "dessus"
      >>
      \new Staff <<
        { s2 s1*3 s2 \ffclef "haute-contre" }
        \global \includeNotes "haute-contre"
      >>
      \new Staff <<
        { s2 s1*3 s2 \ffclef "taille" }
        \global \includeNotes "taille"
      >>
      \new Staff <<
        { s2 s1*3 s2 \ffclef "quinte" }
        \global \includeNotes "quinte"
      >>
    >>
    %% Chœur
    <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2 s1*3 s2 \ffclef "vdessus" }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2 s1*3 s2 \ffclef "vhaute-contre" }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2 s1*3 s2 \ffclef "vtaille" }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2 s1*3 s2 \ffclef "vbasse" }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    %% Céphise
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \keepWithTag #'cephise \includeNotes "voix"
    >> \keepWithTag #'cephise \includeLyrics "paroles"
    %% Nymphe
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'nymphe \includeNotes "voix"
    >> \keepWithTag #'nymphe \includeLyrics "paroles"
    %% B.C.
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s2 s1*3 s2 \bar "" \break
        s2 s1*3 s2 \bar "" \break
        s2 s1*11 s2 \bar "" \break
      }
      \origLayout {
        s2 s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*3\break s1*4 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*4\pageBreak
        s1*3 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*3\break s1*4\break s1*4 s2 \bar "" \pageBreak
        s2 s1*3\pageBreak
        s1*4 s2 \bar "" \pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
