\clef "haute-contre"
r2 R1*3 r2
mi'4 mi' |
mi' mi' fa'4. fa'8 |
mi'4 mi' do''4. do''8 |
sold'4 la' la' sold' |
la' la'
r2 R1*11 r2
do''4. re''8 |
mi''4 mi'' do''4. do''8 |
si'2 re''4. re''8 |
re''4 do'' do'' si' |
do'' sol' do''4. do''8 |
do''4. re''8 si'4. si'8 |
si'2 la'4. la'8 |
la'4. si'8 sold'4. la'8 |
la'2 do''4. do''8 |
do''4. re''8 si'4. si'8 |
si'2 la'4. la'8 |
la'4. si'8 sold'4. la'8 |
la'2

