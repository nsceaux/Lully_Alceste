\tag #'(cephise nymphe basse) {
  Plus les a -- mes sont re -- bel -- les,
  plus leurs pei -- nes sont cru -- el -- le,
}
\tag #'(choeur basse) {
  Plus les a -- mes sont re -- bel -- les,
  plus leurs pei -- nes sont cru -- el -- le,
}
\tag #'(cephise nymphe basse) {
  Les plai -- sirs doux et char -- mants,
  sont le prix des cœurs fi -- del -- les ;
  si l’a -- mour a des tour -- ments
  c’est la fau -- te des a -- mants.
  Si l’a -- mour a des tour -- ments
  c’est la fau -- te des a -- mants.
}
\tag #'(choeur basse) {
  Les plai -- sirs doux et char -- mants,
  sont le prix des cœurs fi -- del -- les ;
  Si l’a -- mour a des tour -- ments
  c’est la fau -- te des a -- mants.
  Si l’A -- mour a des tour -- ments
  c’est la fau -- te des a -- mants.
}
