<<
  %%% Alceste
  \tag #'(alceste basse recit) {
    \clef "vbas-dessus" R2.*4 |
    r4 <>^\markup\character Alceste
    sol''4 re''8 sib'16 sib' sib'8 do''16 re'' |
    sol'4 sol'8 <<
      \tag #'(basse recit) {
        s8 s2 s1 s2. s1 s2. s1*3 s2 \ffclef "vbas-dessus"
      }
      \tag #'alceste { r8 r2 R1 R2. R1 R2. R1*3 r2 }
    >> <>^\markup\character Alceste
    re''8 re''16 re'' sol''8. re''16 |
    mib''8 mib''16 sol'' do''8 do''16 sol' la'4 la' |
    <<
      \tag #'(basse recit) {
        s1*5 s2 \ffclef "vbas-dessus" <>^\markup\character Alceste
      }
      \tag #'alceste { R1*5 r2 }
    >> r4 sol''8. fa''16 |
    mib''8 mib''16 mib'' do''8.\trill do''16 do''8. sib'16 sib'8[ la'16] sib' |
    la'4\trill la'16 la' re'' la' sib'4 sib'8. do''16 |
    re''4 re'' r r8 fa''16 fa'' |
    re''8\trill re''16 fa'' sib'8. sib'16 sib' do'' re'' sib' |
    fa''4 fa''8. do''16 re''8 re'' do''8.\trill la'16 |
    sib'8. sib'16 sib' sib' do'' re'' sol'4 r8 sol''16 fa'' |
    mib''8 mib''16 mib'' do''8.\trill do''16 do''8. sib'16 sib'8[ la'16] sib' |
    la'4\trill la'16 la' re'' la' sib'4 sib'8. do''16 |
    re''4 re'' <<
      \tag #'(basse recit) {
        s2 s1 s2.
        \ffclef "vbas-dessus" <>^\markup\character Alceste
      }
      \tag #'alceste { r2 R1 r2 r4 }
    >> sol''8. fa''16 |
    mib''8 mib''16 mib'' do''8.\trill do''16 do''8. sib'16 sib'8[ la'16] sib' |
    la'4\trill la'16 la' re'' la' sib'4 sib'8. do''16 |
    re''2 re''4 <<
      \tag #'basse {
        s4 s1 s2
        \ffclef "vbas-dessus" <>^\markup\character Alceste
      }
      \tag #'(alceste recit) { r4 R1 r2 }
    >> r4 mib'' |
    do''\trill do'' r do''8 do'' |
    la'2\trill r4 la' |
    re'' re'' r sib'8 sib' |
    sol'2 do''4 do'' |
    la'2\trill la'8[ sol'] la'4 |
    sib'2 <<
      \tag #'basse {
        s2 s2.
        \ffclef "vbas-dessus" <>^\markup\character Alceste
      }
      \tag #'(alceste recit) { r2 r r4 }
    >> re''4 |
    re''4. do''8 do''4. sib'8 |
    la'2\trill sib'4. sib'8 |
    sib'4.( la'8) la'4. la'8 |
    la'4.( sol'8) sol'[ fad'] sol'4 |
    fad'4 r8 la'16 la' re''8 re''16 re'' re''8. mib''16 |
    si'8\trill si' r sol'16 sol' re''8. mib''16 fa''8 fa''16 re'' |
    mib''4 mib'' r8 do''16 sib' la'8\trill la'16 sib' |
    fad'2.\trill re''4 |
    si'2\trill si'4 <<
      \tag #'basse {
        s4 s2.
        \ffclef "vbas-dessus" <>^\markup\character Alceste
      }
      \tag #'(alceste recit) { r4 r2 r4 }
    >> la'4 |
    fad'2\trill fad'4 <<
      \tag #'basse {
        s4 s1
        \ffclef "vbas-dessus" <>^\markup\character Alceste
      }
      \tag #'(alceste recit) { r4 R1 }
    >>
    r2 sol'4 fad' |
    sol'1 |
    R1 |
  }
  %%% Cleante
  \tag #'(cleante basse recit) {
    <<
      \tag #'(basse recit) { s2.*4 s1 s4. \ffclef "vbasse" }
      \tag #'cleante { \clef "vbasse" R2.*4 R1 r4 r8 }
    >> <>^\markup\character Cleante
    re'8 sib4 sib8 sib16 sib |
    sol4.\trill sol8 re4 re8 re16 sol |
    do4 r8 do'16 do' la8\trill la16 la |
    fad8. fad16 fad fad fad la re8 re r sib |
    sib4 r8 sib16 la la8\trill la16 sib |
    sol4
    \tag #'cleante { r4 r2 R1*13 }
  }
  %%% Admete
  \tag #'(admete basse recit recit2) {
    <<
      \tag #'recit2 {
        \clef "vtaille" R2.*4 R1*3 R2. R1 R2. R1*10 R1 R1*3 R2.
        R1*9 | r2 r4 <>^\markup\character Admete
      }
      \tag #'(admete basse recit) {
        <<
          \tag #'(basse recit) { s2.*4 s1*3 s2. s1 s2. s4 \ffclef "vtaille" }
          \tag #'admete { \clef "vtaille" R2.*4 R1*3 R2. R1 R2. r4 }
        >> <>^\markup\character Admete
        r8 re'8 la8.\trill sib16 do'8. do'16 |
        do'8.[ sib16] sib8 re' mib'4 la8 la16 sib |
        fad4. re'8 la8. sib16 fad!8. sol16 |
        sol2 <<
          \tag #'(basse recit) {
            s2 s1
            \ffclef "vtaille" <>^\markup\character Admete
          }
          \tag #'admete { r2 R1 }
        >> r8 do' do'8. re'16 mib'8. mib'16 mib'8[ re'16] mib' |
        re'4\trill re'8. mib'16 fa'4 sib8([ lab32\trill sol]) lab16 |
        sol4\trill do'8. sol16 la8 sib sib[ la] |
        sib4 r8 re' mib'4 la8 la16 sib |
        fad4. re'8 la8. sib16 fad!8. sol16 |
        sol2 <<
          \tag #'(basse recit) {
            s2 s1*3 s2. s1*4 s2
            \ffclef "vtaille" <>^\markup\character Admete
          }
          \tag #'admete { r2 R1*3 R2. R1*4 r2 }
        >> r4 la8. la16 |
        sib8 sib16 sib sib8[ la16] sib la4\trill r8 re'16 re' |
        si8 si16 si si8. re'16 sol8 sol <<
          \tag #'basse {
            s4 s1*2 s2.
            \ffclef "vtaille" <>^\markup\character Admete
          }
          \tag #'admete { r4 R1*2 r2 r4 }
        >>
      }
    >>
    \tag #'(admete basse recit2) {
      re'4 |
      sib sib r sib8 sib |
      sol2\trill <<
        \tag #'basse {
          s2 s1*5 s2
          \ffclef "vtaille" <>^\markup\character Admete
        }
        \tag #'(admete recit2) {
          r2 |
          R1 |
          r2 r4 do' |
          fa' fa' r re'8 re' |
          sib2 mib'4. mib'8 |
          do'2 do'4 fa' |
          re'2
        }
      >> r4 re' |
      sol' fa' <<
        \tag #'basse {
          fa'4 s | s1*8 | s2.
          \ffclef "vtaille" <>^\markup\character Admete
        }
        \tag #'(admete recit2) {
          fa'4. mib'16[ re'] |
          mib'2. mib'4 |
          mib'4. mib'8 re'4. re'8 |
          do'2 do'4. do'8 |
          do'4.( sib8) sib[ la] sib4 |
          la\trill r r2 |
          R1*3 |
          r2 r4
        }
      >> re'4 |
      mib'2 mib'4 <<
        \tag #'basse {
          s4 s2.
          \ffclef "vtaille" <>^\markup\character Admete
        }
        \tag #'(admete recit2) { r4 | r2 r4 }
      >> re'4 |
      sol2 sol4 r |
      \tag #'(admete recit2) {
        r2 la4 sib |
        sol1 |
        R1 |
      }
    }
  }
>>
