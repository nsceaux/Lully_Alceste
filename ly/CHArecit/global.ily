\key re \minor \beginMark "Ritournelle"
\digitTime\time 3/4 \midiTempo#120 s2.*4
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*10
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1*9
\time 2/2 \midiTempo#160 s1*14
\time 4/4 \midiTempo#80 s1*3
\time 2/2 \midiTempo#160 s1*8 \bar "|."
