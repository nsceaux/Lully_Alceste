\tag #'(alceste basse recit) {
  O Dieux ! quel spec -- ta -- cle fu -- nes -- te ?
}
\tag #'(cleante basse recit) {
  Le chef des en -- ne -- mis mou -- rant, & ter -- ras -- sé,
  de sa rage ex -- pi -- rante a ra -- mas -- sé le res -- te,
  le roy vient d’en es -- tre bles -- sé.
}
\tag #'(admete basse recit) {
  Je meurs, char -- mante Al -- ces -- te,
  mon sort est as -- sez doux
  puis que je meurs pour vous.
}
\tag #'(alceste basse recit) {
  C’est pour vous voir mou -- rir que le ciel me dé -- li -- vre !
}
\tag #'(admete basse recit) {
  A -- vec le nom de vostre es -- poux
  j’eusse es -- té trop heu -- reux trop heu -- reux de vi -- vre ;
  mon sort est as -- sez doux
  puis que je meurs pour vous.
}
\tag #'(alceste basse recit) {
  Est- ce là cét hy -- men si doux, si plein d’ap -- pas,
  qui nous pro -- met -- toit tant de char -- mes ?
  Fa -- loit- il que si- tost l’a -- veu -- gle sort des ar -- mes
  tran -- chast des nœuds si beaux par un af -- freux tré -- pas ?
  Est- ce là cét hy -- men si doux, si plein d’ap -- pas,
  qui nous pro -- met -- toit tant de char -- mes ?
}
\tag #'(admete basse recit) {
  Belle Al -- ces -- te ne pleu -- rez pas,
  tout mon sang ne vaut point vos lar -- mes.
}
\tag #'(alceste basse recit) {
  Est- ce là cét hy -- men si doux, si plein d’ap -- pas,
  qui nous pro -- met -- toit tant de char -- mes ?
}
\tag #'(admete basse recit2) {
  Al -- ces -- te, vous pleu -- rez ?
}
\tag #'(alceste basse recit) {
  Ad -- me -- te, vous mou -- rez.
  Ad -- me -- te, vous mou -- rez, vous mou -- rez, vous mou -- rez.
}
\tag #'(admete recit2) {
  Al -- ces -- te, vous pleu -- rez, vous pleu -- rez,
  vous pleu -- rez ?
}
\tag #'(admete basse recit2) {
  Al -- ces -- te, vous
  \tag #'(admete recit2) {
    pleu -- rez ?
    Al -- ces -- te, vous pleu -- rez, vous pleu -- rez,
    vous pleu -- rez.
  }
}
\tag #'(alceste basse recit) {
  Ad -- me -- te, vous mou -- rez, vous mou -- rez,
  vous mou -- rez, vous mou -- rez.
}
\tag #'(alceste basse recit) {
  Se peut- il que le Ciel per -- met -- te,
  que les cœurs d’Al -- ceste & d’Ad -- me -- te
  soient ain -- si se -- pa -- rez ?
}
\tag #'(alceste basse recit) {
  Ad -- me -- te,
}
\tag #'(admete basse recit2) {
  Al -- ces -- te,
}
\tag #'(alceste basse recit) {
  Ad -- me -- te,
}
\tag #'(admete basse recit2) {
  Al -- ces -- te,
}
\tag #'(alceste basse recit) {
  vous mou -- rez.
}
\tag #'(admete recit2) {
  vous pleu -- rez.
}
