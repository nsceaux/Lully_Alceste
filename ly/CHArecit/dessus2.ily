\clef "dessus" r4 sib'4. do''8 |
la'4 la'4. sib'8 |
sol'4 sol'4. la'8 |
sib'4 fad'4.\trill sol'8 |
sol'4 r r2 |
R1*2 R2. R1 R2. R1*10 R1 R1*3 R2. R1*9 R1*14 R1*3 R1*8 |
