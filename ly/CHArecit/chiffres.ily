s2. <6- 4 2>4 <6>2 <7>4 <6 4+ 3>2 <6 4>4 \new FiguredBass <_+>2
s1*2 <6>1 <_->2 <6 5>4 <_+>2 <6> s4. <6 5 _->8 <4>8
\new FiguredBass <_+>
s2 <6- 4 2>4 <5/> <4> <_-> <9 7 _->\figExtOn <8 6 _->\figExtOff
<_+>2 <6 4+>8 <7 5> \new FiguredBass { <_+>\figExtOn <_+>\figExtOff }
s2. <6>4 <_->1 s4 <6 +4>8 <6+> <_->2 s4 <6 4>8. <6>16 <6>4 <5/>
s4 <6>2 <4>8 <3> s2 <9 7 _->4\figExtOn <8 6 _->\figExtOff
<_+>2 <6 4+>8 <7 5> <_+>4 <_+>1 <_->2 <6>4 <5/> s4 <6>2 <7>8 <6>
<_+>2 <5> s2. <6>2. <5/>4 s2. <6>4 <_->2 <6>4 <5/> s4 <6>2 <7>8 <6>
<_+>1 <6 4>2 <5 _+> <_+>2 <6>4 <6> <_->2 <6>4 <5/> s <6>2 <7>8 <6>
\new FiguredBass { <_+>2\figExtOn <_+>\figExtOff }
s1*2 <6 5/>1 s <6> s2 <_-> s1*2
<6>2 <5/> <\markup\triangle-down 9 8 \figure-flat >1 <7->1 <4>2 <6 5/>
<9 4>2 <8 3-> <_+>1 <_+>2 <6>4 <5/> <_->2 <6 5> <_+>1 <_+> <_->2 <6 5>
<_+>1 <6 4+ 3>2. <6 5 _->4 <6 4>2
\new FiguredBass { <5 4>4\figPosOn <5 _+>\figPosOff }
<_+>1

