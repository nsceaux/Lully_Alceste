\clef "dessus" r4 re''4. mib''8 |
do''4\trill re''4. re''8 |
re''4 do''( do''8.\trill sib'32) do'' |
re''4 la'4.\trill sol'8 |
sol'4 r r2 |
R1*2 R2. R1 R2. R1*10 R1 R1*3 R2. R1*9 R1*14 R1*3 R1*8 |
