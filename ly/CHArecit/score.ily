\score {
  \new ChoirStaff \with { \haraKiriFirst } <<
    \new GrandStaff <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit2 \includeNotes "voix"
    >> \keepWithTag #'recit2 \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s2.*4\break
        s1*2\break s1 s2. s2 \bar "" \break s2 s2. s1\break
        s1*2 s2 \bar "" \break s2 s1\break s1*2\pageBreak
        s1*2 s2 \bar "" \break s2 s1 s2 \bar "" \break s2 s1*2\break
        s2. s1\break s1 s2 \bar "" \break s2 s1*2\pageBreak
        s1*2\break s1*2\break s1*4\break s1*5\break s1*4\pageBreak
        s1*2 s2 \bar "" \break s2 s1*2\break s1*4\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
