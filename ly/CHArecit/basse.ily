\clef "basse" sol,4 sol4. sol8 |
sol4 fa4. fa8 |
mi4 mib2 |
re4 re,2 |
<<
  \tag #'basse {
    sol,4 r r2 |
    R1*2 R2. R1 R2. R1*10 R1 R1*3 R2. R1*9 R1*14 R1*3 R1*8 |
  }
  \tag #'basse-continue {
    sol,1~ |
    sol,2 sol |
    si,1 |
    do2. |
    re2 sib, |
    mib4. do8 re[ re,] |
    sol,4 sol2 fad4 |
    sol sol, do2 |
    re do4 re8 re, |
    sol,2 sol4 si, |
    do2 fa~ |
    fa4 mib8 re do4. do8 |
    sol4 fa8. mib16 re2 |
    mib4 mi fa8 sib, fa,4 |
    sib,2 do |
    re do4 re |
    sol,1 | \allowPageTurn
    do4. re8 mib2 |
    fa4 fad sol8. fa16 mib4 |
    re8. mib16 re8 do sib,2 |
    sib2. |
    la2 sib4 fad |
    sol4. fa8 mib4 si, |
    do4. re8 mi2 |
    fa4 fad sol8. fa16 mib4 |
    re1~ |
    re |
    sol2 mib4 si, |
    do4. re8 mi2 |
    fa4 fad sol8. fa16 mib4 |
    re2 re, |
    sol,1 |
    mib |
    mi! |
    fa |
    re |
    mib2 do |
    fa fa, |
    sib,1 |
    si, |
    do2. do4 |
    fa2 sib, |
    fa,4 fa fad2 |
    sol2 sol, |
    re1 |
    sol2 si, |
    do1 |
    re |
    sol2 sol, |
    do1 |
    re |
    mib2. do4 |
    re2 re, |
    sol,~ sol,8 fa, mi, re, |
    do,1 |
    \once\set Staff.whichBar = "|"
  }
>>
