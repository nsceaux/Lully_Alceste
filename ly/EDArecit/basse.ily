\clef "basse" sib,1~ |
sib,2 sib |
mib4 mi fa2 |
re do |
la,4 sib, do |
fa, fa re2 |
mib2 do4 re |
re,2 re4 |
si,2. |
do1 |
do |
fa |
fa, |
sib, |
mib1*7/8~ \hideNotes mib8 |
