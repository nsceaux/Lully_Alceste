Quit -- tez, quit -- tez les jeux, son -- gez à vous def -- fen -- dre,
contre un au -- da -- ci -- eux u -- nis -- sons nos ef -- forts :
le fils de Ju -- pi -- ter vient i -- cy de des -- cen -- dre
seul, il ose at -- ta -- quer tout l’em -- pi -- re des morts.

Qu’on ar -- res -- te ce te -- me -- rai -- re,
ar -- mez vous, a -- mis, ar -- mez vous,
qu’on des -- chai -- ne Cer -- be -- re,
cou -- rez tous, cou -- rez tous.


Son bras a -- bat tout ce qu’il frap -- pe.
Tout cede à ses hor -- ri -- bles coups.
Rien ne re -- sis -- te, rien n’es -- cha -- pe.
