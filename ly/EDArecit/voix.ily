\clef "vhaute-contre" <>^\markup\character Alecton
r8 sib re'16\trill re' re' mib' fa'8. fa'16 re'\trill re' re' fa' |
sib4 sib r8 re' re'16 re' mib' fa' |
sol'8 sol16 sol do'8 do'16 do' la8\trill r16 do' fa' fa' fa' fa' |
fa'8 re'16 re' si8 si16 re' sol8 sol sol' r16 do' |
fa'8 fa'16 do' re'8 sib16 re' sol8\trill sol16 la |
fa4 \ffclef "vbasse" <>^\markup\character Pluton r8 fa16 fa sib8. sib16 sib8 sib16 sib |
sol8\trill sol r sol16 sol do'8. la16 fad8 fad16 la |
re4 r8 re16 re re8 mi16 fad |
sol8 sol r sol16 sol re8 re16 sol |
mi1\trill |
\ffclef "vhaute-contre" <>^\markup\character Alecton
r8 sol' mi'8. mi'16 do'8. do'16 sol8. do'16 |
la2\trill la |
r8 do' fa'8. fa'16 fa'8. fa'16 do'8. fa'16 |
re'4\trill re'8 re'16 fa' sib8. sib16 sib8. re'16 |
sol2 sol |
