\score {
  \new ChoirStaff <<
    \new Staff \withRecit <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s1*2\break }
    >>
  >>
  \layout { }
  \midi { }
}