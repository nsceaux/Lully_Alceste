\clef "vbas-dessus" <>^\markup\character La Nymphe de la Seine
r4 do''8 do''16 mi'' do''8. do''16 sol'8. sol'16 |
mi'8\trill mi' r4 do''8 do''16 re'' mi''8. fa''16 |
re''4\trill si'8. re''16 sol'4. sol'16 fad' |
sol'1 |
