\tag #'(thetis basse) {
  Es -- poux in -- for -- tu -- né re -- dou -- te ma co -- lé -- re,
  tu vas has -- ter l’ins -- tant qui doit fi -- nir tes jours ;
  c’est Thé -- tis que la mer ré -- vé -- re,
  que tu vois con -- tre toy du par -- ty de son fre -- re ;
  et c’est à la mort que tu cours.
}
\tag #'(admete alcide basse) {
  Au se -- cours, au se -- cours, au se -- cours.
}
\tag #'(choeur basse) {
  Au se -- cours,
  au se -- cours, au se -- cours, au se -- cours,
  au se -- cours, au se -- cours, au se -- cours.
}
\tag #'(thetis basse) {
  Puis qu’on mé -- pri -- se ma puis -- san -- ce
  que les vents __ dé -- chaî -- nez,
  que les flots __ mu -- ti -- nez
  s’ar -- ment pour ma ven -- gean -- ce.
  Que les vents dé -- chaî -- nez,
  que les flots mu -- ti -- nez
  s’ar -- ment pour ma ven -- gean -- ce.
}
