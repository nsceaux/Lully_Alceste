<<
  %% Thetis
  \tag #'(thetis basse) {
    \clef "vdessus" <>^\markup\character-text Thetis sortant de la mer
    r8 fa'' re''16\trill re'' re'' re'' la'8. la'16 re'' re'' re'' mi'' |
    fa''8 fa'' r re'' la'8. la'16 la'8. si'16 |
    do''8. do''16 do''8. do''16 re''8. la'16 |
    sib'4 sib'8. re''16 sol'8 sol'16 sol' sol'8. sol'16 |
    mi'8\trill mi' r16 do'' do''32 do'' re'' mi'' fa''8 do''16 re'' sol'8\trill la'16 sib' |
    la'8\trill la'16 do'' fa''8 fa''16 fa'' re''8\trill re''16 mi'' |
    dod''2. |
    <<
      \tag #'basse { s2.*10 s2 \ffclef "vdessus" }
      \tag #'thetis { R2.*10 r2 }
    >> <>^\markup\character Thetis
    r16 la' la' la' fa' fa' fa' re' |
    la'8 la' r la'16 la' re''[ mi'' re'' mi'' fa''8] mi''16 re'' |
    dod''8 la'16 la' re''[ mi'' re'' do''?]( si'8)\trill si'16 si' |
    do''4 fa''8. fa''16 fa''8 fa''16 mi'' |
    fa''8. fa''16 r8 fa'16 sol' la'8\trill la'16 si' |
    do''4 r8 do''16 do'' fa''8 fa''16 fa'' |
    re''4\trill mi''8. mi''16 mi''8 mi''16 fa'' |
    re''2\trill re''4 |
  }
  %% Admete
  \tag #'(admete basse) {
    <<
      \tag #'basse { s1*2 s2. s1*2 s2.*2 }
      \tag #'admete { \clef "vtaille" R1*2 R2. R1*2 R2.*2 }
    >> \ffclef "vtaille" <>^\markup\character Admete
    r4 mi' mi' |
    fa' <<
      \tag #'basse { s2. \ffclef "vtaille" <>^\markup\character Admete }
      \tag #'admete { r4 r | r }
    >> re'4 re' |
    mi' <<
      \tag #'basse { s2.*3 \ffclef "vtaille" <>^\markup\character Admete }
      \tag #'admete { r4 r | R2.*2 | r4 }
    >> mi'4 mi' |
    fa'
    \tag #'admete { r4 r | R2.*2 R1*2 R2.*6 }
  }
  %% Alcide
  \tag #'alcide {
    \clef "vbasse-taille" R1*2 R2. R1*2 R2.*2
    \ffclef "vbasse-taille" <>^\markup\character Alcide
    r4 dod' dod' |
    re' r r |
    r si si |
    do' r r |
    R2.*2 |
    r4 dod' dod' |
    re' r r |
    R2.*2 R1*2 R2.*6
  }
  %% Chœur
  \tag #'(vdessus basse) {
    <<
      \tag #'basse {
        s1*2 s2. s1*2 s2.*3 s4 \ffclef "vdessus" <>^\markup\character Chœur
      }
      \tag #'vdessus {
        \clef "vdessus" R1*2 R2. R1*2 R2.*2
        \ffclef "vdessus" <>^\markup\character Chœur R2. r4
      }
    >>
    fa''4 fa'' |
    <<
      \tag #'basse { re''4 s2 s4 \ffclef "vdessus" <>^\markup\character Chœur }
      \tag #'vdessus { re''2 r4 | r4 }
    >> mi''4 mi'' |
    do'' do'' re'' |
    si' si' mi'' |
    <<
      \tag #'basse { dod''4 s2 s4 \ffclef "vdessus" <>^\markup\character Chœur }
      \tag #'vdessus { dod''2 r4 | r4 }
    >> fa''4 fa'' |
    re'' re'' re'' |
    re'' re'' dod'' |
    re''2
    \tag #'vdessus { r2 | R1 R2.*6 }
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre"
    R1*2 R2. R1*2 R2.*2
    \ffclef "vhaute-contre" R2.
    r4 la' la' |
    sol'2 r4 |
    r4 sol' sol' |
    fa' fa' fa' |
    mi' mi' mi' |
    mi'2 r4 |
    r4 la' la' |
    fa' fa' sol' |
    mi' mi'4. la'8 |
    fad'2 r |
    R1 R2.*6 |
  }
  \tag #'vtaille {
    \clef "vtaille"
    R1*2 R2. R1*2 R2.*2
    \ffclef "vtaille" R2.
    r4 re' re' |
    si2 r4 |
    r4 do' do' |
    la la la |
    la la sold |
    la2 r4 |
    r4 re' re' |
    re' sib sib |
    la la la |
    la2 r |
    R1 R2.*6 |
  }
  \tag #'vbasse {
    \clef "vbasse"
    R1*2 R2. R1*2 R2.*2
    \ffclef "vbasse" R2.
    r4 re re |
    sol2 r4 |
    r4 do do |
    fa fa re |
    mi mi mi |
    la,2 r4 |
    r4 re re |
    sib sib sol |
    la la la, |
    re2 r |
    R1 R2.*6 |
  }
>>
