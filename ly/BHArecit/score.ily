\score {
  \new StaffGroupNoBar <<
    %% Chœur
    \new ChoirStaff <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2 s1*3 s2 \ffclef "vdessus" }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2 s1*3 s2 \ffclef "vhaute-contre" }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2 s1*3 s2 \ffclef "vtaille" }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2 s1*3 s2 \ffclef "vbasse" }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    %% Thetis
    \new Staff \with { \haraKiri } \withLyrics <<
      \global \keepWithTag #'thetis \includeNotes "voix"
    >> \keepWithTag #'thetis \includeLyrics "paroles"
    %% Admete
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'admete \includeNotes "voix"
    >> \keepWithTag #'admete \includeLyrics "paroles"
    %% Alcide
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'alcide \includeNotes "voix"
    >> \keepWithTag #'alcide \includeLyrics "paroles"
    %% B.C.
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s1*2 s2. s1*2 s2.*2\break \newSpacingSection
        s2.*10 s2 \bar "" \break \newSpacingSection
      }
      \origLayout {
        s1*2\break s2. s1\break s1 s4 \bar "" \pageBreak
        s2 s2.*3\break s2.*6\pageBreak
        s2.*2 s1\break s1 s2. s2 \bar "" \break s4 s2.*2\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
