\piecePartSpecs
#`((basse-continue #:score-template "score-basse-voix")
   (basse #:music , #{
s1*2 s2. s1*2 s2. s2 s8 s_\markup\right-align\italic\line {
  et c’est à la mort que tu cours.
} #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#25 #}))
