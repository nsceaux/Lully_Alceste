\clef "basse"
<<
  \tag #'basse { R1*2 R2. R1*2 R2.*2 }
  \tag #'basse-continue {
    re1~ |
    re |
    la,8. sol,16 fad,2 |
    sol, si, |
    do4. sib,8 la,8. sib,16 do4 |
    fa re sib |
    la2. |
  }
>>
la, |
re2 re4 |
sol2 sol,4 |
do2 do4 |
fa2 re4 |
mi2 mi,4 |
la,2 la4 |
re2 re4 |
sib sib sol |
la la la, |
<<
  \tag #'basse { re2 r R1 R2.*6 }
  \tag #'basse-continue {
    re2~ re~ |
    re1 |
    la4 fad sol |
    do8. sib,16 la,8. sib,16 do4 |
    fa,2. |
    fa |
    sib4 sol la8 la, |
    re2. |
  }
>>
