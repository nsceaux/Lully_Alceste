\clef "taille" sol'4 |
sol'2 re'4 re' re'2 |
re'4. do'8 si4 la sol2 |
la si4 do' re'2 |
re'2.~ re'2 sol'4 |
sol'2 re'4 re' re'2 |
re'4. do'8 si4 la sol2 |
la4 re' re' mi' re'4. do'8 |
si2.~ si2 sol'4 |
la'2 la'4 re' re'2 |
mi' fad'4 mi' re'2 |
dod' re'4 re' dod'2 |
re'2.~ re'2 re'4 |
re'2 sol'4 sol' la'2 |
sol' fad'4 sol' mi'2 |
re' re'4 mi'4. re'8 do'4 |
si2.~ si2 sol'4 |
si2.~ si2
