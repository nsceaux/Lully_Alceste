\clef "dessus" re''4 |
si'4.\trill do''8 re''4 do'' si'2\trill |
la'\trill sol'4 la' si'2 |
la'\trill sol'4 la' si'2 |
la'2.\trill~ la'2 re''4 |
si'4.\trill do''8 re''4 do'' si'2\trill |
la'\trill sol'4 la' si'2 |
la'\trill si'4 do'' la'2\trill |
sol'2.~ sol'2 sol''4 |
fad''4.\trill sol''8 la''4 sol'' fad''2\trill |
mi''2\trill re''4 mi'' fad''2 |
mi''\trill fad''4 sol'' mi''2\trill |
re''2.~ re''2 sol''4 |
fad''4.\trill mi''8 re''4 mi'' do''2\trill |
si' do''8.(\trill si'32 do'') re''4 mi''2 |
la'2 si'4 do'' la'2\trill |
sol'2.~ sol'2 sol''4 |
sol'2.~ sol'2
