\clef "basse" sol4 |
sol4. la8 si4 fad sol2 |
re mi4 fad sol2 |
re si,4 la, sol,2 |
re2.~ re2 sol4 |
sol4. la8 si4 fad sol2 |
re mi4 fad sol2 |
re sol4 do re2 |
sol,2.~ sol,2 sol,4 |
re4. mi8 fad4 sol re2 |
la re4 dod re2 |
la, re4 sol, la,2 |
re, re4 si,4. la,8 sol,4 |
re4. do8 si,4 do la,2 |
mi2 la,4 si, do2 |
re sol4 do re re, |
sol,2.~ sol,2 sol,4 |
sol,2.~ sol,2
