\clef "quinte" re'4 |
re'4. do'8 si4 la re'2 |
re' sol4 re'2 sol4 |
fad2 re'4 do' sol2 |
la2.~ la2 sol4 |
sol re'8 do' si4 la re'2 |
re' sol4 re'2 sol4 |
fad8 sol la4 sol2 re'4. re'8 |
re'2.~ re'2 re'4 |
re'2 do'4 si la2 |
la la4 la la2 |
mi' re' la4. la8 |
la2.~ la2 re'4 |
re'2 re'4 do' do'2 |
sol do'4 si la2 |
la sol4 sol re'4. re'8 |
re'2.~ re'2 re'4 |
re'2.~ re'2
