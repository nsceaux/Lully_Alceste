\clef "haute-contre" si'4 |
si'4. la'8 sol'4 la' sol'2 |
fad' mi'4 re' sol'2 |
fad' mi'4 fad' sol'2 |
fad'2.~ fad'2 si'4 |
si'4. la'8 sol'4 la' sol'2 |
fad' mi'4 re' sol'2 |
fad' sol'4 la' fad'2 |
sol'2.~ sol'2 re''4 |
re''4. re''8 do''4 si' la'2 |
la' la'4 la' la'2 |
la' la'4 si' la'2 |
la'2.~ la'2 si'4 |
la'2 si'4 mi' mi'2 |
mi'2 fad'4 re' sol'2 |
fad' sol'4 la' fad'2 |
sol'2.~ sol'2 re''4 |
sol'2.~ sol'2
