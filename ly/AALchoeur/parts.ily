\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse #:tag-global nuances1)
   (basse-continue #:tag-global nuances1
                   #:music , #{ s2.*8\noBreak s2.\noBreak #}
                   #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#95 #}))
