\clef "vdessus" R2.*27
\once\override Staff.TextScript.outside-staff-priority = #999
<>^\markup\column {
  \character Chœur des Nayades, et des Divinités champestres
}
\setMusic #'reprise {
  r4 r mi''4 |
  mi''4. mi''8 re''4 |
  mi'' mi'' mi'' |
  mi''4. mi''8 re''4 |
  mi'' mi'' do'' |
  si'2\trill si'4 |
  do''4. re''8 mi''4 |
  re''2\trill si'4 |
  do''4. re''8 mi''4 |
  re''2\trill re''4 |
  mi''2 mi''4 |
  re''4. mi''8 fa''4 |
  mi''2 mi''4 |
  re''4. mi''8 fa''4 |
  mi''2 r4 |
}
\keepWithTag #'() \reprise
R2.*11 |
r4 mi'' mi'' |
fa''4. fa''8 mi''4 |
re''2 mi''4 |
dod'' dod'' re'' |
mi''4. mi''8 re''4 |
re''4. re''8 dod''4 |
re''2 la'4 |
re''4. do''8 si'4 |
la'4.\trill si'8 do''4 |
si'2\trill r4 |
R2.*5 |
\reprise
R2.*12
