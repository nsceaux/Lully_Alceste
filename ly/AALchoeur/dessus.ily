\clef "dessus" R2.*20 |
<>_"Hautbois"
\twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''8 fa'' sol'' la'' sol''4 |
    sol''8 fa'' sol'' la'' sol'' la'' |
    fa'' mi'' fa'' sol'' fa'' sol'' |
    mi''2\trill mi''4 |
    re''8 do'' re'' mi'' re''4 |
    re''8 do'' re'' mi'' fa'' sol'' |
    mi'' fa'' re''4.\trill do''8 |
    do''2 }
  { mi''8 re'' mi'' fa'' mi''4 |
    mi''8 re'' mi'' fa'' mi'' fa'' |
    re'' do'' re'' mi'' re''4 |
    sol'2 do''4 |
    si'8 la' si' do'' si'4 |
    si'8 la' si' do'' re'' mi'' |
    do'' re'' si'4.\trill do''8 |
    do''2 }
>>
\setMusic #'reprise {
  mi''4 |
  mi''4. mi''8 re''4 |
  mi'' mi'' mi'' |
  mi''4. mi''8 re''4 |
  mi'' mi'' do'' |
  si'2\trill si'4 |
  do''4. re''8 mi''4 |
  re''2\trill si'4 |
  do''4. re''8 mi''4 |
  re''2\trill re''4 |
  mi''2 mi''4 |
  re''4. mi''8 fa''4 |
  mi''2 mi''4 |
  re''4. mi''8 fa''4 |
  mi''4 r8 <>^"Trompettes" do'' mi'' do'' |
  sol''16 fa'' sol'' la'' sol''8 sol'' sol''16 fa'' mi'' re'' |
  mi''8.\trill re''16 do''4 sol''16 fa'' mi'' re'' |
  mi''8.\trill re''16 do''8 do''16 re'' mi'' re'' mi'' fa'' |
  re''16 do'' re'' mi'' re''4 re''16 do'' re'' mi'' |
  re''4 r8 re'' sol'' sol''16 fa'' |
  mi'' re'' mi'' fa'' mi''4 mi''16 re'' mi'' fa'' |
  mi''4. do''16 re'' mi'' fa'' sol'' la'' |
  sol''2 mi''16 fa'' sol'' la'' |
  sol''4 r8 re''16 mi'' fa'' mi'' fa'' sol'' |
  mi''16 re'' mi'' fa'' sol'' fa'' mi'' fa'' re''4\trill |
  do'' sol''16 fa'' mi'' fa'' re''4\trill |
}
\keepWithTag #'() \reprise
do''4 mi'' mi'' |
fa''4. fa''8 mi''4 |
re''2 mi''4 |
dod'' dod'' re'' |
mi''4. mi''8 re''4 |
re''4. re''8 dod''4 |
re''2 la'4 |
re''4. do''8 si'4 |
la'4.\trill si'8 do''4 |
si'4.\trill <>_\markup\whiteout Hautbois
\twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''8 re''8. fa''16 |
    mi''8 re'' mi'' fa'' mi'' fa'' |
    re''4\trill re''8. sol''16 re''8. fa''16 |
    mi''8 re'' mi'' fa'' mi'' fa'' |
    re''\trill do'' re'' mi'' fa'' sol'' |
    fa''\trill mi'' re''4.\trill do''8 |
    do''2 }
  { si'8 si'8. si'16 |
    do''8 si' do'' re'' do'' re'' |
    si'4\trill si'8. si'16 si'8. si'16 |
    do''8 si' do'' re'' do'' re'' |
    si' la' si' do'' re'' mi'' |
    re'' do'' si'4.\trill do''8 |
    do''2 }
>>
\reprise
do''2. |
