\score {
  \new StaffGroupNoBar <<
    %% La Gloire, les Nymphes
    \new Staff \with { \haraKiri } \withLyrics <<
      { \noHaraKiri s2.*26 \revertNoHaraKiri }
      \keepWithTag #'() \global
      \keepWithTag #'voix1 \includeNotes "voix"
    >> \keepWithTag #'(gloire nymphe-thuilleries) \includeLyrics "paroles"
    \new Staff \with { \haraKiri } \withLyrics <<
      { \noHaraKiri s2.*26 \revertNoHaraKiri }
      \keepWithTag #'() \global
      \keepWithTag #'voix2 \includeNotes "voix"
    >> \keepWithTag #'(nymphe-seine nymphe-marne) \includeLyrics "paroles"
    %% Orchestre
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff <<
        \keepWithTag #'(nuances1 nuances2) \global
        \keepWithTag #'dessus \includeNotes "dessus"
      >>
      \new Staff <<
        \keepWithTag #'(nuances1 nuances2) \global
        \includeNotes "haute-contre"
      >>
      \new Staff <<
        \keepWithTag #'(nuances1 nuances2) \global
        \includeNotes "taille"
      >>
      \new Staff <<
        \keepWithTag #'(nuances1 nuances2) \global
        \includeNotes "quinte"
      >>
    >>
    %% Chœur
    <<
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2.*28 \noHaraKiri }
        \override Staff.TextScript.direction = #UP
        \keepWithTag #'nuances1 \global
        \includeNotes "vdessus"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2.*28 \noHaraKiri }
        \override Staff.TextScript.direction = #UP
        \keepWithTag #'nuances1 \global
        \includeNotes "vhaute-contre"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2.*28 \noHaraKiri }
        \override Staff.TextScript.direction = #UP
        \keepWithTag #'nuances1 \global
        \includeNotes "vtaille"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \with { \haraKiriFirst } \withLyrics <<
        { s2.*28 \noHaraKiri }
        \override Staff.TextScript.direction = #UP
        \keepWithTag #'nuances1 \global
        \includeNotes "vbasse"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff <<
      \keepWithTag #'nuances1 \global
      \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s2.*20\break s2.*7\pageBreak
        s2.*10\break\noPageBreak s2.*8\pageBreak
        s2.*7\break\noPageBreak s2.*9\pageBreak
        s2.*9\break\noPageBreak s2.*11\pageBreak
      }
      \origLayout {
        s2.*6\break s2.*6\break s2.*8\break s2.*7\pageBreak
        s2.*6\pageBreak s2.*6\pageBreak s2.*6\pageBreak s2.*6\pageBreak
        s2.*6\pageBreak s2.*5\pageBreak s2.*8\pageBreak s2.*5\pageBreak
        s2.*5\pageBreak s2.*6\pageBreak s2.*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
