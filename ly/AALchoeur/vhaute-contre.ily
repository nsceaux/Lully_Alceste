\clef "vhaute-contre" R2.*27 |
\setMusic #'reprise {
  r4 r sol' |
  sol'4. sol'8 sol'4 |
  sol' sol' sol' |
  sol'4. sol'8 sol'4 |
  sol' sol' sol' |
  sol'2 sol'4 |
  sol'4. sol'8 sol'4 |
  sol'2 sol'4 |
  mi'4. mi'8 mi'4 |
  sol'2 sol'4 |
  sol'2 sol'4 |
  sol'4. sol'8 sol'4 |
  sol'2 sol'4 |
  sol'4. sol'8 sol'4 |
  sol'2 r4 |
}
\keepWithTag #'() \reprise
R2.*11 |
r4 sol' sol' |
la'4. sol'8 fa'4 |
fa'2 sol'4 |
mi' mi' fa' |
mi'4. mi'8 fa'4 |
fa'4. fa'8 mi'4 |
fad'2 la'4 |
la'4. la'8 sol'4 |
sol'4. sol'8 fad'4 |
sol'2 r4 |
R2.*5 |
\reprise
R2.*12
