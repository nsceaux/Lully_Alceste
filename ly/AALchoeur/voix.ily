<<
  %% La Gloire
  \tag #'(voix1 basse) {
    \clef "vdessus" <>^\markup\character La Gloire
    r4 r do'' |
    do''4. do''8 sol'4 |
    do'' do'' sol'' |
    mi''4.\trill re''8 do''4 |
    si' sol' sol'' |
    fa''2\trill mi''4 |
    re''4.\trill mi''8 fa''4 |
    mi''2\trill
  }
  %% La Nymphe de la Seine
  \tag #'voix2 {
    \clef "vbas-dessus" <>^\markup\character La Nymphe de la Seine
    R2.*2 |
    r4 r sol' |
    do''4. re''8 mi''4 |
    re'' si' mi'' |
    re''2\trill do''4 |
    do''4. do''8 si'4 |
    do''2
  }
>>
<<
  %% La Nymphe des Thuilleries
  \tag #'(voix1 basse) {
    \ffclef "vdessus" <>^\markup\character La Nymphe des Thuilleries
    mi''4 |
    re''2\trill do''4 |
    si'2\trill si'4 |
    do'' re'' mi'' |
    mi''( re'')\trill do'' |
    do''( si'4.)\trill la'8 |
    la'4
  }
  \tag #'voix2 { r4 R2.*5 s4 }
>>
<<
  %% La Nymphe de la Marne
  \tag #'(voix2 basse) {
    \ffclef "vbas-dessus" <>^\markup\character La Nymphe de la Marne
    r8 dod'' dod''4 |
    re''4. do''8 si'4 |
    do''4. si'8[ la' si'] |
    si'4\trill sol' r8 sol' |
    do''4 re'' mi'' |
    fa'' re''4.\trill do''8 |
    do''2. |
  }
  \tag #'voix1 { r4 r R2.*6 }
>>
R2.*75