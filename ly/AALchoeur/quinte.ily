\clef "quinte" R2.*27 |
r4 r
\setMusic #'reprise {
  do'4 |
  do'4. do'8 re'4 |
  do' do' do' |
  do'4. do'8 re'4 |
  do' do' do' |
  re'2 sol4 |
  sol4. sol8 sol4 |
  sol2 sol4 |
  sol4. sol8 sol4 |
  sol2 re'4 |
  do'2 do'4 |
  re'4. re'8 re'4 |
  do'2 do'4 |
  re'4. re'8 re'4 |
  do'4 r8 do' do' do' |
  do'8. do'16 do'4 do'16 re' mi' fa' |
  mi'8. fa'16 sol'8 sol' do' do' |
  do'16 re' do' si do'4 do' |
  re'16 mi' re' do' re'4 re'16 mi' re' do' |
  re'4 r8 re' re' re' |
  do'16 re' do' si do'4 do'16 re' do' si |
  do'4 r8 do' do'8. do'16 |
  re'4 r8 do' do'8. do'16 |
  re'4 r8 re' si8. si16 |
  do'8. do'16 do'8 sol sol8. sol16 |
  sol4 do'8 sol sol8. sol16 |
}
\keepWithTag #'() \reprise
sol4 do' do' |
do'4. do'8 fa'4 |
fa'2 mi'4 |
mi' mi' re' |
la4. la8 la4 |
la4. la8 la4 |
la2 re'4 |
re'4. re'8 sol4 |
la4. la8 la4 |
sol2 r4 |
R2.*5 |
r4 r
\reprise
sol2. |
