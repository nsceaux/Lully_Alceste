\clef "haute-contre" R2.*27 |
r4 r
\setMusic #'reprise {
  do''4 |
  do''4. do''8 si'4 |
  do'' do'' do'' |
  do''4. do''8 si'4 |
  do'' do'' sol' |
  sol'2 sol'4 |
  sol'4. sol'8 sol'4 |
  sol'2 sol'4 |
  sol'4. sol'8 sol'4 |
  sol'4. la'8 si'4 |
  do''2 do''4 |
  do''4. do''8 si'4 |
  do''2 do''4 |
  do''4. do''8 si'4 |
  do''4 r8 sol' sol' sol' |
  sol'4. do''8 do'' do'' |
  do''4. do''8 do'' do'' |
  do''4. do''8 do'' do'' |
  si'16 la' si' do'' si'4 si'16 la' si' do'' |
  si'4. si'8 si' si' |
  do''16 si' do'' re'' do''4 do''16 si' do'' re'' |
  do''4. sol'8 do''8. do''16 |
  si'2 do''8. do''16 |
  si'4 r8 si'16 do'' re'' do'' re'' mi'' |
  do'' si' do'' re'' mi'' re'' do'' re'' si'4\trill |
  do'' mi''16 re'' do'' re'' si'4\trill |
}
\keepWithTag #'() \reprise
do''4 do'' do'' |
do''4. do''8 do''4 |
sib'2 sib'4 |
la' la' la' |
la'4. la'8 la'4 |
la'4. la'8 la'4 |
la'2 la'4 |
la'4. la'8 sol'4 |
sol'4. sol'8 fad'4 |
sol'2 r4 |
R2.*5 |
r4 r
\reprise
do''2. |
