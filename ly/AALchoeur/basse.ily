\clef "basse" do2.~ |
do~ |
do~ |
do |
sol2 mi4 |
fa2 fa4 |
sol2 sol,4 |
do2 do4 |
si,2 la,4 |
mi2 re4 |
do si, la, |
sold,2 la,4 |
mi,2. |
la,2 la4 |
fad2 sol4 |
sol2 fad4 |
sol2 fa4 |
mi re do |
fa, sol,2 |
do2.~ |
do |
do'~ |
do'4 si2 |
do'2 do4 |
sol2.~ |
sol2 si,4 |
do sol,2 |
do2
\setMusic #'reprise {
  do'4 |
  do'4. do'8 sol4 |
  do' do' do' |
  do'4. do'8 sol4 |
  do' do' do |
  sol2 sol4 |
  do4. si,8 do4 |
  sol,2 sol4 |
  do4. si,8 do4 |
  sol,2 sol4 |
  mi2 do4 |
  sol4. sol8 sol,4 |
  do2 do4 |
  sol4. sol8 sol,4 |
  do2.~ |
  do~ |
  do~ |
  do |
  sol,2.~ |
  sol, |
  do2.~ |
  do |
  sol,2 do4 |
  sol,2. |
  do2 sol,4 |
  do2 sol,4 |
}
\keepWithTag #'() \reprise
do4 do' do' |
fa4. sol8 la4 |
sib2 sol4 |
la la fa |
dod2 re4 |
la,2. |
re2 re'4 |
fad2 sol4 |
re4. do8 re4 |
sol,2 sol4 |
do2 do4 |
sol,2 sol4 |
do2 do4 |
sol2 si,4 |
do sol,2 |
do
\reprise
do2.