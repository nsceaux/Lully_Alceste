\key do \major \midiTempo#160
\digitTime\time 3/4 s2.*27
%% Chœur
<<
  \tag #'nuances1 {
    s2 s4\fort s2. s2 s4\doux s2. s2 s4\fort s2.*2 s2 s4\doux s2. s2 s4\fort s2.*2
    s2 s4\doux s2.
  }
  s2.*14
>>
<<
  \tag #'nuances2 {
    s4. s\fort s2. s2 s4\doux
    s4. s\fort s2 s4\doux
    s4. s\fort s2 s4\doux
    s4. s\fort s2 s4\doux
    s4. s\fort s2. s4 s2\doux
  }
  s2.*12
>>
s2.*15
<<
  \tag #'nuances1 {
    s2 s4\fort s2. s2 s4\doux s2. s2 s4\fort s2.*2 s2 s4\doux s2. s2 s4\fort s2.*2
    s2 s4\doux s2.
  }
  s2.*14
>>
<<
  \tag #'nuances2 {
    s4. s\fort s2. s2 s4\doux
    s4. s\fort s2 s4\doux
    s4. s\fort s2 s4\doux
    s4. s\fort s2 s4\doux
    s4. s\fort s2. s4 s2\doux
  }
  s2.*12
>>
s2. \bar "|."
