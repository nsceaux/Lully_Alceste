\clef "vtaille" R2.*27 |
\setMusic #'reprise {
  r4 r do' |
  do'4. do'8 si4 |
  do' do' do' |
  do'4. do'8 si4 |
  do' do' do' |
  re'2 re'4 |
  do'4. sol8 do'4 |
  si2 re'4 |
  do'4. sol8 do'4 |
  si2 si4 |
  do'2 do'4 |
  do'4. do'8 si4 |
  do'2 do'4 |
  do'4. do'8 si4 |
  do'2 r4 |
}
\keepWithTag #'() \reprise
R2.*11 |
r4 do' do' |
do'4. do'8 do'4 |
sib2 sib4 |
la la la |
la4. la8 la4 |
la4. la8 la4 |
la2 re'4 |
re'4. re'8 sol4 |
la4. la8 la4 |
si2 r4 |
R2.*5 |
\reprise
R2.*12
