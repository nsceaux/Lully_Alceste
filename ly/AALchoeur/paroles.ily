\tag #'(gloire basse) {
  Que tout re -- ten -- tis -- se,
  que tout re -- ten -- tis -- se :
  que tout ré -- ponde à nos voix :
}
\tag #'nymphe-seine {
  Que tout re -- ten -- tis -- se :
  que tout ré -- ponde à nos voix :
}
\tag #'(nymphe-thuilleries basse) {
  Que tout fleu -- ris -- se
  dans nos Jar -- dins & dans nos Bois.
}
\tag #'(nymphe-marne basse) {
  Que le chant des oy -- seaux s’u -- nis -- se
  a -- vec le doux son des haut -- bois.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Que tout re -- ten -- tis -- se,
  que tout re -- ten -- tis -- se,
  que tout ré -- ponde à nos voix,
  ré -- ponde à nos voix,
  que tout ré -- ponde à nos voix,
  ré -- ponde à nos voix.
  Que le chant des oy -- seaux s’u -- nis -- se
  a -- vec le doux son des haut -- bois
  a -- vec le doux son des haut -- bois.
  Que tout re -- ten -- tis -- se
  que tout re -- ten -- tis -- se
  que tout ré -- ponde à nos voix,
  ré -- ponde à nos voix,
  que tout ré -- ponde à nos voix,
  ré -- ponde à nos voix.
}
