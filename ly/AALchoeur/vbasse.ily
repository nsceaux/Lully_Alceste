\clef "vbasse" R2.*27 |
\setMusic #'reprise {
  r4 r do' |
  do'4. do'8 sol4 |
  do' do' do' |
  do'4. do'8 sol4 |
  do'4 do' do |
  sol2 sol4 |
  do4. si,8 do4 |
  sol,2 sol4 |
  do4. si,8 do4 |
  sol,2 sol4 |
  mi2 do4 |
  sol4. sol8 sol,4 |
  do2 do4 |
  sol4. sol8 sol,4 |
  do2 r4 |
}
\keepWithTag #'() \reprise
R2.*11 |
r4 do' do' |
fa4. sol8 la4 |
sib2 sol4 |
la la fa |
dod4. dod8 re4 |
la,4. sol,8 la,4 |
re2 re'4 |
fad4. fad8 sol4 |
re4. do8 re4 |
sol,2 r4 |
R2.*5 |
\reprise
R2.*12
