\clef "taille" R2.*27 |
r4 r
\setMusic #'reprise {
  sol'4 |
  sol'4. sol'8 sol'4 |
  sol' sol' sol' |
  sol'4. sol'8 sol'4 |
  sol' sol' mi' |
  re'2 re'4 |
  mi'4. re'8 do'4 |
  si2 re'4 |
  mi'4. re'8 do'4 |
  si2 sol'4 |
  sol'2 sol'4 |
  sol'4. sol'8 sol'4 |
  sol'2 sol'4 |
  sol'4. sol'8 sol'4 |
  sol' r8 mi' mi' mi' |
  mi'16 re' mi' fa' mi'8 mi' mi'16 fa' sol' la' |
  sol'8. fa'16 mi'4 mi'16 fa' sol' la' |
  sol'8. fa'16 mi'8 mi'16 fa' sol' fa' sol' la' |
  sol'8. sol'16 sol'4 sol'8. sol'16 |
  sol'4 r8 sol' sol' sol' |
  sol'8. sol'16 sol'4 sol'8. sol'16 |
  sol'4 r8 mi'16 fa' sol' fa' mi' fa' |
  re'4 r4 sol'16 fa' mi' fa' |
  re'4 r8 sol' sol'8. sol'16 |
  sol'4 sol'8 sol' sol'8.\trill fa'16 |
  mi'4 sol'8. sol'16 sol'8.\trill fa'16 |
}
\keepWithTag #'() \reprise
mi'4 sol' sol' |
la'4. la'8 la'4 |
fa'2 sol'4 |
mi' mi' fa' |
sol'4. sol'8 fa'4 |
mi'4. fa'8 sol'4 |
fad'2 fad'4 |
re'4. re'8 re'4 |
re'4. re'8 re'4 |
re'2 r4 |
R2.*5 |
r4 r
\reprise
mi'2. |
