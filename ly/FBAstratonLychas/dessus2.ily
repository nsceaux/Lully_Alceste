\clef "dessus" R1*7 R2. R1*4 R2.*54 |
r4 sib' do'' |
la' la' sib' |
sol' sol' la' |
la' fad' sol' |
sol'8. la'16 fad'4. sol'8 |
sol'4
