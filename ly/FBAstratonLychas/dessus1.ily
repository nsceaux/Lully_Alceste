\clef "dessus" R1*7 R2. R1*4 R2.*54 |
r4 re''4^"Violons" mib'' |
mib''4 re''4. re''8 |
re''4 do''4. do''8 |
do''4 la' sib'~ |
sib'8. do''16 la'4.\trill sol'8 |
sol'4
