<<
  \tag #'(recit basse) {
    \clef "vbasse" <>^\markup\character Straton
    r2 sib8 sib16 sib sib8. re'16 |
    sol4. sol8 sol8. fa16 fa8. mi16 |
    mi4\trill mi r8 sol16 sol do'8 do'16 do' |
    la4.\trill la8 mi8. fad16 sol8. la16 |
    fad2 re'4. fad8 |
    sol4 sol8. la16 sib4 sol8. sol16 |
    sol4 fa8[ mi16] fa mi8\trill mi do'8. mi16 |
    fad4 sol8. sol16 sol8. fad16 |
    sol4
    \fclef "vhaute-contre" <>^\markup\character Lychas
    r8 sol'16 sol' re'8.\trill re'16 re'8 re'16 mi' |
    fa'8 fa' r fa' sib8. sib16 sib8. re'16 |
    sol4 r8 sol do'8. do'16 do'8. do'16 |
    la4\trill la8 la re'8. la16 sib8. do'16 |
    sib4\trill sol sol'8. sol'16 |
    sol'2 fa'4 |
    mi'2\trill mi'8. fa'16 |
    re'2 fa'4 |
    fa' mib'4. mib'8 |
    mib'4 re'4. mib'8 |
    do'[\trill sib do' re' mib' do']( |
    re'4) sib re'8. mi'16 |
    fa'2 fa'4 |
    fa' sol'8[ fa'] sol'[ mi'] |
    fa'2 la4 |
    si2 si8. si16 |
    dod'2 dod'8 dod' |
    re'[\melisma dod' re' mi' re' mi'] |
    dod'[ re' mi' fa' sol' mi']( |
    fa'4)\melismaEnd re' fa'8. fa'16 |
    fa'2 mib'4 |
    re'2\trill re'8. mib'16 |
    do'2 mib'4 |
    re'2 re'8. re'16 |
    sol'4 sol'4. la'8 |
    fad'[ mi' re' do' sib\trill la]( |
    sib4) sol mib' |
    mib' re'4. re'8 |
    re'4 do'4. do'8 |
    do'4 do'8[ la] sib4 |
    sib( la2)\trill |
    sol2 sol'8. sol'16 |
    sol'2 fa'4 |
    mi'2\trill mi'8. fa'16 |
    re'2 fa'4 |
    fa' mib'4. mib'8 |
    mib'4 re'4. mib'8 |
    do'8[\trill sib do' re' mib' do']( |
    re'4) sib re'8. mi'16 |
    fa'2 fa'4 |
    fa' sol'8[ fa'] sol'[ mi'] |
    fa'2 la4 |
    si2 si8. si16 |
    dod'2 dod'8 dod' |
    re'[\melisma dod' re' mi' re' mi'] |
    dod'[ re' mi' fa' sol' mi']( |
    fa'4)\melismaEnd re' fa'8. fa'16 |
    fa'2 mib'4 |
    re'2\trill re'8. mib'16 |
    do'2 mib'4 |
    re'2 re'8. re'16 |
    sol'4 sol'4. la'8 |
    fad'8[ mi' re' do' sib\trill la]( |
    sib4) sol mib' |
    mib' re'4. re'8 |
    re'4 do'4. do'8 |
    do'4 sib8[ la] sib4 |
    sib( la2)\trill |
    sol4 r r |
    R2.*4 |
    r4
  }
  \tag #'recit2 {
    \clef "vbasse" R1*7 R2. R1*4 R2.*27 |
    r4 r <>^\markup\character Straton sol8. sol16 |
    la2 re4 |
    la,2 la,8. la,16 |
    re2 re'4 |
    si4 do'4. sib?8 |
    la4 sib mib |
    fa8[\melisma sol la sib do' la] |
    sib[ la sib do' sib do'] |
    la[ sol la sib la sib] |
    sol[ fa sol la sol la]( |
    fa4)\melismaEnd fa fa8. fa16 |
    sol2 sol4 |
    la la4. la8 |
    sib4 sib sol |
    la2 la,8 la, |
    re2 re4 |
    sol sol do |
    sol,2 sol,4 |
    do do4. do8 |
    sol4 sol fa |
    mib8[\melisma re mib fa mib fa] |
    re[ mi fad mi fad re]( |
    sol4)\melismaEnd sol sol |
    fa fa4. fa8 |
    mib4 mib4. mib8 |
    re4 re do |
    re2. |
    sol,4 r r |
    R2.*4 |
    r4
  }
>>