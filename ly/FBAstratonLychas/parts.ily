\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:system-count 2)
   (basse-continue #:score "score-basse-continue")
   (silence #:on-the-fly-markup , #{ \markup\tacet#71 #}))
