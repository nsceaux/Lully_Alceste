\clef "basse"
<<
  \tag #'basse {
    R1*7 R2. R1*4 R2.*54
  }
  \tag #'basse-continue {
    sol,2 sol |
    mib si, |
    do1 |
    dod |
    re |
    sib,4. la,8 sol,2 |
    la,~ la,4. la,8 |
    re4 mib8 do re re, |
    sol,2 sol |
    re1 |
    mib2 mi |
    fa fad |
    sol2 sol4 |
    la2 re4 |
    la,2. |
    re2 re4 |
    sol do2 |
    fa4 sib, mib |
    fa2 fa,4 |
    sib,2 sib4 |
    la2. |
    sol |
    fa2 fa4 |
    sol2 sol4 |
    la2 la4 |
    sib2 sol4 |
    la2 la,4 |
    re2 re4 |
    sol2 do4 |
    sol,2. |
    do2 do4 |
    sol2 fa4 |
    mib2. |
    re | \allowPageTurn
    sol,4 sol2 |
    fa fa4 |
    mib2 mib4 |
    re2 do4 |
    re re,2 |
    sol, sol4 |
    la2 re4 |
    la,2. |
    re2 re4 |
    sol do2 |
    fa4 sib, mib |
    fa2 fa,4 |
    sib,2 sib4 |
    la2. |
    sol |
    fa2 fa4 |
    sol2 sol4 |
    la2 la4 |
    sib2 sol4 |
    la2 la,4 |
    re2 re4 |
    sol2 do4 |
    sol,2. |
    do2 do4 |
    sol2 fa4 |
    mib2. |
    re |
    sol,4 sol2 |
    fa fa4 |
    mib2 mib4 |
    re2 do4 |
    re re,2 |
  }
>>
sol8 fa sol la sol la |
fa mi fa sol fa sol |
mib re mib fa mib fa |
re2 do4 |
re re,2 |
\custosNote sol,4
