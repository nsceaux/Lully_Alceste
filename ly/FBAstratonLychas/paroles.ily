\tag #'(recit basse) {
  Ne m’os -- te -- ras- tu point la chai -- ne qui m’ac -- ca -- ble,
  dans ce jour des -- ti -- né pour tant d’ai -- ma -- bles jeux ?
  Ah ! qu’il est ri -- gou -- reux
  d’es -- tre seul mi -- se -- ra -- ble
  quand on voit tout le monde heu -- reux !
  
  Au -- jour -- d’huy qu’Al -- ci -- de ra -- mei -- ne
  Al -- ces -- te des en -- fers,
  je veux fi -- nir ta pei -- ne,
  je veux fi -- nir ta pei -- ne.
  Qu’on ne por -- te plus d’au -- tres fers
  que ceux dont l’a -- mour nous en -- chai -- ne.
  Qu’on ne por -- te plus d’au -- tres fers
  que ceux dont l’a -- mour nous en -- chai -- ne.
  Qu’on ne por -- te plus d’au -- tres fers
  que ceux dont l’a -- mour nous en -- chai -- ne,
  que ceux dont l’a -- mour dont l’a -- mour nous en -- chai -- ne.
  
  Qu’on ne por -- te plus d’au -- tres fers
  que ceux dont l’a -- mour nous en -- chai -- ne.
  Qu’on ne por -- te plus d’au -- tres fers
  que ceux dont l’a -- mour nous en -- chai -- ne.
  Qu’on ne por -- te plus d’au -- tres fers
  que ceux dont l’a -- mour nous en -- chai -- ne,
  que ceux dont l’a -- mour dont l’a -- mour nous en -- chai -- ne.
}
\tag #'recit2 {
  Qu’on ne por -- te plus d’au -- tres fers
  que ceux dont l’a -- mour nous en -- chai -- ne.
  Qu’on ne por -- te plus d’au -- tres fers d’au -- tres fers
  qu’on ne por -- te plus d’au -- tres fers
  que ceux dont l’a -- mour nous en -- chai -- ne,
  que ceux dont l’a -- mour dont l’a -- mour nous en -- chai -- ne.
}
