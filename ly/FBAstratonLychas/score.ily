\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'recit2 \includeNotes "voix"
    >> \keepWithTag #'recit2 \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\break
        s1*2\break s1*2 s2.\break s1*2\break s1*2\break
        s2.*6\break s2.*5\pageBreak
        s2.*5\break s2.*5\break s2.*6\break s2.*5\break s2.*5\pageBreak
        s2.*5\break s2.*5\break s2.*5\break
      }
    >>
  >>
  \layout { }
  \midi { }
}