\score {
  \new StaffGroup <<
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "timbales"
    >>
    \new Staff <<
      \global \keepWithTag #'basse \includeNotes "basse"
    >>
  >>
  \layout { }
}
