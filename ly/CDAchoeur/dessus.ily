\clef "dessus" R1*2 R2. R1*3 R2.*2 R1*3 R2.*6 |
r4 sol'' sol'' |
fa''4 fa'' fa'' |
mi''2 sol''4 |
sol''2 sol''4 |
la''2 re''4 |
r sol'' sol'' |
mi''2 mi''4 |
do''2 re''4 |
si'2 la'4 |
r la'' la'' |
fad'' fad'' fad'' |
sol''2 re''4 |
mi''2 mi''4 |
re''2 do''4 |
sol''8 fa'' mi'' re'' mi'' do'' |
re''4 sol' sol''8 fa'' |
mi''4 do''8 re'' mi''4 |
re'' sol' sol''8 fa'' |
mi''4 do''8 re'' mi'' do'' |
re''4 re'' re'' |
mi''2 mi''4 |
fa''2 fa''4 |
re''2 mi''4 |
dod''2 re''4 |
r fad'' fad'' |
sol''2 re''4 |
mi''2 do''4 |
sol''2 mi''4 |
r mi'' mi'' |
fa''4 fa'' fa'' |
re''2 sol''4 |
mi''2 fa''4 |
re''2 do''4 |
do''8 re'' mi'' fa'' sol'' la'' |
sol''4 re'' sol''8 fa'' |
mi''4 do''8 re'' mi'' fa'' |
sol''2 sol''4~ |
sol'' sol''4. fa''8 |
mi''2 r4 |
R2.*6 R1*2 R2.*6 |
r4 r8 re'' re''4. re''8 |
mi''4. mi''8 mi''4. mi''8 |
fa''4. fa''8 fa''4. fa''8 |
mi''4. mi''8 mi''4. mi''8 |
re''4 sol''8 sol'' sol''4. sol''8 |
sol''4. do'''8 sol''4. sol''8 |
sol''4. re''8 sol''4. re''8 |
mi''4. do''8 do'' re'' mi'' fa'' |
sol''4. re''8 re''4. re''8 |
mi''4. mi''8 mi''4. mi''8 |
do''4. do''8 do''4. do''8 |
fa''4. fa''8 fa''4. fa''8 |
mi''4. mi''8 la''4. la''8 |
sold''4. sold''8 sold''4. sold''8 |
la''4. la''8 mi''4. mi''8 |
fad''4. fad''8 fad''4. fad''8 |
sol''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
re''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
%%
re''2. re''8 re'' |
mi''4 mi''8 mi'' mi''8. mi''16 |
fa''4 fa''8 fa'' fa'' fa'' |
re''2\trill re''4. re''8 |
re''4. re''8 re''4 dod'' |
re''4 re'' re'' la' |
si'2 si'4 re'' |
do''4. do''8 la'4. re''8 |
si'4. re''8 re''4. re''8 |
mi''4. mi''8 mi''4. mi''8 |
fa''4. fa''8 fa''4. fa''8 |
mi''4. mi''8 mi''4. mi''8 |
re''4 sol''8 sol'' sol''4. sol''8 |
sol''4. do'''8 sol''4. sol''8 |
sol''4. re''8 sol''4. re''8 |
mi''4. do''8 do'' re'' mi'' fa'' |
sol''4. re''8 re''4. re''8 |
mi''4. mi''8 mi''4. mi''8 |
do''4. do''8 do''4. do''8 |
fa''4. fa''8 fa''4. fa''8 |
mi''4. mi''8 la''4. la''8 |
sold''4. sold''8 sold''4. sold''8 |
la''4. la''8 mi''4. mi''8 |
fad''4. fad''8 fad''4. fad''8 |
sol''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
re''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
re''2 re''8 re'' re'' re'' |
re''4 re''8 re'' mi''4 mi''8 mi'' |
fa'' fa'' fa'' fa'' fa'' fa'' |
mi''4 mi'' mi''8 mi'' |
re''4 re''8 re'' re'' re'' |
re''4. re''8 re''4. re''8 |
mi''4. mi''8 mi''4. mi''8 |
fa''4. fa''8 fa''4. fa''8 |
mi''4. mi''8 mi''4. mi''8 |
re''4 sol''8 sol'' sol''4. sol''8 |
sol''4. do'''8 sol''4. sol''8 |
sol''4. re''8 sol''4. re''8 |
mi''4. do''8 do'' re'' mi'' fa'' |
sol''4. re''8 re''4. re''8 |
mi''4. mi''8 mi''4. mi''8 |
do''4. do''8 do''4. do''8 |
fa''4. fa''8 fa''4. fa''8 |
mi''4. mi''8 la''4. la''8 |
sold''4. sold''8 sold''4. sold''8 |
la''4. la''8 mi''4. mi''8 |
fad''4. fad''8 fad''4. fad''8 |
sol''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
re''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
%%
re''4 sol''8 |
sol'' sol'' fa'' |
fa'' fa'' fa'' |
mi''4 mi''8 |
mi'' mi'' mi'' |
do''4 re''8 |
si'4 mi''8 |
dod''4 la''8 |
la'' la'' sol'' |
sol'' sol'' sol'' |
fad''4 fad''8 |
fad'' fad'' fad'' |
sol''4 sol''8 |
la''4 la''8 |
re''4 sol''8 |
sol'' sol'' fa'' |
fa'' fa'' fa'' |
mi''4 mi''8 |
mi'' mi'' mi'' |
do''4. |
do''8 do'' si' |
do'' do''16 re'' mi'' do'' |
re''8 sol' sol''16 fa'' |
mi'' fa'' re''4\trill |
do'' r8 |
R4.*10 |
r8 r sol'' |
sol'' sol'' fa'' |
fa'' fa'' fa'' |
mi''4 mi''8 |
mi'' mi'' mi'' |
do''4 re''8 |
si'4 mi''8 |
dod''4 la''8 |
la'' la'' sol'' |
sol'' sol'' sol'' |
fad''4 fad''8 |
fad'' fad'' fad'' |
sol''4 sol''8 |
la''4 la''8 |
re''4 sol''8 |
sol'' sol'' fa'' |
fa'' fa'' fa'' |
mi''4 mi''8 |
mi'' mi'' mi'' |
do''4. |
do''8 do'' si' |
do''4. |
