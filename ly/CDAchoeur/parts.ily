\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille)
   (quinte)
   (basse)
   (basse-continue #:score-template "score-basse-voix")
   (timbales #:music , #{ s1*2 s2. s1*3 s2.*2 s1*3 s2.*51
s1*2 s2.*6 s1*21 s2.*2 s1*5\pageTurn #})
   (trompette)
   (silence #:on-the-fly-markup , #{ \markup\tacet#200 #}))
