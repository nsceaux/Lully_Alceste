\clef "basse" R1*2 R2. R1*3 R2.*2 R1*3 R2.*20 |
do4. do16 do do4 |
sol,4. sol,16 sol, sol,4 |
do4. do16 do do4 |
sol,4. sol,16 sol, sol,4 |
do4. do16 do do4 |
sol, r r |
R2.*13 |
do4. do16 do do4 |
sol,4. sol,16 sol, sol,4 |
do4. do16 do do4 |
sol,2 do4~ |
do sol,4. do8 |
do2 r4 |
R2.*6 R1*2 R2.*6 R1*4 |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4. sol,8 |
do4. do16 do do4. do8 |
sol,2 r |
R1*7 |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
%%
sol,2. r4 |
R2.*2 R1*5
R1*4 |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4. sol,8 |
do4. do16 do do4. do8 |
sol,2 r |
R1*7 |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,2 r |
R1 R2.*3 |
R1*4 |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4. sol,8 |
do4. do16 do do4. do8 |
sol,2 r |
R1*7 |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
sol,4. sol,16 sol, sol,4 sol, |
do4. do16 do do4 do |
%%
sol,4 r8 |
R4.*20 |
do8 do16 do do8 |
sol,8 sol,16 sol, sol,8 |
do8 sol,4 |
do4 r8 |
R4.*32 |
