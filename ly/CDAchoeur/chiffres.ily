s1*2
s2 <7>8 <6> <_+>2 \new FiguredBass { <6- 4>4 <5 _+> }

s2 <6> s1 s2.
<6> <_+>4\figExtOn <_+>\figExtOff
<6 4+ 3>8 <6 5_-> \new FiguredBass { <4> <_+> } <_+>1
<_+>2. <4>8 <_+> s2.
s2.*3 <6>2. s

s2. <4 2>4 <5/>2 s2.*2 <6>2. s
s s2 <6 5>4 <_+>2 <_+>4 <_+>2. <_+>2\figExtOn <_+>4\figExtOff s2.
<6> s2.*6
s2.*3 s2 <6 5 _->4 <_+>2 <_+>4 <_+>2.

s2.*2 <6>2. s2.*3
s2 <6 5>4 s2.*6
s2.*2 <6 4 2>2 <6 5/>4 s2.*2 <6 5/>2.
s2. s1 <6> s2.

<_+>2. <6> s <6> s
s1*4
s1*5
s1*4

<_+>4.\figExtOn <_+>8\figExtOff <_+>4.\figExtOn <_+>8\figExtOff <_+>4.\figExtOn <_+>8\figExtOff s2 <_+>4.\figExtOn <_+>8\figExtOff s2 s1
s1*4 s2.
s s2. <6 5 _->4 <4>4.\figExtOn <4>8\figExtOff <4>4. <_+>8 <_+>2\figExtOn <_+>\figExtOff
s2. <6>4 <6 5>2 <4>4 <_+> s1*2

s1*5
s1*4
s1*2 <_+>2\figExtOn <_+>\figExtOff <_+>\figExtOn <_+>\figExtOff
<_+>1 s1*4

s1*2 s2.
s2. s2 s8 <_+> s1*10

s1*3
<_+>4.\figExtOn <_+>8\figExtOff <_+>4.\figExtOn <_+>8\figExtOff <_+>4.\figExtOn <_+>8\figExtOff <_+>4.\figExtOn <_+>8\figExtOff <_+>4.\figExtOn <_+>8\figExtOff <_+>4.\figExtOn <_+>8\figExtOff
s1*4 s4. <7>4 <6>8
<6 5/>4. s4.*3 <4>4 <_+>8 <_+>4\figExtOn <_+>8\figExtOff

<7>4 <6>8 <6 5/>4. <_+> <_+>4\figExtOn <_+>8\figExtOff <7>4 <6>8 <6 5/>4.
s4. <7>4 <6>8 <6 5/>4. s <6> <6 5>
<4>4 <3>8 s4.*9 s8 <6 4+> <6+> <_+>4.

<_+>4. s <6> s <7>4 <6>8 <6 5/>4.
s4.*3 <4>4 <_+>8 <_+>4\figExtOn <_+>8\figExtOff <7>4 <6>8 <6 5/>4\figExtOn <6>8\figExtOff
<_+>4. <_+>4\figExtOn <_+>8\figExtOff <7>4 <6>8 <6 5/>4. s <7>4 <6>8 <6 5/>4.
s <6> <6 5> <4>4 <3>8
