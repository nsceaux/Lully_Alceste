
\clef "dessus" R1*2 R2. R1*3 R2.*2 R1*3 R2.*6 |
R2.*14

sol''8 fa'' mi'' re'' mi'' do'' |
re''4 sol' sol''8 fa'' |
mi''4 do''8 re'' mi''4 |
re'' sol' sol''8 fa'' |
mi''4 do''8 re'' mi'' do'' |
re''4 r r |
R2.*13 |

do''8 re'' mi'' fa'' sol'' la'' |
sol''4 re'' sol''8 fa'' |
mi''4 do''8 re'' mi'' fa'' |
sol''2 sol''4~ |
sol'' sol''4. fa''8 |
mi''2 r4 |
R2.*6 R1*2 R2.*6 |

R1*4
re''4 sol''8 sol'' sol''4. sol''8 |
sol''4. do'''8 sol''4. sol''8 |
sol''4. re''8 sol''4. re''8 |
mi''4. do''8 do'' re'' mi'' fa'' |
sol''2 r |
R1*7

sol''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
re''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
%%
re''2. r4 |
R2.*2
R1*9
re''4 sol''8 sol'' sol''4. sol''8 |
sol''4. do'''8 sol''4. sol''8 |
sol''4. re''8 sol''4. re''8 |
mi''4. do''8 do'' re'' mi'' fa'' |
sol''2 r |
R1*7
sol''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
re''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
re''2 r2 
R1
R2.*3
R1*4
re''4 sol''8 sol'' sol''4. sol''8 |
sol''4. do'''8 sol''4. sol''8 |
sol''4. re''8 sol''4. re''8 |
mi''4. do''8 do'' re'' mi'' fa'' |
sol''2 r
R1*7
sol''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
re''4 sol'' re''4. mi''16 fa'' |
mi''4. do''16 re'' mi''4. re''16 do'' |
%%
R4.*21
do''8 do''16 re'' mi'' do'' |
re''8 sol' sol''16 fa'' |
mi'' fa'' re''4\trill |
do'' r8 |
R4.*10 |
R4.*22

%{
\clef "treble" R1*2 R2. R1*3 R2.*2 R1*3 R2.*20 |
do''4. do''16 do'' do''4 |
sol'4. sol'16 sol' sol'4 |
do''4. do''16 do'' do''4 |
sol'4. sol'16 sol' sol'4 |
do''4. do''16 do'' do''4 |
sol' r r |
R2.*13 |
do''4. do''16 do'' do''4 |
sol'4. sol'16 sol' sol'4 |
do''4. do''16 do'' do''4 |
sol'2 do''4~ |
do'' sol'4. do''8 |
do''2 r4 |
R2.*6 R1*2 R2.*6 R1*4 |
sol'4. sol'16 sol' sol'4 sol' |
do''4. do''16 do'' do''4 do'' |
sol'4. sol'16 sol' sol'4. sol'8 |
do''4. do''16 do'' do''4. do''8 |
sol'2 r |
R1*7 |
sol'4. sol'16 sol' sol'4 sol' |
do''4. do''16 do'' do''4 do'' |
sol'4. sol'16 sol' sol'4 sol' |
do''4. do''16 do'' do''4 do'' |
%%
sol'2. r4 |
R2.*2 R1*5
R1*4 |
sol'4. sol'16 sol' sol'4 sol' |
do''4. do''16 do'' do''4 do'' |
sol'4. sol'16 sol' sol'4. sol'8 |
do''4. do''16 do'' do''4. do''8 |
sol'2 r |
R1*7 |
sol'4. sol'16 sol' sol'4 sol' |
do''4. do''16 do'' do''4 do'' |
sol'4. sol'16 sol' sol'4 sol' |
do''4. do''16 do'' do''4 do'' |
sol'2 r |
R1 R2.*3 |
R1*4 |
sol'4. sol'16 sol' sol'4 sol' |
do''4. do''16 do'' do''4 do'' |
sol'4. sol'16 sol' sol'4. sol'8 |
do''4. do''16 do'' do''4. do''8 |
sol'2 r |
R1*7 |
sol'4. sol'16 sol' sol'4 sol' |
do''4. do''16 do'' do''4 do'' |
sol'4. sol'16 sol' sol'4 sol' |
do''4. do''16 do'' do''4 do'' |
%%
sol'4 r8 |
R4.*20 |
do''8 do''16 do'' do''8 |
sol'8 sol'16 sol' sol'8 |
do''8 sol'4 |
do''4 r8 |
R4.*32 |
%}