\clef "quinte" R1*2 R2. R1*3 R2.*2 R1*3 R2.*6 |
r4 do' do' |
re' re' re' |
mi'2 do'4 |
re'2 re'4 |
re'2 re'4 |
r re' re' |
do'2 do'4 |
la2 la4 |
mi'2 dod'4 |
r mi' mi' |
re' re' re' |
re'2 si4 |
sol2 sol4 |
sol2 sol4 |
do' do'8 do' sol4 |
sol4 re'8 re' re'4 |
do' do'8 do' sol4 |
sol re'8 re' re'4 |
do' do'8 do' sol4 |
sol re' re' |
do' do' do' |
do'2 do'4 |
re'2 re'4 |
la'2 fad'4 |
r re' re' |
si2 re'4 |
do'2 mi'4 |
re'2 do'4 |
r do' do' |
la4 la re' |
re'2 do'4 |
do'2 do'4 |
sol2 sol4 |
sol do'8 do' do'4 |
re' re'8 re' re'4 |
do' do'8 do' do'4 |
re'2 mi'4~ |
mi'4 re'4. do'8 |
do'2 r4 |
R2.*6 R1*2 R2.*6 |
r4 r8 sol8 sol4. sol8 |
do'4. do'8 sol4. do'8 |
fa4. fa8 fa4. fa8 |
sol4. sol8 do'4. do'8 |
re'4 re'8 re' sol'4 re' |
mi'4 mi'8 mi' sol'4 mi' |
re'4. re'8 si4. re'8 |
do'4. mi'8 sol'4. do'8 |
re'4. sol8 sol4. sol8 |
do'4. do'8 do'4. do'8 |
do'4. do'8 do'4. do'8 |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 mi'4. la8 |
si4. si8 si4. si8 |
la4. la8 la4. la8 |
la4. la8 re'4. re'8 |
si4 re' re'4. do'16 si |
do'4. do'8 do'4 re'8 mi' |
re'4. re'8 re'4. do'16 si |
do'4. do'8 do'4. re'16 mi' |
%%
\sugNotes {
  re'2. re'8 re' |
  do'4 do'8 do' do'8. do'16 |
  do'4 do'8 do' do' do' |
  re'2 re'4 re' |
  re'4. re'8 re'4 mi' |
  re' re' re' re' |
  si2 si4 re' |
  mi'4. mi'8 la4. la8 |
  sol4.
} sol8 sol4. sol8 |
do'4. do'8 sol4. do'8 |
fa4. fa8 fa4. fa8 |
sol4. sol8 do'4. do'8 |
re'4 re'8 re' sol'4 re' |
mi'4 mi'8 mi' sol'4 mi' |
re'4. re'8 si4. re'8 |
do'4. mi'8 sol'4. do'8 |
re'4. sol8 sol4. sol8 |
do'4. do'8 do'4. do'8 |
do'4. do'8 do'4. do'8 |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 mi'4. la8 |
si4. si8 si4. si8 |
la4. la8 la4. la8 |
la4. la8 re'4. re'8 |
si4 re' re'4. do'16 si |
do'4. do'8 do'4 re'8 mi' |
re'4. re'8 re'4. do'16 si |
do'4. do'8 do'4. re'16 mi' |
re'2 \sugNotes {
  re'8 re' re' re' |
  re'4 re'8 re' do'4 do'8 do' |
  do'8 do' do' do' do' do' |
  do'4 do' do'8 do' |
  re'4 re'8 re' re' re' |
  re'4.
} sol8 sol4. sol8 |
do'4. do'8 sol4. do'8 |
fa4. fa8 fa4. fa8 |
sol4. sol8 do'4. do'8 |
re'4 re'8 re' sol'4 re' |
mi'4 mi'8 mi' sol'4 mi' |
re'4. re'8 si4. re'8 |
do'4. mi'8 sol'4. do'8 |
re'4. sol8 sol4. sol8 |
do'4. do'8 do'4. do'8 |
do'4. do'8 do'4. do'8 |
re'4. re'8 re'4. re'8 |
mi'4. mi'8 mi'4. la8 |
si4. si8 si4. si8 |
la4. la8 la4. la8 |
la4. la8 re'4. re'8 |
si4 re' re'4. do'16 si |
do'4. do'8 do'4 re'8 mi' |
re'4. re'8 re'4. do'16 si |
do'4. do'8 do'4. re'16 mi' |
%%
re'4 re'8 |
do' do' fa' |
re' re' re' do'4 do'8 |
do' sol sol |
la4 la8 |
mi'4 si8 |
dod'4 mi'8 |
re'8 re' sol' |
mi'4 mi'8 |
re'4 re'8 |
re' re' re' |
si4 mi'8 |
re'4 re'8 |
re'4 re'8 |
do' do' fa' |
re' re' re' |
do'4 do'8 |
do' do' do' |
do'4. |
sol8 sol sol |
sol sol16 sol sol8 |
sol sol16 sol sol8 |
sol sol4 |
sol r8 |
R4.*10 |
r8 r re' |
do' do' fa' |
re' re' re' |
do'4 do'8 |
do' sol sol |
la4 la8 |
mi'4 si8 |
dod'4 mi'8 |
re' re' sol' |
mi'4 mi'8 |
re'4 re'8 |
re' re' re' |
si4 mi'8 |
re'4 re'8 |
re'4 re'8 |
do' do' fa' |
re' re' re' |
do'4 do'8 |
do' do' do' |
do'4. |
sol8 sol sol |
sol4. |
