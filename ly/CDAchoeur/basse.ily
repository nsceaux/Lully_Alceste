\clef "basse"
<<
  \tag #'basse { R1*2 R2. R1 }
  \tag #'basse-continue {
    do1 |
    fa, |
    sib,2 mib4 |
    re2 re, |
  }
>>
sol,4 sol mi do |
fa re sol sol, |
do2. |
<<
  \tag #'basse {
    R2. R1*3 R2.*3 |
    r4 r \new CueVoice {
      <>^\markup { Chœur \italic ou Licomède & Straton }
      sol4 |
      mi2 do4 |
      sol2 do4 |
      do'4
    } do'4^\markup Chœur do' |
  }
  \tag #'basse-continue {
    sold,2. |
    la,4 la sib8 sol la la, |
    re1~ |
    re2 mi8 do re re, |
    sol,2. |
    sol |
    do |
    sol,2 sol4 |
    mi2 do4 |
    sol2 do4 |
    do'2 do'4 |
  }
>>
do'4 si si |
do'2 do4 |
sol2 sol4 |
fad2 sol4 |
sol,2. |
do2 do4 |
fa2 re4 |
mi2 la,4 |
la2 la4 |
re2 re4 |
sol2 sol4 |
mi2 do4 |
sol2 do4 |
do2. |
sol, |
do |
sol, |
do |
sol,4 sol sol |
do' do' do' |
fa2 fa4 |
sib2 sol4 |
la2 re4 |
re'2 re'4 |
sol2 sol4 |
do'2 do'4 |
si2 do'4 |
do2 do4 |
fa4 fa re |
sol2 mi4 |
la2 fa4 |
sol2 do4 |
do2. |
sol,2 sol,4 |
do2 do4 |
sol,2 do4~ |
do sol,2 |
do2 <<
  \tag #'basse { r4 R2.*6 R1*2 R2.*6 \allowPageTurn r4 r8 }
  \tag #'basse-continue {
    do4 |
    sol2 sol4 |
    sol2 fad4 |
    sol2 sol4 |
    la2 la4 |
    si2 si4 |
    do'4 do' do |
    sol1 | \allowPageTurn
    mi |
    fa2. |
    re2 mi4 |
    fad2 re4 |
    sol2 sol4 |
    mi mi do |
    fa2 re4 |
    sol4.
  }
>> sol8 sol4. sol8 |
do'4. do8 do4. do8 |
fa4. fa8 fa4. fa8 |
do4. do8 do4. do8 |
sol,4 sol, sol, sol, |
do2 do4 do |
sol,2 sol,4 sol, |
do2 do4 do |
sol,4. sol8 sol4. sol8 |
do'4. do8 do4. do8 |
fa4. fa8 fa4. fa8 |
re4. re8 re4. re8 |
la4. la8 la4. la8 |
mi4. mi8 mi4. mi8 |
la,4. la,8 la,4. la,8 |
re4. re8 re4. re8 |
sol,2 sol,4 sol, |
do2 do4 do |
sol,2 sol,4 sol, |
do2 do4 do |
%%
sol,2. sol8 sol |
do'4 do'8 do' do' do' |
fa4 fa8 fa fa fa |
sib2 sib4 sol |
la4. la8 la4. la,8 |
re4 re re re |
sol2 sol4 si, |
do do re re |
sol,2. sol4 |
do'4. do8 do4. do8 |
fa4. fa8 fa4. fa8 |
do4. do8 do4. do8 |
sol,4 sol, sol, sol, |
do2 do4 do |
sol,2 sol,4 sol, |
do2 do4 do |
sol,2. sol4 |
do'4. do8 do4. do8 |
fa4. fa8 fa4. fa8 |
re4. re8 re4. re8 |
la4. la8 la4. la8 |
mi4. mi8 mi4. mi8 |
la,4. la,8 la,4. la,8 |
re4. re8 re4. re8 |
sol,2 sol,4 sol, |
do2 do4 do |
sol,2 sol,4 sol, |
do2 do4 do |
sol,2 sol8 sol sol sol |
sol4 sol8 sol do'4 do'8 do' |
fa fa fa fa fa fa |
do4 do do8 do |
sol4 sol8 sol sol re |
sol2. sol4 |
do'4. do8 do4. do8 |
fa4. fa8 fa4. fa8 |
do4. do8 do4. do8 |
sol,4 sol, sol, sol, |
do2 do4 do |
sol,2 sol,4 sol, |
do2 do4 do |
sol,2. sol4 |
do'4. do8 do4. do8 |
fa4. fa8 fa4. fa8 |
re4. re8 re4. re8 |
la4. la8 la4. la8 |
mi4. mi8 mi4. mi8 |
la,4. la,8 la,4. la,8 |
re4. re8 re4. re8 |
sol,2 sol,4 sol, |
do2 do4 do |
sol,2 sol,4 sol, |
do2 do4 do |
%%
sol,4 sol8 |
la la la |
si si si |
do'4 do'8 |
do do do |
fa4 re8 |
mi4 mi8 |
la,4 la8 |
si si si |
dod' dod' dod' |
re'4 re'8 |
re re re |
mi4 mi8 |
fad4 fad8 |
sol4 sol8 |
la la la |
si si si |
do'4 do'8 |
mi mi mi |
fa4. |
sol8 sol sol, |
do do16 do do8 |
sol,8 sol,16 sol, sol,8 |
do8 sol,4 |
<<
  \tag #'basse { do4 r8 | \allowPageTurn R4.*10 | r8 r }
  \tag #'basse-continue {
    do4.~ |
    do~ |
    do |
    fa, |
    fa~ |
    fa~ |
    fa4 mi8 |
    re4.~ |
    re4 mi8 |
    fad4.~ |
    fad |
    sol4
  }
>> sol8 |
la la la |
si si si |
do'4 do'8 |
do8 do do |
fa4 re8 |
mi4 mi8 |
la,4 la8 |
si si si |
dod' dod' dod' |
re'4 re'8 |
re re re |
mi4 mi8 |
fad4 fad8 |
sol4 sol8 |
la la la |
si si si |
do'4 do'8 |
mi8 mi mi |
fa4. |
sol8 sol sol, |
do4. |
