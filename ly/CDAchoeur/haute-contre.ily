\clef "haute-contre" R1*2 R2. R1*3 R2.*2 R1*3 R2.*6 |
r4 mi'' mi'' |
re'' re'' re'' |
do''2 mi''4 |
re''2 re''4 |
re''2 si'4 |
r si' si' |
do''2 sol'4 |
la'2 la'4 |
sold'2 la'4 |
r dod'' dod'' |
re'' re'' la' |
si'2 si'4 |
do''2 do''4 |
si'2 do''4 |
do'' sol'8 sol' sol'4 |
sol'4 sol'8 sol' sol'4 |
sol' sol'8 sol' sol'4 |
sol' sol'8 sol' sol'4 |
sol' sol'8 sol' do''4 |
si' si' si' |
do''2 do''4 |
do''2 do''4 |
sib'2 sib'4 |
la'2 la'4 |
r re'' re'' |
re''2 si'4 |
do''2 do''4 |
re''2 sol'4 |
r do'' do'' |
do'' do'' re'' |
si'2 mi''4 |
do''2 do''4 |
si'2 do''4 |
do'' do''8 do'' do''4 |
si' si' si'8 si' |
do''4 do''8 do'' do''4 |
si'2 do''4~ |
do'' si'4. do''8 |
do''2 r4 |
R2.*6 R1*2 R2.*6 |
r4 r8 si' si'4. si'8 |
do''4. do''8 do''4. do''8 |
do''4. do''8 do''4. do''8 |
do''4. do''8 do''4. do''8 |
si'4 si'8 do'' re''4. si'8 |
do''4 do''8 re'' mi''4 do'' |
si'4. si'8 si'4. si'8 |
do''4. do''8 do''4. do''8 |
si'4. si'8 si'4. si'8 |
do''4. do''8 sol'4. sol'8 |
la'4. la'8 la'4. la'8 |
re''4. re''8 re''4. re''8 |
do''4. do''8 do''4. do''8 |
si'4. si'8 si'4. si'8 |
dod''4. dod''8 dod''4. dod''8 |
re''4. re''8 re''4. re''8 |
re''4 re'' si'4. do''16 re'' |
do''4. sol'8 do''4. si'16 la' |
si'4 si' si'4. do''16 re'' |
do''4 sol' do''4. si'16 la' |
%%
\sugNotes {
  si'2. si'8 si' |
  do''4 do''8 do'' do''8. do''16 |
  do''4 do''8 do'' do'' do'' |
  sib'2 sib'4. sib'8 |
  la'4. la'8 la'4 la' |
  la' la' la' la' |
  sol'2 sol'4 si' |
  la'4. la'8 sol'4. fad'8 |
  sol'4.
} si'8 si'4. si'8 |
do''4. do''8 do''4. do''8 |
do''4. do''8 do''4. do''8 |
do''4. do''8 do''4. do''8 |
si'4 si'8 do'' re''4. si'8 |
do''4 do''8 re'' mi''4 do'' |
si'4. si'8 si'4. si'8 |
do''4. do''8 do''4. do''8 |
si'4. si'8 si'4. si'8 |
do''4. do''8 sol'4. sol'8 |
la'4. la'8 la'4. la'8 |
re''4. re''8 re''4. re''8 |
do''4. do''8 do''4. do''8 |
si'4. si'8 si'4. si'8 |
dod''4. dod''8 dod''4. dod''8 |
re''4. re''8 re''4. re''8 |
re''4 re'' si'4. do''16 re'' |
do''4. sol'8 do''4. si'16 la' |
si'4 si' si'4. do''16 re'' |
do''4 sol' do''4. si'16 la' |
si'2 \sugNotes {
  si'8 si' si' si' |
  si'4 si'8 si' do''4 do''8 do'' |
  do''8 do'' do'' do'' do'' do'' |
  do''4 do'' do''8 do'' |
  si'4 si'8 si' si' la' |
  si'4.
} si'8 si'4. si'8 |
do''4. do''8 do''4. do''8 |
do''4. do''8 do''4. do''8 |
do''4. do''8 do''4. do''8 |
si'4 si'8 do'' re''4. si'8 |
do''4 do''8 re'' mi''4 do'' |
si'4. si'8 si'4. si'8 |
do''4. do''8 do''4. do''8 |
si'4. si'8 si'4. si'8 |
do''4. do''8 sol'4. sol'8 |
la'4. la'8 la'4. la'8 |
re''4. re''8 re''4. re''8 |
do''4. do''8 do''4. do''8 |
si'4. si'8 si'4. si'8 |
dod''4. dod''8 dod''4. dod''8 |
re''4. re''8 re''4. re''8 |
re''4 re'' si'4. do''16 re'' |
do''4. sol'8 do''4. si'16 la' |
si'4 si' si'4. do''16 re'' |
do''4 sol' do''4. si'16 la' |
%%
si'4 si'8 |
do'' do'' do'' |
re'' re'' re'' |
sol'4 sol'8 |
do'' do'' do'' |
la'4 la'8 |
la'4 sold'8 |
la'4 dod''8 |
re'' re'' re'' |
mi'' mi'' mi'' |
la'4 la'8 |
re'' re'' re'' |
re''4 do''8 |
do''4 do''8 |
si'4 si'8 |
do'' do'' do'' |
re''4 re''8 |
sol'4 sol'8 |
do'' do'' do'' |
la'4 la'8 |
sol' sol' sol' |
sol' sol' sol' |
sol' sol' si' |
do''16 re'' si'4\trill |
do''4 r8 |
R4.*10 |
r8 r si' |
do'' do'' do'' |
re'' re'' re'' |
sol'4 sol'8 |
do'' do'' do'' |
la'4 la'8 |
la'4 sold'8 |
la'4 dod''8 |
re'' re'' re'' |
mi''4 mi''8 |
re''4 la'8 |
re'' re'' re'' |
re''4 do''8 |
do''4 do''8 |
si'4 si'8 |
do'' do'' do'' |
re''4 re''8 |
sol'4 sol'8 |
do'' do'' do'' |
la'4. |
sol'8 sol' sol' |
sol'4. |
