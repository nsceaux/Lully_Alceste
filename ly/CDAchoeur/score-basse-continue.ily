\score {
  \new ChoirStaff <<
    \new Staff \with { \tinyStaff \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'basse \includeNotes "voix"
    >> { \set fontSize = #-2
      \keepWithTag #'basse \includeLyrics "paroles" }
    \new Staff \with { \haraKiriFirst } <<
      \global \includeNotes "timbales"
      { s1*2 s2. s1*3 s2.*2 s1*3 s2.*20
        s2.*24 s2 \bar "" \break
        s4 s2.*6 s1*2 s2.*6 s1*21 s2.*2 s1*27 s2.*3 s1*20
        s4. \startHaraKiri }
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
    >>
  >>
  \layout { }
}
