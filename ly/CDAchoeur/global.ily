\key do \major
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*3
\digitTime\time 3/4 s2.*2
\time 4/4 s1*3
\digitTime\time 3/4 \midiTempo#240 s2.*51
\time 2/2 s1*2
\digitTime\time 3/4 s2.*6
\time 2/2 s1*20
%%
s1
\digitTime\time 3/4 s2.*2
\time 2/2 s1*27
\digitTime\time 3/4 s2.*3
\time 2/2 s1*20
%%
\time 3/8 s4.*57 \bar "|."
