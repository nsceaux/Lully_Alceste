<<
  %% Admete
  \tag #'(admete basse) {
    <<
      \tag #'basse { s1*2 s2. s1*3 s2 \ffclef "vtaille" }
      \tag #'admete { \clef "vtaille" R1*2 R2. R1*3 r2 }
    >> <>^\markup\character Admete
    r8 mi' |
    si8\trill si r16 si si do' re'8. mi'16 |
    dod'8 dod'16 mi' mi' mi' mi' fa' re'\trill re' re' re' re'8. dod'16 |
    re'4 re' <<
      \tag #'basse { s2 s1 s2. \ffclef "vtaille" }
      \tag #'admete { r2 R1 R2. }
    >> <>^\markup\character Admete
    r4 re' re' |
    mi' mi' mi' |
    re'2\trill <<
      \tag #'basse { s4 s2.*2 \ffclef "vtaille" }
      \tag #'admete { r4 R2.*2 }
    >>
    R2.*38
    r4 r <>^\markup\character Admete mi' |
    re' re' re' |
    do'2 do'4 |
    si2\trill si4 |
    do' do' do' |
    re'2 re'4 |
    mi' mi' fa' |
    re'2 <<
      \tag #'basse { s2 s1 s2.*3 s2 \ffclef "vtaille" }
      \tag #'admete { r2 R1 R2.*3 r4 r }
    >> <>^\markup\character Admete
    re'4 |
    mi' mi' mi' |
    do'2 fa'4 |
    re'4.\trill r8 r2 R1*19
  }
  %% Alcide
  \tag #'(alcide basse) {
    <<
      \tag #'basse { s1*2 s2. s1*3 s2.*2 s1*3 s2.*51 s2 \ffclef "vbasse-taille" }
      \tag #'alcide {
        \clef "vbasse-taille" R1*2 R2. R1*3 R2.*2 R1*3 R2.
        <>^\markup\character Alcide
        r4 si si |
        do' do' do' |
        si2 r4 |
        R2.*40 |
        \ffclef "vbasse-taille"
        r4 r <>^\markup\character Alcide do' |
        si si si |
        la2 la4 |
        sol2 r4 |
        R2.*3 |
        r2
      }
    >> <>^\markup\character Alcide
    si4 si |
    do' do' do' sol |
    la la la |
    re' re' re' |
    la2 re'4 |
    si2\trill <<
      \tag #'basse {
        s4 s2.*2 s1*20
        s1 s2.*2 s1*27 s2.*3 s1*20
        \ffclef "vbasse-taille" }
      \tag #'alcide {
        si4 |
        do' do' do' |
        la2 re'4 |
        si4.\trill r8 r2 |
        R1*19 |
        %%
        R1 R2.*2 R1*27 R2.*3 R1*20
        %%
      }
    >>
    R4.*24 |
    <>^\markup\character Alcide
    r8 r sol |
    mi\trill mi mi |
    mi fa sol |
    la4 la8 |
    r r do' |
    la la la |
    la si dod' |
    re'4 re'8 |
    re' do' si |
    la4.\trill |
    la8 si do' |
    si4\trill r8 |
    R4.*21
  }
  %% Licomede
  \tag #'(licomede basse) {
    \clef "vbasse" <>^\markup\character Licomede
    r2 do16 do do re mi8\trill fa16 sol |
    la8 la r do' la\trill la16 sol fa8 sol16 la |
    re8 re r16 sib sib la sol sol sol la |
    fad4 re8 re sol4 sol8 fad |
    sol <<
      \tag #'basse { s2.. s1 s2.*2 s1 s2 \ffclef "vbasse" }
      \tag #'licomede { r8 r4 r2 R1 R2.*2 R1 r2 }
    >> <>^\markup\character Licomede
    la16 la la la fad8 fad16 la |
    re8. re'16 re' re' re' fad sol8 sol16 do re8 re16 re |
    sol,2 sol,4 |
    <<
      \tag #'basse { s2.*2 s2 \ffclef "vbasse" <>^\markup\character Licomede }
      \tag #'licomede {
        R2.*2 | r4 r
        <>^\markup\character-text Licomede \tiny "[édition gravée uniquement]"
      }
    >>
    si4 |
    do'2 do'4 |
    si2\trill do'4 |
    \tag #'licomede {
      R2.*38 |
      r4 r <>^\markup\character Licomede do |
      sol sol sol |
      sol2 fad4 |
      sol2 sol4 |
      la la la |
      si2 si4 |
      do' do' do |
      sol2 r |
      R1 R2.*3 |
      r4 r sol |
      mi mi do |
      fa2 re4 |
      sol4. r8 r2 |
      R1*19
    }
  }
  %% Straton
  \tag #'straton {
    \clef "vbasse" R1*2 R2. R1*3 R2.*2 R1*3 R2.*3
    r4 r <>^\markup\character Straton sol4 |
    mi2 do4 |
    sol2 do4 |
    R2.*38 |
  }
  %% Chœur
  \tag #'(vdessus basse) {
    <<
      \tag #'basse { s1*2 s2. s1 s8 \ffclef "vdessus" }
      \tag #'vdessus { \clef "vdessus" R1*2 R2. R1 r8 }
    >> <>^\markup\character Chœur
    re''8 re'' re'' mi'' mi'' mi'' mi'' |
    do''4 do''8 fa'' re''4\trill re''8. sol''16 |
    mi''2\trill
    \tag #'vdessus {
      r4 R2. R1*3 R2.*3
      r4 r <>^\markup\character-text Chœur \tiny "[manuscrit uniquement]"
      re''4 |
      mi''2 mi''4 |
      re''2\trill do''4 |
      <>^\markup\character Chœur
      r4 do'' do'' |
      re'' re'' re'' |
      sol'2 mi''4 |
      re''2 re''4 |
      re''2 si'4 |
      r re'' re'' |
      mi''2 mi''4 |
      do''2 re''4 |
      si'2 la'4 |
      r la' la' |
      la' la' la' |
      si'2 re''4 |
      mi''2 mi''4 |
      re''2 do''4 |
      R2.*5 |
      r4 re'' re'' |
      mi'' mi'' mi'' |
      fa''2 fa''4 |
      re''2 mi''4 |
      dod''2 re''4 |
      r4 la' la' |
      si'2 si'4 |
      do''2 do''4 |
      re''2 sol'4 |
      r mi'' mi'' |
      fa'' fa'' fa'' |
      re''2 sol''4 |
      mi''2 fa''4 |
      re''2\trill do''4 |
      R2.*5
      R2.*7 R1*2 R2.*6
      r4 r8 <>^\markup\character Chœur
      re''8 re''4. re''8 |
      mi''4. mi''8 mi''4. mi''8 |
      fa''4. fa''8 fa''4. fa''8 |
      mi''4. mi''8 mi''4. mi''8 |
      re''4 r r2 |
      R1*3 |
      r4 r8 re'' re''4. re''8 |
      mi''4. mi''8 mi''4. mi''8 |
      do''4. do''8 do''4. do''8 |
      fa''4. fa''8 fa''4. fa''8 |
      mi''4. mi''8 mi''4. mi''8 |
      mi''4. mi''8 mi''4. mi''8 |
      dod''4. dod''8 dod''4. dod''8 |
      re''4. re''8 re''4. re''8 |
      re''4 r r2 |
      R1*3 |
      %%
      r2 r4 re''8 re'' |
      mi''4 mi''8 mi'' mi''8. mi''16 |
      fa''4 fa''8 fa'' fa'' fa'' |
      re''2\trill re''4. re''8 |
      re''4. re''8 re''4 dod'' |
      re''4 re'' re'' la' |
      si'2 si'4 re'' |
      do''4. do''8 la'4. re''8 |
      si'4. re''8 re''4. re''8 |
      mi''4. mi''8 mi''4. mi''8 |
      fa''4. fa''8 fa''4. fa''8 |
      mi''4. mi''8 mi''4. mi''8 |
      re''4 r r2 |
      R1*3 |
      r4 r8 re'' re''4. re''8 |
      mi''4. mi''8 mi''4. mi''8 |
      do''4. do''8 do''4. do''8 |
      fa''4. fa''8 fa''4. fa''8 |
      mi''4. mi''8 mi''4. mi''8 |
      mi''4. mi''8 mi''4. mi''8 |
      dod''4. dod''8 dod''4. dod''8 |
      re''4. re''8 re''4. re''8 |
      re''4 r r2 |
      R1*3 |
      r2 re''8 re'' re'' re'' |
      re''4 re''8 re'' mi''4 mi''8 mi'' |
      fa'' fa'' fa'' fa'' fa'' fa'' |
      mi''4 mi'' mi''8 mi'' |
      re''4 re''8 re'' re'' re'' |
      re''4. re''8 re''4. re''8 |
      mi''4. mi''8 mi''4. mi''8 |
      fa''4. fa''8 fa''4. fa''8 |
      mi''4. mi''8 mi''4. mi''8 |
      re''4 r r2 |
      R1*3 |
      r4 r8 re'' re''4. re''8 |
      mi''4. mi''8 mi''4. mi''8 |
      do''4. do''8 do''4. do''8 |
      fa''4. fa''8 fa''4. fa''8 |
      mi''4. mi''8 mi''4. mi''8 |
      mi''4. mi''8 mi''4. mi''8 |
      dod''4. dod''8 dod''4. dod''8 |
      re''4. re''8 re''4. re''8 |
      re''4 r r2 |
      R1*3 |
      %%
      r8 r sol'' |
      sol'' sol'' fa'' |
      fa'' fa'' fa'' |
      mi''4 mi''8 |
      mi'' mi'' mi'' |
      do''4 re''8 si'4 mi''8 |
      dod''4 dod''8 |
      re'' re'' re'' |
      mi'' mi'' mi'' |
      la'4 la'8 |
      re'' re'' re'' |
      re''4 do''8 |
      do''4 do''8 |
      si'4 sol''8 |
      sol'' sol'' fa'' |
      fa'' fa'' fa'' |
      mi''4 mi''8 |
      mi'' mi'' mi'' |
      do''4. |
      do''8 do'' si' |
      do''4. |
      R4.*2 |
      R4.*11
      r8 r <>^\markup\character Chœur
      sol''8 |
      sol'' sol'' fa'' |
      fa'' fa'' fa'' |
      mi''4 mi''8 |
      mi'' mi'' mi'' |
      do''4 re''8 |
      si'4 mi''8 |
      dod''4 dod''8 |
      re'' re'' re'' |
      mi'' mi'' mi'' |
      la'4 la'8 |
      re'' re'' re'' |
      re''4 do''8 |
      do''4 do''8 |
      si'4 sol''8 |
      sol'' sol'' fa'' |
      fa'' fa'' fa'' |
      mi''4 mi''8 |
      mi'' mi'' mi'' |
      do''4. |
      do''8 do'' si' |
      do''4. |
    }
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre"
    R1*2 R2. R1 |
    r8 sol' sol' sol' sol' sol' sol' sol' |
    fa'4 fa'8 la' sol'4 sol'8 sol' |
    sol'2 r4 |
    R2. R1*3 R2.*3 |
    r4 r sol' |
    sol'2 sol'4 |
    sol'2 mi'4 |
    r4 sol' sol' |
    fa'4 fa' fa' |
    mi'2 sol'4 |
    sol'2 sol'4 |
    la'2 re'4 |
    r sol' sol' |
    sol'2 sol'4 |
    fa'2 fa'4 |
    mi'2 dod'4 |
    r mi' mi' |
    fad' fad' fad' |
    sol'2 sol'4 |
    sol'2 sol'4 |
    sol'2 mi'4 |
    R2.*5 |
    r4 sol' sol' |
    sol' sol' sol' |
    la'2 la'4 |
    fa'2 sol'4 |
    mi'2 re'4 |
    r fad' fad' |
    sol'2 sol'4 |
    sol'2 sol'4 |
    sol'2 mi'4 |
    r sol' sol' |
    la'4 la' la' |
    sol'2 sol'4 |
    la'2 la'4 |
    sol'2 mi'4 |
    R2.*12 R1*2 R2.*6 |
    r4 r8 re'8 sol'4. sol'8 |
    sol'4. sol'8 sol'4. sol'8 |
    la'4. la'8 la'4. la'8 |
    sol'4. sol'8 sol'4. sol'8 |
    sol'4 r r2 |
    R1*3 |
    r4 r8 re' re'4. sol'8 |
    sol'4. sol'8 sol'4. sol'8 |
    fa'4. fa'8 fa'4. fa'8 |
    la'4. la'8 la'4. la'8 |
    la'4. la'8 la'4. la'8 |
    sold'4. sold'8 sold'4. sold'8 |
    la'4. mi'8 mi'4. mi'8 |
    fad'4. fad'8 fad'4. fad'8 |
    sol'4 r r2 |
    R1*3 |
    %%
    r2 r4 \sugNotes {
      sol'8 sol' |
      sol'4 sol'8 sol' sol'8. sol'16 |
      la'4 la'8 la' la' la' |
      fa'2 fa'4. sol'8 |
      fa'4. fa'8 fa'4 sol' |
      fad' fad' fad' fad' |
      sol'2 sol'4 sol' |
      sol'4. sol'8 re'4. re'8 |
      re'4.
    } re'8 sol'4. sol'8 |
    sol'4. sol'8 sol'4. sol'8 |
    la'4. la'8 la'4. la'8 |
    sol'4. sol'8 sol'4. sol'8 |
    sol'4 r r2 |
    R1*3 |
    r4 r8 re' re'4. sol'8 |
    sol'4. sol'8 sol'4. sol'8 |
    fa'4. fa'8 fa'4. fa'8 |
    la'4. la'8 la'4. la'8 |
    la'4. la'8 la'4. la'8 |
    sold'4. sold'8 sold'4. sold'8 |
    la'4. mi'8 mi'4. mi'8 |
    fad'4. fad'8 fad'4. fad'8 |
    sol'4 r r2 |
    R1*3 |
    r2 \sugNotes {
      sol'8 sol' sol' sol' |
      sol'4 sol'8 sol' sol'4 sol'8 sol' |
      la'8 la' la' la' la' la' |
      sol'4 sol' sol'8 sol' |
      sol'4 sol'8 sol' sol' fad' |
      sol'4.
    }re'8 sol'4. sol'8 |
    sol'4. sol'8 sol'4. sol'8 |
    la'4. la'8 la'4. la'8 |
    sol'4. sol'8 sol'4. sol'8 |
    sol'4 r r2 |
    R1*3 |
    r4 r8 re' re'4. sol'8 |
    sol'4. sol'8 sol'4. sol'8 |
    fa'4. fa'8 fa'4. fa'8 |
    la'4. la'8 la'4. la'8 |
    la'4. la'8 la'4. la'8 |
    sold'4. sold'8 sold'4. sold'8 |
    la'4. mi'8 mi'4. mi'8 |
    fad'4. fad'8 fad'4. fad'8 |
    sol'4 r r2 |
    R1*3 |
    %%
    r8 r sol' |
    mi' mi' la' |
    sol' sol' sol' |
    sol'4 sol'8 |
    sol' sol' sol' |
    fa'4 fa'8 |
    mi'4 mi'8 |
    mi'4 la'8 |
    la' la' sol' |
    sol' sol' sol' |
    fad'4 fad'8 |
    fad' fad' fad' |
    sol'4 sol'8 |
    la'4 la'8 |
    re'4 sol'8 |
    mi' mi' la' |
    sol' sol' sol' |
    sol'4 sol'8 |
    sol' sol' sol' |
    fa'4. |
    re'8 re' sol' |
    mi'4. |
    R4.*13 |
    r8 r sol' |
    mi' mi' la' |
    sol' sol' sol' |
    sol'4 sol'8 |
    sol' sol' sol' |
    fa'4 fa'8 |
    mi'4 mi'8 |
    mi'4 la'8 |
    la' la' sol' |
    sol' sol' sol' |
    fad'4 fad'8 |
    fad' fad' fad' |
    sol'4 sol'8 |
    la'4 la'8 |
    sol'4 sol'8 |
    mi' mi' la' |
    sol' sol' sol' |
    sol'4 sol'8 |
    sol' sol' sol' |
    fa'4. |
    re'8 re' sol' |
    mi'4. |
  }
  \tag #'vtaille {
    \clef "vtaille"
    R1*2 R2. R1 |
    r8 si si si do' do' do' do' |
    la4 la8 re' si4 si8 si |
    do'2 r4 |
    R2. R1*3 R2.*3 |
    r4 r re'4 |
    do'2 do'4 |
    si2 do'4 |
    r mi' mi' |
    re' re' re' |
    do'2 do'4 |
    si2 si4 |
    la2 sol4 |
    r si si |
    do'2 do'4 |
    la2 la4 |
    sold2 la4 |
    r dod' dod' |
    re' re' re' |
    re'2 si4 |
    do'2 do'4 |
    si2 do'4 |
    R2.*5 |
    r4 si si |
    do' do' do' |
    do'2 do'4 |
    sib2 sib4 |
    la2 la4 |
    r re' re' |
    re'2 re'4 |
    mi'2 mi'4 |
    re'2 do'4 |
    r do' do' |
    do'4 do' re' |
    si2 do'4 |
    do'2 do'4 |
    si2 do'4 |
    R2.*12 R1*2 R2.*6 |
    r4 r8 sol sol4. sol8 |
    do'4. do'8 do'4. do'8 |
    do'4. do'8 do'4. do'8 |
    do'4. do'8 do'4. do'8 |
    si4 r r2 |
    R1*3 |
    r4 r8 sol sol4. sol8 |
    do'4. do'8 do'4. do'8 |
    la4. la8 la4. la8 |
    re'4. re'8 re'4. re'8 |
    do'4. do'8 do'4. do'8 |
    si4. si8 si4. si8 |
    la4. la8 la4. la8 |
    la4. la8 la4. re'8 |
    re'4 r r2 |
    R1*3 |
    %%
    r2 r4 \sugNotes {
      si8 si |
      do'4 do'8 do' do'8. do'16 |
      do'4 do'8 do' do' do' |
      sib2 sib4. sib8 |
      la4. la8 la4 la |
      la la la la |
      sol2 sol4 si |
      la4. la8 sol4. fad8 |
      sol4.
    } sol8 sol4. sol8 |
    do'4. do'8 do'4. do'8 |
    do'4. do'8 do'4. do'8 |
    do'4. do'8 do'4. do'8 |
    si4 r r2 |
    R1*3 |
    r4 r8 sol sol4. sol8 |
    do'4. do'8 do'4. do'8 |
    la4. la8 la4. la8 |
    re'4. re'8 re'4. re'8 |
    do'4. do'8 do'4. do'8 |
    si4. si8 si4. si8 |
    la4. la8 la4. la8 |
    la4. la8 la4. re'8 |
    re'4 r r2 |
    R1*3 |
    r2 \sugNotes {
      si8 si si si |
      si4 si8 si do'4 do'8 do' |
      do'8 do' do' do' do' do' |
      do'4 do' do'8 do' |
      si4 si8 si si la |
      si4.
    } sol8 sol4. sol8 |
    do'4. do'8 do'4. do'8 |
    do'4. do'8 do'4. do'8 |
    do'4. do'8 do'4. do'8 |
    si4 r r2 |
    R1*3 |
    r4 r8 sol sol4. sol8 |
    do'4. do'8 do'4. do'8 |
    la4. la8 la4. la8 |
    re'4. re'8 re'4. re'8 |
    do'4. do'8 do'4. do'8 |
    si4. si8 si4. si8 |
    la4. la8 la4. la8 |
    la4. la8 la4. re'8 |
    re'4 r r2 |
    R1*3 |
    %%
    r8 r si |
    do' do' do' |
    re' re' re' |
    do'4 do'8 |
    do' do' do' |
    la4 la8 |
    la4 sold8 |
    la4 la8 |
    si8 si si |
    dod' dod' dod' |
    re'4 re'8 |
    la la re' |
    si4 mi'8 |
    re'4 re'8 |
    re'4 si8 |
    do' do' do' |
    re' re' re' |
    do'4 do'8 |
    do' do' do' |
    la4. |
    sol8 sol sol |
    sol4. |
    R4.*13 |
    r8 r si |
    do' do' do' |
    re' re' re' |
    do'4 do'8 |
    do'8 do' do' |
    la4 la8 |
    la4 sold8 |
    la4 la8 |
    si si si |
    dod' dod' dod' |
    re'4 re'8 |
    la la re' |
    si4 mi'8 |
    re'4 re'8 |
    re'4 si8 |
    do' do' do' |
    re' re' re' |
    do'4 do'8 |
    do' do' do' |
    la4. |
    sol8 sol sol |
    sol4. |
  }
  \tag #'vbasse {
    \clef "vbasse"
    R1*2 R2. R1 |
    r8 sol sol sol mi mi mi do |
    fa4 fa8 re sol4 sol8 sol |
    do2 r4 |
    R2. R1*3 R2.*3 |
    r4 r sol |
    mi2 do4 |
    sol2 do4 |
    r do' do' |
    do' si si |
    do'2 do4 |
    sol2 sol4 |
    fad2 sol4 |
    r sol sol |
    do'2 do4 |
    fa2 re4 |
    mi2 la,4 |
    r la la |
    re' re' re' |
    sol2 sol4 |
    mi2 do4 |
    sol2 do4 |
    R2.*5 |
    r4 sol sol |
    do' do' do' |
    fa2 fa4 |
    sib2 sol4 |
    la2 re4 |
    r re' re' |
    sol2 sol4 |
    do'2 do'4 |
    si2 do'4 |
    r do do |
    fa4 fa re |
    sol2 mi4 |
    la2 fa4 |
    sol2 do4 |
    R2.*12 R1*2 R2.*6 |
    r4 r8 sol sol4. sol8 |
    do'4. do8 do4. do8 |
    fa4. fa8 fa4. fa8 |
    do4. do8 do4. do8 |
    sol,4 r r2 |
    R1*4 |
    r4 r8 do8 do4. do8 |
    fa4. fa8 fa4. fa8 |
    re4. re8 re4. re8 |
    la4. la8 la4. la8 |
    mi4. mi8 mi4. mi8 |
    la,4. la,8 la,4. la,8 |
    re4. re8 re4. re8 |
    sol,4 r r2 |
    R1*3 |
    %%
    r2 r4 sol8 sol |
    do'4 do'8 do' do' do' |
    fa4 fa8 fa fa fa |
    sib2 sib4 sol |
    la4. la8 la4. la,8 |
    re4 re re re |
    sol2 sol4 si, |
    do do re re |
    sol,2 r |
    r4 r8 do8 do4. do8 |
    fa4. fa8 fa4. fa8 |
    do4. do8 do4. do8 |
    sol,4 r r2 |
    R1*4 |
    r4 r8 do8 do4. do8 |
    fa4. fa8 fa4. fa8 |
    re4. re8 re4. re8 |
    la4. la8 la4. la8 |
    mi4. mi8 mi4. mi8 |
    la,4. la,8 la,4. la,8 |
    re4. re8 re4. re8 |
    sol,4 r r2 |
    R1*3 |
    r2 sol8 sol sol sol |
    sol4 sol8 sol do'4 do'8 do' |
    fa fa fa fa fa fa |
    do4 do do8 do |
    sol4 sol8 sol sol re |
    sol2 r |
    r4 r8 do8 do4. do8 |
    fa4. fa8 fa4. fa8 |
    do4. do8 do4. do8 |
    sol,4 r r2 |
    R1*4 |
    r4 r8 do8 do4. do8 |
    fa4. fa8 fa4. fa8 |
    re4. re8 re4. re8 |
    la4. la8 la4. la8 |
    mi4. mi8 mi4. mi8 |
    la,4. la,8 la,4. la,8 |
    re4. re8 re4. re8 |
    sol,4 r r2 |
    R1*3 |
    %%
    r8 r sol |
    la la la |
    si si si |
    do'4 do'8 |
    do do do |
    fa4 re8 |
    mi4 mi8 |
    la,4 r8 |
    R4.*3
    re8 re re |
    mi4 mi8 |
    fad4 fad8 |
    sol4 sol8 |
    la la la |
    si si si |
    do'4 do'8 |
    mi mi mi |
    fa4. |
    sol8 sol sol, |
    do4. |
    R4.*13 |
    r8 r sol |
    la la la |
    si si si |
    do'4 do'8 |
    do do do |
    fa4 re8 |
    mi4 mi8 |
    la,4 r8 |
    R4.*3 |
    re8 re re |
    mi4 mi8 |
    fad4 fad8 |
    sol4 sol8 |
    la la la |
    si si si |
    do'4 do'8 |
    mi8 mi mi |
    fa4. |
    sol8 sol sol, |
    do4. |
  }
>>