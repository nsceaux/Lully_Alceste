\score {
  \new StaffGroupNoBar <<
    %% Violons
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus" >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
      \new Staff << \global \includeNotes "timbales" >>
    >>
    %% Chœur
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      %% Admete
      \new Staff \withLyrics <<
        \global \keepWithTag #'admete \includeNotes "voix"
      >> \keepWithTag #'admete \includeLyrics "paroles"
      %% Alcide
      \new Staff \withLyrics <<
        \global \keepWithTag #'alcide \includeNotes "voix"
      >> \keepWithTag #'alcide \includeLyrics "paroles"
      %% Licomede
      \new Staff \withLyrics <<
        \global \keepWithTag #'licomede \includeNotes "voix"
      >> \keepWithTag #'licomede \includeLyrics "paroles"
      %% Straton
      \new Staff \withLyrics <<
        \global \keepWithTag #'straton \includeNotes "voix"
      >> \keepWithTag #'straton \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s1*2 s2. s1 s1\noBreak s1\noBreak s2. s2. s1*3 s2.*6\break
        % chœur
        s2.*38 s2 \bar "" \break
        % solistes
        s4 s2.*6 s1*2 s2.*6
        % chœur
        s1*21 s2.*2 s1*27 s2.*3 s1*20 s4.*24\break
        % Alcide
      }
      \origLayout {
        s1*2\break s2. s1\pageBreak
        s1*2 s2.\break s2. s2 s8 \bar "" \break
        s4. s1 s2 \bar "" \break s2 s2.\pageBreak
        s2.*6\pageBreak
        s2.*7\pageBreak
        s2.*7\pageBreak
        s2.*7\pageBreak
        s2.*7\pageBreak
        s2.*7\pageBreak
        s2.*7\pageBreak
        s2.*2 s1*2 s2.\pageBreak
        s2.*5 s1\pageBreak
        s1*5\pageBreak
        s1*5 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \pageBreak
        s2 s1*4
        %%
        s1 s2.\pageBreak
        s2. s1*3\pageBreak
        s1*4\pageBreak
        s1*5\pageBreak
        s1*4\pageBreak
        s1*4\pageBreak
        s1*5\pageBreak
        s1*2 s2.\pageBreak
        s2.*2 s1\pageBreak
        s1*3 s2 \bar "" \pageBreak
        s2 s1*4 s2 \bar "" \pageBreak
        s2 s1*3\pageBreak
        s1*4\pageBreak
        s1*3
        %%
        s4.*2\pageBreak
        s4.*6\pageBreak
        s4.*6\pageBreak
        s4.*6\pageBreak
        s4.*7\pageBreak
        s4.*7\pageBreak
        s4.*5\pageBreak
        s4.*6\pageBreak
        s4.*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
