\tag #'(licomede basse) {
  Ne pre -- ten -- dez pas nous sur -- pren -- dre,
  ve -- nez, nous al -- lons vous at -- ten -- dre :
  nous fe -- rons tous nos -- tre de -- voir
  pour vous bien re -- ce -- voir.
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Nous fe -- rons tous nos -- tre de -- voir
  pour vous bien re -- ce -- voir.
}
\tag #'(admete basse) {
  Per -- fi -- de, é -- vite un sort fu -- nes -- te,
  on te par -- don -- ne tout si tu veux rendre Al -- ces -- te.
}
\tag #'(licomede basse) {
  J’ai -- me mieux mou -- rir, s’il le faut,
  que de ce -- der ja -- mais cét ob -- jet plein de char -- mes.
}
\tag #'(admete alcide basse) {
  A l’as -- saut, à l’as -- saut.
}
\tag #'(licomede straton basse vdessus vhaute-contre vtaille vbasse) {
  Aux armes, aux ar -- mes.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  A l’as -- saut, à l’as -- saut.
  Aux armes, aux ar -- mes.
  A l’as -- saut,
  aux armes, aux ar -- mes.
  A l’as -- saut, à l’as -- saut.
  Aux armes, aux ar -- mes.
  
  A l’as -- saut, à l’as -- saut.
  Aux armes, aux ar -- mes.
  A l’as -- saut,
  aux armes, aux ar -- mes.
  A l’as -- saut, à l’as -- saut.
  Aux armes, aux ar -- mes.
}
\tag #'(admete alcide licomede basse) {
  A moy, com -- pa -- gnons, à moy.
}
\tag #'(admete licomede basse) {
  Sui -- vez vos -- tre roy.
  Sui -- vez vos -- tre roy.
}
\tag #'(alcide basse) {
  C’est Al -- ci -- de
  qui vous gui -- de.
  A moy, com -- pa -- gnons, à moy.
}
\tag #'(admete alcide licomede basse) {
  A moy, com -- pa -- gnons, à moy.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Don -- nons, don -- nons de tou -- tes parts.
  Don -- nons, don -- nons de tou -- tes parts.

  Don -- nons, don -- nons de tou -- tes parts.
  \tag #'(vdessus vhaute-contre vtaille) {
    Don -- nons, don -- nons de tou -- tes parts.
  }
  \tag #'vbasse {
    Don -- nons, don -- nons 
  }
  Don -- nons, don -- nons de tou -- tes parts.
  Don -- nons, don -- nons de tou -- tes parts.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Que cha -- cun à l’en -- vy com -- bat -- te.
  Que l’on a -- bat -- te
  les murs, & les rem -- parts.
  Que l’on a -- bat -- te
  les murs, & les rem -- parts.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Don -- nons, don -- nons de tou -- tes parts.
  \tag #'(vdessus vhaute-contre vtaille) {
    Don -- nons, don -- nons de tou -- tes parts.
  }
  \tag #'vbasse { de tou -- tes parts. }

  Don -- nons, don -- nons de tou -- tes parts.
  \tag #'(vdessus vhaute-contre vtaille basse) {
    Don -- nons, don -- nons de tou -- tes parts.
  }
  \tag #'vbasse {
    Don -- nons, don -- nons
  }
  Don -- nons, don -- nons de tou -- tes parts.
  Don -- nons, don -- nons de tou -- tes parts.
}

\tag #'(vdessus vbasse vhaute-contre vtaille) {
  Que les En -- ne -- mis, pes -- le mes -- le,
  tré -- bu -- chent sous l’af -- freu -- se gres -- le
  de nos flé -- ches, & de nos dards.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Don -- nons, don -- nons de tou -- tes parts.
  \tag #'(vdessus vhaute-contre vtaille) {
    Don -- nons, don -- nons de tou -- tes parts.
  }
  \tag #'vbasse { de tou -- tes parts. }

  Don -- nons, don -- nons de tou -- tes parts.
  \tag #'(vdessus vhaute-contre vtaille) {
    Don -- nons, don -- nons de tou -- tes parts.
  }
  \tag #'vbasse {
    Don -- nons, don -- nons 
  }
  Don -- nons, don -- nons de tou -- tes parts.
  Don -- nons, don -- nons de tou -- tes parts.

  Cou -- ra -- ge, cou -- ra -- ge, cou -- ra -- ge,
  ils sont à nous, ils sont à nous.
  \tag #'(vdessus vhaute-contre vtaille) {
    Cou -- ra -- ge, cou -- ra -- ge, cou -- ra -- ge,
  }
  ils sont à nous, ils sont à nous.
  Cou -- ra -- ge, cou -- ra -- ge, cou -- ra -- ge,
  ils sont à nous, ils sont à nous.
}
\tag #'(alcide basse) {
  C’est trop dis -- pu -- ter l’a -- van -- ta -- ge,
  je vais vous ou -- vrir un pas -- sa -- ge,
  sui -- vez- moy tous, sui -- vez- moy tous.
}
\tag #'(vdessus vhaute-contre vtaille vbasse) {
  Cou -- ra -- ge, cou -- ra -- ge, cou -- ra -- ge,
  ils sont à nous, ils sont à nous.
  \tag #'(vdessus vhaute-contre vtaille) {
    Cou -- ra -- ge, cou -- ra -- ge, cou -- ra -- ge,
  }
  ils sont à nous, ils sont à nous.
  Cou -- ra -- ge, cou -- ra -- ge, cou -- ra -- ge,
  ils sont à nous, ils sont à nous.
}
