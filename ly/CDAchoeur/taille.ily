\clef "taille" R1*2 R2. R1*3 R2.*2 R1*3 R2.*6 |
r4 sol' do'' |
la' sol' sol' |
sol'2 do''4 |
si'2 si'4 |
la'2 sol'4 |
r re' sol' |
sol'2 sol'4 |
fa'2 fa'4 |
mi'2 mi'4 |
r mi' la' |
la' la' la' |
sol'2 sol'4 |
sol'2 sol'4 |
sol'2 mi'4 |
mi'8 re' do' si do' re' |
si4 si8 si si4 |
do' mi'8 re' do'4 |
si si8 si si4 |
do' mi'8 fa' sol'4 |
sol' sol' sol' |
sol' sol' sol' |
la'2 la'4 |
fa'2 sol'4 |
mi'2 re'4 |
r la' la' |
sol'2 sol'4 |
sol'2 sol'4 |
sol'2 sol'4 |
r sol' sol' |
la'4 la' la' |
sol'2 sol'4 |
la'2 la'4 |
sol'2 mi'4 |
mi'8 fa' sol' fa' mi' fa' |
re'4 sol'8 sol' sol'4 |
sol'8 fa' mi' fa' sol' la' |
sol'2 sol'4~ |
sol' sol'4. sol'8 |
sol'2 r4 |
R2.*6 R1*2 R2.*6 |
r4 r8 sol'8 sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
la'4. la'8 la'4. la'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4 sol'8 sol' si'4 sol' |
sol'4 sol'8 sol' do''4 sol' |
sol'4. sol'8 mi'4. sol'8 |
sol'4. sol'8 mi'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4. sol'8 mi'4. mi'8 |
fa'4. fa'8 fa'4. fa'8 |
la'4. la'8 la'4. la'8 |
la'4. la'8 mi'4. mi'8 |
mi'4. mi'8 mi'4. mi'8 |
mi'4. mi'8 la'4. la'8 |
la'4. la'8 la'4. la'8 |
sol'4 sol'8 sol' sol'4. sol'8 |
sol'4. mi'16 fa' sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4. mi'16 fa' sol'4. sol'8 |
%%
\sugNotes {
  sol'2. sol'8 sol' |
  sol'4 sol'8 sol' sol'8. sol'16 |
  la'4 la'8 la' la' la' |
  fa'2 fa'4. sol'8 |
  fa'4. fa'8 fa'4 sol' |
  fad' fad' fad' fad' |
  sol'2 sol'4 sol' |
  sol'4. sol'8 re'4. re'8 |
  re'4.
} sol'8 sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
la'4. la'8 la'4. la'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4 sol'8 sol' si'4 sol' |
sol'4 sol'8 sol' do''4 sol' |
sol'4. sol'8 mi'4. sol'8 |
sol'4. sol'8 mi'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4. sol'8 mi'4. mi'8 |
fa'4. fa'8 fa'4. fa'8 |
la'4. la'8 la'4. la'8 |
la'4. la'8 mi'4. mi'8 |
mi'4. mi'8 mi'4. mi'8 |
mi'4. mi'8 la'4. la'8 |
la'4. la'8 la'4. la'8 |
sol'4 sol'8 sol' sol'4. sol'8 |
sol'4. mi'16 fa' sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4. mi'16 fa' sol'4. sol'8 |
sol'2 \sugNotes {
  sol'8 sol' sol' sol' |
  sol'4 sol'8 sol' sol'4 sol'8 sol' |
  la'8 la' la' la' la' la' |
  sol'4 sol' sol'8 sol' |
  sol'4 sol'8 sol' sol' fad' |
  sol'4.
} sol'8 sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
la'4. la'8 la'4. la'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4 sol'8 sol' si'4 sol' |
sol'4 sol'8 sol' do''4 sol' |
sol'4. sol'8 mi'4. sol'8 |
sol'4. sol'8 mi'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4. sol'8 mi'4. mi'8 |
fa'4. fa'8 fa'4. fa'8 |
la'4. la'8 la'4. la'8 |
la'4. la'8 mi'4. mi'8 |
mi'4. mi'8 mi'4. mi'8 |
mi'4. mi'8 la'4. la'8 |
la'4. la'8 la'4. la'8 |
sol'4 sol'8 sol' sol'4. sol'8 |
sol'4. mi'16 fa' sol'4. sol'8 |
sol'4. sol'8 sol'4. sol'8 |
sol'4. mi'16 fa' sol'4. sol'8 |
%%
sol'4 sol'8 |
mi' mi' la' |
sol' sol' sol' |
sol'4 sol'8 |
sol' sol' sol' |
fa'4 fa'8 |
mi'4 mi'8 |
mi'4 la'8 |
fad'8 fad' si' |
la'4 la'8 |
la'4 la'8 |
la' la' la' |
sol'4 do''8 |
la'4 la'8 |
sol'4 sol'8 |
mi'8 mi' la' |
sol' sol' sol' |
sol'4 sol'8 |
sol' sol' sol' |
fa'4. |
re'8 re' re' |
mi' mi'16 re' do'8 |
si8 si re' |
do' re'8. mi'16 |
mi'4 r8 |
R4.*10 |
r8 r sol' |
mi' mi' la' |
sol' sol' sol' |
sol'4 sol'8 |
sol' sol' sol' |
fa'4 fa'8 |
mi'4 mi'8 |
mi'4 la'8 |
fad'8 fad' si' |
la'4 la'8 |
la'4 la'8 |
la' la' la' |
sol'4 do''8 |
la'4 la'8 |
sol'4 sol'8 |
mi' mi' la' |
sol' sol' sol' |
sol'4 sol'8 |
sol' sol' sol' |
fa'4. |
re'8 re' re' |
mi'4. |
