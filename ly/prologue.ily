\newBookPart #'(full-rehearsal)
\actn "Prologue"
%% 0-1
\pieceToc "Ouverture"
\includeScore "AAAouverture"
\newBookPart #'(full-rehearsal)
%% 0-2
\pieceToc\markup\wordwrap { La Nymphe de la Seine :
  \italic { Le Heros que j’attens ne reviendra-t’il pas ? }
}
\includeScore "AABair"
\newBookPart #'(full-rehearsal)
%% 0-3
\pieceToc "Bruit de Guerre"
\includeScore "AACbruitGuerre"
%% 0-4
\pieceToc\markup\wordwrap { La Nymphe de la Seine :
  \italic { Quel bruit de guerre m’épouvante ? }
}
\includeScore "AADrecit"
%% 0-5
\pieceToc "Rondeau pour la Gloire"
\includeScore "AAErondeau"
\newBookPart #'(full-rehearsal)
%% 0-6
\pieceToc\markup\wordwrap {
  La Nymphe de la Seine, La Gloire :
  \italic { Helas ! superbe Gloire, helas ! }
}
\includeScore "AAFrecit"
%% 0-7
\pieceToc\markup\wordwrap {
  Chœur des Nayades et des Divinitez champestres :
  \italic { Qu’il est doux d'accorder ensemble }
}
\includeScore "AAGchoeur"
\newBookPart #'(full-rehearsal)
%% 0-8
\pieceToc\markup\wordwrap {
  La Nymphe des Thuilleries :
  \italic { L’Art d’accord avec la Nature }
}
\includeScore "AAHair"
%% 0-9
\pieceToc\markup\wordwrap { Air pour les divinités des fleuves }
\includeScore "AAIentree"
%% 0-10
\pieceToc\markup\wordwrap { La Nymphe de la Marne :
  \italic { L’Onde se presse } }
\includeScore "AAJair"
\newBookPart #'(full-rehearsal)
%% 0-11
\pieceToc\markup\wordwrap { Air pour les divinitez des fleuves et les nymphes }
\includeScore "AAKloure"
\newBookPart #'(full-rehearsal)
%% 0-12
\pieceToc\markup\wordwrap { La Gloire, les Nymphes, chœur :
  \italic { Que tout retentisse }
}
\includeScore "AALchoeur"
\newBookPart #'(full-rehearsal)
%% 0-13
\pieceToc\markup\wordwrap { Air pour les divinités des fleuves et les nymphes }
\includeScore "AAMentree"
%% 0-14
\pieceToc\markup\wordwrap { Chœur des divinités des Fleuves et des Nymphes :
  \italic { Quel Cœur sauvage } }
\includeScore "AANchoeur"
%% 0-15
\pieceToc\markup\wordwrap { Chœur des divinités, des Fleuves et les Nymphes :
  \italic { Revenez Plaisirs exilez } }
\includeScore "AAOchoeur"
\newBookPart #'(full-rehearsal)
%% 0-16
\pieceToc "Ouverture"
\reIncludeScore "AAAouverture" "AAPentract"
\actEnd "Fin du prologue"
