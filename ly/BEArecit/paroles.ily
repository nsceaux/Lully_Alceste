Stra -- ton, donne or -- dre qu’on s’a -- pres -- te
pour com -- men -- cer la fes -- te.

En -- fin, grace au dé -- pit, je gous -- te la dou -- ceur
de sen -- tir le re -- pos de re -- tour dans mon cœur.
J’es -- tois à pre -- fe -- rer au roy de Thes -- sa -- li -- e ;
et si pour sa gloire on pu -- bli -- e
qu’A -- pol -- lon au -- tre -- fois luy ser -- vit de pas -- teur,
je suis roy de Scy -- ros, & Thé -- tis est ma sœur.
J’ay sçeu me con -- so -- ler d’un hy -- men qui m’ou -- tra -- ge,
j’en or -- don -- ne les jeux a -- vec tran -- qui -- li -- té.
Qu’ai -- sé -- ment le dé -- pit dé -- ga -- ge
des fers d’une in -- gra -- te beau -- té !
Qu’ai -- sé -- ment le dé -- pit dé -- ga -- ge
des fers d’une in -- gra -- te beau -- té !
Et qu’a -- prés un long es -- cla -- va -- ge,
il est doux d’estre en li -- ber -- té
en li -- ber -- té !
Et qu’a -- prés un long es -- cla -- va -- ge,
il est doux d’estre en li -- ber -- té
en li -- ber -- té
en li -- ber -- té !

Il n’est pas sûr toû -- jours de croi -- re l’ap -- pa -- ren -- ce :
un cœur bien pris, & bien tou -- ché,
n’est pas ai -- sé -- ment dé -- ta -- ché,
ny si tost gue -- ry que l’on pen -- se ;
et l’a -- mour est sou -- vent ca -- ché
sous u -- ne feinte in -- dif -- fe -- ren -- ce.
et l’a -- mour est sou -- vent ca -- ché
sous u -- ne feinte in -- dif -- fe -- ren -- ce.

Quand on est sans es -- pe -- ran -- ce,
on est bien tost sans a -- mour.
Quand on est sans es -- pe -- ran -- ce,
on est bien tost sans a -- mour.
Mon Ri -- val a la pre -- fe -- ren -- ce,
ce que j’aime est en sa puis -- san -- ce,
je perds tout es -- poir en ce jour :
je perds tout es -- poir tout es -- poir en ce jour :
quand on est sans es -- pe -- ran -- ce,
on est bien tost sans a -- mour.
Quand on est sans es -- pe -- ran -- ce,
on est bien tost sans a -- mour.
Voi -- cy l’heu -- re qu’il faut que la fes -- te com -- men -- ce,
cha -- cun s’a -- van -- ce,
pre -- pa -- rons- nous.
