\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse #:music , #{ s1*15 s2.*2
        <>_\markup\italic\right-align\line\whiteout {
          J’en ordonne les Jeux avec tranquilité.
        }
        s1.*33 s4
        <>_\markup\italic\right-align\line\whiteout {
          Sous une feinte indifference.
        }
      #})
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#79 #}))
