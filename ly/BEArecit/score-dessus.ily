\score {
  \new GrandStaff <<
    \new Staff <<
      { s1*15 s2.*2
        <>_\markup\italic\right-align\line\whiteout {
          J’en ordonne les Jeux avec tranquilité.
        }
        s1.*33 s4
        <>_\markup\italic\right-align\line\whiteout {
          Sous une feinte indifference.
        }
      }
      \global \includeNotes "dessus1"
    >>
    \new Staff << \global \includeNotes "dessus2" >>
  >>
  \layout { }
}
