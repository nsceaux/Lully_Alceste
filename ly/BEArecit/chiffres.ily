s1 <6- 4>2 <5> s8 <6 5 _-> <_+>4
s2 s1*2
<6- 4 2>4 <6 5/>2 <7>8 <6> <_+>2 <_+>4.\figExtOn <_+>8\figExtOff
<6>2 <6> <_->4\figExtOn <_->\figExtOff <6>2
s <6> s

<_->2 s2. <4>8 <3> s1
<6>2 <7>4 <6> s2. <6 4+>8 <6+>
<_->2 <6 5>4 <_+>2. s2 <6+>4 <6> <_-> <6> s2 <6 5 _->4
<_+>2. s2 <6 5>4 <4>2 <3>4 s2 <6 _->4 <6>2 <6 5>4 s2 <6>4

s2 <6 5 _->4 <_+>4.\figExtOn <_+>8 <6>4 <_+>4 <_+>\figExtOff <_+> s1. s2 <6>1
s1 <6 4+>4 <6+> s2 <6>4 <6+>2. <_+>4.\figExtOn <_+>8\figExtOff <6>4 <6 5> <_+>2 <_+>2.\figExtOn <_+>\figExtOff
s2. <6> s <6> s <6 5 _-> <_+>4.\figExtOn <_+>8\figExtOff <6>4 <6 5 _-> <_+>2

s2. <6 5 _->4 <_+>2 s1 <6>2 <6 5 _->2. <_+>2 <6 4>4 <6>1 \new FiguredBass { <4>4 <_+> }
<_+>2. <5> <6>1 <6 _->2 s1 <6 5/>2 <6>4 <5/>2. <6>2
<_->4 <6 5/>2 <6 5>4 <4> \new FiguredBass <3> s1. <6 5 _->2 <_+>4 <7>4 <6>2 <_+>2\figExtOn <_+>4\figExtOff <6>2 <5/>4
s2. <6 5> <6>1 <6 4+>4 <6+> <_->2 <6>4 <6+> <6 5/>2 <_+>2\figExtOn <_+>4\figExtOff
<6 5/>2. <_+>2 <6>8 <6+> s4 <4> <_+> s2

s2 <7>4 <6> <6 5>2 <5 _->2. <6>4 <6 5>1 s s2
<6 5/> <_-> <5+> <6 5/> <_+> s1*2
s2 <_-> s <4 2>4 <6 5/> s1 s2 <"">4.\figExtOn <"">8\figExtOff

s2 <"">4.\figExtOn <"">8 <_+>4 <_+>8 <6> <"">4 <"">8\figExtOff <6 5 _-> <_+>1 <7>4 <6> <6 5>2
<_->2. <6>4 <6 5>1 s s2 <5/>
<_->2 <5+> <5/>4 <3> <_+>2 s1 s2

s4 <6 5/> s1 s2 <7>4 <6> <_+>1
