\clef "dessus" R1*15 R2.*2 |
r4 fa'' sol'' la'' sib'' do''' |
fa''2 sol''4 mi''2 dod''4 |
re''8 mi'' fa''4 fa'' fa''4. fa''8 mi''4 |
fa'' do''' sib'' la''8 sib'' do'''4 sib''8 la'' |
sol'' fa'' mi''4 fa'' re'' re'' mi'' |
dod''2 re''4 si' mi'' dod'' |
re''8 dod'' re'' mi'' fa''4 r fa''8 mi'' re'' do''? |
si'2 do''4 la' re'' si' |
do''2 do''4 do'' re'' mi'' |
fa''2 mi''4 re''2 do''4 |
si' si' do''4. re''8 si'4. mi''8 |
dod''2 dod''4 r mi'' la'' |
fa''4. sol''8 la''2 sol''4. sol''8 |
sol''2 sol''4 la''2 la''8 sol'' |
fa''2 fa''4 sol''2 sol''8 fa'' |
mi''2 fa''4 mi''8 re'' dod''4.( si'16 dod'') |
re''8 mi'' re'' dod'' re''4 mi'' dod''4.\trill re''8 |
re''2. r2*3/2 |
R1.*15 |
r2 re''4. re''8 |
re''4 do'' do'' sib'8 la' |
sib'4 sib' do'' fa'' |
re'' sol''8 fa'' mi''4. fa''8 |
fa''4 la'' la''4. sol''8 |
fa''4. sol''8 la''4. la''8 |
re''4 re'' sol''4. sol''8 |
sol''4 fa'' mi''4.\trill re''8 |
re''2 r4 la'8 si' do'' do'' re'' mi'' fa''4. fa''8 |
re''4 re'' sol''4. sol''8 |
mi''4 mi'' re''4. sol''8 |
sol''4 sol''8 fa'' mi''4. mi''8 |
fa''4 do'' re'' re''8 do'' |
si'4. si'8 mi''4 mi''8 re'' |
dod''4 si'8 la' re''4 re''8 mi'' |
dod''2 re''4. re''8 |
re''4 do'' do'' sib'8 la' |
sib'4 sib' do'' fa'' |
re'' sol''8 fa'' mi''4.\trill fa''8 |
fa''4 la'' la''4. sol''8 |
fa''4. sol''8 la''4. la''8 |
re''4 re'' sol''4. sol''8 |
sol''4 fa'' mi''4.\trill re''8 |
re''2 r |
R1*4 |
