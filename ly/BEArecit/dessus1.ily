\clef "dessus" R1*15 R2.*2 |
r4 <>_"Violons" la'' sol'' fa''2 mi''4 |
re''2 mi''4 dod''2 la''4 |
la''4.( sol''16 la'') sib''4 sol''4. la''8 sib''4 |
la'' la' sib' do'' do'' re'' |
mi''8 fa'' sol''4 la'' fa'' fa'' sol'' |
mi''2 fa''4 re'' sol'' mi'' |
fa''8 mi'' fa'' sol'' la''4 r la''8 sol'' fa'' mi'' |
re''2 mi''4 do'' fa'' re'' |
mi''2 mi''4 la' si' dod'' |
re''8 mi'' fa'' sol'' la'' si'' sold''2 la''4 |
si'' sold'' la''4. si''8 sold''4.( fad''16 sold'') |
la''2 mi''4 r dod'' dod'' |
re''4. mi''8 fa''2 fa''4. sol''8 |
mi''2 mi''4 fa''2 fa''8 mi'' |
re''2 re''4 mi''2 mi''8 re'' |
dod''2 la''4 sol''8 fa'' mi''4 la'' |
fa''8 sol'' fa'' mi'' fa''4 sol'' mi''4.\trill re''8 |
re''2. r2*3/2 | \allowPageTurn
R1.*15 |
r2 <>_\markup\whiteout Violons fa''4. fa''8 |
sol''4. sol''8 la''4. la''8 |
re''4 sol'' mi'' la'' |
fa'' sib''8 la'' sol''4 do'''8 sib'' |
la''4 fa'' fa''4. mi''8 |
re''4. re''8 do''4 sib'8 la' |
sib'4 sib' si'4. si'8 |
mi''4 re'' dod''4. re''8 |
re''2 r4 fa''8 fa'' |
mi'' mi'' fa'' sol'' la''4. la''8 |
fa''4 fa'' sib''4. sib''8 |
sol''4 sol'' fa''4. re''8 |
mi''4 mi''8 fa'' sol''4. sol''8 |
la''4 la''8 sol'' fa''4 fa''8 mi'' |
re''4. re''8 sol''4 sol''8 fa'' |
mi''4. la''8 fa''4 fa''8 sol'' |
mi''2 fa''4. fa''8 |
sol''4. sol''8 la''4. la''8 |
re''4 sol'' mi'' la'' |
fa'' sib''8 la'' sol''4 do'''8 sib'' |
la''4 fa'' fa''4. mi''8 |
re''4. re''8 do''4 sib'8 la' |
sib'4 sib' si'4. si'8 |
mi''4 re'' dod''4. re''8 |
re''2 r |
R1*4 |
