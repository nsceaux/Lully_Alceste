\clef "vbasse" <>^\markup\character Licomede
r4 r8 re re4 r8 re' |
sib8\trill sib sib8. la16 la8\trill la r4 |
fa8 sol16 la mi8.\trill fa16 re4\trill re |
r r8 re la4 la8 la16 la |
re'4 r8 la fa8\trill fa fa8. sol16 |
mi8 mi16 fa sol8 la16 mi fa8 sol16 la la8 la16 sol |
la4 r8 la fad8. fad16 fad8. fad16 |
sol4. sol8 la8. la16 la8. sib16 |
sib8 sib sib16 sib sib re' sol4 sol8. sol16 |
mi16\trill mi sol sol do'8 do'16 do' fa8 fa16 fa fa8 sol16 la |
re4 r8 re'16 do' sib4 sib8. la16 |
sol4\trill do'8. sib16 la4 sol8.\trill fa16 |
fa4 r8 do' la8. la16 la8. sib16 |
sol4\trill sol8. la16 fa4 re8. sol16 |
do4 do r8 mi16 fa sol8 sol16 la |
sib8. la16 sol8. fa16 mi8. re16 |
dod2. |
%%
r4 re mi fa sol la |
sib2 sol4 la la la |
re'4. do'8 sib4 do' do' do |
fa fa sol la la sib |
do'2 la4 sib sib sol |
la4. sol8 fa4 sol mi la |
re2. r4 re re |
sol2 mi4 fa re sol |
do2 do4 fa fa mi |
re2 do4 si,2\trill la,4 |
mi2 do4 re mi2 |
la,2. r4 la la |
re'2 re'4 si si sol |
do'2 do'4 la la la |
sib2 sib4 sol4.(\trill fa8) sol4 |
la2 fa4 sol la2 |
re2 sib,4 sol, la,2 |
re2.
\ffclef "vdessus" <>^\markup\character Céphise
re''4 mi'' fa'' |
mi''2\trill re''4 dod''2 re''4 |
mi'' si' do''4. re''8 si'2\trill |
la'2. la'4 sib' do'' |
do''2 la'4 sib'2 la'4 |
sol'2\trill do''4 fa' sol' la' |
sol'4.\trill la'8 fa'4 mi'\trill do'' re'' |
sib'2\trill la'4 sol'4.\trill la'8 sib'4 |
la'2\trill fa'4 r fa'' la' |
sib' sib' la' la'( sol'8.\trill[ fa'16]) sol'4 |
la'2. re''4. mi''8 do''4 |
si'4.\trill si'8 do''4. re''8 re''2\trill |
do''2. r4 sol' la' |
sib' sib' la' sol'2\trill fa'4 |
mi'2.\trill mi''4 mi'' fa'' |
dod''4. dod''8 re''[ dod''] re''4 re''( dod'') |
re''2
\ffclef "vbasse" <>^\markup\character Licomede
re4. re8 |
mi4 mi fa4. fa8 |
sol4 sol r8 la la la |
sib2 do'4 do |
fa2 re4. re8 |
sib4. sib8 fad4. fad8 |
sol4 sol r mi8 mi |
dod4 re la,4. re8 |
re2 r4 re'8 re' |
la4 la8 sol fa[ sol] la[ fa] |
sib4 sib r4 sol8 sol |
do'4 do'8 do' do'4 si |
do'4 do' r do |
fa fa8 mi re4 mi8 fa |
sol4. sol8 mi4\trill fa8 sol |
la4 sol8 fa sib4 la8\trill sol |
la2 re4. re8 |
mi4. mi8 fa4. fa8 |
sol4 sol r8 la la la |
sib2 do'4 do |
fa2 re4. re8 |
sib4. sib8 fad4. fad8 |
sol4 sol r mi8 mi |
dod4 re la,4. re8 |
re2 r |
r8 la16 la mi8 mi16 fa sol8 sol16 sol sol8 fa16 mi |
fa4 fa r8 la, la,8. si,16 |
do4 do r8 do do re |
mi1 |
