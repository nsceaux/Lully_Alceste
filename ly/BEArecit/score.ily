\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \withRecit <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2 s2 \bar "" \break s2 s1*2\break s1*2\break
        s1*2\break s1 s2 \bar "" \pageBreak
        s2 s1*2\break s1*2\break
        s2.*2 s1. s2. \bar "" \break s2. s1.*2 s2. \bar "" \pageBreak
        s2. s1.*3\break s1.*4\break s1.*4\pageBreak
        s1.*4\break s1.*4\break s1.*4\break
        s1.*3 s2. \bar "" \break s2. s1. s2 \bar "" \pageBreak
        s2 s1*4 s2 \bar "" \break s2 s1*4\break s1*4\pageBreak
        s1*4\break s1*4\break s1*3 s2 \bar "" \pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
