\clef "basse" <<
  \tag #'basse {
    R1*15 R2.*2 | r4
  }
  \tag #'basse-continue {
    re,1 |
    re |
    re8 sol, la,4 re,2 |
    re1 |
    re~ |
    re4 dod re8 do? sib,4 |
    la,2 re4. do8 |
    sib,2 fad, |
    sol,4 sol si,2 |
    do la, |
    sib, sol, |
    do4 la, re8 sib, do4 |
    fa,2 fa |
    mi re |
    do~ do4 sib,8 la, |
    sol,2. |
    la, |
    <>^"gay" re,4
  }
>> re mi fa sol la |
sib2 sol4 la2 la4 |
re'4. do'8 sib4 do'2 do4 |
fa4 fa sol la la sib |
do'2 la4 sib2 sol4 |
la4. sol8 fa4 sol mi la |
re2. re,2 re4 |
sol2 mi4 fa re sol |
do2 do4 fa fa mi |
re2 do4 si,2 la,4 |
mi4. re8 do4 re mi2 |
la,2. la2 la4 |
re'2 re'4 si si sol |
do'2 do'4 la2 la4 |
sib2 sib4 sol2 sol4 |
la4. sol8 fa4 sol la2 |
re4. do8 sib,4 sol, la,2 |
re,2. <<
  \tag #'basse { r2*3/2 | R1.*15 | r2 }
  \tag #'basse-continue {
    re'4 dod' re' |
    sol2. la |
    sold2 la4 re mi mi, |
    la, la8 sib la sol fa2. |
    mi2 fa4 sib,8 la, sol,4 fa, |
    do2 la,4 re mi fa |
    sib si2 do'4 la sib |
    sol mi fa sib, do do, |
    fa,2 fa8 mi re2. |
    sol2 la4 sib2. |
    la2 sol4 fad2. | \allowPageTurn
    sol4. sol8 la4 fa sol4. fa8 |
    mi4. re8 do4 do' sib la |
    sol2 fa4 mi dod re |
    la, la si dod'2 re'4 |
    la4. sol8 fa mi re4 la,2 |
    re2
  }
>> re4. re8 |
mi4 mi fa4. fa8 |
sol2 la4. la8 |
sib2 do'4 do |
fa2 re4. re8 |
sib2 fad |
sol mi4. mi8 |
dod4 re la,2 |
re re'4. re'8 |
la4. sol8 fa sol la fa |
sib4 sib sol4. sol8 |
do'4. do'8 do'4 si |
do'4 do' do2 |
fa4 fa8 mi re4 mi8 fa |
sol4. sol8 mi4 fa8 sol |
la4 sol8 fa sib4 la8 sol |
la4 la, re4. re8 |
mi4 mi fa4. fa8 |
sol2 la4. la8 |
sib2 do'4 do |
fa2 re4. re8 |
sib2 fad |
sol mi4. mi8 |
dod4 re la,2 |
<<
  \tag #'basse { re2 r | R1*4 | }
  \tag #'basse-continue {
    re2. do8 si, |
    la,2 mi4 dod |
    re2 re, |
    la,4. sol,8 fa,2 |
    mi,1 |
  }
>>
