\key la \minor
\time 4/4 \midiTempo#80 s1*15
\digitTime\time 3/4 s2.*2
\time 6/4 \midiTempo#160 s1.*33
\time 2/2 \midiTempo#160 s1*25
\time 4/4 \midiTempo#80 s1*3
\time 2/2 s1 \bar "|."
