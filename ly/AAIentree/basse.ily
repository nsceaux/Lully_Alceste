\clef "basse" sol,4 sol fad |
sol do re |
sol, sol4. la8 |
sib2 sol4 |
re'2 la4 |
sib mib fa |
sib,4. do8 re4 |
mib fa fa, |
sib,4. do8 sib, la, |
sib,2 fad,4 |
sol, do2 |
re4 mi fad |
sol4. la8 sib4 |
si2. |
do'4. sib8 la4 |
sib fad re |
sol sol, re |
sol2 la4 |
sib2 la4 |
sol re re, |
sol,2 fad,4 |
sol,2. |
