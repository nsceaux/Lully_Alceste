\piecePartSpecs
#`((dessus)
   (haute-contre)
   (taille #:system-count 3)
   (quinte #:system-count 3)
   (basse #:system-count 3)
   (basse-continue #:music , #{ s2.*7\break s2.*7\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#40 #}))
