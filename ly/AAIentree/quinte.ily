\clef "quinte" sol4 sol la |
sib la la |
sol re' sib |
fa'2 mi'4 |
re'2 do'4 |
re' do' do' |
sib8 do' re'4 sib |
sol fa4. fa8 |
fa2. |
fa4. sol8 la4 |
sol la2 |
la2 la4 |
sol4 sib8 do' re'4 |
re'2 re'4 |
do'2 do'4 |
sib la2 |
sol la4 |
sol sib la |
re'2 do'4 |
sib8 do' re'4. do'8 |
sib4. sol8 la4 |
sib2. |
