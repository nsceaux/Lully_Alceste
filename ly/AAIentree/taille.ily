\clef "taille" re'4 re' re' |
re' mib' re' |
re' sib4. do'8 |
re'2 sol'4 |
fa'2 fa'4 |
fa' sol' fa' |
fa' do' fa'8 mib' |
re'4 do'2 |
re'2. |
re'4. re'8 re'4 |
re' mib'2 |
re'2 re'4 |
re' sol'2 |
sol' sol'4 |
sol'8 fa' mib'4 mib' |
re'2 re'4 |
re'2 re'4 |
re'2 do'4 |
sib2 do'4 |
re' re'4. re'8 |
re'4. re'8 re'4 |
re'2. |
