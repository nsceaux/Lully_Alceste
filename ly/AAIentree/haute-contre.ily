\clef "haute-contre" sib'2 la'4 |
sol' sol' fad' |
sol'2. |
fa'2 sib'4 |
la'4. sib'8 do''4 |
sib'4 sib' la' |
sib'2 sib'4 |
sib' la'4. sib'8 |
sib'2. |
sib'4. re''8 do''4 |
sib' do'' sib' |
la'8 re'' re''4. do''8 |
sib'2. |
sol'4 re''2 |
sol' fad'4 |
sol' re' fad' |
sol'2 fad'4 |
sol'2 fa'4 |
fa' sol' la' |
sib' la'4. sol'8 |
sol'4. re''8 do''4 |
sol'2. |
