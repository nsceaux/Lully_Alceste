\clef "dessus" re''4. do''8 re''4 |
sib' do'' la' |
sib' sol' sol'' |
re''2\trill mib''4 |
fa''4. sol''8 fa''4 |
re'' mib'' do'' |
re'' sib' fa'' |
do'' do''4.\trill sib'8 |
sib'2. |
sib'4. sib''8 la''4 |
sib''4 la'' sol'' |
fad'' sol'' la'' |
re''2. |
sol''4. fa''8 mib'' re'' |
mib''4. re''8 do''4 |
re''4 la'4.\trill sib'8 |
sib'2\trill la'4 |
sib'2 do''8\trill sib'16 do'' |
re''4 mi'' fad'' |
sol'' fad''4.\trill sol''8 |
sol''4. sib''8 la''4 |
sol''2. |
