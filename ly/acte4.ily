\bookpart {
  \act "Acte Quatriême"
  \sceneDescription\markup\wordwrap-center {
    Le Théatre Représente le Fleuve Acheron.
  }
  \scene "Scene Premiére" "Scene I"
  \sceneDescription\markup\wordwrap-center {
    Charon, les Ombres.
  }
  %% 4-1
  \pieceToc\markup\wordwrap {
    Charon, les Ombres :
    \italic { Il faut passer tost ou tard }
  }
  \includeScore "EAArecit"
}
\bookpart {
  \scene "Scene Deuxiême" "Scene II"
  \sceneDescription\markup\wordwrap-center {
    Alcide, Charon, les Ombres.
  }
  %% 4-2
  \pieceToc\markup\wordwrap {
    Alcide, Charon :
    \italic { Sortez, Ombres, faites moy place }
  }
  \includeScore "EBArecit"
}
\bookpart {
  \scene "Scene Troisiême" "Scene III"
  \sceneDescription\markup\wordwrap-center {
    Pluton, Proserpine, l’Ombre d’Alceste, Suivans de Pluton.
  }
  %% 4-3
  \pieceToc\markup\wordwrap {
    Pluton, Proserpine, chœur :
    \italic { Reçoy le juste prix de ton amour fidelle }
  }
  \includeScore "ECArecit"
}
\bookpart {
  \sceneDescription "Feste Infernale"
  %% 4-4
  \pieceToc\markup Premier Air
  \includeScore "ECBair"
}
\bookpart {
  \paper { page-count = 1 }
  %% 4-5
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Tout mortel doit icy paroistre }
  }
  \includeScore "ECCchoeur"
  \markup\fill-line {
    \null
    \right-column {
      \line {
        On reprend le Premier Air des Violons page
        \page-refIII #'ECBair .
      }
      \line {
        Ensuite l’air qui suit.
      }
    }
  }
}
\bookpart {
  %% 4-6
  \pieceToc\markup Deuxième Air
  \includeScore "ECDair"
}
\bookpart {
  \paper { page-count = 2 }
  %% 4-7
  \pieceToc\markup\wordwrap {
    Chœur : \italic { Chacun vient icy bas prendre place }
  }
  \includeScore "ECEchoeur"
  \markup\fill-line {
    \null
    \right-column {
      \line {
        On reprend le \concat { 2 \super e } Air
        page \page-refIII#'ECDair .
        
        Ensuite le Chœur \italic { Chacun vient }
        page \page-refIII#'ECEchoeur .
      }
      \line {
        Et on rejoüe encor le \concat { 2 \super e } Air pour finir.
        
        \score {
          { \clef "bass" \key fa \major \partial 2.
            <>_\markup { Finale du \concat { 2 \super e } air. }
            fa4. mib8 re do | \custosNote sib,4 }
          \layout {
            indent = 0
            \context { \Staff \remove "Time_signature_engraver" }
          }
        }
      }
    }
  }
}
\bookpart {
  \scene "Scene Quatriême" "Scene IV"
  \sceneDescription\markup\wordwrap-center {
    Alecton, Pluton, Proserpine.
  }
  %% 4-8
  \pieceToc\markup\wordwrap {
    Alecton, Pluton :
    \italic { Quittez, quittez les Jeux, songez à vous deffendre }
  }
  \includeScore "EDArecit"

  \scene "Scene Cinquiême" "Scene V"
  \sceneDescription\markup\wordwrap-center {
    Alcide, Pluton, Proserpine, Alecton.
  }
  %% 4-9
  \pieceToc\markup\wordwrap {
    Pluton, Alcide, Proserpine :
    \italic { Insolent jusqu’icy braves-tu mon courroux ? }
  }
  \includeScore "EEArecit"
  \actEnd\markup Fin du Quatriême Acte
}

\bookpart {
  %% 4-10
  \pieceToc\markup Entr’acte
  \reIncludeScore "ECDair" "EEBentracte"
  \markup\fill-line {
    \null
    \score {
      { \clef "bass" \key fa \major \partial 2.
        <>_\markup { Finale de l’entr’acte. }
        fa,4 fa8 mib re do | sib,2*1/2 \custosNote sib4 }
      \layout {
        \context { \Staff \remove "Time_signature_engraver" }
      }
    }
  }
}
