\clef "quinte" do'4 do' do' |
re' re' mi' |
mi' do' do' |
sol2 sol4 |
do' do' do' |
re' re' mi' |
mi' do' do' |
sol2. |
do'4 do' do' |
do' la la |
mi' si si |
la2. |
la4. la8 la4 |
si4 sol sol |
la la la |
sol2 sol4 |
sol si re' |
si2 mi'4 |
mi' re' re' |
do'2 do'4 |
sol si re' |
si2 mi'4 |
mi' re' re' |
do'2 do'4 |
