\key do \major \midiTempo#240
\digitTime\time 3/4 s2.*8 \bar ":|."
s2.*16 \bar "|."
\endMark\markup\column {
  \line { On reprend l’air des divinités page \page-refII #'AAMentree , }
  \line { et ensuitte les \concat { 2 \super e } parolles du chœur. }
}

  