\clef "taille" sol'4 mi' sol' |
sol' sol' sol' |
mi' mi' la' |
sol'2 sol'4 |
sol' mi' sol' |
sol' sol' sol' |
mi' mi' la' |
sol'2. |
sol'4 mi' do' |
do' do' fa' |
mi' mi' mi' |
mi'2. |
fad'4. sol'8 la'4 |
sol' sol' mi' |
mi' re' re' |
re'2 re'4 |
sol' sol' fad' |
sol'2 sol'4 |
mi' fa' re' |
mi'2 mi'4 |
sol' sol' fad' |
sol'2 sol'4 |
mi' fa' re' |
mi'2 mi'4 |
