\score {
  \new StaffGroupNoBar <<
    %% Orchestre
    \new StaffGroupNoBracket \with { \haraKiriFirst } <<
      \new Staff <<
        \global \keepWithTag #'dessus \includeNotes "dessus"
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    %% Chœur
    <<
      \new Staff \withLyricsB <<
        \global \includeNotes "vdessus"
      >> \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
      \new Staff \withLyricsB <<
        \global \includeNotes "vhaute-contre"
      >> \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
      \new Staff \withLyricsB <<
        \global \includeNotes "vtaille"
      >> \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
      \new Staff \withLyricsB <<
        \global \includeNotes "vbasse"
      >> \keepWithTag #'couplet1 \includeLyrics "paroles"
      \keepWithTag #'couplet2 \includeLyrics "paroles"
    >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout { s2.*6\pageBreak s2.*7\pageBreak s2.*6\pageBreak }
    >>
  >>
  \layout { }
  \midi { }
}
