\clef "dessus" mi''4 do'' mi'' |
re''\trill re'' sol'' |
do'' do'' fa'' |
re''2\trill re''4 |
mi'' do'' mi'' |
re''\trill re'' sol'' |
do'' do'' fa'' |
re''2.\trill |
mi''4 mi'' mi'' |
la' la' re'' |
si' si' mi'' |
dod''2. |
re''4. mi''8 fad''4 |
sol'' re'' mi'' |
do'' la' re'' |
si'2\trill si'4 |
re'' re'' re'' |
re''2 mi''4 |
do'' re'' si'\trill |
do''2 do''4 |
re'' re'' re'' |
re''2 mi''4 |
do'' re'' si' |
do''2 do''4 |
