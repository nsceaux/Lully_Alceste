\tag #'couplet1 {
  Quel Cœur sau -- va -- ge
  i -- cy ne s’en -- ga -- ge ?
  Quel cœur sau -- va -- ge
  ne sent point l’a -- mour ?
  Nous al -- lons voir les plai -- sirs de re -- tour ;
  ne man -- quons pas d’en faire un doux u -- sa -- ge :
  pour rire un peu, l’on n’est pas moins sa -- ge.
  Pour rire un peu, l’on n’est pas moins sa -- ge.
}
\tag #'couplet2 {
  Ah quel dom -- ma -- ge
  de fuir ce ri -- va -- ge !
  Ah quel dom -- ma -- ge
  de perdre un beau jour !
}
