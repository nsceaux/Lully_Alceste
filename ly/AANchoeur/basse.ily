\clef "basse" do mi do |
sol sol mi |
la la fa |
sol2 sol4 |
do mi do |
sol sol mi |
la la fa |
sol2. |
do4 do do |
fa fa re |
mi mi mi |
la,2. |
re4. re8 do4 |
si,4 si, do |
la, re re |
sol,2 sol,4 |
sol sol re |
sol2 mi4 |
la fa sol |
do2 do4 |
sol sol re |
sol2 mi4 |
la fa sol |
do2 do4 |
