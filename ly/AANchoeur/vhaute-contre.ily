\clef "vhaute-contre" sol'4 sol' sol' |
sol' sol' sol' |
mi' mi' la' |
sol'2 sol'4 |
sol' sol' sol' |
sol' sol' sol' |
mi' mi' la' |
sol'2. |
sol'4 sol' sol' |
fa' do' fa' |
mi' mi' mi' |
mi'2. |
fad'4. sol'8 la'4 |
sol' sol' sol' |
sol' sol' fad' |
sol'2 sol'4 |
sol' sol' fad' |
sol'2 sol'4 |
mi' fa' re' |
mi'2 mi'4 |
sol'4 sol' fad' |
sol'2 sol'4 |
mi' fa' re' |
mi'2 mi'4 |
