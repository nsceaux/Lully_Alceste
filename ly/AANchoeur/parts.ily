\piecePartSpecs
#`((dessus #:system-count 3)
   (haute-contre)
   (taille #:system-count 3)
   (quinte #:system-count 3)
   (basse #:system-count 3)
   (basse-continue #:music , #{ s2.*8\break s2.*8\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#32 #}))
