\clef "haute-contre" do''4 sol' do'' |
si' si' si' |
la' do'' do'' |
si'2 si'4 |
do'' sol' do'' |
si' si' si' |
la' do'' do'' |
si'2. |
sol'4 sol' sol' |
fa' fa' la' |
la' la' sold' |
la'2. |
la'4. la'8 re''4 |

re'' si' sol' |
sol' sol' fad' |
sol'2 sol'4 |
si' si' la' |
si'2 si'4 |
la' la' sol' |
sol'2 sol'4 |
si' si' la' |
si'2 si'4 |
la' la' sol' |
sol'2 sol'4 |
