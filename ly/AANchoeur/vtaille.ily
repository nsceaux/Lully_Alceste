\clef "vtaille" do'4 sol do' |
si si si |
do' do' do' |
si2 si4 |
do' sol do' |
si si si |
do' do' do' |
si2. |
do'4 do' do' |
do' la la |
la la sold |
la2. |
la4. la8 re'4 |
re'4 re' si |
mi' re' re' |
re'2 re'4 |
si si la |
si2 do'4 |
do' la sol |
sol2 sol4 |
si si la |
si2 do'4 |
do' la sol |
sol2 sol4 |
