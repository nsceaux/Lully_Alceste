<<
  %% Céphise
  \tag #'(cephise basse) {
    \clef "vdessus" <>^\markup\character Céphise
    r4 do''8 do''16 do'' fa''4 do''8 la'16 do'' |
    fa'8 fa' r fa'16 sol' la'8.\trill la'16 la'8. si'16 |
    do''2 <<
      \tag #'basse {
        s2 s1 s2
        \ffclef "vdessus" <>^\markup\character Céphise
      }
      \tag #'cephise { r2 R1 r2 }
    >> r4 do'' |
    fa''4. sol''8 mi''4\trill mi''8 do'' |
    fa''2 fa''4. do''8 |
    re''4 do'' sib'\trill la'8 sib' |
    la'2\trill fa''4 fa'' |
    fa''4. mib''8 mib''4( re''8) mib'' |
    re''4\trill re'' do''4.\trill la'8 |
    sib'4. la'8 la'4.\trill sol'8 |
    sol'2 sib'4. sib'8 |
    sib'4 la' la'4. la'8 |
    la'8[\trill sol'] sol'4 r sol'8. sol'16 |
    sol'4 la' sol'4.\trill fa'8 |
    fa'2 sib'4. sib'8 |
    sib'4 la' la'4. la'8 |
    la'8[\trill sol'] sol'4 sol'4. sol'8 |
    sol'4 la' sol'4.\trill fa'8 |
    <<
      \tag #'basse {
        fa'8 s2.. s1*2 s4
        \ffclef "vdessus" <>^\markup\character Céphise
      }
      \tag #'cephise { fa'4 r r2 R1*2 r4 }
    >> r8 sol' sol'4 fa'8 fa'16 mi' |
    mi'4\trill <<
      \tag #'basse {
        s2. s2.
        \ffclef "vdessus" <>^\markup\character Céphise
      }
      \tag #'cephise { r4 r2 r r4 }
    >> r8 do'' |
    la'8.\trill la'16 sib'8. do''16 re''4 r8 sib'16 re'' |
    sol'4\trill sol'8. la'16 fa'4 <<
      \tag #'basse {
        s4 s1*2 s4
        \ffclef "vdessus" <>^\markup\character Céphise
      }
      \tag #'cephise { r4 R1*2 r4 }
    >> r8 re''16 re'' la'8 si'16 do'' |
    si'4 do'' do''16 sib'? sib' la' |
    la'4\trill sib'8 sib'16 sib' sib'8 la' |
    <<
      \tag #'basse {
        sib'16 sib' s2.. s2.
        \ffclef "vdessus" \outside <>^\markup\character Céphise
      }
      \tag #'cephise { sib'8 sib' r4 r2 R2. }
    >>
    r4 <>^"gay" do'' sib' |
    la'4.\trill sol'8 fa'4 |
    sol' do'' sib' |
    la' sol'\trill do'' |
    la'2\trill fa'4 |
    r do'' sib' |
    la'4.\trill sol'8 fa'4 |
    sol' do'' sib' |
    la' sol'\trill do'' |
    la'2\trill fa'4 |
    fa'' mi'' fa'' |
    re''2. |
    do''4 do''4.\trill sib'8 |
    la'2\trill re''4 |
    si' do'' re'' |
    mi'' fa''8[ mi''] re''[ do''] |
    sol''4 mi''\trill fa'' |
    sol'' fa''\trill mi'' |
    re'' mi'' fa'' |
    mi''( re''2)\trill |
    do''2. |
    |
    do''4 do'' la' |
    sib'2. |
    la'4 la'4. sib'8 |
    sol'2\trill do''4 |
    sol' la' sib' |
    la' sib'8[ la'] sol'[ fa'] |
    do''4 la' r8 fa'' |
    la'4\trill sib' do'' |
    re'' mi'' fa'' |
    fa''( mi''2) |
    fa''4 <<
      \tag #'basse {
        s2. s1*2 s4
        \ffclef "vdessus" <>^\markup\character Céphise
      }
      \tag #'cephise { r4 r2 R1*2 r4 }
    >> r8 re'' sol'8. sol'16 sol'8 fa' |
    mi'4\trill do''8 do''16 re'' sib'8.\trill la'16 sol'4\trill |
    fa'4 <<
      \tag #'basse {
        s2. s1*19
        \ffclef "vdessus" <>^\markup\character Céphise
      }
      \tag #'cephise { r4 r2 R1*19 }
    >>
    do''4 do''8 do'' re''4 mi'' |
    fa''2. fa''4 |
    mi''4.\trill re''8 do''4.\trill sib'8 |
    la'2\trill r |
    re''8 re'' re'' do'' sib'4 sib'8 la' |
    sol'4\trill sol' do'' do''8 sib' |
    la'4\trill la' fa'' fa''8 mi'' |
    re''4\trill re'' r sol''8 la'' |
    fa''4.\trill mi''8 re''4\trill re''8 mi'' |
    do''2 do''4 re''8 mib'' |
    re''4\trill re'' re'' mi''8 fa'' |
    mi''4\trill mi'' r do''8 re'' |
    sib'4.\trill la'8 sol'4\trill sol'8 la' |
    fa'4 <<
      \tag #'basse {
        s2. s1 s2.
        \ffclef "vdessus" <>^\markup\character Céphise
      }
      \tag #'cephise { r4 r2 R1 R2. }
    >>
    la'4 la' sib' |
    do''2. |
    la'4 sib'8[ la'] sol'[ fa'] |
    sol'2\trill sol'4 |
    la' sib' do'' |
    re''2. |
    mi''4 fa''8[ mi''] re''[ do''] |
    fa''2 fa''4 |
    r r la' |
    sib'2. |
    la'4 re'' mi'' |
    dod''4. re''8 mi''4 |
    fa'' mi''4.\trill re''8 |
    re''2. |
    re''4 la' re'' |
    si'2 si'4 |
    do'' sol' do'' |
    la'2\trill la'4 |
    r fa'' mi'' |
    re''2\trill mi''4 |
    do'' re'' si' |
    do''2. |
    fa''4 do'' fa'' |
    re''2\trill re''4 |
    r sib' la' |
    sol'2\trill la'4 |
    fa' sol' mi'\trill |
    fa'2. |
    r4 sib' la' |
    sol'2\trill la'4 |
    fa' sol' mi'\trill |
    fa'2 r4 do'' |
    do''4. re''8 do''4. sib'8 |
    la'2\trill r4 do'' |
    fa''4. fa''8 re''4.\trill re''8 |
    sol''4. fa''8 mi''4. re''8 |
    do''4. sib'8 la'4.\trill la'8 |
    sib'4. do''8 re''4. re''8 |
    mi''4. fa''8 fa''2( mi''4.)\trill fa''8 |
    fa''2 la'4. sib'8 |
    do''2 do''4 sib'8[ la'] |
    sol'4.\trill sol'8 sol'4. la'8 |
    sib'4. sib'8 sib'2\trill |
    la' la'4. la'8 |
    re''2 re''4. mi''8 |
    dod''4. dod''8 dod''4. dod''8 |
    re''4. la'8 re''2 |
    si'2 do''4. sol'8 |
    la'2\trill la'4. si'8 |
    do''1 |
    fa''4 fa''8 mi'' re''4.\trill sol''8 |
    mi''2\trill do''4 do'' |
    do''4. re''8 do''4. sib'8 |
    la'2\trill r4 do'' |
    fa''4. fa''8 re''4.\trill re''8 |
    sol''4. fa''8 mi''4. re''8 |
    do''4. sib'8 la'4.\trill la'8 |
    sib'4. do''8 re''4. re''8 |
    mi''4. fa''8 fa''2( mi''4.)\trill fa''8 |
    fa''1 |
  }
  %% Straton
  \tag #'(straton basse) {
    <<
      \tag #'basse {
        s1*2 s2
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton {
        \clef "vbasse" <>^\markup\character Straton
        R1*2 r2
      }
    >> do8 do16 re mi fa sol la |
    sib8 sib r re16 re mi8\trill fa do8. fa,16 |
    fa,2 <<
      \tag #'basse {
        s2 s1*15 s8
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r2 R1*15 r8 }
    >> do8 fa4 fa8 fa16 sol la8 fa |
    sib8 sib16 sib sib sib do' re' sol8 sol16 sol la8 la16 la |
    fad8 fad16 re' re' do' sib la sol8 sol16 do re8 re16 re |
    <<
      \tag #'basse {
        sol,4 s2. s4
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { sol,2 r2 r4 }
    >> do'4 r8 sol16 sol mi8. sol16 |
    do8 do16 re mi8\trill mi16 do fa8 fa <<
      \tag #'basse {
        s4 s1 s2.
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r4 R1 r2 r4 }
    >> r8 la |
    mi\trill r r16 mi mi la fad8. fad16 fad fad sol la |
    sib8 sib16 re' re' do' sib re' sol8 sol16 sol mib8 mib16 re |
    re8\trill re <<
      \tag #'basse {
        s2 s2.*2 s8
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { r4 r R2.*2 r8 }
    >> sib,16 do re8\trill re16 sib, fa8. fa16 sol sol la sib |
    la2.\trill |
    <<
      \tag #'basse {
        s2.*31 s4
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { R2.*31 r4 }
    >> r8 do' la8.\trill la16 la8. do'16 |
    fa fa fa mib mib8. re16 re4\trill r8 sib16 la |
    sol8 sol16 la fad8 mi16 re sol8 la16 sib la4\trill |
    <<
      \tag #'basse {
        sol4 s2. s1 s4
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { sol2 r R1 r4 }
    >> la4 la4. sib8 |
    sol2\trill sol4 la8 mi |
    fa2 r4 sib |
    sol4.\trill sol8 sol4 do' |
    la4.\trill la8 la4. re'8 |
    si4. si8 si4. si8 |
    do'4. do'8 do'4( si) |
    do'2 r4 sol8 la |
    sib4 sib8 la sol4(\trill fad8) sol |
    fad2\trill r |
    sib4. sib8 do'4. re'8 |
    fad2 sol8[ fad] sol4 |
    sol2( fad) |
    sol2 r4 sib8. sib16 |
    sib4 sib8. do'16 la4( sib8) do' |
    sol2\trill r |
    la4. la8 sib4. do'8 |
    mi2\trill fa8[ mi] fa4 |
    fa2( mi) |
    fa1 |
    <<
      \tag #'basse {
        s1*13 s4
        \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton { R1*13 r4 }
    >> r8 la re re sib sib16 sib |
    sol4.\trill sol8 sol4 fa8 fa16 mi |
    mi2\trill mi4 |
    \tag #'straton {
      R2.*31 |
      R1 |
      r2 r4 do |
      fa4. sol8 fa4. mi8 |
      re4. re8 sib4. sib8 |
      sol4. sol8 do'4. sib8 |
      la4. sol8 fa4. mi8 |
      re4. do8 sib,4. la,8 |
      sol,4. fa,8 do2~ do4. do8 |
      fa,2 fa4. sol8 |
      la2 la4. sib8 |
      do'4. do'8 sib4. la8 |
      sol4. sol8 sol,2 re4. re'8 re'4. do'8 |
      sib4. sib8 sib2 |
      la2 la4 sol |
      fad2 fad4. fad8 |
      sol2 mi4 mi8 mi |
      fa2. re4 |
      la2 la |
      fa4 fa8 re sol4. sol,8 |
      do2 do |
      r r4 do |
      fa4. sol8 fa4. mi8 |
      re4. re8 sib4. sib8 |
      sol4. sol8 do'4. sib8 |
      la4. sol8 fa4. mib8 |
      re4. do8 sib,4. la,8 |
      sol,4. fa,8 do2~ do4. do8 |
      fa,1 |
    }
  }
>>