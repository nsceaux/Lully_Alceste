\tag #'(cephise basse) {
  Dans ce beau jour, quelle hu -- meur som -- bre
  fais- tu voir à con -- tre- temps ?
}
\tag #'(straton basse) {
  C’est que je ne suis pas du nom -- bre
  des a -- mants qui sont con -- tents.
}
\tag #'(cephise basse) {
  Un ton gron -- deur & se -- ve -- re
  n’est pas un grand a -- gré -- ment ;
  le cha -- grin n’a -- van -- ce gue -- re
  les af -- fai -- res d’un a -- mant.
  Le cha -- grin n’a -- van -- ce gue -- re
  les af -- fai -- res d’un a -- mant.
  Le cha -- grin n’a -- van -- ce gue -- re
  les af -- fai -- res d’un a -- mant.
}
\tag #'(straton basse) {
  Ly -- chas vient de me faire en -- ten -- dre
  que je n’ay plus ton cœur, qu’il doit seul y pre -- ten -- dre,
  et que tu ne vois plus mon a -- mour qu’à re -- gret ?
}
\tag #'(cephise basse) {
  Ly -- chas est peu dis -- cret…
}
\tag #'(straton basse) {
  Ah je m’en dou -- tois bien qu’il vou -- loit me sur -- pren -- dre.
}
\tag #'(cephise basse) {
  Ly -- chas est peu dis -- cret
  d’a -- voir dit mon se -- cret.
}
\tag #'(straton basse) {
  Co -- ment ! il est donc vray ! tu n’en fais point d’ex -- cu -- se ?
  Tu me tra -- his ain -- si sans en es -- tre con -- fu -- se ?
}
\tag #'(cephise basse) {
  Tu te plains sans rai -- son ;
  est-ce u -- ne tra -- hi -- son
  quand on te de -- sa -- bu -- se ?
}
\tag #'(straton basse) {
  Que je suis es -- ton -- né de voir ton chan -- ge -- ment !
}
\tag #'(cephise basse) {
  Si je chan -- ge d’a -- mant
  qu’y trou -- ves- tu d’é -- tran -- ge ?
  Si je chan -- ge d’a -- mant
  qu’y trou -- ves- tu d’é -- tran -- ge ?
  Est-ce un su -- jet d’es -- ton -- ne -- ment
  de voir u -- ne fil -- le qui chan -- ge
  de voir u -- ne fil -- le qui chan -- ge ?
  Est-ce un su -- jet d’es -- ton -- ne -- ment
  de voir u -- ne fil -- le qui chan -- ge
  de voir u -- ne fil -- le qui chan -- ge ?
}
\tag #'(straton basse) {
  A -- prés deux ans pas -- sez dans un si doux li -- en,
  De -- vois- tu ja -- mais prendre u -- ne chai -- ne nou -- vel -- le.
}
\tag #'(cephise basse) {
  Ne con -- tes- tu pour rien
  D’es -- tre deux ans fi -- del -- le ?
}
\tag #'(straton basse) {
  Par un es -- poir doux, & trom -- peur,
  pour -- quoy m’en -- ga -- geois- tu dans un a -- mour
  dans un a -- mour si ten -- dre ?
  Fal -- loit- il me don -- ner ton cœur
  puis que tu vou -- lois le re -- pren -- dre ?
  Fal -- loit- il me don -- ner ton cœur
  puis que tu vou -- lois le re -- pren -- dre ?
}
\tag #'(cephise basse) {
  Quand je t’of -- frois mon cœur, c’es -- toit de bon -- ne foy
  que n’em -- pes -- che tu qu’on te l’os -- te ?
  Est- ce ma fau -- te
  est- ce ma fau -- te
  si Ly -- chas me plaist plus que toy ?
  Est- ce ma fau -- te
  est- ce ma fau -- te
  si Ly -- chas me plaist plus que toy ?
}
\tag #'(straton basse) {
  In -- grat -- te, est- ce le prix de ma per -- se -- ve -- ran -- ce ?
}
\tag #'(cephise basse) {
  Es -- saye un peu de l’in -- cons -- tan -- ce,
  es -- saye un peu de l’in -- cons -- tan -- ce :
  c’est toy qui le pre -- mier m’a -- pris à m’en -- ga -- ger,
  pour re -- com -- pen -- se
  pour re -- com -- pen -- se
  je te veux ap -- prendre à chan -- ger.
  Pour re -- com -- pen -- se
  je te veux ap -- prendre à chan -- ger,
  je te veux ap -- prendre à chan -- ger.

  Il faut chan -- ger toû -- jours,
  il faut chan -- ger toû -- jours,
  il faut chan -- ger toû -- jours,
  il faut chan -- ger
  il faut chan -- ger toû -- jours.
  Les plus dou -- ces a -- mours
  sont les a -- mours nou -- vel -- les.
  Les plus dou -- ces a -- mours
  sont les a -- mours nou -- vel -- les.
  Les plus dou -- ces a -- mours
  sont les a -- mours nou -- vel -- les.
  Il faut chan -- ger toû -- jours,
  il faut chan -- ger toû -- jours,
  il faut chan -- ger toû -- jours,
  il faut chan -- ger
  il faut chan -- ger toû -- jours.
}
\tag #'straton {
  Il faut ai -- mer toû -- jours,
  il faut ai -- mer toû -- jours,
  il faut ai -- mer toû -- jours,
  il faut
  il faut ai -- mer toû -- jours.
  Les plus dou -- ces a -- mours
  sont les a -- mours fi -- del -- les,
  sont les a -- mours fi -- del -- les.
  Les plus dou -- ces a -- mours
  sont les a -- mours fi -- del -- les,
  sont les a -- mours fi -- del -- les.
  Il faut ai -- mer toû -- jours,
  il faut ai -- mer toû -- jours,
  il faut ai -- mer toû -- jours,
  il faut
  il faut ai -- mer toû -- jours.
}
