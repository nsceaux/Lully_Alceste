\clef "basse" fa,1 |
fa2 re |
do~ do4. sib,16 la, |
sol,2 do8 fa, do,4 |
fa,2 fa~ |
fa4 fa do' sib |
la4. sol8 fa4 la, |
sib, la, sol, do |
fa,2 fa4. fa8 |
sol4. sol8 la4. la8 |
sib4. sib8 fad4. fad8 |
sol4 do re re, |
sol,2 sol4. fa8 |
mi2 fa8 fa sol la |
sib4. sib8 si4. si8 |
do'4 fa do2 |
fa4. mi8 re4. re8 |
mi4. mi8 fa4 fa, |
sib,4. sib,8 si,4. si,8 |
do4 fa, do,2 |
fa,2. fa4 |
re2 mib4 do |
re8. do16 sib,4 mib8 do re re, |
sol,1 |
do~ | \allowPageTurn
do2 la,~ |
la, sib, |
do fa,4 fa |
dod2 re4~ re16 do sib, la, |
sol,2 mib4 do |
re2. |
sol4 mi2 |
fa4 re8 mib do4 |
sib,2 la,4 sol, |
fa,2 fa4 |
mi2. |
fa4 fa,2 |
do4 la, sol, |
fa, do do, |
fa, fa2 |
mi2. |
fa4 fa,2 |
do4 la, sol, |
fa, do do, |
fa, fa2 |
fa4 sol la |
sib la sol |
do' do2 |
re4 mi fa |
sol la si |
do'4 do2 |
si,4 do re |
mi fa2 |
sol4 do fa, |
sol,2. |
do,4 do re |
mi fad2 |
sol4 fa mi |
fa la fa |
do'2 do'4 |
sib la sol |
fa mi re |
do fa2 |
fa8 mib re4 do |
sib,8 la, sol,4 fa, |
do do,2 |
fa, fa |
la, sib, |
mib4 re8. do16 sib,8 la,16 sol, re8 re, |
sol,4 sol si,2 |
do4 la, sib, do8 do, |
fa,2 fa |
mi dod |
re2. re4 |
mi2. mi4 |
fa2 fad |
sol2. sol4 |
lab fa sol sol, |
do2 sib,4. la,8 sol,4 sol8 fa mib2 |
re4. mib8 re4 do |
sib,2 la,4 sol, |
re2 mib4 do |
re2 re, |
sol, sol4. fa8 |
mi2 fa4 fa, |
do4. re8 do4. sib,8 |
la,2 sol,4 fa, |
do2 re4 sib, |
do2 do, |
fa,1 | \allowPageTurn
fa2. mi4 |
re2. re4 |
mi2. mi4 |
fa1 |
fad2 sol4 sol, |
do4. re8 mi2 |
fa4 fa8 mi re2 |
sol4 sol8 fa mi2 |
fa2 sol4. sol8 |
do'4 do'8 sib la2 |
sib4 sib8 la sol4 sol, |
do4 do8 sib, la,2 |
sib, do4 do, |
fa,2 sib, |
si,1 |
do2 do'4 |
fa2. |
mi |
fa2 fa,4 |
do4. re8 mi do |
fa4 sol la |
sib la sol |
do'2 sib4 |
la2 sib4 |
fa mi re |
sol2 sol4 |
la sib sol |
la sol8 fa mi4 |
re la,2 |
re,4 re mi |
fad2 re4 |
sol re sol |
mi2 mi4 |
fa do fa |
re si, do |
sol, sol mi |
la fa sol |
do' sol do' |
la2 la4 |
sib fa sib |
sol mi fa |
do do' la |
re' sib do' |
fa fa mi |
re mi fa |
do2 la,4 |
re sib, do |
fa,2 fa |
mi2. do4 |
fa4. sol8 fa4. mi8 |
re2 sib |
sol do'4. sib8 |
la4. sol8 fa4. mi8 |
re4. do8 sib,4. la,8 |
sol,4. fa,8 do2 do, |
fa,2 fa4. sol8 |
la2 la4. sib8 |
do'2 sib4. la8 |
sol2 sol, |
re re'4. do'8 |
sib2 sib |
la la4 sol |
fad2. fad4 |
sol2 mi4. mi8 |
fa2. re4 |
la1 |
fa4 fa8 re sol4 sol, |
do,2 do4. re8 |
mi2. do4 |
fa4. sol8 fa4. mi8 |
re2 sib |
sol do'4. sib8 |
la4. sol8 fa4. mib8 |
re4. do8 sib,4. la,8 |
sol,4. fa,8 do2 do, |
fa,2~ fa,8 sol, fa, mi, |
\once\set Staff.whichBar = "|"
\custosNote re,4
