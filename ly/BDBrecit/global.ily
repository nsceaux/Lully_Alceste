\key fa \major \bar ""
\set Score.currentBarNumber = #10
\time 4/4 \midiTempo#80 s1*4
\time 2/2 \midiTempo#160 s1*16
\time 4/4 \midiTempo#80 s1*10
\digitTime\time 3/4 s2.*3
\time 4/4 s1
\digitTime\time 3/4 \midiTempo#160 s2.*32
\time 4/4 \midiTempo#80 s1*5
\time 2/2 \midiTempo#160 s1*20
\digitTime\time 2/2 s1*13
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 \midiTempo#160 s2.*32
\time 2/2 s1*7
\time 3/2 s1.
\time 2/2 s1*19
\time 3/2 s1.
\time 2/2 s1 \bar "|."
