\score {
  \new ChoirStaff <<
    %% Céphise
    \new Staff \withLyrics <<
      \global \keepWithTag #'cephise \includeNotes "voix"
    >> \keepWithTag #'cephise \includeLyrics "paroles"
    %% Straton
    \new Staff \withLyrics <<
      \global \keepWithTag #'straton \includeNotes "voix"
    >> \keepWithTag #'straton \includeLyrics "paroles"
    \new Staff <<
      \keepWithTag #'() \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s1*2\break s1*4\break s1*4\break
        s1*4 s2 \bar "" \break s2 s1*3\pageBreak
        s1 s2 \bar "" \break s2 s1\break s1*2\break s1*2\break
        s1*2\break s1\pageBreak
        s2.*2\break s2. s2 \bar "" \break s2 s2.*3\break
        s2.*6\break s2.*6\break s2.*6\pageBreak
        s2.*7\break s2.*4 s2 \bar "" \break s2 s1 s2 \bar "" \break
        s2 s1*2\break s1*4\break s1*4 s2 \bar "" \pageBreak
        s2 s1*4 s2 \bar "" \break s2 s1*4\break
        s1*5\break s1*4\break s1*4\break s1*3\pageBreak
        s1 s2.*4\break s2.*8\break s2.*7\break
        s2.*6\break s2.*6\break s2. s1*3 s2 \bar "" \pageBreak
        s2 s1*3\break s1. s1*3\break s1*4\break
        s1*5\break s1*4\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}
