s1 s2 <7>4 <6+>
s2.. <6>16 <6+> s2 <7>8 <3> \new FiguredBass { <4> <3> }
s1 s2 <"">4\figExtOn <"">\figExtOff <6>2. <6>4 s <6> <7> <7>
s1 <7>2 <5/> s <5/> <"">4\figExtOn <"">\figExtOff \new FiguredBass { <4> <_+> }
s1 <6 5/> <7>8 <6>4. <6>2 s <4>4 <3> s2
<6> <5/>1 <7>8 <6>4. <5/>2 s \new FiguredBass { <4>4 <3> }

s1 <6>2
s4 <6 5 _-> <_+> <6> s8 <6 5 _-> <4> \new FiguredBass <_+>
<_+>2. <7>4 s1
s2 <6> <6>2. <6 5>4
<4>4 <3>2. <6>2 <_+>4. <6>16 <6+>
s2 <7>4 <6 5 _->

<_+>2. s4 <6> <5/>
s <6> <\markup\triangle-down 7 6 \figure-flat >4 s2
<6>4 <7>8 <6> s2. <6> s
s4 <6> <6> s <4> \new FiguredBass <3> s2. <6> s s4 <6> <6>
s4 <4> \new FiguredBass <3> s2. s4 <6> <6> <6>\figExtOn <6>2\figExtOff s2. s2 <6>4
<_+>2 <6>4 s2. <6>2 <6+>4 <6> <6 5>2 <_+>2 <6 5>4 <4>2 <_+>4

s2. <6>4 <5/>2 <"">4\figExtOn <"">2\figExtOff s2.*2 <6 4+>4 <6> <6> s \new FiguredBass { <6> <6+> }
s2. s4 <6> <6 _-> s <6>2 <4>4 \new FiguredBass <3>2 s2
s <6>4 <5/>2. <7>8 <6> <_+>4
<6>8 <6+> <4> \new FiguredBass <_+> s2 <6> s4 <6> <6 5> <4>8 \new FiguredBass <3>
s1 <6+>2 <5/> s2 <6> <6 5/>1
s2 <5/> <_+>1 <6 4+ 3>4 <6 5 _-> <4> <_+> <_->2 <6>4. <6+>8 s2

<7>4 <6> <_+>2\figExtOn <_+>\figExtOff <6>2 <6+> <_+> <6 4+ 3>4 <6 5 _-> <4>2 \new FiguredBass <_+> s2
s <6 5/>1 <"">2\figExtOn <"">\figExtOff <6> <6> s <6 4 3>4 <6 5>
<4>2 \new FiguredBass <3> s1 s2 <6 4+>4 <6+> s1 <6>
s1 <5/> s2 <6> s1
<_+>2 <6> <6 5> <4>4 <_+> s2 <5/> s1
s2 <6> <6 5> <4>4 \new FiguredBass <3> s1

<6>2. <5/>4 s2.*2 <6>2. s
s s4 <6> <6> <"">4\figExtOn <"">2\figExtOff <"">\figExtOn <"">4\figExtOff <6>2. s2.*2 <6>2 <6 5>4
<_+>4\figExtOn <_+>8\figExtOff <6> <6+>4 s <4> <_+> <_+>2\figExtOn <_+>4\figExtOff <6>2 <_+>4 <_+>2\figExtOn <_+>4\figExtOff <6>2. <"">2\figExtOn <"">4\figExtOff
s4 <5/>2 <_+>4\figExtOn <_+>\figExtOff <6> s <6 5> <_+> <"">2\figExtOn <"">4\figExtOff <6>2. <"">2\figExtOn <"">4\figExtOff
s4 <5/>2 s2 <6>4 s <6 5>2 s2. <6>4 <6 5>2 s <6>4
s4 <6 5>2 s1 <6>1 s1.

s2 s1*2 <6>1
<6>1. s1 <6>2..\figExtOn <6>8 <"">2 <"">4.\figExtOff <6+>8
s1*2 <7>2 <6> <_+>2\figExtOn <_+>\figExtOff
<6>1 <_+>2 <6> s2. <6+>4 s1*2
s1 <6> s1*2
s1*2 <6>1 <6>1.
