\piecePartSpecs
#`((dessus #:score-template "score-dessus2")
   (basse #:score "score-basse")
   (basse-continue #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#77 #}))
