\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new ChoirStaff \with { \haraKiriFirst } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'alceste \includeNotes "voix"
    >> \keepWithTag #'alceste \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'licomede \includeNotes "voix"
    >> \keepWithTag #'licomede \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \modVersion {
        s1*15\break
      }
      \origLayout {
        s1*2 s2 \bar "" \break s2 s1\break s1*2\break
        s1*2\break s1*2 s2 \bar "" \pageBreak
        s2 s1 s2 \bar "" \break s2 s1*2\break s1*5\break s1*5\pageBreak
        s1*6\break s1*5\break s1*3\break s2.*2\pageBreak
        s1 s2. s2 \bar "" \break s2 s2. s1\break s1*2\break
        s1*2 s2 \bar "" \break s2 s1*2\break s1*2\pageBreak
        s1*2 s2 \bar "" \break s2 s1 s2 \bar "" \break s2 s1\break
        s2. s1\break s2.*2\break s2. s1\pageBreak
        s1*2\break s2. s2 \bar "" \break s2 s1*2\break s1*2\break
      }
    >>
  >>
  \layout { }
  \midi { }
}