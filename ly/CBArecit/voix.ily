<<
  %% Alceste
  \tag #'(alceste basse) {
    <<
      \tag #'basse { s1*2 \ffclef "vbas-dessus" <>^\markup\character Alceste }
      \tag #'alceste { \clef "vbas-dessus" <>^\markup\character Alceste R1*2 }
    >>
    re''2 la'8 la'16 sib' do''8 do''16 re'' |
    si'4 si'8 <<
      \tag #'basse { s8 s2 s1 s4
        \ffclef "vbas-dessus" <>^\markup\character Alceste }
      \tag #'alceste { r8 r2 R1 r4 }
    >> fa''4 r8 re''16 re'' sib'\trill sib' sib' re'' |
    sol'4 sol'8 <<
      \tag #'basse { s8 s2 s1 s2
        \ffclef "vbas-dessus" <>^\markup\character Alceste }
      \tag #'alceste { r8 r2 R1 r2 }
    >> r8 fad'16 fad' fad'8 fad'16 fad' |
    sol'4. sol'8 sol'4 sol'8 sol'16 fad' |
    sol'2 r8 re''16 re'' re''8 mib''16 fa'' |
    mib''4. do''8 la'8.\trill la'16 la'8. sib'16 |
    fad'4 fad'8 <<
      \tag #'basse { s s2 s1*23 s4 r8
        \ffclef "vbas-dessus" <>^\markup\character Alceste }
      \tag #'alceste { r8 r2 R1*23 r4 r8 }
    >> re''8 mib''4. mib''16 mib'' |
    si'4 si'8. re''16 sol'4 sol' |
    <<
      \tag #'basse { s1 s2.*2 s4
        \ffclef "vbas-dessus" <>^\markup\character Alceste }
      \tag #'alceste { R1 R2.*2 r4 }
    >> r8 re'' do''8.\trill do''16 do''8 sib'16[ la'] |
    sib'8 sib' sib' la' sol'\trill[ fad'16] sol' |
    fad'4 fad'8. re''16 re''8. re''16 re''8. do''16 |
    si'8. si'16 do''8. do''16 re''8[ do''16] re'' |
    mib''4 mib'' sol'8 sol'16 sol' la'8. sib'16 |
    la'4.\trill la'8 sib'8. sib'16 do''16[ sib'] do''[ la'] |
    sib'4 r8 re''16 mib'' fa''8 fa''16 fa'' mib''8.\trill re''16 |
    do''4\trill do'' do''8 do''16 do'' do''[ sib'8]\trill la'16 |
    sib'4 r8 sib'16 do'' re''4 r8 re''16 do'' |
    re''4 r8 re''16 re'' sol''8 sol''16 fa'' mib''8.\trill re''16 |
    mib''4 mib'' la'8 la'16 la' la'8. sib'16 |
    fad'4 r8 la'16 la' sib'8[ do''] la'8.\trill sol'16 |
    sol'4 <<
      \tag #'basse { s2. s1*7 s2. s1 s2.*3 s1*3 s2
        \ffclef "vbas-dessus" <>^\markup\character Alceste }
      \tag #'alceste { r4 r2 R1*7 R2. R1 R2.*3 R1*3 r4 r }
    >> sol''8 r16 re'' |
    si'8 si'16 si' do''8. re''16 mib''4 r8 sol'' |
    mib''8. re''16 do''8. sib'16 la'4\trill la' |
  }
  %% Licomede
  \tag #'(licomede basse) {
    \clef "vbasse" <>^\markup\character Licomede
    r2 r8 sol sib8. sol16 |
    re'8. re16 sol8. la16 fad4 fad |
    <<
      \tag #'basse { s1 s4. \ffclef "vbasse" <>^\markup\character Licomede }
      \tag #'licomede { R1 r4 r8 }
    >> re8 sol sol16 sol sol8 la16 si |
    do'4 r8 la16 la sib8 sib16 mib fa[ mib] fa8 |
    sib,4 <<
      \tag #'basse { s2. s4. \ffclef "vbasse" <>^\markup\character Licomede }
      \tag #'licomede { r4 r2 r4 r8 }
    >> mib'8 do'8\trill do' do'16 do' sib la |
    sib4 r16 sol sol la sib8. sib16 sib8. do'16 |
    re'4 re' <<
      \tag #'basse { s2 s1*3 s4.
        \ffclef "vbasse" <>^\markup\character Licomede }
      \tag #'licomede { r2 R1*3 r4 r8 }
    >> re8 sol8. la16 sib8 sib16 do' |
    re'8 re' r la16 la sib[ la] sol[ fa] mi8.\trill re16 |
    re1 |
    r4 r8 sol sol4. sol8 |
    la4. la8 la4. la8 |
    sib2 sib4. sib,8 |
    fa4. fa8 fa4. re8 |
    mib2 mib4. do8 |
    fa2 fa4. fa8 |
    sib,1 |
    r4 sib sib4. sib8 |
    la4. la8 sol4. sol8 |
    fa2 fa |
    r4 fa fa4. fa8 |
    mib4. mib8 re4. re8 |
    do2 do |
    r do4. re8 |
    mib2 mib4. fa8 |
    sol2 sol4. la8 |
    sib2 sib4. do'8 |
    re'2 re4. mi8 |
    fad2 fad4. fad8 |
    sol2 sol4. do8 |
    re2 re4. re8 |
    <<
      \tag #'basse { sol,4 s2. s1
        \ffclef "vbasse" <>^\markup\character Licomede }
      \tag #'licomede { sol,2 r R1 }
    >>
    r8 si16 si si8 si16 si do'8 do'16 do' sol8 la16 sib? |
    la4\trill fa16 fa fa fa sib8 do'16 re' |
    sol8\trill sol do'16 do' do' sib la8\trill la16 re' |
    sol4 <<
      \tag #'basse { s2. s2. s1 s2. s1*8 s4
        \ffclef "vbasse" <>^\markup\character Licomede }
      \tag #'licomede { r4 r2 R2. R1 R2. R1*8 r4 }
    >> sib8 sib16 sib sol4\trill sol8 sol16 sol |
    re8 re r16 re re re sol8. sol16 sol8. la16 |
    fad4 re' r8 fad16 fad sol sol sol sol |
    mi4.\trill mi8 fa8. fa16 fa8. mi16 |
    fa4 fa r8 fa fa16 sol la si |
    do'4 do'8 do' do'4\trill do'8 si |
    do'8 do'16 do' sol8\trill sol16 la sib sib sib sib sib8. la16 |
    la8\trill la r do' la\trill la16 do' fa8 sol16 la |
    sib8 sib r fa16 sol mib8\trill mib16 re |
    re4\trill sib8. sib16 sol8\trill sol do' sol16 do' |
    la8\trill la re' la16 la la8 sib16 do' |
    sib8\trill sol mib'8 mib'16 re' do' sib la sol |
    fad8 mi16 re sol8 sol16 do re8[ do16] re |
    sol,2
    \ffclef "vbasse" <>^\markup\character Straton
    r4 r8 re' |
    sib8\trill sib16 sib sol8\trill sol16 sol re8 re r16 re re mi |
    fad8 fad
    \ffclef "vbasse" <>^\markup\character Licomede
    r16 la la la re4 r16 re' re' re' |
    la\trill la si do' si8 si
    \tag #'licomede { r4 R1*2 }
  }
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    R1*14 R1*22 R1*3 R2.*2 R1 R2. R1 R2. R1*13 R1 R1*2 R2. R1 R2.*3
    R1 R1*2 R2. R1*2
  }
>>
%% Chœur
<<
  \tag #'(vdessus basse) {
    \clef "vdessus" <>^\markup\character Chœur
    r8 la' la' la' sib'8. sib'16 sib'8. sib'16 |
    do''8 do'' re'' re'' mib''8. re''16 do''4\trill |
    sib'8 re'' re'' re'' do''8. do''16 do''8. sib'16 |
    la'8. la'16 sib'8. sib'16 sib'8. do''16 la'4\trill |
    sol'2.
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r8 fad' fad' fad' sol'8. sol'16 sol'8. sol'16 |
    fa'8 fa' fa' fa' sol'8. fa'16 mib'4 |
    re'8 fa' fa' sol' la'8. la'16 la'8. sol'16 |
    fad'8. fad'16 sol'8. sol'16 sol'8. la'16 fad'4 |
    sol'2.
  }
  \tag #'vtaille {
    \clef "vtaille" r8 re' re' re' re'8. re'16 re'8. re'16 |
    do'8 do' sib sib sib8. sib16 la4 |
    sib8 sib sib sib la8. la16 re'8. re'16 |
    re'8. re'16 re'8. re'16 mib'8. mib'16 re'4 |
    si2.
  }
  \tag #'vbasse {
    \clef "vbasse" r8 re re re sol8. sol16 sol8. sol16 |
    la8 la sib sib mib8. sib,16 fa4 |
    sib,8 sib sib sib fad8. fad16 fad8. sol16 |
    re8. re16 sib,8. sib,16 mib8. do16 re4\trill |
    sol,2.
  }
  \tag #'(alceste licomede) { R1*4 r2 r4 }
>>
