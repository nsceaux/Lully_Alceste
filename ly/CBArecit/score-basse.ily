\score {
  \new Staff \notemode <<
    { \key re \minor \time 4/4
      \clef "bass" R1*14 |
      \digitTime\time 2/2 r2 r4
      _\markup\italic\right-align\right-column {
        L’amour se change en furie
        quand il est au désespoir.
      }
      \keepWithTag #'no-tag \includeNotes "basse"
      sol,2 r |
      R1*35 | \noBreak
      \grace s8_\markup\italic\right-align\right-column {
        Ah cruel, que n’espargnez-vous le sang qu’on va respandre
      }
      re,4 re sol4. sol8 |
      la4 sib mib8. sib,16 fa4 |
      sib,4. sib8 fad4. sol8 |
      re4 sib, mib8 do re4 |
      sol,2. \bar "|."
    }
  >>
  \layout {
    system-count = 4
  }
}
