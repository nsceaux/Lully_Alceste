\clef "basse"
<<
  \tag #'basse { R1*14 | r2 r4 }
  \tag #'basse-continue {
    sol,2~ sol,4 sol |
    fa mib re2~ |
    re fad, |
    sol,2. sol4 |
    mi fa re8. mib16 fa8 fa, |
    sib,1 |
    mib2 fa4 fad |
    sol2 mib |
    re2. do4 |
    sib,2 la, |
    sol,4 sol si,2 |
    do1 | \allowPageTurn
    re2 sib,8. la,16 sol,4 |
    fad,2 sol,4 la, |
    re,2.
  }
>> re4 |
sol2. sol4 |
la2. la4 |
sib2 sib4 sib, |
fa2. re4 |
mib2. do4 |
fa2. fa,4 |
sib,1 |
sib2. sib4 |
la2 sol |
fa fa, |
fa2. fa4 |
mib2 re |
do do, |
do2. re4 |
mib2 mib4. fa8 |
sol2 sol4. la8 |
sib2 sib4 do' |
re'2 re4 mi |
fad2 fad4. fad8 |
sol2 sol4 do |
re2 re, |
<<
  \tag #'basse {
    sol,2 r |
    R1*2 R2.*2 R1 R2. R1 R2. R1*13 R1 R1*2 R2. R1 R2.*3
    R1 R1*2 R2. R1*6 r2 r4
  }
  \tag #'basse-continue {
    sol,1 |
    sol~ |
    sol2 mi |
    fa2 re4 |
    mib4 do re8 re, |
    sol,4 sol2 fad4 |
    sol4. fa8 mib4 |
    re2 fad, |
    sol,8 sol mib4 si, |
    do2 mi, |
    fa,4 fa re8 sib, fa fa, |
    sib,4. do8 re4 mib |
    fa2 fad |
    sol fa4 mib |
    re2 si, |
    do4. re8 mib2 |
    re4 fad, sol, re, |
    sol,1~ |
    sol, | \allowPageTurn
    re2. si,4 |
    do2 la,8 sib, sol,4 |
    fa,2 fa |
    mib re |
    do sol4 mi |
    fa1 |
    re2 do4 |
    sib,2 mib4 mi |
    fa4 fad2 |
    sol4 do2 |
    re4 mib8 do re re, |
    sol,1~ |
    sol, |
    re~ |
    re4 sol,2 |
    sol8 fa mib re do2~ |
    do re |
    re,4 re sol4. sol8 |
    la4 sib mib8. sib,16 fa4 |
    sib,4. sib8 fad4. sol8 |
    re4 sib, mib8 do re4 |
    sol,2.
  }
>>
