\clef "dessus" R1*15 |
r4 r8 sib' sib'4. sib'8 |
do''4. do''8 do''4.\trill do''8 |
re''2 re''4. sib'8 |
la'4. la'8 la'4. sib'8 |
sol'2 sol'4. do''8 |
la'4. sib'8 do''4. la'8 |
sib'4. sib'8 sib'4. do''8 |
re''4. re''8 re''4. mi''8 |
fa''4. fa''8 fa''4. mi''8 |
fa''4. fa'8 fa'4. sol'8 |
la'4. la'8 la'4. si'8 |
do''4. do''8 do''4. si'8 |
do''4. si'8 do''4. re''8 |
mib''2 mib''4. fa''8 |
sol''2 fa''4. mib''8 |
re''2 re''4. do''8 |
sib'2 la'4. sol'8 |
fad'2 la'4. la'8 |
re''2 do''4. re''8 |
sib'2 sib'4. do''8 |
la'4. sib'8 la'4.\trill sol'8 |
sol'2 r2 |
R1*2 R2.*2 R1 R2. R1 R2. R1*13 R1 R1*2 R2. R1 R2.*3
R1 R1*2 R2. R1*6 r2 r4
