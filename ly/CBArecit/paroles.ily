\tag #'(licomede basse) {
  Al -- lons, al -- lons, la plainte est vai -- ne.
}
\tag #'(alceste basse) {
  Ah quel -- le ri -- gueur in -- hu -- mai -- ne !
}
\tag #'(licomede basse) {
  Al -- lons, je suis sourd à vos cris,
  je me van -- ge de vos mes -- pris.
}
\tag #'(alceste basse) {
  Quoy, vous se -- rez in -- ex -- o -- ra -- ble ?
}
\tag #'(licomede basse) {
  Cru -- el -- le, vous m’a -- vez a -- pris
  a de -- ve -- nir im -- pi -- toy -- a -- ble.
}
\tag #'(alceste basse) {
  Est-ce ain -- si que l’a -- mour a sçeu vous é -- mou -- voir ?
  Est-ce ain -- si que pour moy vostre ame est at -- ten -- dri -- e ?
}
\tag #'(licomede basse) {
  L’a -- mour se change en fu -- ri -- e
  quand il est au dé -- ses -- poir.
  Puis que je perds toute es -- pe -- ran -- ce,
  je veux de -- ses -- pe -- rer mon ri -- val à son tour ;
  et les dou -- ceurs de la ven -- gean -- ce
  et les dou -- ceurs de la ven -- gean -- ce
  ont de -- quoy con -- so -- ler les ri -- gueurs de l’a -- mour,
  ont de -- quoy con -- so -- ler les ri -- gueurs de l’a -- mour.
}
\tag #'(alceste basse) {
  Voy -- ez la dou -- leur qui m’ac -- ca -- ble.
}
\tag #'(licomede basse) {
  Vous a -- vez sans pi -- tié re -- gar -- dé ma dou -- leur.
  Vous m’a -- vez ren -- du mi -- se -- ra -- ble
  vous par -- ta -- ge -- rez mon mal- heur.
}
\tag #'(alceste basse) {
  Ad -- mete a -- voit mon cœur dés ma plus tendre en -- fan -- ce ;
  nous ne con -- nois -- sions pas l’a -- mour ny sa puis -- san -- ce.
  Lors que d’un nœud fa -- tal il vint nous en -- chais -- ner :
  ce n’est pas u -- ne grande of -- fen -- ce
  que le re -- fus d’un cœur qui n’est plus à don -- ner.
  Ce n’est pas u -- ne grande of -- fen -- ce
  que le re -- fus d’un cœur qui n’est plus à don -- ner.
}
\tag #'(licomede basse) {
  Est-ce aux a -- mants qu’on de -- ses -- pe -- re
  a de -- voir rien ex -- a -- mi -- ner ?
  Non, je ne puis vous par -- don -- ner
  d’a -- voir trop sçeu me plai -- re.
  Que ne m’ont point cous -- té vos fu -- nes -- tes at -- traits !
  Ils ont mis dans mon cœur u -- ne cru -- el -- le fla -- me,
  ils ont ar -- ra -- ché de mon a -- me
  l’in -- no -- cence, & la paix.
  Non, in -- gra -- te, non, in -- hu -- mai -- ne,
  non, quel -- que soit vos -- tre pei -- ne,
  non, je ne vous ren -- dray ja -- mais
  tous les maux que vous m’a -- vez faits.

  % Straton
  Voi -- cy l’en -- ne -- my qui s’a -- van -- ce
  en di -- li -- gen -- ce.

  % Licomede
  Pre -- pa -- rons- nous
  pre -- pa -- rons- nous
  a nous de -- fen -- dre.
}
\tag #'(alceste basse) {
  Ah cru -- el, que n’es -- par -- gnez- vous
  le sang qu’on va res -- pan -- dre !
}
\tag #'(choeur basse) {
  Pe -- ris -- sons tous
  pe -- ris -- sons tous
  plû -- tost que de nous ren -- dre.
  Pe -- ris -- sons tous
  pe -- ris -- sons tous
  plû -- tost que de nous ren -- dre.
}
