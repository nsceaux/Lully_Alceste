<<
  %% Dessus
  \tag #'(vdessus basse) {
    <>^\markup\character Chœur des Thessaliens
    \clef "vdessus" r4 mi'' do''4. do''8 |
    fa''4 fa'' re''4.\trill re''8 |
    mi''4. mi''8 mi''4. do''8 |
    do''4. re''8 si'4.\trill la'8 |
    la'2. |
  }
  %% Haute-contre
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r4 mi' mi'4. mi'8 |
    re'4 re' re'4. sol'8 |
    sol'4. sol'8 mi'4. mi'8 |
    fa'4. fa'8 mi'4. si8 |
    dod'2. |
  }
  %% Taille
  \tag #'vtaille {
    \clef "vtaille" r4 sold la4. la8 |
    la4 la si4. si8 |
    do'4. do'8 si4. la8 |
    la4. la8 sold4. la8 |
    la2. |
  }
  %% Basse
  \tag #'vbasse {
    \clef "vbasse" r4 mi la4. la8 |
    re4 re sol4. sol8 |
    do4. do'8 sold4. la8 fa4. re8 mi4. mi8 |
    la2. |
  }
>>
<<
  %% Pheres
  \tag #'(vtaille basse) {
    \ffclef "vtaille" <>^\markup\character Pheres
    r4 r mi'8. fa'16 |
    re'2 do'8\trill si |
    do'2 r8 do' |
    la4.\trill la8 si do' |
    si4\trill si \tag #'basse \clef "vdessus"
  }
  \tag #'(vdessus vhaute-contre vbasse) { R2.*4 r4 r }
>>
<<
  %% Dessus
  \tag #'(vdessus basse) {
    <>^\markup\character Chœur
    r8 si' |
    do''8. do''16 re''8. re''16 mi''8. fa''16 |
    re''4.\trill re''8 sol''8. fa''16 |
    mi''2
  }
  %% Haute-contre
  \tag #'vhaute-contre {
    r8 sol' |
    sol'8. sol'16 sol'8. sol'16 sol'8. sol'16 |
    sol'4. sol'8 sol'8. la'16 |
    sol'2
  }
  %% Taille
  \tag #'vtaille {
    r8 re' |
    mi'8. mi'16 re'8. re'16 do'8. do'16 |
    si4. si8 do'8. do'16 |
    do'2
  }
  %% Basse
  \tag #'vbasse {
    r8 sol |
    do'8. do'16 si8. si16 do'8. do'16 |
    sol4. sol8 mi8. fa16 |
    do2
  }
>>
<<
  %% Alceste
  \tag #'(vdessus basse) {
    \ffclef "vbas-dessus" <>^\markup\character Alceste
    sol'8. la'16 |
    si'2\trill si'8. dod''16 |
    re''4. la'8 la'8. si'16 |
    do''8. re''16 do''4( si')\trill |
    do''2 do''8. do''16 |
    do''2 si'4 |
    si'4. do''8 la'4 |
    sold'1 |
  }
  %% Pheres
  \tag #'vtaille {
    \ffclef "vtaille" <>^\markup\character Pheres
    mi'8. mi'16 |
    re'2 re'8. mi'16 |
    fa'4. fa'8 fa'8. sol'16 |
    mi'8. fa'16 mi'4( re')\trill |
    do'2 mi'8. mi'16 |
    mi'2 re'4 |
    re'4. mi'8 do'4 |
    si1\trill |
  }
  \tag #'(vhaute-contre vbasse) { r4 R2.*6 R1 }
>>
<<
  %% Dessus
  \tag #'(vdessus basse) {
    <>^\markup\character Chœur
    r4 mi'' do''4. do''8 |
    fa''4. fa''8 re''4.\trill re''8 |
    mi''4. mi''8 mi''4. do''8 |
    do''4. re''8 si'4.\trill la'8 |
    la'1 |
  }
  %% Haute-contre
  \tag #'vhaute-contre {
    r4 mi' mi'4. mi'8 |
    re'4. re'8 re'4. sol'8 |
    sol'4. sol'8 mi'4. mi'8 |
    fa'4. fa'8 mi'4. si8 |
    dod'1 |
  }
  %% Taille
  \tag #'vtaille {
    r4 sold la4. la8 |
    la4. la8 si4. si8 |
    do'4. do'8 si4. la8 |
    la4. la8 sold4. la8 |
    la1 |
  }
  %% Basse
  \tag #'vbasse {
    r4 mi la4. la8 |
    re4. re8 sol4. sol8 |
    do4. do'8 sold4. la8 |
    fa4. re8 mi4. mi8 |
    la,1 |
  }
>>