\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
}
\tag #'(vtaille basse) {
  Joü -- is -- sez des dou -- ceurs du nœud qui vous as -- sem -- ble.
}\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Vi -- vez, vi -- vez, heu -- reux es -- poux,
  heu -- reux es -- poux.
}
\tag #'(vdessus vtaille basse) {
  Quand l’hi -- men & l’a -- mour sont bien d’ac -- cord en -- sem -- ble
  Que les nœuds qu’ils for -- ment sont doux !
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
}

