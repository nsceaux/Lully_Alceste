\clef "basse" mi,4 mi la4. la8 |
re4. re8 sol4. sol8 |
do4. do'8 sold4. la8 |
fa4. re8 mi4 mi, |
la,2. |
<<
  \tag #'basse { R2.*4 r4 r }
  \tag #'basse-continue {
    la2.~ |
    la4 sold2 |
    la la4 |
    fad2 fad4 |
    sol2
  }
>> sol4 |
do'8. do'16 si8. si16 do'8. do'16 |
sol4. sol8 mi8. fa16 |
do2 <<
  \tag #'basse { r4 R2.*6 R1 }
  \tag #'basse-continue {
    do4 |
    sol2 fa8. mi16 |
    re2 re4 |
    la8 fa sol4 sol, |
    do2 la,4 |
    re2 re4 |
    mi2 la,4 |
    mi,1 |
  }
>>
mi,4 mi la4. la8 |
re4. re8 sol4. sol8 |
do4. do'8 sold4. la8 |
fa4. re8 mi4 mi, |
<<
  \tag #'basse { la,1 }
  \tag #'basse-continue {
    la,2 si, | \once\set Staff.whichBar = "|"
    do2. \bar "|."
  }
>>
