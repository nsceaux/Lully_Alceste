\bookpart {
  \paper { page-count = 5 }

  %% 1-1
  \act "Acte Premier"
  \sceneDescription\markup\wordwrap-center {
    Le Théatre Représente un Port de Mer.
  }
  \scene "Scene Premiere" "Scene I"
  \sceneDescription\markup\wordwrap-center {
    Le Chœur des Thessaliens, Alcide, Lycas
  }
  \pieceToc\markup\wordwrap {
    Chœur, Lycas, Alcide : \italic { Vivez, vivez, heureux Espoux }
  }
  \includeScore "BAAchoeurRecit"
  
  %% 1-2
  \scene "Scene II" "Scene II"
  \sceneDescription\markup\wordwrap-center {
    Alcide, Straton, & Lycas
  }
  \pieceToc\markup\wordwrap {
    Alcide, Straton, Lycas : \italic { L’amour a bien des maux }
  }
  \includeScore "BBAtrio"
}

\bookpart {
  %% 1-3
  \scene "Scene III" "Scene III"
  \sceneDescription\markup\wordwrap-center {
    Straton, Lychas
  }
  \pieceToc\markup\wordwrap {
    Straton, Lychas : \italic { Lychas, j’ay deux mots à te dire. }
  }
  \includeScore "BCArecit"
}

\bookpart {
  \paper { page-count = 6 }
  %% 1-4
  \scene "Scene Quatriéme" "Scene IV"
  \sceneDescription\markup\wordwrap-center {
    Céphise, Straton
  }
  \pieceToc\markup\wordwrap {
    Céphise, Straton : \italic { Dans ce beau jour, quelle humeur sombre }
  }
  \includeScore "BDAritournelle"
  \includeScore "BDBrecit"
}
\bookpart {
  \paper { page-count = 5 }
  %% 1-5
  \scene "Scene Cinquiéme" "Scene V"
  \sceneDescription\markup\wordwrap-center {
    Licomede, Straton, Céphise
  }
  \pieceToc\markup\wordwrap {
    Licomede, Céphise : \italic { Straton, donne ordre qu'on s'apreste }
  }
  \includeScore "BEArecit"

  %% 1-6
  \scene "Scene Sixiéme" "Scene VI"
  \sceneDescription\markup\wordwrap-center {
    Pheres, Admete, et Alceste
  }
  \pieceToc\markup\wordwrap {
    Chœur, Pheres, Alceste : \italic { Vivez, vivez, heureux Espoux }
  }
  \includeScore "BFAchoeur"
}

\bookpart {
  %% 1-7
  \scene "Scene Septiême" "Scene VII"
  \sceneDescription\markup\wordwrap-center {
    Des Nymphes de la Mer et des Tritons
    viennent faire une fête marine
  }
  \pieceToc\markup\wordwrap { Air pour les matelots }
  \includeScore "BGAair"
}
\bookpart {
  %% 1-8
  \pieceToc\markup\wordwrap {
    Deux Tritons : \italic { Malgré tant d’orages }
  }
  \includeScore "BGBtritons"
  \includeScore "BGChautbois"
}
\bookpart {
  %% 1-9
  \pieceToc\markup { \concat { 2 \super e } Air – Gavotte }
  \includeScore "BGDgavotte"
}
\bookpart {
  %% 1-10
  \pieceToc\markup\wordwrap {
    Céphise, une nymphe, chœur :
    \italic { Jeunes Cœurs laissez-vous prendre }
  }
  \includeScore "BGEchoeur"
}
\bookpart {
  %% 1-11
  \pieceToc\markup { \concat { 3 \super e } Air – Rondeau }
  \includeScore "BGFrondeau"

  %% 1-12
  \pieceToc\markup\wordwrap {
    Céphise, une nymphe, chœur :
    \italic { Plus les ames sont rebelles }
  }
  \includeScore "BGGchoeur"
}
\bookpart {
  \paper { page-count = 3 }
  %% 13
  \pieceToc\markup\wordwrap {
    Licomede, Straton, Admete, Alcide, Alceste, Céphise, chœur :
    \italic { On vous apreste }
  }
  \includeScore "BGHrecit"
  \noPageBreak
  %% 14
  \scene "Scene Huitiême" "Scene VIII"
  \sceneDescription\markup\wordwrap-center {
    Thetis, Admete, Alceste
  }
  \pieceToc\markup\wordwrap {
    Thetis, Admete, Alcide, chœur :
    \italic { Espoux infortuné redoute ma colere }
  }
  \includeScore "BHArecit"
}
\bookpart {
  \paper { page-count = 1 }
  %% 15
  \pieceToc "Les Vents"
  \includeScore "BHBvents"
}
\bookpart {
  \paper { page-count = 2 }
  %% 16
  \scene "Scene Neuviême" "Scene IX"
  \sceneDescription\markup\wordwrap-center {
    Eole, les Aquilons, et les zephirs
  }
  \pieceToc\markup\wordwrap {
    Eole : \italic { Le Ciel protege les Heros }
  }
  \includeScore "BIAeole"
}
\bookpart {
  %% 17
  \pieceToc "Entr’acte"
  \reIncludeScore "BGFrondeau" "BIBentracte"
  \actEnd "Fin du Premier Acte"
}
