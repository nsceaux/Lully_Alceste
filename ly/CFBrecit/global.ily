\set Score.currentBarNumber = 11 \bar ""
\key sol \major
\time 4/4 \midiTempo#80 s1*7
\digitTime\time 3/4 s2.*4
\time 4/4 s1*2
\digitTime\time 3/4 s2.
\time 2/2 \midiTempo#160 s1
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 s2.
\time 4/4 s1*5
\digitTime\time 3/4 s2.*3
\time 4/4 s1*4
