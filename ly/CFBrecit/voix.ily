<<
  \tag #'(alceste basse recit) {
    <<
      \tag #'(basse recit) {
        s1*5 s4 \ffclef "vbas-dessus"
      }
      \tag #'alceste {
        \clef "vbas-dessus" R1*5 r4
      }
    >> <>^\markup\character Alceste
    r16 mi'' mi'' mi'' si'8. si'16 sol'8. si'16 |
    mi'8 mi' r sol' sol'8. mi'16 la' mi' fad' sol' |
    fad'4\trill si'8 si'16 si' si'8. si'16 |
    sold'8 sold' <<
      \tag #'(basse recit) {
        s2 s2.*2 s4 \ffclef "vbas-dessus" <>^\markup\character Alceste
      }
      \tag #'alceste { r4 r R2.*2 r4 }
    >> r8 fad' sol'16 sol' la' si' do''8. re''16 |
    si'16\trill si' r si' si' si' do'' re'' la'8.\trill re''16 sol' sol' sol' fad' |
    fad'8\trill fad' r re''16 do'' si'8\trill si'16 si' |
    do''4 do''8 do'' re''4 mi''8 fa'' |
    mi''4\trill <<
      \tag #'(basse recit) {
        s4 s2 s4 \ffclef "vbas-dessus" <>^\markup\character Alceste
      }
      \tag #'alceste { r4 r2 r4 }
    >> r8 sol' si'\trill si'16 do'' re''8 mi''16 fad'' |
    sol''8 sol'' r re'' mi''8. mi''16 re''8.\trill do''16 |
    si'4\trill r16 si' do'' re'' do''8.\trill si'16 |
    la'4\trill re''8 re''16 re'' sold'8. sold'16 sold'8. la'16 |
    la'8 la' r do''16 do'' do''8 do''16 do'' do''8. si'16 |
    si'8\trill si' r si'16 re'' sol'8. fa'16 fa'8 fa'16 mi' |
    mi'4\trill
    \tag #'alceste { r4 r2 R1 R2.*3 R1*4 }
  }
  \tag #'(alcide basse recit) {
    \clef "vbasse-taille" <>^\markup\character Alcide
    r2 r4 r8 re |
    sol8. sol16 sol8. la16 si8 si16 si si8 do'16 re' |
    sol4 sol <<
      \tag #'(basse recit) {
        s2 s2. \ffclef "vbasse-taille" <>^\markup\character Alcide
      }
      \tag #'alcide { r2 r2 r4 }
    >> r8 re |
    la4 r16 la la si do'8. do'16 do' do' do' si |
    si4\trill <<
      \tag #'(basse recit) {
        s2. s1 s2. s4 \ffclef "vbasse-taille" <>^\markup\character Alcide
      }
      \tag #'alcide { r4 r2 R1 R2. r4 }
    >> r16 mi mi mi la la la la |
    re8. re16 sol8. sol16 sol8. la16 |
    si8\trill si16 do' re'4 r8 re'16 dod' |
    re'4 <<
      \tag #'(basse recit) {
        s2. s1 s2. s1 s4
        \ffclef "vbasse-taille" <>^\markup\character Alcide
      }
      \tag #'alcide { r4 r2 R1 R2. R1 r4 }
    >> r16 do' do' mi' la4 la8 la16 si |
    sol4 <<
      \tag #'(basse recit) {
        s2. s1 s2. s1*3 s4
        \ffclef "vbasse-taille" <>^\markup\character Alcide
      }
      \tag #'alcide { r4 r2 R1 R2. R1*3 r4 }
    >> r16 do' do' mi' la4\trill la8 la16 si |
    sol4 r8 si fad4\trill r16 si si si |
    sold8.\trill sold16 la8 la la si |
    do'8 do' mi'4 do'16 do' do' mi' |
    la8. re'16 re'8. do'16 do'8. si16 |
    si8\trill si r si16 re' sol8. sol16 sol8 sol16 fad |
    fad4\trill re' r8 la fad\trill fad16 la |
    re4. re'8 si\trill si do' re' |
    mi' mi' r16 do' do' mi' la4\trill la8 la16 si |
    sol2*1/2
  }
  \tag #'(pheres basse recit) {
    <<
      \tag #'(basse recit) {
        s1*2 s2 \ffclef "vtaille"
      }
      \tag #'pheres {
        \clef "vtaille" R1*2 r2
      }
    >> <>^\markup\character Pheres
    r8 re' si16\trill si si32[ la] si16 |
    do'8. mi'16 do'\trill do' do' do' la4\trill
    \tag #'pheres {
      r4 R1*3 R2.*4 R1*2
    }
  }
>>
