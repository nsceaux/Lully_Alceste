\score {
  \new ChoirStaff <<
    \new Staff \withRecit <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2 s2 \bar "" \break s2 s1\break s1*2\pageBreak
        s1\break s2.*2\break s2.*2 s2 \bar "" \break
        s2 s \bar "" \break s2 s2.\break s1*2\pageBreak
        s1*2\break s2. s1\break s1 s2 \bar "" \break
        s2 s1\break s1 s2.\break s2.*2\pageBreak
        s1*2\break
      }
    >>
  >>
  \layout { indent = \noindent }
  \midi { }
}