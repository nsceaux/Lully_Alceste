\tag #'(alcide basse recit) {
  Ren -- dez à vos -- tre fils cette ai -- ma -- ble prin -- ces -- se.
}
\tag #'(pheres basse recit) {
  Ce don de vos -- tre main se -- roit en -- cor plus doux.
}
\tag #'(alcide basse recit) {
  Al -- lez, al -- lez la rendre à son heu -- reux es -- poux.
}
\tag #'(alceste basse recit) {
  Tout est soû -- mis, la guer -- re ces -- se ;
  sei -- gneur, pour -- quoy me lais -- sez- vous ?
  Quel nou -- veau soin vous pres -- se ?
}
\tag #'(alcide basse recit) {
  Vous n’a -- vez rien à re -- dou -- ter,
  je vais cher -- cher ail -- leurs des ty -- rans à domp -- ter.
}
\tag #'(alceste basse recit) {
  Les nœuds d’une a -- mi -- tié pres -- san -- te
  ne re -- tien -- dront- ils point vostre ame im -- pa -- ti -- en -- te ?
  Et la gloi -- re toû -- jours vous doit- elle em -- por -- ter ?
}
\tag #'(alcide basse recit) {
  Gar -- dez- vous bien de m’ar -- res -- ter.
}
\tag #'(alceste basse recit) {
  C’est vos -- tre va -- leur tri -- om -- phan -- te
  qui fait le sort char -- mant que nous al -- lons goû -- ter ;
  Quel -- que dou -- ceur que l’on res -- sen -- te,
  un a -- my tel que vous l’aug -- men -- te,
  vou -- lez- vous si- tost nous quit -- ter ?
}
\tag #'(alcide basse recit) {
  Gar -- dez- vous bien de m’ar -- res -- ter.
  Lais -- sez, lais -- sez - moy fuïr un char -- me qui m’en -- chan -- te :
  non, tou -- te ma ver -- tu n’est pas as -- sez puis -- san -- te
  pour ré -- pon -- dre d’y re -- sis -- ter.
  Non, en -- core u -- ne fois, prin -- ces -- se trop char -- man -- te,
  gar -- dez- vous bien de m’ar -- res -- ter.
}
