\clef "basse" sol,1~ |
sol,~ |
sol,2 sol |
do re~ |
re4 do8. si,16 la,2 |
mi1~ |
mi2 dod |
re4 red2 |
mi dod4 |
re8. do16 si,4. la,8 |
sol,4 fad, mi, |
re4. do8 si,4 la, |
sol, sol fad mi |
re2 sol4 |
mi2 re |
do re4 re, |
sol,2 sol~ |
sol mi4 fad |
sol2 do4 |
re2 si, |
la,4 la fad2 |
sol si, |
do re4 re, |
sol, sol red2 |
mi8. re16 do4. si,8 |
la,2 la,8. sol,16 |
fad,2. |
sol,2 mi, |
re, re~ |
re sol8 fa mi re |
do2 re4 re, |
\custosNote sol,4
