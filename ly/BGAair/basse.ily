\clef "basse" do'4 si2 |
do' do4 sol sol2 |
la2 la4 si2 si4 |
do' sol la fa sol sol, |
do2 do4 do' fa2 |
do do4 fa do2 |
sol, sol,4 sol do2 |
si, mi4 do re re, |
sol,2 sol4 mi la2 |
re sib4 sol la2 |
re4. do8 sib,4 sol, la,2 |
re, re4 la sol fa |
mi2 la4 fa sol sol, |
do2 do4
