\clef "dessus" sol''4 fa''2\trill |
mi''4.\trill re''8 do''4 re'' si'4.(\trill la'16 si') |
do''4. re''8 mi''4 re''4. mi''8 fa''4 |
mi''4.\trill re''8 do''4 re'' si'4.(\trill la'16 si') |
do''2 do''4 sol''4 la''2 |
sol''2\trill sol''4 fa'' mi''2\trill |
re''\trill re''4 re'' mi''2 |
re''4.\trill do''8 si'4 mi'' la'2\trill |
sol' sol'4 sol'' mi''2\trill |
fa''4. sol''8 fa''4 fa'' mi''2\trill |
fa''4. mi''8 re''4 mi'' dod''4.(\trill si'16 dod'') |
re''4. mi''8 fa''4 mi''\trill mi'' fa'' |
sol''4. fa''8 mi''4 la'' re''4.\trill do''8 |
do''2 do''4
