\clef "taille" sol'4 sol'2 |
sol'4. fa'8 mi'4 re' re'2 |
do' do'4 si do' re' |
mi'2 mi'4 re'2 sol'4 |
sol'2 sol'4 sol' fa'2 |
mi' mi'4 la' sol'2 |
sol' sol'4 sol' sol'2 |
sol' sol4 do' do'2 |
si si4 mi' mi'2 |
re' re'4 re' la'2 |
fa'4 fa'2 sol'4 mi'2 |
re' re'4 mi' re'2 |
do' do'4 fa' fa'2 |
mi'2 mi'4
