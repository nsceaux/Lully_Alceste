\piecePartSpecs
#`((dessus         #:music , #{ s2. s1.*3 s2.\break s2. s1.*4\break #})
   (haute-contre   #:music , #{ s2. s1.*3 s2.\break s2. s1.*4\break #})
   (taille         #:music , #{ s2. s1.*3 s2.\break s2. s1.*4\break #})
   (quinte         #:music , #{ s2. s1.*3 s2.\break s2. s1.*4\break #})
   (basse          #:music , #{ s2. s1.*3 s2.\break s2. s1.*4\break #})
   (basse-continue #:music , #{ s2. s1.*3 s2.\break s2. s1.*4\break #})
   (silence #:on-the-fly-markup , #{ \markup\tacet#16 #}))
