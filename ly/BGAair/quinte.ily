\clef "quinte" mi' re'2 |
do' do'4 si sol2 |
do' do'4 re'2 re'4 |
do' si la la sol2 |
sol do'4 mi' fa'2 |
sol' do'4 do' do'2 |
re' re'4 re' do'2 |
re' mi'4 mi' re'2 |
re' re'4 sol la2 |
la sib4 sib la2 |
la4 fa2 mi mi4 |
fa4. sol8 la4 la2 la4 |
sol2 la4 la sol2 |
sol sol4
