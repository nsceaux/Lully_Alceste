\clef "haute-contre" do''4 re''2 |
sol' sol'4 sol' fa'2 |
mi'4. re'8 do'4 sol'2 sol'4 |
sol'4. fa'8 mi'4 fa' re'2 |
mi' mi'4 do'' do''2 |
do'' do''4 do'' do''2 |
si' si'4 si' do''2 |
sol' sol'4 sol' fad'2 |
sol' sol'4 si' dod''2 |
re'' re''4 re'' dod''2 |
re'' re''4 sib' la'2 |
la' la'8 si' do''4 do'' re'' |
mi''4. re''8 do''4 re'' si'2 |
do''2 do''4
