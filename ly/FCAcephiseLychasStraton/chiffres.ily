s1 <5>4 <6> <\markup\triangle-down 7 6 \figure-flat >2
s2. <6 5 _->4 \new FiguredBass { <_+>2\figExtOn <_+>4\figExtOff }
<_+>2 <6>4
<7>2 <_+>4 s2 <6>4 <4>8 <3> s2 <9 7>4 <8 6> <_+> <6> <7>8 <6 5> <_+>4
<_+>2.\figExtOn <_+>4\figExtOff s4 <6>2. <6>1 <6->4. <6>8 s4 <_->
s2 <_-> <6 5/>2 <4>4 <3> s2 <6> s1 <6->2. <6 5 _->4 <_+>1
<_+>4 <7- _-> <4> <_+> <_->4\figExtOn <_->\figExtOff <6>2
<_->2 <6+> <6>\figExtOn <6>\figExtOff s <6 4+ 3>
<_+>4\figExtOn <_+>\figExtOff <6>2 s <6+>
<6> <_+>4\figExtOn <_+>\figExtOff <6>2 <6>
<7 _->4 <6 5> \new FiguredBass { <_+>\figExtOn <_+>\figExtOff }
s1 <6->2. <6 5 _->4 <_+>2.\figExtOn <_+>4\figExtOff
<_+> <7- _-> <4> <_+> <_->2 <6> <_-> <6 5 _->
<_+>2.\figExtOn <_+>4\figExtOff s2 <6>
<_+>4\figExtOn <_+>\figExtOff <6>2 <_-> <6 5 _->
<_+>2.\figExtOn <_+>4\figExtOff <6>2 <6>
<6 5 _-> \new FiguredBass { <_+>4\figExtOn <_+>\figExtOff }
s1 s2 <6>4 <5/> s2.
%% chœur
s2.*5 s2 s8 <_-> s2. <_+>2 <6>4 <_-> <_+>2
s2.*2 s2 <6>4 <6 5> <7->2
