\tag #'(lychas straton basse) {
  Voy, Cé -- phi -- se, voy qui de nous
  peut ren -- dre ton des -- tin plus doux,
  et ter -- mine en -- fin nos que -- rel -- les.
}
\tag #'(lychas basse) {
  Mes a -- mours se -- ront e -- ter -- nel -- les.
}
\tag #'(straton basse) {
  Mon cœur ne se -- ra plus ja -- loux.
}
\tag #'(lychas straton basse) {
  En -- tre deux a -- mants fi -- del -- les,
  choi -- sis un heu -- reux es -- poux.
}
\tag #'(cephise basse) {
  Je n’ay point de choix à fai -- re ;
  par -- lons d’ai -- mer & de plai -- re,
  et vi -- vons toû -- jours en paix.
  Je n’ay   paix.
  L’hi -- men dé -- truit la ten -- dres -- se
  il rend l’a -- mour sans at -- traits ;
  vou -- lez- vous ai -- mer sans ces -- se,
  a -- mants, n’es -- pou -- sez ja -- mais.
  Vou -- lez- vous ai -- mer sans ces -- se,
  a -- mants, a -- mants, n’es -- pou -- sez ja -- mais.
}
\tag #'(cephise lychas straton basse) {
  L’hi -- men dé -- truit la ten -- dres -- se
  il rend l’a -- mour sans at -- traits ;
  vou -- lez- vous
  \tag #'(cephise basse straton) { vou -- lez- vous }
  ai -- mer sans ces -- se,
  a -- mants, n’es -- pou -- sez ja -- mais.
  Vou -- lez- vous
  \tag #'(straton) { vou -- lez- vous }
  ai -- mer sans ces -- se,
  a -- mants, a -- mants, n’es -- pou -- sez ja -- mais.
}
\tag #'(cephise basse) {
  Pre -- nons part aux trans -- ports d’u -- ne joye é -- cla -- tan -- te :
}
\tag #'(vdessus vhaute-contre vtaille vbasse basse) {
  Que cha -- cun chan -- te,
  que cha -- cun chan -- te,
  Al -- cide est vain -- queur du tré -- pas,
  l’en -- fer ne luy re -- sis -- te pas.
  L’en -- fer, l’en -- fer ne luy re -- sis -- te pas.
}
