\key re \minor
\time 4/4 \midiTempo#80 s1*3
\digitTime\time 3/4 \midiTempo#160 s2.*3
\time 4/4 \midiTempo#80 s1*3
\time 2/2 \midiTempo#160 s1
\bar "|!:" \segnoMark s1*5 \alternatives s1 s1
s1*26 \bar "||"
\time 4/4 \midiTempo#80 s1
\digitTime\time 3/4 \midiTempo160 s2.*15 \bar "|."
