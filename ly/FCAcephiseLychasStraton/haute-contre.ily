\clef "haute-contre" R1*3 R2.*3 R1*3 R1*35 R2.
sib'4 sib' la' |
sib'2 sib'4 |
la' la' sol' |
la' la' la' |
sib' sib' sib' |
sib' sib' do'' |
la'2 re''4 |
si'4. si'8 do''4 |
do'' si'4. do''8 |
do''2 sol'4 |
fa'2 la'4 |
sib'4. do''8 re''4 |
sib' la'4. sib'8 |
sib'2 r4 |
