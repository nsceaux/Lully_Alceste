<<
  %% Céphise
  \tag #'(cephise basse) {
    <<
      \tag #'basse { s1*3 s2.*3 s1*3 s2 \ffclef "vdessus" }
      \tag #'cephise { \clef "vdessus" R1*3 R2.*3 R1*3 r2 }
    >> <>^\markup\character Céphise
    r4 re''8 la' |
    sib'4 do'' re'' sib' |
    fa''2 re''4.\trill re''8 |
    mib''4. fa''8 mib''4\trill mib''8. re''16 |
    do''4\trill do'' r mib''8 mib'' |
    mib''4 re'' do''4.\trill sib'8 |
    sib'2 r4 re''8 la' |
    sib'2 r4 re'' |
    mib''4. mib''8 do''4\trill do''8 re'' |
    si'2 si'4 re'' |
    re''4. mib''8 re''4\trill re''8 mib'' |
    do''2 r4 sol''8 fa'' |
    mib''4.\trill re''8 do''4.\trill sib'8 |
    la'2\trill la'4 r8 la' |
    sib'4 sib'8 do'' do''4.\trill re''8 |
    re''2 r4 sol''8 fa'' |
    mib''4.\trill re''8 do''4.\trill sib'8 |
    la'2\trill la'4 r8 re'' |
    re''2 r4 re'' |
    mib'' do''8 sib' la'4.\trill sol'8 |
    sol'2 r4 re'' |
    mib''4. mib''8 do''4\trill do''8 re'' |
    si'2 si'4 re'' |
    re'' mib'' re''\trill re''8 mib'' |
    do''4 mib''8 mib'' re''4 re''8 re'' |
    do''4.\trill sib'8 la'4.\trill sol'8 |
    fad'4 fad' r la' |
    sib' sib'8. do''16 do''4.\trill re''8 |
    re''2 r4 re''8 re'' |
    do''4. sib'8 la'4. sol'8 |
    fad'4 fad' r sol' |
    sib'2 r4 sib' |
    sol' sol'8 la' fad'4. sol'8 |
    sol'2 r |
    r8 sib'16 do'' re''8 re''16 mi'' fa''8 fa''16 fa'' do''8 re''16 mib'' |
    re''2\trill re''4 |
  }
  %% Lychas
  \tag #'(lychas basse) {
    \clef "vhaute-contre" <>^\markup\character Lychas
    r4 re'8. re'16 sib8 sib re' re'16 mi' |
    fa'8. fa'16 fa'8. re'16 mib'8. mib'16 mib'8[ re'16] mib' |
    re'4\trill re'8. re'16 sib8.\trill sib16 do'8 do'16 do' |
    la4\trill la re'8. do'16 |
    si2 r8 do' |
    do'2 do'8 si |
    do'4 <<
      \tag #'basse {
        do'8 s s2 s4 \ffclef "vhaute-contre" <>^\markup\character Lychas
      }
      \tag #'lychas { do'4 r2 | r4 }
    >> fa'8. fa'16 fa'8. sol'16 mi'8.\trill re'16 |
    dod'8 dod' r fa' re' mi'16 re' dod'8. re'16 |
    re'2
    \tag #'lychas {
      r2 R1*19 |
      r2 r4 <>^\markup\character Lychas fa' |
      sol'4. sol'8 mib'4 mib'8 fa' |
      re'2 re'4 si |
      si? do' do' si!8 do' |
      do'2 r4 sol'8 fa' |
      mib'4. re'8 do'4.\trill sib8 |
      la4\trill la r fad' |
      sol' re'8 re' sol'4. sol'8 |
      fad'2 r4 sol'8 fa'? |
      mib'4. re'8 do'4. sib8 |
      la4 la r re' |
      re'2 r4 re' |
      mib' do'8 sib la4.\trill sol8 |
      sol2 r |
      R1 R2. |
    }
  }
  %% Straton
  \tag #'(straton basse) {
    <<
      \tag #'basse {
        s1*3 s2.*3 s4. \ffclef "vbasse" <>^\markup\character Straton
      }
      \tag #'straton {
        \clef "vbasse" <>^\markup\character Straton
        r4 sib8. sib16 sol8 sol sib sib16 do' |
        la8. la16 sib8. sib16 sib8. la16 la8. sib16 |
        sib4 sib8. sib16 sol8. sol16 la8 la16 la |
        fad4 fad r |
        R2.*2 |
        r4 r8
      }
    >> do8 fa16 fa fa sib, do8. do16 |
    fa,4
    \tag #'straton {
      la8. la16 la8. sib16 sol8.\trill fa16 |
      mi8 mi r la sib sol16 fa mi8.\trill re16 |
      re2 r |
      R1*19 |
      r2 r4 <>^\markup\character Straton sib |
      sol4. sol8 lab4 lab8 fa |
      sol2 sol4 sol |
      sol fa sol sol8 sol, |
      do4 do'8 do' si4 si8 si |
      do'4. do'8 do4. do8 |
      re4 re r re |
      sol4 sol8 fa mib4. re8 |
      re4 re'8 do' si4 si8 si |
      do'4. do'8 do4. do8 |
      re4 re r re |
      sib,2 r4 sol, |
      do4 do8 do re[ do] re4 |
      sol,2 r |
      R1 R2. |
    }
  }
  \tag #'vdessus \clef "vdessus"
  \tag #'vhaute-contre \clef "vhaute-contre"
  \tag #'vtaille \clef "vtaille"
  \tag #'vbasse \clef "vbasse"
  \tag #'(vdessus vhaute-contre vtaille vbasse) {
    R1*3 R2.*3 R1*3 R1*35 R2.
  }
>>
%% Chœur
<<
  \tag #'(vdessus basse) {
    \tag #'basse \ffclef "vdessus"
    <>^\markup\character Chœur
    re''4 re'' do'' |
    re''2 re''4 |
    do'' fa'' mi'' |
    fa'' fa'' fa'' |
    re''\trill re'' re'' |
    mib'' mib''4. mib''8 |
    do''2\trill fa''4 |
    re''4.\trill re''8 mib''4 |
    mib'' re''4.\trill do''8 |
    do''2 do''4 |
    la'2\trill do''4 |
    re''4.\trill mib''8 fa''4 |
    sol'' do''4.\trill sib'8 |
    sib'2 r4
  }
  \tag #'vhaute-contre {
    fa'4 fa' fa' |
    fa'2 fa'4 |
    fa' do' do' |
    do' do' fa' |
    fa' fa' fa' |
    sol' sol'4. sol'8 |
    fa'2 fa'4 |
    sol'4. sol'8 sol'4 |
    sol' sol'4. sol'8 |
    mi'2 mi'4 |
    fa'2 fa'4 |
    fa'4. fa'8 fa'4~ |
    fa'8 mi' mi'4. re'8 |
    re'2 r4 |
  }
  \tag #'vtaille {
    sib4 sib la |
    sib2 sib4 |
    la la sol |
    la la la |
    sib sib sib |
    sib sib do' |
    la2 re'4 |
    si4. si8 do'4 |
    do' si4. do'8 |
    do'2 do'4 |
    do'2 la4 |
    sib4. sib8 sib4 |
    sib la4. sib8 |
    sib2 r4 |
  }
  \tag #'vbasse {
    sib4 sib fa |
    sib2 sib4 |
    fa fa do |
    fa fa fa |
    sib sib sib, |
    mib mib4. do8 |
    fa2 re4 |
    sol4. sol8 mib4 |
    do sol,4. do8 |
    do2 do'4 |
    fa2 fa4 |
    sib,4. do8 re4 |
    mib fa4. fa,8 |
    sib,2 r4 |
  }
  \tag #'(cephise lychas straton) { R2.*14 }
>>