\clef "taille" R1*3 R2.*3 R1*3 R1*35 R2.
fa'4 fa' fa' |
fa'2 fa'4 |
fa' fa' sol' |
fa' fa' fa' |
fa' fa' fa' |
sol' sol'4. sol'8 |
fa'2 fa'4 |
sol'4. sol'8 sol'4 |
sol' sol'4. sol'8 |
mi'2 mi'4 |
fa'2 fa'4 |
fa'4. fa'8 fa'4 |
mib' mib'4. re'8 |
re'2 r4 |
