\clef "quinte" R1*3 R2.*3 R1*3 R1*35 R2.
sib4 sib do' |
sib2 fa'4 |
do' do' do' |
do' do' do' |
re' re' sib |
sol sol sol |
la2 la4 |
sol4. sol8 sol4 |
sol4 sol4. sol8 |
sol2 do'4 |
do'2 do'4 |
sib4. sib8 sib4 |
sol fa4. fa8 |
fa2 r4 |
