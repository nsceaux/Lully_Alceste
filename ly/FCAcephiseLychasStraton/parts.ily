\piecePartSpecs
#(let ((text #{ s1*3 s2.*3 s1*3 s1*35 s2
s4_\markup\italic\right-align\right-column { Prenons part aux transports d’une joye éclatante } #}))
   `((dessus #:music ,text)
     (haute-contre #:music ,text)
     (taille #:music ,text)
     (quinte #:music ,text)
     (basse #:music ,text)
     (basse-continue #:score-template "score-basse-voix")
     (silence #:on-the-fly-markup , #{ \markup\tacet#65 #})))
