\score {
  \new StaffGroupNoBar \with { \haraKiriFirst } <<
    \new StaffGroupNoBracket <<
      \new Staff <<
        \global \includeNotes "dessus"
        { s1*3 s2.*3 s1*3 s1*35 s2.\break s2.*13 s2
          \footnoteHere #'(0 . 0) \markup\wordwrap {
            Manuscrit : reprise d’Admete
            \italic { “Il rameine Alceste…” }
            page \page-refIII#'FAAadmeteChoeur " "
            mesure 78 jusqu’à la fin du chœur qui suit.
          }
        }
      >>
      \new Staff << \global \includeNotes "haute-contre" >>
      \new Staff << \global \includeNotes "taille" >>
      \new Staff << \global \includeNotes "quinte" >>
    >>
    \new ChoirStaff <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'vdessus \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'vhaute-contre \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'vtaille \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'vbasse \includeLyrics "paroles"
    >>
    \new Staff \withLyrics <<
      \global \keepWithTag #'cephise \includeNotes "voix"
    >> \keepWithTag #'cephise \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'lychas \includeNotes "voix"
    >> \keepWithTag #'lychas \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'straton \includeNotes "voix"
    >> \keepWithTag #'straton \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s1 s2.*2\break s2. s1 s2 \bar "" \pageBreak
        s2 s1*2\break s1*3 s2 \bar "" \break s2 s1*3 s2 \bar "" \break
        s2 s1*3\break s1*4\pageBreak
        s1*4\break s1*4\break s1*3 s2 \bar "" \pageBreak
        s2 s1*3 s2 \bar "" \break s2 s1*2 s2 \bar "" \break
        s2 s2.\pageBreak
        s2.*5\pageBreak s2.*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}