\clef "dessus" R1*3 R2.*3 R1*3 R1*35 R2.
re''4 re'' do'' |
re''2 re''4 |
do'' fa'' mi'' |
fa'' fa'' fa'' |
re''\trill re'' re'' |
mib''4 mib''4. mib''8 |
do''2\trill fa''4 |
re''4.\trill re''8 mib''4 |
mib'' re''4.\trill do''8 |
do''2 do''4 |
la'2\trill do''4 |
re''4.\trill mib''8 fa''4 |
sol'' do''4.\trill sib'8 |
sib'2 r4 |
