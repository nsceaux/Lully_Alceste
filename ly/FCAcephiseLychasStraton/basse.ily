\clef "basse"
<<
  \tag #'basse { R1*3 R2.*3 R1*3 R1*35 R2. }
  \tag #'basse-continue {
    sol,1 |
    re2 do |
    sib, mib4 do |
    re2 re4 |
    sol4. fa8 mib4 |
    re2 sol4 |
    do2 la,8. sib,16 do4 |
    fa,2 sol, |
    la,4 fa, sol, la, |
    re2. re4 |
    sol la sib2 |
    la4 fa sib2 |
    sol4. re8 mib4 do |
    fa2 do4. sib,8 |
    la,4 sib, fa,2 |
    sib,4 sib fad2 |
    sib, sib |
    sol lab4 fa |
    sol1 | \allowPageTurn
    sol4 fa sol sol, |
    do do' si2 |
    do'4. sib8 la4. sol8 |
    fad4. mi8 fad4 re |
    sol4. fa8 mib2 |
    re4 re'8 do' si2 |
    do'4. sib8 la4. sol8 |
    fad4. mi8 re4 do |
    sib,4. la,8 sib,4 sol, |
    do2 re4 re, |
    sol, sol8 la sib2 |
    sol lab4 fa |
    sol2. sol4 |
    sol fa sol sol, |
    do do' si2 |
    do' do |
    re2. re4 |
    sol4. fa8 mib2 |
    re4 re'8 do' si2 |
    do' do |
    re2. re8. do16 |
    sib,4. la,8 sib,4 sol, |
    do2 re4 re, |
    sol,2. sol,8 la, |
    sib,2 la, |
    sib,2. |
  }
>>
sib4 sib fa |
sib2 sib4 |
fa fa do |
fa fa fa
sib sib sib, |
mib mib4. do8 |
fa2 re4 |
sol4. sol8 mib4 |
do sol,4. do8 |
do2 do'4 |
fa2 fa4 |
sib,4. do8 re4 |
mib fa4. fa,8 |
<<
  \tag #'basse { sib,2 r4 }
  \tag #'basse-continue {
    sib,2~ sib,16 sib, la, sol, |
    \once\set Staff.whichBar = "|"
    fa,1*11/16~ \hideNotes fa,16 |
  }
>>
