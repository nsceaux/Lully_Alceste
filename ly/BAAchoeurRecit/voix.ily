<<
  %% Chœur
  \tag #'(vdessus basse) {
    \clef "vdessus" r8 do'' do''8. do''16 re''8 re'' mi''8. fa''16 |
    re''8\trill re'' mi''8. mi''16 mi''8 fa'' re''[ mi''16] fa'' |
    <<
      \tag #'basse { mi''8 s4. s2 s1*3 s8 \ffclef "vdessus" }
      \tag #'vdessus { mi''2\trill r2 R1*3 r8 }
    >>
    la'8 la'8. la'16 si'8 si' do''8. re''16 |
    si'8\trill si' do''8. do''16 do''8 re'' si'[ dod''16] re'' |
    dod''2 r |
    <<
      \tag #'basse { s1*3 s8 \ffclef "vdessus" }
      \tag #'vdessus { R1*3 r8 }
    >>
    do''8 do''8. do''16 re''8 re'' mi''8. fa''16 |
    re''8\trill re'' mi''8. mi''16 mi''8 fa'' re''[ mi''16] fa'' |
    mi''2\trill r2
    \tag #'vdessus R1*9
  }
  \tag #'vhaute-contre {
    \clef "vhaute-contre" r8 sol' sol'8. sol'16 sol'8 sol' sol'8. sol'16 |
    sol'8 re' sol'8. sol'16 mi'8 la' sol'8. sol'16 |
    sol'2 r |
    R1*3 |
    r8 mi' mi'8. mi'16 mi'8 mi' mi'8. mi'16 |
    mi'8 mi' mi'8. mi'16 fa'8 fa' mi'8. mi'16 |
    mi'2 r |
    R1*3 |
    r8 sol' sol'8. sol'16 sol'8 sol' sol'8. sol'16 |
    sol'8 re' sol'8. sol'16 mi'8 la' sol'8. sol'16 |
    sol'2 r |
    R1*9
  }
  \tag #'vtaille {
    \clef "vtaille" r8 mi' mi'8. mi'16 re'8 re' do'8. do'16 |
    si8 si si8. si16 do'8 do' si8. do'16 |
    do'2 r |
    R1*3 |
    r8 do' do'8. do'16 si8 si la8. la16 |
    sold8 sold la8. la16 la8 la sold8. la16 |
    la2 r |
    R1*3 |
    r8 mi' mi'8. mi'16 re'8 re' do'8. do'16 |
    si8 si si8. si16 do'8 do' si8. do'16 |
    do'2 r |
    R1*9
  }
  \tag #'vbasse {
    \clef "vbasse" r8 do' do'8. do'16 si8 si do'8. do16 |
    sol8 sol mi8. mi16 la8 fa sol8. sol16 |
    do2 r |
    R1*3 |
    r8 la la8. la16 sold8 sold la8. la16 |
    mi8 mi do8. do16 fa8 re mi8. mi16 |
    la,2 r |
    R1*3 |
    r8 do' do'8. do'16 si8 si do'8. do16 |
    sol8 sol mi8. mi16 la8 fa sol8. sol16 |
    do2 r |
    R1*9
  }
  %% Lycas
  \tag #'(lycas basse recit) {
    <<
      \tag #'basse { s1*2 s8 \ffclef "vhaute-contre" }
      \tag #'(lycas recit) { \clef "vhaute-contre" R1*2 r8 }
    >> <>^\markup\character Lycas
    do'16 re' mi'8\trill mi'16 fa' sol'8. sol'16 re' re' mi' fa' |
    mi'8\trill mi' r16 mi' mi' sol' do'8. do'16 do'8. si16 |
    si4\trill si re'8 re'16 re' re'8 mi' |
    fa'4. la'8 fa'\trill r fa' fa'16 mi' |
    mi'8\trill <<
      \tag #'basse {
        s8 s2. s1*2 \ffclef "vhaute-contre" <>^\markup\character Lycas
      }
      \tag #'(lycas recit) { r8 r4 r2 R1*2 }
    >> r8 mi' mi'16 mi' dod' mi' la8. la'16 mi' mi' fad' sol' |
    fad'8 fad' re' re'16 re' re'8. re'16 do'8. si16 |
    si8\trill si16 sol sol sol la si do' do' re' mi' fa'8. sol'16 |
    mi'8\trill <<
      \tag #'basse {
        s8 s2. s1*2 \ffclef "vhaute-contre" <>^\markup\character Lycas
      }
      \tag #'(lycas recit) { r8 r4 r2 R1*2 }
    >> r4 r8 mi' do'4\trill r16 do' re' mi' |
    si4\trill r8 si16 si si4 si8. do'16 |
    la4\trill la <<
      \tag #'(basse recit) {
        s2 s1 s2 \ffclef "vhaute-contre" <>^\markup\character Lycas
      }
      \tag #'lycas { r2 R1 r2 }
    >> re'4 si16 si do' re' |
    mi'4. mi'8 do'4\trill do'8 re'16 mi' |
    la4 <<
      \tag #'(basse recit) {
        s2. s1*15 s4 \ffclef "vhaute-contre" <>^\markup\character Lycas
      }
      \tag #'lycas { r4 r2 R1*15 r4 }
    >> r8 sol' mi'8.\trill mi'16 mi'8. sol'16 |
    do'8 do'16 do' sol8 sol16 do' la8\trill la r la'16 la' |
    fa'8 fa'16 fa' do'8. do'16 la8.\trill la16 sib8. do'16 |
    re'8 re' fa'16 fa' fa' fa' fad'4 fad'8. sol'16 |
    sol'8 <<
      \tag #'(basse recit) {
        s2.. s1*4 s2.*3 s1 s8 \ffclef "vhaute-contre" <>^\markup\character Lycas
      }
      \tag #'lycas { r8 r4 r2 R1*4 R2.*3 R1 r8 }
    >> mi'16 mi' mi'8 mi'16 mi' si8.\trill si16 re' re' re' mi' |
    dod' dod' la la la8 si16 dod' re'8 re'16 re' re'8 re'16 dod' |
    re'8 re' r re'16 re' re'8 mi'16 fad' |
    sol'8. sol'16 re' re' re' sol' mi'4\trill r8 do'16 mi' |
    la4 la16 la la si sold4 <<
      \tag #'(basse recit) {
        s4 s1 s8 \ffclef "vhaute-contre" <>^\markup\character Lycas
      }
      \tag #'lycas { r4 R1 r8 }
    >> la16 la la8 la16 la re'8. re'16 la16 la la re' |
    si8\trill si r4
    \tag #'lycas { r2 R1*6 R2. R1 }
  }
  %% Alcide
  \tag #'(alcide basse recit) {
    <<
      \tag #'(basse recit) { s1*17 s2 \ffclef "vbasse-taille" }
      \tag #'alcide { \clef "vbasse" R1*17 r2 }
    >> <>^\markup\character Alcide
    do'4. la8 |
    fad4 re16 re mi fad sol8. sol16 sol8. fad16 |
    sol4 sol <<
      \tag #'(basse recit) {
        s2 s1 s4 \ffclef "vbasse-taille" <>^\markup\character Alcide
      }
      \tag #'alcide { r2 R1 r4 }
    >> r4 r8 fa16 fa fa8 sol16 la |
    re4. sol8 fa8\trill mi re8.\trill do16 |
    do2 r |
    r8 sol16 sol do'8 do'16 do' sol8. sol16 sol sol mi\trill sol |
    do8 do r mi la8. la16 la8. la16 |
    fad8. fad16 fad fad sold la sold8 sold r si |
    do'8. do'16 do'8. do'16 la8\trill la16 la mi8 mi16 fad |
    sol4 r8 si16 si mi8 r16 sol sol8 sol16 fad |
    fad8\trill fad r re' fad4 fad8 fad16 fad |
    sol8. sol16 sol fa fa mi mi4\trill do' |
    r8 sol mi\trill mi16 sol do8 do r do |
    fa8 fa16 fa sib8 sib16 la la4\trill r8 do' |
    la8.\trill la16 la la la do' fa8 fa r16 do' do' do' |
    dod'8.\trill dod'16 dod'8. re'16 re'8. la16 fad fad sol la |
    si8 si r sol do'4 do'8 re'16 mi' |
    la8\trill la r la16 la si8 do'16 do' do'8. si16 |
    do'4 <<
      \tag #'(basse recit) {
        s2. s1*3 s8 \ffclef "vbasse-taille" <>^\markup\character Alcide
      }
      \tag #'alcide { r4 r2 R1*3 r8 }
    >> si16 re' si8\trill si16 si sol8. sol16 re16 re mi fa |
    mi2\trill r8 do' do' sol |
    la8. la16 sol8. fa16 mi4\trill mi |
    r8 do'16 do' do'8 re'16 la sib8 sib16 la sol8\trill sol16 la |
    fad8 fad r re16 re sol8 sol16 sol sol8. fad16 |
    sol4 r8 sol sol16 la si sol |
    do'8 do' r sol16 sol mi8\trill fa16 sol |
    la8 la do'8. si16 la8 sol16 fa |
    mi4\trill mi8 la re4\trill re8 mi |
    do8 <<
      \tag #'(basse recit) {
        s2.. s1 s2. s1 s2. \ffclef "vbasse-taille" <>^\markup\character Alcide
      }
      \tag #'alcide { r8 r4 r2 R1 R2. R1 r2 r4 }
    >> mi'8 r16 si |
    sold8 sold16 si mi4 mi' si16 si si mi' |
    dod' dod' <<
      \tag #'(basse recit) {
        s2.. s2 \ffclef "vbasse-taille" <>^\markup\character Alcide
      }
      \tag #'alcide { r8 r4 r2 r2 }
    >> r8 sol sol8. sol16 |
    mi8\trill mi r mi' do' do'16 do' sol8 la16 sib |
    la2\trill r8 fa16 fa fa8 sol16 la |
    re4. sol8 fa\trill mi re8.\trill do16 |
    do4 r8 sol do'8. do'16 do'8. do'16 |
    sold8 sold16 sold sold8 sold16 si mi4 mi |
    r8 do'16 mi' do'8\trill do'16 do' la8\trill la si16 si si do' |
    sold4\trill mi16 mi mi mi la8 la16 la |
    fad4 r8 re'16 re' la\trill r la la la8. si16 |
    

  }
>>