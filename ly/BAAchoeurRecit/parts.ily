\piecePartSpecs
#`((basse-continue #:score-template "score-basse-voix")
   (basse #:system-count 4)
   (silence #:on-the-fly-markup , #{ \markup\tacet#66 #}))
