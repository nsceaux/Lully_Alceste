\tag #'(choeur basse) {
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
}
\tag #'(lycas basse recit) {
  Vostre a -- my le plus cher é -- pou -- se la prin -- ces -- se
  la plus char -- man -- te de la Gre -- ce,
  lors que cha -- cun les suit, sei -- gneur, les fuy -- ez- vous ?
}
\tag #'(choeur basse) {
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
}
\tag #'(lycas basse recit) {
  Vous pa -- rois -- sez trou -- blé des cris qui re -- ten -- tis -- sent ?
  Quand deux a -- mants heu -- reux s’u -- nis -- sent,
  le cœur du grand Al -- cide en se -- roit- il ja -- loux ?
}
\tag #'(choeur basse) {
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
  Vi -- vez, vi -- vez, heu -- reux es -- poux.
}
\tag #'(lycas basse recit) {
  Sei -- gneur, vous soû -- pi -- rez, & gar -- dez le si -- len -- ce ?
}
\tag #'(alcide basse recit) {
  Ah Ly -- chas, lais -- se- moy par -- tir en di -- li -- gen -- ce.
}
\tag #'(lycas basse recit) {
  Quoy dés ce mes -- me jour pres -- ser vos -- tre dé -- part ?
}
\tag #'(alcide basse recit) {
  J’au -- ray beau me pres -- ser je par -- ti -- ray trop tard.
  Ce n’est point a -- vec toy que je pre -- tens me tai -- re ;
  al -- ceste est trop ai -- mable, elle a trop sçeu me plai -- re ;
  un autre en est ai -- mé, rien ne flat -- te mes vœux,
  c’en est fait, Ad -- me -- te l’es -- pou -- ze,
  et c’est dans ce mo -- ment qu’on les u -- nit tous deux.
  Ah qu’une a -- me ja -- lou -- se
  es -- prouve un tour -- ment ri -- gou -- reux !
  J’ay peine à l’ex -- pri -- mer moy- mes -- me :
  fi -- gu -- re- toy, si tu le peux,
  quelle est l’hor -- reur ex -- tres -- me
  de voir ce que l’on ai -- me
  au pou -- voir d’un ri -- val heu -- reux.
}
\tag #'(lycas basse recit) {
  L’a -- mour est- il plus fort qu’un he -- ros in -- domp -- ta -- ble ?
  L’u -- ni -- vers n’a point eû de mons -- tre re -- dou -- ta -- ble
  que vous n’ay -- ez pû sur -- mon -- ter.
}
\tag #'(alcide basse recit) {
  Eh crois- tu que l’a -- mour soit moins à re -- dou -- ter ?
  Le plus grand cœur a sa foi -- bles -- se.
  Je ne puis me sau -- ver de l’ar -- deur qui me pres -- se
  qu’en quit -- tant ce fa -- tal sé -- jour :
  con -- tre d’ai -- ma -- bles char -- mes,
  la va -- leur est sans ar -- mes,
  et ce n’est qu’en fuy -- ant qu’on peut vain -- cre l’a -- mour.
}
\tag #'(lycas basse recit) {
  Vous de -- vez vous for -- cer, au moins, à voir la fes -- te
  qui dé -- ja dans ce port vous pa -- roist tou -- te pres -- te.
  Vos -- tre fuite à pre -- sent fe -- roit un trop grand bruit ;
  dif -- fe -- rez jus -- ques à la nuit.
}
\tag #'(alcide basse recit) {
  Ah Ly -- cas ! qu’el -- le nuit ! ah qu’el -- le nuit fu -- nes -- te !
}
\tag #'(lycas basse recit) {
  Tout le res -- te du jour voy -- ez en -- core Al -- ces -- te.
}
\tag #'(alcide basse recit) {
  La voir en -- co -- re ?… hé bien dif -- fe -- rons mon dé -- part,
  je te l’a -- vois bien dit, je par -- ti -- ray trop tard.
  je vais la voir ai -- mer un es -- poux qui l’a -- do -- re,
  je ver -- ray dans leurs yeux un tendre em -- pres -- se -- ment :
  que je vais pay -- er che -- re -- ment
  le plai -- sir de la voir en - %%- co -- re !
}
