\score {
  \new ChoirStaff <<
    %% Chœur
    <<
      \new Staff \with { \haraKiri } \withLyrics <<
        { \noHaraKiri s1*14\revertNoHaraKiri }
        \global \keepWithTag #'vdessus \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyrics <<
        { \noHaraKiri s1*14\revertNoHaraKiri }
        \global \keepWithTag #'vhaute-contre \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyrics <<
        { \noHaraKiri s1*14\revertNoHaraKiri }
        \global \keepWithTag #'vtaille \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
      \new Staff \with { \haraKiri } \withLyrics <<
        { \noHaraKiri s1*14\revertNoHaraKiri }
        \global \keepWithTag #'vbasse \includeNotes "voix"
      >> \keepWithTag #'choeur \includeLyrics "paroles"
    >>
    %% Lycas & Alcide
    \new Staff \withRecit <<
      \global \keepWithTag #'recit \includeNotes "voix"
    >> \keepWithTag #'recit \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*2\break s1 s2 \bar "" \pageBreak s2 s1*2\break s1*3\pageBreak
        s1*2\break s1\break s1*3\break s1*3\break s1*2\break s1*2 s2\bar "" \pageBreak
        s2 s1*2\break s1*2\break s1*2\break s1*2\break s1*2\break s1 s2 \bar "" \pageBreak
        s2 s1\break s1*2\break s1*2\break s1 s2\bar "" \break s2 s1*2\break s1 s2 \bar "" \pageBreak
        s2 s2.*2\break s2. s1\break s1\break s1\break s2. s1\break s1*2\pageBreak
        s1*2\break s1*2\break s1*2\break s1*2\break s2. s1\pageBreak
        
      }
    >>
  >>
  \layout { }
  \midi { }
}