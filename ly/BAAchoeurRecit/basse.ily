\clef "basse" do4 do' si do'8 do |
sol4 mi la8 fa sol sol, |
do2 <<
  \tag #'basse { r2 R1*3 }
  \tag #'basse-continue {
    do'4 do'8 si |
    do'4. si8 la2 |
    sol2. fa8 mi |
    re1 |
  }
>>
la2 sold4 la |
mi do fa8 re mi mi, |
la,1 |
<<
  \tag #'basse { R1*3 }
  \tag #'basse-continue {
    la2. dod4 |
    re1 |
    sol,4 sol8. fa16 mi4 re |
  }
>>
do4 do' si do'8 do |
sol4 mi la8 fa sol sol, |
<<
  \tag #'basse {
    do2 r |
    R1*8 R1 R1*22 R2.*3 R1 R1*2 R2. R1*11 R2. R1*1
  }
  \tag #'basse-continue {
    do2~ do8 re do si, |
    %%
    la,1 |
    sold, |
    la, | \allowPageTurn
    re4. do8 si, do re re, |
    sol,2 sol |
    mi1 |
    fa2 fa, |
    sol,4. mi,8 fa,4 sol, |
    do,1 |
    do~ |
    do2 dod |
    re mi |
    la,1 |
    mi2. dod4 |
    re1 |
    si,2 do~ |
    do1 |
    la,4 sol, fa,2~ |
    fa, fa |
    mi re4~ re16 do si, la, |
    sol,4 sol mi2 |
    fa4 re sol8 do sol,4 |
    do1 |
    mi,2 fa,~ |
    fa, fa |
    sib,4 sib la2 |
    sol sol, |
    do mi, |
    fa,4 sol, la,2 |
    la8. sol16 fad4 sol8 sol, mib4 |
    re do sib,8 sol, re re, |
    sol,4 sol4. fa8 |
    mi2 do4 |
    fa fa,4. sol,8 |
    la,4. fa,8 sol,2 |
    do4 do' sold2 |
    la4. sol8 fa4 mi |
    re2. | \allowPageTurn
    si,2 do |
    fa mi~ |
    mi1 |
    la,2 fad, |
    sol,1 |
    do2 mi, |
    fa,1 |
    sol,4. mi,8 fa,4 sol, |
    do1 |
    si,2 la,~ |
    la, fa4 re |
    mi2 dod4 |
    re si, do re |
  }
>>
