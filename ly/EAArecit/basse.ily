\clef "basse" do8 re mi do sol fa sol mi |
la sol la fa sol la si sol |
do' sib la sol fa mi re do |
sol la sol fa mi re mi do |
fa mi fa re sol fa sol sol, |
<<
  \tag #'basse { do2 r | R1*4 | }
  \tag #'basse-continue {
    do8 si, do fa, sol, la, si, sol, |
    do si, la, sol, fad, mi, fad, re, |
    sol, la, si, sol, do re mi fa |
    sol fa mi re do la, sol, fa, |
    mi, do, fa, re, sol, la, si, sol, |
  }
>>
do8 re mi do sol fa sol mi |
<<
  \tag #'basse { R1*13 | }
  \tag #'basse-continue {
    do8 si, do re mi re mi do |
    fa mi fa re mi re dod la, |
    re re, fa, re, sol, la, si, do |
    re mi fad re sol la sol fa |
    mi re do si, la, sol, re do |
    si, do si, la, sol, sol, fa, mi, |
    re, re re mi fa mi fa re |
    sol fa mi do sol fa sol sol, |
    do si, do fa, sol, la, si, sol, |
    do si, la, sol, fad, mi, fad, re, |
    sol, la, si, sol, do re mi fa |
    sol fa mi re do la, sol, fa, |
    mi, do, fa, re, sol, la, si, sol, |
  }
>>
do re mi do sol fa sol mi |
la sol la fa sol la si sol |
do' sib la sol fa mi re do |
sol la sol fa mi re mi do |
fa mi fa re sol fa sol sol, |
<<
  \tag #'basse { do2 r | R1*11 | }
  \tag #'basse-continue {
    do1~ | \allowPageTurn
    do2 fa, |
    sol,1~ |
    sol,2 do |
    dod re~ |
    re mib8 do re re, |
    sol,2 r |
    r2 r4 sol, |
    sol,1 |
    do4~ do16 sib, la, sol, fa,4 fad, |
    sol,2 r |
    r sol8 fa mi re |
  }
>>
do4. re8 mi4 do |
fa4. sol8 la4 fa |
sol4. fa8 sol4 sol, |
do4. si,8 do4 la, |
re4. do8 re4 re, |
sol,4. la,8 si,4 sol, |
do4. re8 mi4 do |
fa4. mi8 fa4 re |
mi4. re8 mi4 mi, |
la,4. si,8 dod4 la, |
re4. mi8 fad4 re |
sol4. la8 sol4 fa |
mi4. re8 mi4 do |
fa4. mi8 fa4 re |
sol4. fa8 sol4 sol, |
<<
  \tag #'basse {
    do1 |
    R1*2 R2. R1*5 R2. R1*2 |
  }
  \tag #'basse-continue {
    do1~ | \allowPageTurn
    do |
    fa4 re mi2 |
    sold,4 la, mi, |
    la,2 re |
    si, fad, |
    sol, re, |
    sol,1~ |
    sol, |
    mi,4 fa, sol, |
    do do' do'2 |
    do'4. do'8 do'4 si |
  }
>>
do'4 do'8. do'16 si2 |
do'4. do8 sol4. sol8 |
re4. re8 la4. sol8 |
fa2 mi4. mi8 |
la4. la8 la[ la,] |
re4. re8 sol8. fa16 |
mi8 mi16 re do8 do16 si, la,4 |
sol,4. sol8 mi4. mi8 |
la4 la8 fa sol4 sol8 sol, |
<<
  \tag #'basse { do4 r r2 | R1 }
  \tag #'basse-continue {
    do4. do'8 do'2 |
    do'4. do'8 do'4 si |
  }
>>
do'4 mi8 fa sol4. sol8 |
do'4. do'8 do'4. do'8 |
do'4. do'8 do'4 si |
do'4. do8 sol4. mi8 |
la4. fa8 sol4 sol8 sol, |
<<
  \tag #'basse { do4 r r2 }
  \tag #'basse-continue {
    do2~ do8 sib, la, sol, |
    \once\set Staff.whichBar = "|"
  }
>>
