\clef "dessus" do''4. do''8 si'4. mi''8 |
do''4. re''8 si' do'' re'' si' |
mi'' do'' do'' sib' la' la' si' do'' |
si'4. si'8 mi''4. mi''8 |
la'4. re''8 si' do'' do'' si' |
do''2 r2 |
R1*4 |
do''4. do''8 si'4. mi''8 |
R1*13 |
do''4. do''8 si'4. mi''8 |
do''4. re''8 si' do'' re'' si' |
mi'' do'' do'' sib' la' la' si' do'' |
si'4. si'8 mi''4. mi''8 |
la'4. re''8 si' do'' do'' si' |
do''2 r |
R1*11 |
do''2. do''4 |
la'4. si'8 do''4 do'' |
do''2 si'4.(\trill la'16 si') |
do''2. do''4 |
la'4. si'8 do''4 la' |
si'4. do''8 re''4 si' |
do''2. do''4 |
la'2. re''4 |
si'4. dod''8 re''4 mi'' |
dod''4. re''8 mi''4 dod'' |
re''2. re''4 |
si'2. si'4 |
mi''2. mi''4 |
la'2. re''4 |
si'4. do''8 do''4 si' |
do''1 |
R1*2 R2. R1*5 R2. R1*2 |
r4 sol''8. sol''16 sol''2 |
sol''8 sol'' sol''8. fa''32 mi'' re''4 si'8. dod''16 |
re''4 la'8. si'16 do''8 do'' do'' si' |
la'8. sold'16 la'8. si'16 sold'4 sold'8. sold'16 |
la'8. si'16 dod''8[ re''] mi''[ dod''] |
re''4 la'8 re''16 do'' si'8. si'16 |
mi''8 mi''16 fa'' sol''8 sol''16 sol'' sol''8 fad'' |
sol''4. si'8 mi'' mi'' mi'' mi'' |
do''4 do''8 do'' do''8. re''16 si'8.\trill do''16 |
do''4 r r2 |
R1 |
sol''8 fa'' mi''8. re''32 do'' si'4 si'8 si' |
do''4 do''8 mi'' fa''4. fa''8 |
mi''4. mi''8 re''4. re''8 |
sol'4. do''8 si' si' si' mi'' |
do''4 do''8 re'' si'8. do''16 si'8.\trill do''16 |
do''4 r r2 |
