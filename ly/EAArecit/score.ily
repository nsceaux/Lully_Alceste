\score {
  \new ChoirStaff <<
    \new GrandStaff \with { \haraKiriFirst } <<
      \new Staff << \global \includeNotes "dessus1" >>
      \new Staff << \global \includeNotes "dessus2" >>
    >>
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'ombre1 \includeNotes "voix"
    >> \keepWithTag #'ombres \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'ombre2 \includeNotes "voix"
    >> \keepWithTag #'ombres \includeLyrics "paroles"
    \new Staff \with { \haraKiriFirst } \withLyrics <<
      \global \keepWithTag #'ombre3 \includeNotes "voix"
    >> \keepWithTag #'(ombres ombre) \includeLyrics "paroles"
    \new Staff \withLyrics <<
      \global \keepWithTag #'charon \includeNotes "voix"
    >> \keepWithTag #'charon \includeLyrics "paroles"
    \new Staff <<
      \global \keepWithTag #'basse-continue \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*3\break s1*3\break s1*2 s2 \bar "" \pageBreak
        s2 s1*2 s2 \bar "" \break s2 s1*2\break s1*2 s2 \bar "" \break
        s2 s1*2\break s1*2 s2 \bar "" \break s2 s1\pageBreak
        s1*4\break s1*3\break s1*2 s2 \bar "" \break
        s2 s1 s2 \bar "" \pageBreak
        s2 s1*2\break s1*2\break s1*4\pageBreak
        s1*5\break s1*5\break s1*4\pageBreak
        s1 s2.\break s1*2\break s1*2\break s1 s2. s1\break s1*3\pageBreak
        s1*2 s2.\break s2.*2 s2 \bar "" \break s2 s1*2\pageBreak
        s1*3\break
      }
      \modVersion {
        s1*24%\break
        s1*17\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
