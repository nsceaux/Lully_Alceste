\piecePartSpecs
#`((dessus #:score "score-dessus")
   (basse)
   (basse-continue
    #:music , #{ s1*25\break #}
    #:score-template "score-basse-voix")
   (silence #:on-the-fly-markup , #{ \markup\tacet#85 #}))
