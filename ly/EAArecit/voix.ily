%% Charon
<<
  \tag #'(charon basse) {
    \clef "vbasse" R1*5 |
    r8 <>^\markup\character Charon sol sol la sol4 sol8 fa |
    mi do' do' si la4 la8 re' |
    si4\trill sol r8 sol sol la |
    sol4 sol8 fa mi fa sol la |
    sol4 fa8.\trill mi16 mi4( re)\trill |
    do2 r2 |
    do2 r4 do'8. si16 |
    la8 la la8. si16 sold8. sold16 la8. mi16 |
    fa4 fa8. mi16 re4 re |
    r8 re' re' do' si4 si8 sol |
    do'4 mi8 mi fad sol sol fad |
    sol4 sol r8 re re mi |
    fa4 fa8 sol la4 la8 la |
    si si do' do' do'4( si) |
    do'8 sol sol la sol4 sol8 fa |
    mi do' do' si la4 la8 re' |
    si4\trill sol r8 sol sol la |
    sol4 sol8 fa mi fa sol la |
    sol4 fa8.\trill mi16 mi4( re4)\trill |
    do2 <>^\markup\italic { Charon sort de sa Barque } r |
    R1*4 |
    r2 do'8 do'16 do' do'8 sol |
    do'4. do'8 la4 fa8 fa16 la |
    re4 r8 re' si4 r8 sol16 sol |
    re4 re8. sol16 do4 do8 r16 mi |
    la8 la16 la la8 la16 la fad2 |
    r8 re' re'16 re' re' fad sol sol sol do re8[ do16] re |
    sol,4 sol,
  }
  \tag #'(ombre1 ombre2 ombre3) { \clef "vdessus" R1*35 r2 }
>>
%% Les Ombres
<<
  \tag #'(ombre1 basse) {
    \tag #'basse \ffclef "vdessus" <>^\markup\character Les Ombres
    r8 re''16 re'' re''8 re'' |
    mi''4 mi''8. mi''16 re''4 
  }
  \tag #'ombre2 {
    r8 si'16 si' si'8 si' |
    do''4 do''8. do''16 si'4
  }
  \tag #'ombre3 {
    r8 sol'16 sol' sol'8 sol' |
    sol'4 sol'8. sol'16 sol'4
  }
  \tag #'charon { r2 | r2 r4 }
>>
%% Charon
<<
  \tag #'(charon basse) {
    \tag #'basse \ffclef "vbasse" <>^\markup\character Charon
    r8 sol |
    sol8. sol16 sol8. sol16 re8. re16 re re mi fa |
    mi8\trill mi16 mi mi mi fa sol la fa fa la re8. re16 |
    re4 r
  }
  \tag #'(ombre1 ombre2 ombre3) { r4 R1*2 r2 }
>>
%% Les Ombres
<<
  \tag #'(ombre1 basse) {
    \tag #'basse \ffclef "vdessus" <>^\markup\character Les Ombres
    r8 re''16 re'' re''8 re'' |
    mi''4 mi''8. mi''16 re''4 r |
  }
  \tag #'ombre2 {
    r8 si'16 si' si'8 si' |
    do''4 do''8. do''16 si'4 r4 |
  }
  \tag #'ombre3 {
    r8 sol'16 sol' sol'8 sol' |
    sol'4 sol'8. sol'16 sol'4 r4 |
  }
  \tag #'charon { r2 | R1 }
>>
%% Charon
<<
  \tag #'(charon basse) {
    \tag #'basse \ffclef "vbasse" <>^\markup\character Charon
    do'4 do' r2 |
    fa4 fa r2 |
    sol4 sol r2 |
    do4 do r2 |
    re4 re r2 |
    sol,4 sol, r2 |
    do4 do r2 |
    fa4 fa r2 |
    mi4 mi r2 |
    la,4 la, r2 |
    re4 re r2 |
    sol4 sol r2 |
    mi4 mi r2 |
    fa4 fa r2 |
    sol4 sol r2 |
    do4 do r2 |
    r4 r16 do' do' do' sol4 r8 sol16 do' |
    la8\trill r16 la si si si si sold8 sold <<
      \tag #'basse { s4 s2. s4. \ffclef "vbasse" }
      \tag #'charon { r4 R2. r4 r8 }
    >> <>^\markup\character Charon
    la8 fad8. fad16 fad fad fad la |
    re4 <<
      \tag #'basse { s2. s1 s4 \ffclef "vbasse" }
      \tag #'charon { r4 r2 R1 r4 }
    >> <>^\markup\character Charon
    r8 sol16 sol re8. re16 si,8\trill si,16 re |
    sol,2 r4 sol8 sol |
    do' sol16 la fa8.\trill mi16 re4\trill |
    do <<
      \tag #'basse { s2. s1 s4 \ffclef "vbasse" }
      \tag #'charon { r4 r2 R1 r4 }
    >> <>^\markup\character Charon
    do'8. do'16 si2\trill |
    do'8 do' do' do sol4 sol8. sol16 |
    re4 re8. re16 la8 la la sol |
    fa8. fa16 fa4\trill mi mi8 mi |
    la4 la8 la la la, |
    re4 re8 re16 re sol8. fa16 |
    mi8 mi16 re do8 do16 si, la,4\trill |
    sol,4. sol8 mi\trill mi mi mi |
    la4 la8 fa sol4 sol8 sol, |
    do4 <<
      \tag #'basse { s2. s1 \ffclef "vbasse" }
      \tag #'charon { r4 r2 R1 }
    >> <>^\markup\character Charon
    do8 re mi fa sol4 sol8 sol |
    do'4 do'8 do' do'4. do'8 |
    do'4. do'8 do'4 si |
    do'4. do8 sol sol sol mi |
    la4 la8 fa sol4 sol8 sol, |
    do4 r r2 |
  }
  \tag #'(ombre1 ombre2) { R1*18 R2. R1*5 R2. R1*6 R2.*3 R1*10 }
  \tag #'(ombre3 basse) {
    <<
      \tag #'basse { s1*17 s2. \tag #'basse \ffclef "vdessus" }
      \tag #'ombre3 { R1*17 r2 r4 }
    >> <>^\markup\character Une Ombre
    r8 mi'' |
    mi''8. mi''16 mi''8. mi''16 mi''8. mi''16 |
    mi''4 mi''8 <<
      \tag #'basse { s8 s2 s4 \tag #'basse \ffclef "vdessus" }
      \tag #'ombre3 { r8 r2 r4 }
    >> <>^\markup\character L'Ombre
    r8 re'' re''8. re''16 re''8. re''16 |
    re''4. re''8 re''8. re''16 re''8. re''16 |
    re''4 <<
      \tag #'basse { s4 s2 s1 s2. s4 \tag #'basse \ffclef "vdessus" }
      \tag #'ombre3 { r4 r2 R1 R2. r4 }
    >> <>^\markup\character L'Ombre
    r8 mi'' fa''4. fa''8 |
    mi''4. mi''8 fa''4 fa''8.[ mi''16] |
    mi''4\trill <<
      \tag #'basse {
        s2. s1*3 s2.*3 s1*2 s4 \tag #'basse \ffclef "vdessus" 
      }
      \tag #'ombre3 { r4 r2 R1*3 R2.*3 R1*2 r4 }
    >> <>^\markup\character L'Ombre
    r8 mi'' fa''4. fa''8 |
    mi''4. mi''8 fa''4. fa''16[ mi''] |
    \tag #'ombre3 { mi''4 r r2 | R1*5 }
  }
>>
