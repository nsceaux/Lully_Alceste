\key do \major \tempo "Ritournelle"
\time 4/4 \midiTempo#120 s1 \bar "|!:" s1*9 \alternatives s1 s1 s1*12
\tempo "Ritournelle" s1*5
\midiTempo#80 s1*12
\time 2/2 \midiTempo#160 s1*16
\time 4/4 \midiTempo#80 s1*2
\digitTime\time 3/4 s2.
\time 4/4 s1*4
\time 2/2 s1
\digitTime\time 3/4 s2.
\time 4/4 s1*6
\digitTime\time 3/4 s2.*3
\time 4/4 s1*10 \bar "|."
