\clef "dessus" mi''4. mi''8 re''4.\trill sol''8 |
mi''4.\trill fa''8 re''4\trill sol''8. sol''16 |
sol''4. sol''8 la'' sol'' fa'' mi'' |
re''4.\trill re''8 sol''4. sol''8 |
do''4. fa''8 re'' mi'' fa'' re'' |
mi''2 r2 |
R1*4 |
mi''4. mi''8 re''4.\trill sol''8 |
R1*13 |
<>^"Violons" mi''4. mi''8 re''4.\trill sol''8 |
mi''4.\trill fa''8 re''4\trill sol''8. sol''16 |
sol''4. sol''8 la'' sol'' fa'' mi'' |
re''4.\trill re''8 sol''4. sol''8 |
do''4. fa''8 re'' mi'' fa'' re'' |
mi''2 r |
R1*11 |
<>^"Violons" mi''4. fa''8 sol''4 mi'' |
fa''2. fa''4 |
re''4. mi''8 fa''4 re'' |
mi''2. la''4 |
fad''4. sol''8 sol''4 fad'' |
sol''4. sol''8 fa''4 sol'' |
mi''4. fa''8 sol''4 mi'' |
fa''4. sol''8 la''4 si'' |
sold''4. la''8 la''4\trill sold'' |
la''2. la''4 |
fad''4. sol''8 la''4 la'' |
re''2. re''4 |
sol''2. sol''4 |
do''2. fa''4 |
re''4. mi''8 fa''4 re'' |
mi''1 |
R1*2 \allowPageTurn R2. R1*5 R2. R1*2 |
r4 <>^"Violons" mi''8. mi''16 re''2 |
mi''8 mi'' mi''8. re''32 do'' si'4 re''8. mi''16 |
fa''4 fa''8. sol''16 mi''8 mi'' mi'' mi'' |
mi''8. fa''16 re''4\trill mi'' si'8. si'16 |
dod''8. re''16 mi''8[ fad''] sol''[ mi''] |
fad''4 fad''8 fad''16 fad'' sol''8 re'' |
sol'' sol''16 fa'' mi''8 mi''16 re'' do''8. re''16 |
si'4. re''8 sol'' sol'' sol'' sol'' |
mi''4 mi''8 fa'' re''4 re''8 sol'' |
mi''4 r r2 |
R1 |
mi''8 fa'' sol''8. fa''32 mi'' re''4 re''8 sol'' |
mi''4 mi''8 sol'' la''4. la''8 |
sol''4. sol''8 fa''4. fa''8 |
mi''4. mi''8 re'' re'' re'' sol'' |
mi''4 mi''8 fa'' re''8. mi''16 re''8.\trill do''16 |
do''4 r r2 |
