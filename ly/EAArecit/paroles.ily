\tag #'(charon basse) {
  Il faut pas -- ser tost ou tard,
  il faut pas -- ser dans ma bar -- que.
  Il faut pas -- ser tost ou tard,
  il faut pas -- ser dans ma bar -- que.  - que.
  On y vient jeune, ou vieil -- lard,
  ain -- si qu’il plaist à la Par -- que ;
  on y re -- çoit sans é -- gard,
  le ber -- ger, & le mo -- nar -- que.
  On y re -- çoit sans é -- gard,
  le ber -- ger, & le mo -- nar -- que.
  Il faut pas -- ser tost ou tard,
  il faut pas -- ser dans ma bar -- que.
  Il faut pas -- ser tost ou tard,
  il faut pas -- ser dans ma bar -- que.
  Vous qui vou -- lez pas -- ser, ve -- nez, ma -- nes er -- rants,
  ve -- nez, a -- van -- cez, tris -- tes om -- bres,
  pay -- ez le tri -- but que je prens,
  où re -- tour -- nez er -- rer sur ces ri -- va -- ges som -- bres.
}
\tag #'(ombres basse) {
  Pas -- se- moy, Cha -- ron, pas -- se- moy.
}
\tag #'(charon basse) {
  Il faut au -- pa -- ra -- vant que l’on me sa -- tis -- fas -- se,
  on doit pay -- er les soins d’un si pe -- nible em -- ploy.
}
\tag #'(ombres basse) {
  Pas -- se- moy, Cha -- ron, pas -- se- moy.
}
\tag #'(charon basse) {
  Don -- ne, pas -- se, don -- ne, pas -- se,
  don -- ne, pas -- se, don -- ne, pas -- se,
  don -- ne, pas -- se, don -- ne, pas -- se,
  don -- ne, pas -- se, don -- ne, pas -- se,
  de -- meu -- re toy.
  Tu n’as rien, il faut que l’on te chas -- se.
}
\tag #'(ombre basse) {
  Une om -- bre tient si peu de pla -- ce.
}
\tag #'(charon basse) {
  Où paye, où tourne ail -- leurs tes pas.
}
\tag #'(ombre basse) {
  De gra -- ce, par pi -- tié, ne me re -- but -- te pas.
}
\tag #'(charon basse) {
  La pi -- tié n’est point i -- cy bas,
  Et Cha -- ron ne fait point de gra -- ce.
}
\tag #'(ombre basse) {
  He -- las ! Cha -- ron, he -- las ! he -- las !
}
\tag #'(charon basse) {
  Crie he -- las ! tant que tu vou -- dras,
  Rien pour rien, en tous lieux est u -- ne loy sui -- vi -- e :
  les mains vui -- des sont sans ap -- pas,
  et ce n’est point as -- sés de pay -- er dans la vi -- e.
  Il faut en -- cor pay -- er au de -- là du tré -- pas.
}
\tag #'(ombre basse) {
  He -- las ! Cha -- ron, he -- las !
  \tag #'ombre { he -- las ! }
  \tag #'basse { he - }
}
\tag #'(charon basse) {
  Il m’im -- por -- te peu que l’on cri -- e
  He -- las ! Cha -- ron, he -- las ! he -- las !
  Il faut en -- cor pay -- er au de -- là du tré -- pas.
}
