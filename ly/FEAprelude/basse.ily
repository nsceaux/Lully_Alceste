\clef "basse" sol,2 re4. re8 |
la,2. fa,4 |
sib,2 <>^\markup\whiteout [flûtes] sib4. sol8 |
do'1 |
re'2 <>^\markup\whiteout tous re4. re8 |
sol4. fa8 mib4 re |
do2 <>^\markup\whiteout flûtes do'4. do'8 |
fa'4 sib fa2 |
sib <>^\markup\whiteout tous sib, |
la, re4 sol, |
fad,1 |
sol,2 la, |
re, <>^\markup\whiteout flûtes re' |
sol2. sol4 |
fa2 sol |
do'2 <>^\markup\whiteout tous do4. sib,8 |
la,2 sol, |
fa, <>^\markup\whiteout flûtes fa4. fa8 |
do'2 sol4. sol8 |
mib'1 |
re'2 <>^\markup\whiteout tous re4. re8 |
fad,4 sol, re,2 |
sol,2 <>^\markup\whiteout flûtes do'4. do'8 |
fa'4 sib mib'2 |
re' <>^\markup\whiteout tous re4. re8 |
fad,4 sol, re,2 |
sol,1 |
