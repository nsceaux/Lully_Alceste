\clef "dessus" <>^\markup\whiteout "Violons et flûtes" sol''4. re''8 fa''4. fa''8 |
fa''4. mib''16 re'' mib''4. fa''8 |
re''2\trill <>^\markup\whiteout "flûtes seules" \twoVoices #'(dessus1 dessus2 dessus) <<
  { sib''4. sib''8 |
    sib''4. la''8 la''4.\trill sol''8 |
    fad''2 }
  { re''4. re''8 |
    mib''4. re''8 do''4.\trill sib'8 |
    la'2 }
>> <>^\markup\whiteout "tous" re''4. do''8 |
si'4. si'8 do''4 re'' |
mib''2 <>^\markup\whiteout "flûtes" \twoVoices #'(dessus1 dessus2 dessus) <<
  { sol''4. sol''8 |
    la''4 sib'' la''4.(\trill sol''16 la'') |
    sib''2 }
  { mib''4. mib''8 |
    mib''4 re'' do''4.\trill sib'8 |
    sib'2 }
>> <>^\markup\whiteout "tous" re''4. mi''8 |
fa''4. fa''8 fad''4. sol''8 |
la''2 la''4. sib''8 |
sol''4.\trill fa''8 mi''4.\trill re''8 |
re''2 <>^\markup\whiteout "flûtes" \twoVoices #'(dessus1 dessus2 dessus) <<
  { fad''4. fad''8 |
    sol''2 sol''4. re''8 |
    mib''4. fa''8 re''4.\trill do''8 |
    do''4. }
  { re''4. do''8 |
    si'2 si'4. si'8 |
    do''4. re''8 si'4. do''8 |
    do''4. }
>> <>^\markup\whiteout "tous" mi''8 mi''4. mi''8 |
fa''4. sol''8 sol''4.(\trill fa''16 sol'') |
la''4. <>^\markup\whiteout "flûtes" \twoVoices #'(dessus1 dessus2 dessus) <<
  { fa''8 do''4. re''8 |
    mib''4. fa''8 re''4. re''8 |
    re''4. mib''8 do''4.(\trill sib'16 do'') |
    re''2 }
  { la'8 la'4. si'8 |
    do''4 sol'8 la' sib'4. la'8 |
    sol'4. fad'8 sol'4. la'8 |
    fad'2 }
>> <>^\markup\whiteout "tous" la'4. la'8 |
la'4. sib'8 la'4.\trill sol'8 |
sol'2 <>^\markup\whiteout "flûtes" \twoVoices #'(dessus1 dessus2 dessus) <<
  { mib''4. mib''8 |
    mib''4 re'' re''(\trill do'') |
    re''2 }
  { do''4. sib'8 |
    la'4 sib' sol' la' |
    fad'2 }
>> <>^\markup\whiteout "tous" la''4. la''8 |
la''4. sib''8 la''4.\trill sol''8 |
sol''1 |
