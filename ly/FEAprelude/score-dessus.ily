\score {
  \new GrandStaff <<
    \new Staff <<
      \global \keepWithTag #'dessus1 \includeNotes "dessus"
      { s1*5\break s1*5\break s1*6\break s1*5\break }
    >>
    \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
  >>
  \layout { }
}
