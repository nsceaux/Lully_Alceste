\score {
  \new StaffGroup <<
    \new GrandStaff <<
      \new Staff << \global \keepWithTag #'dessus1 \includeNotes "dessus" >>
      \new Staff << \global \keepWithTag #'dessus2 \includeNotes "dessus" >>
    >>
    \new Staff << \global \includeNotes "haute-contre" >>
    \new Staff << \global \includeNotes "taille" >>
    \new Staff << \global \includeNotes "quinte" >>
    \new Staff <<
      \global \includeNotes "basse"
      \includeFigures "chiffres"
      \origLayout {
        s1*7 s2 \bar "" \break s2 s1*7\pageBreak
        s1*7\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
