\clef "haute-contre" sib'4. sib'8 la'4. sib'8 |
do''2. do''4 |
sib'2 r |
R1 |
r2 la'4. la'8 |
sol'4. sol'8 la'4 si' |
do''2 r |
R1 |
r2 fa'4. sol'8 |
la'4. la'8 la'4 sib' |
do''2 do''4. re''8 |
sib'4. sib'8 la'4. sol'8 |
fad'2 r |
R1*2 |
r4 r8 do'' do''4 sol' |
la'4. sib'8 sib'4.\trill la'16 sib' |
do''4 r r2 |
R1*2 |
r2 fad'4. fad'8 |
la'4. sol'8 fad'4. sol'8 |
sol'2 r |
R1 |
r2 re''4. re''8 |
re''4 re'' re''4. do''8 |
si'1 |
