\clef "quinte" sol4. sol8 la4. la8 |
la4. sib8 do'2 |
fa r |
R1 |
r2 re'4. re'8 |
re'2 do'4 si |
sol2 r |
R1 |
r2 sib4. sib8 |
do'4. do'8 do'4 sib |
la2. re'4 |
re'4. re'8 la4. la8 |
la2 r |
R1*2 |
r4 r8 do' do'4. do'8 |
do'4. sib8 sib4. la8 |
la4 r r2 |
R1*2 |
r2 la4. la8 |
re'4. re'8 re'4 la |
si2 r |
R1 |
r2 la4. la8 |
re'4. re'8 re'4. re'8 |
re'1 |
