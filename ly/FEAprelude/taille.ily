\clef "taille" re'4. re'8 re'4 mi' |
fa'2. fa'4 |
fa'2 r |
R1 |
r2 fad'4. fad'8 |
sol'2. sol'4 |
sol'2 r |
R1 |
r2 re'4. re'8 |
do'4. do'8 re'4. re'8 |
re'2. re'4 |
dod'4 re' dod'4. re'8 |
re'2 r |
R1*2 |
r4 r8 sol' sol'4. sol'8 |
fa'4. fa'8 fa'4 mi' |
fa'4 r r2 |
R1*2 |
r2 re'4. re'8 |
re'4 re' re'4. re'8 |
re'2 r |
R1 |
r2 fad'4. fad'8 |
la'4. sol'8 fad'4. sol'8 |
sol'1 |
