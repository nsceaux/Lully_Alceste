\clef "taille" r4 r mi'8. fa'16 |
sol'2 fa'8. mi'16 |
re'2 sol'4 |
fa' fa' re' |
mi'2 do'8. do'16 |
re'2 re'4 |
mi' mi' re' |
re'2 r8 mi' |
fa'2 r8 fa' |
mi' re' do' sol' sol'8. sol'16 |
fa'4 la'8 la' la'8. la'16 |
sol'4. sol'8 sol'8. sol'16 |
sol'8 fa' mi' mi' mi'8. mi'16 |
re'4. re'8 re'8. re'16 |
re'4. sol'8 sol'8. sol'16 |
sol'4. sol'8 fa' mi' |
fa'4. fa'8 fa'8. la'16 |
sol'4. sol'8 sol'8. sol'16 |
sol'4. sol'8 sol'8. sol'16 |
sol'2. |
