\clef "vhaute-contre" r4 r mi'8. fa'16 |
sol'2 fa'8. mi'16 |
re'2 sol'4 |
fa' fa' re' |
mi'2 mi'8. fad'16 |
sol'2 sol'4 |
sol' sol' fad' |
sol'2 r8 sol' |
la'2 r8 la' |
sol'[\melisma fa' mi' re' do' si] |
la4.\melismaEnd la'8 la'8. la'16 |
sol'4. re'8 mi'[\melisma fa'] |
sol'[ fa' mi']\melismaEnd mi' mi'8. mi'16 |
re'4. fad'8 fad'8. fad'16 |
sol'4. re'8 mi'8. sol'16 |
sol'2 r8 mi' |
fa'[ mi' fa' sol' fa' mi']( |
re'4.) sol'8 sol'8. sol'16 |
sol'4. sol'8 sol'8. sol'16 |
sol'2. |
