\clef "haute-contre" r4 r do''8. si'16 |
do''2 sol'8. sol'16 |
sol'2 sol'4 |
la' la' sol' |
sol'2 mi'8. fad'16 |
sol'2 sol'4 |
sol' sol' fad' |
sol'2 r8 sol' |
la'2 r8 la' |
sol'4. sol'8 do''8. do''16 |
do''8 la' re'' mi'' re'' do'' |
si'2 r8 si' |
do'' si' do'' si' la' sol' |
fad'4. fad'8 fad'8. fad'16 |
sol'4. si'8 si'8. si'16 |
do''2 r8 sol' |
la'4. la'8 si'8. do''16 |
re''4. re''8 do''8. re''16 |
si'8 la' si' do'' re'' si' |
do''2. |
