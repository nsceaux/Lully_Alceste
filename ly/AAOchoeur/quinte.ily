\clef "quinte" r4 r do'8. do'16 |
sol2 sol8. la16 |
si2 do'4 |
la re' re' |
do'2 do'8. do'16 |
sol2 sol4 |
do' la la |
sol2 r8 sol |
fa2 r8 do' |
do' re' mi' mi' do'8. do'16 |
do'8 do' re' re' re'8. re'16 |
re'4. re'8 re'8. re'16 |
mi'8 mi' mi' do' do'8. do'16 |
re'4 la8 la la8. la16 |
sol4. re'8 re'8. re'16 |
do'4. do'8 do'8. do'16 |
do'4. do'8 fa'8. fa'16 |
re'4. sol8 sol8. sol16 |
sol4. sol8 sol8. sol16 |
sol2. |
