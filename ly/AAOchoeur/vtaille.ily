\clef "vtaille" r4 r do'8. do'16 |
do'2 sol8. sol16 |
sol2 sol4 |
la la sol |
sol2 do'8. do'16 |
re'2 re'4 |
mi' mi' re' |
re'2 r8 mi' |
fa'2 r8 fa' |
mi'[ re' do'] do' do'8. do'16 |
do'8. la16 re'8[\melisma mi' re' do'] |
si2\melismaEnd r8 si |
do'[\melisma si do' sol la sol] |
fad4.\melismaEnd la8 la8. re'16 |
re'4. si8 si8. si16 |
do'2 r8 do' |
do'[ si la] la8 la8. la16 |
sol4. re'8 do'8. do'16 |
si8[ la si do' re'8.] si16 |
do'2. |
