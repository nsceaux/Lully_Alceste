\clef "basse" do2 do8. re16 |
mi2 mi8. fa16 |
sol2 mi4 |
fa re sol |
do2 do8. do16 |
si,2 si,4 |
la, la, re |
sol,2 r8 sol |
re mi fa sol la si |
do'4. do8 do8. do16 |
fa4. re8 re8. re16 |
sol8 la sol fa mi re |
do4. do8 la,8. la,16 |
re2 re8. re16 |
sol,2 sol8. sol16 |
do'8 re' do' sib la sol |
fa sol fa mi re do |
si,2 do8. do16 |
sol,2. |
do,2. |
