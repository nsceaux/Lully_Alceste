\clef "vdessus" r4 r sol''8. fa''16 |
mi''2 re''8. do''16 |
si'2\trill mi''4 |
do'' re'' si' |
do''2 sol'8. la'16 |
si'2 si'4 |
do'' do'' la' |
si'2\trill r8 dod'' |
re'' re'' la'[\melisma si' do'' re''] |
mi''[ fa'' sol'']\melismaEnd mi'' mi''8. mi''16 |
fa''4. fa''8 fa''8. fa''16 |
re''4.\trill re''8 sol''[\melisma fa''] |
mi''[ re'' do'']\melismaEnd do'' do''8. do''16 |
la'8.\trill la'16 re''8[ mi'' re'' do'']( |
si'8.) re''16 sol''8.[\melisma la''16 sol''8 fa''] |
mi''[ fa'' mi'' re'' do'' sib']( |
la'4.)\melismaEnd do''8 re''8. mi''16 |
fa''4. fa''8 mi''8. fa''16 |
re''8[ do'' re'' mi'' fa''8.] sol''16 |
mi''2.\trill |

