\clef "vbasse" r4 r do8. re16 |
mi2 mi8. fa16 |
sol2 mi4 |
fa re sol |
do2 do8. do16 |
si,2 si,4 |
la, la, re |
sol,2 r8 sol |
re[\melisma mi fa sol la si] |
do'4.\melismaEnd do'8 do do |
fa4. re8 re8. re16 |
sol8[\melisma la sol fa mi re] |
do4.\melismaEnd do8 la,8. la,16 |
re4. re8 re8. re16 |
sol,4. sol8 sol8. sol16 |
do'8[\melisma re' do' sib la sol] |
fa[ sol fa mi re do] |
si,4.\melismaEnd si,8 do8. do16 |
sol,2 r8 sol, |
do2. |
