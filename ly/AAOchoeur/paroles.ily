Re -- ve -- nez re -- ve -- nez plai -- sirs e -- xi -- lez ;
re -- ve -- nez plai -- sirs e -- xi -- lez ;
\tag #'vdessus {
  Vo -- lez, vo -- lez __ de tou -- tes parts,
  vo -- lez, vo -- lez, vo -- lez, __
  de tou -- tes parts,
  vo -- lez, __ vo -- lez, __
  vo -- lez, vo -- lez,
  vo -- lez, vo -- lez, __ vo -- lez.
}
\tag #'vhaute-contre {
  Vo -- lez, vo -- lez, __ vo -- lez, vo -- lez, vo -- lez __
  de tou -- tes parts,
  vo -- lez, vo -- lez de tou -- tes parts,
  vo -- lez __ de tou -- tes parts, vo -- lez, vo -- lez.
}
\tag #'vtaille {
  Vo -- lez, vo -- lez __ de tou -- tes parts, vo -- lez, __ vo -- lez, __
  vo -- lez, vo -- lez de tou -- tes parts,
  vo -- lez de tou -- tes parts,
  vo -- lez, vo -- lez, __ vo -- lez.
}
\tag #'vbasse {
  Vo -- lez, __ de tou -- tes parts, vo -- lez, vo -- lez, __
  vo -- lez, vo -- lez de tou -- tes parts,
  vo -- lez, vo -- lez __
  de tou -- tes parts, vo -- lez.
}
