\version "2.19.80"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}
\layout {
  \context {
    \Lyrics
    %% de la place sous les paroles (basse continue)
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.padding = #3
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header { title = "Alceste" }
  \markup\null
}

%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}

%% Musique
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Prologue
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\actn "Prologue"
%% 0-1
\pieceToc "Ouverture"
\includeScore "AAAouverture"
%% 0-2
\pieceToc\markup\wordwrap {
  La Nymphe de la Seine :
  \italic { Le Heros que j’attens ne reviendra-t’il pas ? }
}
\includeScore "AABair"
%% 0-3
\pieceToc "Bruit de Guerre"
\includeScore "AACbruitGuerre"
\newBookPart #'(basse-continue)
%% 0-4
\pieceToc\markup\wordwrap {
  La Nymphe de la Seine :
  \italic { Quel bruit de guerre m’épouvante ? }
}
\includeScore "AADrecit"
%% 0-5
\pieceToc "Rondeau pour la Gloire"
\includeScore "AAErondeau"
%% 0-6
\pieceToc\markup\wordwrap {
  La Nymphe de la Seine, La Gloire :
  \italic { Helas ! superbe Gloire, helas ! }
}
\includeScore "AAFrecit"
\newBookPart #'(dessus haute-contre)
%% 0-7
\pieceToc\markup\wordwrap {
  Chœur des Nayades et des Divinitez champestres :
  \italic { Qu’il est doux d'accorder ensemble }
}
\includeScore "AAGchoeur"
\newBookPart #'(basse-continue)
%% 0-8
\pieceToc\markup\wordwrap {
  La Nymphe des Thuilleries :
  \italic { L’Art d’accord avec la Nature }
}
\includeScore "AAHair"
%% 0-9
\pieceToc\markup\wordwrap { Air pour les divinités des fleuves }
\includeScore "AAIentree"
%% 0-10
\pieceToc\markup\wordwrap {
  La Nymphe de la Marne :
  \italic { L’Onde se presse } }
\includeScore "AAJair"
%% 0-11
\pieceToc\markup\wordwrap {
  Air pour les divinitez des fleuves et les nymphes
}
\includeScore "AAKloure"
\newBookPart #'(basse-continue)
%% 0-12
\pieceToc\markup\wordwrap {
  La Gloire, les Nymphes, chœur :
  \italic { Que tout retentisse }
}
\includeScore "AALchoeur"
\newBookPart #'(haute-contre)
%% 0-13
\pieceToc\markup\wordwrap {
  Air pour les divinités des fleuves et les nymphes
}
\includeScore "AAMentree"
%% 0-14
\pieceToc\markup\wordwrap {
  Chœur des divinités des Fleuves et des Nymphes :
  \italic { Quel Cœur sauvage } }
\includeScore "AANchoeur"
%% 0-15
\pieceToc\markup\wordwrap {
  Chœur des divinités, des Fleuves et les Nymphes :
  \italic { Revenez Plaisirs exilez } }
\includeScore "AAOchoeur"
\newBookPart #'(basse-continue)
%% 0-16
\pieceToc "Ouverture"
\reIncludeScore "AAAouverture" "AAPentract"
\actEnd "Fin du prologue"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte 1
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
%% 1-1
\act "Acte Premier"
\sceneDescription\markup\wordwrap-center {
  Le Théatre Représente un Port de Mer.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Le Chœur des Thessaliens, Alcide, Lycas
}
\pieceToc\markup\wordwrap {
  Chœur, Lycas, Alcide : \italic { Vivez, vivez, heureux Espoux }
}
\includeScore "BAAchoeurRecit"

%% 1-2
\scene "Scene II" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Alcide, Straton, & Lycas
}
\pieceToc\markup\wordwrap {
  Alcide, Straton, Lycas : \italic { L’amour a bien des maux }
}
\includeScore "BBAtrio"

%% 1-3
\scene "Scene III" "Scene III"
\sceneDescription\markup\wordwrap-center {
  Straton, Lychas
}
\pieceToc\markup\wordwrap {
  Straton, Lychas : \italic { Lychas, j’ay deux mots à te dire. }
}
\includeScore "BCArecit"

%% 1-4
\scene "Scene Quatriéme" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  Céphise, Straton
}
\pieceToc\markup\wordwrap {
  Céphise, Straton : \italic { Dans ce beau jour, quelle humeur sombre }
}
\includeScore "BDAritournelle"
\includeScore "BDBrecit"

%% 1-5
\scene "Scene Cinquiéme" "Scene V"
\sceneDescription\markup\wordwrap-center {
  Licomede, Straton, Céphise
}
\pieceToc\markup\wordwrap {
  Licomede, Céphise : \italic { Straton, donne ordre qu'on s'apreste }
}
\includeScore "BEArecit"

%% 1-6
\scene "Scene Sixiéme" "Scene VI"
\sceneDescription\markup\wordwrap-center {
  Pheres, Admete, et Alceste
}
\pieceToc\markup\wordwrap {
  Chœur, Pheres, Alceste : \italic { Vivez, vivez, heureux Espoux }
}
\includeScore "BFAchoeur"

%% 1-7
\scene "Scene Septiême" "Scene VII"
\sceneDescription\markup\wordwrap-center {
  Des Nymphes de la Mer et des Tritons
  viennent faire une fête marine
}
\pieceToc\markup\wordwrap { Air pour les matelots }
\includeScore "BGAair"
\newBookPart #'(haute-contre)

%% 1-8
\pieceToc\markup\wordwrap {
  Deux Tritons : \italic { Malgré tant d’orages }
}
\includeScore "BGBtritons"
\includeScore "BGChautbois"

%% 1-9
\pieceToc\markup { \concat { 2 \super e } Air – Gavotte }
\includeScore "BGDgavotte"

%% 1-10
\pieceToc\markup\wordwrap {
  Céphise, une nymphe, chœur :
  \italic { Jeunes Cœurs laissez-vous prendre }
}
\includeScore "BGEchoeur"
\newBookPart #'(haute-contre)
%% 1-11
\pieceToc\markup { \concat { 3 \super e } Air – Rondeau }
\includeScore "BGFrondeau"

%% 1-12
\pieceToc\markup\wordwrap {
  Céphise, une nymphe, chœur :
  \italic { Plus les ames sont rebelles }
}
\includeScore "BGGchoeur"

%% 13
\pieceToc\markup\wordwrap {
  Licomede, Straton, Admete, Alcide, Alceste, Céphise, chœur :
  \italic { On vous apreste }
}
\includeScore "BGHrecit"

%% 14
\scene "Scene Huitiême" "Scene VIII"
\sceneDescription\markup\wordwrap-center {
  Thetis, Admete, Alceste
}
\pieceToc\markup\wordwrap {
  Thetis, Admete, Alcide, chœur :
  \italic { Espoux infortuné redoute ma colere }
}
\includeScore "BHArecit"
%% 15
\pieceToc "Les Vents"
\includeScore "BHBvents"
\newBookPart #'(haute-contre)
%% 16
\scene "Scene Neuviême" "Scene IX"
\sceneDescription\markup\wordwrap-center {
  Eole, les Aquilons, et les zephirs
}
\pieceToc\markup\wordwrap {
  Eole : \italic { Le Ciel protege les Heros }
}
\includeScore "BIAeole"

%% 17
\pieceToc "Entr’acte"
\reIncludeScore "BGFrondeau" "BIBentracte"
\actEnd "Fin du Premier Acte"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte 2
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
%% 2-1
\act "Acte Second"
\sceneDescription\markup\wordwrap-center {
  Le Théatre Represente la principale Ville de l’ile de Scyros.
}
\scene "Scene Premiere" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Céphise, Straton
}
\pieceToc\markup\wordwrap {
  Céphise, Straton : \italic { Alceste ne vient point, & nous devons l’attendre }
}
\includeScore "CAArecit"
\newBookPart#'(basse)
%% 2-2
\scene "Scene Deuxiême" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Licomede, Alceste, et Straton
}
\pieceToc\markup\wordwrap {
  Licomede, Alceste, Straton : \italic { Allons, allons, la plainte est vaine }
}
\includeScore "CBArecit"
%% 2-3
\scene "Scene Troisiéme" "Scene III"
\sceneDescription\markup\wordwrap-center {
  Admete et Alcide
}
\pieceToc "Marche en rondeau"
\includeScore "CCAmarche"
%% 2-4
\pieceToc\markup\wordwrap {
  Admete & Alcide : \italic { Marchez, marchez, marchez }
}
\includeScore "CCBair"

%% 2-5
\scene "Scene Quatriême" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  Licomede, Straton, Admete, Alcide, Lychas
}
\pieceToc\markup\wordwrap {
  Licomede, Admete, Alcide, chœur : \italic { Marchez, marchez, marchez }
}
\includeScore "CDAchoeur"

%% 2-6
\pieceToc "Entrée"
\includeScore "CDBentree"

%% 2-7
\pieceToc\markup\wordwrap {
  Chœur, Lychas, Straton : \italic { Achevons d’emporter la Place }
}
\includeScore "CDCchoeur"

\scene "Scene Cinquiême" "Scene V"
\sceneDescription\markup\wordwrap-center { Pheres }
%% 2-8
\pieceToc\markup\wordwrap {
  Pheres : \italic { Courage Enfants, je suis à vous }
}
\includeScore "CEApheres"

\scene "Scene Sixiême" "Scene VI"
\sceneDescription\markup\wordwrap-center {
  Alcide, Pheres, Et Alceste
}
%% 2-9
\pieceToc\markup\wordwrap {
  Alcide, Pheres, Alceste :
  \italic { Rendez à vostre Fils cette aimable Princesse }
}
\includeScore "CFAritournelle"
\includeScore "CFBrecit"

\scene "Scene Septiême" "Scene VII"
\sceneDescription\markup\wordwrap-center {
  Alceste, Pheres, Céphise
}
%% 2-10
\pieceToc\markup\wordwrap {
  Alceste, Céphise, Pheres :
  \italic { Cherchons Admete promptement }
}
\includeScore "CGAtrio"

\scene "Scene Huitiême" "Scene VIII"
\sceneDescription\markup\wordwrap-center {
  Admete blessé, Cleante, Alceste, Pheres, Céphise, Soldats
}
%% 2-11
\pieceToc\markup\wordwrap {
  Alceste, Cleante, Admete :
  \italic { O Dieux ! quel spectacle funeste }
}
\includeScore "CHArecit"
\newBookPart #'(basse-continue)
\scene "Scene Neuviéme" "Scene IX"
\sceneDescription\markup\wordwrap-center {
  Apollon, les Arts, Admete, Alceste, Pheres, Céphise,
  Celante, soldats
}
%% 2-12
\pieceToc\markup\wordwrap {
  Apollon : \italic { La Lumiere aujourd’hui te doit estre ravie }
}
\includeScore "CIAritournelle"
\includeScore "CIBrecit"

%% 2-13
\pieceToc "Entr’acte"
\reIncludeScore "CCAmarche" "CICmarche"
\actEnd "Fin du Second Acte"

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte 3
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
\act "Acte Troisiême"
\sceneDescription\markup\wordwrap-center {
  Le Théatre Représente un Monument Élevé Par les Arts.
}
\scene "Scene Premiére" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Alceste, Pheres, Céphise
}
%% 3-1
\pieceToc\markup\wordwrap {
  Alceste, Pheres, Céphise :
  \italic { Ah pourquoy nous séparez-vous ? }
}
\includeScore "DAArecit"

\scene "Scene Seconde" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Pheres, Cleante
}
%% 3-2
\pieceToc\markup\wordwrap {
  Pheres, Cleante :
  \italic { Voyons encor mon Fils, allons, bastons nos pas }
}
\includeScore "DBArecit"

\scene "Scene III" "Scene III"
\sceneDescription\markup\wordwrap-center {
  Le Chœur, Admete, Pheres, Cleante.
}
%% 3-3
\pieceToc\markup\wordwrap {
  Chœur, Admete, Pheres, Cleante :
  \italic { O trop heureux Admete }
}
\includeScore "DCArecit"
\partNoPageBreak #'(basse-continue)

\scene "Scene IV" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  Céphise, Admete, Pheres, Cleante, le Chœur.
}
%% 3-4
\pieceToc\markup\wordwrap {
  Céphise, Admete, Chœur :
  \italic { Alceste est morte }
}
\includeScore "DDArecit"
\newBookPart #'(basse-continue)

\scene "Scene Cinquiême" "Scene V"
\sceneDescription\markup\wordwrap-center {
  Troupe de femmes affligées, Troupe d’Hommes desolez,
  qui portent des fleurs, & tous les ornements qui ont servy
  à parer Alceste.
}
%% 3-5
\pieceToc\markup Pompe funebre
\includeScore "DEAsimphonie"

%% 3-6
\pieceToc\markup\wordwrap {
  Une femme affligée, chœur : \italic { La Mort, la Mort barbare }
}
\includeScore "DEBair"
\newBookPart #'(basse-continue)
%% 3-7
\pieceToc\markup\wordwrap {
  Chœur :
  \italic { Que nos pleurs, que nos cris renouvellent sans cesse }
}
\includeScore "DECchoeur"

\scene "Scene Sixiême" "Scene VI"
\sceneDescription\markup\wordwrap-center {
  Admete, Pheres, Céphise, Cleante, suite.
}
%% 3-8
\pieceToc\markup\wordwrap {
  Admete :
  \italic { Sans Alceste, sans ses appas }
}
\includeScore "DFArecit"

\scene "Scene Septiême" "Scene VII"
\sceneDescription\markup\wordwrap-center {
  Alcide, Admete, Pheres, Céphise, Cleante.
}
%% 3-9
\pieceToc\markup\wordwrap {
  Alcide, Admete :
  \italic { Tu me vois arresté sur le point de partir }
}
\includeScore "DGArecit"

\scene "Scene Huitiême" "Scene VIII"
\sceneDescription\markup\wordwrap-center {
  Diane, Mercure, Alcide, Admete, Pheres, Céphise, Cleante.
}
%% 3-10
\pieceToc\markup\wordwrap {
  Diane : \italic { Le Dieu dont tu tiens la naissance }
}
\includeScore "DHArecit"

%% 3-11 
\pieceToc\markup Entr’acte
\reIncludeScore "BGAair" "DHBentracte"
\actEnd "Fin du Troisiême Acte"
\partBlankPageBreak #'(basse-continue)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte 4
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
\act "Acte Quatriême"
\sceneDescription\markup\wordwrap-center {
  Le Théatre Représente le Fleuve Acheron.
}
\scene "Scene Premiére" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Charon, les Ombres.
}
%% 4-1
\pieceToc\markup\wordwrap {
  Charon, les Ombres :
  \italic { Il faut passer tost ou tard }
}
\includeScore "EAArecit"

\scene "Scene Deuxiême" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Alcide, Charon, les Ombres.
}
%% 4-2
\pieceToc\markup\wordwrap {
  Alcide, Charon :
  \italic { Sortez, Ombres, faites moy place }
}
\includeScore "EBArecit"

\newBookPart #'(basse-continue)
\scene "Scene Troisiême" "Scene III"
\sceneDescription\markup\wordwrap-center {
  Pluton, Proserpine, l’Ombre d’Alceste, Suivans de Pluton.
}
%% 4-3
\pieceToc\markup\wordwrap {
  Pluton, Proserpine, chœur :
  \italic { Reçoy le juste prix de ton amour fidelle }
}
\includeScore "ECArecit"

\sceneDescription "Feste Infernale"
%% 4-4
\pieceToc\markup Premier Air
\includeScore "ECBair"

%% 4-5
\pieceToc\markup\wordwrap {
  Chœur : \italic { Tout mortel doit icy paroistre }
}
\includeScore "ECCchoeur"
\markup\fill-line {
  \null
  \right-column {
    \line {
      On reprend le Premier Air des Violons page
      \page-refIII #'ECBair .
    }
    \line {
      Ensuite l’air qui suit.
    }
  }
}

%% 4-6
\pieceToc\markup Deuxième Air
\includeScore "ECDair"

%% 4-7
\pieceToc\markup\wordwrap {
  Chœur : \italic { Chacun vient icy bas prendre place }
}
\includeScore "ECEchoeur"
\markup\wordwrap {
  On reprend le \concat { 2 \super e } Air
  page \page-refII#'ECDair .
  Ensuite le Chœur \italic { Chacun vient }
  page \page-refII#'ECEchoeur .
  Et on rejoüe encor le \concat { 2 \super e } Air pour finir.
  \score {
    { \clef "bass" \key fa \major \partial 2.
      <>_\markup { Finale du \concat { 2 \super e } air. }
      fa4. mib8 re do | \custosNote sib,4 }
    \layout {
      \context { \Staff \remove "Time_signature_engraver" }
    }
  }
}
\newBookPart #'(basse-continue)
\scene "Scene Quatriême" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  Alecton, Pluton, Proserpine.
}
%% 4-8
\pieceToc\markup\wordwrap {
  Alecton, Pluton :
  \italic { Quittez, quittez les Jeux, songez à vous deffendre }
}
\includeScore "EDArecit"

\scene "Scene Cinquiême" "Scene V"
\sceneDescription\markup\wordwrap-center {
  Alcide, Pluton, Proserpine, Alecton.
}
%% 4-9
\pieceToc\markup\wordwrap {
  Pluton, Alcide, Proserpine :
  \italic { Insolent jusqu’icy braves-tu mon courroux ? }
}
\includeScore "EEArecit"
\actEnd\markup Fin du Quatriême Acte

%% 4-10
\pieceToc\markup Entr’acte
\reIncludeScore "ECDair" "EEBentracte"
\markup\fill-line {
  \null
  \score {
    { \clef "bass" \key fa \major \partial 2.
      <>_\markup { Finale de l’entr’acte. }
      fa,4 fa8 mib re do | sib,2*1/2 \custosNote sib4 }
    \layout {
      \context { \Staff \remove "Time_signature_engraver" }
    }
  }
}
\partBlankPageBreak #'(basse-continue)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%
%%% Acte 5
%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newBookPart #'()
\act "Acte Cinquiême"
\sceneDescription\markup\wordwrap-center {
  Le Théatre Représente un Arc de Triomphe.
}
\scene "Scene Premiére" "Scene I"
\sceneDescription\markup\wordwrap-center {
  Admete
}
%% 5-1
\pieceToc\markup\wordwrap {
  Admete, chœur : \italic { Alcide est vainqueur du Trépas}
}
\includeScore "FAAadmeteChoeur"

\scene "Scene II" "Scene II"
\sceneDescription\markup\wordwrap-center {
  Lychas, Straton.
}
%% 5-2
\pieceToc\markup\wordwrap {
  Straton, Lychas : \italic { Ne m'osteras-tu point la chaine qui m'accable }
}
\includeScore "FBAstratonLychas"
\newBookPart#'(basse)

\scene "Scene Troisiême" "Scene III"
\sceneDescription\markup\wordwrap-center {
  Céphise, Lychas, Straton.
}
%% 5-3
\pieceToc\markup\wordwrap {
  Lychas, Straton, Céphise : \italic { Voy, Céphise, voy qui de nous }
}
\includeScore "FCAcephiseLychasStraton"

\scene "Scene Quatriême" "Scene IV"
\sceneDescription\markup\wordwrap-center {
  Alcide, Admete, Alceste.
}
%% 5-4
\pieceToc\markup\wordwrap {
  Alcide, Alceste, Admete : \italic { Pour une si belle victoire }
}
\includeScore "FDAprelude"
\includeScore "FDBrecit"

\scene "Scene Cinquiême" "Scene V"
\sceneDescription\markup\wordwrap-center {
  Apollon, les Muses.
}
%% 5-5
\pieceToc "Prélude"
\includeScore "FEAprelude"
\newBookPart #'(haute-contre)
%% 5-6
\pieceToc\markup\wordwrap {
  Apollon : \italic { Les Muses & les Jeux s’empressent de descendre }
}
\includeScore "FEBrecit"
\scene "Sixiême et derniére Scene" "Scene VI"
%% 5-7
\pieceToc\markup\wordwrap {
  Chœur : \italic { Chantons, chantons, faisons entendre }
}
\includeScore "FFAchoeur"

%% 5-8
\pieceToc\markup Premier Air
\includeScore "FFBair"
\newBookPart #'(dessus)
%% 5-9
\pieceToc\markup { \concat { 2 \super e } Air – Les Pastres }
\includeScore "FFCair"

%% 5-10
\pieceToc\markup\wordwrap {
  Straton : \italic { A quoy bon Tant de raison }
}
\includeScore "FFDstraton"
%% 5-11
\pieceToc\markup { \concat { 3 \super e } Air – Menuet }
\includeScore "FFEmenuet"
\newBookPart #'(haute-contre)
%% 5-12
\pieceToc\markup\wordwrap {
  Céphise, chœur : \italic { C'est la saison d'aimer }
}
\includeScore "FFFcephiseChoeur"
\actEnd Fin du Cinquiême et dernier Acte
