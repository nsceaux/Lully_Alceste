\livretAct ACTE QUATRIÉME
\livretDescAtt\justify {
  Le Theatre represente le Fleuve Acheron & ses sombres Rivages.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  CHARON, LES OMBRES.
}
\livretPersDidas\smallCaps Charon, ramant sa Barque
\livretRef#'EAArecit
%# Il faut passer tost ou tard,
%# Il faut passer dans ma Barque.
%# On y vient jeune, ou vieillard,
%# Ainsi qu'il plaist à la Parque;
%# On y reçoit sans égard,
%# Le Berger, & le Monarque.
%# Il faut passer tost ou tard,
%# Il faut passer dans ma Barque.
%# Vous qui voulez passer, venez, Manes errants,
%# Venez, avancez, tristes Ombres,
%# Pay=ez le tribut que je prens,
%# Où retournez errer sur ces Rivages sombres.
\livretPers Les Ombres
%# Passe-moy, Charon, passe-moy.
\livretPers Charon
%# Il faut auparavant que l'on me satisfasse,
%# On doit pay=er les soins d'un si penible employ.
\livretPers Les Ombres
%# Passe-moy, Charon, passe-moy.
\livretPers Charon
%# Donne, passe, donne, passe,
%# Demeure toy.
%# Tu n'as rien, il faut qu'on te chasse.
\livretPersDidas\smallCaps Une Ombre rebuttée
%# Une Ombre tient si peu de place.
\livretPers Charon
%# Où paye, où tourne ailleurs tes pas.
\livretPers L'Ombre
%# De grace, par pitié, ne me rebutte pas.
\livretPers Charon
%# La pitié n'est point icy bas,
%# Et Charon ne fait point de grace.
\livretPers L'Ombre
%# Helas! Charon, helas! helas!
\livretPers Charon
%# Crie helas! tant que tu voudras,
%# Rien pour rien, en tous lieux est une loy suivie:
%# Les mains vides sont sans appas,
%# Et ce n'est point assés de pay=er dans la vie.
%#12 Il faut encore pay=er au delà du Trépas.
\livretPersDidas\smallCaps L'Ombre en se retirant
%# Helas! Charon, helas! helas!
\livretPers Charon
%# Il m'importe peu que l'on crie
%# Helas! Charon, helas! helas!
%#12 Il faut encore pay=er au delà du Trépas.

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  ALCIDE, CHARON, LES OMBRES.
}
\livretPersDidas\smallCaps Alcide sautant dans la Barque
\livretRef #'EBArecit
%# Sortez, Ombres, faites moy place,
%# Vous passerez une autre fois.
\livretDidasP\line { Les Ombres s’enfuïent. }
\livretPers Charon
%# Ah ma Barque ne peut souffrir un si grand poids!
\livretPers Alcide
%# Allons, il faut que l'on me passe.
\livretPers Charon
%# Retire-toy d'icy, Mortel, qui que tu sois,
%# Les Enfers irritez puniront ton audace.
\livretPers Alcide
%# Passe-moy, sans tant de façons.
\livretPers Charon
%# L'eau nous gagne, ma Barque créve.
\livretPers Alcide
%# Allons, rame, dépesche, achéve.
\livretPers Charon
%# Nous enfonçons
\livretPers Alcide
%# Passons, passons.

\livretScene SCENE III
\livretDescAtt\center-column {
  \justify {
    Le Theatre change, & represente le Palais de Pluton.
  }
  \wordwrap-center {
    PLUTON, PROSERPINE, L’OMBRE D’ALCESTE, Suivans de Pluton.
  }
}
\livretPersDidas\smallCaps Pluton sur son Thrône
\livretRef #'ECArecit
%# Reçoy le juste prix de ton amour fidelle;
%# Que ton destin nouveau soit heureux à jamais:
%# Commence de goûter la douceur eternelle
%# D'une profonde paix.
\livretPers Suivants de Pluton
%# Commence de goûter la douceur eternelle
%# D'une profonde paix.
\livretPersDidas\smallCaps Proserpine à costé de Pluton
%# L'espouze de Pluton te retient auprés d'elle:
%# Tous tes vœux seront satisfaits.
\livretPers Suivants de Pluton
%# Commence de goûter la douceur eternelle
%# D'une profonde paix.
\livretPers Pluton & Proserpine
%# En faveur d'une Ombre si belle,
%# Que l'Enfer fasse voir tout ce qu'il a d'attraits.
\livretPers Suivants de Pluton
%# En faveur d'une Ombre si belle,
%# Que l'Enfer fasse voir tout ce qu'il a d'attraits.
\livretRef#'ECBair
\livretDidasPPage\justify {
  Les Suivants de Pluton se réjoüissent de la venuë d’Alceste
  dans les Enfers, par une espece de Feste.
}
\livretPers Suivants de Pluton
\livretRef#'ECCchoeur
%# Tout mortel doit icy paroistre,
%# On ne peut naistre
%# Que pour mourir:
%# De cent maux le Trespas délivre;
%# Qui cherche à vivre
%# Cherche à souffrir.
%# Venez tous sur nos sombres bords.
%# Le repos qu'on desire
%# Ne tient son Empire
%# Que dans le sejour des Morts.
\null
\livretRef#'ECEchoeur
%# Chacun vient icy bas prendre place,
%# Sans cesse on y passe,
%# Jamais on n'en sort.
%# C'est pour tous une loy necessaire;
%# L'effort qu'on peut faire
%# N'est qu'un vain effort:
%# Est-on sage
%# De fuïr ce passage?
%# C'est un orage
%# Qui meine au Port.
%# Chacun vient icy bas prendre place,
%# Sans cesse on y passe,
%# Jamais on n'en sort.
%# Tous les charmes,
%# Plaintes, cris, larmes,
%# Tout est sans armes
%# Contre la Mort.
%# Chacun vient icy bas prendre place,
%# Sans cesse on y passe,
%# Jamais on n'en sort.

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  ALECTON, PLUTON, PROSERPINE, L’OMBRE D’ALCESTE, Suivans de Pluton.
}
\livretPers Alecton
\livretRef#'EDArecit
%# Quittez, quittez les Jeux, songez à vous deffendre,
%# Contre un audaci=eux unissons nos efforts:
%# Le Fils de Jupiter vient icy de descendre
%# Seul, il ose attaquer tout l'Empire des Morts.
\livretPers Pluton
%# Qu'on arreste ce Temeraire,
%# Armez vous, Amis, armez vous,
%# Qu'on deschaine Cerbere,
%# Courez tous, courez tous.
\livretDidasP\justify { On entend aboyer Cerbere. }
\livretPers Alecton
%# Son bras abat tout ce qu'il frappe.
%# Tout cede à ses horribles coups.
%# Rien ne resiste, rien n'eschape.

\livretScene SCENE V
\livretDescAtt\wordwrap-center {
  ALCIDE, PLUTON, PROSERPINE, ALECTON, Suivans de Pluton.
}
\livretPersDidas\smallCaps Pluton voyant Alcide qui enchaine Cerbere.
\livretRef#'EEArecit
%# Insolent jusqu'icy braves-tu mon courroux?
%# Quelle injuste audace t'engage,
%# A troubler la paix de ces lieux?
\livretPers Alcide
%# Je suis né pour dompter la rage
%# Des Monstres les plus furi=eux.
\livretPers Pluton
%# Est-ce le Dieu jaloux qui lance le Tonnerre
%# Qui t'oblige à porter la guerre
%# Jusqu'au centre de l'Univers?
%# Il tient sous son pouvoir & le Ciel & la Terre,
%# Veut-il encor ravir l'empire des Enfers?
\livretPers Alcide
%# Non, Pluton, regne en paix, joü=is de ton partage;
%# Je viens chercher Alceste en cét affreux Séjour,
%# Permets que je la rende au jour,
%# Je ne veux point d'autre avantage.
%# Si c'est te faire outrage
%# D'entrer par force dans ta Cour
%# Pardonne à mon Courage
%# Et fais grace à l'Amour.
\livretPers Proserpine
%# Un grand Coœur peut tout quand il aime,
%# Tout doit ceder à son effort.
%# C'est un Arrest du Sort,
%# Il faut que l'Amour extréme
%# Soit plus fort
%# Que la Mort.
\livretPers Pluton
%# Les Enfers, Pluton luy-mesme,
%# Tout doit en estre d'accord;
%# Il faut que l'Amour extréme
%# Soit plus fort
%# Que la Mort.
\livretPers Suivans de Pluton
%# Il faut que l'Amour extréme
%# Soit plus fort
%# Que la Mort.
\livretPers Pluton
%# Que pour revoir le jour l'Ombre d'Alceste sorte;
\livretDidasP\justify {
  Pluton donne un coup de son Trident & fait sortir son Char.
}
%# Prenez place tous deux au Char dont je me sers:
%# Qu'au gré de vos vœux, il vous porte;
%# Partez, les chemins sont ouverts.
%# Qu'une volante Escorte
%# Vous conduise au travers
%# Des noires vapeurs des Enfers.
\livretDidasP\justify {
  Alcide & l’Ombre d’Alceste se placent sur le Char de Pluton,
  qui les enleve sous la conduite d’une Troupe volante de
  Suivants de Plutons.
}
\livretFinAct Fin du quatriéme Acte
\sep

