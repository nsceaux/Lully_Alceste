\livretAct ACTE CINQUIÉME
\livretDescAtt\justify {
  Le Theatre change, & represente un Arc de Triomphe au milieu de deux
  Amphiteatres, où l’on void une multitude de differents Peuples de la
  Gréce assemblez pour recevoir Alcide triomphant des Enfers.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  ADMETE, LE CHŒUR.
}
\livretPers Admete
\livretRef#'FAAadmeteChoeur
%# Alcide est vainqueur du Trépas,
%# L’Enfer ne luy resiste pas.
%# Il rameine Alceste vivante;
%# Que chacun chante,
%# Alcide est vainqueur du Trépas,
%# L'Enfer ne luy resiste pas.
\livretPersDidas Le Chœur \line { sur l’Arc de Triomphe & sur les Amphiteatres }
%# Alcide est vainqueur du Trépas,
%# L’Enfer ne luy resiste pas.
\livretPers Admete
%# Quelle douleur secrette
%# Rend mon ame inqui=ette,
%# Et trouble mon amour.
%# Alceste voit encor le jour,
%# Mais c'est pour un autre qu'Admete.
\livretPers Le Chœur
%# Alcide est vainqueur du Trépas,
%# L’Enfer ne luy resiste pas.
\livretPers Admete
%# Ah! du moins cachons ma tristesse;
%# Alceste dans ces Lieux rameine les plaisirs.
%# Je dois rougir de ma foiblesse,
%# Quelle honte à mon cœur de mesler des souspirs
%# Avec tant de cris d'allegresse.
\livretPers Le Chœur
%# Alcide est vainqueur du Trépas,
%# L’Enfer ne luy resiste pas.
\livretPers Admete
%# Par une ardeur impati=ente
%# Courons, & devançons ses pas.
%# Il rameine Alceste vivante,
%# Que chacun chante.
\livretPers Admete & le Chœur
%# Alcide est vainqueur du Trépas,
%# L’Enfer ne luy resiste pas.

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  LYCHAS, STRATON enchaisné.
}
\livretPers Straton
\livretRef#'FBAstratonLychas
%# Ne m'osteras-tu point la chaine qui m'accable,
%# Dans ce jour destiné pour tant d'aimables jeux!
%# Ah! qu'il est rigoureux
%# D'estre seul miserable
%# Quand on voit tout le monde *heureux!
\livretPersDidas Lychas mettant Straton en liberté
%# Aujourd'huy qu'Alcide rameine
%# Alceste des Enfers,
%# Je veux finir ta peine.
%# Qu'on ne porte plus d'autres fers
%# Que ceux dont l'Amour nous enchaine.
\livretPers Straton, & Lychas
%# Qu'on ne porte plus d'autres fers
%# Que ceux dont l'Amour nous enchaine.

\livretScene SCENE III
\livretDescAtt\wordwrap-center {
  CÉPHISE, LYCHAS, STRATON.
}
\livretPers Lychas, & Straton
\livretRef#'FCAcephiseLychasStraton
%# Voy, Céphise, voy qui de nous
%# Peut rendre ton destin plus doux,
%# Et termine enfin nos querelles.
\livretPers Lychas
%# Mes amours seront eternelles.
\livretPers Straton
%# Mon cœur ne sera plus jaloux.
\livretPers Lychas & Straton
%# Entre deux Amants fidelles,
%# Choisis un heureux Espoux.
\livretPers Céphise
%# Je n'ay point de choix à faire;
%# Parlons d'aimer & de plaire,
%# Et vivons toûjours en paix.
%# L'*Himen détruit la tendresse
%# Il rend l'Amour sans attraits;
%# Voulez-vous aimer sans cesse,
%# Amants, n'espousez jamais.
\livretPers Céphise, Lychas, & Straton
%# L'*Himen détruit la tendresse
%# Il rend l'Amour sans attraits;
%# Voulez-vous aimer sans cesse,
%# Amants, n'espousez jamais.
\livretPers Céphise
%# Prenons part aux transports d'une joye éclatante:
%# Que chacun chante.
\livretPers Tous ensemble
%# Alcide est vainqueur du Trépas,
%# L’Enfer ne luy resiste pas.
%# Il rameine Alceste vivante;
%# Que chacun chante,
%# Alcide est vainqueur du Trépas,
%# L'Enfer ne luy resiste pas.

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  ALCIDE, ALCESTE, ADMETE, CÉPHISE, LYCHAS, STRATON, PHERES, CLEANTE,
  LE CHŒUR.
}
\livretPers Alcide
\livretRef#'FDBrecit
%# Pour une si belle victoire
%# Peut-on avoir trop entrepris?
%# Ah qu'il est doux de courir à la gloire
%# Lors que l'Amour en doit donner le prix!
%# Vous détournez vos yeux! je vous trouve insensible?
%# Admete a seul icy vos regards les plus doux?
\livretPers Alceste
%# Je fais ce qui m'est possible
%# Pour ne regarder que vous.
\livretPers Alcide
%# Vous devez suivre mon envie,
%# C'est pour moy qu'on vous rend le jour.
\livretPers Alceste
%# Je n'ay pû reprendre la vie
%# Sans reprendre aussi mon amour.
\livretPers Alcide
%# Admete en ma faveur vous a cedé luy-mesme.
\livretPers Admete
%# Alcide pouvoit seul vous oster au Trépas.
%# Alceste, vous vivez, je revoy vos appas,
%# Ay-je pû trop pay=er cette douceur extréme.
\livretPers Admete & Alceste
%# Ah que ne fait-on pas
%# Pour sauver ce qu'on aime!
\livretPers Alcide
%# Vous soûpirez tous deux au gré de vos desirs;
%# Est-ce ainsi qu'on me tient parole?
\livretPersDidas Admete & Alceste ensemble
%# Pardonnez aux derniers soûpirs
%# D'un mal-heureux Amour qu'il faut qu'on vous immole.
\livretVerse#8 { \raise#0.5 { \raise#1 \column { Alceste, Ademete, } \right-brace#20 } il ne faut plus nous voir. }
\livretVerse#12  { D’un autre que  \raise#0.7 { \left-brace#20 \raise#1 \column { \line { de moy vostre sort } \line { de vous mon destin } } \right-brace#20 } doit dépendre, }
%# Il faut dans les grands Cœurs que l'Amour le plus tendre
%# Soit la Victime du devoir.
\livretVerse#8 { \raise#0.5 { \raise#1 \column { Alceste, Ademete, } \right-brace#20 } il ne faut plus nous voir. }
\livretDidasP\justify {
  Admete se retire, & Alceste offre sa main à Alcide qui arreste Admete,
  & luy cede la main qu’Alceste luy presente.
}
\livretPers Alcide
%# Non, non, vous ne devez pas croire
%# Qu'un Vainqueur des Tirans soit Tiran à son tour:
%# Sur l'Enfer, sur la Mort, j'emporte la victoire;
%# Il ne manque plus à ma gloire
%# Que de tri=ompher de l'Amour.
\livretPers Admete & Alcide
%# Ah quelle gloire extresme!
%# Quel *hero=ïque effort!
%# Le Vainqueur de la Mort
%# Tri=omphe de luy-mesme.

\livretScene SCENE V
\livretDescAtt\wordwrap-center {
  APOLLON, LES MUSES, LES JEUX, ALCIDE, ADMETE, ALCESTE, & leur Suite.
}
\livretDescAtt\justify {
  Apollon descend dans un Palais éclatant au milieu des Muses & des Jeux
  qu’il ameine pour prendre part à la joye d’Admete & d’Alceste, & pour
  celebrer le Triomphe d’Alcide.
}
\livretPers Apollon
\livretRef#'FEBrecit
%# Les Muses & les Jeux s’empressent de descendre,
%# Apollon les conduit dans ces aimables lieux.
%# Vous, à qui j'ay pris soin d'aprendre
%# A chanter vos Amours sur le ton le plus tendre,
%# Bergers, chantez avec les Dieux.
%# Chantons, chantons, faisons entendre
%# Nos chansons jusques dans les Cieux.

\livretScene SCENE SIXIÉME ET DERNIERE
\livretDescAtt\justify {
  Une Troupe de Bergers & de Bergeres, & une Troupe de Pastres, dont les
  uns chantent & les autres dancent, viennent par l’ordre d’Apollon
  contribuer à la rejoüissance.
}
\livretPersDidas Les Chœurs des Muses des Thessaliens & des Bergers chantent ensemble
\livretRef#'FFAchoeur
%# Chantons, chantons, faisons entendre
%# Nos chansons jusques dans les Cieux.
\livretPersDidas Straton chante au milieu des Pastres dançants
\livretRef#'FFDstraton
%# A quoy bon
%# Tant de raison
%# Dans le bel âge?
%# A quoy bon
%# Tant de raison
%# Hors de saison?
%# Qui craint le danger
%# De s’engager
%# Est sans courage:
%# Tout rit aux Amants.
%# Les Jeux charmants
%# Sont leur partage:
%# Tost, tost, tost, soy=ons contents,
%# Il vient un temps
%# Qu'on est trop sage.
\livretPersDidas Céphise \wordwrap { chante au milieu des Bergers & des Bergeres qui dancent }
\livretRef#'FFFcephiseChoeur
%# C'est la saison d'aimer
%# Quand on sçait plaire,
%# C'est la saison d'aimer
%# Quand on sçait charmer.
%# Les plus beaux de nos jours ne durent guére,
%# Le sort de la Beauté nous doit allarmer,
%# Nos Champs n'ont point de Fleur plus passagere;
%# C'est la saison d'aimer
%# Quand on sçait plaire,
%# C'est la saison d'aimer
%# Quand on sçait charmer.
%# Un peu d'amour est necessaire,
%# Il n'est jamais trop tost de s'enflamer;
%# Nous donne-t’on un cœur pour n'en rien faire?
%# C'est la saison d'aimer
%# Quand on sçait plaire,
%# C'est la saison d'aimer
%# Quand on sçait charmer.
\livretDidasP\justify {
  La Troupe des Bergers dance avec la Troupe des Pastres.
  Les Chœurs se respondent les uns aux autres, & s’unissent
  enfin tous ensemble.
}
\livretPers Les Chœurs
%# Tri=omphez, genereux Alcide,
%# Aimez en paix *heureux Espoux.
\livretVerse#8 { Que \raise#0.5 { \left-brace#20 \raise#1 \column { \line { toûjours la Gloire } \line { sans cesse l’Amour } } \right-brace#20 } vous guide. }
\livretVerse#12 { Joüissez à jamais des \raise#0.5 { \left-brace#20 \raise#1 \column { honneurs plaisirs } \right-brace#20 } les plus doux. }
%# Tri=omphez, genereux Alcide,
%# Aimez en paix *heureux Espoux.
\livretDidasP\justify { Apollon vole avec les Jeux. }
\livretFinAct Fin du cinquiéme & dernier Acte
