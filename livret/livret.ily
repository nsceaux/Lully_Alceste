\notesSection "Livret"
\markuplist\abs-fontsize-lines #8 \page-columns-title \act\line { LIVRET } {
\livretAct LE RETOUR DES PLAISIRS
\livretScene PROLOGUE
\livretDescAtt\justify {
  Le Theatre represente le Palais & les Jardins des Thuilleries ; La
  Nymphe de la Seine paroist apuyée sur une Urne au milieu d’une Allée
  dont les Arbres sont separez par des Fontaines.
}
\livretPers La Nymphe de la Seine
\livretRef #'AABair
\livretVerse#12 { Le Heros que j’attens ne reviendra-t’il pas ? }
\livretVerse#8 { Serai-je toûjours languissante }
\livretVerse#8 { Dans une si cruelle attente ? }
\livretVerse#12 { Le Heros que j’attens ne reviendra-t’il pas ? }
\livretVerse#8 { On n’entend plus d’Oyseau qui chante, }
\livretVerse#12 { On ne voit plus de Fleurs qui naissent sous nos pas. }
\livretVerse#12 { Le Heros que j’attens ne reviendra-t’il pas ? }
\livretVerse#4 { L’herbe naissante }
\livretVerse#4 { Paroist mourante, }
\livretVerse#12 { Tout languit avec moy dans ces lieux pleins d’appas. }
\livretVerse#12 { Le Heros que j’attens ne reviendra-t’il pas ? }
\livretVerse#8 { Serai-je toûjours languissante }
\livretVerse#8 { Dans une si cruelle attente ? }
\livretVerse#12 { Le Heros que j’attens ne reviendra-t’il pas ? }
\null
\livretRef #'AADrecit
\livretVerse#8 { Quel bruit de guerre m’épouvante ? }
\livretVerse#12 { Quelle Divinité va descendre icy bas ? }
\null
\livretRef #'AAErondeau
\livretDidasPPage\justify {
  La Gloire paroist au milieu d’un Palais brillant qui descend au
  bruit d’une harmonie guerriere
}
\livretPers La Nymphe de la Seine
\livretRef #'AAFrecit
\livretVerse#8 { Helas ! superbe Gloire, helas ! }
\livretVerse#8 { Ne dois-tu point estre contente ? }
\livretVerse#12 { Le Heros que j’attens ne reviendra-t’il pas ? }
\livretVerse#12 { Il ne te suit que trop dans l’horreur des Combas ; }
\livretVerse#12 { Laisse en paix un moment sa Valeur triomphante. }
\livretVerse#12 { Le Heros que j’attens ne reviendra-t’il pas ? }
\livretVerse#8 { Serai-je toûjours languissante }
\livretVerse#8 { Dans une si cruelle attente ? }
\livretVerse#12 { Le Heros que j’attens ne reviendra-t’il pas ? }
\livretPers La Gloire
\livretVerse#12 { Pourquoy tant murmurer ? Nymphe, ta plainte est vaine, }
\livretVerse#12 { Tu ne peux voir sans moy le Heros que tu sers ; }
\livretVerse#12 { Si son éloignement te couste tant de peine, }
\livretVerse#12 { Il recompense assés les douceurs que tu pers ; }
\livretVerse#12 { Voy ce qu’il fait pour toy quand la Gloire l’emmeine ; }
\livretVerse#12 { Voy comme sa Valeur a soûmis à la Seine }
\livretVerse#12 { Le Fleuve le plus fier qui soit dans l’Univers. }
\livretPers La Nymphe de la Seine
\livretVerse#8 { On ne voit plus icy paraistre }
\livretVerse#8 { Que des Ornements imparfaits ; }
\livretVerse#8 { Ah ! rends-nous nostre Auguste Maistre, }
\livretVerse#8 { Tu nous rendras tous nos attraits. }
\livretPers La Gloire
\livretVerse#8 { Il revient, & tu dois m’en croire ; }
\livretVerse#8 { Je luy sers de guide avec soin : }
\livretVerse#6 { Puisque tu vois la Gloire }
\livretVerse#6 { Ton Heros n’est pas loin. }
\livretVerse#12 { Il laisse respirer tout le Monde qui tremble ; }
\livretVerse#12 { Soyons icy d’accord pour combler ses desirs. }
\livretPers La Gloire et la Nymphe de la Seine
\livretVerse#8 { Qu’il est doux d’accorder ensemble }
\livretVerse#6 { La Gloire & les Plaisirs. }
\livretPers La Nymphe de la Seine
\livretVerse#12 { Nayades, Dieux des Bois, Nymphes, que tout s’assemble, }
\livretVerse#12 { Qu’on entende nos chants apres tant de soûpirs. }
\null
\livretDidasP\justify {
  La Nymphe des Thuilleries s’avance avec une Troupe de Nymphes qui
  dancent, les Arbres s’ouvrent & font voir des Divinitez Champestres
  qui joüent de differents Instruments, & les Fontaines se changent en
  Nayades qui chantent.
}
\livretPers Le Chœur
\livretRef #'AAGchoeur
\livretVerse#8 { Qu’il est doux d’accorder ensemble }
\livretVerse#6 { La Gloire & les Plaisirs. }
\livretPers La Nymphe des Thuilleries
\livretRef #'AAHair
\livretVerse#8 { L’Art d’accord avec la Nature }
\livretVerse#8 { Sert l’Amour dans ces lieux charmants : }
\livretVerse#12 { Ces Eaux qui font resver par un si doux murmure, }
\livretVerse#12 { Ces Tapis où les Fleurs forment tant d’ornements, }
\livretVerse#8 { Ces Gazons, ces Lits de verdure, }
\livretVerse#8 { Tout n’est fait que pour les Amants. }
\null
\livretRef #'AAIentree
\livretDidasPPage\justify {
  La Nymphe de la Marne Compagne de la Seine vient chanter au milieu
  d’une troupe de Divinitez de Fleuves qui témoignent leur joye par
  leur dance.
}
\livretPers La Nymphe de la Marne
\livretRef #'AAJair
\livretVerse#4 { L’Onde se presse }
\livretVerse#4 { D’aller sans cesse }
\livretVerse#6 { Jusqu’au bout de son cours : }
\livretVerse#8 { S’il faut qu’un Cœur suive une pante, }
\livretVerse#8 { En est-il qui soit plus charmante }
\livretVerse#8 { Que le doux penchant des Amours ? }
\livretPers La Gloire et la Nymphe de la Seine
\livretRef #'AALchoeur
\livretVerse#5 { Que tout retentisse : }
\livretVerse#7 { Que tout réponde à nos voix : }
\livretPers La Nymphe des Thuilleries
\livretVerse#4 { Que tout fleurisse }
\livretVerse#8 { Dans nos Jardins & dans nos Bois. }
\livretPers La Nymphe de la Marne
\livretVerse#8 { Que le chant des Oyseaux s’unisse }
\livretVerse#8 { Avec le doux son des Haut-bois. }
\livretPers Tous Ensemble
\livretVerse#5 { Que tout retentisse, }
\livretVerse#7 { Que tout réponde à nos voix. }
\livretVerse#8 { Que le chant des Oyseaux s’unisse }
\livretVerse#8 { Avec le doux son des Haut-bois. }
\livretVerse#5 { Que tout retentisse }
\livretVerse#7 { Que tout réponde à nos voix. }
\null
\livretRef #'AAMentree
\livretDidasPPage\justify {
  Les Divinitez de Fleuves & les Nymphes forment une dance generale
  tandis que tous les Instruments & toutes les Voix s’unissent.
}
\livretPers Tous Ensemble
\livretRef #'AANchoeur
\livretVerse#4 { Quel Cœur sauvage }
\livretVerse#5 { Icy ne s’engage ? }
\livretVerse#4 { Quel Cœur sauvage }
\livretVerse#5 { Ne sent point l’amour ? }
\livretVerse#10 { Nous allons voir les Plaisirs de retour ; }
\livretVerse#10 { Ne manquons pas d’en faire un doux usage : }
\livretVerse#9 { Pour rire un peu, l’on n’est pas moins sage. }
\livretVerse#4 { Ah quel dommage }
\livretVerse#5 { De fuir ce rivage ! }
\livretVerse#4 { Ah quel dommage }
\livretVerse#5 { De perdre un beau jour ! }
\livretVerse#10 { Nous allons voir les Plaisirs de retour ; }
\livretVerse#10 { Ne manquons pas d’en faire un doux usage : }
\livretVerse#9 { Pour rire un peu, l’on n’est pas moins sage. }
\livretRef #'AAOchoeur
\livretVerse#8 { Revenez Plaisirs exilez ; }
\livretVerse#8 { Volez, de toutes parts, volez. }
\null
\livretDidasP\justify {
  Les Plaisirs volents, & viennent preparer des Divertissements.
}
\livretFinAct Fin du Prologue
\sep

\livretAct ACTE PREMIER
\livretDescAtt\column {
  \justify {
    La Scene est dans la Ville d’Ycolos en Thessalie.
  }
  \justify {
    Le Theatre represente une Port de Mer, où l’on void un grand
    Vaisseau orné & preparé pour une Feste galante au milieu de
    plusieurs Vaisseaux de guerre.
  }
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  LE CHŒUR DES THESSALIENS, ALCIDE, LYCAS
}
\livretPers Le Chœur
\livretRef #'BAAchoeurRecit
\livretVerse#8 { Vivez, vivez, heureux Espoux. }
\livretPers Lychas
\livretVerse#12 { Vostre Amy le plus cher épouze la Princesse }
\livretVerse#8 { La plus charmante de la Grece, }
\livretVerse#12 { Lors que chacun les suit, Seigneur, les fuyez-vous ? }
\livretPers Le Chœur
\livretVerse#8 { Vivez, vivez, heureux Espoux. }
\livretPers Lychas
\livretVerse#12 { Vous paroissez troublé des cris qui retentissent ? }
\livretVerse#9 { Quand deux Amants heureux s’unissent }
\livretVerse#12 { Le Cœur du grand Alcide en seroit-il jaloux ? }
\livretPers Le Chœur
\livretVerse#8 { Vivez, vivez, heureux Espoux. }
\livretPers Lychas
\livretVerse#12 { Seigneur, vous soûpirez, & gardez le silence ? }
\livretPers Alcide
\livretVerse#12 { Ah Lychas, laisse-moy partir en diligence. }
\livretPers Lychas
\livretVerse#12 { Quoy dés ce mesme jour presser vostre départ ? }
\livretPers Alcide
\livretVerse#12 { J’auray beau me presser je partiray trop tard. }
\livretVerse#12 { Ce n’est point avec toy que je pretens me taire ; }
\livretVerse#12 { Alceste est trop aimable, elle a trop sçeu me plaire ; }
\livretVerse#12 { Un autre en est aimé, rien ne flatte mes vœux, }
\livretVerse#8 { C’en est fait, Admete l’espouze, }
\livretVerse#12 { Et c’est dans ce moment qu’on les unit tous deux. }
\livretVerse#6 { Ah qu’une ame jalouse }
\livretVerse#8 { Esprouve un tourment rigoureux ! }
\livretVerse#8 { J’ay peine à l’exprimer moy-mesme : }
\livretVerse#8 { Figure-toy, si tu le peux, }
\livretVerse#6 { Quelle est l’horreur extresme }
\livretVerse#6 { De voir ce que l’on aime }
\livretVerse#8 { Au pouvoir d’un Rival heureux. }
\livretPers Lychas
\livretVerse#12 { L’Amour est-il plus fort qu’un Heros indomptable ? }
\livretVerse#12 { L’univers n’a point eû de Monstre redoutable }
\livretVerse#8 { Que vous n’ayez pû surmonter. }
\livretPers Alcide
\livretVerse#12 { Eh crois-tu que l’Amour soit moins à redouter ? }
\livretVerse#8 { Le plus grand Cœur a sa foiblesse. }
\livretVerse#12 { Je ne puis me sauver de l’ardeur qui me presse }
\livretVerse#8 { Qu’en quittant ce fatal Séjour : }
\livretVerse#6 { Contre d’aimables charmes, }
\livretVerse#6 { La Valeur est sans armes, }
\livretVerse#12 { Et ce n’est qu’en fuyant qu’on peut vaincre l’Amour. }
\livretPers Lychas
\livretVerse#12 { Vous devez vous forcer, au moins, à voir la Feste }
\livretVerse#12 { Qui déja dans ce Port vous paroist toute preste. }
\livretVerse#12 { Vostre fuite à present feroit un trop grand bruit ; }
\livretVerse#8 { Differez jusques à la nuit. }
\livretPers Alcide
\livretVerse#12 { Ah Lycas ! qu’elle nuit ! ah qu’elle nuit funeste ! }
\livretPers Lychas
\livretVerse#12 { Tout le reste du jour voyez encore Alceste. }
\livretPers Alcide
\livretVerse#13 { La voir encore ?… hé bien differons mon départ, }
\livretVerse#12 { Je te l’avois bien dit, je partiray trop tard. }
\livretVerse#12 { Je vais la voir aimer un Espoux qui l’adore, }
\livretVerse#12 { Je verray dans leurs yeux un tendre empressement : }
\livretVerse#8 { Que je vais payer cherement }
\livretVerse#8 { Le plaisir de la voir encore ! }

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  ALCIDE, STRATON, & LYCAS
}
\livretPers Ensemble
\livretRef #'BBAtrio
\livretVerse#12 { L’amour a bien des maux, mais le plus grand de tous }
\livretVerse#8 { C’est le tourment d’estre jaloux. }

\livretScene SCENE III
\livretDescAtt\wordwrap-center {
  STRATON, LYCAS
}
\livretPers Straton
\livretRef #'BCArecit
\livretVerse#8 { Lychas, j’ay deux mots à te dire. }
\livretPers Lychas
\livretVerse#8 { Que veux-tu ? parle ; je t’entends. }
\livretPers Straton
\livretVerse#8 { Nous sommes amis de tous temps ; }
\livretVerse#12 { Céphise, tu le sçais, me tient sous son Empire. }
\livretVerse#12 { Tu suis par tout ses pas : qu’est-ce que tu pretens ? }
\livretPers Lychas
\livretVerse#4 { Je pretens rire. }
\livretPers Straton
\livretVerse#12 { Pourquoy veux-tu troubler deux Cœurs qui sont contents ? }
\livretPers Lychas
\livretVerse#4 { Je pretens rire. }
\livretVerse#8 { Tu peux à ton gré t’enflamer ; }
\livretVerse#8 { Chacun a sa façon d’aimer ; }
\livretVerse#8 { Qui voudra soûpirer, soûpire, }
\livretVerse#4 { Je pretens rire. }
\livretPers Straton
\livretVerse#12 { J’aime, & je suis aimé : laisse en paix nos amours. }
\livretPers Lychas
\livretVerse#12 { Rien ne doit t’allarmer s’il est bien vray qu’on t’aime ; }
\livretVerse#12 { Un Rival rebutté donne un plaisir extresme. }
\livretPers Straton
\livretVerse#12 { Un Rival quel qu’il soit importune toûjours. }
\livretPers Lychas
\livretVerse#8 { Je voy ton amour sans colere, }
\livretVerse#8 { Tu devrois en user ainsi : }
\livretVerse#8 { Puisque Céphise t’a sçeu plaire, }
\livretVerse#12 { Pourquoy ne veux-tu pas qu’elle me plaise aussi ? }
\livretPers Straton
\livretVerse#12 { A quoy sert-il d’aimer ce qu’il faut que l’on quitte ? }
\livretVerse#12 { Tu ne peux demeurer long-temps dans cette Cour. }
\livretPers Lychas
\livretVerse#12 { Moins on a de momens à donner à l’Amour, }
\livretVerse#8 { Et plus il faut qu’on en profite. }
\livretPers Straton
\livretVerse#12 { J’aime depuis deux ans avec fidelité : }
\livretVerse#8 { Je puis croire, sans vanité, }
\livretVerse#12 { Que tu ne dois pas estre un Rival qui m’alarme. }
\livretPers Lychas
\livretVerse#7 { J’ay pour moy la nouveauté, }
\livretVerse#7 { En amour c’est un grand charme. }
\livretPers Straton
\livretVerse#12 { Céphise m’a promis un cœur tendre, & constant. }
\livretPers Lychas
\livretVerse#8 { Céphise m’en promet autant. }
\livretPers Straton
\livretVerse#12 { Ah si je le croyois !… Mais tu n’es pas croyable. }
\livretPers Lychas
\livretVerse#12 { Croy-moy, fais ton profit d’un reste d’amitié, }
\livretVerse#8 { Sers-toy d’un avis charitable }
\livretVerse#8 { Que je te donne par pitié. }
\livretPers Straton
\livretVerse#7 { Le mespris d’une volage }
\livretVerse#7 { Doit estre un assés grand mal, }
\livretVerse#7 { Et c’est un nouvel outrage }
\livretVerse#7 { Que la pitié d’un Rival. }
\livretVerse#6 { Elle vient l’Infidelle, }
\livretVerse#12 { Pour chanter dans les Jeux dont je prens soins icy. }
\livretPers Lychas
\livretVerse#6 { Je te laisse avec elle, }
\livretVerse#12 { Il ne tiendra qu’à toy d’estre mieux éclaircy. }

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  CÉPHISE, STRATON
}
\livretPers Céphise
\livretRef #'BDBrecit
\livretVerse#8 { Dans ce beau jour, qu’elle humeur sombre }
\livretVerse#7 { Fais-tu voir à contre-temps ? }
\livretPers Straton
\livretVerse#8 { C’est que je ne suis pas du nombre }
\livretVerse#7 { Des Amants qui sont contents. }
\livretPers Céphise
\livretVerse#7 { Un ton grondeur & severe }
\livretVerse#7 { N’est pas un grand agrément ; }
\livretVerse#7 { Le chagrin n’avance guere }
\livretVerse#7 { Les affaires d’un Amant. }
\livretPers Straton
\livretVerse#8 { Lychas vient de me faire entendre }
\livretVerse#12 { Que je n’ay plus ton cœur, qu’il doit seul y pretendre, }
\livretVerse#12 { Et que tu ne vois plus mon amour qu’à regret ? }
\livretPers Céphise
\livretVerse#6 { Lychas est peu discret… }
\livretPers Straton
\livretVerse#12 { Ah je m’en doutois bien qu’il vouloit me surprendre. }
\livretPers Céphise
\livretVerse#6 { Lychas est peu discret }
\livretVerse#6 { D’avoir dit mon secret. }
\livretPers Straton
\livretVerse#12 { Coment ! il est donc vray ! tu n’en fais point d’excuse ? }
\livretVerse#12 { Tu me trahis ainsi sans en estre confuse ? }
\livretPers Céphise
\livretVerse#6 { Tu te plains sans raison ; }
\livretVerse#6 { Est-ce une trahison }
\livretVerse#6 { Quand on te desabuse ? }
\livretPers Straton
\livretVerse#12 { Que je suis estonné de voir ton changement ! }
\livretPers Céphise
\livretVerse#6 { Si je change d’Amant }
\livretVerse#6 { Qu’y trouves-tu d’étrange ? }
\livretVerse#8 { Est-ce un sujet d’estonnement }
\livretVerse#8 { De voir une Fille qui change ? }
\livretPers Straton
\livretVerse#11 { Apres deux ans passez dans un si doux lien, }
\livretVerse#12 { Devois-tu jamais prendre une chaine nouvelle. }
\livretPers Céphise
\livretVerse#6 { Ne contes-tu pour rien }
\livretVerse#6 { D’estre deux ans fidelle ? }
\livretPers Straton
\livretVerse#8 { Par un espoir doux, & trompeur, }
\livretVerse#12 { Pourquoy m’engageois-tu dans un amour si tendre ? }
\livretVerse#8 { Faloit-il me donner ton cœur }
\livretVerse#8 { Puis que tu voulois le reprendre ? }
\livretPers Céphise
\livretVerse#12 { Quand je t’offrois mon cœur, c’estoit de bonne foy }
\livretVerse#8 { Que n’empesche tu qu’on te l’oste ? }
\livretVerse#4 { Est-ce ma faute }
\livretVerse#8 { Si Lychas me plaist plus que toy ? }
\livretPers Straton
\livretVerse#12 { Ingrate, est-ce le prix de ma perseverance ? }
\livretPers Céphise
\livretVerse#7 { Essaye un peu de l’inconstance : }
\livretVerse#12 { C’est toy qui le premier m’apris à m’engager, }
\livretVerse#4 { Pour recompense }
\livretVerse#8 { Je te veux aprendre à changer. }
\livretPers Straton & Céphise
\livretVerse#6 { Il faut \raise#0.7 { \left-brace#20 \raise#1 \column { aimer changer } \right-brace#20 } toûjours. }
\livretVerse#6 { Les plus douces amours }
\livretVerse#6 { Sont les amours \raise#0.5 { \left-brace#20 \raise#1 \column { fidelles nouvelles } } }
\livretVerse#6 { Il faut \raise#0.7 { \left-brace#20 \raise#1 \column { aimer changer } \right-brace#20 } toûjours. }

\livretScene SCENE V
\livretDescAtt\wordwrap-center {
  LICOMEDE, STRATON, CÉPHISE
}
\livretPers Licomede
\livretRef #'BEArecit
\livretVerse#9 { Straton, donne ordre qu’on s’apreste }
\livretVerse#6 { Pour commencer la Feste. }
\livretDidasP\justify {
  Straton se retire, & Licomede parle à Céphise.
}
\livretVerse#12 { Enfin, grace au dépit, je gouste la douceur }
\livretVerse#12 { De sentir le repos de retour dans mon cœur. }
\livretVerse#12 { J’estois à preferer au Roy de Thessalie ; }
\livretVerse#8 { Et si pour sa gloire on publie }
\livretVerse#12 { Qu’Apollon autrefois luy servit de Pasteur, }
\livretVerse#12 { Je suis Roy de Scyros, & Thétis est ma Sœur. }
\livretVerse#12 { J’ay sçeu me consoler d’un hymen qui m’outrage, }
\livretVerse#12 { J’en ordonne les Jeux avec tranquilité. }
\livretVerse#8 { Qu’aisément le dépit dégage }
\livretVerse#8 { Des fers d’une ingrate Beauté ! }
\livretVerse#8 { Et qu’apres un long esclavage, }
\livretVerse#8 { Il est doux d’estre en liberté ! }
\livretPers Céphise
\livretVerse#12 { Il n’est pas seur toûjours de croire l’apparence : }
\livretVerse#8 { Un Cœur bien pris, & bien touché, }
\livretVerse#8 { N’est pas aisément détaché, }
\livretVerse#8 { Ny si tost guery que l’on pense ; }
\livretVerse#8 { Et l’amour est souvent caché }
\livretVerse#8 { Sous une feinte indifference. }
\livretPers Licomede
\livretVerse#7 { Quand on est sans esperance, }
\livretVerse#7 { On est bien tost sans amour. }
\livretVerse#8 { Mon Rival a la preference, }
\livretVerse#8 { Ce que j’aime est en sa puissance, }
\livretVerse#8 { Je perds tout espoir en ce jour : }
\livretVerse#7 { Quand on est sans esperance, }
\livretVerse#7 { On est bien tost sans amour. }
\livretVerse#12 { Voicy l’heure qu’il faut que la Feste commence, }
\livretVerse#4 { Chacun s’avance, }
\livretVerse#4 { Preparons-nous. }

\livretScene SCENE VI
\livretDescAtt\wordwrap-center {
  LE CHŒUR, ADMETE, ALCESTE, PHERES, ALCIDE, LYCHAS, CÉPHISE & STRATON
}
\livretPers Le Chœur
\livretRef #'BFAchoeur
\livretVerse#8 { Vivez, vivez, heureux Espoux. }
\livretPers Pheres
\livretVerse#12 { Joüissez des douceurs du nœud qui vous assemble. }
\livretPers Admete & Alceste
\livretVerse#12 { Quand l’Himen & l’Amour sont bien d’accord ensemble }
\livretVerse#8 { Que les nœuds qu’ils forment sont doux ? }
\livretPers Le Chœur
\livretVerse#8 { Vivez, vivez, heureux Espoux. }

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  Des Nymphes de la Mer, & des Tritons, viennent faire une Feste Marine,
  où se meslent des Matelots & des Pescheurs.
}
\livretPers Deux Tritons
\livretRef #'BGBtritons
\livretVerse#5 { Malgré tant d’orages, }
\livretVerse#5 { Et tant de naufrages, }
\livretVerse#5 { Chacun à son tour }
\livretVerse#6 { S’embarque avec l’Amour. }
\livretVerse#5 { Par tout où l’on meine }
\livretVerse#5 { Les Cœurs amoureux, }
\livretVerse#5 { On voit la Mer pleine }
\livretVerse#5 { D’Escueils dangereux, }
\livretVerse#5 { Mais sans quelque peine }
\livretVerse#6 { On n’est jamais heureux : }
\livretVerse#5 { Une ame constante }
\livretVerse#5 { Apres la tourmente }
\livretVerse#5 { Espere un beau jour. }
\livretVerse#5 { Malgré tant d’orages, }
\livretVerse#5 { Et tant de naufrages, }
\livretVerse#5 { Chacun à son tour }
\livretVerse#6 { S’embarque avec l’Amour. }
\null
\livretVerse#5 { Un cœur qui differe }
\livretVerse#5 { D’entrer en affaire }
\livretVerse#5 { S’expose à manquer }
\livretVerse#6 { Le temps de s’embarquer. }
\livretVerse#5 { Une ame commune }
\livretVerse#5 { S’estonne d’abord, }
\livretVerse#5 { Le soin l’importune, }
\livretVerse#5 { Le calme l’endort, }
\livretVerse#5 { Mais quelle fortune }
\livretVerse#6 { Fait-on sans quelque effort ? }
\livretVerse#5 { Est-il un commerce }
\livretVerse#5 { Exempt de traverse ? }
\livretVerse#5 { Chacun doit risquer. }
\livretVerse#5 { Un cœur qui differe }
\livretVerse#5 { D’entrer en affaire }
\livretVerse#5 { S’expose à manquer }
\livretVerse#6 { Le temps de s’embarquer. }
\null
\livretDidasP\justify {
  Céphise vestüe en Nymphe de la Mer, chante au milieu des Divinitez
  Marines qui luy respondent.
}
\livretRef #'BGEchoeur
\livretVerse#7 { Jeunes Cœurs laissez-vous prendre }
\livretVerse#7 { Le peril est grand d’attendre, }
\livretVerse#7 { Vous perdez d’heureux moments }
\livretVerse#7 { En cherchant à vous défendre ; }
\livretVerse#7 { Si l’Amour a des tourments }
\livretVerse#7 { C’est la faute des Amants. }
\null
\livretRef #'BGGchoeur
\livretVerse#7 { Plus les ames sont rebelles, }
\livretVerse#6 { plus leurs peines sont cruelle, }
\livretVerse#7 { Les plaisirs doux et charmants, }
\livretVerse#8 { Sont le prix des cœurs fidelles  ; }
\livretVerse#7 { Si l’amour a des tourments }
\livretVerse#7 { C’est la faute des amants. }
\livretPersDidas Licomede à Alceste
\livretRef #'BGHrecit
\livretVerse#4 { On vous apreste }
\livretVerse#4 { Dans mon Vaisseau }
\livretVerse#8 { Un divertissement nouveau. }
\livretPers Licomede & Straton
\livretVerse#8 { Venez voir ce que nostre Feste }
\livretVerse#6 { Doit avoir de plus beau. }
\null
\livretDidasP\justify {
  Licomede conduit Alceste dans son Vaisseau, Straton y meine Céphise,
  & dans le temps qu’Admete & Alcide y veulent passer, le Pont
  s’enfonce dans la Mer.
}
\livretPers Admete & Alcide
\livretVerse#8 { Dieux ! le Pont s’abisme dans l’eau. }
\livretPers Licomede & Straton
\livretVerse#8 { Venez voir ce que nostre feste }
\livretVerse#6 { doit avoir de plus beau. }
\livretPers Le Chœur des Thessaliens
\livretVerse#8 { Ah quelle trahison funeste. }
\livretPers Alceste & Céphise
\livretVerse#6 { Au secours, au secours. }
\livretPers Alcide
\livretVerse#6 { Perfide… }
\livretPers Admete
\livretVerse#6 { \transparent { Perfide… } Alceste… }
\livretPers Alcide & Admete
\livretVerse#6 { Laissons les vains discours. }
\livretVerse#6 { Au secours, au secours. }
\livretDidasP\justify {
  Les Thessaliens courent s’embarquer pour suivre Licomede.
}
\livretPers Le Chœur des Thessaliens
\livretVerse#6 { Au secours, au secours. }

\livretScene SCENE VIII
\livretDescAtt\wordwrap-center {
  THETIS, ADMETE
}
\livretPersDidas Thetis sortant de la Mer
\livretRef #'BHArecit
\livretVerse#12 { Espoux infortuné redoute ma colere, }
\livretVerse#12 { Tu vas haster l’instant qui doit finir tes jours ; }
\livretVerse#8 { C’est Thétis que la Mer revere, }
\livretVerse#12 { Que tu vois contre toy du party de son Frere ; }
\livretVerse#8 { Et c’est à la mort que tu cours. }
\livretPersDidas Admete courant s’embarquer
\livretVerse#6 { Au secours, au secours. }
\livretPers Thetis
\livretVerse#8 { Puis qu’on mesprise ma puissance }
\livretVerse#6 { Que les vents deschainez }
\livretVerse#6 { Que les flots mutinez }
\livretVerse#6 { S’arment pour ma vengeance. }
\null
\livretDidasP\justify {
  Thetis rentre dans la Mer, & les Aquilons excitent une tempeste qui
  agite les Vaisseaux qui s'efforcent de poursuivre Licomede.
}

\livretScene SCENE IX
\livretDescAtt\wordwrap-center {
  ÉOLE, LES AQUILONS, LES ZEPHIRS
}
\livretPers Éole
\livretRef #'BIAeole
\livretVerse#8 { Le Ciel protege les Heros : }
\livretVerse#8 { Allez Admete, allez Alcide ; }
\livretVerse#8 { Le Dieu qui sur les Dieux preside }
\livretVerse#8 { M’ordonne de calmer les flots : }
\livretVerse#8 { Allez, poursuivez un perfide. }
\livretVerse#4 { Retirez-vous }
\livretVerse#4 { Vents en courroux, }
\livretVerse#8 { Rentrez dans vos prisons profondes : }
\livretVerse#8 { Et laissez regner sur les ondes }
\livretVerse#6 { Les Zephirs les plus doux. }
\null
\livretDidasP\justify {
  L’orage cesse, les Zephirs volent & font fuïr les Aquilons qui
  tombent dans la Mer avec les nuages qu’ils en avoient élevez, & les
  Vaisseaux d’Alcide & d’Admete poursuivent Licomede.
}
\livretFinAct Fin du premier Acte
\sep

\livretAct ACTE SECOND
\livretDescAtt\justify {
  La Scene est dans l’Isle de Scyros, & le Theatre
  represente la Ville principale de l’Isle.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  CÉPHISE, STRATON.
}
\livretPers Céphise
\livretRef #'CAArecit
\livretVerse#12 { Alceste ne vient point, & nous devons attendre. }
\livretPers Straton
\livretVerse#6 { Que peut-elle pretendre ? }
\livretVerse#12 { Pourquoy se tourmenter icy mal à propos ? }
\livretVerse#8 { Ses cris ont beau se faire entendre, }
\livretVerse#12 { Peut-estre son Espoux a peri dans les flots, }
\livretVerse#12 { Et nous sommes enfin dans l’Isle de Scyros. }
\livretPers Céphise
\livretVerse#12 { Tu ne te plaindras point que j’en use de mesme. }
\livretVerse#8 { Je t’ay donné peu d’embarras. }
\livretVerse#8 { Tu vois comme je suis tes pas. }
\livretPers Straton
\livretVerse#12 { Tu sçais dissimuler une colere extresme. }
\livretPers Céphise
\livretVerse#12 { Et si je te disois que c’est toy seul que j’ayme ? }
\livretPers Straton
\livretVerse#12 { Tu le dirois en vain je ne te croirois pas. }
\livretPers Céphise
\livretVerse#8 { Croy moy : si j’ay faint de changer }
\livretVerse#8 { C’estoit pour te mieux engager. }
\livretVerse#8 { Un Rival n’est pas inutile, }
\livretVerse#12 { Il réveille l’ardeur & les soins d’un Amant ; }
\livretVerse#7 { Une conqueste facile }
\livretVerse#7 { Donne peu d’empressement, }
\livretVerse#5 { Et l’Amour tranquile }
\livretVerse#5 { S’endort aisément. }
\livretPers Straton
\livretVerse#12 { Non, non, ne tente point une seconde ruse, }
\livretVerse#8 { Je voy plus clair que tu ne crois. }
\livretVerse#12 { On excuse d’abord un Amant qu’on abuse. }
\livretVerse#8 { Mais la sotise est sans excuse }
\livretVerse#8 { De se laisser tromper deux fois. }
\livretPers Céphise
\livretVerse#12 { N’est-il aucun moyen d’apaiser ta colere ? }
\livretPers Straton
\livretVerse#12 { Consens à m’espouzer & sans retardement. }
\livretPers Céphise
\livretVerse#6 { Une si grande affaire }
\livretVerse#8 { Ne se fait pas si promptement }
\livretVerse#6 { Un Himen qu’on differe }
\livretVerse#6 { N’en est que plus charmant. }
\livretPers Straton
\livretVerse#6 { Un Himen qui peut plaire }
\livretVerse#4 { Ne couste guère, }
\livretVerse#8 { Et c’est un nœud bien tost formé ; }
\livretVerse#8 { Rien n’est plus aisé que de faire }
\livretVerse#8 { Un Espoux d’un Amant aimé. }
\livretPers Céphise
\livretVerse#8 { Je t’aime d’une amour sincere ; }
\livretVerse#6 { Et s’il est necessaire, }
\livretVerse#8 { Je m’offre à t’en faire un serment. }
\livretPers Straton
\livretVerse#8 { Amusement, amusement. }
\livretPers Céphise
\livretVerse#8 { L’injuste enlevement d’Alceste }
\livretVerse#12 { Attire dans ces lieux une guerre funeste, }
\livretVerse#12 { Les plus braves des Grecs s’arment pour son secours : }
\livretVerse#8 { Au milieu des cris & des larmes, }
\livretVerse#6 { L’Himen a peu de charmes ; }
\livretVerse#8 { Attendons de tranquiles jours. }
\livretVerse#6 { Le bruit affreux des armes }
\livretVerse#8 { Effarouche bien les Amours. }
\livretPers Straton
\livretVerse#6 { Discours, discours, discours. }
\livretVerse#12 { Tu n’as qu’à m’espouzer pour m’oster tout ombrage, }
\livretVerse#8 { Pourquoy differer davantage ? }
\livretVerse#8 { A quoy servent tant de façons ? }
\livretPers Céphise
\livretVerse#12 { Rends moy la liberté pour m’espouzer sans crainte ; }
\livretVerse#8 { Un Himen fait avec contrainte }
\livretVerse#12 { Est un mauvais moyen de finir tes soupçons. }
\livretPers Straton
\livretVerse#6 { Chansons, chansons, chansons. }

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  LICOMEDE, ALCESTE, STRATON, CÉPHISE, Soldats de Licomede.
}
\livretPers Licomede
\livretRef #'CBArecit
\livretVerse#8 { Allons, allons, la plainte est vaine. }
\livretPers Alceste
\livretVerse#8 { Ah quelle rigueur inhumaine ! }
\livretPers Licomede
\livretVerse#8 { Allons, je suis sourd à vos cris, }
\livretVerse#8 { Je me vange de vos mespris. }
\livretPers Alceste
\livretVerse#7 { Vous serez inexorable ? }
\livretPers Licomede
\livretVerse#8 { Cruelle, vous m’avez apris }
\livretVerse#8 { A devenir impitoyable. }
\livretPers Alceste
\livretVerse#12 { Est-ce ainsi que l’Amour a sçeu vous émouvoir ? }
\livretVerse#12 { Est-ce ainsi que pour moy vostre ame est attendrie ? }
\livretPers Licomede
\livretVerse#7 { L’Amour se change en Furie }
\livretVerse#7 { Quand il est au désespoir. }
\livretVerse#8 { Puis que je perds toute esperance, }
\livretVerse#12 { Je veux desesperer mon Rival à son tour ; }
\livretVerse#8 { Et les douceurs de la Vengeance }
\livretVerse#12 { Ont dequoy consoler les rigueurs de l’Amour. }
\livretPers Alceste
\livretVerse#8 { Voyez la douleur qui m’accable. }
\livretPers Licomede
\livretVerse#12 { Vous avez sans pitié regardé ma douleur. }
\livretVerse#8 { Vous m’avez rendu miserable }
\livretVerse#8 { Vous partagerez mon mal-heur. }
\livretPers Alceste
\livretVerse#12 { Admete avoit mon cœur dés ma plus tendre enfance ; }
\livretVerse#12 { Nous ne connoissions pas l’Amour ny sa puissance. }
\livretVerse#12 { Lors que d’un nœud fatal il vint nous enchaisner : }
\livretVerse#8 { Ce n’est pas une grande offence }
\livretVerse#12 { Que le refus d’un cœur qui n’est plus à donner. }
\livretPers Licomede
\livretVerse#8 { Est-ce aux Amants qu’on desespere }
\livretVerse#8 { A devoir rien examiner }
\livretVerse#8 { Non, je ne puis vous pardonner }
\livretVerse#6 { D’avoir trop sçeu me plaire. }
\livretVerse#12 { Que ne m’ont point cousté vos funestes attraits ! }
\livretVerse#12 { Ils ont mis dans mon cœur une cruelle flame, }
\livretVerse#8 { Ils ont arraché de mon ame }
\livretVerse#6 { L’innocence, & la paix. }
\livretVerse#8 { Non, Ingrate, non, Inhumaine, }
\livretVerse#8 { Non, quelle que soit vostre peine, }
\livretVerse#8 { Non, je ne vous rendray jamais }
\livretVerse#8 { Tous les maux que vous m’avez faits. }
\livretPers Straton
\livretVerse#8 { Voicy l’Ennemy qui s’avance }
\livretVerse#4 { En diligence. }
\livretPers Licomede
\livretVerse#4 { Preparons-nous }
\livretVerse#4 { A nous defendre. }
\livretPers Alceste
\livretVerse#8 { Ah Cruel, que n’espargnez-vous }
\livretVerse#6 { Le sang qu’on va respandre ! }
\livretPersDidas \smallCaps Licomede à ses soldats
\livretVerse#4 { Perissons tous }
\livretVerse#6 { Plûtost que de nous rendre. }
\livretDidasP\justify {
  Licomede contraint Alceste d’entrer dans la Ville,
  Céphise la suit, & les Soldats de Licomede ferment
  la Porte de la Ville aussi tost qu’ils y sont entrez.
}

\livretScene SCENE III
\livretDescAtt\wordwrap-center {
  ADMETE, ALCIDE, LYCHAS, Soldats assiegans.
}
\livretPers Ademete & Alcide
\livretRef #'CCBair
\livretVerse#6 { Marchez, marchez, marchez. }
\livretVerse#8 { Aprochez, Amis, aprochez, }
\livretVerse#6 { Marchez, marchez, marchez. }
\livretVerse#8 { Hastons-nous de punir des Traistres, }
\livretVerse#4 { Rendons-nous Maistres }
\livretVerse#8 { Des Murs qui les tiennent cachez : }
\livretVerse#6 { Marchez, marchez, marchez. }

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  LICOMEDE, STRATON, Soldats assiegez,
  ADMETE, ALCIDE, LYCHAS, Soldates assiegans.
}
\livretPersDidas Licomede sur les Remparts
\livretRef #'CDAchoeur
\livretVerse#8 { Ne pretendez pas nous surprendre, }
\livretVerse#8 { Venez, nous allons vous attendre : }
\livretVerse#8 { Nous ferons tous nostre devoir }
\livretVerse#6 { Pour vous bien recevoir. }
\livretPers\line { \smallCaps Straton & les Soldats assiegez }
\livretVerse#8 { Nous ferons tous nostre devoir }
\livretVerse#6 { Pour vous bien recevoir. }
\livretPers Admete
\livretVerse#8 { Perfide, évite un sort funeste, }
\livretVerse#12 { On te pardonne tout si tu veux rendre Alceste. }
\livretPers Licomede
\livretVerse#8 { J’aime mieux mourir, s’il le faut, }
\livretVerse#12 { Que de ceder jamais cét Objet plein de charmes. }
\livretPers Ademete & Alcide
\livretVerse#6 { A l’assaut, à l’assaut. }
\livretPers Licomede & Straton
\livretVerse#5 { Aux armes, aux armes. }
\livretPers Les Assiegeans
\livretVerse#6 { A l’assaut, à l’assaut. }
\livretPers Les Assiegez
\livretVerse#5 { Aux armes, aux armes. }
\livretPers Ademete, Alcide & Licomede
\livretVerse#7 { A moy, Compagnons, à moy. }
\livretPers Ademete & Licomede
\livretVerse#7 { A moy, suivez vostre Roy. }
\livretPers Alcide
\livretVerse#3 { C’est Alcide }
\livretVerse#3 { Qui vous guide. }
\livretPers Ademete, Alcide & Licomede
\livretVerse#7 { A moy, Compagnons, à moy. }
\livretDidasP\justify {
  On fait avancer des Beliers & autres Machines de guerre pour battre
  la Place.
}
\livretPers Tous ensemble
\livretVerse#8 { Donnons, donnons de toutes parts. }
\livretPers Les Assiegeans
\livretVerse#8 { Que chacun à l’envy combatte. }
\livretVerse#4 { Que l’on abatte }
\livretVerse#6 { Les Tours, & les Remparts. }
\livretPers Tous ensemble
\livretVerse#8 { Donnons, donnons de toutes parts. }
\livretPers Les Assiegez
\livretVerse#8 { Que les Ennemis, pesle mesle, }
\livretVerse#8 { Trébuchent sous l’affreuse gresle }
\livretVerse#8 { De nos fléches, & de nos dards. }
\livretPers Tous
\livretVerse#8 { Donnons, donnons de toutes parts. }
\livretVerse#8 { Courage, courage, courage, }
\livretVerse#8 { Ils sont à nous, ils sont à nous. }
\livretPers Alcide
\livretVerse#8 { C’est trop disputer l’avantage, }
\livretVerse#8 { Je vais vous ouvrir un passage, }
\livretVerse#8 { Suivez-moy tous, suivez-moy tous. }
\livretPers Tous ensemble
\livretVerse#8 { Courage, courage, courage, }
\livretVerse#8 { Ils sont à nous, ils sont à nous. }
\livretRef #'CDBentree
\livretDidasPPage\justify {
  Les Assiegez voyant leurs Remparts à demy abattus, & la Porte de la
  Ville enfoncée, font un fernier effort dans une sortie pour
  repousser les Assiegeans.
}
\livretPers Les Assiegeans
\livretRef #'CDCchoeur
\livretVerse#8 { Achevons d’emporter la Place ; }
\livretVerse#8 { L’Ennemy commence à plier. }
\livretVerse#8 { Main basse, main basse, main basse. }
\livretPersDidas Les Assiegez rendans les Armes
\livretVerse#6 { Quartier, quartier, quartier. }
\livretPers Les Assiegeans
\livretVerse#4 { La Ville est prise. }
\livretPers Les Assiegez
\livretVerse#6 { Quartier, quartier, quartier. }
\livretPersDidas Lychas terrassant Straton
\livretVerse#6 { Il faut rendre Céphise. }
\livretPers Straton
\livretVerse#6 { Je suis ton prisonnier, }
\livretVerse#6 { Quartier, quartier, quartier. }

\livretScene SCENE V
\livretPersDidas Pheres \line { armé, & marchant avec peine }
\livretRef #'CEApheres
\livretVerse#8 { Courage Enfants, je suis à vous ; }
\livretVerse#8 { Mon bras va seconder vos coups : }
\livretVerse#12 { Mais c’en est déja fait, & l’on a pris la Ville ; }
\livretVerse#12 { La foiblesse de l’âge a retardé mes pas : }
\livretVerse#8 { La valeur devient inutile }
\livretVerse#8 { Quand la force n’y respond pas. }
\livretVerse#6 { Que la vieillesse est lente, }
\livretVerse#6 { Les efforts qu’elle tente }
\livretVerse#6 { Sont toûjours impuissans : }
\livretVerse#8 { C’est une charge bien pesante }
\livretVerse#8 { Qu’un fardeau de quatre-vingts ans. }

\livretScene SCENE VI
\livretDescAtt\wordwrap-center {
  ALCIDE, ALCESTE, CÉPHISE, PHERES, LYCHAS, STRATON enchaisné.
}
\livretPersDidas Alcide à Pheres
\livretRef #'CFBrecit
\livretVerse#12 { Rendez à vostre Fils cette aimable Princesse. }
\livretPers Pheres
\livretVerse#12 { Ce don de vostre main seroit encor plus doux. }
\livretPers Alcide
\livretVerse#12 { Allez, allez la rendre à son heureux Espoux. }
\livretPers Alceste
\livretVerse#8 { Tout est soûmis, la guerre cesse ; }
\livretVerse#8 { Seigneur, pourquoy me laissez-vous ? }
\livretVerse#6 { Quel nouveau soin vous presse ? }
\livretPers Alcide
\livretVerse#8 { Vous n’avez rien à redouter, }
\livretVerse#12 { Je vais chercher ailleurs des Tyrans à dompter. }
\livretPers Alceste
\livretVerse#8 { Les nœuds d’une amitié pressante }
\livretVerse#12 { Ne retiendront-ils point vostre Ame impatiente ? }
\livretVerse#12 { Et la Gloire toûjours vous doit-elle emporter ? }
\livretPers Alcide
\livretVerse#8 { Gardez-vous bien de m’arrester. }
\livretPers Alceste
\livretVerse#8 { C’est vostre Valeur triomphante }
\livretVerse#12 { Qui fait le sort charmant que nous allons goûter ; }
\livretVerse#8 { Quelque douceur que l’on ressente, }
\livretVerse#8 { Un Amy tel que vous l’augmente, }
\livretVerse#8 { Voulez-vous si-tost nous quitter ? }
\livretPers Alcide
\livretVerse#8 { Gardez-vous bien de m’arrester. }
\livretVerse#12 { Laissez, laissez moy fuïr un charme qui m’enchante : }
\livretVerse#12 { Non, toute ma vertu n’est pas assez puissante }
\livretVerse#8 { Pour répondre d’y resister. }
\livretVerse#12 { Non, encore une fois, Princesse trop charmante, }
\livretVerse#8 { Gardez-vous bien de m’arrester. }

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  ALCESTE, PHERES, CÉPHISE.
}
\livretPers A trois
\livretRef #'CGAtrio
\livretVerse#8 { Cherchons Admete promptement. }
\livretPers Alceste
\livretVerse#7 { Peut-on chercher ce qu’on aime }
\livretVerse#7 { Avec trop d’empressement ! }
\livretVerse#6 { Quand l’amour est extréme, }
\livretVerse#6 { Le moindre esloignement }
\livretVerse#6 { Est un cruel tourment. }
\livretPers Alceste, Pheres, & Céphise
\livretVerse#8 { Cherchons Admete promptement. }

\livretScene SCENE VIII
\livretDescAtt\wordwrap-center {
  ADMETE blessé, CLEANTE, ALCESTE, PHERES, CÉPHISE, Soldats.
}
\livretPers Alceste
\livretRef #'CHArecit
\livretVerse#8 { O Dieux ! quel spectacle funeste ? }
\livretPers Cleante
\livretVerse#12 { Le Chef des Ennemis mourant, & terrassé, }
\livretVerse#12 { De sa rage expirante a ramassé le reste, }
\livretVerse#8 { Le Roy vient d’en estre blessé. }
\livretPers Admete
\livretVerse#6 { Je meurs, charmante Alceste, }
\livretVerse#6 { Mon sort est assez doux }
\livretVerse#6 { Puis que je meurs pour vous. }
\livretPers Alceste
\livretVerse#12 { C’est pour vous voir mourir que le Ciel me délivre ! }
\livretPers Admete
\livretVerse#8 { Avec le nom de vostre Espoux }
\livretVerse#8 { J’eusse esté trop heureux de vivre ; }
\livretVerse#6 { Mon sort est assez doux }
\livretVerse#6 { Puis que je meurs pour vous. }
\livretPers Alceste
\livretVerse#12 { Est-ce là cét Hymen si doux, si plein d’appas, }
\livretVerse#8 { Qui nous promettoit tant de charmes ? }
\livretVerse#12 { Faloit-il que si-tost l’aveugle sort des armes }
\livretVerse#12 { Tranchast des nœuds si beaux par un affreux trépas ? }
\livretVerse#12 { Est-ce là cét Hymen si doux, si plein d’appas, }
\livretVerse#8 { Qui nous promettoit tant de charmes ? }
\livretPers Admete
\livretVerse#8 { Belle Alceste ne pleurez pas, }
\livretVerse#8 { Tout mon sang ne vaut point vos larmes. }
\livretPers Alceste
\livretVerse#12 { Est-ce là cét Hymen si doux, si plein d’appas, }
\livretVerse#8 { Qui nous promettoit tant de charmes ? }
\livretPers Admete
\livretVerse#6 { Alceste, vous pleurez. }
\livretPers Alceste
\livretVerse#6 { Admete, vous mourez. }
\livretPers Admete & Alceste ensemble
\livretVerse#6 { Alceste, vous pleurez ; }
\livretVerse#6 { Admete, vous mourez. }
\livretPers Alceste
\livretVerse#8 { Se peut-il que le Ciel permette, }
\livretVerse#8 { Que les cœurs d’Alceste & d’Admete }
\livretVerse#6 { Soient ainsi separez ? }
\livretPers Admete & Alceste
\livretVerse#6 { Alceste, vous pleurez ; }
\livretVerse#6 { Admete, vous mourez. }

\livretScene SCENE IX
\livretDescAtt\wordwrap-center {
  APOLLON, LES ARTS, ADMETE, ALCESTE, PHERES, CÉPHISE, CELANTE,
  Soldats.
}
\livretPersDidas Apollon environné des Arts
\livretRef #'CIBrecit
\livretVerse#12 { La Lumiere aujourd’hui te doit estre ravie ; }
\livretVerse#12 { Il n’est qu’un seul moyen de prolonger ton sort ; }
\livretVerse#12 { Le Destin me promet de te rendre à la vie, }
\livretVerse#12 { Si quelqu’Autre pour toy veut s’offrir à la mort. }
\livretVerse#12 { Reconnoist si quelqu’un t’aime parfaitement ; }
\livretVerse#12 { Sa mort aura pour prix une immortelle gloire : }
\livretVerse#8 { Pour en conserver la memoire }
\livretVerse#12 { Les Arts vont élever un pompeux Monument. }
\livretDidasP\justify {
  Les Arts qui sont autour d’Apollon se separent sur des Nuages
  differents, & tous descendent pour élever un Monument superbe,
  tandis qu’Apollon s’envole.
}
\livretFinAct Fin du second Acte
\sep

\livretAct ACTE TROISIÉME
\livretDescAtt\justify {
  Le Theatre est un grand Monument élevé par les Arts.
  Un Autel vide paroist au milieu pour servir à porter
  l’Image de la personne qui s’immolera pour Admete.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  ALCESTE, PHERES, CÉPHISE.
}
\livretPers Alceste
\livretRef #'DAArecit
\livretVerse#8 { Ah pourquoy nous séparez-vous ? }
\livretVerse#12 { Eh du moins attendez que la Mort nous separe ; }
\livretVerse#8 { Cruels, quelle pitié barbare }
\livretVerse#12 { Vous presse d’arracher Alceste à son Espoux ? }
\livretVerse#8 { Ah pourquoy nous séparez-vous ? }
\livretPers Pheres, & Céphise
\livretVerse#12 { Plus vostre Espoux mourant voit d’amour, & d’appas, }
\livretVerse#12 { Et plus le jour qu’il perd luy doit faire d’envie : }
\livretVerse#8 { Ce sont les douceurs de la vie }
\livretVerse#8 { Qui font les horreurs du trépas. }
\livretPers Alceste
\livretVerse#12 { Les Arts n’ont point encore achevé leur ouvrage ; }
\livretVerse#12 { Cét Autel doit porter la glorieuse Image }
\livretVerse#8 { De qui signalera sa foy }
\livretVerse#8 { En mourant pour sauver son Roy. }
\livretVerse#8 { Le prix d’une gloire immortelle }
\livretVerse#8 { Ne peut-il toucher un grand Cœur ? }
\livretVerse#8 { Faut-il que la Mort la plus belle }
\livretVerse#8 { Ne laisse pas de faire peur ? }
\livretVerse#8 { A quoy sert la foule importune }
\livretVerse#8 { Dont les Roys sont embarrassez ? }
\livretVerse#8 { Un coup fatal de la Fortune }
\livretVerse#8 { Escarte les plus empressez. }
\livretPers Alceste, Pherès, & Céphise
\livretVerse#8 { De tant d’Amis qu’avoit Admete }
\livretVerse#8 { Aucun ne vient le secourir ; }
\livretVerse#6 { Quelque honneur qu’on promette }
\livretVerse#6 { On le laisse mourir. }
\livretPers Pheres
\livretVerse#8 { J’aime mon Fils, je l’ay fait Roy ; }
\livretVerse#12 { Pour prolonger son sort je mourrois sans effroy, }
\livretVerse#12 { Si je pouvois offrir des jours dignes d’envie ; }
\livretVerse#8 { Je n’ay plus qu’un reste de vie }
\livretVerse#12 { Ce n’est rien pour Admete, & c’est beaucoup pour moy. }
\livretPers Céphise
\livretVerse#8 { Les Honneurs les plus éclatans }
\livretVerse#12 { En vain dans le Tombeau promettent de nous suivre ; }
\livretVerse#8 { La Mort est affreuse en tout temps : }
\livretVerse#8 { Mais peut-on renoncer à vivre }
\livretVerse#8 { Quand on n’a vescu que quinze ans ? }
\livretPers Alceste
\livretVerse#12 { Chacun est satisfait des excuses qu’il donne : }
\livretVerse#8 { Cependant on ne voit personne }
\livretVerse#12 { Qui pour sauver Admete ose perdre le jour ; }
\livretVerse#12 { Le Devoir, l’Amitié, le Sang, tout l’abandonne, }
\livretVerse#8 { Il n’a plus d’espoir qu’en l’Amour. }

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  PHERES, LE CHŒUR, CLEANTE.
}
\livretPers Pheres
\livretRef #'DBArecit
\livretVerse#12 { Voyons encor mon Fils, allons, hastons nos pas ; }
\livretVerse#12 { Ses yeux vont se couvrir d’éternelles tenebres. }
\livretPers Le Chœur
\livretVerse#6 { Helas ! helas ! helas ! }
\livretPers Pheres
\livretVerse#8 { Quels cris ! quelles plaintes funebres ! }
\livretPers Le Chœur
\livretVerse#6 { Helas ! helas ! helas ! }
\livretPers Pheres
\livretVerse#7 { Où vas-tu ? Cleante, demeure. }
\livretPers Cleante
\livretVerse#4 { Helas ! helas ! }
\livretVerse#9 { Le Roy touche à sa dernière heure, }
\livretVerse#8 { Il s’affoiblit, il faut qu’il meure, }
\livretVerse#8 { Et je viens pleurer son trespas. }
\livretVerse#4 { Helas ! helas ! }
\livretPers Le Chœur
\livretVerse#6 { Helas ! helas ! helas ! }
\livretPers Pheres
\livretVerse#8 { On le plaint, tout le monde pleure, }
\livretVerse#8 { Mais nos pleurs ne le sauvent pas. }
\livretVerse#4 { Helas ! helas ! }
\livretPers Le Chœur
\livretVerse#6 { Helas ! helas ! helas ! }

\livretScene SCENE III
\livretDescAtt\wordwrap-center {
  LE CHŒUR, ADMETE, PHERES, CLEANTE.
}
\livretPers Le Chœur
\livretRef #'DCArecit
\livretVerse#6 { O trop heureux Admete ! }
\livretVerse#6 { Que vostre sort est beau ! }
\livretPers Pheres & Cleante
\livretVerse#8 { Quel changement ! quel bruit nouveau ! }
\livretPers Le Chœur
\livretVerse#6 { O trop heureux Admete ! }
\livretVerse#6 { Que vostre sort est beau ! }
\livretPersDidas\smallCaps Pheres & Cleante voyant Admete guery.
\livretVerse#8 { L’effort d’une Amitié parfaite }
\livretVerse#6 { L’a sauvé du Tombeau }
\livretPersDidas\smallCaps Pheres embrassant Admete.
\livretVerse#6 { O trop heureux Admete ! }
\livretVerse#6 { Que vostre sort est beau ! }
\livretPers Le Chœur
\livretVerse#6 { O trop heureux Admete ! }
\livretVerse#6 { Que vostre sort est beau ! }
\livretPers Admete
\livretVerse#6 { Qu’une Pompe funebre }
\livretVerse#6 { Rende à jamais celebre }
\livretVerse#6 { Le genereux effort }
\livretVerse#6 { Qui m’arrache à la Mort. }
\livretVerse#8 { Alceste n’aura plus d’allarmes, }
\livretVerse#8 { Je reverray ses yeux charmants }
\livretVerse#8 { A qui j’ay cousté tant de larmes : }
\livretVerse#6 { Que la vie a de charmes }
\livretVerse#6 { Pour les heureux Amants ! }
\livretVerse#12 { Achevez, Dieux des Arts, faites nous voir l’Image }
\livretVerse#12 { Qui doit eterniser la grandeur de courage }
\livretVerse#8 { De qui s’est immolé pour moy ; }
\livretVerse#8 { Ne differez point davantage… }
\livretVerse#8 { Ciel ! ô Ciel ! qu’est-ce que je voy ! }
\livretDidasP\justify {
  L’Autel s’ouvre, & l’on voit sortir l’Image d’Alceste qui se perce le
  sein.
}

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  CÉPHISE, ADMETE, PHERES, CLEANTE, LE CHŒUR.
}
\livretPers Céphise
\livretRef #'DDArecit
\livretVerse#9 { Alceste est morte. }
\livretPers Admete
\livretVerse#9 { \transparent { Alceste est morte. } Alceste est morte ! }
\livretPers Le Chœur
\livretVerse#4 { Alceste est morte. }
\livretPers Céphise
\livretVerse#12 { Alceste a satisfait les Parques en courroux ; }
\livretVerse#12 { Vostre Tombeau s’ouvroit, elle y descend pour vous, }
\livretVerse#12 { Elle-mesme a voulu vous en fermer la porte ; }
\livretVerse#9 { Alceste est morte. }
\livretPers Admete
\livretVerse#9 { \transparent { Alceste est morte. } Alceste est morte ! }
\livretPers Le Chœur
\livretVerse#4 { Alceste est morte. }
\livretPers Céphise
\livretVerse#12 { J’ay couru, mais trop tard pour arrester ses coups : }
\livretVerse#8 { Jamais en faveur d’un Espoux }
\livretVerse#12 { On ne verra d’ardeur si fidelle & si forte ; }
\livretVerse#9 { Alceste est morte. }
\livretPers Admete
\livretVerse#9 { \transparent { Alceste est morte. } Alceste est morte ! }
\livretPers Le Chœur
\livretVerse#4 { Alceste est morte. }
\livretPers Céphise
\livretVerse#12 { Sujets, Amis, Parents, vous abandonnoient tous ; }
\livretVerse#12 { Sur les Droits les plus forts, sur les Nœuds les plus doux, }
\livretVerse#8 { L’Amour, le tendre Amour l’emporte : }
\livretVerse#9 { Alceste est morte. }
\livretPers Admete
\livretVerse#9 { \transparent { Alceste est morte. } Alceste est morte ! }
\livretPers Le Chœur
\livretVerse#4 { Alceste est morte. }
\livretDidasP\justify {
  Admete tombe accablé de douleur entre les bras de sa suite.
}

\livretScene SCENE V
\livretDescAtt\justify {
  Troupe de femmes affligées, Troupe d’Hommes desolez, qui portent des
  fleurs, & tous les ornements qui ont servy à parer Alceste.
}
% \livretPers Tous ensemble
% %# Formons les plus lugubres chants,
% %# Et les regrets les plus touchants.
\livretPers Une femme affligée
\livretRef #'DEBair
\livretVerse#6 { La Mort, la Mort barbare, }
\livretVerse#8 { Détruit aujourd’huy mille appas. }
\livretVerse#6 { Quelle Victime, helas ! }
\livretVerse#8 { Fut jamais si belle, & si rare ? }
\livretVerse#6 { La Mort, la Mort barbare }
\livretVerse#8 { Détruit aujourd’huy mille appas. }

% \livretPers Un homme désolé
% %# Alceste si jeune, & si belle,
% %# Court se precipiter dans la Nuit eternelle,
% %# Pour sauver ce qu'elle aime elle a perdu le jour.
% \livretPers Le Chœur
% %# O trop parfait Modelle
% %# D'une Espouse fidelle!
% %# O trop parfait Modelle
% %# D'un veritable Amour!
% \livretPers Une femme affligée

\livretVerse#8 { Que nostre zéle se partage ; }
\livretVerse#12 { Que les uns par leurs chants celebrent son courage, }
\livretVerse#12 { Que d’autres par leurs cris déplorent ses mal-heurs. }
\livretPers Le Chœur
\livretVerse#4 { Rendons hommage }
\livretVerse#4 { A son Image ; }
\livretVerse#4 { Jettons des fleurs, }
\livretVerse#4 { Versons des pleurs. }
\livretPers Une femme affligée
\livretVerse#8 { Alceste, la Charmante Alceste, }
\livretVerse#8 { La fidelle Alceste n’est plus. }
\livretPers Le Chœur
\livretVerse#8 { Alceste, la Charmante Alceste, }
\livretVerse#8 { La fidelle Alceste n’est plus. }
\livretPers Une femme affligée
\livretVerse#8 { Tant de beautez, tant de vertus, }
\livretVerse#8 { Meritoient un sort moins funeste. }
\livretPers Le Chœur
\livretVerse#8 { Alceste, la Charmante Alceste, }
\livretVerse#8 { La fidelle Alceste n’est plus. }
\livretDidasP\justify {
  Un transport de douleur saisit les deux Troupes affligées,
  une partie déchire ses habits, l’autre s’arrache les cheveux,
  & chacun brise au pied de l’Image d’Alceste les ornements qu’il
  porte à la main.
}
\livretPers Le Chœur
\livretVerse#8 { Rompons, brisons le triste reste }
\livretVerse#8 { De ces Ornemens superflus. }
\livretRef #'DECchoeur
\livretVerse#12 { Que nos pleurs, que nos cris renouvellent sans cesse }
\livretVerse#12 { Allons porter par tout la douleur qui nous presse. }

\livretScene SCENE VI
\livretDescAtt\wordwrap-center {
  ADMETE, PHERES, CÉPHISE, CLEANTE, suite.
}
\livretPersDidas Admete \line { revenu de son évanoüissement, & se voyant désarmé. }
\livretRef #'DFArecit
\livretVerse#8 { Sans Alceste, sans ses appas, }
\livretVerse#8 { Croyez-vous que je puisse vivre ! }
\livretVerse#8 { Laissez moy courir au Trespas }
\livretVerse#8 { Où ma chere Alceste se livre. }
\livretVerse#8 { Sans Alceste, sans ses appas, }
\livretVerse#8 { Croyez-vous que je puisse vivre ? }
\livretVerse#8 { C’est pour moy qu’elle meurt, helas ! }
\livretVerse#8 { Pourquoy m’empescher de la suivre ? }
\livretVerse#8 { Sans Alceste, sans ses appas, }
\livretVerse#8 { Croyez-vous que je puisse vivre. }

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  ALCIDE, ADMETE, PHERES, CÉPHISE, CLEANTE.
}
\livretPers Alcide
\livretRef #'DGArecit
\livretVerse#12 { Tu me vois arresté sur le point de partir }
\livretVerse#12 { Par les tristes clameurs qu’on entend retentir. }
\livretPers Admete
\livretVerse#12 { Alceste meurt pour moy par une amour extresme, }
\livretVerse#12 { Je ne reverray plus les yeux qui m’ont charmé : }
\livretVerse#8 { Helas ! j’ay perdu ce que j’aime }
\livretVerse#8 { Pour avoir esté trop aimé. }
\livretPers Alcide
\livretVerse#12 { J’aime Alceste, il est temps de ne m’en plus defendre ; }
\livretVerse#12 { Elle meurt, ton amour n’a plus rien à pretendre ; }
\livretVerse#12 { Admete, cede moy la Beauté que tu perds : }
\livretVerse#12 { Au Palais de Pluton j’entreprends de descendre : }
\livretVerse#8 { J’iray jusqu’au fonds des Enfers }
\livretVerse#8 { Forcer la Mort à me la rendre. }
\livretPers Admete
\livretVerse#8 { Je verrois encor ses beaux yeux ? }
\livretVerse#12 { Allez, Alcide, allez, revenez glorieux, }
\livretVerse#8 { Obtenez qu’Alceste vous suive : }
\livretVerse#8 { Le Fils du plus puissant des Dieux }
\livretVerse#12 { Est plus digne que moy du bien dont on me prive. }
\livretVerse#8 { Allez, allez, ne tardez pas, }
\livretVerse#8 { Arrachez Alceste au Trespas, }
\livretVerse#12 { Et ramenez au jour son Ombre fugitive ; }
\livretVerse#12 { Qu’elle vive pour vous avec tous ses appas, }
\livretVerse#12 { Admete est trop heureux pourveu qu’Alceste vive. }
\livretPers Pheres, Céphise, Cleante
\livretVerse#8 { Allez, allez, ne tardez pas, }
\livretVerse#8 { Arrachez Alceste au Trespas. }

\livretScene SCENE VIII
\livretDescAtt\wordwrap-center {
  DIANE, MERCURE, ALCIDE, ADMETE, PHERES, CÉPHISE, CLEANTE.
}
\livretDidasP\justify {
  La Lune paroist, son Globe s’ouvre, & fait voir Diane sur un
  Nuage brillant.
}
\livretPers Diane
\livretRef #'CHArecit
\livretVerse#8 { Le Dieu dont tu tiens la naissance }
\livretVerse#12 { Oblige tous les Dieux d’estre d’intelligence }
\livretVerse#8 { En faveur d’un dessein si beau ; }
\livretVerse#8 { Je viens t’offrir mon assistance ; }
\livretVerse#6 { Et Mercure s’avance }
\livretVerse#12 { Pour t’ouvrir aux Enfers un passage nouveau. }
\livretDidasP\justify {
  Mercure vient en volant frapper la Terre de son Caducée,
  l’Enfer s’ouvre, & Alcide y descend.
}
\livretFinAct Fin du troisiéme Acte
\sep

\livretAct ACTE QUATRIÉME
\livretDescAtt\justify {
  Le Theatre represente le Fleuve Acheron & ses sombres Rivages.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  CHARON, LES OMBRES.
}
\livretPersDidas\smallCaps Charon, ramant sa Barque
\livretRef#'EAArecit
\livretVerse#7 { Il faut passer tost ou tard, }
\livretVerse#7 { Il faut passer dans ma Barque. }
\livretVerse#7 { On y vient jeune, ou vieillard, }
\livretVerse#7 { Ainsi qu’il plaist à la Parque ; }
\livretVerse#7 { On y reçoit sans égard, }
\livretVerse#7 { Le Berger, & le Monarque. }
\livretVerse#7 { Il faut passer tost ou tard, }
\livretVerse#7 { Il faut passer dans ma Barque. }
\livretVerse#12 { Vous qui voulez passer, venez, Manes errants, }
\livretVerse#8 { Venez, avancez, tristes Ombres, }
\livretVerse#8 { Payez le tribut que je prens, }
\livretVerse#12 { Où retournez errer sur ces Rivages sombres. }
\livretPers Les Ombres
\livretVerse#8 { Passe-moy, Charon, passe-moy. }
\livretPers Charon
\livretVerse#12 { Il faut auparavant que l’on me satisfasse, }
\livretVerse#12 { On doit payer les soins d’un si penible employ. }
\livretPers Les Ombres
\livretVerse#8 { Passe-moy, Charon, passe-moy. }
\livretPers Charon
\livretVerse#7 { Donne, passe, donne, passe, }
\livretVerse#4 { Demeure toy. }
\livretVerse#8 { Tu n’as rien, il faut qu’on te chasse. }
\livretPersDidas\smallCaps Une Ombre rebuttée
\livretVerse#8 { Une Ombre tient si peu de place. }
\livretPers Charon
\livretVerse#7 { Où paye, où tourne ailleurs tes pas. }
\livretPers L'Ombre
\livretVerse#12 { De grace, par pitié, ne me rebutte pas. }
\livretPers Charon
\livretVerse#8 { La pitié n’est point icy bas, }
\livretVerse#8 { Et Charon ne fait point de grace. }
\livretPers L'Ombre
\livretVerse#8 { Helas ! Charon, helas ! helas ! }
\livretPers Charon
\livretVerse#8 { Crie helas ! tant que tu voudras, }
\livretVerse#12 { Rien pour rien, en tous lieux est une loy suivie : }
\livretVerse#8 { Les mains vides sont sans appas, }
\livretVerse#12 { Et ce n’est point assés de payer dans la vie. }
\livretVerse#12 { Il faut encore payer au delà du Trépas. }
\livretPersDidas\smallCaps L'Ombre en se retirant
\livretVerse#8 { Helas ! Charon, helas ! helas ! }
\livretPers Charon
\livretVerse#8 { Il m’importe peu que l’on crie }
\livretVerse#8 { Helas ! Charon, helas ! helas ! }
\livretVerse#12 { Il faut encore payer au delà du Trépas. }

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  ALCIDE, CHARON, LES OMBRES.
}
\livretPersDidas\smallCaps Alcide sautant dans la Barque
\livretRef #'EBArecit
\livretVerse#8 { Sortez, Ombres, faites moy place, }
\livretVerse#8 { Vous passerez une autre fois. }
\livretDidasP\line { Les Ombres s’enfuïent. }
\livretPers Charon
\livretVerse#12 { Ah ma Barque ne peut souffrir un si grand poids ! }
\livretPers Alcide
\livretVerse#8 { Allons, il faut que l’on me passe. }
\livretPers Charon
\livretVerse#12 { Retire-toy d’icy, Mortel, qui que tu sois, }
\livretVerse#12 { Les Enfers irritez puniront ton audace. }
\livretPers Alcide
\livretVerse#8 { Passe-moy, sans tant de façons. }
\livretPers Charon
\livretVerse#8 { L’eau nous gagne, ma Barque créve. }
\livretPers Alcide
\livretVerse#8 { Allons, rame, dépesche, achéve. }
\livretPers Charon
\livretVerse#4 { Nous enfonçons }
\livretPers Alcide
\livretVerse#4 { Passons, passons. }

\livretScene SCENE III
\livretDescAtt\center-column {
  \justify {
    Le Theatre change, & represente le Palais de Pluton.
  }
  \wordwrap-center {
    PLUTON, PROSERPINE, L’OMBRE D’ALCESTE, Suivans de Pluton.
  }
}
\livretPersDidas\smallCaps Pluton sur son Thrône
\livretRef #'ECArecit
\livretVerse#12 { Reçoy le juste prix de ton amour fidelle ; }
\livretVerse#12 { Que ton destin nouveau soit heureux à jamais : }
\livretVerse#12 { Commence de goûter la douceur eternelle }
\livretVerse#6 { D’une profonde paix. }
\livretPers Suivants de Pluton
\livretVerse#12 { Commence de goûter la douceur eternelle }
\livretVerse#6 { D’une profonde paix. }
\livretPersDidas\smallCaps Proserpine à costé de Pluton
\livretVerse#12 { L’espouze de Pluton te retient auprés d’elle : }
\livretVerse#8 { Tous tes vœux seront satisfaits. }
\livretPers Suivants de Pluton
\livretVerse#12 { Commence de goûter la douceur eternelle }
\livretVerse#6 { D’une profonde paix. }
\livretPers Pluton & Proserpine
\livretVerse#8 { En faveur d’une Ombre si belle, }
\livretVerse#12 { Que l’Enfer fasse voir tout ce qu’il a d’attraits. }
\livretPers Suivants de Pluton
\livretVerse#8 { En faveur d’une Ombre si belle, }
\livretVerse#12 { Que l’Enfer fasse voir tout ce qu’il a d’attraits. }
\livretRef#'ECBair
\livretDidasPPage\justify {
  Les Suivants de Pluton se réjoüissent de la venuë d’Alceste
  dans les Enfers, par une espece de Feste.
}
\livretPers Suivants de Pluton
\livretRef#'ECCchoeur
\livretVerse#8 { Tout mortel doit icy paroistre, }
\livretVerse#4 { On ne peut naistre }
\livretVerse#4 { Que pour mourir : }
\livretVerse#8 { De cent maux le Trespas délivre ; }
\livretVerse#4 { Qui cherche à vivre }
\livretVerse#4 { Cherche à souffrir. }
\livretVerse#8 { Venez tous sur nos sombres bords. }
\livretVerse#6 { Le repos qu’on desire }
\livretVerse#5 { Ne tient son Empire }
\livretVerse#7 { Que dans le sejour des Morts. }
\null
\livretRef#'ECEchoeur
\livretVerse#9 { Chacun vient icy bas prendre place, }
\livretVerse#5 { Sans cesse on y passe, }
\livretVerse#5 { Jamais on n’en sort. }
\livretVerse#9 { C’est pour tous une loy necessaire ; }
\livretVerse#5 { L’effort qu’on peut faire }
\livretVerse#5 { N’est qu’un vain effort : }
\livretVerse#3 { Est-on sage }
\livretVerse#5 { De fuïr ce passage ? }
\livretVerse#4 { C’est un orage }
\livretVerse#4 { Qui meine au Port. }
\livretVerse#9 { Chacun vient icy bas prendre place, }
\livretVerse#5 { Sans cesse on y passe, }
\livretVerse#5 { Jamais on n’en sort. }
\livretVerse#3 { Tous les charmes, }
\livretVerse#4 { Plaintes, cris, larmes, }
\livretVerse#4 { Tout est sans armes }
\livretVerse#4 { Contre la Mort. }
\livretVerse#9 { Chacun vient icy bas prendre place, }
\livretVerse#5 { Sans cesse on y passe, }
\livretVerse#5 { Jamais on n’en sort. }

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  ALECTON, PLUTON, PROSERPINE, L’OMBRE D’ALCESTE, Suivans de Pluton.
}
\livretPers Alecton
\livretRef#'EDArecit
\livretVerse#12 { Quittez, quittez les Jeux, songez à vous deffendre, }
\livretVerse#12 { Contre un audacieux unissons nos efforts : }
\livretVerse#12 { Le Fils de Jupiter vient icy de descendre }
\livretVerse#12 { Seul, il ose attaquer tout l’Empire des Morts. }
\livretPers Pluton
\livretVerse#8 { Qu’on arreste ce Temeraire, }
\livretVerse#8 { Armez vous, Amis, armez vous, }
\livretVerse#6 { Qu’on deschaine Cerbere, }
\livretVerse#6 { Courez tous, courez tous. }
\livretDidasP\justify { On entend aboyer Cerbere. }
\livretPers Alecton
\livretVerse#8 { Son bras abat tout ce qu’il frappe. }
\livretVerse#8 { Tout cede à ses horribles coups. }
\livretVerse#8 { Rien ne resiste, rien n’eschape. }

\livretScene SCENE V
\livretDescAtt\wordwrap-center {
  ALCIDE, PLUTON, PROSERPINE, ALECTON, Suivans de Pluton.
}
\livretPersDidas\smallCaps Pluton voyant Alcide qui enchaine Cerbere.
\livretRef#'EEArecit
\livretVerse#12 { Insolent jusqu’icy braves-tu mon courroux ? }
\livretVerse#8 { Quelle injuste audace t’engage, }
\livretVerse#8 { A troubler la paix de ces lieux ? }
\livretPers Alcide
\livretVerse#8 { Je suis né pour dompter la rage }
\livretVerse#8 { Des Monstres les plus furieux. }
\livretPers Pluton
\livretVerse#12 { Est-ce le Dieu jaloux qui lance le Tonnerre }
\livretVerse#8 { Qui t’oblige à porter la guerre }
\livretVerse#8 { Jusqu’au centre de l’Univers ? }
\livretVerse#12 { Il tient sous son pouvoir & le Ciel & la Terre, }
\livretVerse#12 { Veut-il encor ravir l’empire des Enfers ? }
\livretPers Alcide
\livretVerse#12 { Non, Pluton, regne en paix, joüis de ton partage ; }
\livretVerse#12 { Je viens chercher Alceste en cét affreux Séjour, }
\livretVerse#8 { Permets que je la rende au jour, }
\livretVerse#8 { Je ne veux point d’autre avantage. }
\livretVerse#6 { Si c’est te faire outrage }
\livretVerse#8 { D’entrer par force dans ta Cour }
\livretVerse#6 { Pardonne à mon Courage }
\livretVerse#6 { Et fais grace à l’Amour. }
\livretPers Proserpine
\livretVerse#8 { Un grand Coœur peut tout quand il aime, }
\livretVerse#8 { Tout doit ceder à son effort. }
\livretVerse#6 { C’est un Arrest du Sort, }
\livretVerse#7 { Il faut que l’Amour extréme }
\livretVerse#3 { Soit plus fort }
\livretVerse#3 { Que la Mort. }
\livretPers Pluton
\livretVerse#7 { Les Enfers, Pluton luy-mesme, }
\livretVerse#7 { Tout doit en estre d’accord ; }
\livretVerse#7 { Il faut que l’Amour extréme }
\livretVerse#3 { Soit plus fort }
\livretVerse#3 { Que la Mort. }
\livretPers Suivans de Pluton
\livretVerse#7 { Il faut que l’Amour extréme }
\livretVerse#3 { Soit plus fort }
\livretVerse#3 { Que la Mort. }
\livretPers Pluton
\livretVerse#12 { Que pour revoir le jour l’Ombre d’Alceste sorte ; }
\livretDidasP\justify {
  Pluton donne un coup de son Trident & fait sortir son Char.
}
\livretVerse#12 { Prenez place tous deux au Char dont je me sers : }
\livretVerse#8 { Qu’au gré de vos vœux, il vous porte ; }
\livretVerse#8 { Partez, les chemins sont ouverts. }
\livretVerse#6 { Qu’une volante Escorte }
\livretVerse#6 { Vous conduise au travers }
\livretVerse#8 { Des noires vapeurs des Enfers. }
\livretDidasP\justify {
  Alcide & l’Ombre d’Alceste se placent sur le Char de Pluton,
  qui les enleve sous la conduite d’une Troupe volante de
  Suivants de Plutons.
}
\livretFinAct Fin du quatriéme Acte
\sep

\livretAct ACTE CINQUIÉME
\livretDescAtt\justify {
  Le Theatre change, & represente un Arc de Triomphe au milieu de deux
  Amphiteatres, où l’on void une multitude de differents Peuples de la
  Gréce assemblez pour recevoir Alcide triomphant des Enfers.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  ADMETE, LE CHŒUR.
}
\livretPers Admete
\livretRef#'FAAadmeteChoeur
\livretVerse#8 { Alcide est vainqueur du Trépas, }
\livretVerse#8 { L’Enfer ne luy resiste pas. }
\livretVerse#8 { Il rameine Alceste vivante ; }
\livretVerse#4 { Que chacun chante, }
\livretVerse#8 { Alcide est vainqueur du Trépas, }
\livretVerse#8 { L’Enfer ne luy resiste pas. }
\livretPersDidas Le Chœur \line { sur l’Arc de Triomphe & sur les Amphiteatres }
\livretVerse#8 { Alcide est vainqueur du Trépas, }
\livretVerse#8 { L’Enfer ne luy resiste pas. }
\livretPers Admete
\livretVerse#6 { Quelle douleur secrette }
\livretVerse#6 { Rend mon ame inquiette, }
\livretVerse#6 { Et trouble mon amour. }
\livretVerse#8 { Alceste voit encor le jour, }
\livretVerse#8 { Mais c’est pour un autre qu’Admete. }
\livretPers Le Chœur
\livretVerse#8 { Alcide est vainqueur du Trépas, }
\livretVerse#8 { L’Enfer ne luy resiste pas. }
\livretPers Admete
\livretVerse#8 { Ah ! du moins cachons ma tristesse ; }
\livretVerse#12 { Alceste dans ces Lieux rameine les plaisirs. }
\livretVerse#8 { Je dois rougir de ma foiblesse, }
\livretVerse#12 { Quelle honte à mon cœur de mesler des souspirs }
\livretVerse#8 { Avec tant de cris d’allegresse. }
\livretPers Le Chœur
\livretVerse#8 { Alcide est vainqueur du Trépas, }
\livretVerse#8 { L’Enfer ne luy resiste pas. }
\livretPers Admete
\livretVerse#8 { Par une ardeur impatiente }
\livretVerse#8 { Courons, & devançons ses pas. }
\livretVerse#8 { Il rameine Alceste vivante, }
\livretVerse#4 { Que chacun chante. }
\livretPers Admete & le Chœur
\livretVerse#8 { Alcide est vainqueur du Trépas, }
\livretVerse#8 { L’Enfer ne luy resiste pas. }

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  LYCHAS, STRATON enchaisné.
}
\livretPers Straton
\livretRef#'FBAstratonLychas
\livretVerse#12 { Ne m’osteras-tu point la chaine qui m’accable, }
\livretVerse#12 { Dans ce jour destiné pour tant d’aimables jeux ! }
\livretVerse#6 { Ah ! qu’il est rigoureux }
\livretVerse#6 { D’estre seul miserable }
\livretVerse#8 { Quand on voit tout le monde heureux ! }
\livretPersDidas Lychas mettant Straton en liberté
\livretVerse#8 { Aujourd’huy qu’Alcide rameine }
\livretVerse#6 { Alceste des Enfers, }
\livretVerse#6 { Je veux finir ta peine. }
\livretVerse#8 { Qu’on ne porte plus d’autres fers }
\livretVerse#8 { Que ceux dont l’Amour nous enchaine. }
\livretPers Straton, & Lychas
\livretVerse#8 { Qu’on ne porte plus d’autres fers }
\livretVerse#8 { Que ceux dont l’Amour nous enchaine. }

\livretScene SCENE III
\livretDescAtt\wordwrap-center {
  CÉPHISE, LYCHAS, STRATON.
}
\livretPers Lychas, & Straton
\livretRef#'FCAcephiseLychasStraton
\livretVerse#8 { Voy, Céphise, voy qui de nous }
\livretVerse#8 { Peut rendre ton destin plus doux, }
\livretVerse#8 { Et termine enfin nos querelles. }
\livretPers Lychas
\livretVerse#8 { Mes amours seront eternelles. }
\livretPers Straton
\livretVerse#8 { Mon cœur ne sera plus jaloux. }
\livretPers Lychas & Straton
\livretVerse#7 { Entre deux Amants fidelles, }
\livretVerse#7 { Choisis un heureux Espoux. }
\livretPers Céphise
\livretVerse#7 { Je n’ay point de choix à faire ; }
\livretVerse#7 { Parlons d’aimer & de plaire, }
\livretVerse#7 { Et vivons toûjours en paix. }
\livretVerse#7 { L’Himen détruit la tendresse }
\livretVerse#7 { Il rend l’Amour sans attraits ; }
\livretVerse#7 { Voulez-vous aimer sans cesse, }
\livretVerse#7 { Amants, n’espousez jamais. }
\livretPers Céphise, Lychas, & Straton
\livretVerse#7 { L’Himen détruit la tendresse }
\livretVerse#7 { Il rend l’Amour sans attraits ; }
\livretVerse#7 { Voulez-vous aimer sans cesse, }
\livretVerse#7 { Amants, n’espousez jamais. }
\livretPers Céphise
\livretVerse#11 { Prenons part aux transports d’une joye éclatante : }
\livretVerse#4 { Que chacun chante. }
\livretPers Tous ensemble
\livretVerse#8 { Alcide est vainqueur du Trépas, }
\livretVerse#8 { L’Enfer ne luy resiste pas. }
\livretVerse#8 { Il rameine Alceste vivante ; }
\livretVerse#4 { Que chacun chante, }
\livretVerse#8 { Alcide est vainqueur du Trépas, }
\livretVerse#8 { L’Enfer ne luy resiste pas. }

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  ALCIDE, ALCESTE, ADMETE, CÉPHISE, LYCHAS, STRATON, PHERES, CLEANTE,
  LE CHŒUR.
}
\livretPers Alcide
\livretRef#'FDBrecit
\livretVerse#8 { Pour une si belle victoire }
\livretVerse#8 { Peut-on avoir trop entrepris ? }
\livretVerse#10 { Ah qu’il est doux de courir à la gloire }
\livretVerse#10 { Lors que l’Amour en doit donner le prix ! }
\livretVerse#12 { Vous détournez vos yeux ! je vous trouve insensible ? }
\livretVerse#12 { Admete a seul icy vos regards les plus doux ? }
\livretPers Alceste
\livretVerse#7 { Je fais ce qui m’est possible }
\livretVerse#7 { Pour ne regarder que vous. }
\livretPers Alcide
\livretVerse#8 { Vous devez suivre mon envie, }
\livretVerse#8 { C’est pour moy qu’on vous rend le jour. }
\livretPers Alceste
\livretVerse#8 { Je n’ay pû reprendre la vie }
\livretVerse#8 { Sans reprendre aussi mon amour. }
\livretPers Alcide
\livretVerse#12 { Admete en ma faveur vous a cedé luy-mesme. }
\livretPers Admete
\livretVerse#12 { Alcide pouvoit seul vous oster au Trépas. }
\livretVerse#12 { Alceste, vous vivez, je revoy vos appas, }
\livretVerse#12 { Ay-je pû trop payer cette douceur extréme. }
\livretPers Admete & Alceste
\livretVerse#6 { Ah que ne fait-on pas }
\livretVerse#6 { Pour sauver ce qu’on aime ! }
\livretPers Alcide
\livretVerse#12 { Vous soûpirez tous deux au gré de vos desirs ; }
\livretVerse#8 { Est-ce ainsi qu’on me tient parole ? }
\livretPersDidas Admete & Alceste ensemble
\livretVerse#8 { Pardonnez aux derniers soûpirs }
\livretVerse#12 { D’un mal-heureux Amour qu’il faut qu’on vous immole. }
\livretVerse#8 { \raise#0.5 { \raise#1 \column { Alceste, Ademete, } \right-brace#20 } il ne faut plus nous voir. }
\livretVerse#12  { D’un autre que  \raise#0.7 { \left-brace#20 \raise#1 \column { \line { de moy vostre sort } \line { de vous mon destin } } \right-brace#20 } doit dépendre, }
\livretVerse#12 { Il faut dans les grands Cœurs que l’Amour le plus tendre }
\livretVerse#8 { Soit la Victime du devoir. }
\livretVerse#8 { \raise#0.5 { \raise#1 \column { Alceste, Ademete, } \right-brace#20 } il ne faut plus nous voir. }
\livretDidasP\justify {
  Admete se retire, & Alceste offre sa main à Alcide qui arreste Admete,
  & luy cede la main qu’Alceste luy presente.
}
\livretPers Alcide
\livretVerse#8 { Non, non, vous ne devez pas croire }
\livretVerse#12 { Qu’un Vainqueur des Tirans soit Tiran à son tour : }
\livretVerse#12 { Sur l’Enfer, sur la Mort, j’emporte la victoire ; }
\livretVerse#8 { Il ne manque plus à ma gloire }
\livretVerse#8 { Que de triompher de l’Amour. }
\livretPers Admete & Alcide
\livretVerse#6 { Ah quelle gloire extresme ! }
\livretVerse#6 { Quel heroïque effort ! }
\livretVerse#6 { Le Vainqueur de la Mort }
\livretVerse#6 { Triomphe de luy-mesme. }

\livretScene SCENE V
\livretDescAtt\wordwrap-center {
  APOLLON, LES MUSES, LES JEUX, ALCIDE, ADMETE, ALCESTE, & leur Suite.
}
\livretDescAtt\justify {
  Apollon descend dans un Palais éclatant au milieu des Muses & des Jeux
  qu’il ameine pour prendre part à la joye d’Admete & d’Alceste, & pour
  celebrer le Triomphe d’Alcide.
}
\livretPers Apollon
\livretRef#'FEBrecit
\livretVerse#12 { Les Muses & les Jeux s’empressent de descendre, }
\livretVerse#12 { Apollon les conduit dans ces aimables lieux. }
\livretVerse#8 { Vous, à qui j’ay pris soin d’aprendre }
\livretVerse#12 { A chanter vos Amours sur le ton le plus tendre, }
\livretVerse#8 { Bergers, chantez avec les Dieux. }
\livretVerse#8 { Chantons, chantons, faisons entendre }
\livretVerse#8 { Nos chansons jusques dans les Cieux. }

\livretScene SCENE SIXIÉME ET DERNIERE
\livretDescAtt\justify {
  Une Troupe de Bergers & de Bergeres, & une Troupe de Pastres, dont les
  uns chantent & les autres dancent, viennent par l’ordre d’Apollon
  contribuer à la rejoüissance.
}
\livretPersDidas Les Chœurs des Muses des Thessaliens & des Bergers chantent ensemble
\livretRef#'FFAchoeur
\livretVerse#8 { Chantons, chantons, faisons entendre }
\livretVerse#8 { Nos chansons jusques dans les Cieux. }
\livretPersDidas Straton chante au milieu des Pastres dançants
\livretRef#'FFDstraton
\livretVerse#3 { A quoy bon }
\livretVerse#4 { Tant de raison }
\livretVerse#4 { Dans le bel âge ? }
\livretVerse#3 { A quoy bon }
\livretVerse#4 { Tant de raison }
\livretVerse#4 { Hors de saison ? }
\livretVerse#5 { Qui craint le danger }
\livretVerse#4 { De s’engager }
\livretVerse#4 { Est sans courage : }
\livretVerse#5 { Tout rit aux Amants. }
\livretVerse#4 { Les Jeux charmants }
\livretVerse#4 { Sont leur partage : }
\livretVerse#7 { Tost, tost, tost, soyons contents, }
\livretVerse#4 { Il vient un temps }
\livretVerse#4 { Qu’on est trop sage. }
\livretPersDidas Céphise \wordwrap { chante au milieu des Bergers & des Bergeres qui dancent }
\livretRef#'FFFcephiseChoeur
\livretVerse#6 { C’est la saison d’aimer }
\livretVerse#4 { Quand on sçait plaire, }
\livretVerse#6 { C’est la saison d’aimer }
\livretVerse#5 { Quand on sçait charmer. }
\livretVerse#10 { Les plus beaux de nos jours ne durent guére, }
\livretVerse#11 { Le sort de la Beauté nous doit allarmer, }
\livretVerse#10 { Nos Champs n’ont point de Fleur plus passagere ; }
\livretVerse#6 { C’est la saison d’aimer }
\livretVerse#4 { Quand on sçait plaire, }
\livretVerse#6 { C’est la saison d’aimer }
\livretVerse#5 { Quand on sçait charmer. }
\livretVerse#8 { Un peu d’amour est necessaire, }
\livretVerse#10 { Il n’est jamais trop tost de s’enflamer ; }
\livretVerse#10 { Nous donne-t’on un cœur pour n’en rien faire ? }
\livretVerse#6 { C’est la saison d’aimer }
\livretVerse#4 { Quand on sçait plaire, }
\livretVerse#6 { C’est la saison d’aimer }
\livretVerse#5 { Quand on sçait charmer. }
\livretDidasP\justify {
  La Troupe des Bergers dance avec la Troupe des Pastres.
  Les Chœurs se respondent les uns aux autres, & s’unissent
  enfin tous ensemble.
}
\livretPers Les Chœurs
\livretVerse#8 { Triomphez, genereux Alcide, }
\livretVerse#8 { Aimez en paix heureux Espoux. }
\livretVerse#8 { Que \raise#0.5 { \left-brace#20 \raise#1 \column { \line { toûjours la Gloire } \line { sans cesse l’Amour } } \right-brace#20 } vous guide. }
\livretVerse#12 { Joüissez à jamais des \raise#0.5 { \left-brace#20 \raise#1 \column { honneurs plaisirs } \right-brace#20 } les plus doux. }
\livretVerse#8 { Triomphez, genereux Alcide, }
\livretVerse#8 { Aimez en paix heureux Espoux. }
\livretDidasP\justify { Apollon vole avec les Jeux. }
\livretFinAct Fin du cinquiéme & dernier Acte
}
