\livretAct ACTE TROISIÉME
\livretDescAtt\justify {
  Le Theatre est un grand Monument élevé par les Arts.
  Un Autel vide paroist au milieu pour servir à porter
  l’Image de la personne qui s’immolera pour Admete.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  ALCESTE, PHERES, CÉPHISE.
}
\livretPers Alceste
\livretRef #'DAArecit
%# Ah pourquoy nous séparez-vous?
%# Eh du moins attendez que la Mort nous separe;
%# Cru=els, quelle pitié barbare
%# Vous presse d'arracher Alceste à son Espoux?
%# Ah pourquoy nous séparez-vous?
\livretPers Pheres, & Céphise
%# Plus vostre Espoux mourant voit d'amour, & d'appas,
%# Et plus le jour qu'il perd luy doit faire d'envie:
%# Ce sont les douceurs de la vie
%# Qui font les horreurs du trépas.
\livretPers Alceste
%# Les Arts n'ont point encore achevé leur ouvrage;
%# Cét Autel doit porter la glori=euse Image
%# De qui signalera sa foy
%# En mourant pour sauver son Roy.
%# Le prix d'une gloire immortelle
%# Ne peut-il toucher un grand Cœur?
%# Faut-il que la Mort la plus belle
%# Ne laisse pas de faire peur?
%# A quoy sert la foule importune
%# Dont les Roys sont embarrassez?
%# Un coup fatal de la Fortune
%# Escarte les plus empressez.
\livretPers Alceste, Pherès, & Céphise
%# De tant d'Amis qu'avoit Admete
%# Aucun ne vient le secourir;
%# Quelque *honneur qu'on promette
%# On le laisse mourir.
\livretPers Pheres
%# J'aime mon Fils, je l'ay fait Roy;
%# Pour prolonger son sort je mourrois sans effroy,
%# Si je pouvois offrir des jours dignes d'envie;
%# Je n'ay plus qu'un reste de vie
%# Ce n'est rien pour Admete, & c'est beaucoup pour moy.
\livretPers Céphise
%# Les Honneurs les plus éclatans
%# En vain dans le Tombeau promettent de nous suivre;
%# La Mort est affreuse en tout temps:
%# Mais peut-on renoncer à vivre
%# Quand on n'a vescu que quinze ans?
\livretPers Alceste
%# Chacun est satisfait des excuses qu'il donne:
%# Cependant on ne voit personne
%# Qui pour sauver Admete ose perdre le jour;
%# Le Devoir, l'Amitié, le Sang, tout l'abandonne,
%# Il n'a plus d'espoir qu'en l'Amour.

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  PHERES, LE CHŒUR, CLEANTE.
}
\livretPers Pheres
\livretRef #'DBArecit
%# Voy=ons encor mon Fils, allons, hastons nos pas;
%# Ses yeux vont se couvrir d'éternelles tenebres.
\livretPers Le Chœur
%# Helas! helas! helas!
\livretPers Pheres
%# Quels cris! quelles plaintes funebres!
\livretPers Le Chœur
%# Helas! helas! helas!
\livretPers Pheres
%# Où vas-tu? Cleante, demeure.
\livretPers Cleante
%# Helas! helas!
%# Le Roy touche à sa dernière heure,
%# Il s'affoiblit, il faut qu'il meure,
%# Et je viens pleurer son trespas.
%# Helas! helas!
\livretPers Le Chœur
%# Helas! helas! helas!
\livretPers Pheres
%# On le plaint, tout le monde pleure,
%# Mais nos pleurs ne le sauvent pas.
%# Helas! helas!
\livretPers Le Chœur
%# Helas! helas! helas!

\livretScene SCENE III
\livretDescAtt\wordwrap-center {
  LE CHŒUR, ADMETE, PHERES, CLEANTE.
}
\livretPers Le Chœur
\livretRef #'DCArecit
%# O trop heureux Admete!
%# Que vostre sort est beau!
\livretPers Pheres & Cleante
%# Quel changement! quel bruit nouveau!
\livretPers Le Chœur
%# O trop heureux Admete!
%# Que vostre sort est beau!
\livretPersDidas\smallCaps Pheres & Cleante voyant Admete guery.
%# L'effort d'une Amitié parfaite
%# L'a sauvé du Tombeau
\livretPersDidas\smallCaps Pheres embrassant Admete.
%# O trop heureux Admete!
%# Que vostre sort est beau!
\livretPers Le Chœur
%# O trop heureux Admete!
%# Que vostre sort est beau!
\livretPers Admete
%# Qu'une Pompe funebre
%# Rende à jamais celebre
%# Le genereux effort
%# Qui m'arrache à la Mort.
%# Alceste n'aura plus d'allarmes,
%# Je reverray ses yeux charmants
%# A qui j'ay cousté tant de larmes:
%# Que la vie =a de charmes
%# Pour les heureux Amants!
%# Achevez, Dieux des Arts, faites nous voir l'Image
%# Qui doit eterniser la grandeur de courage
%# De qui s'est immolé pour moy;
%#8 Ne differez point davantage…
%# Ciel! ô Ciel! qu'est-ce que je voy!
\livretDidasP\justify {
  L’Autel s’ouvre, & l’on voit sortir l’Image d’Alceste qui se perce le
  sein.
}

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  CÉPHISE, ADMETE, PHERES, CLEANTE, LE CHŒUR.
}
\livretPers Céphise
\livretRef #'DDArecit
%#- Alceste est morte.
\livretPers Admete
%#= Alceste est morte!
\livretPers Le Chœur
%# Alceste est morte.
\livretPers Céphise
%# Alceste a satisfait les Parques en courroux;
%# Vostre Tombeau s'ouvroit, elle y descend pour vous,
%# Elle-mesme a voulu vous en fermer la porte;
%#- Alceste est morte.
\livretPers Admete
%#= Alceste est morte!
\livretPers Le Chœur
%# Alceste est morte.
\livretPers Céphise
%# J'ay couru, mais trop tard pour arrester ses coups:
%# Jamais en faveur d'un Espoux
%# On ne verra d'ardeur si fidelle & si forte;
%#- Alceste est morte.
\livretPers Admete
%#= Alceste est morte!
\livretPers Le Chœur
%# Alceste est morte.
\livretPers Céphise
%# Sujets, Amis, Parents, vous abandonnoient tous;
%# Sur les Droits les plus forts, sur les Nœuds les plus doux,
%# L'Amour, le tendre Amour l'emporte:
%#- Alceste est morte.
\livretPers Admete
%#= Alceste est morte!
\livretPers Le Chœur
%# Alceste est morte.
\livretDidasP\justify {
  Admete tombe accablé de douleur entre les bras de sa suite.
}

\livretScene SCENE V
\livretDescAtt\justify {
  Troupe de femmes affligées, Troupe d’Hommes desolez, qui portent des
  fleurs, & tous les ornements qui ont servy à parer Alceste.
}
% \livretPers Tous ensemble
% %# Formons les plus lugubres chants,
% %# Et les regrets les plus touchants.
\livretPers Une femme affligée
\livretRef #'DEBair
%# La Mort, la Mort barbare,
%# Détruit aujourd'huy mille appas.
%# Quelle Victime, *helas!
%# Fut jamais si belle, & si rare?
%# La Mort, la Mort barbare
%# Détruit aujourd'huy mille appas.

% \livretPers Un homme désolé
% %# Alceste si jeune, & si belle,
% %# Court se precipiter dans la Nuit eternelle,
% %# Pour sauver ce qu'elle aime elle a perdu le jour.
% \livretPers Le Chœur
% %# O trop parfait Modelle
% %# D'une Espouse fidelle!
% %# O trop parfait Modelle
% %# D'un veritable Amour!
% \livretPers Une femme affligée

%# Que nostre zéle se partage;
%# Que les uns par leurs chants celebrent son courage,
%# Que d'autres par leurs cris déplorent ses mal-heurs.
\livretPers Le Chœur
%# Rendons hommage
%# A son Image;
%# Jettons des fleurs,
%# Versons des pleurs.
\livretPers Une femme affligée
%# Alceste, la Charmante Alceste,
%# La fidelle Alceste n'est plus.
\livretPers Le Chœur
%# Alceste, la Charmante Alceste,
%# La fidelle Alceste n'est plus.
\livretPers Une femme affligée
%# Tant de beautez, tant de vertus,
%# Meritoient un sort moins funeste.
\livretPers Le Chœur
%# Alceste, la Charmante Alceste,
%# La fidelle Alceste n'est plus.
\livretDidasP\justify {
  Un transport de douleur saisit les deux Troupes affligées,
  une partie déchire ses habits, l’autre s’arrache les cheveux,
  & chacun brise au pied de l’Image d’Alceste les ornements qu’il
  porte à la main.
}
\livretPers Le Chœur
%# Rompons, brisons le triste reste
%# De ces Ornemens superflus.
\livretRef #'DECchoeur
%# Que nos pleurs, que nos cris renouvellent sans cesse
%# Allons porter par tout la douleur qui nous presse.

\livretScene SCENE VI
\livretDescAtt\wordwrap-center {
  ADMETE, PHERES, CÉPHISE, CLEANTE, suite.
}
\livretPersDidas Admete \line { revenu de son évanoüissement, & se voyant désarmé. }
\livretRef #'DFArecit
%# Sans Alceste, sans ses appas,
%# Croy=ez-vous que je puisse vivre!
%# Laissez moy courir au Trespas
%# Où ma chere Alceste se livre.
%# Sans Alceste, sans ses appas,
%# Croy=ez-vous que je puisse vivre?
%# C'est pour moy qu'elle meurt, helas!
%# Pourquoy m'empescher de la suivre?
%# Sans Alceste, sans ses appas,
%# Croy=ez-vous que je puisse vivre.

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  ALCIDE, ADMETE, PHERES, CÉPHISE, CLEANTE.
}
\livretPers Alcide
\livretRef #'DGArecit
%# Tu me vois arresté sur le point de partir
%# Par les tristes clameurs qu'on entend retentir.
\livretPers Admete
%# Alceste meurt pour moy par une amour extresme,
%# Je ne reverray plus les yeux qui m'ont charmé:
%# Helas! j'ay perdu ce que j'aime
%# Pour avoir esté trop aimé.
\livretPers Alcide
%# J'aime Alceste, il est temps de ne m'en plus defendre;
%# Elle meurt, ton amour n'a plus rien à pretendre;
%# Admete, cede moy la Beauté que tu perds:
%# Au Palais de Pluton j'entreprends de descendre:
%# J'iray jusqu'au fonds des Enfers
%# Forcer la Mort à me la rendre.
\livretPers Admete
%# Je verrois encor ses beaux yeux?
%# Allez, Alcide, allez, revenez glori=eux,
%# Obtenez qu'Alceste vous suive:
%# Le Fils du plus puissant des Dieux
%# Est plus digne que moy du bien dont on me prive.
%# Allez, allez, ne tardez pas,
%# Arrachez Alceste au Trespas,
%# Et ramenez au jour son Ombre fugitive;
%# Qu'elle vive pour vous avec tous ses appas,
%# Admete est trop heureux pourveu qu'Alceste vive.
\livretPers Pheres, Céphise, Cleante
%# Allez, allez, ne tardez pas,
%# Arrachez Alceste au Trespas.

\livretScene SCENE VIII
\livretDescAtt\wordwrap-center {
  DIANE, MERCURE, ALCIDE, ADMETE, PHERES, CÉPHISE, CLEANTE.
}
\livretDidasP\justify {
  La Lune paroist, son Globe s’ouvre, & fait voir Diane sur un
  Nuage brillant.
}
\livretPers Diane
\livretRef #'CHArecit
%# Le Dieu dont tu tiens la naissance
%# Oblige tous les Dieux d'estre d'intelligence
%# En faveur d'un dessein si beau;
%# Je viens t'offrir mon assistance;
%# Et Mercure s'avance
%# Pour t'ouvrir aux Enfers un passage nouveau.
\livretDidasP\justify {
  Mercure vient en volant frapper la Terre de son Caducée,
  l’Enfer s’ouvre, & Alcide y descend.
}
\livretFinAct Fin du troisiéme Acte
\sep

