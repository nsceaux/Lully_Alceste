\livretAct ACTE SECOND
\livretDescAtt\justify {
  La Scene est dans l’Isle de Scyros, & le Theatre
  represente la Ville principale de l’Isle.
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  CÉPHISE, STRATON.
}
\livretPers Céphise
\livretRef #'CAArecit
%# Alceste ne vient point, & nous devons attendre.
\livretPers Straton
%# Que peut-elle pretendre?
%# Pourquoy se tourmenter icy mal à propos?
%# Ses cris ont beau se faire entendre,
%# Peut-estre son Espoux a peri dans les flots,
%# Et nous sommes enfin dans l'Isle de Scyros.
\livretPers Céphise
%# Tu ne te plaindras point que j'en use de mesme.
%# Je t'ay donné peu d'embarras.
%# Tu vois comme je suis tes pas.
\livretPers Straton
%# Tu sçais dissimuler une colere extresme.
\livretPers Céphise
%# Et si je te disois que c'est toy seul que j'ayme?
\livretPers Straton
%# Tu le dirois en vain je ne te croirois pas.
\livretPers Céphise
%# Croy moy: si j'ay faint de changer
%# C'estoit pour te mieux engager.
%# Un Rival n'est pas inutile,
%# Il réveille l'ardeur & les soins d'un Amant;
%# Une conqueste facile
%# Donne peu d'empressement,
%# Et l'Amour tranquile
%# S'endort aisément.
\livretPers Straton
%# Non, non, ne tente point une seconde ruse,
%# Je voy plus clair que tu ne crois.
%# On excuse d'abord un Amant qu'on abuse.
%# Mais la sotise est sans excuse
%# De se laisser tromper deux fois.
\livretPers Céphise
%# N'est-il aucun mo=yen d'apaiser ta colere?
\livretPers Straton
%# Consens à m'espouzer & sans retardement.
\livretPers Céphise
%# Une si grande affaire
%# Ne se fait pas si promptement
%# Un Himen qu'on differe
%# N'en est que plus charmant.
\livretPers Straton
%# Un Himen qui peut plaire
%# Ne couste guère,
%# Et c'est un nœud bien tost formé;
%# Rien n'est plus aisé que de faire
%# Un Espoux d'un Amant aimé.
\livretPers Céphise
%# Je t'aime d'une amour sincere;
%# Et s'il est necessaire,
%# Je m'offre à t'en faire un serment.
\livretPers Straton
%# Amusement, amusement.
\livretPers Céphise
%# L'injuste enlevement d'Alceste
%# Attire dans ces lieux une guerre funeste,
%# Les plus braves des Grecs s'arment pour son secours:
%# Au milieu des cris & des larmes,
%# L'*Himen a peu de charmes;
%# Attendons de tranquiles jours.
%# Le bruit affreux des armes
%# Effarouche bien les Amours.
\livretPers Straton
%# Discours, discours, discours.
%# Tu n'as qu'à m'espouzer pour m'oster tout ombrage,
%# Pourquoy differer davantage?
%# A quoy servent tant de façons?
\livretPers Céphise
%# Rends moy la liberté pour m'espouzer sans crainte;
%# Un Himen fait avec contrainte
%# Est un mauvais moy=en de finir tes soupçons.
\livretPers Straton
%# Chansons, chansons, chansons.

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  LICOMEDE, ALCESTE, STRATON, CÉPHISE, Soldats de Licomede.
}
\livretPers Licomede
\livretRef #'CBArecit
%# Allons, allons, la plainte est vaine.
\livretPers Alceste
%# Ah quelle rigueur inhumaine!
\livretPers Licomede
%# Allons, je suis sourd à vos cris,
%# Je me vange de vos mespris.
\livretPers Alceste
%# Vous serez inexorable?
\livretPers Licomede
%# Cru=elle, vous m'avez apris
%# A devenir impitoy=able.
\livretPers Alceste
%# Est-ce ainsi que l'Amour a sçeu vous émouvoir?
%# Est-ce ainsi que pour moy vostre ame est attendrie?
\livretPers Licomede
%# L'Amour se change en Furie
%# Quand il est au désespoir.
%# Puis que je perds toute esperance,
%# Je veux desesperer mon Rival à son tour;
%# Et les douceurs de la Vengeance
%# Ont dequoy consoler les rigueurs de l'Amour.
\livretPers Alceste
%# Voy=ez la douleur qui m'accable.
\livretPers Licomede
%# Vous avez sans pitié regardé ma douleur.
%# Vous m'avez rendu miserable
%# Vous partagerez mon mal-heur.
\livretPers Alceste
%# Admete avoit mon cœur dés ma plus tendre enfance;
%# Nous ne connoissions pas l'Amour ny sa puissance.
%# Lors que d'un nœud fatal il vint nous enchaisner:
%# Ce n'est pas une grande offence
%# Que le refus d'un cœur qui n'est plus à donner.
\livretPers Licomede
%# Est-ce aux Amants qu'on desespere
%# A devoir rien examiner
%# Non, je ne puis vous pardonner
%# D'avoir trop sçeu me plaire.
%# Que ne m'ont point cousté vos funestes attraits!
%# Ils ont mis dans mon cœur une cru=elle flame,
%# Ils ont arraché de mon ame
%# L'innocence, & la paix.
%# Non, Ingrate, non, Inhumaine,
%# Non, quelle que soit vostre peine,
%# Non, je ne vous rendray jamais
%# Tous les maux que vous m'avez faits.
\livretPers Straton
%# Voicy l'Ennemy qui s'avance
%# En diligence.
\livretPers Licomede
%# Preparons-nous
%# A nous defendre.
\livretPers Alceste
%# Ah Cru=el, que n'espargnez-vous
%# Le sang qu'on va respandre!
\livretPersDidas \smallCaps Licomede à ses soldats
%# Perissons tous
%# Plûtost que de nous rendre.
\livretDidasP\justify {
  Licomede contraint Alceste d’entrer dans la Ville,
  Céphise la suit, & les Soldats de Licomede ferment
  la Porte de la Ville aussi tost qu’ils y sont entrez.
}

\livretScene SCENE III
\livretDescAtt\wordwrap-center {
  ADMETE, ALCIDE, LYCHAS, Soldats assiegans.
}
\livretPers Ademete & Alcide
\livretRef #'CCBair
%# Marchez, marchez, marchez.
%# Aprochez, Amis, aprochez,
%# Marchez, marchez, marchez.
%# Hastons-nous de punir des Traistres,
%# Rendons-nous Maistres
%# Des Murs qui les tiennent cachez:
%# Marchez, marchez, marchez.

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  LICOMEDE, STRATON, Soldats assiegez,
  ADMETE, ALCIDE, LYCHAS, Soldates assiegans.
}
\livretPersDidas Licomede sur les Remparts
\livretRef #'CDAchoeur
%# Ne pretendez pas nous surprendre,
%# Venez, nous allons vous attendre:
%# Nous ferons tous nostre devoir
%# Pour vous bien recevoir.
\livretPers\line { \smallCaps Straton & les Soldats assiegez }
%# Nous ferons tous nostre devoir
%# Pour vous bien recevoir.
\livretPers Admete
%# Perfide, évite un sort funeste,
%# On te pardonne tout si tu veux rendre Alceste.
\livretPers Licomede
%# J'aime mieux mourir, s'il le faut,
%# Que de ceder jamais cét Objet plein de charmes.
\livretPers Ademete & Alcide
%# A l'assaut, à l'assaut.
\livretPers Licomede & Straton
%# Aux armes, aux armes.
\livretPers Les Assiegeans
%# A l'assaut, à l'assaut.
\livretPers Les Assiegez
%# Aux armes, aux armes.
\livretPers Ademete, Alcide & Licomede
%# A moy, Compagnons, à moy.
\livretPers Ademete & Licomede
%# A moy, suivez vostre Roy.
\livretPers Alcide
%# C'est Alcide
%# Qui vous guide.
\livretPers Ademete, Alcide & Licomede
%# A moy, Compagnons, à moy.
\livretDidasP\justify {
  On fait avancer des Beliers & autres Machines de guerre pour battre
  la Place.
}
\livretPers Tous ensemble
%# Donnons, donnons de toutes parts.
\livretPers Les Assiegeans
%# Que chacun à l'envy combatte.
%# Que l'on abatte
%# Les Tours, & les Remparts.
\livretPers Tous ensemble
%# Donnons, donnons de toutes parts.
\livretPers Les Assiegez
%# Que les Ennemis, pesle mesle,
%# Trébuchent sous l'affreuse gresle
%# De nos fléches, & de nos dards.
\livretPers Tous
%# Donnons, donnons de toutes parts.
%# Courage, courage, courage,
%# Ils sont à nous, ils sont à nous.
\livretPers Alcide
%# C'est trop disputer l'avantage,
%# Je vais vous ouvrir un passage,
%# Suivez-moy tous, suivez-moy tous.
\livretPers Tous ensemble
%# Courage, courage, courage,
%# Ils sont à nous, ils sont à nous.
\livretRef #'CDBentree
\livretDidasPPage\justify {
  Les Assiegez voyant leurs Remparts à demy abattus, & la Porte de la
  Ville enfoncée, font un fernier effort dans une sortie pour
  repousser les Assiegeans.
}
\livretPers Les Assiegeans
\livretRef #'CDCchoeur
%# Achevons d'emporter la Place;
%# L'Ennemy commence à pli=er.
%# Main basse, main basse, main basse.
\livretPersDidas Les Assiegez rendans les Armes
%# Quartier, quartier, quartier.
\livretPers Les Assiegeans
%# La Ville est prise.
\livretPers Les Assiegez
%# Quartier, quartier, quartier.
\livretPersDidas Lychas terrassant Straton
%# Il faut rendre Céphise.
\livretPers Straton
%# Je suis ton prisonnier,
%# Quartier, quartier, quartier.

\livretScene SCENE V
\livretPersDidas Pheres \line { armé, & marchant avec peine }
\livretRef #'CEApheres
%# Courage Enfants, je suis à vous;
%# Mon bras va seconder vos coups:
%# Mais c'en est déja fait, & l'on a pris la Ville;
%# La foiblesse de l'âge a retardé mes pas:
%# La valeur devient inutile
%# Quand la force n'y respond pas.
%# Que la vieillesse est lente,
%# Les efforts qu'elle tente
%# Sont toûjours impuissans:
%# C'est une charge bien pesante
%# Qu'un fardeau de quatre-vingts ans.

\livretScene SCENE VI
\livretDescAtt\wordwrap-center {
  ALCIDE, ALCESTE, CÉPHISE, PHERES, LYCHAS, STRATON enchaisné.
}
\livretPersDidas Alcide à Pheres
\livretRef #'CFBrecit
%# Rendez à vostre Fils cette aimable Princesse.
\livretPers Pheres
%# Ce don de vostre main seroit encor plus doux.
\livretPers Alcide
%# Allez, allez la rendre à son heureux Espoux.
\livretPers Alceste
%# Tout est soûmis, la guerre cesse;
%# Seigneur, pourquoy me laissez-vous?
%# Quel nouveau soin vous presse?
\livretPers Alcide
%# Vous n'avez rien à redouter,
%# Je vais chercher ailleurs des Tyrans à dompter.
\livretPers Alceste
%# Les nœuds d'une amitié pressante
%#12 Ne retiendront-ils point vostre Ame impati=ente?
%# Et la Gloire toûjours vous doit-elle emporter?
\livretPers Alcide
%# Gardez-vous bien de m'arrester.
\livretPers Alceste
%# C'est vostre Valeur tri=omphante
%# Qui fait le sort charmant que nous allons goûter;
%# Quelque douceur que l'on ressente,
%# Un Amy tel que vous l'augmente,
%# Voulez-vous si-tost nous quitter?
\livretPers Alcide
%# Gardez-vous bien de m'arrester.
%# Laissez, laissez moy fuïr un charme qui m'enchante:
%# Non, toute ma vertu n'est pas assez puissante
%# Pour répondre d'y resister.
%# Non, encore une fois, Princesse trop charmante,
%# Gardez-vous bien de m'arrester.

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  ALCESTE, PHERES, CÉPHISE.
}
\livretPers A trois
\livretRef #'CGAtrio
%# Cherchons Admete promptement.
\livretPers Alceste
%# Peut-on chercher ce qu'on aime
%# Avec trop d'empressement!
%# Quand l'amour est extréme,
%# Le moindre esloignement
%# Est un cru=el tourment.
\livretPers Alceste, Pheres, & Céphise
%# Cherchons Admete promptement.

\livretScene SCENE VIII
\livretDescAtt\wordwrap-center {
  ADMETE blessé, CLEANTE, ALCESTE, PHERES, CÉPHISE, Soldats.
}
\livretPers Alceste
\livretRef #'CHArecit
%# O Dieux! quel spectacle funeste?
\livretPers Cleante
%# Le Chef des Ennemis mourant, & terrassé,
%# De sa rage expirante a ramassé le reste,
%# Le Roy vient d'en estre blessé.
\livretPers Admete
%# Je meurs, charmante Alceste,
%# Mon sort est assez doux
%# Puis que je meurs pour vous.
\livretPers Alceste
%# C'est pour vous voir mourir que le Ciel me délivre!
\livretPers Admete
%# Avec le nom de vostre Espoux
%# J'eusse esté trop heureux de vivre;
%# Mon sort est assez doux
%# Puis que je meurs pour vous.
\livretPers Alceste
%# Est-ce là cét Hymen si doux, si plein d'appas,
%# Qui nous promettoit tant de charmes?
%# Faloit-il que si-tost l'aveugle sort des armes
%# Tranchast des nœuds si beaux par un affreux trépas?
%# Est-ce là cét Hymen si doux, si plein d'appas,
%# Qui nous promettoit tant de charmes?
\livretPers Admete
%# Belle Alceste ne pleurez pas,
%# Tout mon sang ne vaut point vos larmes.
\livretPers Alceste
%# Est-ce là cét Hymen si doux, si plein d'appas,
%# Qui nous promettoit tant de charmes?
\livretPers Admete
%# Alceste, vous pleurez.
\livretPers Alceste
%# Admete, vous mourez.
\livretPers Admete & Alceste ensemble
%# Alceste, vous pleurez;
%# Admete, vous mourez.
\livretPers Alceste
%# Se peut-il que le Ciel permette,
%# Que les cœurs d'Alceste & d'Admete
%# Soient ainsi separez?
\livretPers Admete & Alceste
%# Alceste, vous pleurez;
%# Admete, vous mourez.

\livretScene SCENE IX
\livretDescAtt\wordwrap-center {
  APOLLON, LES ARTS, ADMETE, ALCESTE, PHERES, CÉPHISE, CELANTE,
  Soldats.
}
\livretPersDidas Apollon environné des Arts
\livretRef #'CIBrecit
%# La Lumiere aujourd'hui te doit estre ravie;
%# Il n'est qu'un seul moy=en de prolonger ton sort;
%# Le Destin me promet de te rendre à la vie,
%# Si quelqu'Autre pour toy veut s'offrir à la mort.
%# Reconnoist si quelqu'un t'aime parfaitement;
%# Sa mort aura pour prix une immortelle gloire:
%# Pour en conserver la memoire
%# Les Arts vont élever un pompeux Monument.
\livretDidasP\justify {
  Les Arts qui sont autour d’Apollon se separent sur des Nuages
  differents, & tous descendent pour élever un Monument superbe,
  tandis qu’Apollon s’envole.
}
\livretFinAct Fin du second Acte
\sep

