\livretAct LE RETOUR DES PLAISIRS
\livretScene PROLOGUE
\livretDescAtt\justify {
  Le Theatre represente le Palais & les Jardins des Thuilleries ; La
  Nymphe de la Seine paroist apuyée sur une Urne au milieu d’une Allée
  dont les Arbres sont separez par des Fontaines.
}
\livretPers La Nymphe de la Seine
\livretRef #'AABair
%# Le Heros que j'attens ne reviendra-t'il pas?
%# Serai-je toûjours languissante
%# Dans une si cru=elle attente?
%# Le Heros que j'attens ne reviendra-t'il pas?
%# On n'entend plus d'Oyseau qui chante,
%# On ne voit plus de Fleurs qui naissent sous nos pas.
%# Le Heros que j'attens ne reviendra-t'il pas?
%# L'*herbe naissante
%# Paroist mourante,
%# Tout languit avec moy dans ces lieux pleins d'appas.
%# Le Heros que j'attens ne reviendra-t'il pas?
%# Serai-je toûjours languissante
%# Dans une si cru=elle attente?
%# Le Heros que j'attens ne reviendra-t'il pas?
\null
\livretRef #'AADrecit
%# Quel bruit de guerre m'épouvante?
%# Quelle Divinité va descendre icy bas?
\null
\livretRef #'AAErondeau
\livretDidasPPage\justify {
  La Gloire paroist au milieu d’un Palais brillant qui descend au
  bruit d’une harmonie guerriere
}
\livretPers La Nymphe de la Seine
\livretRef #'AAFrecit
%# Helas! superbe Gloire, *helas!
%# Ne dois-tu point estre contente?
%# Le Heros que j'attens ne reviendra-t'il pas?
%# Il ne te suit que trop dans l'*horreur des Combas;
%# Laisse en paix un moment sa Valeur tri=omphante.
%# Le Heros que j'attens ne reviendra-t'il pas?
%# Serai-je toûjours languissante
%# Dans une si cru=elle attente?
%# Le Heros que j'attens ne reviendra-t'il pas?
\livretPers La Gloire
%# Pourquoy tant murmurer? Nymphe, ta plainte est vaine,
%# Tu ne peux voir sans moy le Heros que tu sers;
%# Si son éloignement te couste tant de peine,
%# Il recompense assés les douceurs que tu pers;
%# Voy ce qu'il fait pour toy quand la Gloire l'emmeine;
%# Voy comme sa Valeur a soûmis à la Seine
%# Le Fleuve le plus fier qui soit dans l'Univers.
\livretPers La Nymphe de la Seine
%# On ne voit plus icy paraistre
%# Que des Ornements imparfaits;
%# Ah! rends-nous nostre Auguste Maistre,
%# Tu nous rendras tous nos attraits.
\livretPers La Gloire
%# Il revient, & tu dois m'en croire;
%# Je luy sers de guide avec soin:
%# Puisque tu vois la Gloire
%# Ton Heros n'est pas loin.
%# Il laisse respirer tout le Monde qui tremble;
%# Soy=ons icy d'accord pour combler ses desirs.
\livretPers La Gloire et la Nymphe de la Seine
%# Qu'il est doux d'accorder ensemble
%# La Gloire & les Plaisirs.
\livretPers La Nymphe de la Seine
%# Na=yades, Dieux des Bois, Nymphes, que tout s'assemble,
%# Qu'on entende nos chants apres tant de soûpirs.
\null
\livretDidasP\justify {
  La Nymphe des Thuilleries s’avance avec une Troupe de Nymphes qui
  dancent, les Arbres s’ouvrent & font voir des Divinitez Champestres
  qui joüent de differents Instruments, & les Fontaines se changent en
  Nayades qui chantent.
}
\livretPers Le Chœur
\livretRef #'AAGchoeur
%# Qu'il est doux d'accorder ensemble
%# La Gloire & les Plaisirs.
\livretPers La Nymphe des Thuilleries
\livretRef #'AAHair
%# L'Art d'accord avec la Nature
%# Sert l'Amour dans ces lieux charmants:
%# Ces Eaux qui font resver par un si doux murmure,
%# Ces Tapis où les Fleurs forment tant d'ornements,
%# Ces Gazons, ces Lits de verdure,
%# Tout n'est fait que pour les Amants.
\null
\livretRef #'AAIentree
\livretDidasPPage\justify {
  La Nymphe de la Marne Compagne de la Seine vient chanter au milieu
  d’une troupe de Divinitez de Fleuves qui témoignent leur joye par
  leur dance.
}
\livretPers La Nymphe de la Marne
\livretRef #'AAJair
%# L'Onde se presse
%# D'aller sans cesse
%# Jusqu'au bout de son cours:
%# S'il faut qu'un Cœur suive une pante,
%# En est-il qui soit plus charmante
%# Que le doux penchant des Amours?
\livretPers La Gloire et la Nymphe de la Seine
\livretRef #'AALchoeur
%# Que tout retentisse:
%# Que tout réponde à nos voix:
\livretPers La Nymphe des Thuilleries
%# Que tout fleurisse
%# Dans nos Jardins & dans nos Bois.
\livretPers La Nymphe de la Marne
%# Que le chant des Oyseaux s'unisse
%# Avec le doux son des Haut-bois.
\livretPers Tous Ensemble
%# Que tout retentisse,
%# Que tout réponde à nos voix.
%# Que le chant des Oyseaux s'unisse
%# Avec le doux son des Haut-bois.
%# Que tout retentisse
%# Que tout réponde à nos voix.
\null
\livretRef #'AAMentree
\livretDidasPPage\justify {
  Les Divinitez de Fleuves & les Nymphes forment une dance generale
  tandis que tous les Instruments & toutes les Voix s’unissent.
}
\livretPers Tous Ensemble
\livretRef #'AANchoeur
%# Quel Cœur sauvage
%# Icy ne s'engage?
%# Quel Cœur sauvage
%# Ne sent point l'amour?
%# Nous allons voir les Plaisirs de retour;
%# Ne manquons pas d'en faire un doux usage:
%# Pour rire un peu, l'on n'est pas moins sage.
%# Ah quel dommage
%# De fuir ce rivage!
%# Ah quel dommage
%# De perdre un beau jour!
%# Nous allons voir les Plaisirs de retour;
%# Ne manquons pas d'en faire un doux usage:
%# Pour rire un peu, l'on n'est pas moins sage.
\livretRef #'AAOchoeur
%# Revenez Plaisirs exilez;
%# Volez, de toutes parts, volez.
\null
\livretDidasP\justify {
  Les Plaisirs volents, & viennent preparer des Divertissements.
}
\livretFinAct Fin du Prologue
\sep

