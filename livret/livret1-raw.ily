\livretAct ACTE PREMIER
\livretDescAtt\column {
  \justify {
    La Scene est dans la Ville d’Ycolos en Thessalie.
  }
  \justify {
    Le Theatre represente une Port de Mer, où l’on void un grand
    Vaisseau orné & preparé pour une Feste galante au milieu de
    plusieurs Vaisseaux de guerre.
  }
}
\livretScene SCENE PREMIERE
\livretDescAtt\wordwrap-center {
  LE CHŒUR DES THESSALIENS, ALCIDE, LYCAS
}
\livretPers Le Chœur
\livretRef #'BAAchoeurRecit
%# Vivez, vivez, heureux Espoux.
\livretPers Lychas
%# Vostre Amy le plus cher épouze la Princesse
%# La plus charmante de la Grece,
%# Lors que chacun les suit, Seigneur, les fuy=ez-vous?
\livretPers Le Chœur
%# Vivez, vivez, heureux Espoux.
\livretPers Lychas
%#12 Vous paroissez troublé des cris qui retentissent?
%# Quand deux Amants heureux s'unissent
%# Le Cœur du grand Alcide en seroit-il jaloux?
\livretPers Le Chœur
%# Vivez, vivez, heureux Espoux.
\livretPers Lychas
%# Seigneur, vous soûpirez, & gardez le silence?
\livretPers Alcide
%# Ah Lychas, laisse-moy partir en diligence.
\livretPers Lychas
%# Quoy dés ce mesme jour presser vostre départ?
\livretPers Alcide
%# J'auray beau me presser je partiray trop tard.
%# Ce n'est point avec toy que je pretens me taire;
%# Alceste est trop aimable, elle a trop sçeu me plaire;
%# Un autre en est aimé, rien ne flatte mes vœux,
%# C'en est fait, Admete l'espouze,
%# Et c'est dans ce moment qu'on les unit tous deux.
%# Ah qu'une ame jalouse
%# Esprouve un tourment rigoureux!
%# J'ay peine à l'exprimer moy-mesme:
%# Figure-toy, si tu le peux,
%# Quelle est l'*horreur extresme
%# De voir ce que l'on aime
%# Au pouvoir d'un Rival heureux.
\livretPers Lychas
%# L'Amour est-il plus fort qu'un Heros indomptable?
%# L'univers n'a point eû de Monstre redoutable
%# Que vous n'ay=ez pû surmonter.
\livretPers Alcide
%# Eh crois-tu que l'Amour soit moins à redouter?
%# Le plus grand Cœur a sa foiblesse.
%# Je ne puis me sauver de l'ardeur qui me presse
%# Qu'en quittant ce fatal Séjour:
%# Contre d'aimables charmes,
%# La Valeur est sans armes,
%# Et ce n'est qu'en fuy=ant qu'on peut vaincre l'Amour.
\livretPers Lychas
%# Vous devez vous forcer, au moins, à voir la Feste
%# Qui déja dans ce Port vous paroist toute preste.
%# Vostre fuite à present feroit un trop grand bruit;
%# Differez jusques à la nuit.
\livretPers Alcide
%# Ah Lycas! qu'elle nuit! ah qu'elle nuit funeste!
\livretPers Lychas
%# Tout le reste du jour voy=ez encore Alceste.
\livretPers Alcide
%# La voir encore?… hé bien differons mon départ,
%# Je te l'avois bien dit, je partiray trop tard.
%# Je vais la voir aimer un Espoux qui l'adore,
%# Je verray dans leurs yeux un tendre empressement:
%# Que je vais pay=er cherement
%# Le plaisir de la voir encore!

\livretScene SCENE II
\livretDescAtt\wordwrap-center {
  ALCIDE, STRATON, & LYCAS
}
\livretPers Ensemble
\livretRef #'BBAtrio
%# L’amour a bien des maux, mais le plus grand de tous
%# C'est le tourment d'estre jaloux.

\livretScene SCENE III
\livretDescAtt\wordwrap-center {
  STRATON, LYCAS
}
\livretPers Straton
\livretRef #'BCArecit
%# Lychas, j'ay deux mots à te dire.
\livretPers Lychas
%# Que veux-tu? parle; je t'entends.
\livretPers Straton
%# Nous sommes amis de tous temps;
%# Céphise, tu le sçais, me tient sous son Empire.
%# Tu suis par tout ses pas: qu'est-ce que tu pretens?
\livretPers Lychas
%# Je pretens rire.
\livretPers Straton
%# Pourquoy veux-tu troubler deux Cœurs qui sont contents?
\livretPers Lychas
%# Je pretens rire.
%# Tu peux à ton gré t'enflamer;
%# Chacun a sa façon d'aimer;
%# Qui voudra soûpirer, soûpire,
%# Je pretens rire.
\livretPers Straton
%# J'aime, & je suis aimé: laisse en paix nos amours.
\livretPers Lychas
%# Rien ne doit t'allarmer s'il est bien vray qu'on t'aime;
%# Un Rival rebutté donne un plaisir extresme.
\livretPers Straton
%# Un Rival quel qu'il soit importune toûjours.
\livretPers Lychas
%# Je voy ton amour sans colere,
%# Tu devrois en user ainsi:
%# Puisque Céphise t'a sçeu plaire,
%# Pourquoy ne veux-tu pas qu'elle me plaise aussi?
\livretPers Straton
%# A quoy sert-il d'aimer ce qu'il faut que l'on quitte?
%# Tu ne peux demeurer long-temps dans cette Cour.
\livretPers Lychas
%# Moins on a de momens à donner à l'Amour,
%# Et plus il faut qu'on en profite.
\livretPers Straton
%# J'aime depuis deux ans avec fidelité:
%# Je puis croire, sans vanité,
%# Que tu ne dois pas estre un Rival qui m'alarme.
\livretPers Lychas
%# J'ay pour moy la nouveauté,
%# En amour c'est un grand charme.
\livretPers Straton
%# Céphise m'a promis un cœur tendre, & constant.
\livretPers Lychas
%# Céphise m'en promet autant.
\livretPers Straton
%# Ah si je le croy=ois!… Mais tu n'es pas croy=able.
\livretPers Lychas
%# Croy-moy, fais ton profit d'un reste d'amitié,
%# Sers-toy d'un avis charitable
%# Que je te donne par pitié.
\livretPers Straton
%# Le mespris d'une volage
%# Doit estre un assés grand mal,
%# Et c'est un nouvel outrage
%# Que la pitié d'un Rival.
%# Elle vient l'Infidelle,
%# Pour chanter dans les Jeux dont je prens soins icy.
\livretPers Lychas
%# Je te laisse avec elle,
%# Il ne tiendra qu'à toy d'estre mieux éclaircy.

\livretScene SCENE IV
\livretDescAtt\wordwrap-center {
  CÉPHISE, STRATON
}
\livretPers Céphise
\livretRef #'BDBrecit
%# Dans ce beau jour, qu'elle *humeur sombre
%# Fais-tu voir à contre-temps?
\livretPers Straton
%# C'est que je ne suis pas du nombre
%# Des Amants qui sont contents.
\livretPers Céphise
%# Un ton grondeur & severe
%# N'est pas un grand agrément;
%# Le chagrin n'avance guere
%# Les affaires d'un Amant.
\livretPers Straton
%# Lychas vient de me faire entendre
%# Que je n'ay plus ton cœur, qu'il doit seul y pretendre,
%# Et que tu ne vois plus mon amour qu'à regret?
\livretPers Céphise
%# Lychas est peu discret…
\livretPers Straton
%# Ah je m'en doutois bien qu'il vouloit me surprendre.
\livretPers Céphise
%# Lychas est peu discret
%# D'avoir dit mon secret.
\livretPers Straton
%# Coment! il est donc vray! tu n'en fais point d'excuse?
%# Tu me trahis ainsi sans en estre confuse?
\livretPers Céphise
%# Tu te plains sans raison;
%# Est-ce une trahison
%# Quand on te desabuse?
\livretPers Straton
%# Que je suis estonné de voir ton changement!
\livretPers Céphise
%# Si je change d'Amant
%# Qu'y trouves-tu d'étrange?
%# Est-ce un sujet d'estonnement
%# De voir une Fille qui change?
\livretPers Straton
%# Apres deux ans passez dans un si doux lien,
%# Devois-tu jamais prendre une chaine nouvelle.
\livretPers Céphise
%# Ne contes-tu pour rien
%# D'estre deux ans fidelle?
\livretPers Straton
%# Par un espoir doux, & trompeur,
%# Pourquoy m'engageois-tu dans un amour si tendre?
%# Faloit-il me donner ton cœur
%# Puis que tu voulois le reprendre?
\livretPers Céphise
%# Quand je t'offrois mon cœur, c'estoit de bonne foy
%# Que n'empesche tu qu'on te l'oste?
%# Est-ce ma faute
%# Si Lychas me plaist plus que toy?
\livretPers Straton
%# Ingrate, est-ce le prix de ma perseverance?
\livretPers Céphise
%# Essaye un peu de l'inconstance:
%# C'est toy qui le premier m'apris à m'engager,
%# Pour recompense
%# Je te veux aprendre à changer.
\livretPers Straton & Céphise
\livretVerse#6 { Il faut \raise#0.7 { \left-brace#20 \raise#1 \column { aimer changer } \right-brace#20 } toûjours. }
%# Les plus douces amours
\livretVerse#6 { Sont les amours \raise#0.5 { \left-brace#20 \raise#1 \column { fidelles nouvelles } } }
\livretVerse#6 { Il faut \raise#0.7 { \left-brace#20 \raise#1 \column { aimer changer } \right-brace#20 } toûjours. }

\livretScene SCENE V
\livretDescAtt\wordwrap-center {
  LICOMEDE, STRATON, CÉPHISE
}
\livretPers Licomede
\livretRef #'BEArecit
%# Straton, donne ordre qu'on s'apreste
%# Pour commencer la Feste.
\livretDidasP\justify {
  Straton se retire, & Licomede parle à Céphise.
}
%# Enfin, grace au dépit, je gouste la douceur
%# De sentir le repos de retour dans mon cœur.
%# J'estois à preferer au Roy de Thessalie;
%# Et si pour sa gloire on publie
%# Qu'Apollon autrefois luy servit de Pasteur,
%# Je suis Roy de Scyros, & Thétis est ma Sœur.
%# J'ay sçeu me consoler d'un hymen qui m'outrage,
%# J'en ordonne les Jeux avec tranquilité.
%# Qu'aisément le dépit dégage
%# Des fers d'une ingrate Beauté!
%# Et qu'apres un long esclavage,
%# Il est doux d'estre en liberté!
\livretPers Céphise
%# Il n'est pas seur toûjours de croire l'apparence:
%# Un Cœur bien pris, & bien touché,
%# N'est pas aisément détaché,
%# Ny si tost guery que l'on pense;
%# Et l'amour est souvent caché
%# Sous une feinte indifference.
\livretPers Licomede
%# Quand on est sans esperance,
%# On est bien tost sans amour.
%# Mon Rival a la preference,
%# Ce que j'aime est en sa puissance,
%# Je perds tout espoir en ce jour:
%# Quand on est sans esperance,
%# On est bien tost sans amour.
%# Voicy l'*heure qu'il faut que la Feste commence,
%# Chacun s'avance,
%# Preparons-nous.

\livretScene SCENE VI
\livretDescAtt\wordwrap-center {
  LE CHŒUR, ADMETE, ALCESTE, PHERES, ALCIDE, LYCHAS, CÉPHISE & STRATON
}
\livretPers Le Chœur
\livretRef #'BFAchoeur
%# Vivez, vivez, heureux Espoux.
\livretPers Pheres
%# Joü=issez des douceurs du nœud qui vous assemble.
\livretPers Admete & Alceste
%# Quand l'*Himen & l'Amour sont bien d'accord ensemble
%# Que les nœuds qu'ils forment sont doux?
\livretPers Le Chœur
%# Vivez, vivez, heureux Espoux.

\livretScene SCENE VII
\livretDescAtt\wordwrap-center {
  Des Nymphes de la Mer, & des Tritons, viennent faire une Feste Marine,
  où se meslent des Matelots & des Pescheurs.
}
\livretPers Deux Tritons
\livretRef #'BGBtritons
%# Malgré tant d'orages,
%# Et tant de naufrages,
%# Chacun à son tour
%# S'embarque avec l'Amour.
%# Par tout où l'on meine
%# Les Cœurs amoureux,
%# On voit la Mer pleine
%# D'Escueils dangereux,
%# Mais sans quelque peine
%# On n'est jamais heureux:
%# Une ame constante
%# Apres la tourmente
%# Espere un beau jour.
%# Malgré tant d'orages,
%# Et tant de naufrages,
%# Chacun à son tour
%# S'embarque avec l'Amour.
\null
%# Un cœur qui differe
%# D'entrer en affaire
%#5 S'expose à manquer
%#6 Le temps de s'embarquer.
%# Une ame commune
%# S'estonne d'abord,
%# Le soin l'importune,
%# Le calme l'endort,
%# Mais quelle fortune
%# Fait-on sans quelque effort?
%# Est-il un commerce
%# Exempt de traverse?
%#5 Chacun doit risquer.
%# Un cœur qui differe
%# D'entrer en affaire
%#5 S'expose à manquer
%#6 Le temps de s'embarquer.
\null
\livretDidasP\justify {
  Céphise vestüe en Nymphe de la Mer, chante au milieu des Divinitez
  Marines qui luy respondent.
}
\livretRef #'BGEchoeur
%# Jeunes Cœurs laissez-vous prendre
%# Le peril est grand d'attendre,
%# Vous perdez d'heureux moments
%# En cherchant à vous défendre;
%# Si l'Amour a des tourments
%# C'est la faute des Amants.
\null
\livretRef #'BGGchoeur
%# Plus les ames sont rebelles,
%# plus leurs peines sont cruelle,
%# Les plaisirs doux et charmants,
%# Sont le prix des cœurs fidelles ;
%# Si l’amour a des tourments
%# C’est la faute des amants.
\livretPersDidas Licomede à Alceste
\livretRef #'BGHrecit
%# On vous apreste
%# Dans mon Vaisseau
%# Un divertissement nouveau.
\livretPers Licomede & Straton
%# Venez voir ce que nostre Feste
%# Doit avoir de plus beau.
\null
\livretDidasP\justify {
  Licomede conduit Alceste dans son Vaisseau, Straton y meine Céphise,
  & dans le temps qu’Admete & Alcide y veulent passer, le Pont
  s’enfonce dans la Mer.
}
\livretPers Admete & Alcide
%# Dieux! le Pont s'abisme dans l'eau.
\livretPers Licomede & Straton
%# Venez voir ce que nostre feste
%# doit avoir de plus beau.
\livretPers Le Chœur des Thessaliens
%# Ah quelle trahison funeste.
\livretPers Alceste & Céphise
%# Au secours, au secours.
\livretPers Alcide
%#- Perfide…
\livretPers Admete
%#= Alceste…
\livretPers Alcide & Admete
%# Laissons les vains discours.
%# Au secours, au secours.
\livretDidasP\justify {
  Les Thessaliens courent s’embarquer pour suivre Licomede.
}
\livretPers Le Chœur des Thessaliens
%# Au secours, au secours.

\livretScene SCENE VIII
\livretDescAtt\wordwrap-center {
  THETIS, ADMETE
}
\livretPersDidas Thetis sortant de la Mer
\livretRef #'BHArecit
%# Espoux infortuné redoute ma colere,
%# Tu vas haster l'instant qui doit finir tes jours;
%# C'est Thétis que la Mer revere,
%# Que tu vois contre toy du party de son Frere;
%# Et c'est à la mort que tu cours.
\livretPersDidas Admete courant s’embarquer
%# Au secours, au secours.
\livretPers Thetis
%# Puis qu'on mesprise ma puissance
%# Que les vents deschainez
%# Que les flots mutinez
%# S'arment pour ma vengeance.
\null
\livretDidasP\justify {
  Thetis rentre dans la Mer, & les Aquilons excitent une tempeste qui
  agite les Vaisseaux qui s'efforcent de poursuivre Licomede.
}

\livretScene SCENE IX
\livretDescAtt\wordwrap-center {
  ÉOLE, LES AQUILONS, LES ZEPHIRS
}
\livretPers Éole
\livretRef #'BIAeole
%# Le Ciel protege les Heros:
%# Allez Admete, allez Alcide;
%# Le Dieu qui sur les Dieux preside
%# M'ordonne de calmer les flots:
%# Allez, poursuivez un perfide.
%# Retirez-vous
%# Vents en courroux,
%# Rentrez dans vos prisons profondes:
%# Et laissez regner sur les ondes
%# Les Zephirs les plus doux.
\null
\livretDidasP\justify {
  L’orage cesse, les Zephirs volent & font fuïr les Aquilons qui
  tombent dans la Mer avec les nuages qu’ils en avoient élevez, & les
  Vaisseaux d’Alcide & d’Admete poursuivent Licomede.
}
\livretFinAct Fin du premier Acte
\sep

