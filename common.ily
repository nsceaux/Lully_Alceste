\version "2.19.37"

\header {
  copyrightYear = "2016"
  composer = "Jean-Baptiste Lully"
  poet = "Philippe Quinault"
  date = "1674"
}

#(ly:set-option 'original-layout (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'use-ancient-clef (eqv? (ly:get-option 'urtext) #t))
#(ly:set-option 'show-ancient-clef (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))
#(ly:set-option 'use-rehearsal-numbers #t)
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
%% Staff size
#(set-global-staff-size
  (cond ((not (symbol? (ly:get-option 'part))) 16)
        ((memq (ly:get-option 'part) '(basse-continue)) 18)
        (else 20)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking (if (eqv? (ly:get-option 'part) #f)
                             ly:optimal-breaking
                             ly:page-turn-breaking))
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"
\opusPartSpecs
#`((dessus       "Dessus"        () (#:notes "dessus"))
   (haute-contre "Hautes-contre" () (#:notes "haute-contre" #:clef "alto"))
   (taille       "Tailles"       () (#:notes "taille" #:clef "alto"))
   (quinte       "Quintes"       () (#:notes "quinte" #:clef "alto"))
   (basse        "Basses"        () (#:notes "basse" #:tag-notes basse #:clef "basse"))
   (timbales     "Timbales"      () (#:notes "timbales" #:clef "basse"))
   (trompette    "Trompette"     () (#:notes "trompette" #:clef "treble"))
   (basse-continue
    "Basse continue" ()
    (#:notes "basse" #:tag-notes basse-continue #:clef "basse")))
\opusTitle "Alceste"

\header {
  maintainer = \markup {
    Nicolas Sceaux,
    \with-url #"http://www.lestalenslyriques.com" \line {
      Les Talens Lyriques – Christophe Rousset
    }
  }
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
}
